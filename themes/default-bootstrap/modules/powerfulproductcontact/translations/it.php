<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{powerfulproductcontact}default-bootstrap>header_e52cc8e7ada188b98c70ab9dcf0daee5'] = 'See e modificare le impostazioni';
$_MODULE['<{powerfulproductcontact}default-bootstrap>header_f4f70727dc34561dfde1a3c529b6205c'] = 'Impostazioni';
$_MODULE['<{powerfulproductcontact}default-bootstrap>header_9da9313497e398af0103ad33ab08136e'] = 'Gestire i campi visualizzati';
$_MODULE['<{powerfulproductcontact}default-bootstrap>header_a4ca5edd20d0b5d502ebece575681f58'] = 'campi';
$_MODULE['<{powerfulproductcontact}default-bootstrap>form_4ec1c39345fe8820d68463eea8803b0f'] = 'Il tuo messaggio è stato inviato con successo al nostro team.';
$_MODULE['<{powerfulproductcontact}default-bootstrap>form_94966d90747b97d1f0f206c98a8b1ac3'] = 'Inviare';
$_MODULE['<{powerfulproductcontact}default-bootstrap>form17_4ec1c39345fe8820d68463eea8803b0f'] = 'Il tuo messaggio è stato inviato con successo al nostro team.';
$_MODULE['<{powerfulproductcontact}default-bootstrap>form17_9ead913c5b7c87efda39ca9640c46ae4'] = 'Ci sono %d errori';
$_MODULE['<{powerfulproductcontact}default-bootstrap>form17_5fbddd8d4e0d8c7a868272f2f171df09'] = 'C\'è %d errore';
$_MODULE['<{powerfulproductcontact}default-bootstrap>form17_94966d90747b97d1f0f206c98a8b1ac3'] = 'Inviare';
$_MODULE['<{powerfulproductcontact}default-bootstrap>ppcrenderer_2deae2efe4b65df80c188a31fed3cc17'] = 'è obbligatorio Il campo %s.';
$_MODULE['<{powerfulproductcontact}default-bootstrap>ppcrenderer_01a1a3023ca46c56520e554398e59496'] = 'Il campo %s deve essere un indirizzo email valido.';
$_MODULE['<{powerfulproductcontact}default-bootstrap>ppcrenderer_fc5f6e0858c5417e009314f7f1d640dc'] = 'Il campo %s deve essere un URL valido.';
$_MODULE['<{powerfulproductcontact}default-bootstrap>ppcrenderer_bb9405bfd4a9c7f2bf3d3aee585ead07'] = 'Voce non valida dato per %s.';
$_MODULE['<{powerfulproductcontact}default-bootstrap>ppcrenderer_93cba07454f06a4a960172bbd6e2a435'] = 'Si';
$_MODULE['<{powerfulproductcontact}default-bootstrap>ppcrenderer_bafd7322c6e97d25b6299b5d6fe8920b'] = 'No';
