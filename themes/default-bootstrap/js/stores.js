/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
$(document).ready(function(){

    lat = document.getElementById('lat');

	console.log("Stores.js 1");

	if(lat != "") {

        console.log("Stores.js 2");

        map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(defaultLat, defaultLong),
            zoom: 4,
            mapTypeId: 'roadmap',
            scrollwheel: true,
            scaleControl: true,
            draggable: true,
            navigationControl: false,
            mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU}
        });
        infoWindow = new google.maps.InfoWindow();


        initMarkers();

    }


    $("a.myclick").click(function(event) {
        event.preventDefault();
        var i = $(this).data('repair');
		i = parseInt(i)-1;

        map.setCenter(gmarkers[i].getPosition());
        google.maps.event.trigger(gmarkers[i], "click");

        //Do stuff when clicked
    });


});

function myclick(i) {

	console.log("clickcccc");

    var i = 2;   map.setCenter(gmarkers[i].getPosition());
    google.maps.event.trigger(gmarkers[i], "click");

}

function success(position) {
     var lat = position.coords.latitude;
     var lon = position.coords.longitude;
	 
console.log(lat+' '+lon);

  /*   var lat = 44.49489;
     var lon = 	11.34262;	 */
	 var geocoder = new google.maps.Geocoder();
	
				var latlng = new google.maps.LatLng(lat, lon);
				 geocoder.geocode({
				'latLng': latlng
					  }, function (results, status) { 
						if (status === google.maps.GeocoderStatus.OK) {
						  if (results[1]) {
							console.log(results[0].formatted_address);
							 $('#addressInput').val(results[0].formatted_address);
								//passaggio con distance abilitata
									searchLocations("OK");

						  } else {
								//alert('No results found');
						  }
						} else {
							alert('Geocoder failed due to: ' + status);
						}
				});	 
		
}


function initMarkers()
{
	searchUrl += '?ajax=1&all=1&cat='+setIdcat+'&posti='+setPosti+'&latitude='+setLat+'&longitude='+setLng;
	downloadUrl(searchUrl, function(data) {

        console.log("call 111 --->");

		var xml = parseXml(data.trim());
		var markerNodes = xml.documentElement.getElementsByTagName('marker');
		var bounds = new google.maps.LatLngBounds();
		for (var i = 0; i < markerNodes.length; i++)
		{
			var name = markerNodes[i].getAttribute('name');
			var address = markerNodes[i].getAttribute('address');
			var addressNoHtml = markerNodes[i].getAttribute('addressNoHtml');
			var other = markerNodes[i].getAttribute('other');
			var id_store = markerNodes[i].getAttribute('id_product');

			var has_store_picture = markerNodes[i].getAttribute('has_store_picture');

			//console.log("----------------------------------"+markerNodes[i].getAttribute('has_store_picture'));

            var store_img = markerNodes[i].getAttribute('picture');

            //console.log("----------------------------------"+markerNodes[i].getAttribute('picture'));

			var latlng = new google.maps.LatLng(
			parseFloat(markerNodes[i].getAttribute('lat')),
			parseFloat(markerNodes[i].getAttribute('lng')));

            var picture = markerNodes[i].getAttribute('picture');

            var link = markerNodes[i].getAttribute('link');



			createMarker(latlng, name, address, other, id_store, has_store_picture, picture, link);
			bounds.extend(latlng);
		}
		 markerCluster = new MarkerClusterer(map, markers, {imagePath: '../img/m'});
			       
		
		map.fitBounds(bounds);
		var zoomOverride = map.getZoom();
        if(zoomOverride > 10)
        	zoomOverride = 10;
		map.setZoom(zoomOverride);
	});
	
}

function searchLocations(geolocation)
{
	$('#stores_loader').show();
	var address = document.getElementById('addressInput').value;
	var geocoder = new google.maps.Geocoder();
	
	var holter_pressorio = $('#holter_pressorio:checked').is(":checked");
	var sleep_monitor = $('#sleep_monitor').is(":checked");
	var holter_egc_epatch = $('#holter_egc_epatch:checked').is(":checked");
	var ecg_elettrocardiogramma = $('#ecg_elettrocardiogramma:checked').is(":checked");
	 
	 geoloc = 0;

	if(geolocation=="OK") {
				geoloc = 1;
    }
				
	
	
	geocoder.geocode({address: address}, function(results, status) {
		if (status === google.maps.GeocoderStatus.OK)
			searchLocationsNear(results[0].geometry.location,geoloc);
		else
		{
			if (!!$.prototype.fancybox && isCleanHtml(address))
			    $.fancybox.open([
			        {
			            type: 'inline',
			            autoScale: true,
			            minHeight: 30,
			            content: '<p class="fancybox-error">' + address + ' ' + translation_6 + '</p>'
			        }
			    ], {
			        padding: 0
			    });
			else
			    alert(address + ' ' + translation_6);
		}
		$('#stores_loader').hide();
	});
}

function clearLocations(n)
{
	infoWindow.close();
	for (var i = 0; i < markers.length; i++)
		markers[i].setMap(null);

	markers.length = 0;
	


	locationSelect.innerHTML = '';
	var option = document.createElement('option');
	option.value = 'none';
	if (!n)
		option.innerHTML = translation_1;
	else
	{
		if (n === 1)
			option.innerHTML = '1'+' '+translation_2;
		else
			option.innerHTML = n+' '+translation_3;
	}
	locationSelect.appendChild(option);

	if (!!$.prototype.uniform)
		$("select#locationSelect").uniform();

	$('#stores-table tr.node').remove();
}

function searchLocationsNear(center, geoloc)
{
	
	
	var holter_pressorio = $('#holter_pressorio:checked').is(":checked");
	var sleep_monitor = $('#sleep_monitor').is(":checked");
	var holter_egc_epatch = $('#holter_egc_epatch:checked').is(":checked");	
	var ecg_elettrocardiogramma = $('#ecg_elettrocardiogramma:checked').is(":checked");	
	
	
	var esami = $( "#esami option:selected" ).val();

	holter_pressorio = 0;
	sleep_monitor = 0;
	holter_egc_epatch = 0;
	ecg_elettrocardiogramma = 0;

	if(esami==1) holter_pressorio = 1;
	if(esami==2) sleep_monitor = 1;
	if(esami==3) holter_egc_epatch = 1;
	if(esami==4) ecg_elettrocardiogramma = 1;

	console.log(esami);

	console.log(holter_pressorio);

	console.log(sleep_monitor);

	console.log(holter_egc_epatch);

	console.log(ecg_elettrocardiogramma);



	var regione = document.getElementById('country-list').value;
	var provincia = document.getElementById('state-list').value;
	var citta = document.getElementById('state-list2').value;
	var farmacia = document.getElementById('state-list3').value;	
	 var trov = 1;
	
	
	
	var radius = document.getElementById('radiusSelect').value;
	var searchUrl = baseUri+'?controller=stores&ajax=1&latitude=' + center.lat() + '&longitude=' + center.lng() + '&radius=' + radius + '&ecg_elettrocardiogramma=' + ecg_elettrocardiogramma + '&holter_pressorio=' + holter_pressorio + '&sleep_monitor=' + sleep_monitor + '&holter_egc_epatch='+holter_egc_epatch+'&regione='+regione+'&provincia='+provincia+'&citta='+citta+'&farmacia='+farmacia+'&geoloc='+geoloc;
	downloadUrl(searchUrl, function(data) {
		var xml = parseXml(data.trim());
		var markerNodes = xml.documentElement.getElementsByTagName('marker');
		var bounds = new google.maps.LatLngBounds();
        
       
        
		clearLocations(markerNodes.length);
		
	
		
		$('table#stores-table').find('tbody tr').remove();
		for (var i = 0; i < markerNodes.length; i++)
		{
			var name = markerNodes[i].getAttribute('name');



			var address = markerNodes[i].getAttribute('address');
			var addressNoHtml = markerNodes[i].getAttribute('addressNoHtml');
			var other = markerNodes[i].getAttribute('other');
			var distance = parseFloat(markerNodes[i].getAttribute('distance'));
			var id_store = parseFloat(markerNodes[i].getAttribute('id_store'));
			var phone = markerNodes[i].getAttribute('phone');
			var has_store_picture = markerNodes[i].getAttribute('has_store_picture');


			var latlng = new google.maps.LatLng(
			parseFloat(markerNodes[i].getAttribute('lat')),
			parseFloat(markerNodes[i].getAttribute('lng')));

			createOption(name, distance, i);
			
			createMarker(latlng, name, address, other, id_store, has_store_picture);


			/*
			var gmarkers = [];
      var htmls = [];
      var i = 0;
			 */

			bounds.extend(latlng);
			address = address.replace(phone, '');

			$('table#stores-table').find('tbody').append('<tr ><td class="num">'+parseInt(i + 1)+'</td><td class="name">'+(has_store_picture == 1 ? '<img src="'+img_store_dir+parseInt(id_store)+'.jpg" alt="" />' : '')+'<span>'+name+'</span></td><td class="address">'+address+(phone !== '' ? ''+translation_4+' '+phone : '')+'</td><td class="distance">'+distance+' '+distance_unit+'</td></tr>');
			//$('#stores-table').show();
		}
		     
		if (markerNodes.length)
		{
		    console.log("risultato");
			map.fitBounds(bounds);
			var listener = google.maps.event.addListener(map, "idle", function() {
				if (map.getZoom() > 13) map.setZoom(13);
				google.maps.event.removeListener(listener);
			});
		} else {
			console.log("nessun risultato");
			$('#msgalert').text("Nessun risultato per la zona selezionata. Guarda la farmacia più vicina");
			trov = 0;
		}
		 
		if(trov) {
    		locationSelect.style.visibility = 'visible';
    		//$(locationSelect).parent().parent().addClass('active').show();
    		locationSelect.onchange = function() {
    			var markerNum = locationSelect.options[locationSelect.selectedIndex].value;
    			google.maps.event.trigger(markers[markerNum], 'click');
    		};
		}	else {
		     
		       
                	var searchUrl = baseUri+'?controller=stores&ajax=1&latitude=' + center.lat() + '&news=1&longitude=' + center.lng() + '&radius=' + radius + '&ecg_elettrocardiogramma=' + ecg_elettrocardiogramma + '&holter_pressorio=' + holter_pressorio + '&sleep_monitor=' + sleep_monitor + '&holter_egc_epatch='+holter_egc_epatch+'&regione='+regione+'&provincia='+provincia+'&citta='+citta+'&farmacia='+farmacia+'&geoloc='+geoloc;
   
                                     
                        	downloadUrl(searchUrl, function(data) {
                            		var xml = parseXml(data.trim());
                            		var markerNodes = xml.documentElement.getElementsByTagName('marker');
                            		var bounds = new google.maps.LatLngBounds();
                                    
                                    //console.log("search");
                                    
                            		clearLocations(markerNodes.length);
                        		
                        		
		
                            		$('table#stores-table').find('tbody tr').remove();
                            		for (var i = 0; i < markerNodes.length; i++)
                            		{
                            			var name = markerNodes[i].getAttribute('name');
                            			var address = markerNodes[i].getAttribute('address');
                            			var addressNoHtml = markerNodes[i].getAttribute('addressNoHtml');
                            			var other = markerNodes[i].getAttribute('other');
                            			var distance = parseFloat(markerNodes[i].getAttribute('distance'));
                            			var id_store = parseFloat(markerNodes[i].getAttribute('id_store'));
                            			var phone = markerNodes[i].getAttribute('phone');
                            			var has_store_picture = markerNodes[i].getAttribute('has_store_picture');
                            			var latlng = new google.maps.LatLng(
                            			parseFloat(markerNodes[i].getAttribute('lat')),
                            			parseFloat(markerNodes[i].getAttribute('lng')));
                            
                            			createOption(name, distance, i);
                            			
                            			createMarker(latlng, name, address, other, id_store, has_store_picture); 
                            			bounds.extend(latlng);
                            			address = address.replace(phone, '');
                            
                            			$('table#stores-table').find('tbody').append('<tr ><td class="num">'+parseInt(i + 1)+'</td><td class="name">'+(has_store_picture == 1 ? '<img src="'+img_store_dir+parseInt(id_store)+'.jpg" alt="" />' : '')+'<span>'+name+'</span></td><td class="address">'+address+(phone !== '' ? ''+translation_4+' '+phone : '')+'</td><td class="distance">'+distance+' '+distance_unit+'</td></tr>');
                            			//$('#stores-table').show();
                            		}
		
                        		    
                        		    		if (markerNodes.length)
                                    		{
                                    			map.fitBounds(bounds);
                                    			var listener = google.maps.event.addListener(map, "idle", function() {
                                    				if (map.getZoom() > 13) map.setZoom(13);
                                    				google.maps.event.removeListener(listener);
                                    			});
                                    		} else {
                                    		
                                    		
                                    		}
		
                        	});
		       
		       
		}	
		
		
	});
	
	

	
	
	 // var markerCluster = new MarkerClusterer(map, markers, {imagePath: '../img/m'});

markerCluster.clearMarkers();	
}

var gmarkers = [];
var htmls = [];
var i = 0;

function createMarker(latlng, name, address, other, id_store, has_store_picture, picture, link)
{

    //console.log(link+"---------------------------------->>>>>>>"+picture);

	var html = '<a href="'+link+'"><b>'+name+'</b></a><br/>'+address+'<br />'+other+(has_store_picture == 1 ? '<br /><br /><img src="'+picture+'" alt="" /><br />' : '')+'<br />'; //<a href="http://maps.google.com/maps?saddr=&daddr='+latlng+'" target="_blank">'+translation_5+'<\/a>
	var image = new google.maps.MarkerImage(img_ps_dir+logo_store);
	var marker = '';

	//console.log(hasStoreIcon+"---->>>>>>>>"+image);

    marker = new google.maps.Marker({ map: map, icon: image, position: latlng });

    gmarkers[i] = marker;
    htmls[i] = html;

	/*
	if (hasStoreIcon)
		marker = new google.maps.Marker({ map: map, icon: image, position: latlng });
	else
		marker = new google.maps.Marker({ map: map, position: latlng });
		*/
	google.maps.event.addListener(marker, 'click', function() {
		infoWindow.setContent(html);
		infoWindow.open(map, marker);
	});


    i++;
	//console.log(gmarkers);

	markers.push(marker);
	

}

function createOption(name, distance, num)
{
	var option = document.createElement('option');
	option.value = num;
	option.innerHTML = name+' ('+distance.toFixed(1)+' '+distance_unit+')';
	locationSelect.appendChild(option);
}

function downloadUrl(url, callback)
{
	var request = window.ActiveXObject ?
	new ActiveXObject('Microsoft.XMLHTTP') :
	new XMLHttpRequest();

	request.onreadystatechange = function() {
		if (request.readyState === 4) {
			request.onreadystatechange = doNothing;
			callback(request.responseText, request.status);
		}
	};

	request.open('GET', url, true);
	request.send(null);
}

function parseXml(str)
{
	if (window.ActiveXObject)
	{
		var doc = new ActiveXObject('Microsoft.XMLDOM');
		doc.loadXML(str);
		return doc;
	}
	else if (window.DOMParser)
		return (new DOMParser()).parseFromString(str, 'text/xml');
}

function doNothing()
{
}
