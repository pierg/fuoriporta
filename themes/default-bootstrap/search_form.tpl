<div class="search_container container">
  <form action="">
    <div class="row link_bar">
      <div class="col-md-6">
        <div class="top_link">
          <a href="#">Alloggi</a>
        </div>
      </div>
      <div class="col-md-6">
        <div class="top_link">
          <a href="#">Esperienze</a>
        </div>
      </div>
    </div>
    <div class="search_form">

      <div class="search_form_container">

        <div class="form-group lavoro">
          <input type="checkbox" class="beautyform" data-beautytype="checkbox" name="work" id="work" value="1">
          <label for="work">Viaggi per lavoro</label>
          <div class="clearfix"></div>
        </div>

        <div class="form-group dove">
          <label for="where">Dove</label>
          <input type="text" class="form-control" id="where" placeholder="Parma, Cagliari, Valencia, ...">
        </div>
        <div class="form-group quando">
          <label for="da_data">Quando</label>
          <input type="text" class="form-control" id="da_data" placeholder="gg/mm/aaa">
          <input type="text" class="form-control" id="a_data" placeholder="gg/mm/aaa">
          <div class="clearfix"></div>
        </div>
        <div class="form-group ospiti">
          <label for="ospiti">ospiti</label>
          <!-- <input type="text" class="form-control" id="ospiti"> -->
          <select name="adulti" id="adulti" class="beautyform" data-beautytype="select" data-placeholder="Adulti">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
          </select>
          <select name="bambini" id="bambini" class="beautyform" data-beautytype="select" data-placeholder="Bambini (0-14)">
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
          </select>
        </div>

        <div class="form-group submit">
          <input type="submit" class="btn btn-default cerca" value="Cerca">
        </div>

      </div>
    </div>
  </form>
</div>
