<?php /* Smarty version Smarty-3.1.19, created on 2019-01-11 12:39:45
         compiled from "C:\MAMP\htdocs\fuoriporta\modules\myownreservations\views\templates\front\planning\_partials\line_day.tpl" */ ?>
<?php /*%%SmartyHeaderCode:217465c38808190a185-19540448%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e8f0d2d75deeb1ccfbb0b3df64501ecd88170946' => 
    array (
      0 => 'C:\\MAMP\\htdocs\\fuoriporta\\modules\\myownreservations\\views\\templates\\front\\planning\\_partials\\line_day.tpl',
      1 => 1545145951,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '217465c38808190a185-19540448',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'widgetHome' => 0,
    'reservationView' => 0,
    'mediumSize' => 0,
    'linetype' => 0,
    'id_lang' => 0,
    'id_product' => 0,
    'productsImages' => 0,
    'product' => 0,
    'viewedProduct' => 0,
    'myownlink' => 0,
    'isDaySel' => 0,
    'timeListStr' => 0,
    'isNightSel' => 0,
    'potentialResa' => 0,
    'cell_days' => 0,
    'reservationMonth' => 0,
    'cell_day' => 0,
    'dayParallel' => 0,
    'calendar' => 0,
    'col_ts' => 0,
    'col_key' => 0,
    'timeSlotObj' => 0,
    'timeSlot' => 0,
    'timeListTime' => 0,
    'cell_class' => 0,
    'cutTimes' => 0,
    'timeLength' => 0,
    'planning_cell' => 0,
    'planningType' => 0,
    'planningStyle' => 0,
    'cell_content' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5c388081a3aac5_48080875',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5c388081a3aac5_48080875')) {function content_5c388081a3aac5_48080875($_smarty_tpl) {?>			<?php if ($_smarty_tpl->tpl_vars['widgetHome']->value!=2&&$_smarty_tpl->tpl_vars['reservationView']->value->homeProducts!=array()) {?>
					<td class="myOwnCalendarProduct" width="<?php echo $_smarty_tpl->tpl_vars['mediumSize']->value->width;?>
px">
						<?php if ($_smarty_tpl->tpl_vars['linetype']->value=="sel") {?>
							<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_smarty_tpl->tpl_vars['id_product'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['reservationView']->value->getAllProducts($_smarty_tpl->tpl_vars['id_lang']->value); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
 $_smarty_tpl->tpl_vars['id_product']->value = $_smarty_tpl->tpl_vars['product']->key;
?>
								<?php if (array_key_exists($_smarty_tpl->tpl_vars['id_product']->value,$_smarty_tpl->tpl_vars['productsImages']->value)) {?>
					            	<?php $_smarty_tpl->tpl_vars['viewedProduct'] = new Smarty_variable($_smarty_tpl->tpl_vars['productsImages']->value[$_smarty_tpl->tpl_vars['id_product']->value], null, 0);?>
					            			<div class="block_content thumbnail-container" style="padding-top:1px;height:inherit;width:inherit;margin-bottom:2px">
												<center>
													<?php if ($_smarty_tpl->tpl_vars['mediumSize']->value->width>0) {?>
														<div style="<?php if ($_smarty_tpl->tpl_vars['product']->value['available']) {?>background-image:url(<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['mediumSize']->value->name;?>
<?php $_tmp1=ob_get_clean();?><?php echo $_smarty_tpl->tpl_vars['myownlink']->value->getImageLink($_smarty_tpl->tpl_vars['viewedProduct']->value->link_rewrite,$_smarty_tpl->tpl_vars['viewedProduct']->value->cover,$_tmp1);?>
);<?php }?>height:<?php echo $_smarty_tpl->tpl_vars['mediumSize']->value->height;?>
px;width:<?php echo $_smarty_tpl->tpl_vars['mediumSize']->value->width;?>
px" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['viewedProduct']->value->legend, ENT_QUOTES, 'UTF-8', true);?>
"></div>
													<?php } else { ?>
														<div style="height:30px;width:<?php echo $_smarty_tpl->tpl_vars['mediumSize']->value->width;?>
px"></div>
													<?php }?>
													<h6 class="widget_title"><?php if ($_smarty_tpl->tpl_vars['product']->value['available']) {?><a id="product_link_<?php echo $_smarty_tpl->tpl_vars['viewedProduct']->value->id;?>
" href="<?php echo $_smarty_tpl->tpl_vars['myownlink']->value->getProductLink($_smarty_tpl->tpl_vars['viewedProduct']->value->id,$_smarty_tpl->tpl_vars['viewedProduct']->value->link_rewrite,$_smarty_tpl->tpl_vars['viewedProduct']->value->category_rewrite);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['viewedProduct']->value->name, ENT_QUOTES, 'UTF-8', true);?>
"><?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate($_smarty_tpl->tpl_vars['viewedProduct']->value->name,14,'...'), ENT_QUOTES, 'UTF-8', true);?>
</a><?php }?></h5>
												</center>
											</div>
								<?php }?>
							<?php } ?>
						<?php }?>
					</td>
			<?php }?>
			<?php if (!$_smarty_tpl->tpl_vars['isDaySel']->value) {?>
            	    <td class="myOwnCalendarHours">
            	        <?php if ($_smarty_tpl->tpl_vars['linetype']->value=="sel") {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['timeListStr']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>
            	    </td>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['isNightSel']->value&&(($_smarty_tpl->tpl_vars['linetype']->value=="sel"&&$_smarty_tpl->tpl_vars['potentialResa']->value->_selType!="end")||($_smarty_tpl->tpl_vars['linetype']->value=="top"&&$_smarty_tpl->tpl_vars['potentialResa']->value->_selType=="end"))) {?>
					<td<?php if ($_smarty_tpl->tpl_vars['linetype']->value=="sel") {?> class="myOwnCalendarLineItem"<?php }?>></td>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['cell_days']->value!=array()) {?>
	            <?php  $_smarty_tpl->tpl_vars['cell_day'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cell_day']->_loop = false;
 $_smarty_tpl->tpl_vars['cell_key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['cell_days']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cell_day']->key => $_smarty_tpl->tpl_vars['cell_day']->value) {
$_smarty_tpl->tpl_vars['cell_day']->_loop = true;
 $_smarty_tpl->tpl_vars['cell_key']->value = $_smarty_tpl->tpl_vars['cell_day']->key;
?>
	            	<?php if (!isset($_smarty_tpl->tpl_vars['reservationMonth']->value)||$_smarty_tpl->tpl_vars['linetype']->value=="top"||(isset($_smarty_tpl->tpl_vars['reservationMonth']->value)&&$_smarty_tpl->tpl_vars['reservationMonth']->value->isDayIn($_smarty_tpl->tpl_vars['cell_day']->value->date))) {?>
					
 <?php  $_smarty_tpl->tpl_vars['col_ts'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['col_ts']->_loop = false;
 $_smarty_tpl->tpl_vars['col_key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['dayParallel']->value[$_smarty_tpl->tpl_vars['cell_day']->value->getDayNumber()]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['col_ts']->key => $_smarty_tpl->tpl_vars['col_ts']->value) {
$_smarty_tpl->tpl_vars['col_ts']->_loop = true;
 $_smarty_tpl->tpl_vars['col_key']->value = $_smarty_tpl->tpl_vars['col_ts']->key;
?>

					   <?php if (!$_smarty_tpl->tpl_vars['isDaySel']->value&&$_smarty_tpl->tpl_vars['linetype']->value!="top") {?>
						   	 <?php if (isset($_smarty_tpl->tpl_vars['timeListStr']->value)&&$_smarty_tpl->tpl_vars['calendar']->value->getTimeListTime($_smarty_tpl->tpl_vars['cell_day']->value->date,$_smarty_tpl->tpl_vars['timeListStr']->value)>=$_smarty_tpl->tpl_vars['reservationView']->value->start&&$_smarty_tpl->tpl_vars['calendar']->value->getTimeListTime($_smarty_tpl->tpl_vars['cell_day']->value->date,$_smarty_tpl->tpl_vars['timeListStr']->value)<$_smarty_tpl->tpl_vars['reservationView']->value->end) {?>
						       <?php $_smarty_tpl->tpl_vars['timeSlot'] = new Smarty_variable($_smarty_tpl->tpl_vars['timeSlotObj']->value->getTimeSlotOfTimeStr($_smarty_tpl->tpl_vars['cell_day']->value->date,$_smarty_tpl->tpl_vars['timeListStr']->value,$_smarty_tpl->tpl_vars['col_ts']->value,$_smarty_tpl->tpl_vars['col_key']->value), null, 0);?>
						       <!--ts--> <?php if ($_smarty_tpl->tpl_vars['timeSlot']->value==null) {?><!--null--><?php }?>
						     <?php } else { ?>
						     	<?php $_smarty_tpl->tpl_vars['timeSlot'] = new Smarty_variable(null, null, 0);?>
						     	<!--null--> <?php if (isset($_smarty_tpl->tpl_vars['timeListStr']->value)) {?><?php }?>
						     <?php }?>
					   <?php } else { ?>
					   <?php }?>
					   <!--linetype:<?php echo $_smarty_tpl->tpl_vars['linetype']->value;?>
-->
					   <?php if ($_smarty_tpl->tpl_vars['linetype']->value=="top"||$_smarty_tpl->tpl_vars['linetype']->value==''||$_smarty_tpl->tpl_vars['timeSlot']->value!=null) {?>
					   		<?php if ($_smarty_tpl->tpl_vars['linetype']->value!="top"&&$_smarty_tpl->tpl_vars['timeSlot']->value!=null) {?>
					   		<?php }?>
					   		<?php if ($_smarty_tpl->tpl_vars['isDaySel']->value||($_smarty_tpl->tpl_vars['linetype']->value==''&&$_smarty_tpl->tpl_vars['col_key']->value==1)||($_smarty_tpl->tpl_vars['linetype']->value=="top"&&$_smarty_tpl->tpl_vars['col_key']->value==1)||($_smarty_tpl->tpl_vars['linetype']->value!="top"&&$_smarty_tpl->tpl_vars['timeSlot']->value!=null&&$_smarty_tpl->tpl_vars['timeSlot']->value->getStartTime()==$_smarty_tpl->tpl_vars['timeListTime']->value)) {?>
				                <td col="<?php echo $_smarty_tpl->tpl_vars['col_key']->value;?>
"
				                	class="<?php echo $_smarty_tpl->tpl_vars['cell_class']->value;?>
"
				                	<?php if ($_smarty_tpl->tpl_vars['isNightSel']->value) {?>colspan="2"<?php }?>
				                	<?php if (($_smarty_tpl->tpl_vars['linetype']->value=="top"||$_smarty_tpl->tpl_vars['linetype']->value=='')&&$_smarty_tpl->tpl_vars['col_key']->value==1) {?>colspan="<?php echo count($_smarty_tpl->tpl_vars['dayParallel']->value[$_smarty_tpl->tpl_vars['cell_day']->value->getDayNumber()]);?>
"<?php }?>
				                	<?php if (!$_smarty_tpl->tpl_vars['isDaySel']->value&&$_smarty_tpl->tpl_vars['linetype']->value!="top"&&$_smarty_tpl->tpl_vars['linetype']->value!=''&&$_smarty_tpl->tpl_vars['cutTimes']->value) {?>rowspan="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['timeSlot']->value->getLength($_smarty_tpl->tpl_vars['timeLength']->value*60), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"<?php }?>>
					                	<?php if ($_smarty_tpl->tpl_vars['linetype']->value=="sel") {?>
					                		<?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['planning_cell']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('day'=>$_smarty_tpl->tpl_vars['cell_day']->value), 0);?>

					                	<?php } elseif ($_smarty_tpl->tpl_vars['linetype']->value=="top") {?>
					                		<?php if ($_smarty_tpl->tpl_vars['planningType']->value=="column"||$_smarty_tpl->tpl_vars['planningType']->value=="topcolumn"||$_smarty_tpl->tpl_vars['planningStyle']->value=="topproduct") {?>
					                    		<?php echo $_smarty_tpl->tpl_vars['calendar']->value->getShortDayString($_smarty_tpl->tpl_vars['cell_day']->value->date,$_smarty_tpl->tpl_vars['id_lang']->value);?>

					                    	<?php } elseif (isset($_smarty_tpl->tpl_vars['cell_content']->value)&&$_smarty_tpl->tpl_vars['cell_content']->value!='') {?>
					                    		<?php $_tmp2=$_smarty_tpl->tpl_vars['cell_content']->value;?><?php echo $_smarty_tpl->tpl_vars['calendar']->value->$_tmp2($_smarty_tpl->tpl_vars['cell_day']->value->date,$_smarty_tpl->tpl_vars['id_lang']->value);?>

					                    	<?php } else { ?>
					                    		<?php echo $_smarty_tpl->tpl_vars['calendar']->value->getDayString($_smarty_tpl->tpl_vars['cell_day']->value->date,$_smarty_tpl->tpl_vars['id_lang']->value);?>

					                    	<?php }?>
					                	<?php } else { ?>
					                		<?php $_tmp3=$_smarty_tpl->tpl_vars['cell_content']->value;?><?php echo $_smarty_tpl->tpl_vars['calendar']->value->$_tmp3($_smarty_tpl->tpl_vars['cell_day']->value->date,$_smarty_tpl->tpl_vars['id_lang']->value);?>

					                	<?php }?>
			                	</td>
							<?php }?>
						<?php } else { ?>	
													<td col="<?php echo $_smarty_tpl->tpl_vars['col_key']->value;?>
" class="<?php echo $_smarty_tpl->tpl_vars['cell_class']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['isNightSel']->value) {?>colspan="2"<?php }?>></td>
						<?php }?>

<?php } ?>
	                	
					<?php } else { ?>
						<?php if ($_smarty_tpl->tpl_vars['cell_content']->value!="ggetDayOfMonth") {?><td class="myOwnCalendarLineItem" <?php if ($_smarty_tpl->tpl_vars['isNightSel']->value) {?>colspan="2"<?php }?>></td><?php }?>
					<?php }?>
	            <?php } ?>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['isNightSel']->value&&(($_smarty_tpl->tpl_vars['linetype']->value=="top"&&$_smarty_tpl->tpl_vars['potentialResa']->value->_selType!="end")||($_smarty_tpl->tpl_vars['linetype']->value=="sel"&&$_smarty_tpl->tpl_vars['potentialResa']->value->_selType=="end"))) {?>
            		<td<?php if ($_smarty_tpl->tpl_vars['linetype']->value=="sel") {?> class="myOwnCalendarLineItem"<?php }?>></td>
            <?php }?>
<?php }} ?>
