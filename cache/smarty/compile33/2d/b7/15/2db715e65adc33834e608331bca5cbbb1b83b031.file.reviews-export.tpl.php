<?php /* Smarty version Smarty-3.1.19, created on 2019-01-11 15:32:58
         compiled from "C:\MAMP\htdocs\fuoriporta\modules\gsnippetsreviews\views\templates\admin\review-tool\reviews-export.tpl" */ ?>
<?php /*%%SmartyHeaderCode:57085c38a91a02eb75-06386133%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2db715e65adc33834e608331bca5cbbb1b83b031' => 
    array (
      0 => 'C:\\MAMP\\htdocs\\fuoriporta\\modules\\gsnippetsreviews\\views\\templates\\admin\\review-tool\\reviews-export.tpl',
      1 => 1547116567,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '57085c38a91a02eb75-06386133',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'sURI' => 0,
    'sCtrlParamName' => 0,
    'sController' => 0,
    'aQueryParams' => 0,
    'sDisplay' => 0,
    'bUpdate' => 0,
    'sConfirmInclude' => 0,
    'aErrors' => 0,
    'sErrorInclude' => 0,
    'iTotalReviews' => 0,
    'aLangs' => 0,
    'aLang' => 0,
    'sLoader' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5c38a91a065d49_86710235',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5c38a91a065d49_86710235')) {function content_5c38a91a065d49_86710235($_smarty_tpl) {?>
<script type="text/javascript">
	
	var oExportCallBack =
	[{
		//'name' : 'updatePrerequisites',
		//'url' : '<?php echo $_smarty_tpl->tpl_vars['sURI']->value;?>
',
		//'params' : '<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sCtrlParamName']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
=<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sController']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
&sAction=display&sType=prerequisites',
		//'toShow' : 'bt_prerequisites-settings',
		//'toHide' : 'bt_prerequisites-settings',
		//'bFancybox' : false,
		//'bFancyboxActivity' : false,
		//'sLoadbar' : null,
		//'sScrollTo' : null,
		//'oCallBack' : {}
	}];
	
</script>

<div class="bootstrap">
	<form class="form-horizontal col-xs-12 col-md-12 col-lg-12" action="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sURI']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" method="post" id="bt_export-form" name="bt_export-form">
		<input type="hidden" name="sAction" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['aQueryParams']->value['export']['action'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
		<input type="hidden" name="sType" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['aQueryParams']->value['export']['type'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
		<input type="hidden" name="sDisplay" id="sExportDisplay" value="<?php if (!empty($_smarty_tpl->tpl_vars['sDisplay']->value)) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sDisplay']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php } else { ?>export<?php }?>" />

		<?php if (empty($_smarty_tpl->tpl_vars['sDisplay']->value)||(!empty($_smarty_tpl->tpl_vars['sDisplay']->value)&&$_smarty_tpl->tpl_vars['sDisplay']->value=='export')) {?>
			<h3><i class="icon icon-upload"></i>&nbsp;<?php echo smartyTranslate(array('s'=>'Export your reviews','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
</h3>
			<div class="clr_10"></div>
			<?php if (!empty($_smarty_tpl->tpl_vars['bUpdate']->value)) {?>
				<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['sConfirmInclude']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

				<div class="clr_10"></div>
			<?php } elseif (!empty($_smarty_tpl->tpl_vars['aErrors']->value)) {?>
				<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['sErrorInclude']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('aErrors'=>$_smarty_tpl->tpl_vars['aErrors']->value), 0);?>

				<div class="clr_10"></div>
			<?php }?>

			<div class="clr_10"></div>

			<div class="form-group">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="alert alert-info">
						<?php echo smartyTranslate(array('s'=>'Technically speaking, you can export the entire list of your ratings / reviews, or decide to export them by language. A CSV file will be generated and download automatically when you click on the export button.','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>

					</div>
				</div>
			</div>

			<div class="clr_10"></div>

			<div class="form-group">
				<label class="control-label col-xs-12 col-sm-12 col-md-2 col-lg-2">
					<b><?php echo smartyTranslate(array('s'=>'Export your reviews','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
</b> :
				</label>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<table class="table table-striped col-xs-12 bt-table-info">
						<tr>
							<td class="center"><i class="icon icon-download-alt"></i>&nbsp;<b><?php echo smartyTranslate(array('s'=>'Total reviews to export','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
</b></td>
							<td class="center"><i class="icon icon-filter"></i>&nbsp;<b><?php echo smartyTranslate(array('s'=>'Language','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
</b></td>
							<td class="center"><i class="icon icon-file"></i>&nbsp;<b><?php echo smartyTranslate(array('s'=>'UTF-8','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
</b></td>
							<td class="center"><b><?php echo smartyTranslate(array('s'=>'Action','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
</b></td>
						</tr>
						<tr>
							<td class="center">
								<div class="filter">
									<?php if (!empty($_smarty_tpl->tpl_vars['iTotalReviews']->value)) {?><?php echo intval($_smarty_tpl->tpl_vars['iTotalReviews']->value);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'There aren\'t any reviews','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
<?php }?>
								</div>
							</td>
							<td class="center">
								<div class="filter">
									<select name="bt_export-review-lang" id="bt_export-review-lang">
										<option value="0"> -- <?php echo smartyTranslate(array('s'=>'all','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
 --</option>
										<?php  $_smarty_tpl->tpl_vars['aLang'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['aLang']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aLangs']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['aLang']->key => $_smarty_tpl->tpl_vars['aLang']->value) {
$_smarty_tpl->tpl_vars['aLang']->_loop = true;
?>
											<option value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['aLang']->value['id_lang'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['aLang']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
										<?php } ?>
									</select>
								</div>
							</td>
							<td class="center">
								<div class="filter">
									<input type="checkbox" name="bt_export-utf8" id="bt_export-utf8" checked="checked" />
								</div>
							</td>
							<td class="center">
								<input type="button" name="bt_basics-button" value="<?php echo smartyTranslate(array('s'=>'export','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
" class="btn btn-success btn-mini" onclick="oGsr.form('bt_export-form', '<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sURI']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
', null, 'bt_export-result', 'bt_export-result', false, false, null, 'export', 'export');return false;" />
							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="clr_10"></div>

			<div class="form-group">
				<label class="control-label col-xs-12 col-sm-12 col-md-2 col-lg-2"><b><?php echo smartyTranslate(array('s'=>'Your reviews list','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
</b> :</label>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 center">
					<div class="alert alert-info"><?php echo smartyTranslate(array('s'=>'A list of your reviews will be displayed below in order to check in the list what you just have exported!','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
</div>
				</div>
			</div>

			<div id="bt_loading-div-export" style="display: none;">
				<div class="alert alert-info">
					<p style="text-align: center !important;"><img src="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sLoader']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" alt="Loading" /></p><div class="clr_20"></div>
					<p style="text-align: center !important;"><?php echo smartyTranslate(array('s'=>'Your reviews export is in progress','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
</p>
				</div>
			</div>

			<div class="clr_10"></div>

			<div class="form-group" id="bt_export-result">
			</div>


			<div class="clr_10"></div>
		<?php }?>
		<div class="clr_20"></div>
		<div class="clr_hr"></div>
		<div class="clr_20"></div>

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-11 col-lg-11">
				<div id="bt_error-export"></div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
		</div>
	</form>

	<div class="clr_20"></div>
</div>


<script type="text/javascript">
	//bootstrap components init
	$(document).ready(function() {
		$('.label-tooltip, .help-tooltip').tooltip();
	});
</script>
<?php }} ?>
