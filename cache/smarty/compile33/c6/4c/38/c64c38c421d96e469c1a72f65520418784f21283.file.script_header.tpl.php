<?php /* Smarty version Smarty-3.1.19, created on 2019-01-11 12:33:34
         compiled from "C:\MAMP\htdocs\fuoriporta\modules\myownreservations\views\templates\hook\script_header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:245925c387f0eed4e41-36706231%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c64c38c421d96e469c1a72f65520418784f21283' => 
    array (
      0 => 'C:\\MAMP\\htdocs\\fuoriporta\\modules\\myownreservations\\views\\templates\\hook\\script_header.tpl',
      1 => 1545145951,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '245925c387f0eed4e41-36706231',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'myOwnPriceDisplay' => 0,
    'cartLink' => 0,
    'orderLink' => 0,
    'defaultCarrier' => 0,
    'nextStep' => 0,
    'previousStep' => 0,
    '_PS_VERSION_' => 0,
    'addToCartOnList' => 0,
    'isResaSession' => 0,
    'potentialResa' => 0,
    'myOwnPriceType' => 0,
    'myownpage' => 0,
    'isCatOfSession' => 0,
    'stdCheckout' => 0,
    'hideStep1' => 0,
    'hideStep2' => 0,
    'currentStep' => 0,
    'islogged' => 0,
    'cust_id_address_delivery' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5c387f0f024823_74269113',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5c387f0f024823_74269113')) {function content_5c387f0f024823_74269113($_smarty_tpl) {?>
<?php if (!$_smarty_tpl->tpl_vars['myOwnPriceDisplay']->value) {?>
<style type="text/css">
span.price,
span.ajax_block_products_total,
span.ajax_cart_shipping_cost,
span.ajax_block_cart_total,
#our_price_display,
div.cart-prices,
span.product-price,
tr.cart_total_price {
	display:none !important;
}
</style>
<?php }?>
<script type="text/javascript">
var myOwnCartLink='<?php echo $_smarty_tpl->tpl_vars['cartLink']->value;?>
';
var myOwnPriceFrom='<?php echo smartyTranslate(array('s'=>'From','mod'=>'myownreservations'),$_smarty_tpl);?>
';

	//checkout steps mgmt
	//--------------------------------------------------------------------------------------
	function hideOpcStep1() {
	    $("#opc_account").remove();
		$("h2").each(function(index) {
			if (isLogged && $(this).html().indexOf("1") != -1) $(this).hide();
		});
	}
	function hideOpcStep2() {
		$("#carrierTable").hide();
		
		$(".carrier_title").remove();
		$(".delivery_options_address").remove();
		var html = $("#opc_delivery_methods").html();
		$("#carrier_area").after('<div><h2><span>2</span>&nbsp;<?php echo smartyTranslate(array('s'=>'Order Informations','mod'=>'myownreservations'),$_smarty_tpl);?>
</h2><div id="opc_informations" class="opc-main-block">'+html+'</div></div>');
		$("#carrier_area").remove();
		$('#cgv').on('click', function(e){
			updatePaymentMethodsDisplay();
		});
				
		$("h2").each(function(index) {
			if ($(this).html().indexOf("2.") != -1) {
				$(this).html("<?php echo smartyTranslate(array('s'=>'2. ORDER INFORMATIONS','mod'=>'myownreservations'),$_smarty_tpl);?>
");
				updateCarrierSelectionAndGift();
			}
		});
		$(".carrier_title").remove();
	}
	function prepareHideOpcStep1() {
		if ($("#button_order_cart")!=null) {
    		var htmlStr = $("#button_order_cart").parent().html();
    		
	        htmlStr += '<form name="myOwnReservationSkipStepA" action="<?php echo $_smarty_tpl->tpl_vars['orderLink']->value;?>
" method="post"><input type="hidden" name="cgv" value="1" ><input type="hidden" name="id_carrier" value="<?php echo $_smarty_tpl->tpl_vars['defaultCarrier']->value;?>
"><input type="hidden" name="processCarrier" value="1"></form>';
	        
	        $("#button_order_cart").parent().html( htmlStr );
			$("#button_order_cart").attr("href", "javascript:document.forms.myOwnReservationSkipStepA.submit();");
	    }
	}
	function hideStdStep1() {
		if ($('input[name="processAddress"]')!=null) {
    		var cont = $('input[name="processAddress"]').parent();
    		cont.children("input").remove();
    		var htmlStr = cont.html();
    		
    		htmlStr += '</form><form name="myOwnReservationSkipStepB" action="<?php echo $_smarty_tpl->tpl_vars['orderLink']->value;?>
&step=3" method="post"><input type="hidden" name="step" value="3" ><input type="hidden" name="cgv" value="1" ><input type="hidden" name="id_carrier" value="<?php echo $_smarty_tpl->tpl_vars['defaultCarrier']->value;?>
"><input type="hidden" name="processCarrier" value="1"><a href="javascript:document.forms.myOwnReservationSkipStepB.submit();" class="exclusive"><?php echo smartyTranslate(array('s'=>'Next','mod'=>'myownreservations'),$_smarty_tpl);?>
 »</a>';
    		
    		cont.html(htmlStr);
    	}
	}
	function hideStdStep() {
		if ($(".cart_navigation")!=null) {
    		var htmlStr = '';//$(".cart_navigation").html();
    		
	        htmlStr += '<form name="myOwnReservationSkipStepB" action="<?php echo $_smarty_tpl->tpl_vars['orderLink']->value;?>
&step=<?php echo $_smarty_tpl->tpl_vars['nextStep']->value;?>
" method="post"><input type="hidden" name="step" value="<?php echo $_smarty_tpl->tpl_vars['nextStep']->value;?>
" ><input type="hidden" name="cgv" value="1" ><input type="hidden" name="id_carrier" value="<?php echo $_smarty_tpl->tpl_vars['defaultCarrier']->value;?>
"><input type="hidden" name="processCarrier" value="1"></form>';
	        
	        $(".cart_navigation").after( htmlStr );
	        if ($(".standard-checkout").length ) $(".standard-checkout").attr("href","javascript:document.forms.myOwnReservationSkipStepB.submit();");
	        else $(".cart_navigation a").eq(1).attr("href","javascript:document.forms.myOwnReservationSkipStepB.submit();");
	    }
	}
	function changePreviousStep() {
		if ($(".cart_navigation")!=null) {
    		var htmlStr = '';//$(".cart_navigation").html();
    		
	        htmlStr += '<form name="myOwnReservationSkipPrevious" action="<?php echo $_smarty_tpl->tpl_vars['orderLink']->value;?>
&step=<?php echo $_smarty_tpl->tpl_vars['previousStep']->value;?>
" method="post"><input type="hidden" name="step" value="<?php echo $_smarty_tpl->tpl_vars['previousStep']->value;?>
" ></form>';
	        
	        $(".cart_navigation").after( htmlStr );
	        $(".cart_navigation a").eq(0).attr("href","javascript:document.forms.myOwnReservationSkipPrevious.submit();");
	    }
	}
	function loginStep() {
		
		$('#login_form input[name="back"]').remove();
		$("#login_form").append('<input type="hidden" name="back" value="<?php echo $_smarty_tpl->tpl_vars['orderLink']->value;?>
&step=3&cgv=1&id_carrier=<?php echo $_smarty_tpl->tpl_vars['defaultCarrier']->value;?>
&processCarrier=1">');
		
	}
	function addressCreationStep(step) {
		
		$('#add_address input[name="back"]').val("order.php?step="+step);
		
	}
	
	function setOrderSkipDeliveryBtn() {
		var id_address=$('input[name="id_address_delivery"]').val();
		var id_carrier=<?php echo $_smarty_tpl->tpl_vars['defaultCarrier']->value;?>
;
		alert(id_address+' '+id_carrier);
		var btn_label = $('button[name="confirm-addresses"]').html();
		$('button[name="confirm-addresses"]').remove();
		var form = '<form class="clearfix" id="js-delivery" data-url-update="commande?ajax=1&action=selectDeliveryOption" method="post"><input type="hidden" name="id_address_delivery" value="'+id_address+'"><input type="hidden" name="delivery_option['+id_address+']" id="delivery_option_'+id_carrier+'" value="'+id_carrier+',"/><button type="submit" class="continue btn btn-primary pull-xs-right" name="confirmDeliveryOption" value="1">'+btn_label+'</button></form>';
		$('#checkout-addresses-step .content').append(form);
	}
	
	function orderSkipAddressStep () {
		$("#checkout-addresses-step").hide();
		$(".step-number").html('&nbsp;');
	}
	
	//price and button display
	//--------------------------------------------------------------------------------------
	function hideCartButtons() {
		$(".right_block > .exclusive").css("display","none");
		$("#product_list > li > div > .ui-block-b").css("display","none");
	}
	function hidePriceDisplay() {
    	$(".right_block .price, .center_block .price").each(function(index) {
    		$(this).html("");
    	});
	}
	
	<?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value<"1.6") {?>
	
	function setSessionPrices() {
		$(".exclusive").each(function(index) {
			if (typeof(productResaPrice)!='undefined') {
				for (id_product in productResaPrice) {
					if ($(this).attr("rel")=="ajax_id_product_"+id_product) {
						if (productResaPrice[id_product]=='-1') {
							$(this).parent().parent().contents().find(".availability").html("<?php echo smartyTranslate(array('s'=>'Period not valid for this product','mod'=>'myownreservations'),$_smarty_tpl);?>
");
							$(this).after('<span class="exclusive"><?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'myownreservations'),$_smarty_tpl);?>
</span>');
						} else if (productResaPrice[id_product]=='') {
							$(this).parent().parent().contents().find(".availability").html("<?php echo smartyTranslate(array('s'=>'Not available for selected period','mod'=>'myownreservations'),$_smarty_tpl);?>
");
							$(this).after('<span class="exclusive"><?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'myownreservations'),$_smarty_tpl);?>
</span>');
						} else {
							
							$(this).parent().parent().contents().find(".availability").html("<?php echo smartyTranslate(array('s'=>'Available for selected period','mod'=>'myownreservations'),$_smarty_tpl);?>
");
							<?php if ($_smarty_tpl->tpl_vars['addToCartOnList']->value&&$_smarty_tpl->tpl_vars['isResaSession']->value) {?>
							$(this).after('<a class="exclusive ajax_add_resa_to_cart_button" onclick="myownreservationCallCart('+id_product+', '+productDefaultAttr[id_product]+', 1, \'<?php echo $_smarty_tpl->tpl_vars['potentialResa']->value->startDate;?>
_<?php echo $_smarty_tpl->tpl_vars['potentialResa']->value->startTimeslot;?>
@<?php echo $_smarty_tpl->tpl_vars['potentialResa']->value->endDate;?>
_<?php echo $_smarty_tpl->tpl_vars['potentialResa']->value->endTimeslot;?>
\', this);"><?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'myownreservations'),$_smarty_tpl);?>
</a>');
							<?php }?>
							
						}
						
						<?php if ($_smarty_tpl->tpl_vars['myOwnPriceDisplay']->value) {?>
							if (productResaPrice[id_product]!='-1') $(this).parent().contents().find(".price").html(productResaPrice[id_product]);
						<?php } else { ?>
							$(this).parent().contents().find(".price").remove();
						<?php }?>
						$(this).remove();
						
					}
				}
			}
		});
	}
	
	<?php }?>
	
	function myOwnReservationDislay() {
	
		<?php if ($_smarty_tpl->tpl_vars['myOwnPriceType']->value==0) {?>
			setGlobalUnits<?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.6") {?>16<?php }?>();
		<?php } else { ?>
			setMinPrices<?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.6") {?>16<?php }?>();
		<?php }?>
		
	<?php if ($_smarty_tpl->tpl_vars['isResaSession']->value&&($_smarty_tpl->tpl_vars['myownpage']->value!='category'||$_smarty_tpl->tpl_vars['isCatOfSession']->value||$_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.6")&&$_smarty_tpl->tpl_vars['myownpage']->value!='product') {?>
		<?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value<"1.6") {?>
		setSessionPrices();
		<?php } else { ?>
		setSessionPrices16();
		<?php }?>
	<?php }?>
	<?php if (!$_smarty_tpl->tpl_vars['myOwnPriceDisplay']->value) {?>
		hidePriceDisplay();
	<?php }?>
		
	}
	
	//--------------------------------------------------------------------------------------
	document.addEventListener("DOMContentLoaded", function(event) {
		
		//alert('<?php echo $_smarty_tpl->tpl_vars['myownpage']->value;?>
');
		<?php if ($_smarty_tpl->tpl_vars['myownpage']->value=='order-opc'&&!$_smarty_tpl->tpl_vars['stdCheckout']->value) {?>
			<?php if ($_smarty_tpl->tpl_vars['hideStep1']->value) {?>hideOpcStep1();<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['hideStep2']->value) {?>hideOpcStep2();<?php }?>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['myownpage']->value=='address'&&$_smarty_tpl->tpl_vars['hideStep2']->value) {?>
			addressCreationStep(3);
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['myownpage']->value=='order'&&$_smarty_tpl->tpl_vars['stdCheckout']->value) {?>
			<?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value<"1.7") {?>
				<?php if ($_smarty_tpl->tpl_vars['hideStep1']->value) {?>
					$("#order_step li").eq(2).css("display","none");
					prepareHideOpcStep1();
					<?php if ($_smarty_tpl->tpl_vars['currentStep']->value==1) {?>hideStdStep1();<?php }?>
					/*<?php if ($_smarty_tpl->tpl_vars['currentStep']->value==0) {?>$(".order_delivery").hide();<?php }?>*/
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['hideStep2']->value) {?>
					$("#order_step li").eq(3).css("display","none");
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['hideStep1']->value||$_smarty_tpl->tpl_vars['hideStep2']->value) {?>
					<?php if ($_smarty_tpl->tpl_vars['currentStep']->value>0) {?>changePreviousStep();<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['currentStep']->value==0&&$_smarty_tpl->tpl_vars['islogged']->value) {?>hideStdStep();<?php }?>
				<?php }?>
			<?php } else { ?>
				//setOrderSkipDeliveryBtn();
				<?php if ($_smarty_tpl->tpl_vars['hideStep1']->value&&$_smarty_tpl->tpl_vars['cust_id_address_delivery']->value) {?>orderSkipAddressStep();<?php }?>
			<?php }?>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['hideStep1']->value&&$_smarty_tpl->tpl_vars['myownpage']->value=='authentication'&&$_smarty_tpl->tpl_vars['stdCheckout']->value) {?>
		loginStep();
		<?php }?>
		myOwnReservationDislay();
		
	});
	document.addEventListener("DOMContentLoaded", function(event) {
		$(document).ajaxComplete(function() {
			myOwnReservationDislay();
		});
	});

</script>
<?php }} ?>
