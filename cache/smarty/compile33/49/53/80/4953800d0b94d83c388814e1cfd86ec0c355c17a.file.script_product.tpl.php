<?php /* Smarty version Smarty-3.1.19, created on 2019-01-11 12:36:39
         compiled from "C:\MAMP\htdocs\fuoriporta\modules\myownreservations\views\templates\hook\script_product.tpl" */ ?>
<?php /*%%SmartyHeaderCode:59545c387fc76a9eb0-27868197%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4953800d0b94d83c388814e1cfd86ec0c355c17a' => 
    array (
      0 => 'C:\\MAMP\\htdocs\\fuoriporta\\modules\\myownreservations\\views\\templates\\hook\\script_product.tpl',
      1 => 1545145951,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '59545c387fc76a9eb0-27868197',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'myPlanningBox' => 0,
    'module_dir' => 0,
    'myOwnResAlsoSell' => 0,
    'myOwnPriceDisplay' => 0,
    'myHomeSelection' => 0,
    'myHomePage' => 0,
    'imgDir' => 0,
    'defaultcombinaison' => 0,
    '_PS_VERSION_' => 0,
    'myOwnResWay' => 0,
    'myOwnResLblStart' => 0,
    'myOwnResLblEnd' => 0,
    'myOwnResIgnoreProdQty' => 0,
    'productLink' => 0,
    'myOwnPriceType' => 0,
    'myOwnResPriceUnit' => 0,
    'isResaSession' => 0,
    'id_product' => 0,
    'potentialResa' => 0,
    'isquickview' => 0,
    'resaSessionString' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5c387fc7705a95_34680343',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5c387fc7705a95_34680343')) {function content_5c387fc7705a95_34680343($_smarty_tpl) {?>
<?php if ($_smarty_tpl->tpl_vars['myPlanningBox']->value==1) {?>
<link href="<?php echo $_smarty_tpl->tpl_vars['module_dir']->value;?>
views/css/planning.css" rel="stylesheet" type="text/css" media="all" />
<?php }?>
<script type="text/javascript">
var myOwnResAlsoSell=<?php echo $_smarty_tpl->tpl_vars['myOwnResAlsoSell']->value;?>
;
var myOwnPriceDisplay=<?php echo $_smarty_tpl->tpl_vars['myOwnPriceDisplay']->value;?>
;
var myOwnPriceFrom='<?php echo smartyTranslate(array('s'=>'From','mod'=>'myownreservations'),$_smarty_tpl);?>
';
var myHomeSelection='<?php echo $_smarty_tpl->tpl_vars['myHomeSelection']->value;?>
';
var myHomePage='<?php echo $_smarty_tpl->tpl_vars['myHomePage']->value;?>
';
var imgDir='<?php echo $_smarty_tpl->tpl_vars['imgDir']->value;?>
';
var quantity = 1;
<?php if ($_smarty_tpl->tpl_vars['myPlanningBox']->value==1) {?>
var myPlanningBox = true;
<?php } else { ?>
var myPlanningBox = false;
<?php }?>

function myownreservationAttrChange() {
	myownreservationActions();
	//set -1 to last arg to indicate a first load
	myOwnGetSelector('product').showView(-1, 'Product');
	//myProductSelector.showView(-1);
}

function initLocationChange(time)
{
	if(!time) time = 500;
	setInterval(myOwnCheckUrl, time);
}
function myOwnCheckUrl() {
	var my_original_url=original_url;
	checkUrl();
	if (my_original_url != window.location) {
		myownreservationActions();
		my_original_url=window.location;
	}
}
function myOwnGetSelector(target) {
	var temp = window["my"+target+"Selector"];
	if (typeof(temp)!='undefined') return temp;
	else return myOwnSelector;
}

function myownreservationActions () {

		var idCombination = <?php echo $_smarty_tpl->tpl_vars['defaultcombinaison']->value;?>

		<?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value<"1.7") {?>
			idCombination = $('#idCombination').val();
		<?php } else { ?>
			if (history.state!=null) idCombination = history.state.id_product_attribute;
		<?php }?>
		var myowntab = "idTab1";
		<?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.7") {?>myowntab = $("#idTabResa").parent().attr('id');<?php }?>
		var adchtml = '';
		if (idCombination=='') idCombination=0;
		if (typeof productHasAttributes != "undefined" && productHasAttributes && selectedCombination['unavailable']) idCombination=-1;
		$("#add_to_cart").html('');
		var label = "<?php echo smartyTranslate(array('s'=>'Purchase','mod'=>'myownreservations'),$_smarty_tpl);?>
";
		<?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value<"1.6") {?>
		if (myOwnResAlsoSell) $("#add_to_cart").append( '<input type="submit" name="Submit" value="'+label+'" class="exclusive">');
		<?php } else { ?>
		if (myOwnResAlsoSell) $("#add_to_cart").append( '<button type="submit" name="Submit" class="exclusive"><span>'+label+'</span></button>');
		<?php }?>
		
		<?php if ($_smarty_tpl->tpl_vars['myOwnResWay']->value) {?>
		if ($("#myOwnResWay").length==0)
			$(".product_attributes").append('<fieldset class="attribute_fieldset" id="myOwnResWay"><div class="attribute_list"><ul><li><div class="radio"><span class="checked"><input type="radio" class="myownresway_radio" name="myownresway" value="start" checked="checked"></span></div><span><i class="icon-long-arrow-right"></i> <?php echo $_smarty_tpl->tpl_vars['myOwnResLblStart']->value;?>
</span></li><li><div class="radio"><span><input type="radio" class="myownresway_radio" name="myownresway" value="stop"></span></div><span><i class="icon-long-arrow-left"></i> <?php echo $_smarty_tpl->tpl_vars['myOwnResLblEnd']->value;?>
</span></li><li><div class="radio"><span><input type="radio" class="myownresway_radio" name="myownresway" value="both"></span></div><span><i class="icon-exchange"></i> <?php echo $_smarty_tpl->tpl_vars['myOwnResLblStart']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['myOwnResLblEnd']->value;?>
</span></li></ul></div></fieldset>');
			myOwnGetSelector('product').resWay='start';
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['myOwnResIgnoreProdQty']->value) {?>
		$("#quantity_wanted_p").empty();
		$("#quantityAvailable").hide();
		$("#quantityAvailableTxt").hide();
		$("#quantityAvailableTxtMultiple").hide();
		<?php }?>
		
		<?php if ($_smarty_tpl->tpl_vars['myPlanningBox']->value==0) {?>
		if ($("#idTabResa").length) {
			$("#add_to_cart, .product-add-to-cart .add-to-cart").append('<a href="javascript:showTab(\'idTabResa\');goToResaTabScroll(\'<?php echo $_smarty_tpl->tpl_vars['productLink']->value;?>
\');" class="button"><?php echo smartyTranslate(array('s'=>'Select a period','mod'=>'myownreservations'),$_smarty_tpl);?>
</a>');
			$(".product-add-to-cart .add-to-cart").attr("data-button-action", "").attr("onclick", "javascript:showTab('"+myowntab+"');goToResaTabScroll('<?php echo $_smarty_tpl->tpl_vars['productLink']->value;?>
');return false;").html('<i class="material-icons grid_on">event</i><?php echo smartyTranslate(array('s'=>'Select a period','mod'=>'myownreservations'),$_smarty_tpl);?>
');
		}
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['myPlanningBox']->value==1) {?>
			<?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value<"1.7") {?>
			$("#add_to_cart, .product-add-to-cart .add-to-cart").append('<a onclick="showPlanningBox();myOwnGetSelector(\'product\').showView(-1, \'Product\', -1);" class="button"><?php echo smartyTranslate(array('s'=>'Select a period','mod'=>'myownreservations'),$_smarty_tpl);?>
</a>');
			<?php } else { ?>
			$(".product-add-to-cart .add-to-cart").attr("data-button-action", "").attr("onclick", "showPlanningBox();myOwnGetSelector('product').showView(-1, 'Product', -1);return false;").html('<i class="material-icons grid_on">event</i><?php echo smartyTranslate(array('s'=>'Select a period','mod'=>'myownreservations'),$_smarty_tpl);?>
');
			<?php }?>
		<?php }?>
		var priceItem = $("#our_price_display, .current-price span");
		<?php if ($_smarty_tpl->tpl_vars['myOwnPriceType']->value==0) {?>
		if (priceItem.length && priceItem.html().indexOf("/<?php echo $_smarty_tpl->tpl_vars['myOwnResPriceUnit']->value;?>
") == -1) {
			if (!myOwnResAlsoSell && "<?php echo $_smarty_tpl->tpl_vars['myOwnResPriceUnit']->value;?>
"!=" ") priceItem.append("/<?php echo $_smarty_tpl->tpl_vars['myOwnResPriceUnit']->value;?>
");
		}
		var oldPriceItem = $("#old_price_display");
		if (oldPriceItem.length && oldPriceItem.html().indexOf("/<?php echo $_smarty_tpl->tpl_vars['myOwnResPriceUnit']->value;?>
") == -1) {
			if (!myOwnResAlsoSell && "<?php echo $_smarty_tpl->tpl_vars['myOwnResPriceUnit']->value;?>
"!=" ") oldPriceItem.append("/<?php echo $_smarty_tpl->tpl_vars['myOwnResPriceUnit']->value;?>
");
		}
		<?php } else { ?>
		if (typeof(productAttrMinPrice[idCombination])!='undefined') {
			if (priceItem.find("span.myownresPrice").length==0)
				priceItem.html('<span class="myownresPrice" style="font-size:10px;">'+myOwnPriceFrom+' </span>'+productAttrMinPrice[idCombination]);
		}
		<?php }?>
<?php if ($_smarty_tpl->tpl_vars['isResaSession']->value) {?>

		//actions if resas dates in session 
		if (productAttrAvailable[idCombination]==0) {
			$("#pQuantityAvailable").css("display","none");
			$("#availability_value, #product-availability").html('<?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.7") {?><i class="material-icons product-unavailable">&#xE14B;</i><?php }?><?php echo smartyTranslate(array('s'=>'Not available','mod'=>'myownreservations'),$_smarty_tpl);?>
 '+productAttrLabel[idCombination]);
			$("#availability_value").removeClass("label-success");
			$("#availability_value").addClass("label-danger");
			adchtml = '<i class="material-icons grid_on">event</i><?php echo smartyTranslate(array('s'=>'Change period','mod'=>'myownreservations'),$_smarty_tpl);?>
';
		} else {
			
			var label='';
			if (myOwnResAlsoSell) label="<?php echo smartyTranslate(array('s'=>'Reserve product','mod'=>'myownreservations'),$_smarty_tpl);?>
";
			else label='<?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'myownreservations'),$_smarty_tpl);?>
';
			<?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.7") {?>
			$(".product-add-to-cart .add-to-cart").attr("data-button-action", "").attr("onclick", "myownreservationCallCart(<?php echo $_smarty_tpl->tpl_vars['id_product']->value;?>
, $('#idCombination').val(), $('#quantity_wanted').val(), '<?php echo $_smarty_tpl->tpl_vars['potentialResa']->value->startDate;?>
_<?php echo $_smarty_tpl->tpl_vars['potentialResa']->value->startTimeslot;?>
@<?php echo $_smarty_tpl->tpl_vars['potentialResa']->value->endDate;?>
_<?php echo $_smarty_tpl->tpl_vars['potentialResa']->value->endTimeslot;?>
', this);return false;");//.append('<i class="material-icons grid_on">event</i><?php echo smartyTranslate(array('s'=>'Select a period','mod'=>'myownreservations'),$_smarty_tpl);?>
');
			adchtml = '';//<a id="reserve" class="exclusive button ajax_add_resa_to_cart_button" onclick="">'+label+'</a>';
			<?php } elseif ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value<"1.6") {?>
			adchtml += '<a id="reserve" class="exclusive button ajax_add_resa_to_cart_button" onclick="myownreservationCallCart(<?php echo $_smarty_tpl->tpl_vars['id_product']->value;?>
, $(\'#idCombination\').val(), $(\'#quantity_wanted\').val(), \'<?php echo $_smarty_tpl->tpl_vars['potentialResa']->value->startDate;?>
_<?php echo $_smarty_tpl->tpl_vars['potentialResa']->value->startTimeslot;?>
@<?php echo $_smarty_tpl->tpl_vars['potentialResa']->value->endDate;?>
_<?php echo $_smarty_tpl->tpl_vars['potentialResa']->value->endTimeslot;?>
\', this);\">'+label+'</a>';
			<?php } else { ?>
			adchtml += '<button id="reserve" class="exclusive ajax_add_resa_to_cart_button" onclick="myownreservationCallCart(<?php echo $_smarty_tpl->tpl_vars['id_product']->value;?>
, $(\'#idCombination\').val(), $(\'#quantity_wanted\').val(), \'<?php echo $_smarty_tpl->tpl_vars['potentialResa']->value->startDate;?>
_<?php echo $_smarty_tpl->tpl_vars['potentialResa']->value->startTimeslot;?>
@<?php echo $_smarty_tpl->tpl_vars['potentialResa']->value->endDate;?>
_<?php echo $_smarty_tpl->tpl_vars['potentialResa']->value->endTimeslot;?>
\', this);event.stopPropagation(); event.preventDefault();\"><span>'+label+'</span></button>';
			<?php }?>
			$("#pQuantityAvailable").css("display","block");
			$("#availability_value, #product-availability").html('<?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.7") {?><i class="material-icons product-available">&#xE5CA;</i><?php }?><?php echo smartyTranslate(array('s'=>'Available','mod'=>'myownreservations'),$_smarty_tpl);?>
'+productAttrLabel[idCombination]);
			$("#last_quantities").css("display","block");
			$(".myOwnResQuantityAvailable").html(productAttrQty[idCombination]);
			
		}

		var selLabel ="<?php echo smartyTranslate(array('s'=>'Purchase','mod'=>'myownreservations'),$_smarty_tpl);?>
";
		<?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value<"1.6") {?>
		if (myOwnResAlsoSell) adchtml += '<input type="submit" name="Submit" value="'+selLabel+'" class="exclusive">';
		<?php } else { ?>
		if (myOwnResAlsoSell) adchtml += '<button type="submit" name="Submit" class="exclusive"><span>'+selLabel+'</span></button>';
		<?php }?>
		else {
			$("#our_price_display, .current-price span").html(productAttrPrice[idCombination]);
			$("#old_price_display, span.regular-price").html(productAttrOldPrice[idCombination]);
		}
		showTab(myowntab);
		if (adchtml!='') $("#add_to_cart, .product-add-to-cart .add-to-cart").html(adchtml);
		//if (adchtml=='') $(".product-add-to-cart .add-to-cart").remove();
		$("#availability_statut").css("display","block");
		$("#last_quantities").css("display","none");
<?php } else { ?>
		//actions if no resas dates in session 
		$("#pQuantityAvailable").css("display","none");
		$('.modal-body .product-add-to-cart').remove();
<?php }?>
		if (!myOwnPriceDisplay) $("p.price").remove();
		
		$(".accessories-block a.exclusive, .accessories-block a.ajax_add_to_cart_button").each(function() {
			var id_accessoire = $(this).attr("data-id-product");
			if (typeof(productLinkedAvailable)!='undefined' && typeof(productLinkedAvailable[id_accessoire])!='undefined') {
				if (productLinkedAvailable[id_accessoire]) {
					$(this).html("<span><?php echo smartyTranslate(array('s'=>'Reserve product','mod'=>'myownreservations'),$_smarty_tpl);?>
</span>");
					$(this).attr("onclick", productLinkedAction[id_accessoire]);
					$(this).attr("href", "");
				} else {
					$(this).remove();
				}
				$(this).parent().parent().parent().find("span.price").html(productLinkedPrice[id_accessoire]);
				
			} else {
				$(this).remove();
			}
		});

}
<?php if (!$_smarty_tpl->tpl_vars['isquickview']->value) {?>document.addEventListener("DOMContentLoaded", function(event) {<?php }?>
	$(window).trigger('planning'); 
	keepAddToCart();
	
	<?php if ($_smarty_tpl->tpl_vars['myOwnResIgnoreProdQty']->value) {?>
	
	quantityAvailable=999999;
	if (typeof(combinations)!='undefined')
		$.each(combinations, function(key, combination)
		{
			combination['quantity']=999999;
		});
	
	<?php }?>
	
	$('#buy_block').bind("keyup keypress", function(e) {
	  var code = e.keyCode || e.which; 
	  if (code  == 13) {               
	    e.preventDefault();
	    return false;
	  }
	});

<?php if ($_smarty_tpl->tpl_vars['isResaSession']->value) {?>
	$("#quantityAvailable").addClass("myOwnResQuantityAvailable");
	$("#pb-left-column .price, .our_price_display").before("<span class='myOwnSelPeriod'><?php echo $_smarty_tpl->tpl_vars['resaSessionString']->value;?>
</span>");
	/*$("#pQuantityAvailable").append("<span class='myOwnSelPeriod'><?php echo $_smarty_tpl->tpl_vars['resaSessionString']->value;?>
</span>");*/
	if ($("#idTabResa").length) {
	//
		<?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value<"1.6") {?>
		$("#add_to_cart").after('<p class="buttons_bottom_block"><a href="javascript:showTab(\'idTabResa\');goToResaTabScroll();" class="button"><?php echo smartyTranslate(array('s'=>'Change period','mod'=>'myownreservations'),$_smarty_tpl);?>
</a></p>');
		<?php } else { ?>
		$("#add_to_cart").after('<p class="buttons_bottom_block"><a href="javascript:showTab(\'idTabResa\');goToResaTabScroll();" class="button lnk_view btn btn-default"><span><?php echo smartyTranslate(array('s'=>'Change period','mod'=>'myownreservations'),$_smarty_tpl);?>
</span></a></p>');
		<?php }?>
	}
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value<"1.7") {?>

	//attributes change
	if (typeof(updateDisplay)=='undefined') {
		$("#attributes :input").change(function () {
			findCombination();
			myownreservationActions();
			myOwnGetSelector('product').showView(-1, 'Product', -1);
		});
	
		$("#attributes :input, .color_pick").click(function () {
			myownreservationActions();
			myOwnGetSelector('product').showView(-1, 'Product', -1);
		});
	}

<?php }?>

	//quantity change
	$("#quantity_wanted").keyup(function () {
		if (typeof(myOwnResQty)!='undefined' && $(this).val() != "" && myOwnResQty != $(this).val()) {
			myOwnSelList.length=0;
			myOwnResQty = $(this).val();
	      	myOwnGetSelector('product').showView(-1, 'Product', -1);
	  	}
	});
	
	$("#quantity_wanted").change(function () {
		if (typeof(myOwnResQty)!='undefined' && $(this).val() != "" && myOwnResQty != $(this).val()) {
			myOwnSelList.length=0;
			myOwnResQty = $(this).val();
	      	myOwnGetSelector('product').showView(-1, 'Product', -1);
	  	}
	});
	
	$("#myownr_object").change(function () {
		myownreservationActions();
		myOwnGetSelector('product').showView(-1, 'Product', -1);
	});


	<?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.7") {?>
		
	prestashop.on("updatedProduct", function() { 
	    myownreservationActions();
	    myOwnGetSelector('product').showView(-1, 'Product', -1);
	});
	prestashop.on("updatedCart", function() { 
	    myownreservationActions();
	    myOwnGetSelector('product').showView(-1, 'Product', -1);
	});
	<?php }?>

	
	myownreservationActions();
	
	$(".myownresway_radio").click(function () {
		var resWay = $("input[name=myownresway]:checked").val();
		myOwnGetSelector('product').resWay=resWay;
		myOwnGetSelector('product').showView(-1, 'Product', -1);
	});
	myOwnGetSelector('product').showView(-1, 'Product', -1);
<?php if (!$_smarty_tpl->tpl_vars['isquickview']->value) {?>});<?php }?>

</script>
<?php }} ?>
