<?php /* Smarty version Smarty-3.1.19, created on 2019-01-11 15:32:57
         compiled from "C:\MAMP\htdocs\fuoriporta\modules\gsnippetsreviews\views\templates\admin\review-tool\review-status.tpl" */ ?>
<?php /*%%SmartyHeaderCode:152625c38a919eac445-46554108%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7b5b0396e5c57d432f77eaf3aef8c5a361d9918f' => 
    array (
      0 => 'C:\\MAMP\\htdocs\\fuoriporta\\modules\\gsnippetsreviews\\views\\templates\\admin\\review-tool\\review-status.tpl',
      1 => 1547116567,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '152625c38a919eac445-46554108',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'bStatus' => 0,
    'bWarning' => 0,
    'bUpdate' => 0,
    'aErrors' => 0,
    'bOneStatusUpdate' => 0,
    'sErrorInclude' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5c38a919ebb4d7_01984628',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5c38a919ebb4d7_01984628')) {function content_5c38a919ebb4d7_01984628($_smarty_tpl) {?>
<?php if ($_smarty_tpl->tpl_vars['bStatus']->value==0) {?>
	<?php if (!empty($_smarty_tpl->tpl_vars['bWarning']->value)) {?>
		<i class="icon-warning text-warning" style="font-size:20px;" title="<?php echo smartyTranslate(array('s'=>'warning','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
"></i>
	<?php } else { ?>
		<i class="icon-remove text-danger" style="font-size:20px;" title="<?php echo smartyTranslate(array('s'=>'activate','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
"></i>
	<?php }?>
<?php } else { ?>
	<i class="icon-ok-sign text-success" style="font-size:20px;" title="<?php echo smartyTranslate(array('s'=>'deactivate','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
"></i>
<?php }?>
<?php if (empty($_smarty_tpl->tpl_vars['bUpdate']->value)&&!empty($_smarty_tpl->tpl_vars['aErrors']->value)&&!empty($_smarty_tpl->tpl_vars['bOneStatusUpdate']->value)) {?>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['sErrorInclude']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php }?><?php }} ?>
