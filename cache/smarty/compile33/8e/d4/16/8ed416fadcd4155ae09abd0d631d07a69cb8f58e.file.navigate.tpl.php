<?php /* Smarty version Smarty-3.1.19, created on 2019-01-11 12:39:45
         compiled from "C:\MAMP\htdocs\fuoriporta\modules\myownreservations\views\templates\front\planning\navigate.tpl" */ ?>
<?php /*%%SmartyHeaderCode:297805c388081769340-93020975%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8ed416fadcd4155ae09abd0d631d07a69cb8f58e' => 
    array (
      0 => 'C:\\MAMP\\htdocs\\fuoriporta\\modules\\myownreservations\\views\\templates\\front\\planning\\navigate.tpl',
      1 => 1545145951,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '297805c388081769340-93020975',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'potentialResa' => 0,
    'planningStyle' => 0,
    'sel' => 0,
    'planningSelector' => 0,
    'planningType' => 0,
    'selStartValue' => 0,
    'previous_label' => 0,
    '_PS_VERSION_' => 0,
    'ishomeselector' => 0,
    'selectCats' => 0,
    'resa_category' => 0,
    'selectCatId' => 0,
    'selectCatName' => 0,
    'selector' => 0,
    'selectorVal' => 0,
    'selectorLabel' => 0,
    'maxSelection' => 0,
    'next_label' => 0,
    'previous_sub_label' => 0,
    'subSelector' => 0,
    'subSelectorVal' => 0,
    'subSelectorLabel' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5c3880818320b2_25020007',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5c3880818320b2_25020007')) {function content_5c3880818320b2_25020007($_smarty_tpl) {?><?php if (!is_callable('smarty_function_math')) include 'C:\\MAMP\\htdocs\\fuoriporta\\tools\\smarty\\plugins\\function.math.php';
?><?php if ($_smarty_tpl->tpl_vars['potentialResa']->value->_selType!="end") {?>
<?php $_smarty_tpl->tpl_vars['selStartValue'] = new Smarty_variable('0', null, 0);?>
<?php } else { ?>
<?php $_smarty_tpl->_capture_stack[0][] = array('my_module_tempvar', null, null); ob_start(); ?><?php echo $_smarty_tpl->tpl_vars['potentialResa']->value->startDate;?>
_<?php echo $_smarty_tpl->tpl_vars['potentialResa']->value->startTimeslot;?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php $_smarty_tpl->tpl_vars['selStartValue'] = new Smarty_variable(Smarty::$_smarty_vars['capture']['my_module_tempvar'], null, 0);?>
<?php }?>
	
<table width="100%" id="myOwn<?php echo $_smarty_tpl->tpl_vars['planningStyle']->value;?>
CalendarHeader" class="myOwnCalendarHeader">
	  <tr class="myOwnNavigateTop">
	        <td align="left" width="30%"><?php if ($_smarty_tpl->tpl_vars['sel']->value>0) {?><a tabindex="5" style="font-weight:normal" onclick="my<?php echo $_smarty_tpl->tpl_vars['planningSelector']->value;?>
Selector.showView(<?php echo smarty_function_math(array('equation'=>"x-1",'x'=>$_smarty_tpl->tpl_vars['sel']->value),$_smarty_tpl);?>
, '<?php echo $_smarty_tpl->tpl_vars['planningType']->value;?>
', '<?php echo $_smarty_tpl->tpl_vars['selStartValue']->value;?>
');" title="><?php echo $_smarty_tpl->tpl_vars['previous_label']->value;?>
" title="&laquo; <?php echo $_smarty_tpl->tpl_vars['previous_label']->value;?>
" class="<?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.6") {?>btn btn-default button button-small<?php } else { ?>button<?php }?>"><span>&laquo; <?php echo $_smarty_tpl->tpl_vars['previous_label']->value;?>
</span></a><?php }?></td>
	        <td style="text-align:center" align="center">
	        <?php if ($_smarty_tpl->tpl_vars['planningType']->value=="home"&&!$_smarty_tpl->tpl_vars['ishomeselector']->value) {?>
	    		<h3><?php echo $_smarty_tpl->tpl_vars['potentialResa']->value->_mainProduct->name;?>
</h3><br/>
	    	<?php }?>
	        <?php if ($_smarty_tpl->tpl_vars['planningType']->value=="home"&&$_smarty_tpl->tpl_vars['ishomeselector']->value&&count($_smarty_tpl->tpl_vars['selectCats']->value)>0) {?>
				<?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.6"&&$_smarty_tpl->tpl_vars['_PS_VERSION_']->value<"1.7") {?>
		    		<div class="selector" id="" style="display:inline-block;text-align:center">
		    			<span style="width: 190px; -webkit-user-select: none;"><?php echo $_smarty_tpl->tpl_vars['selectCats']->value[$_smarty_tpl->tpl_vars['resa_category']->value];?>
</span>
		    		<?php }?>
				<select id="myOwnSelectCat" style="padding:2px" class="button_large" type="<?php echo $_smarty_tpl->tpl_vars['planningSelector']->value;?>
" onChange="my<?php echo $_smarty_tpl->tpl_vars['planningSelector']->value;?>
Selector.checkHomeCat();">
		    		<?php  $_smarty_tpl->tpl_vars['selectCatName'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['selectCatName']->_loop = false;
 $_smarty_tpl->tpl_vars['selectCatId'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['selectCats']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['selectCatName']->key => $_smarty_tpl->tpl_vars['selectCatName']->value) {
$_smarty_tpl->tpl_vars['selectCatName']->_loop = true;
 $_smarty_tpl->tpl_vars['selectCatId']->value = $_smarty_tpl->tpl_vars['selectCatName']->key;
?>
		    			<option value="<?php echo $_smarty_tpl->tpl_vars['selectCatId']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['resa_category']->value==$_smarty_tpl->tpl_vars['selectCatId']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['selectCatName']->value;?>
</option>
		    		<?php } ?>
		    	</select>
		    	<?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.6"&&$_smarty_tpl->tpl_vars['_PS_VERSION_']->value<"1.7") {?></div><?php }?>
	        <?php }?>
	    	<?php if (count($_smarty_tpl->tpl_vars['selector']->value)>1) {?>
	    		<?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.6"&&$_smarty_tpl->tpl_vars['_PS_VERSION_']->value<"1.7") {?>
	    		<div class="selector" id="" style="display:inline-block;text-align:center">
	    			<span style="width: 190px; -webkit-user-select: none;"><?php echo $_smarty_tpl->tpl_vars['selector']->value[$_smarty_tpl->tpl_vars['sel']->value];?>
</span>
	    		<?php }?>
	    		<select style="padding:2px" class="button_large" onChange="my<?php echo $_smarty_tpl->tpl_vars['planningSelector']->value;?>
Selector.viewSelectorChange(this);" id="viewSelector" name="viewSelector">
	    		<?php  $_smarty_tpl->tpl_vars['selectorLabel'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['selectorLabel']->_loop = false;
 $_smarty_tpl->tpl_vars['selectorVal'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['selector']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['selectorLabel']->key => $_smarty_tpl->tpl_vars['selectorLabel']->value) {
$_smarty_tpl->tpl_vars['selectorLabel']->_loop = true;
 $_smarty_tpl->tpl_vars['selectorVal']->value = $_smarty_tpl->tpl_vars['selectorLabel']->key;
?>
	    			<option value="<?php echo $_smarty_tpl->tpl_vars['selectorVal']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['selectorVal']->value==$_smarty_tpl->tpl_vars['sel']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['selectorLabel']->value;?>
</option>
	    		<?php } ?>
	    		</select>
	    		<?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.6"&&$_smarty_tpl->tpl_vars['_PS_VERSION_']->value<"1.7") {?></div><?php }?>
	    	<?php }?>
	        </td>
	        <td align="right" style="text-align:right" width="30%"><?php if ($_smarty_tpl->tpl_vars['sel']->value<$_smarty_tpl->tpl_vars['maxSelection']->value) {?><a  tabindex="6" style="font-weight:normal" onclick="my<?php echo $_smarty_tpl->tpl_vars['planningSelector']->value;?>
Selector.showView(<?php echo smarty_function_math(array('equation'=>"x+1",'x'=>$_smarty_tpl->tpl_vars['sel']->value),$_smarty_tpl);?>
, '<?php echo $_smarty_tpl->tpl_vars['planningType']->value;?>
', '<?php echo $_smarty_tpl->tpl_vars['selStartValue']->value;?>
');" title="<?php echo $_smarty_tpl->tpl_vars['next_label']->value;?>
" class="<?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.6") {?>btn btn-default button button-small<?php } else { ?>button<?php }?>"><span><?php echo $_smarty_tpl->tpl_vars['next_label']->value;?>
 &raquo;</span></a><?php }?></td>
	  </tr>
  
  <tr class="myOwnSubNavigate" style="display:none">
        <td width="150" align="left"><a style="font-weight:normal" onclick="viewSubSelectorParent('<?php echo $_smarty_tpl->tpl_vars['planningType']->value;?>
')" title="<?php echo $_smarty_tpl->tpl_vars['previous_sub_label']->value;?>
" title="&laquo; <?php echo $_smarty_tpl->tpl_vars['previous_sub_label']->value;?>
" class="<?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.6") {?>btn btn-default button button-small<?php } else { ?>button<?php }?>"><span>&laquo; <?php echo $_smarty_tpl->tpl_vars['previous_sub_label']->value;?>
</span></a></td>
        <td style="text-align:center" align="center" <?php if ($_smarty_tpl->tpl_vars['planningType']->value=="column") {?>colspan="2"<?php }?>>
    	<?php if (count($_smarty_tpl->tpl_vars['subSelector']->value)) {?>
    		<select style="padding:2px" class="button_large" onChange="my<?php echo $_smarty_tpl->tpl_vars['planningSelector']->value;?>
Selector.viewSubSelectorChange(this);" id="view<?php echo $_smarty_tpl->tpl_vars['planningType']->value;?>
SubSelector" name="viewSubSelector">
    		<?php  $_smarty_tpl->tpl_vars['subSelectorLabel'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['subSelectorLabel']->_loop = false;
 $_smarty_tpl->tpl_vars['subSelectorVal'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['subSelector']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['subSelectorLabel']->key => $_smarty_tpl->tpl_vars['subSelectorLabel']->value) {
$_smarty_tpl->tpl_vars['subSelectorLabel']->_loop = true;
 $_smarty_tpl->tpl_vars['subSelectorVal']->value = $_smarty_tpl->tpl_vars['subSelectorLabel']->key;
?>
    			<option value="<?php echo $_smarty_tpl->tpl_vars['subSelectorVal']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['subSelectorLabel']->value;?>
</option>
    		<?php } ?>
    		</select>
    	<?php }?>
        </td>
        <?php if ($_smarty_tpl->tpl_vars['planningType']->value=="product") {?><td width="150" align="right"></td><?php }?>
  </tr>
</table><?php }} ?>
