<?php /* Smarty version Smarty-3.1.19, created on 2019-01-11 12:36:39
         compiled from "C:\MAMP\htdocs\fuoriporta\modules\wkproductrating\views\templates\hook\productfooter.tpl" */ ?>
<?php /*%%SmartyHeaderCode:118965c387fc7255c61-54227796%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '988b8a04620776e9374891c270ec8bcdceb02d6e' => 
    array (
      0 => 'C:\\MAMP\\htdocs\\fuoriporta\\modules\\wkproductrating\\views\\templates\\hook\\productfooter.tpl',
      1 => 1546596078,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '118965c387fc7255c61-54227796',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'tableContents' => 0,
    'avgRating' => 0,
    'comment' => 0,
    'i' => 0,
    'k' => 0,
    'j' => 0,
    'allContents' => 0,
    'commentLimit' => 0,
    'idProduct' => 0,
    'link' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5c387fc729a205_00705467',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5c387fc729a205_00705467')) {function content_5c387fc729a205_00705467($_smarty_tpl) {?>

<?php if ($_smarty_tpl->tpl_vars['tableContents']->value) {?>
<section class="page-product-box">
	<h3 class="page-product-heading"><i class="icon icon-star"></i><?php echo smartyTranslate(array('s'=>' AVERAGE REVIEW ','mod'=>'wkproductrating'),$_smarty_tpl);?>
<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['avgRating']->value['averageRating'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</h3>
</section>

<?php  $_smarty_tpl->tpl_vars['comment'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['comment']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tableContents']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['comment']->key => $_smarty_tpl->tpl_vars['comment']->value) {
$_smarty_tpl->tpl_vars['comment']->_loop = true;
?>
	<div class="fullDiv">
		<div class="row">
			<div class="sideBox col-md-1">
				<span class="<?php if ($_smarty_tpl->tpl_vars['comment']->value['rating']<3) {?>alert-danger<?php } elseif ($_smarty_tpl->tpl_vars['comment']->value['rating']<4) {?>alert-warning<?php } else { ?>alert-success<?php }?> ratingBox"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['comment']->value['rating'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</span>
			</div>
			<div class="col-md-8">
				<div id="ratingHeader">
					<div id="showStar">
						<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable(0, null, 0);?>
						<?php while ($_smarty_tpl->tpl_vars['i']->value!=$_smarty_tpl->tpl_vars['comment']->value['rating']) {?>
							<img class="starSize" src="<?php echo mb_convert_encoding(htmlspecialchars(@constant('_MODULE_DIR_'), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
/wkproductrating/views/img/star-on.png" />
							<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable($_smarty_tpl->tpl_vars['i']->value+1, null, 0);?>
						<?php }?>
							<?php $_smarty_tpl->tpl_vars['k'] = new Smarty_variable(0, null, 0);?>	
							<?php $_smarty_tpl->tpl_vars['j'] = new Smarty_variable(5-$_smarty_tpl->tpl_vars['comment']->value['rating'], null, 0);?>
						<?php while ($_smarty_tpl->tpl_vars['k']->value!=$_smarty_tpl->tpl_vars['j']->value) {?>
							<img class="starSize" src="<?php echo mb_convert_encoding(htmlspecialchars(@constant('_MODULE_DIR_'), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
/wkproductrating/views/img/star-off.png" />
							<?php $_smarty_tpl->tpl_vars['k'] = new Smarty_variable($_smarty_tpl->tpl_vars['k']->value+1, null, 0);?>
						<?php }?>
					</div>
					<strong><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['comment']->value['title'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</strong>
				</div>
				<div class="field">
					<p><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['comment']->value['comment'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</p>
				</div>
				<div class="field row">
					<span class="col-md-3">
						<?php echo smartyTranslate(array('s'=>'By','mod'=>'wkproductrating'),$_smarty_tpl);?>

						<strong><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['comment']->value['firstname'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['comment']->value['lastname'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</strong>
					</span>
					<span class="col-md-4">
						<?php echo smartyTranslate(array('s'=>'Posted on:','mod'=>'wkproductrating'),$_smarty_tpl);?>

						<strong>
							<i class="icon-calendar"></i> 
							<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['dateFormat'][0][0]->dateFormat(array('date'=>$_smarty_tpl->tpl_vars['comment']->value['date_upd'],'full'=>mb_convert_encoding(htmlspecialchars(0, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8')),$_smarty_tpl);?>
 - 
							<i class="icon-time"></i> 
							<?php echo mb_convert_encoding(htmlspecialchars(substr($_smarty_tpl->tpl_vars['comment']->value['date_upd'],11,5), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

						</strong>
					</span>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
	<?php if (count($_smarty_tpl->tpl_vars['allContents']->value)>$_smarty_tpl->tpl_vars['commentLimit']->value) {?>
	<div class="row col-md-8">
		<a class="button btn btn-default button-medium viewAll" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getModuleLink('wkproductrating','allcomments',array('id_product'=>mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['idProduct']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8')));?>
">
			<span><?php echo smartyTranslate(array('s'=>'View All (','mod'=>'wkproductrating'),$_smarty_tpl);?>
<?php echo htmlspecialchars(count($_smarty_tpl->tpl_vars['allContents']->value), ENT_QUOTES, 'UTF-8', true);?>

			<?php echo smartyTranslate(array('s'=>' Reviews)','mod'=>'wkproductrating'),$_smarty_tpl);?>
</span>
		</a>
	</div>
	<?php }?>
</div>
<?php }?><?php }} ?>
