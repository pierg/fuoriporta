<?php /* Smarty version Smarty-3.1.19, created on 2019-01-11 12:39:45
         compiled from "C:\MAMP\htdocs\fuoriporta\modules\myownreservations\views\templates\front\planning\header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:101515c388081606380-29662662%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5685d637e56bf042a7fa24ba04125d5c2be9c6ae' => 
    array (
      0 => 'C:\\MAMP\\htdocs\\fuoriporta\\modules\\myownreservations\\views\\templates\\front\\planning\\header.tpl',
      1 => 1545145951,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '101515c388081606380-29662662',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'planningStyle' => 0,
    'potentialResa' => 0,
    'planningType' => 0,
    'id_lang' => 0,
    'calendar' => 0,
    'resStartTimeSlot' => 0,
    'myOwnResLblStart' => 0,
    '_PS_VERSION_' => 0,
    'planningSelector' => 0,
    'myOwnResWay' => 0,
    'myOwnResLblEnd' => 0,
    'wayLabel' => 0,
    'selectionLabel' => 0,
    'reservationLabel' => 0,
    'reservationView' => 0,
    'lengthWidget' => 0,
    'widget' => 0,
    'extra_step' => 0,
    'add_label' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5c388081727359_19133787',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5c388081727359_19133787')) {function content_5c388081727359_19133787($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include 'C:\\MAMP\\htdocs\\fuoriporta\\tools\\smarty\\plugins\\modifier.replace.php';
?><style type="text/css">
.myOwnUnvalidate { display:none };
</style>

<table width="100%" id="myOwn<?php echo $_smarty_tpl->tpl_vars['planningStyle']->value;?>
CalendarHeader" class="myOwnCalendarHeader">
	<?php if (($_smarty_tpl->tpl_vars['potentialResa']->value->_selType=="end")) {?>
	  <?php $_smarty_tpl->tpl_vars['resStartTimeSlot'] = new Smarty_variable($_smarty_tpl->tpl_vars['potentialResa']->value->getStartTimeSlot(), null, 0);?>
	  <tr <?php if ($_smarty_tpl->tpl_vars['planningType']->value=="column"||$_smarty_tpl->tpl_vars['planningStyle']->value=="topproduct") {?>style="border-bottom:none"<?php }?>>
	    <td colspan="<?php if ($_smarty_tpl->tpl_vars['planningType']->value=="column"||$_smarty_tpl->tpl_vars['planningStyle']->value=="topproduct") {?>2<?php } else { ?>2<?php }?>">
	    	<?php if ($_smarty_tpl->tpl_vars['planningType']->value=="column"||$_smarty_tpl->tpl_vars['planningStyle']->value=="topproduct") {?>
	    	 <h5 class="myOwnH5" style="font-weight:normal;height:16px;" label="<?php echo smartyTranslate(array('s'=>'From','mod'=>'myownreservations'),$_smarty_tpl);?>
"><label style="margin-right:10px"><?php echo smartyTranslate(array('s'=>'From','mod'=>'myownreservations'),$_smarty_tpl);?>
</label> <?php echo $_smarty_tpl->tpl_vars['calendar']->value->formatDateWithMonthShort($_smarty_tpl->tpl_vars['potentialResa']->value->getStartDateTime(),$_smarty_tpl->tpl_vars['id_lang']->value);?>
<?php if ($_smarty_tpl->tpl_vars['resStartTimeSlot']->value!=null&&$_smarty_tpl->tpl_vars['resStartTimeSlot']->value->sqlId>0&&$_smarty_tpl->tpl_vars['potentialResa']->value->showTime()) {?> <?php echo $_smarty_tpl->tpl_vars['calendar']->value->formatTime($_smarty_tpl->tpl_vars['resStartTimeSlot']->value->startTime,$_smarty_tpl->tpl_vars['id_lang']->value);?>
<?php }?></h5>
	    	<?php }?>
	    	<?php if ($_smarty_tpl->tpl_vars['planningType']->value=="product"&&$_smarty_tpl->tpl_vars['planningStyle']->value!="topproduct") {?>
			<h3 class="myOwnH3" style="font-weight:normal;"><b><?php echo $_smarty_tpl->tpl_vars['myOwnResLblStart']->value;?>
</b> : <?php echo $_smarty_tpl->tpl_vars['calendar']->value->formatDateWithDay($_smarty_tpl->tpl_vars['potentialResa']->value->getStartDateTime(),$_smarty_tpl->tpl_vars['id_lang']->value);?>
<?php if ($_smarty_tpl->tpl_vars['resStartTimeSlot']->value->sqlId>0&&$_smarty_tpl->tpl_vars['potentialResa']->value->showTime()) {?> <?php echo $_smarty_tpl->tpl_vars['calendar']->value->formatTime($_smarty_tpl->tpl_vars['resStartTimeSlot']->value->startTime,$_smarty_tpl->tpl_vars['id_lang']->value);?>
<?php }?></h3>
	      	<?php }?>
	    </td> 
	    <?php if ($_smarty_tpl->tpl_vars['planningType']->value!="home") {?>
	    <td style="text-align:right" align="right" style="vertical-align:top" <?php if ($_smarty_tpl->tpl_vars['planningType']->value=="column") {?>width="40"<?php }?>>
	      <a  tabindex="3" id="myOwnColumnReset" class="button <?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.6") {?><?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.7") {?>btn-tertiary<?php } else { ?>lnk_view btn btn-default btn<?php }?><?php }?>" onclick="my<?php echo $_smarty_tpl->tpl_vars['planningSelector']->value;?>
Selector.change();return false;" title="Change"><span  style="font-size: 14px;padding:3px;<?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value<"1.7") {?>padding-left:7px;padding-right:7px<?php }?>"><?php if ($_smarty_tpl->tpl_vars['planningType']->value=="column") {?><?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.7") {?><i class="material-icons" style="display:inline">event</i> <?php }?><?php echo smartyTranslate(array('s'=>'Edit','mod'=>'myownreservations'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'Change','mod'=>'myownreservations'),$_smarty_tpl);?>
<?php }?></span></a>
	    </td> 
	    <?php }?>
	  </tr>
	<?php }?>
	
	  <tr>
	  	<td colspan="2"> 
	  	    <?php if ($_smarty_tpl->tpl_vars['planningType']->value=="product") {?>
	  	    	<?php $_smarty_tpl->_capture_stack[0][] = array('my_module_tempvar', null, null); ob_start(); ?><?php echo smartyTranslate(array('s'=>'Select #way# of #reservationLabel#','mod'=>'myownreservations'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
				<?php $_smarty_tpl->tpl_vars['selectionLabel'] = new Smarty_variable(Smarty::$_smarty_vars['capture']['my_module_tempvar'], null, 0);?>
				<?php $_smarty_tpl->tpl_vars['wayLabel'] = new Smarty_variable('', null, 0);?>
				<?php if ($_smarty_tpl->tpl_vars['myOwnResWay']->value&&$_smarty_tpl->tpl_vars['potentialResa']->value->_selType=='') {?>
					<?php if ($_smarty_tpl->tpl_vars['potentialResa']->value->_selTypeNum=="1") {?>
						<?php $_smarty_tpl->tpl_vars['wayLabel'] = new Smarty_variable($_smarty_tpl->tpl_vars['myOwnResLblStart']->value, null, 0);?>
					<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['potentialResa']->value->_selTypeNum=="2") {?>
						<?php $_smarty_tpl->tpl_vars['wayLabel'] = new Smarty_variable($_smarty_tpl->tpl_vars['myOwnResLblEnd']->value, null, 0);?>
					<?php }?>
				<?php } else { ?>
					<?php if ($_smarty_tpl->tpl_vars['potentialResa']->value->_selType=="start") {?>
						<?php $_smarty_tpl->tpl_vars['wayLabel'] = new Smarty_variable($_smarty_tpl->tpl_vars['myOwnResLblStart']->value, null, 0);?>
					<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['potentialResa']->value->_selType=="end") {?>
						<?php $_smarty_tpl->tpl_vars['wayLabel'] = new Smarty_variable($_smarty_tpl->tpl_vars['myOwnResLblEnd']->value, null, 0);?>
					<?php }?>
				<?php }?>
				<h3 class="myOwnH3" 
				style="font-weight:normal;" 
				id="<?php echo $_smarty_tpl->tpl_vars['potentialResa']->value->_selType;?>
productSel" 
				label = "<?php echo $_smarty_tpl->tpl_vars['wayLabel']->value;?>
" 
				defLabel = "<?php echo smarty_modifier_replace(smarty_modifier_replace($_smarty_tpl->tpl_vars['selectionLabel']->value,'#reservationLabel#',$_smarty_tpl->tpl_vars['reservationLabel']->value),'#way#',$_smarty_tpl->tpl_vars['wayLabel']->value);?>
" >
					
					<?php if ($_smarty_tpl->tpl_vars['potentialResa']->value->_selType==''&&!$_smarty_tpl->tpl_vars['myOwnResWay']->value) {?>
						<?php if ($_smarty_tpl->tpl_vars['reservationView']->value->multiSel) {?><b><?php echo smartyTranslate(array('s'=>'Select periods of your','mod'=>'myownreservations'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'Select period of your','mod'=>'myownreservations'),$_smarty_tpl);?>
<?php }?> <?php echo $_smarty_tpl->tpl_vars['reservationLabel']->value;?>
</b>
					<?php } else { ?>
						<b><?php echo smarty_modifier_replace(smarty_modifier_replace($_smarty_tpl->tpl_vars['selectionLabel']->value,'#reservationLabel#',$_smarty_tpl->tpl_vars['reservationLabel']->value),'#way#',$_smarty_tpl->tpl_vars['wayLabel']->value);?>
</b>
					<?php }?>
	
				</h3>
				<?php if ($_smarty_tpl->tpl_vars['reservationView']->value->ext_object!=null&&$_smarty_tpl->tpl_vars['potentialResa']->value->showObject()) {?>
				<h3 class="myOwnH3" 
				style="font-weight:bold;display:none;padding-bottom:0px" id="<?php echo $_smarty_tpl->tpl_vars['potentialResa']->value->_selType;?>
productObj">
				<?php echo $_smarty_tpl->tpl_vars['reservationView']->value->ext_object->label;?>
 :
				</h3>
				<?php }?>
			 <?php }?>
			 <?php if ($_smarty_tpl->tpl_vars['planningType']->value=="column") {?>
				<h5 class="myOwnH5" 
				style = "font-weight:normal;height:16px;" id = "<?php echo $_smarty_tpl->tpl_vars['potentialResa']->value->_selType;?>
<?php echo $_smarty_tpl->tpl_vars['planningSelector']->value;?>
Sel" 
				<?php if ($_smarty_tpl->tpl_vars['potentialResa']->value->_selType=="start") {?>
					label = "<?php echo smartyTranslate(array('s'=>'From','mod'=>'myownreservations'),$_smarty_tpl);?>
" defLabel = "<?php echo smartyTranslate(array('s'=>'From','mod'=>'myownreservations'),$_smarty_tpl);?>
" 
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['potentialResa']->value->_selType=="end"&&!$_smarty_tpl->tpl_vars['lengthWidget']->value) {?>
					label = "<?php echo smartyTranslate(array('s'=>'To','mod'=>'myownreservations'),$_smarty_tpl);?>
" defLabel = "<?php echo smartyTranslate(array('s'=>'To','mod'=>'myownreservations'),$_smarty_tpl);?>
" 
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['potentialResa']->value->_selType=="end"&&$_smarty_tpl->tpl_vars['lengthWidget']->value) {?>
					label = "<?php echo smartyTranslate(array('s'=>'During','mod'=>'myownreservations'),$_smarty_tpl);?>
" defLabel = "<?php echo smartyTranslate(array('s'=>'During','mod'=>'myownreservations'),$_smarty_tpl);?>
" 
				<?php }?>>
				<label style="margin-right:10px">
				<?php if ($_smarty_tpl->tpl_vars['potentialResa']->value->_selType=="start") {?><?php echo smartyTranslate(array('s'=>'From','mod'=>'myownreservations'),$_smarty_tpl);?>
<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['potentialResa']->value->_selType=="end"&&!$_smarty_tpl->tpl_vars['lengthWidget']->value) {?><?php echo smartyTranslate(array('s'=>'To','mod'=>'myownreservations'),$_smarty_tpl);?>
<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['potentialResa']->value->_selType=="end"&&$_smarty_tpl->tpl_vars['lengthWidget']->value) {?><?php echo smartyTranslate(array('s'=>'During','mod'=>'myownreservations'),$_smarty_tpl);?>
<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['potentialResa']->value->_selType=='') {?><?php echo smartyTranslate(array('s'=>'At','mod'=>'myownreservations'),$_smarty_tpl);?>
<?php }?>
				</label>
				</h5>
			 <?php }?>
	  	</td>
	  	<?php if ($_smarty_tpl->tpl_vars['planningType']->value!="column") {?>
	  	<td align="right" style="text-align:right;" valign="top">
	  		<?php if (!$_smarty_tpl->tpl_vars['widget']->value) {?> 
		        <?php if ($_smarty_tpl->tpl_vars['potentialResa']->value->_selType=="start") {?>
		        <?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.7") {?>
		        	<button disabled id="myOwnUnvalidate" class="btn btn-primary myOwnUnvalidate"><?php echo smartyTranslate(array('s'=>'Validate','mod'=>'myownreservations'),$_smarty_tpl);?>
 <?php echo $_smarty_tpl->tpl_vars['myOwnResLblStart']->value;?>
</button>
		        <?php } else { ?>
		        	<span id="myOwnUnvalidate" class="exclusive myOwnUnvalidate"><?php echo smartyTranslate(array('s'=>'Validate','mod'=>'myownreservations'),$_smarty_tpl);?>
 <?php echo $_smarty_tpl->tpl_vars['myOwnResLblStart']->value;?>
</span>
		        <?php }?>
		        <a tabindex="4"  id="myOwnValidate" class="myOwnValidate <?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.6") {?>button btn btn-default btn-primary<?php }?>" style="display:none" onclick="my<?php echo $_smarty_tpl->tpl_vars['planningSelector']->value;?>
Selector.validateProductStart();goToResaTabScroll();"><span><?php echo smartyTranslate(array('s'=>'Validate','mod'=>'myownreservations'),$_smarty_tpl);?>
 <?php echo $_smarty_tpl->tpl_vars['myOwnResLblStart']->value;?>
</span></a>
		        <?php }?>
		        <?php if ($_smarty_tpl->tpl_vars['potentialResa']->value->_selType=="end"||$_smarty_tpl->tpl_vars['potentialResa']->value->_selType=='') {?>
		        	<?php $_smarty_tpl->_capture_stack[0][] = array('default', "add_label", null); ob_start(); ?><?php if ($_smarty_tpl->tpl_vars['extra_step']->value!='') {?><?php echo smartyTranslate(array('s'=>'Validate','mod'=>'myownreservations'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'myownreservations'),$_smarty_tpl);?>
<?php }?><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
			        <?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.7") {?>
			        	<button disabled id="myOwnUnvalidate" class="exclusive myOwnUnvalidate btn btn-primary"><?php echo $_smarty_tpl->tpl_vars['add_label']->value;?>
</button>
			        <?php } else { ?>
			        	<span id="myOwnUnvalidate" class="exclusive myOwnUnvalidate btn btn-primary"><?php echo $_smarty_tpl->tpl_vars['add_label']->value;?>
</span>
			        <?php }?>
			        <a id="myOwnValidate" class="myOwnValidate <?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.6") {?>button btn btn-default btn-primary<?php }?>" style="display:none" onclick="<?php if ($_smarty_tpl->tpl_vars['extra_step']->value!='') {?><?php echo $_smarty_tpl->tpl_vars['extra_step']->value;?>
<?php } else { ?>myownreservationAddToCart(this);<?php }?>"><span><?php echo $_smarty_tpl->tpl_vars['add_label']->value;?>
</span></a>
		        <?php }?>
	        <?php }?>
		</td>
		<?php } else { ?>
		<td style="text-align:right" align="right" width="50%">
			<a tabindex="0" id="myOwnColumnEdit" class="button <?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.6") {?><?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.7") {?>btn-tertiary<?php } else { ?>lnk_view btn btn-default<?php }?><?php }?>" onclick="my<?php echo $_smarty_tpl->tpl_vars['planningSelector']->value;?>
Selector.change();$(this).hide();return false;" title="Change" style="float:left;display:none;width:100%;cursor:pointer"><span style="font-size: 14px;padding:3px;padding-left:7px;padding-right:7px;text-align:center"><?php if ($_smarty_tpl->tpl_vars['_PS_VERSION_']->value>="1.7") {?><i class="material-icons" style="display:inline">event</i> <?php }?><?php echo smartyTranslate(array('s'=>'Edit','mod'=>'myownreservations'),$_smarty_tpl);?>
</span></a>
		</td>
		<?php }?>
	  </tr>
	  
	  <?php if ($_smarty_tpl->tpl_vars['reservationView']->value->multiSel&&$_smarty_tpl->tpl_vars['planningType']->value!="column") {?>
	  <tr>
	  	<td colspan="3">
	  		<h4><div class="selectDelete" style="display:none" onclick="my<?php echo $_smarty_tpl->tpl_vars['planningType']->value;?>
Selector.empty();"></div><div class="selectedlegend"></div></h4>
	  	</td>
	  </tr>
	  <?php }?>
</table><?php }} ?>
