<?php /* Smarty version Smarty-3.1.19, created on 2019-01-11 12:39:45
         compiled from "C:\MAMP\htdocs\fuoriporta\modules\myownreservations\views\templates\front\planning\_partials\cell_product.tpl" */ ?>
<?php /*%%SmartyHeaderCode:59555c388081a8e0a2-24446799%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5603c76106c095dcf9d187ef9d5cdcddb8b51706' => 
    array (
      0 => 'C:\\MAMP\\htdocs\\fuoriporta\\modules\\myownreservations\\views\\templates\\front\\planning\\_partials\\cell_product.tpl',
      1 => 1545145951,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '59555c388081a8e0a2-24446799',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'timeSlot' => 0,
    'day' => 0,
    'slice' => 0,
    'planningType' => 0,
    'potentialResa' => 0,
    'planningStyle' => 0,
    'id_lang' => 0,
    'calendar' => 0,
    'hidePrice' => 0,
    'timeSlotAvailable' => 0,
    'planningSelector' => 0,
    'widget' => 0,
    'reservationView' => 0,
    'previewPriceLabel' => 0,
    'previewQuantityLabel' => 0,
    'previewQuantityLabels' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5c388081b41b35_99817623',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5c388081b41b35_99817623')) {function content_5c388081b41b35_99817623($_smarty_tpl) {?>
	<?php $_smarty_tpl->tpl_vars['slice'] = new Smarty_variable($_smarty_tpl->tpl_vars['day']->value->getSlice($_smarty_tpl->tpl_vars['timeSlot']->value->sqlId), null, 0);?>
	<?php if ($_smarty_tpl->tpl_vars['slice']->value==null) {?>
	    <div class="emptySlot <?php echo $_smarty_tpl->tpl_vars['planningType']->value;?>
Slot">&nbsp;</div>
	<?php } else { ?>
		<?php if (!$_smarty_tpl->tpl_vars['slice']->value->isAvailable) {?>
			<?php if ($_smarty_tpl->tpl_vars['potentialResa']->value->_mainProduct->productType==product_type::AVAILABLE) {?>
			<div class="emptySlot <?php echo $_smarty_tpl->tpl_vars['planningType']->value;?>
Slot">
				<div class="unslotLabel">
					<?php if ($_smarty_tpl->tpl_vars['planningStyle']->value!="pproduct") {?><span class="timeLabel"><?php }?>
						<?php echo $_smarty_tpl->tpl_vars['calendar']->value->formatDateShort($_smarty_tpl->tpl_vars['slice']->value->start,$_smarty_tpl->tpl_vars['id_lang']->value);?>

					<?php if ($_smarty_tpl->tpl_vars['planningStyle']->value!="pproduct") {?></span><?php }?>
				</div>
			</div>
			<?php } else { ?>
			<div class="holidaySlot <?php echo $_smarty_tpl->tpl_vars['planningType']->value;?>
Slot">
				<div class="unslotLabel">
					<?php if ($_smarty_tpl->tpl_vars['planningStyle']->value!="pproduct") {?><span class="timeLabel"><?php }?>
						<?php echo $_smarty_tpl->tpl_vars['slice']->value->label;?>

					<?php if ($_smarty_tpl->tpl_vars['planningStyle']->value!="pproduct") {?></span><?php }?>
					<?php if (!$_smarty_tpl->tpl_vars['hidePrice']->value&&$_smarty_tpl->tpl_vars['potentialResa']->value->showPrice()) {?>
						<br/><?php }?><?php if ($_smarty_tpl->tpl_vars['potentialResa']->value->showQuantity()) {?><br/><?php }?>
				</div>
			</div>
			<?php }?>
		<?php } else { ?>
			<?php if (!$_smarty_tpl->tpl_vars['slice']->value->isEnable) {?>
				<div class="unavailableSlot <?php echo $_smarty_tpl->tpl_vars['planningType']->value;?>
Slot">
					<div class="unslotLabel"><?php echo $_smarty_tpl->tpl_vars['slice']->value->label;?>
</div>
				</div>
			<?php } else { ?>
			    <?php $_smarty_tpl->tpl_vars['timeSlotAvailable'] = new Smarty_variable(($_smarty_tpl->tpl_vars['slice']->value->availableQuantity>=$_smarty_tpl->tpl_vars['potentialResa']->value->getOccupationQty()||$_smarty_tpl->tpl_vars['slice']->value->availableQuantity<0), null, 0);?>
			    <div <?php if ($_smarty_tpl->tpl_vars['timeSlotAvailable']->value) {?>
				    	onmouseover="my<?php echo $_smarty_tpl->tpl_vars['planningSelector']->value;?>
Selector.hover(this, true, '<?php echo $_smarty_tpl->tpl_vars['planningType']->value;?>
');" 
				    	onmouseout="my<?php echo $_smarty_tpl->tpl_vars['planningSelector']->value;?>
Selector.hover(this, false, '<?php echo $_smarty_tpl->tpl_vars['planningType']->value;?>
');" 
				    	time="<?php echo $_smarty_tpl->tpl_vars['slice']->value->start;?>
" 
				    	key="<?php echo $_smarty_tpl->tpl_vars['slice']->value->getKey();?>
" 
				    	<?php if ($_smarty_tpl->tpl_vars['potentialResa']->value->showLength()) {?>length start="<?php echo $_smarty_tpl->tpl_vars['slice']->value->getStartMinutes();?>
" endSlice="<?php echo $_smarty_tpl->tpl_vars['slice']->value->getSliceEndMinutes();?>
" endSlot="<?php echo $_smarty_tpl->tpl_vars['slice']->value->getSlotEndMinutes();?>
" endSlotTime="<?php echo $_smarty_tpl->tpl_vars['slice']->value->slotEnd;?>
"  endTime="<?php echo $_smarty_tpl->tpl_vars['slice']->value->end;?>
" <?php }?>
				    	id="<?php echo $_smarty_tpl->tpl_vars['planningType']->value;?>
<?php echo $_smarty_tpl->tpl_vars['slice']->value->getKey();?>
" 
				    	<?php if (($_smarty_tpl->tpl_vars['potentialResa']->value->_selType=="start")) {?>
				    		label="<?php if ($_smarty_tpl->tpl_vars['planningStyle']->value=="topproduct") {?><?php echo $_smarty_tpl->tpl_vars['calendar']->value->formatDateShort($_smarty_tpl->tpl_vars['slice']->value->start,$_smarty_tpl->tpl_vars['id_lang']->value);?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['calendar']->value->formatDateWithDay($_smarty_tpl->tpl_vars['slice']->value->start,$_smarty_tpl->tpl_vars['id_lang']->value);?>
<?php }?> <?php if ($_smarty_tpl->tpl_vars['slice']->value->idTimeSlot&&$_smarty_tpl->tpl_vars['potentialResa']->value->showTime()) {?><?php echo $_smarty_tpl->tpl_vars['calendar']->value->formatTime($_smarty_tpl->tpl_vars['slice']->value->getStartTime(),$_smarty_tpl->tpl_vars['id_lang']->value);?>
<?php }?>" 
				    	<?php }?>
				    	<?php if (($_smarty_tpl->tpl_vars['potentialResa']->value->_selType=="end")) {?>
				    		label="<?php if ($_smarty_tpl->tpl_vars['planningStyle']->value=="topproduct") {?><?php echo $_smarty_tpl->tpl_vars['calendar']->value->formatDateShort($_smarty_tpl->tpl_vars['slice']->value->end,$_smarty_tpl->tpl_vars['id_lang']->value);?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['calendar']->value->formatDateWithDay($_smarty_tpl->tpl_vars['slice']->value->end,$_smarty_tpl->tpl_vars['id_lang']->value);?>
<?php }?> <?php if ($_smarty_tpl->tpl_vars['slice']->value->idTimeSlot&&$_smarty_tpl->tpl_vars['potentialResa']->value->showTime()) {?><?php echo $_smarty_tpl->tpl_vars['calendar']->value->formatTime($_smarty_tpl->tpl_vars['slice']->value->getEndTime(),$_smarty_tpl->tpl_vars['id_lang']->value);?>
<?php }?>" 
				    	<?php }?>
				    	<?php if ($_smarty_tpl->tpl_vars['potentialResa']->value->_selType=='') {?>
				    		label="<?php echo $_smarty_tpl->tpl_vars['slice']->value->label;?>
"
				    	<?php }?>
				    	onclick="<?php if (!$_smarty_tpl->tpl_vars['hidePrice']->value&&$_smarty_tpl->tpl_vars['potentialResa']->value->showPrice()&&$_smarty_tpl->tpl_vars['planningStyle']->value=="topproduct") {?>showResaPrice('<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['slice']->value->price),$_smarty_tpl);?>
');<?php }?>my<?php echo $_smarty_tpl->tpl_vars['planningSelector']->value;?>
Selector.<?php if ($_smarty_tpl->tpl_vars['timeSlot']->value->sqlId!=-1) {?>slotSelection(this)<?php } else { ?>showDay('<?php echo $_smarty_tpl->tpl_vars['day']->value->key;?>
')<?php }?>;" 
				    	class="slot <?php if ($_smarty_tpl->tpl_vars['slice']->value->css!='') {?><?php echo $_smarty_tpl->tpl_vars['slice']->value->css;?>
<?php }?>availableSlot <?php echo $_smarty_tpl->tpl_vars['planningType']->value;?>
Slot" 
						name="<?php if ($_smarty_tpl->tpl_vars['slice']->value->css!='') {?><?php echo $_smarty_tpl->tpl_vars['slice']->value->css;?>
<?php } else { ?>availableSlot<?php }?>"
				    <?php } else { ?>
				    	class="slot unavailableSlot <?php echo $_smarty_tpl->tpl_vars['planningType']->value;?>
Slot"
				    	name="unavailableSlot"
			    	<?php }?>
			    	<?php if ($_smarty_tpl->tpl_vars['slice']->value->object!=null) {?>
			    		objectValue="<?php echo $_smarty_tpl->tpl_vars['slice']->value->object->sqlId;?>
" 
			    		objectLabel="<?php echo $_smarty_tpl->tpl_vars['slice']->value->object->name;?>
" 
				     <?php }?> 
			    >
				    <div class="slotInput">
				    	<?php if (!$_smarty_tpl->tpl_vars['widget']->value) {?>
				            <input name="myOwnDeliveriesRes" type="<?php if ($_smarty_tpl->tpl_vars['reservationView']->value->multiSel) {?>checkbox<?php } else { ?>radio<?php }?>" <?php if (!$_smarty_tpl->tpl_vars['timeSlotAvailable']->value) {?>disabled<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['slice']->value->getKey();?>
" />
				        <?php }?>
				    </div>
			        <div class="slotLabel">
				        <?php if ($_smarty_tpl->tpl_vars['planningStyle']->value!="pproduct") {?><span class="timeLabel"><?php }?>
				        	<?php echo $_smarty_tpl->tpl_vars['slice']->value->label;?>

				        <?php if ($_smarty_tpl->tpl_vars['planningStyle']->value!="pproduct") {?></span><?php }?>
				        <?php if ($_smarty_tpl->tpl_vars['slice']->value->label!='') {?><br/><?php }?>
			        	<?php if ($_smarty_tpl->tpl_vars['timeSlot']->value->sqlId!=-1) {?>
				        	<?php if (!$_smarty_tpl->tpl_vars['hidePrice']->value&&$_smarty_tpl->tpl_vars['potentialResa']->value->showPrice()&&($_smarty_tpl->tpl_vars['planningStyle']->value=="product"||$_smarty_tpl->tpl_vars['planningStyle']->value=="topproduct")) {?>
				        		<span class="priceLabel <?php if ($_smarty_tpl->tpl_vars['slice']->value->isReduc) {?>reductionLabel<?php }?>">
				        			 <?php if (($_smarty_tpl->tpl_vars['potentialResa']->value->_selType=="start")) {?>
				        				<span class="estimateLabel"><?php echo $_smarty_tpl->tpl_vars['previewPriceLabel']->value;?>
</span>
				        			 <?php }?>
				        			 <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['slice']->value->price),$_smarty_tpl);?>
</span>
				        	<?php }?>
				        	<?php if ($_smarty_tpl->tpl_vars['potentialResa']->value->showQuantity()) {?>
				        		<br/><span class="quantityLabel"><?php if ($_smarty_tpl->tpl_vars['slice']->value->availableQuantity==0) {?><?php echo smartyTranslate(array('s'=>'not','mod'=>'myownreservations'),$_smarty_tpl);?>
 <?php echo $_smarty_tpl->tpl_vars['previewQuantityLabel']->value;?>
<?php } else { ?><?php if ($_smarty_tpl->tpl_vars['slice']->value->availableQuantity>0) {?><?php echo $_smarty_tpl->tpl_vars['slice']->value->availableQuantity;?>
<?php }?> <?php if ($_smarty_tpl->tpl_vars['slice']->value->availableQuantity>1) {?><?php echo $_smarty_tpl->tpl_vars['previewQuantityLabels']->value;?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['previewQuantityLabel']->value;?>
<?php }?><?php }?></span>
				        	<?php }?>
				        	<?php if ($_smarty_tpl->tpl_vars['slice']->value->object!=null&&$_smarty_tpl->tpl_vars['potentialResa']->value->showObject()) {?>
				        	<br/><span class="objectLabel"><?php echo $_smarty_tpl->tpl_vars['reservationView']->value->ext_object->label;?>
 : <?php echo $_smarty_tpl->tpl_vars['slice']->value->object->name;?>
</span>
				        	<?php }?>
			        	<?php }?>
			        </div>
			    </div>
			<?php }?>
		<?php }?>
	<?php }?>
<?php }} ?>
