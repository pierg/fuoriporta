<?php /* Smarty version Smarty-3.1.19, created on 2019-01-11 12:36:39
         compiled from "C:\MAMP\htdocs\fuoriporta\modules\powerfulproductcontact\views\templates\front\form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:88875c387fc7570c43-19946568%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd8cfb6ebe50abb9789e3e19d203ee786dbec99bc' => 
    array (
      0 => 'C:\\MAMP\\htdocs\\fuoriporta\\modules\\powerfulproductcontact\\views\\templates\\front\\form.tpl',
      1 => 1547136999,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '88875c387fc7570c43-19946568',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'errors' => 0,
    'successSent' => 0,
    'successMessage' => 0,
    'request_uri' => 0,
    'id_customer' => 0,
    'title' => 0,
    'message' => 0,
    'fields' => 0,
    'field' => 0,
    'link' => 0,
    'email_customer' => 0,
    'isv16' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5c387fc75b2da6_45994674',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5c387fc75b2da6_45994674')) {function content_5c387fc75b2da6_45994674($_smarty_tpl) {?>

<?php if (isset($_smarty_tpl->tpl_vars['errors']->value)&&$_smarty_tpl->tpl_vars['errors']->value) {?>

	
		<script>
	        $(document).ready(function (){
	          
	                $('html, body').animate({
	                    scrollTop: $(".ppc-forms").offset().top
	                }, 2000);
	         
	        });
	    </script>
	

<?php }?>

<?php if (isset($_smarty_tpl->tpl_vars['successSent']->value)) {?>
	<?php if (isset($_smarty_tpl->tpl_vars['successMessage']->value)&&!empty($_smarty_tpl->tpl_vars['successMessage']->value)) {?>
		
			<script>
		        $(document).ready(function (){
		          
		                $('html, body').animate({
		                    scrollTop: $("#toptop").offset().top
		                }, 2000);
		         
		        });
		    </script>
		
	<?php }?>
<?php }?>	

<div id="toptop"></div>

<?php if (isset($_smarty_tpl->tpl_vars['successSent']->value)) {?>
	<?php if (isset($_smarty_tpl->tpl_vars['successMessage']->value)&&!empty($_smarty_tpl->tpl_vars['successMessage']->value)) {?>
	<div class="alert alert-info"><?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['successMessage']->value);?>
</div>
	<?php } else { ?>
	<p class="alert alert-info"><?php echo smartyTranslate(array('s'=>'Your message has been successfully sent to our team.','mod'=>'powerfulproductcontact'),$_smarty_tpl);?>
</p>
	<?php }?>
<?php } else { ?>

<style>
#field_privacy{
	
	margin: 0px !important; 
	    height: auto !important;
	

}
</style>



	<form id="foo" action="<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['request_uri']->value);?>
" method="post" class="std ppc-forms zx" enctype="multipart/form-data" id="contact">
		<fieldset>
			<div class="headline">
				

				<?php if (($_smarty_tpl->tpl_vars['id_customer']->value)) {?>
					<div id="showmenu" class="btn btn-lg btn-primary" data-toggle="modal"> <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['title']->value);?>
</div>
				<?php } else { ?>
					<a class="login btn btn-lg btn-primary" href="javascript:void(0)" rel="nofollow" title="Accedi al tuo account cliente" id="modal_trigger" onclick="showLoginPopup()">
						<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['title']->value);?>

					</a>
				<?php }?>
			</div>

			<div class="fmenu" style="display: none;">
			<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./errors.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


			<?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
			<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['message']->value);?>

			<?php }?>

			<?php  $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field']->key => $_smarty_tpl->tpl_vars['field']->value) {
$_smarty_tpl->tpl_vars['field']->_loop = true;
?>  


				<?php if (($_smarty_tpl->tpl_vars['field']->value['name']=="privacy")) {?>

					<fieldset class="account_creation customerprivacy">
						<h3 class="page-subheading">
							Privacy dei dati dei clienti
						</h3>
						<div style="width:21px; float:left;">
							<div class="required checkbox">
								<div class="checker" id="uniform-customer_privacy">
									<span>
									<input type="checkbox" value="1" id="field_privacy" required class="form-control" name="privacy" autocomplete="off">
									</span>
								</div>
							</div>
						</div>
						<div style="width: 92%; float: left; margin-top: 8px;">
					        <label for="field_privacy" style="font-weight: normal;">
					        	<p>
					        		<span>
										HO LETTO E ACCETTO <a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCmsLink(2);?>
" title="Privacy" target="_blank" rel="nofollow">IL TRATTAMENTO DEI MIEI DATI PERSONALI </a> <em class="required">*</em>
							        </span>
							    </p>
							</label>				 
						</div>
					</fieldset>



				<?php } else { ?>

					<?php if ($_smarty_tpl->tpl_vars['field']->value['type']=='recaptcha') {?>
					<p class="recpatcha">
						<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['field']->value['element']);?>

					</p>
					<?php } elseif ($_smarty_tpl->tpl_vars['field']->value['name']=='email') {?>
						<p class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['type'], ENT_QUOTES, 'UTF-8', true);?>
" style="display: none">
							<label for="<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['field']->value['id']);?>
"><?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['field']->value['label']);?>
<?php if ($_smarty_tpl->tpl_vars['field']->value['required']) {?> <em class="required">*</em><?php }?></label>
							<input type="email" name="email" id="field_email" required="" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['email_customer']->value;?>
">

						</p>
					<?php } elseif ($_smarty_tpl->tpl_vars['field']->value['type']!='hidden') {?>
					<p class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['type'], ENT_QUOTES, 'UTF-8', true);?>
">
						<label for="<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['field']->value['id']);?>
"><?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['field']->value['label']);?>
<?php if ($_smarty_tpl->tpl_vars['field']->value['required']) {?> <em class="required">*</em><?php }?></label>
						<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['field']->value['element']);?>

					</p>
					<?php } else { ?>
					<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['field']->value['element']);?>

					<?php }?>
				<?php }?>	

			<?php } ?>




			<p class="submit">
				<input type="submit" name="submitMessage" id="submitMessage" value="<?php echo smartyTranslate(array('s'=>'Send','mod'=>'powerfulproductcontact'),$_smarty_tpl);?>
" <?php if ($_smarty_tpl->tpl_vars['isv16']->value) {?>class="button btn btn-default button-medium" style="padding: 10px 20px"<?php } else { ?>class="button_large"<?php }?> />
			</p>
			</div>
		</fieldset>
	</form>

<?php }?>
<?php }} ?>
