<?php /* Smarty version Smarty-3.1.19, created on 2019-01-11 12:39:45
         compiled from "C:\MAMP\htdocs\fuoriporta\modules\myownreservations\views\templates\front\planning\view_weekmonth.tpl" */ ?>
<?php /*%%SmartyHeaderCode:204775c388081880813-93277296%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd9f88b9d3044d8b9ef363cc2d6581eadcf367b7b' => 
    array (
      0 => 'C:\\MAMP\\htdocs\\fuoriporta\\modules\\myownreservations\\views\\templates\\front\\planning\\view_weekmonth.tpl',
      1 => 1545145951,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '204775c388081880813-93277296',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'reservationMonths' => 0,
    'planningStyle' => 0,
    'reservationView' => 0,
    'id_lang' => 0,
    'reservationMonth' => 0,
    'timeSlots' => 0,
    'timeSlotKey' => 0,
    'timeSlot' => 0,
    'calendar' => 0,
    'planning_dayline' => 0,
    'isMultipleMonth' => 0,
    'reservationWeek' => 0,
    'planning_cell' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5c3880818c8fc6_33395571',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5c3880818c8fc6_33395571')) {function content_5c3880818c8fc6_33395571($_smarty_tpl) {?><?php  $_smarty_tpl->tpl_vars['reservationMonth'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['reservationMonth']->_loop = false;
 $_smarty_tpl->tpl_vars['month'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['reservationMonths']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['reservationMonth']->key => $_smarty_tpl->tpl_vars['reservationMonth']->value) {
$_smarty_tpl->tpl_vars['reservationMonth']->_loop = true;
 $_smarty_tpl->tpl_vars['month']->value = $_smarty_tpl->tpl_vars['reservationMonth']->key;
?>
<center>
<table width="100%" id="myOwn<?php echo $_smarty_tpl->tpl_vars['planningStyle']->value;?>
CalendarTop" class="myOwnCalendarTop">
	<tr>
		<?php if ($_smarty_tpl->tpl_vars['reservationView']->value->homeProducts!=array()) {?>
		    <td class="myOwnCalendarProduct myOwnCalendarTopItem"></td>
		<?php }?>
        <td class="myOwnCalendarTopItem">
        <?php echo $_smarty_tpl->tpl_vars['reservationMonth']->value->toString($_smarty_tpl->tpl_vars['id_lang']->value);?>

        </td>
	<tr/>
</table>
<?php $_smarty_tpl->tpl_vars['lastEndTime'] = new Smarty_variable('', null, 0);?>
<?php  $_smarty_tpl->tpl_vars['timeSlot'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['timeSlot']->_loop = false;
 $_smarty_tpl->tpl_vars['timeSlotKey'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['timeSlots']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['timeSlot']->key => $_smarty_tpl->tpl_vars['timeSlot']->value) {
$_smarty_tpl->tpl_vars['timeSlot']->_loop = true;
 $_smarty_tpl->tpl_vars['timeSlotKey']->value = $_smarty_tpl->tpl_vars['timeSlot']->key;
?>
<table width="100%" id="myOwn<?php echo $_smarty_tpl->tpl_vars['planningStyle']->value;?>
WeekMonthCalendarLine" class="myOwnCalendarLine">
        <tr>
        	<?php if ($_smarty_tpl->tpl_vars['timeSlotKey']->value>0) {?>
        		<td class="myOwnCalendarHours"><?php echo $_smarty_tpl->tpl_vars['calendar']->value->formatTime($_smarty_tpl->tpl_vars['timeSlot']->value->startTime,$_smarty_tpl->tpl_vars['id_lang']->value);?>
</td>
        	<?php }?>
        	
        	<?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['planning_dayline']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('cell_days'=>array(),'linetype'=>"sel",'isDaySel'=>true), 0);?>

        	<?php if ($_smarty_tpl->tpl_vars['isMultipleMonth']->value&&count($_smarty_tpl->tpl_vars['reservationView']->value->weeks)<5) {?><td class="myOwnCalendarLineItem"></td><?php }?>
            <?php  $_smarty_tpl->tpl_vars['reservationWeek'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['reservationWeek']->_loop = false;
 $_smarty_tpl->tpl_vars['week'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['reservationMonth']->value->_weeks; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['reservationWeek']->key => $_smarty_tpl->tpl_vars['reservationWeek']->value) {
$_smarty_tpl->tpl_vars['reservationWeek']->_loop = true;
 $_smarty_tpl->tpl_vars['week']->value = $_smarty_tpl->tpl_vars['reservationWeek']->key;
?>
            	<?php $_smarty_tpl->tpl_vars['day'] = new Smarty_variable($_smarty_tpl->tpl_vars['reservationWeek']->value->getDaySelection(), null, 0);?>
                <td class="myOwnCalendarLineItem" <?php if ($_smarty_tpl->tpl_vars['isMultipleMonth']->value&&count($_smarty_tpl->tpl_vars['reservationView']->value->weeks)<5) {?>colspan="2"<?php }?>>
                    <?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['planning_cell']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                </td>
            <?php } ?>
            <?php if ($_smarty_tpl->tpl_vars['isMultipleMonth']->value&&count($_smarty_tpl->tpl_vars['reservationView']->value->weeks)<5) {?><td class="myOwnCalendarLineItem"></td><?php }?>
        <tr/>
        
        <?php if ($_smarty_tpl->tpl_vars['timeSlotKey']->value>0) {?>
        	<td class="myOwnCalendarHours"><?php echo $_smarty_tpl->tpl_vars['calendar']->value->formatTime($_smarty_tpl->tpl_vars['timeSlot']->value->endTime,$_smarty_tpl->tpl_vars['id_lang']->value);?>
</td>
        	<?php  $_smarty_tpl->tpl_vars['reservationWeek'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['reservationWeek']->_loop = false;
 $_smarty_tpl->tpl_vars['week'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['reservationMonth']->value->_weeks; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['reservationWeek']->key => $_smarty_tpl->tpl_vars['reservationWeek']->value) {
$_smarty_tpl->tpl_vars['reservationWeek']->_loop = true;
 $_smarty_tpl->tpl_vars['week']->value = $_smarty_tpl->tpl_vars['reservationWeek']->key;
?>
        	<td></td>
        	<?php } ?>
        <?php }?>
</table>
<?php } ?>
</center>
<?php } ?><?php }} ?>
