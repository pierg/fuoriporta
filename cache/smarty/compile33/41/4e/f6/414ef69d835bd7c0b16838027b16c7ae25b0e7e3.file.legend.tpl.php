<?php /* Smarty version Smarty-3.1.19, created on 2019-01-11 12:36:39
         compiled from "C:\MAMP\htdocs\fuoriporta\modules\myownreservations\views\templates\front\planning\legend.tpl" */ ?>
<?php /*%%SmartyHeaderCode:44895c387fc780ca82-93962484%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '414ef69d835bd7c0b16838027b16c7ae25b0e7e3' => 
    array (
      0 => 'C:\\MAMP\\htdocs\\fuoriporta\\modules\\myownreservations\\views\\templates\\front\\planning\\legend.tpl',
      1 => 1545145951,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '44895c387fc780ca82-93962484',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'potentialRules' => 0,
    'id_rule' => 0,
    'potentialRulesConditions' => 0,
    'mainProduct' => 0,
    'potentialRule' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5c387fc7816e67_59450167',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5c387fc7816e67_59450167')) {function content_5c387fc7816e67_59450167($_smarty_tpl) {?><section class="page-product-box">
	<h3 class="page-product-heading"><?php echo smartyTranslate(array('s'=>'Special offers','mod'=>'myownreservations'),$_smarty_tpl);?>
</h3>
	<div id="potentialRules">
		<table class="std table-product-pricerules">
			<thead>
				<tr>
					<th style="width:40%"><?php echo smartyTranslate(array('s'=>'Condition','mod'=>'myownreservations'),$_smarty_tpl);?>
</th>
					<th><?php echo smartyTranslate(array('s'=>'Discount','mod'=>'myownreservations'),$_smarty_tpl);?>
</th>
				</tr>
			</thead>
			<tbody>
			  <?php  $_smarty_tpl->tpl_vars['potentialRule'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['potentialRule']->_loop = false;
 $_smarty_tpl->tpl_vars['id_rule'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['potentialRules']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['potentialRule']->key => $_smarty_tpl->tpl_vars['potentialRule']->value) {
$_smarty_tpl->tpl_vars['potentialRule']->_loop = true;
 $_smarty_tpl->tpl_vars['id_rule']->value = $_smarty_tpl->tpl_vars['potentialRule']->key;
?>
				<tr id="potentialRule_<?php echo $_smarty_tpl->tpl_vars['id_rule']->value;?>
">
					<td><?php echo $_smarty_tpl->tpl_vars['potentialRulesConditions']->value[$_smarty_tpl->tpl_vars['id_rule']->value];?>
</td>
					<td class="priceRule<?php echo $_smarty_tpl->tpl_vars['id_rule']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['potentialRule']->value->getInpactToString($_smarty_tpl->tpl_vars['mainProduct']->value);?>
</td>
				</tr>
			  <?php } ?>
			</tbody>
		</table>
	</div>
</section><?php }} ?>
