<?php /* Smarty version Smarty-3.1.19, created on 2019-01-11 15:32:06
         compiled from "C:\MAMP\htdocs\fuoriporta\modules\myownreservations\views\templates\admin\reservation_display.tpl" */ ?>
<?php /*%%SmartyHeaderCode:264565c38a8e63922e4-00350999%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4bd5235fc0707300738e0a814a2090e9c763aa6a' => 
    array (
      0 => 'C:\\MAMP\\htdocs\\fuoriporta\\modules\\myownreservations\\views\\templates\\admin\\reservation_display.tpl',
      1 => 1545145951,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '264565c38a8e63922e4-00350999',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'endKeys' => 0,
    'startKeys' => 0,
    'startDate' => 0,
    'id_lang' => 0,
    'calendar' => 0,
    'endDate' => 0,
    'weekTs' => 0,
    'showTime' => 0,
    'myownreservations' => 0,
    'resa' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5c38a8e63f0714_18231108',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5c38a8e63f0714_18231108')) {function content_5c38a8e63f0714_18231108($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include 'C:\\MAMP\\htdocs\\fuoriporta\\tools\\smarty\\plugins\\modifier.date_format.php';
?>
<table class="table" width="100%;" cellspacing="0" cellpadding="0" id="documents_table" style="background-color:#FFF;padding-bottom:6px;">
	<thead>
	<tr>
		<th style="width:50%;text-align:center;padding:0px"><?php echo smartyTranslate(array('s'=>'Start','mod'=>'myownreservations'),$_smarty_tpl);?>
</th>
		<th></th>
		<th style="width:50%;text-align:center;padding:0px"><?php echo smartyTranslate(array('s'=>'End','mod'=>'myownreservations'),$_smarty_tpl);?>
</th>
	</tr>
	</thead>
	<tbody>
		<tr class="odd">
			<td class="day_head"<?php if ($_smarty_tpl->tpl_vars['endKeys']->value['month']==$_smarty_tpl->tpl_vars['startKeys']->value['month']) {?>colspan="3"<?php }?>><?php echo $_smarty_tpl->tpl_vars['calendar']->value->getMonthName($_smarty_tpl->tpl_vars['startDate']->value,$_smarty_tpl->tpl_vars['id_lang']->value,true);?>
 <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['startDate']->value,"%Y");?>
</td>
			<?php if ($_smarty_tpl->tpl_vars['endKeys']->value['month']!=$_smarty_tpl->tpl_vars['startKeys']->value['month']) {?>
			<td></td>
			<td class="day_head"><?php echo $_smarty_tpl->tpl_vars['calendar']->value->getMonthName($_smarty_tpl->tpl_vars['endDate']->value,$_smarty_tpl->tpl_vars['id_lang']->value,true);?>
 <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['endDate']->value,"%Y");?>
</td>
			<?php }?>
		</tr>
		<tr>
			<td class="day_body" <?php if ($_smarty_tpl->tpl_vars['endKeys']->value['day']==$_smarty_tpl->tpl_vars['startKeys']->value['day']) {?>colspan="3"<?php }?>>
				<span class="dayName"><?php echo $_smarty_tpl->tpl_vars['calendar']->value->getDayString($_smarty_tpl->tpl_vars['startDate']->value,$_smarty_tpl->tpl_vars['id_lang']->value,true);?>
</span>
				<span class="dayNumber"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['startDate']->value,"%e");?>
</span>
			</td>
			<?php if ($_smarty_tpl->tpl_vars['endKeys']->value['day']!=$_smarty_tpl->tpl_vars['startKeys']->value['day']) {?>
			<td class="day_body" <?php if (!$_smarty_tpl->tpl_vars['weekTs']->value) {?>rowspan="2"<?php }?>>
				<div class="arrow"></div>
			</td>
			<td class="day_body">
				<span class="dayName"><?php echo $_smarty_tpl->tpl_vars['calendar']->value->getDayString($_smarty_tpl->tpl_vars['endDate']->value,$_smarty_tpl->tpl_vars['id_lang']->value,true);?>
</span>
				<span class="dayNumber"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['endDate']->value,"%e");?>
</span>
			</td>
			<?php }?>
		</tr>
		<?php if ($_smarty_tpl->tpl_vars['showTime']->value) {?>
		<tr>
			<td class="day_body" <?php if (($_smarty_tpl->tpl_vars['endKeys']->value['hour']==$_smarty_tpl->tpl_vars['startKeys']->value['hour']&&$_smarty_tpl->tpl_vars['endKeys']->value['day']==$_smarty_tpl->tpl_vars['startKeys']->value['day'])) {?>colspan="3"<?php }?>>
				<?php echo $_smarty_tpl->tpl_vars['resa']->value->getStartHour($_smarty_tpl->tpl_vars['myownreservations']->value);?>

			</td>
			<?php if (($_smarty_tpl->tpl_vars['endKeys']->value['hour']!=$_smarty_tpl->tpl_vars['startKeys']->value['hour']||$_smarty_tpl->tpl_vars['endKeys']->value['day']!=$_smarty_tpl->tpl_vars['startKeys']->value['day'])) {?>
				<?php if ($_smarty_tpl->tpl_vars['endKeys']->value['day']==$_smarty_tpl->tpl_vars['startKeys']->value['day']||$_smarty_tpl->tpl_vars['weekTs']->value) {?>
					<td class="day_body">
						<div class="arrow"></div>
					</td>
				<?php }?>
				<td class="day_body">
					<?php if ($_smarty_tpl->tpl_vars['weekTs']->value) {?><?php echo $_smarty_tpl->tpl_vars['resa']->value->getStartEndHour($_smarty_tpl->tpl_vars['myownreservations']->value);?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['resa']->value->getEndHour($_smarty_tpl->tpl_vars['myownreservations']->value);?>
<?php }?>
				</td>
			<?php }?>
		</tr>
		<?php }?>
	</tbody>
</table><?php }} ?>
