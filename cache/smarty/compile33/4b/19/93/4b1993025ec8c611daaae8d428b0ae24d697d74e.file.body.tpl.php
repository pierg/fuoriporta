<?php /* Smarty version Smarty-3.1.19, created on 2019-01-11 15:32:57
         compiled from "C:\MAMP\htdocs\fuoriporta\modules\gsnippetsreviews\views\templates\admin\review-tool\body.tpl" */ ?>
<?php /*%%SmartyHeaderCode:28515c38a919aa8cd2-04707259%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4b1993025ec8c611daaae8d428b0ae24d697d74e' => 
    array (
      0 => 'C:\\MAMP\\htdocs\\fuoriporta\\modules\\gsnippetsreviews\\views\\templates\\admin\\review-tool\\body.tpl',
      1 => 1547116567,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '28515c38a919aa8cd2-04707259',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'sModuleName' => 0,
    'sHeaderInclude' => 0,
    'sRatingJs' => 0,
    'sRatingCss' => 0,
    'aLangs' => 0,
    'k' => 0,
    'language' => 0,
    'iDefaultLang' => 0,
    'iCurrentLang' => 0,
    'bCountryRestriction' => 0,
    'sDocUri' => 0,
    'sDocName' => 0,
    'sCurrentIso' => 0,
    'sContactUs' => 0,
    'sRateUrl' => 0,
    'sModuleVersion' => 0,
    'bHideConfiguration' => 0,
    'sModerationInclude' => 0,
    'sLoader' => 0,
    'sReviewAddInclude' => 0,
    'sReviewsExportInclude' => 0,
    'aUpdateErrors' => 0,
    'aError' => 0,
    'bDisplayAdvice' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5c38a919b39fa3_70223889',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5c38a919b39fa3_70223889')) {function content_5c38a919b39fa3_70223889($_smarty_tpl) {?>

<div id="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sModuleName']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" class="bootstrap">
	
	<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['sHeaderInclude']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('bContentToDisplay'=>true), 0);?>

	

	<script type="text/javascript" src="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sRatingJs']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sRatingCss']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">

	<div class="clr_20"></div>

	<div>
		<img class="bt-effect image image-responsive" src="<?php echo mb_convert_encoding(htmlspecialchars(@constant('_GSR_URL_IMG'), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
admin/banner.png" width="350" height="60" alt="<?php echo smartyTranslate(array('s'=>'Customer Ratings and Reviews Pro + Google Rich Snippets','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
" />
	</div>

	<div class="clr_10"></div>

	
	<script>
		var languages = new Array();
		
		<?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['aLangs']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['language']->key;
?>
		
		languages[<?php echo intval($_smarty_tpl->tpl_vars['k']->value);?>
] = {
			id_lang: <?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
,
			iso_code: '<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['language']->value['iso_code'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
',
			name: '<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['language']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
',
			is_default: '<?php if ($_smarty_tpl->tpl_vars['iDefaultLang']->value==$_smarty_tpl->tpl_vars['language']->value['id_lang']) {?>1<?php } else { ?>0<?php }?>'
		};
		
		<?php } ?>
		
		// we need allowEmployeeFormLang var in ajax request
		allowEmployeeFormLang = <?php echo intval($_smarty_tpl->tpl_vars['iCurrentLang']->value);?>
;
		displayFlags(languages, id_language, allowEmployeeFormLang);
	</script>
	

	<div class="clr_20"></div>

	<div id="bt_block-tab">
		
		<div class="row">
			<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
				<div class="list-group workTabs">
					<a class="list-group-item active" id="tab-1"><span class="icon-star"></span>&nbsp;&nbsp;<?php echo smartyTranslate(array('s'=>'Moderation','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
</a>
					<?php if (empty($_smarty_tpl->tpl_vars['bCountryRestriction']->value)) {?><a class="list-group-item" id="tab-2"><span class="icon-pencil"></span>&nbsp;&nbsp;<?php echo smartyTranslate(array('s'=>'Add a review','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
</a><?php }?>
					<a class="list-group-item" id="tab-3"><span class="icon-upload"></span>&nbsp;&nbsp;<?php echo smartyTranslate(array('s'=>'Export your reviews','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
</a>
				</div>

				
				<div class="list-group">
					<a class="list-group-item documentation" target="_blank" href="<?php echo mb_convert_encoding(htmlspecialchars(@constant('_GSR_GOOGLE_SNIPPETS_TOOL'), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><span class="icon-file"></span>&nbsp;&nbsp;<?php echo smartyTranslate(array('s'=>'GOOGLE RICH SNIPPETS TEST TOOL','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
</a>
					<a class="list-group-item documentation" target="_blank" href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sDocUri']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sDocName']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><span class="icon-file"></span>&nbsp;&nbsp;<?php echo smartyTranslate(array('s'=>'Documentation','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
</a>
					<a class="list-group-item" target="_blank" href="<?php echo mb_convert_encoding(htmlspecialchars(@constant('_GSR_BT_FAQ_MAIN_URL'), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
?module=23&lg=<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sCurrentIso']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><span class="icon-info-circle"></span>&nbsp;&nbsp;<?php echo smartyTranslate(array('s'=>'Online FAQ','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
</a>
					<a class="list-group-item" target="_blank" href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sContactUs']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><span class="icon-ambulance"></span>&nbsp;&nbsp;<?php echo smartyTranslate(array('s'=>'Contact support','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
</a>
				</div>

				
				<div class="list-group">
					<a class="list-group-item" target="_blank" href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sRateUrl']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><i class="icon-star" style="color: #fbbb22;"></i>&nbsp;&nbsp;<?php echo smartyTranslate(array('s'=>'Rate me','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
</a>
				</div>

				
				<div class="list-group"">
				<a class="list-group-item" href="#"><span class="icon icon-info"></span>&nbsp;&nbsp;<?php echo smartyTranslate(array('s'=>'Version','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
 : <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sModuleVersion']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</a>
			</div>
		</div>
		

		
		<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
			<div class="tab-content">
				
				<?php if (empty($_smarty_tpl->tpl_vars['bHideConfiguration']->value)) {?>
					
					<div id="content-tab-1" class="tab-pane panel active">
						<div id="bt_settings-moderation">
							<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['sModerationInclude']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

						</div>
					</div>

					<div id="bt_loading-div-moderation" style="display: none;">
						<div class="alert alert-info">
							<p class="center"><img src="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sLoader']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" alt="Loading" /></p><div class="clr_20"></div>
							<p class="center"><?php echo smartyTranslate(array('s'=>'Reviews are in progress (Facebook wall posts and vouchers can take a long time ) ...','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
</p>
						</div>
					</div>
					

					
					<div id="content-tab-2" class="tab-pane panel">
						<div id="bt_settings-add">
							<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['sReviewAddInclude']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

						</div>
					</div>

					<div id="bt_loading-div-add" style="display: none;">
						<div class="alert alert-info">
							<p class="center"><img src="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sLoader']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" alt="Loading" /></p><div class="clr_20"></div>
							<p class="center"><?php echo smartyTranslate(array('s'=>'Your review is in progress. Do not forget that a review could already have been done for this customer and product','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
</p>
						</div>
					</div>
					

					
					<div id="content-tab-3" class="tab-pane panel">
						<div id="bt_review-export">
							<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['sReviewsExportInclude']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

						</div>
					</div>

					
						
							
							
						
					</div>
					
				
				<?php } elseif (!empty($_smarty_tpl->tpl_vars['aUpdateErrors']->value)) {?>
					<div id="content-tab-1111" class="tab-pane panel in active information">
						<div class="alert alert-danger">
							<?php  $_smarty_tpl->tpl_vars['aError'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['aError']->_loop = false;
 $_smarty_tpl->tpl_vars['nKey'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['aUpdateErrors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['aError']->key => $_smarty_tpl->tpl_vars['aError']->value) {
$_smarty_tpl->tpl_vars['aError']->_loop = true;
 $_smarty_tpl->tpl_vars['nKey']->value = $_smarty_tpl->tpl_vars['aError']->key;
?>
								<h3><?php echo smartyTranslate(array('s'=>'An error occured while SQL was executed for ','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
 <?php if (isset($_smarty_tpl->tpl_vars['aError']->value['table'])) {?><?php echo smartyTranslate(array('s'=>'table','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
 "<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['aError']->value['table'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" <?php } else { ?><?php echo smartyTranslate(array('s'=>'field','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
 "<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['aError']->value['field'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" <?php echo smartyTranslate(array('s'=>'in table','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
 "<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['aError']->value['linked'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"<?php }?> </h3>
								<ol>
									<li><?php echo smartyTranslate(array('s'=>'SQL file','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
 : <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['aError']->value['file'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</li>
								</ol>
							<?php } ?>
							<p><?php echo smartyTranslate(array('s'=>'Please reload this page for trying again to update SQL tables and fields or see with your web hosting why you\'ve got a SQL error','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>
.</p>
						</div>
					</div>
				<?php } else { ?>
					<div id="content-tab-1111" class="tab-pane panel in active information">
						<div class="alert alert-danger">
							<?php echo smartyTranslate(array('s'=>'The module\'s configuration will be available once you have deactivated the "Product comments" module by PrestaShop. There is a conflict between this module and ours, which prevents our module from working correctly on the front-office of your shop.','mod'=>'gsnippetsreviews'),$_smarty_tpl);?>

						</div>
					</div>
				<?php }?>
			</div>
		</div>
		
	</div>

	
	<script type="text/javascript">
		$(document).ready(function() {
			$('#content').removeClass('nobootstrap');
			$('#content').addClass('bootstrap');

			var sHash = $(location).attr('hash');
			if (sHash != null && sHash != '') {
				sHash = sHash.replace('#', '');
				$(".workTabs a[id='tab-1']").removeClass('active');
				$("#content-tab-1").hide();
				$(".workTabs a[id='tab-"+sHash+"']").addClass('active');
				$("#content-tab-"+sHash).show();
			}

			$(".workTabs a").click(function(e) {
				e.preventDefault();
				// currentId is the current workTabs id
				var currentId = $(".workTabs a.active").attr('id').substr(4);
				// id is the wanted workTabs id
				var id = $(this).attr('id').substr(4);

				if (id != currentId) {
					$(".workTabs a[id='tab-"+currentId+"']").removeClass('active');
					$("#content-tab-"+currentId).hide();
					$(".workTabs a[id='tab-"+id+"']").addClass('active');
					$("#content-tab-"+id).show();
				}
			});
			$(".workTabs a.active").click();

			$('.label-tooltip, .help-tooltip').tooltip();
			$('.dropdown-toggle').dropdown();
			<?php if (!empty($_smarty_tpl->tpl_vars['bDisplayAdvice']->value)) {?>
			$("a#bt_disp-advice").fancybox({
				'hideOnContentClick' : false
			});
			$('#bt_disp-advice').trigger('click');
			<?php }?>
		});
	</script>
	
</div><?php }} ?>
