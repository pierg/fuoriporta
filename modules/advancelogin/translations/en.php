<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{advancelogin}prestashop>advancelogin_dc62348be93995865446bfbdc91e7cbe'] = 'Advance Login';
$_MODULE['<{advancelogin}prestashop>advancelogin_1cbc3540c5f934d1b4626ddd7b8111c3'] = 'Adds a login pop up box when user clicks on sign in button';
$_MODULE['<{advancelogin}prestashop>advancelogin_876f23178c29dc2552c0b48bf23cd9bd'] = 'Are you sure you want to uninstall?';
$_MODULE['<{advancelogin}prestashop>advancelogin_0f40e8817b005044250943f57a21c5e7'] = 'No name provided';
$_MODULE['<{advancelogin}prestashop>advancelogin_b5b01dfdde77f64ac0170a013111e1c5'] = 'Backgorund image has been removed successfully.';
$_MODULE['<{advancelogin}prestashop>advancelogin_2dab1d7536f5362f7d1bcc2ed8d7fd67'] = 'Right column image has been removed successfully.';
$_MODULE['<{advancelogin}prestashop>advancelogin_8aa01656bffee53e12cbd18033235cd1'] = 'Configuration has been saved successfully but';
$_MODULE['<{advancelogin}prestashop>advancelogin_870d738267b04c7a015b0907dc9eab9d'] = 'Configuration has been saved successfully.';
$_MODULE['<{advancelogin}prestashop>advancelogin_52f4393e1b52ba63e27310ca92ba098c'] = 'General Settings';
$_MODULE['<{advancelogin}prestashop>advancelogin_c9d54fda1013c290a4ccc70423a2b62c'] = 'Facebook Settings';
$_MODULE['<{advancelogin}prestashop>advancelogin_ef9ca24726e6c9973bd49dbfc68b83ab'] = 'Google Plus Settings';
$_MODULE['<{advancelogin}prestashop>advancelogin_1cf124095a544c1503f322881f956017'] = 'Do not show';
$_MODULE['<{advancelogin}prestashop>advancelogin_06d1525b412a91cffbc1a01bd336b9e5'] = 'Small Buttons';
$_MODULE['<{advancelogin}prestashop>advancelogin_66a8ee8b9b86c753d0d8c9b6ff92ec71'] = 'Large Buttons';
$_MODULE['<{advancelogin}prestashop>advancelogin_2faec1f9f8cc7f8f40d521c4dd574f49'] = 'Enable';
$_MODULE['<{advancelogin}prestashop>advancelogin_bcfaccebf745acfd5e75351095a5394a'] = 'Disable';
$_MODULE['<{advancelogin}prestashop>advancelogin_e566fe9aef1502d69ccdbe28e1957535'] = 'Enable/Disable';
$_MODULE['<{advancelogin}prestashop>advancelogin_6e4c9d03c23dda2950a0d89ce01006ed'] = 'Enable/Disable this plugin';
$_MODULE['<{advancelogin}prestashop>advancelogin_f09dc975119f5bf24ae305c46e800c30'] = 'Show Button Type';
$_MODULE['<{advancelogin}prestashop>advancelogin_62672cfbec60eabafab7987834ab4c8a'] = 'Choose the size of login buttons i.e facebook and google login buttons';
$_MODULE['<{advancelogin}prestashop>advancelogin_abd809c9e3c1c08e97740f86b8ceabfb'] = 'Background Image';
$_MODULE['<{advancelogin}prestashop>advancelogin_34ebbeca872dad62af266560d2762104'] = 'Upload the background image of the login pop up';
$_MODULE['<{advancelogin}prestashop>advancelogin_3983d9c646f1d0fd987a2edb450ce649'] = 'Show Right Column';
$_MODULE['<{advancelogin}prestashop>advancelogin_eebd8c40c847bbd732c92caafa5b021a'] = 'Enable the right column';
$_MODULE['<{advancelogin}prestashop>advancelogin_c4b1813e7eac0be4749ac2f765f5008b'] = 'Right Column Image';
$_MODULE['<{advancelogin}prestashop>advancelogin_7cf9e4bf2e28a441b7a0385cb04ee0b0'] = 'Best size 300 * 300 pixels.';
$_MODULE['<{advancelogin}prestashop>advancelogin_98e76ce9d0250ca671f4d87cfd1261d4'] = 'Upload image for the right column in the login pop up';
$_MODULE['<{advancelogin}prestashop>advancelogin_e2c483e0ded711e9d0c7947443475c40'] = 'Enable Facebook Login';
$_MODULE['<{advancelogin}prestashop>advancelogin_479f360a69058262cfa745bd5a89b7b1'] = 'Enable/Disable Facebook Login';
$_MODULE['<{advancelogin}prestashop>advancelogin_466ff61f4367f43a1e2bf7656be3a022'] = 'Facebook App ID';
$_MODULE['<{advancelogin}prestashop>advancelogin_b8ff7144e38be57edd2a7b4292efdaeb'] = 'Enter the Facebook app id';
$_MODULE['<{advancelogin}prestashop>advancelogin_fd125f225efc3440c92a219e9b396543'] = 'Facebook App Secret';
$_MODULE['<{advancelogin}prestashop>advancelogin_caf270d44a450bc2a07314ba18a5e5d1'] = 'Enter the Facebook app secret';
$_MODULE['<{advancelogin}prestashop>advancelogin_c49247323a602fa42bebf0455dee3c7e'] = 'Enable Google Plus Login';
$_MODULE['<{advancelogin}prestashop>advancelogin_273bc6ec270411115487168434b1c414'] = 'Enable/Disable Google Plus Login';
$_MODULE['<{advancelogin}prestashop>advancelogin_2297b2a23767f79643d374a3043c2bef'] = 'Google Plus Client ID';
$_MODULE['<{advancelogin}prestashop>advancelogin_78540b059a4867e8823e7b6b92b28923'] = 'Enter the Google plus client id';
$_MODULE['<{advancelogin}prestashop>advancelogin_8875436e3670d15355b7c9733e28e8f7'] = 'Google Plus Client Secret';
$_MODULE['<{advancelogin}prestashop>advancelogin_6935d45587002fad3e22d54693c17972'] = 'Enter the Google plus client secret';
$_MODULE['<{advancelogin}prestashop>advancelogin_38fb7d24e0d60a048f540ecb18e13376'] = 'Save';
$_MODULE['<{advancelogin}prestashop>facebook_89abc3d6a6443bc9645974c55a58c3dc'] = 'Email already exist please choose another one';
$_MODULE['<{advancelogin}prestashop>login_96550f206ccd1c321be787e4f1875ed4'] = 'An email address required.';
$_MODULE['<{advancelogin}prestashop>login_e267e2be02cf3e29f4ba53b5d97cf78a'] = 'Invalid email address.';
$_MODULE['<{advancelogin}prestashop>login_20aedd1e6de4dcf8d115b5a7424c58d7'] = 'Password is required.';
$_MODULE['<{advancelogin}prestashop>login_52fb0a2528fcd83440ec0e3fcfa33777'] = 'Your username or password is incorrect.';
$_MODULE['<{advancelogin}prestashop>login_2f767da1d52b0d06bce20214216f65d1'] = 'Your account isn\'t available at this time, please contact us';
$_MODULE['<{advancelogin}prestashop>login_802b207b05fe5cea22e11b9db804b33d'] = 'Authentication failed.';
$_MODULE['<{advancelogin}prestashop>form_custom_cfa85620e4aaf328885bc9a3e0d91e0a'] = 'Upload image only';
$_MODULE['<{advancelogin}prestashop>form_custom_feacf0a2d212f22ca586042a960107a0'] = 'Are you sure you want to remove the image?';
$_MODULE['<{advancelogin}prestashop>view_custom_fc35ec973f5b3a16f0d4b009834f39a6'] = 'Remove Image';
$_MODULE['<{advancelogin}prestashop>view_custom_1357e394106c1adabf10aba7b3bbc3b3'] = 'Clear Image';
$_MODULE['<{advancelogin}prestashop>view_custom_4a5a98be4202e6653fe9e90e3d923bfd'] = 'Click here to get Facebook app id and app secret';
$_MODULE['<{advancelogin}prestashop>view_custom_c6499f9f8e4c0af4e7029f1d0f08fab4'] = 'Click here to get Google  client id and client secret';
$_MODULE['<{advancelogin}prestashop>view_custom_a847b8e2c121d2e9f96f1ba36a4e7564'] = 'Please upload right column file.';
$_MODULE['<{advancelogin}prestashop>view_custom_d521623dff1a395465f9435857540d74'] = 'Please enter First name.';
$_MODULE['<{advancelogin}prestashop>view_custom_089c0beccb82e57fb7c70a04bea7d101'] = 'First name cannot be greater than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_d8301848fbe047cfb5e0affd080ed866'] = 'First name cannot be less than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_5c303f7cf0338db1d736ee312e00c9a7'] = 'Please enter middle name.';
$_MODULE['<{advancelogin}prestashop>view_custom_a6497add420ff7e8203c05e5f7574cdf'] = 'Middle name cannot be greater than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_44b154cadc1eee3f513ce519afdb17b7'] = 'Middle name cannot be less than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_58f7a43f2c3bd7854d99fbbbc3fa4ae9'] = 'Only alphabets are allowed.';
$_MODULE['<{advancelogin}prestashop>view_custom_8a19ab16f00918305e0579ebc5bd1c77'] = 'Please enter Last name.';
$_MODULE['<{advancelogin}prestashop>view_custom_b08069298ecf4d3825861a0f91f97b19'] = 'Last name cannot be greater than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_18ce9e2a46c9fc0cc00899888093bc35'] = 'Last name cannot be less than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_8966db242c2515204001a1954f76edcc'] = 'Field should be alphanumeric.';
$_MODULE['<{advancelogin}prestashop>view_custom_aae99c4e07ec6ff254f193cdc38ab730'] = 'Please enter Password.';
$_MODULE['<{advancelogin}prestashop>view_custom_b1b1504e80cea97a363e912aa7e502ff'] = 'Password cannot be greater than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_094fee919e8597afae9685c857500941'] = 'Password cannot be less than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_7d5e03f6e38a7f6ddf3d2c6e177336be'] = 'Password should contain atleast 1 special character.';
$_MODULE['<{advancelogin}prestashop>view_custom_01ba0d838f96c9b1516d1c94fb6c2d02'] = 'Password should contain alphabets.';
$_MODULE['<{advancelogin}prestashop>view_custom_2c3ccf343387a027506b273de033f64c'] = 'Password should contain atleast 1 capital letter.';
$_MODULE['<{advancelogin}prestashop>view_custom_571cd36d25a396786dc7109ed98c5f3c'] = 'Password should contain atleast 1 small letter.';
$_MODULE['<{advancelogin}prestashop>view_custom_05d164742dbdd620bd5b2762bd86ca1c'] = 'Password should contain atleast 1 digit.';
$_MODULE['<{advancelogin}prestashop>view_custom_4d4683b716163f77425690c142b1d1d6'] = 'Field cannot be empty.';
$_MODULE['<{advancelogin}prestashop>view_custom_1eed9300a01b6f56798ce3aafac677a3'] = 'You can enter only numbers.';
$_MODULE['<{advancelogin}prestashop>view_custom_a37e23a7c00ae4b9a01bdd21698ca2c4'] = 'Number should be greater than 0.';
$_MODULE['<{advancelogin}prestashop>view_custom_ace25cb531f479f5f5923302e77f0353'] = 'Fields cannot be greater than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_89f56f9c87f5c823c58e97ba78c0e6cb'] = 'Please enter Email.';
$_MODULE['<{advancelogin}prestashop>view_custom_94b068a881985a98289ab641c7aac91a'] = 'Please enter a valid Email.';
$_MODULE['<{advancelogin}prestashop>view_custom_3a478a39c5c450c81cab408ae02d6a56'] = 'Please enter country name.';
$_MODULE['<{advancelogin}prestashop>view_custom_d86ea5738dd3c056ed09010b35d640fd'] = 'Country cannot be greater than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_f538f9a0db4fe325d3dd5ddfcadfbf7b'] = 'Country cannot be less than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_8dfa6847d44f8ca044279b6486280285'] = 'Please enter city name.';
$_MODULE['<{advancelogin}prestashop>view_custom_c9e5ff3e8799a379ea19bd25263db298'] = 'City cannot be greater than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_2e0c2d0bac055aebebd75c2250270279'] = 'City cannot be less than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_43b56347c6e866439e2f5737b359401a'] = 'Please enter state name.';
$_MODULE['<{advancelogin}prestashop>view_custom_62ad908527cc67bf2d788d7c76b58b5b'] = 'State cannot be greater than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_588ae50e1b8f2930a0e68f8707289bb2'] = 'State cannot be less than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_fa23b7ba28269f559b8cb6f8d51d6523'] = 'Please enter product name.';
$_MODULE['<{advancelogin}prestashop>view_custom_1d634b48b82f7e044011ca29721cc28b'] = 'Product cannot be greater than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_828978883160877276194ac033e758ba'] = 'Product cannot be less than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_60e2fe9d6312b3c5c45931c77c272bca'] = 'Please enter category name.';
$_MODULE['<{advancelogin}prestashop>view_custom_5d1b272f74294b0007c258d4a0bba2d0'] = 'Category cannot be greater than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_bd37b20216037e11e2100d50148db38a'] = 'Category cannot be less than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_416f09762489672c542c761a4cd9e095'] = 'Please enter zip code.';
$_MODULE['<{advancelogin}prestashop>view_custom_a91b6e46e76f188b37e2e2ac82b9eccd'] = 'Zip cannot be greater than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_b88ddbd1b1b95e9dfb5ee77b8b5d8c89'] = 'Zip cannot be less than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_e267d34972b3ab66b987fbad59fba513'] = 'Invalid date format.';
$_MODULE['<{advancelogin}prestashop>view_custom_c53880806a2b37a8b1ec932e5f13ebac'] = 'SKU cannot be greater than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_7644a7809de0b53af0b8f9879f0da508'] = 'SKU cannot be less than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_dec7bc82d8ee06aa0f2935f2943bb5c1'] = 'Invalid SKU format.';
$_MODULE['<{advancelogin}prestashop>view_custom_e72d59b595ca8e4195777efeb6b4cfa2'] = 'Please enter SKU.';
$_MODULE['<{advancelogin}prestashop>view_custom_dc08c38881cf47fec59c5d471c7fa61b'] = 'Number is not in the valid range.';
$_MODULE['<{advancelogin}prestashop>view_custom_fbb9b17d3f1907f80de455f50a12de00'] = 'Please enter address.';
$_MODULE['<{advancelogin}prestashop>view_custom_a626663a44b3f4b002d6f2c700c4d723'] = 'Address cannot be less than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_807389fb52da2bb90bae0ec8745d238a'] = 'Address cannot be greater than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_64183635c4d7401eff233985ab090943'] = 'Please enter company name.';
$_MODULE['<{advancelogin}prestashop>view_custom_1a950bdd2fe3eb0f9aba129fb7717cc6'] = 'Company name cannot be less than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_805dc8d156ab894b936228977f63f54a'] = 'Company name cannot be greater than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_dc9857cd9a0e305a71e0025a7eafdd46'] = 'Phone number is invalid.';
$_MODULE['<{advancelogin}prestashop>view_custom_5b873f84f973770104544b64f9d86ef1'] = 'Please enter phone number.';
$_MODULE['<{advancelogin}prestashop>view_custom_1e287b49737c8a4c36a72f5fa1d70a16'] = 'Phone number cannot be less than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_a9136fe0d3ebd0975701afe507345167'] = 'Phone number cannot be greater than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_f34ded741a19b6d9ab0de03affdbb49e'] = 'Please enter brand name.';
$_MODULE['<{advancelogin}prestashop>view_custom_e3646ffa0c2fdc432b7ef1b162e26435'] = 'Brand name cannot be greater than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_c83affa1543f12ec81b41f7dfa4d50a1'] = 'Brand name cannot be less than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_189f1ca24840ef1dfc435b967c6fc87c'] = 'Please enter Shimpment.';
$_MODULE['<{advancelogin}prestashop>view_custom_a3fd030504d69575f7c603fee9a428ba'] = 'Shipment cannot be greater than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_64e3bf561cd8aef65c02a03e53eb3190'] = 'Shipment cannot be less than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_ff9e9477381393a4d5dcea2caa8adbed'] = 'Invalid IP format.';
$_MODULE['<{advancelogin}prestashop>view_custom_bdc5009663f3e2063930c0549b851ac2'] = 'Invalid URL format.';
$_MODULE['<{advancelogin}prestashop>view_custom_ff9b7d125569e6eda2645f6200b341b7'] = 'Please enter URL.';
$_MODULE['<{advancelogin}prestashop>view_custom_1fccd0c5ed28465562293d9477eb90f2'] = 'Amount cannot be empty.';
$_MODULE['<{advancelogin}prestashop>view_custom_4e81784365ed92cd17313fa5adc622fd'] = 'Amount should be numeric.';
$_MODULE['<{advancelogin}prestashop>view_custom_465309cd8d6a4ab2f12c3e2183b1047e'] = 'Email cannot be greater than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_354d78658ab766c9ddfae258232d2bb6'] = 'Zip should not have special characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_ce088f61b0dfaf5d45fa22de60136287'] = 'SKU should not have special characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_c7a09df2e3d24f0e12b61c6c6e94bcc4'] = 'URL cannot be greater than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_6db46a02c1246138180288fb67fb37f7'] = 'Percentage should be in number.';
$_MODULE['<{advancelogin}prestashop>view_custom_5f2d4e949e2954b25785f81680e51366'] = 'Percentage should be between 0 and 100.';
$_MODULE['<{advancelogin}prestashop>view_custom_0050008e05eaffa27e9d53865bdc58bb'] = 'Size cannot be greater than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_33473fe40df48da5bb5934cdf3ce564d'] = 'Size should not have special characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_82dc90d46f51e7ee033d35b043552a11'] = 'UPC should not have special characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_6b18aeb30975d74591ce6ee7a21926ed'] = 'UPC cannot be greater than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_f17fb16efc94155950ccfe5a1ddb48bd'] = 'EAN should not have special characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_5986a527039802e28b7aaf6a40790178'] = 'EAN cannot be greater than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_fd552705a07b391a5e8d037a6ef0fcbb'] = 'Barcode should not have special characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_8b51eb9e614032894fce0b3035dbee97'] = 'Barcode cannot be greater than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_2c50a367917bb6481673d0999a02786c'] = 'Amount should be positive.';
$_MODULE['<{advancelogin}prestashop>view_custom_69ffa8c25953761e25958a9a6df29a15'] = 'Color could not be greater than #d characters.';
$_MODULE['<{advancelogin}prestashop>view_custom_237da44889cbf84e0922e3bf20fb1fff'] = 'Color is not valid.';
$_MODULE['<{advancelogin}prestashop>view_custom_2f2e0b5b2ba04159a1c124184253873e'] = 'Special characters are not allowed.';
$_MODULE['<{advancelogin}prestashop>view_custom_e3f06d5322c6cc889564fff64715b560'] = 'Script tags are not allowed.';
$_MODULE['<{advancelogin}prestashop>view_custom_0815f4569dd8c1fd65875b9fec802523'] = 'Style tags are not allowed.';
$_MODULE['<{advancelogin}prestashop>view_custom_5f234e61961ec4f209a313ed3474afd6'] = 'Iframe tags are not allowed.';
$_MODULE['<{advancelogin}prestashop>view_custom_c2041e583ffa720184e6221c862b0b9f'] = 'Uploaded file is not an image';
$_MODULE['<{advancelogin}prestashop>view_custom_e1d0bf13e59684e6d6ac5b74ec0edb72'] = 'Uploaded file size must be less than #d.';
$_MODULE['<{advancelogin}prestashop>view_custom_edc3075e80ffceb98f59131aa160142e'] = 'Field should not contain HTML tags.';
$_MODULE['<{advancelogin}prestashop>view_custom_d8966441915eebce97a1033bf11fcddb'] = 'You can enter only positive numbers.';
$_MODULE['<{advancelogin}prestashop>view_custom_9847da7a624493b87acb95f87d8174a0'] = 'Invalid comma (#d) separated values.';
$_MODULE['<{advancelogin}prestashop>advancelogin_4d4683b716163f77425690c142b1d1d6'] = 'Field cannot be empty.';
$_MODULE['<{advancelogin}prestashop>advancelogin_94b068a881985a98289ab641c7aac91a'] = 'Please enter a valid Email.';
$_MODULE['<{advancelogin}prestashop>advancelogin_c2041e583ffa720184e6221c862b0b9f'] = 'Uploaded file is not an image';
$_MODULE['<{advancelogin}prestashop>advancelogin_e1d0bf13e59684e6d6ac5b74ec0edb72'] = 'Uploaded file size must be less than #d.';
$_MODULE['<{advancelogin}prestashop>advancelogin_7a91d006bafae0d9f6a2a1d17e8ccc6d'] = 'LOG IN';
$_MODULE['<{advancelogin}prestashop>advancelogin_c3d8baf1b9da3d6922aea0057717a0b7'] = 'Enter email';
$_MODULE['<{advancelogin}prestashop>advancelogin_483bc24d842fe8f3ec3a6f92fbd31922'] = 'Enter password';
$_MODULE['<{advancelogin}prestashop>advancelogin_4976d7ca80f2cb54c1c7d5c87fb0c7ef'] = 'LOGIN';
$_MODULE['<{advancelogin}prestashop>advancelogin_38c8c1f4156fa91158ebbcee727da0b0'] = 'Forgot Password?';
$_MODULE['<{advancelogin}prestashop>advancelogin_f67dfd35ae7ceade0b629e0d4b0fc98e'] = 'Do not have an account?';
$_MODULE['<{advancelogin}prestashop>advancelogin_d9776f0775997b2e698c6975420b5c5d'] = 'Sign up';
$_MODULE['<{advancelogin}prestashop>advancelogin_96d33bac8f44311cee9917e88aaad3bd'] = 'Sign In Using Social Account';
$_MODULE['<{advancelogin}prestashop>advancelogin_d85544fce402c7a2a96a48078edaf203'] = 'Facebook';
$_MODULE['<{advancelogin}prestashop>advancelogin_8b36e9207c24c76e6719268e49201d94'] = 'Google';
$_MODULE['<{advancelogin}prestashop>advancelogin_716f6b30598ba30945d84485e61c1027'] = 'close';
$_MODULE['<{advancelogin}prestashop>advancelogin_55150d3433d81683ca720b679440b284'] = 'Sign In using social account';
