<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{advancelogin}prestashop>advancelogin_dc62348be93995865446bfbdc91e7cbe'] = "antecedência Acesso";
$_MODULE['<{advancelogin}prestashop>advancelogin_1cbc3540c5f934d1b4626ddd7b8111c3'] = "Adiciona um login pop-up caixa quando o usuário clica no sinal no botão";
$_MODULE['<{advancelogin}prestashop>advancelogin_876f23178c29dc2552c0b48bf23cd9bd'] = "Você tem certeza que quer desinstalar?";
$_MODULE['<{advancelogin}prestashop>advancelogin_0f40e8817b005044250943f57a21c5e7'] = "Sem nome fornecido";
$_MODULE['<{advancelogin}prestashop>advancelogin_b5b01dfdde77f64ac0170a013111e1c5'] = "imagem Backgorund foi removido com sucesso.";
$_MODULE['<{advancelogin}prestashop>advancelogin_2dab1d7536f5362f7d1bcc2ed8d7fd67'] = "imagem coluna da direita foi removido com sucesso.";
$_MODULE['<{advancelogin}prestashop>advancelogin_8aa01656bffee53e12cbd18033235cd1'] = "Configuração foi salva com sucesso, mas";
$_MODULE['<{advancelogin}prestashop>advancelogin_870d738267b04c7a015b0907dc9eab9d'] = "Configuração foi salva com sucesso.";
$_MODULE['<{advancelogin}prestashop>advancelogin_52f4393e1b52ba63e27310ca92ba098c'] = "Configurações Gerais";
$_MODULE['<{advancelogin}prestashop>advancelogin_c9d54fda1013c290a4ccc70423a2b62c'] = "Configurações Facebook";
$_MODULE['<{advancelogin}prestashop>advancelogin_ef9ca24726e6c9973bd49dbfc68b83ab'] = "Definições Google Plus";
$_MODULE['<{advancelogin}prestashop>advancelogin_1cf124095a544c1503f322881f956017'] = "Não mostre";
$_MODULE['<{advancelogin}prestashop>advancelogin_06d1525b412a91cffbc1a01bd336b9e5'] = "Botões pequenos";
$_MODULE['<{advancelogin}prestashop>advancelogin_66a8ee8b9b86c753d0d8c9b6ff92ec71'] = "botões grandes";
$_MODULE['<{advancelogin}prestashop>advancelogin_2faec1f9f8cc7f8f40d521c4dd574f49'] = "Habilitar";
$_MODULE['<{advancelogin}prestashop>advancelogin_bcfaccebf745acfd5e75351095a5394a'] = "incapacitar";
$_MODULE['<{advancelogin}prestashop>advancelogin_e566fe9aef1502d69ccdbe28e1957535'] = "Habilitar desabilitar";
$_MODULE['<{advancelogin}prestashop>advancelogin_6e4c9d03c23dda2950a0d89ce01006ed'] = "Ativar / desativar este plugin";
$_MODULE['<{advancelogin}prestashop>advancelogin_f09dc975119f5bf24ae305c46e800c30'] = "Mostrar Tipo Botão";
$_MODULE['<{advancelogin}prestashop>advancelogin_62672cfbec60eabafab7987834ab4c8a'] = "Escolha o tamanho dos botões de login, ou seja, facebook e google botões de login";
$_MODULE['<{advancelogin}prestashop>advancelogin_abd809c9e3c1c08e97740f86b8ceabfb'] = "Imagem de fundo";
$_MODULE['<{advancelogin}prestashop>advancelogin_34ebbeca872dad62af266560d2762104'] = "Carregar a imagem do login fundo pop-up";
$_MODULE['<{advancelogin}prestashop>advancelogin_3983d9c646f1d0fd987a2edb450ce649'] = "Mostrar coluna à direita";
$_MODULE['<{advancelogin}prestashop>advancelogin_eebd8c40c847bbd732c92caafa5b021a'] = "Ative a coluna direita";
$_MODULE['<{advancelogin}prestashop>advancelogin_c4b1813e7eac0be4749ac2f765f5008b'] = "Coluna direita Imagem";
$_MODULE['<{advancelogin}prestashop>advancelogin_7cf9e4bf2e28a441b7a0385cb04ee0b0'] = "Melhor tamanho de 300 * 300 pixels.";
$_MODULE['<{advancelogin}prestashop>advancelogin_98e76ce9d0250ca671f4d87cfd1261d4'] = "Fazer upload de imagem para a coluna da direita no login pop-up";
$_MODULE['<{advancelogin}prestashop>advancelogin_e2c483e0ded711e9d0c7947443475c40'] = "Ativar Facebook Login";
$_MODULE['<{advancelogin}prestashop>advancelogin_479f360a69058262cfa745bd5a89b7b1'] = "Activar / Desactivar Facebook Login";
$_MODULE['<{advancelogin}prestashop>advancelogin_466ff61f4367f43a1e2bf7656be3a022'] = "Facebook App ID";
$_MODULE['<{advancelogin}prestashop>advancelogin_b8ff7144e38be57edd2a7b4292efdaeb'] = "Digite o aplicativo do Facebook id";
$_MODULE['<{advancelogin}prestashop>advancelogin_fd125f225efc3440c92a219e9b396543'] = "Facebook App Segredo";
$_MODULE['<{advancelogin}prestashop>advancelogin_caf270d44a450bc2a07314ba18a5e5d1'] = "Digite o aplicativo do Facebook segredo";
$_MODULE['<{advancelogin}prestashop>advancelogin_c49247323a602fa42bebf0455dee3c7e'] = "Ativar Google Plus Acesso";
$_MODULE['<{advancelogin}prestashop>advancelogin_273bc6ec270411115487168434b1c414'] = "Activar / Desactivar Google Plus Acesso";
$_MODULE['<{advancelogin}prestashop>advancelogin_2297b2a23767f79643d374a3043c2bef'] = "Google Plus Cliente ID";
$_MODULE['<{advancelogin}prestashop>advancelogin_78540b059a4867e8823e7b6b92b28923'] = "Digite o Google mais cliente id";
$_MODULE['<{advancelogin}prestashop>advancelogin_8875436e3670d15355b7c9733e28e8f7'] = "Google Plus Cliente secreto";
$_MODULE['<{advancelogin}prestashop>advancelogin_6935d45587002fad3e22d54693c17972'] = "Digite o segredo Google mais cliente";
$_MODULE['<{advancelogin}prestashop>advancelogin_38fb7d24e0d60a048f540ecb18e13376'] = "Salve ";
$_MODULE['<{advancelogin}prestashop>facebook_89abc3d6a6443bc9645974c55a58c3dc'] = "E-mail já existe, por favor escolha outra";
$_MODULE['<{advancelogin}prestashop>login_96550f206ccd1c321be787e4f1875ed4'] = "Um endereço de e-mail necessário.";
$_MODULE['<{advancelogin}prestashop>login_e267e2be02cf3e29f4ba53b5d97cf78a'] = "Endereço de email invalido.";
$_MODULE['<{advancelogin}prestashop>login_20aedd1e6de4dcf8d115b5a7424c58d7'] = "Senha requerida.";
$_MODULE['<{advancelogin}prestashop>login_52fb0a2528fcd83440ec0e3fcfa33777'] = "O seu nome de usuário ou senha está incorreta.";
$_MODULE['<{advancelogin}prestashop>login_2f767da1d52b0d06bce20214216f65d1'] = "Seus isn conta \ 't disponível no momento, por favor, entre em contato conosco";
$_MODULE['<{advancelogin}prestashop>login_802b207b05fe5cea22e11b9db804b33d'] = "A autenticação falhou.";
$_MODULE['<{advancelogin}prestashop>form_custom_cfa85620e4aaf328885bc9a3e0d91e0a'] = "upload de imagem única";
$_MODULE['<{advancelogin}prestashop>form_custom_feacf0a2d212f22ca586042a960107a0'] = "Tem certeza de que deseja remover a imagem?";
$_MODULE['<{advancelogin}prestashop>view_custom_fc35ec973f5b3a16f0d4b009834f39a6'] = "Remover imagem";
$_MODULE['<{advancelogin}prestashop>view_custom_1357e394106c1adabf10aba7b3bbc3b3'] = "Limpar Image";
$_MODULE['<{advancelogin}prestashop>view_custom_4a5a98be4202e6653fe9e90e3d923bfd'] = "Clique aqui para obter Facebook ID de aplicativo e secreto aplicativo";
$_MODULE['<{advancelogin}prestashop>view_custom_c6499f9f8e4c0af4e7029f1d0f08fab4'] = "Clique aqui para obter ID de cliente do Google e segredo do cliente";
$_MODULE['<{advancelogin}prestashop>view_custom_a847b8e2c121d2e9f96f1ba36a4e7564'] = "Faça o upload de arquivos coluna da direita.";
$_MODULE['<{advancelogin}prestashop>view_custom_d521623dff1a395465f9435857540d74'] = "Por favor, indique primeiro nome.";
$_MODULE['<{advancelogin}prestashop>view_custom_089c0beccb82e57fb7c70a04bea7d101'] = "Nome não pode ser maior do que personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_d8301848fbe047cfb5e0affd080ed866'] = "Nome não pode ser inferior a personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_5c303f7cf0338db1d736ee312e00c9a7'] = "Por favor, indique o nome do meio.";
$_MODULE['<{advancelogin}prestashop>view_custom_a6497add420ff7e8203c05e5f7574cdf'] = "nome do meio não pode ser maior do que personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_44b154cadc1eee3f513ce519afdb17b7'] = "nome do meio não pode ser inferior a personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_58f7a43f2c3bd7854d99fbbbc3fa4ae9'] = "Somente alfabetos são permitidos.";
$_MODULE['<{advancelogin}prestashop>view_custom_8a19ab16f00918305e0579ebc5bd1c77'] = "Digite o sobrenome.";
$_MODULE['<{advancelogin}prestashop>view_custom_b08069298ecf4d3825861a0f91f97b19'] = "Último nome não pode ser maior do que personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_18ce9e2a46c9fc0cc00899888093bc35'] = "Último nome não pode ser inferior a personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_8966db242c2515204001a1954f76edcc'] = "O campo deve ser alfanumérico.";
$_MODULE['<{advancelogin}prestashop>view_custom_aae99c4e07ec6ff254f193cdc38ab730'] = "Digite a senha.";
$_MODULE['<{advancelogin}prestashop>view_custom_b1b1504e80cea97a363e912aa7e502ff'] = "A senha não pode ser maior do que personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_094fee919e8597afae9685c857500941'] = "A senha não pode ser inferior a personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_7d5e03f6e38a7f6ddf3d2c6e177336be'] = "A senha deve conter pelo menos um caractere especial.";
$_MODULE['<{advancelogin}prestashop>view_custom_01ba0d838f96c9b1516d1c94fb6c2d02'] = "A senha deve conter alfabetos.";
$_MODULE['<{advancelogin}prestashop>view_custom_2c3ccf343387a027506b273de033f64c'] = "A senha deve conter pelo menos carta 1 capital.";
$_MODULE['<{advancelogin}prestashop>view_custom_571cd36d25a396786dc7109ed98c5f3c'] = "A senha deve conter pelo menos 1 letra pequena.";
$_MODULE['<{advancelogin}prestashop>view_custom_05d164742dbdd620bd5b2762bd86ca1c'] = "A senha deve conter pelo menos 1 dígito.";
$_MODULE['<{advancelogin}prestashop>view_custom_4d4683b716163f77425690c142b1d1d6'] = "O campo não pode estar vazio.";
$_MODULE['<{advancelogin}prestashop>view_custom_1eed9300a01b6f56798ce3aafac677a3'] = "Você pode digitar apenas números.";
$_MODULE['<{advancelogin}prestashop>view_custom_a37e23a7c00ae4b9a01bdd21698ca2c4'] = "Número deve ser maior que 0.";
$_MODULE['<{advancelogin}prestashop>view_custom_ace25cb531f479f5f5923302e77f0353'] = "Campos não pode ser maior do que personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_89f56f9c87f5c823c58e97ba78c0e6cb'] = "Por favor, indique-mail.";
$_MODULE['<{advancelogin}prestashop>view_custom_94b068a881985a98289ab641c7aac91a'] = "Por favor digite um email válido.";
$_MODULE['<{advancelogin}prestashop>view_custom_3a478a39c5c450c81cab408ae02d6a56'] = "Por favor, indique o nome do país.";
$_MODULE['<{advancelogin}prestashop>view_custom_d86ea5738dd3c056ed09010b35d640fd'] = "País não pode ser maior do que personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_f538f9a0db4fe325d3dd5ddfcadfbf7b'] = "País não pode ser inferior a personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_8dfa6847d44f8ca044279b6486280285'] = "Por favor, insira o nome da cidade.";
$_MODULE['<{advancelogin}prestashop>view_custom_c9e5ff3e8799a379ea19bd25263db298'] = "Cidade não pode ser maior do que personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_2e0c2d0bac055aebebd75c2250270279'] = "Cidade não pode ser inferior a personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_43b56347c6e866439e2f5737b359401a'] = "Por favor, insira o nome do estado.";
$_MODULE['<{advancelogin}prestashop>view_custom_62ad908527cc67bf2d788d7c76b58b5b'] = "Estado não pode ser maior do que personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_588ae50e1b8f2930a0e68f8707289bb2'] = "Estado não pode ser inferior a personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_fa23b7ba28269f559b8cb6f8d51d6523'] = "Por favor, indique o nome do produto.";
$_MODULE['<{advancelogin}prestashop>view_custom_1d634b48b82f7e044011ca29721cc28b'] = "O produto não pode ser maior do que personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_828978883160877276194ac033e758ba'] = "O produto não pode ser inferior a personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_60e2fe9d6312b3c5c45931c77c272bca'] = "Por favor, indique o nome da categoria.";
$_MODULE['<{advancelogin}prestashop>view_custom_5d1b272f74294b0007c258d4a0bba2d0'] = "Categoria não pode ser maior do que personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_bd37b20216037e11e2100d50148db38a'] = "Categoria não pode ser inferior a personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_416f09762489672c542c761a4cd9e095'] = "Por favor, indique CEP.";
$_MODULE['<{advancelogin}prestashop>view_custom_a91b6e46e76f188b37e2e2ac82b9eccd'] = "Zip não pode ser maior do que personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_b88ddbd1b1b95e9dfb5ee77b8b5d8c89'] = "Zip não pode ser inferior a personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_e267d34972b3ab66b987fbad59fba513'] = "Formato de data inválido.";
$_MODULE['<{advancelogin}prestashop>view_custom_c53880806a2b37a8b1ec932e5f13ebac'] = "SKU não pode ser maior do que personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_7644a7809de0b53af0b8f9879f0da508'] = "SKU não pode ser inferior a personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_dec7bc82d8ee06aa0f2935f2943bb5c1'] = "formato SKU inválido.";
$_MODULE['<{advancelogin}prestashop>view_custom_e72d59b595ca8e4195777efeb6b4cfa2'] = "Por favor, indique SKU.";
$_MODULE['<{advancelogin}prestashop>view_custom_dc08c38881cf47fec59c5d471c7fa61b'] = "O número não está no intervalo válido.";
$_MODULE['<{advancelogin}prestashop>view_custom_fbb9b17d3f1907f80de455f50a12de00'] = "Por favor insira o endereço.";
$_MODULE['<{advancelogin}prestashop>view_custom_a626663a44b3f4b002d6f2c700c4d723'] = "Endereço não pode ser inferior a personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_807389fb52da2bb90bae0ec8745d238a'] = "Endereço não pode ser maior do que personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_64183635c4d7401eff233985ab090943'] = "Por favor, indique o nome da empresa.";
$_MODULE['<{advancelogin}prestashop>view_custom_1a950bdd2fe3eb0f9aba129fb7717cc6'] = "nome da empresa não pode ser inferior a personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_805dc8d156ab894b936228977f63f54a'] = "nome da empresa não pode ser maior do que personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_dc9857cd9a0e305a71e0025a7eafdd46'] = "número de telefone é inválido.";
$_MODULE['<{advancelogin}prestashop>view_custom_5b873f84f973770104544b64f9d86ef1'] = "Por favor, indique o número de telefone.";
$_MODULE['<{advancelogin}prestashop>view_custom_1e287b49737c8a4c36a72f5fa1d70a16'] = "número de telefone não pode ser inferior a personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_a9136fe0d3ebd0975701afe507345167'] = "número de telefone não pode ser maior do que personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_f34ded741a19b6d9ab0de03affdbb49e'] = "Por favor, insira o nome da marca.";
$_MODULE['<{advancelogin}prestashop>view_custom_e3646ffa0c2fdc432b7ef1b162e26435'] = "marca não pode ser maior do que personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_c83affa1543f12ec81b41f7dfa4d50a1'] = "marca não pode ser inferior a personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_189f1ca24840ef1dfc435b967c6fc87c'] = "Por favor, indique Shimpment.";
$_MODULE['<{advancelogin}prestashop>view_custom_a3fd030504d69575f7c603fee9a428ba'] = "Transferência não pode ser maior do que personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_64e3bf561cd8aef65c02a03e53eb3190'] = "Transferência não pode ser inferior a personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_ff9e9477381393a4d5dcea2caa8adbed'] = "formato IP inválido.";
$_MODULE['<{advancelogin}prestashop>view_custom_bdc5009663f3e2063930c0549b851ac2'] = "formato de URL inválido.";
$_MODULE['<{advancelogin}prestashop>view_custom_ff9b7d125569e6eda2645f6200b341b7'] = "Por favor, indique URL.";
$_MODULE['<{advancelogin}prestashop>view_custom_1fccd0c5ed28465562293d9477eb90f2'] = "O montante não pode estar vazio.";
$_MODULE['<{advancelogin}prestashop>view_custom_4e81784365ed92cd17313fa5adc622fd'] = "Valor deve ser numérico.";
$_MODULE['<{advancelogin}prestashop>view_custom_465309cd8d6a4ab2f12c3e2183b1047e'] = "Email não pode ser maior do que personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_354d78658ab766c9ddfae258232d2bb6'] = "Zip não deve ter caracteres especiais.";
$_MODULE['<{advancelogin}prestashop>view_custom_ce088f61b0dfaf5d45fa22de60136287'] = "SKU não deve ter caracteres especiais.";
$_MODULE['<{advancelogin}prestashop>view_custom_c7a09df2e3d24f0e12b61c6c6e94bcc4'] = "URL não pode ser maior do que personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_6db46a02c1246138180288fb67fb37f7'] = "Percentagem devem ser em número.";
$_MODULE['<{advancelogin}prestashop>view_custom_5f2d4e949e2954b25785f81680e51366'] = "Percentagem deve situar-se entre 0 e 100.";
$_MODULE['<{advancelogin}prestashop>view_custom_0050008e05eaffa27e9d53865bdc58bb'] = "Tamanho não pode ser maior do que personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_33473fe40df48da5bb5934cdf3ce564d'] = "Tamanho não deve ter caracteres especiais.";
$_MODULE['<{advancelogin}prestashop>view_custom_82dc90d46f51e7ee033d35b043552a11'] = "UPC não deve ter caracteres especiais.";
$_MODULE['<{advancelogin}prestashop>view_custom_6b18aeb30975d74591ce6ee7a21926ed'] = "UPC não pode ser maior do que personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_f17fb16efc94155950ccfe5a1ddb48bd'] = "EAN não deve ter caracteres especiais.";
$_MODULE['<{advancelogin}prestashop>view_custom_5986a527039802e28b7aaf6a40790178'] = "EAN não pode ser maior do que personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_fd552705a07b391a5e8d037a6ef0fcbb'] = "Código de barras não deve ter caracteres especiais.";
$_MODULE['<{advancelogin}prestashop>view_custom_8b51eb9e614032894fce0b3035dbee97'] = "Código de barras não pode ser maior do que personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_2c50a367917bb6481673d0999a02786c'] = "Montante deve ser positivo.";
$_MODULE['<{advancelogin}prestashop>view_custom_69ffa8c25953761e25958a9a6df29a15'] = "A cor não poderia ser maior do que personagens #D.";
$_MODULE['<{advancelogin}prestashop>view_custom_237da44889cbf84e0922e3bf20fb1fff'] = "A cor não é válido.";
$_MODULE['<{advancelogin}prestashop>view_custom_2f2e0b5b2ba04159a1c124184253873e'] = "Caracteres especiais não são permitidos.";
$_MODULE['<{advancelogin}prestashop>view_custom_e3f06d5322c6cc889564fff64715b560'] = "tags de script não são permitidos.";
$_MODULE['<{advancelogin}prestashop>view_custom_0815f4569dd8c1fd65875b9fec802523'] = "marcas de estilo não são permitidos.";
$_MODULE['<{advancelogin}prestashop>view_custom_5f234e61961ec4f209a313ed3474afd6'] = "tags Iframe não são permitidos.";
$_MODULE['<{advancelogin}prestashop>view_custom_c2041e583ffa720184e6221c862b0b9f'] = "O arquivo enviado não é uma imagem";
$_MODULE['<{advancelogin}prestashop>view_custom_e1d0bf13e59684e6d6ac5b74ec0edb72'] = "tamanho de arquivo deve ser inferior a # d.";
$_MODULE['<{advancelogin}prestashop>view_custom_edc3075e80ffceb98f59131aa160142e'] = "O campo não deve conter tags HTML.";
$_MODULE['<{advancelogin}prestashop>view_custom_d8966441915eebce97a1033bf11fcddb'] = "Você pode digitar apenas números positivos.";
$_MODULE['<{advancelogin}prestashop>view_custom_9847da7a624493b87acb95f87d8174a0'] = "vírgula inválido (#d) de valores separados.";
$_MODULE['<{advancelogin}prestashop>advancelogin_4d4683b716163f77425690c142b1d1d6'] = "O campo não pode estar vazio.";
$_MODULE['<{advancelogin}prestashop>advancelogin_94b068a881985a98289ab641c7aac91a'] = "Por favor digite um email válido.";
$_MODULE['<{advancelogin}prestashop>advancelogin_c2041e583ffa720184e6221c862b0b9f'] = "O arquivo enviado não é uma imagem";
$_MODULE['<{advancelogin}prestashop>advancelogin_e1d0bf13e59684e6d6ac5b74ec0edb72'] = "tamanho de arquivo deve ser inferior a # d.";
$_MODULE['<{advancelogin}prestashop>advancelogin_7a91d006bafae0d9f6a2a1d17e8ccc6d'] = "ENTRAR";
$_MODULE['<{advancelogin}prestashop>advancelogin_c3d8baf1b9da3d6922aea0057717a0b7'] = "Digite e-mail";
$_MODULE['<{advancelogin}prestashop>advancelogin_483bc24d842fe8f3ec3a6f92fbd31922'] = "Digite a senha";
$_MODULE['<{advancelogin}prestashop>advancelogin_4976d7ca80f2cb54c1c7d5c87fb0c7ef'] = "ENTRAR";
$_MODULE['<{advancelogin}prestashop>advancelogin_38c8c1f4156fa91158ebbcee727da0b0'] = "Esqueceu a senha?";
$_MODULE['<{advancelogin}prestashop>advancelogin_f67dfd35ae7ceade0b629e0d4b0fc98e'] = "Não possui uma conta?";
$_MODULE['<{advancelogin}prestashop>advancelogin_d9776f0775997b2e698c6975420b5c5d'] = "inscrever-se";
$_MODULE['<{advancelogin}prestashop>advancelogin_96d33bac8f44311cee9917e88aaad3bd'] = "Entrar usando conta Sociais";
$_MODULE['<{advancelogin}prestashop>advancelogin_d85544fce402c7a2a96a48078edaf203'] = "Facebook";
$_MODULE['<{advancelogin}prestashop>advancelogin_8b36e9207c24c76e6719268e49201d94'] = "Google";
$_MODULE['<{advancelogin}prestashop>advancelogin_716f6b30598ba30945d84485e61c1027'] = "fechar";
$_MODULE['<{advancelogin}prestashop>advancelogin_55150d3433d81683ca720b679440b284'] = "Entrar usando a conta sociais";
