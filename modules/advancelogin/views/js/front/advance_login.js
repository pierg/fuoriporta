/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * @author    knowband.com <support@knowband.com>
 * @copyright 2015 Knowband
 * @license   see file: LICENSE.txt
 * @category  PrestaShop Module
 */



$(document).ready(function(){
    $('#vss_login_popup').prependTo('#header > div.banner');
    $('#vss_login_popup').css('z-index','6000');
    // changes by rishabh jain
    if (is_tb == "1") {
        $("div.collapse ul.nav li.blockuserinfo a.login").attr('href', 'javascript:void(0)');
        $("div.collapse ul.nav li.blockuserinfo a.login").attr('id', 'modal_trigger');
        $("div.collapse ul.nav li.blockuserinfo a.login").attr('onclick', 'showLoginPopup()');
    } else {
        $("div.nav div.container div.row nav div.header_user_info a.login").attr('href', 'javascript:void(0)');
        $("div.nav div.container div.row nav div.header_user_info a.login").attr('id', 'modal_trigger');
        $("div.nav div.container div.row nav div.header_user_info a.login").attr('onclick', 'showLoginPopup()');
    }

});

function showLoginPopup()
{
    //$("#error_div").hide();
    $(".kb_error_message").remove();
    $('input[name="login_email"]').removeClass('kb_error_field');
    $('input[name="login_password"]').removeClass('kb_error_field');
    $('#vss_login_popup').show();
}
function closeLoginPopup()
{
    $('#vss_login_popup').hide();
    $("#useremail").val("")
    $("#userpwd").val("");
    $("#login_btn").css("margin-top","20px");
    $("#login_btn").attr("disabled",true);
    $(".login_error").css("display","none");
    $(".err_text").html("");
    $(".loading_block").hide();
    $("#login_btn").css("background-color","");
}
function enablelogin()
{
    $("#login_btn").css("margin-top","20px");
    //$(".login_small").css("width","64%");
    $("#login_btn").attr("disabled",false);
    if($("#useremail").val()=="")
    {
    $("#login_btn").css("margin-top","20px");
    //$(".login_small").css("width","64%");
    $("#login_btn").css("background-color","");
    $("#login_btn").attr("disabled",true);
    $(".login_error").css("display","none");
    $(".err_text").html("");
    }
}
function validate_entry()
{
    $(".kb_error_message").remove();
    $('input[name="login_email"]').removeClass('kb_error_field');
    $('input[name="login_password"]').removeClass('kb_error_field');
    /*Knowband validation start*/
    var error = false;
    var advance_email_err = velovalidation.checkMandatory($('input[name="login_email"]'));
    if (advance_email_err != true)
    {
        error = true;
        $('input[name="login_email"]').addClass('kb_error_field');
        $('input[name="login_email"]').after('<span class="kb_error_message">' + advance_email_err + '</span>');
        /*Knowband validation end*/
    } else {

        /*Knowband validation start*/
        var error = false;
        var advance_email_format_err = velovalidation.checkEmail($('input[name="login_email"]'));
        if (advance_email_format_err != true)
        {
            error = true;
            $('input[name="login_email"]').addClass('kb_error_field');
            $('input[name="login_email"]').after('<span class="kb_error_message">' + advance_email_format_err + '</span>');
        }
        /*Knowband validation end*/
    }
    /*Knowband validation start*/
    var login_password_err = velovalidation.checkMandatory($('input[name="login_password"]'));
    if (login_password_err != true)
    {
        error = true;
        $('input[name="login_password"]').addClass('kb_error_field');
        $('input[name="login_password"]').after('<span class="kb_error_message">' + login_password_err + '</span>');
    }
    if(error == true){
        return false;
    }
    /*Knowband button validation start*/
        $('#login_btn').attr('disabled', 'disabled');
    /*Knowband button validation end*/
    /*Knowband validation end*/
    $(".loading_block").show();
    var formData = $("#advance_login_form").serializeArray();
        $.ajax({
            url: loginaction+"&ajax=true",
            data: formData,
            type: "post",
            success:function(data){
                $('#login_btn').removeAttr('disabled');
                if(data!="")
                {
               $(".login_error").css("display","block");
               $(".err_text").html(data);
               $(".loading_block").hide();
                }
                else
                {
               location.reload();
                }
            }
        });
}
function change_color()
{
    $("#login_btn").css("background", ColorLuminance('#74d578', -0.2));
}
function change_color_normal()
{
    $("#login_btn").css("background",'#74d578');
}
function ColorLuminance(hex, lum) {
    // validate hex string
    hex = String(hex).replace(/[^0-9a-f]/gi, '');
    if (hex.length < 6) {
        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }
    lum = lum || 0;

    // convert to decimal and change luminosity
    var rgb = "#", c, i;
    for (i = 0; i < 3; i++) {
        c = parseInt(hex.substr(i * 2, 2), 16);
        c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
        rgb += ("00" + c).substr(c.length);
    }
    return rgb;
}
