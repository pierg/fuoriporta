/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * @category  PrestaShop Module
 * @author    knowband.com <support@knowband.com>
 * @copyright 2015 Knowband
 * @license   see file: LICENSE.txt
 */


$(document).ready(function()
{
    var banner_old1 = $('#bannerimage1').attr('src');
    var banner_old2 = $('#bannerimage2').attr('src');
    var src1 = $('#bannerimage1').attr('src').split('/');
    var file_banner1 = src1[src1.length - 1];
    var src2 = $('#bannerimage2').attr('src').split('/');
    var file_banner2 = src2[src2.length - 1];
    if(file_banner1 != 'default.jpg'){
        $("#file1-selectbutton").css('position','relative');  
        $("#file1-selectbutton").parent().append($('#vss-remove-bg-image'));
    } else {
        $("#remove-button1").hide();  
    }
    if(file_banner2 != 'default.jpg'){
    $("#file2-selectbutton").css('position','relative');  
    $("#file2-selectbutton").parent().append($('#vss-remove-rg-image'));
    } else {
        $("#remove-button2").hide();  
    }
    $(".panel-heading").after($('.vss-fb-link'));
    $(".panel-heading").after($('.vss-google-link'));
    $("[id^='fieldset'] h3").after($('.vss-google-link'));
    $("[id^='fieldset'] h3").after($('.vss-fb-link'));
    $('.vss-google-link').hide();
    $('.vss-fb-link').hide();
    if(enable_disable_right_column =='disable') {
       $("[name='file2']").parent().parent().parent().parent().hide();  
    } else {
       $("[name='file2']").parent().parent().parent().parent().show();  
    }
    $('#configuration_form').addClass('col-lg-10 col-md-9'); 
  
    $("[name='advance_login[enable]']").closest('.form-group ').show(); 
    $("[name='advance_login[facebook_status]']").closest('.form-group ').hide(); 
    $("[name='advance_login[google_plus_status]']").closest('.form-group ').hide(); 
    
    $('.optn_general').closest('.form-group ').show();
    $('.optn_facebook').closest('.form-group ').hide();
    $('.optn_google_plus').closest('.form-group ').hide();
    
    $("#remove-button1").bind('click', function() {
        if (confirm(confirmation_text))
        {
            $('.kb_error_message_file1').remove();
            $('input[id="file1-name"]').removeClass('kb_error_field');
             if ($.browser.msie) {
                    $('#file1-name').replaceWith($('#file1-name').clone());
             } else {
                    $('#file1-name').val('');
             }
             $('#file1').val('');
            $("#bannerimage1").attr("src", default_image);
            $("#previous_value1").val('1');
            $("#remove-button1").hide();  
//            $.ajax({
//                url: advance_login_action + '&configure=advancelogin&ajax=true&id=1',
//                type: 'post',
//                processData: false,
//                contentType: false,
//                success: function(msg) {
//                    $("#bannerimage1").attr("src", default_image);
//                    $(".row").prepend(msg);
//                },
//            });
        }
        else
        {
            return false;
        }
    });
    $("#clear-button1").bind('click', function() {
        $('#file1-name').removeClass('kb_error_field');
        $('.kb_error_message_file1').hide();
        $('#bannerimage1').attr('src', banner_old1);
        if ($.browser.msie) {
                $('#file1-name').replaceWith($('#file1-name').clone());
        } else {
                $('#file1-name').val('');
        }
        $('#file1').val('');
        var img1 = $('#bannerimage1').attr('src').split('/');
        var img_banner1 = img1[img1.length - 1];
        if(img_banner1 != 'default.jpg'){
            $("#clear-button1").hide();  
            $("#remove-button1").show();  
        } else {
            $("#clear-button1").hide();  
        }
    });
    $("#clear-button2").bind('click', function() {
        $('#file2-name').removeClass('kb_error_field');
        $('.kb_error_message_file2').hide();
        $('#bannerimage2').attr('src', banner_old2);
        if ($.browser.msie) {
                $('#file2-name').replaceWith($('#file2-name').clone());
        } else {
                $('#file2-name').val('');
        }
        $('#file2').val('');
        var img2 = $('#bannerimage2').attr('src').split('/');
        var img_banner2 = img2[img2.length - 1];
        if (img_banner2 != 'default.jpg') {
            $("#clear-button2").hide();  
            $("#remove-button2").show();  
        } else {
            $("#clear-button2").hide();  
        }
    });
    $("#remove-button2").bind('click', function() {
        if (confirm(confirmation_text))
        {
            $('.kb_error_message_file2').remove();
            $('input[id="file2-name"]').removeClass('kb_error_field');
             if ($.browser.msie) {
                    $('#file2-name').replaceWith($('#file2-name').clone());
             } else {
                    $('#file2-name').val('');
             }
             $('#file2').val('');
             $("#bannerimage2").attr("src", default_image);
             $("#previous_value2").val('1');
             $("#remove-button2").hide();  
//            $.ajax({
//                url: advance_login_action + '&&configure=advancelogin&ajax=true&id=2',
//                type: 'post',
//                processData: false,
//                contentType: false,
//                success: function(msg) {
//                    $("#bannerimage2").attr("src", default_image);
//                    $(".row").prepend(msg);
//                },
//            });
        }
        else
        {
            return false;
        }
    });
    $("#enable_right_column").bind('click', function() {
        $("#right_column_table").attr("style", " ")
    });
    $("#disable_right_column").bind('click', function() {
        $("#right_column_table").css("display", "none")
    });
    $("#file1").change(function() {
        $('#file1-name').removeClass('kb_error_field');
        $('.kb_error_message_file1').hide();
        var imgPath = $(this)[0].value;
        /*Knowband image validation start*/
        if($('input[name="file1"]').val() != ''){
            var image_err = velovalidation.checkImage($('input[name="file1"]'));
            if (image_err != true)
            {
                $('#file1-name').addClass('kb_error_field');
                $('input[name="file1"]').parent().append('<span class="kb_error_message_file1">' + image_err + '</span>');
                $("#file1-selectbutton").css('position','relative');  
                $("#file1-selectbutton").parent().append($('#vss-clear-bg-image'));
                $("#clear-button1").show();  
                $("#remove-button1").hide();  
            }else{
                var image_holder = $("#bannerimage1");

                image_holder.empty();

                var reader = new FileReader();
                reader.onload = function(e) {

                    $('#bannerimage1').attr('src', e.target.result);
                }
                image_holder.show();
                reader.readAsDataURL($(this)[0].files[0]);
                $("#file1-selectbutton").css('position','relative');  
                $("#file1-selectbutton").parent().append($('#vss-clear-bg-image'));
                $("#clear-button1").show();  
                $("#remove-button1").hide();  
                
            }
            
        }
        /*Knowband image validation end*/
//        var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
//
//        if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
//            if (typeof (FileReader) != "undefined") {
//
//                var image_holder = $("#bannerimage1");
//
//                image_holder.empty();
//
//                var reader = new FileReader();
//                reader.onload = function(e) {
//
//                    $('#bannerimage1').attr('src', e.target.result);
//                }
//                image_holder.show();
//                reader.readAsDataURL($(this)[0].files[0]);
//            }
//        }
//        else
//        {
//            alert(msg);
//        }
    });
    $("#file2").change(function() {
        $('#file2-name').removeClass('kb_error_field');
        $('.kb_error_message_file2').hide();
        var imgPath = $(this)[0].value;
        /*Knowband image validation start*/
        if($('input[name="file2"]').val() != ''){
            var image_err = velovalidation.checkImage($('input[name="file2"]'));
            if (image_err != true)
            {
                $('#file2-name').addClass('kb_error_field');
                $('input[name="file2"]').parent().append('<span class="kb_error_message_file2">' + image_err + '</span>');
                $("#file2-selectbutton").css('position','relative');  
                $("#file2-selectbutton").parent().append($('#vss-clear-rg-image'));
                $("#clear-button2").show();  
                $("#remove-button2").hide(); 
            }else{
                var image_holder = $("#bannerimage2");

                image_holder.empty();

                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#bannerimage2').attr('src', e.target.result);
                }
                image_holder.show();
                reader.readAsDataURL($(this)[0].files[0]);
                $("#file2-selectbutton").css('position','relative');  
                $("#file2-selectbutton").parent().append($('#vss-clear-rg-image'));
                $("#clear-button2").show();  
                $("#remove-button2").hide(); 
            }
            
        }
        /*Knowband image validation end*/
//        var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
//
//        if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
//            if (typeof (FileReader) != "undefined") {
//
//                var image_holder = $("#bannerimage2");
//
//                image_holder.empty();
//
//                var reader = new FileReader();
//                reader.onload = function(e) {
//
//                    $('#bannerimage2').attr('src', e.target.result);
//                }
//                image_holder.show();
//                reader.readAsDataURL($(this)[0].files[0]);
//            }
//        }
//        else
//        {
//           alert(msg);
//        }
    });
    $('#save_post_setting').bind('click', function() {
        var flag = 0;
        if (flag == 0)
        {
            $('#advance_login_form').submit();
        }
    });
    
    $('.advancesave').click(function() {
        var error = false;
        $(".kb_error_message").remove();

        $('input[name="advance_login[facebook_app_id]"]').removeClass('kb_error_field');
        $('input[name="advance_login[facebook_app_secret]"]').removeClass('kb_error_field');
        $('input[name="advance_login[google_plus_client_id]"]').removeClass('kb_error_field');
        $('input[name="advance_login[google_plus_client_secret]"]').removeClass('kb_error_field');
        $('.kb_error_message_file1').remove();
        $('input[name="file1"]').removeClass('kb_error_field');
        $('.kb_error_message_file2').remove();
        $('input[name="file2"]').removeClass('kb_error_field');

        /*Knowband image validation start*/
        if($('input[name="file1"]').val() != ''){
            var image_err = velovalidation.checkImage($('input[name="file1"]'));
            if (image_err != true)
            {
                error = true;   
                $('#file1-name').addClass('kb_error_field');
                $('input[name="file1"]').parent().append('<span class="kb_error_message_file1">' + image_err + '</span>');
            }
        }
        /*Knowband image validation end*/
        /*Knowband image validation start*/
        if($('input[name="file2"]').val() != ''){
            var image_err = velovalidation.checkImage($('input[name="file2"]'));
            if (image_err != true)
            {
                error = true;   
                $('#file2-name').addClass('kb_error_field');
                $('input[name="file2"]').parent().append('<span class="kb_error_message_file2">' + image_err + '</span>');
            }
        }
        /*Knowband image validation end*/
        if($('select[name="advance_login[show_right_column]"]').val() == 'enable'){
            var src_2 = $('#bannerimage2').attr('src').split('/');
            var file_banner_2 = src_2[src_2.length - 1];
            if(file_banner_2 == 'default.jpg'){
                error = true;   
                $('#file2-name').addClass('kb_error_field');
                $('input[name="file2"]').parent().append('<span class="kb_error_message_file2">' + nofileerror + '</span>');
            }
        }
        if ($("[name='advance_login[facebook_status]']:checked").val() == 1) {
            /*Knowband validation start*/
            var facebook_app_id_err = velovalidation.checkMandatory($('input[name="advance_login[facebook_app_id]"]'));
            if (facebook_app_id_err != true)
            {
                error = true;
                $('input[name="advance_login[facebook_app_id]"]').addClass('kb_error_field');
                $('input[name="advance_login[facebook_app_id]"]').after('<span class="kb_error_message">' + facebook_app_id_err + '</span>');
            }
            /*Knowband validation end*/
            /*Knowband validation start*/
            var facebook_app_secret_err = velovalidation.checkMandatory($('input[name="advance_login[facebook_app_secret]"]'));
            if (facebook_app_secret_err != true)
            {
                error = true;
                $('input[name="advance_login[facebook_app_secret]"]').addClass('kb_error_field');
                $('input[name="advance_login[facebook_app_secret]"]').after('<span class="kb_error_message">' + facebook_app_secret_err + '</span>');
            }
            /*Knowband validation end*/
        } else {
            /*Knowband validation start*/
            var facebook_app_id__tag_err = velovalidation.checkTags($('input[name="advance_login[facebook_app_id]"]'));
            if (facebook_app_id__tag_err != true)
            {
                error = true;
                $('input[name="advance_login[facebook_app_id]"]').addClass('kb_error_field');
                $('input[name="advance_login[facebook_app_id]"]').after('<span class="kb_error_message">' + facebook_app_id__tag_err + '</span>');
            }
            /*Knowband validation end*/
            /*Knowband validation start*/
            var facebook_app_secret__tag_err = velovalidation.checkTags($('input[name="advance_login[facebook_app_secret]"]'));
            if (facebook_app_secret__tag_err != true)
            {
                error = true;
                $('input[name="advance_login[facebook_app_secret]"]').addClass('kb_error_field');
                $('input[name="advance_login[facebook_app_secret]"]').after('<span class="kb_error_message">' + facebook_app_secret__tag_err + '</span>');
            }
            /*Knowband validation end*/
        }
        if ($("[name='advance_login[google_plus_status]']:checked").val() == 1) {
            /*Knowband validation start*/
            var google_plus_client_id_err = velovalidation.checkMandatory($('input[name="advance_login[google_plus_client_id]"]'));
            if (google_plus_client_id_err != true)
            {
                error = true;
                $('input[name="advance_login[google_plus_client_id]"]').addClass('kb_error_field');
                $('input[name="advance_login[google_plus_client_id]"]').after('<span class="kb_error_message">' + google_plus_client_id_err + '</span>');
            }
            /*Knowband validation end*/
            /*Knowband validation start*/
            var google_plus_client_secret_err = velovalidation.checkMandatory($('input[name="advance_login[google_plus_client_secret]"]'));
            if (google_plus_client_secret_err != true)
            {
                error = true;
                $('input[name="advance_login[google_plus_client_secret]"]').addClass('kb_error_field');
                $('input[name="advance_login[google_plus_client_secret]"]').after('<span class="kb_error_message">' + google_plus_client_secret_err + '</span>');
            }
            /*Knowband validation end*/
        } else {
            /*Knowband validation start*/
            var google_plus_client_id__tags_err = velovalidation.checkTags($('input[name="advance_login[google_plus_client_id]"]'));
            if (google_plus_client_id__tags_err != true)
            {
                error = true;
                $('input[name="advance_login[google_plus_client_id]"]').addClass('kb_error_field');
                $('input[name="advance_login[google_plus_client_id]"]').after('<span class="kb_error_message">' + google_plus_client_id__tags_err + '</span>');
            }
            /*Knowband validation end*/
            /*Knowband validation start*/
            var google_plus_client_secret_tags_err = velovalidation.checkTags($('input[name="advance_login[google_plus_client_secret]"]'));
            if (google_plus_client_secret_tags_err != true)
            {
                error = true;
                $('input[name="advance_login[google_plus_client_secret]"]').addClass('kb_error_field');
                $('input[name="advance_login[google_plus_client_secret]"]').after('<span class="kb_error_message">' + google_plus_client_secret_tags_err + '</span>');
            }
            /*Knowband validation end*/
        }
        if (error) {
            return false;
        }
        /*Knowband button validation start*/
        $('.advancesave').attr('disabled','disabled');
        /*Knowband button validation end*/
        $('#configuration_form').submit();
    });
});

function change_tab(a,b)
{
    if(b == 1){
        $("[id^='fieldset'] h3").html(general_settings);
        $(".panel-heading").html(general_settings);
        
        $('.optn_general').closest('.form-group ').show();
        $('.optn_facebook').closest('.form-group ').hide();
        $('.optn_google_plus').closest('.form-group ').hide();
        $('.vss-google-link').hide();
        $('.vss-fb-link').hide();
        
        $("[name='advance_login[enable]']").closest('.form-group ').show(); 
        $("[name='advance_login[facebook_status]']").closest('.form-group ').hide(); 
        $("[name='advance_login[google_plus_status]']").closest('.form-group ').hide();
        $("[name='file1']").parent().parent().parent().parent().show(); 
            if($("[name='advance_login[show_right_column]']").val() == 'disable') {
                  $("[name='file2']").parent().parent().parent().parent().hide();  
            } else {
                  $("[name='file2']").parent().parent().parent().parent().show();  
            }
    }
    if(b==2)
    {
        $("[id^='fieldset'] h3").html(facebook_settings);
        $(".panel-heading").html(facebook_settings);
        
        $('.optn_facebook').closest('.form-group ').show();
        $('.optn_general').closest('.form-group ').hide();  
        $('.optn_google_plus').closest('.form-group ').hide();
        $('.vss-google-link').hide();
        $('.vss-fb-link').show();
        
        $("[name='advance_login[enable]']").closest('.form-group ').hide(); 
        $("[name='advance_login[facebook_status]']").closest('.form-group ').show(); 
        $("[name='advance_login[google_plus_status]']").closest('.form-group ').hide(); 
        $("[name='file1']").parent().parent().parent().parent().hide(); 
        $("[name='file2']").parent().parent().parent().parent().hide(); 
    }
    if(b==3)
    {
        $("[id^='fieldset'] h3").html(google_settings);
        $(".panel-heading").html(google_settings);
        
        $('.optn_facebook').closest('.form-group ').hide();
        $('.optn_general').closest('.form-group ').hide();
        $('.optn_google_plus').closest('.form-group ').show();
        $('.vss-google-link').show();
        $('.vss-fb-link').hide();
        
        $("[name='advance_login[enable]']").closest('.form-group ').hide(); 
        $("[name='advance_login[facebook_status]']").closest('.form-group ').hide(); 
        $("[name='advance_login[google_plus_status]']").closest('.form-group ').show(); 
        $("[name='file1']").parent().parent().parent().parent().hide(); 
        $("[name='file2']").parent().parent().parent().parent().hide(); 
    }
    $('.list-group-item').attr('class','list-group-item');
    $(a).attr('class','list-group-item active');
}

function enable_right_column(a)
{
   if($(a).val()=='enable'){
       $("[name='file2']").parent().parent().parent().parent().show(); 
   } else {
       $("[name='file2']").parent().parent().parent().parent().hide(); 
   }
}