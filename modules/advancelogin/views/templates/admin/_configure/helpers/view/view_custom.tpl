<span id='vss-remove-bg-image'>
    <button id="remove-button1" type="button" name="submitAddAttachments" class="btn btn-default">
        <i class="icon-trash vss-icon"></i>{l s='Remove Image' mod='advancelogin'}</button>
</span>
<span id='vss-remove-rg-image'>
    <button id="remove-button2" type="button" name="submitAddAttachments" class="btn btn-default">
        <i class="icon-trash vss-icon"></i>{l s='Remove Image' mod='advancelogin'}</button>
</span>
<span id='vss-clear-bg-image'>
    <button id="clear-button1" type="button" name="ClearAttachment1" class="btn btn-default">
        <i class="icon-trash vss-icon"></i>{l s='Clear Image' mod='advancelogin'}</button>
</span>
<span id='vss-clear-rg-image'>
    <button id="clear-button2" type="button" name="ClearAttachment2" class="btn btn-default">
        <i class="icon-trash vss-icon"></i>{l s='Clear Image' mod='advancelogin'}</button>
</span>
<span class="vss-fb-link" >
    <a href="https://developers.facebook.com/apps/" target="_blank">{l s='Click here to get Facebook app id and app secret' mod='advancelogin'}</a>
</span>



<span class="vss-google-link">
    <a href="https://console.developers.google.com/project" target="_blank">{l s='Click here to get Google  client id and client secret' mod='advancelogin'}</a>
</span>

<script type="text/javascript"> 
var nofileerror = "{l s='Please upload right column file.' mod='advancelogin'}";
//error messages for velovalidation.js
velovalidation.setErrorLanguage({
    empty_fname: "{l s='Please enter First name.' mod='advancelogin'}",
    maxchar_fname: "{l s='First name cannot be greater than #d characters.' mod='advancelogin'}",
    minchar_fname: "{l s='First name cannot be less than #d characters.'  mod='advancelogin'}",
    empty_mname: "{l s='Please enter middle name.' mod='advancelogin'}",
    maxchar_mname: "{l s='Middle name cannot be greater than #d characters.' mod='advancelogin'}",
    minchar_mname: "{l s='Middle name cannot be less than #d characters.' mod='advancelogin'}",
    only_alphabet: "{l s='Only alphabets are allowed.' mod='advancelogin'}",
    empty_lname: "{l s='Please enter Last name.' mod='advancelogin'}",
    maxchar_lname: "{l s='Last name cannot be greater than #d characters.' mod='advancelogin'}",
    minchar_lname: "{l s='Last name cannot be less than #d characters.' mod='advancelogin'}",
    alphanumeric: "{l s='Field should be alphanumeric.' mod='advancelogin'}",
    empty_pass: "{l s='Please enter Password.' mod='advancelogin'}",
    maxchar_pass: "{l s='Password cannot be greater than #d characters.' mod='advancelogin'}",
    minchar_pass: "{l s='Password cannot be less than #d characters.' mod='advancelogin'}",
    specialchar_pass: "{l s='Password should contain atleast 1 special character.' mod='advancelogin'}",
    alphabets_pass: "{l s='Password should contain alphabets.' mod='advancelogin'}",
    capital_alphabets_pass: "{l s='Password should contain atleast 1 capital letter.' mod='advancelogin'}",
    small_alphabets_pass: "{l s='Password should contain atleast 1 small letter.' mod='advancelogin'}",
    digit_pass: "{l s='Password should contain atleast 1 digit.' mod='advancelogin'}",
    empty_field: "{l s='Field cannot be empty.' mod='advancelogin'}",
    number_field: "{l s='You can enter only numbers.' mod='advancelogin'}",
    positive_number: "{l s='Number should be greater than 0.' mod='advancelogin'}",
    maxchar_field: "{l s='Fields cannot be greater than #d characters.' mod='advancelogin'}",
    minchar_field: "{l s='Fields cannot be greater than #d characters.' mod='advancelogin'}",
    empty_email: "{l s='Please enter Email.' mod='advancelogin'}",
    validate_email: "{l s='Please enter a valid Email.' mod='advancelogin'}",
    empty_country: "{l s='Please enter country name.' mod='advancelogin'}",
    maxchar_country: "{l s='Country cannot be greater than #d characters.' mod='advancelogin'}",
    minchar_country: "{l s='Country cannot be less than #d characters.' mod='advancelogin'}",
    empty_city: "{l s='Please enter city name.' mod='advancelogin'}",
    maxchar_city: "{l s='City cannot be greater than #d characters.' mod='advancelogin'}",
    minchar_city: "{l s='City cannot be less than #d characters.' mod='advancelogin'}",
    empty_state: "{l s='Please enter state name.' mod='advancelogin'}",
    maxchar_state: "{l s='State cannot be greater than #d characters.' mod='advancelogin'}",
    minchar_state: "{l s='State cannot be less than #d characters.' mod='advancelogin'}",
    empty_proname: "{l s='Please enter product name.' mod='advancelogin'}",
    maxchar_proname: "{l s='Product cannot be greater than #d characters.' mod='advancelogin'}",
    minchar_proname: "{l s='Product cannot be less than #d characters.' mod='advancelogin'}",
    empty_catname: "{l s='Please enter category name.' mod='advancelogin'}",
    maxchar_catname: "{l s='Category cannot be greater than #d characters.' mod='advancelogin'}",
    minchar_catname: "{l s='Category cannot be less than #d characters.' mod='advancelogin'}",
    empty_zip: "{l s='Please enter zip code.' mod='advancelogin'}",
    maxchar_zip: "{l s='Zip cannot be greater than #d characters.' mod='advancelogin'}",
    minchar_zip: "{l s='Zip cannot be less than #d characters.' mod='advancelogin'}",
    empty_username: "{l s='Please enter zip code.' mod='advancelogin'}",
    maxchar_username: "{l s='Zip cannot be greater than #d characters.' mod='advancelogin'}",
    minchar_username: "{l s='Zip cannot be less than #d characters.' mod='advancelogin'}",
    invalid_date: "{l s='Invalid date format.' mod='advancelogin'}",
    maxchar_sku: "{l s='SKU cannot be greater than #d characters.' mod='advancelogin'}",
    minchar_sku: "{l s='SKU cannot be less than #d characters.' mod='advancelogin'}",
    invalid_sku: "{l s='Invalid SKU format.' mod='advancelogin'}",
    empty_sku: "{l s='Please enter SKU.' mod='advancelogin'}",
    validate_range: "{l s='Number is not in the valid range.' mod='advancelogin'}",
    empty_address: "{l s='Please enter address.' mod='advancelogin'}",
    minchar_address: "{l s='Address cannot be less than #d characters.' mod='advancelogin'}",
    maxchar_address: "{l s='Address cannot be greater than #d characters.' mod='advancelogin'}",
    empty_company: "{l s='Please enter company name.' mod='advancelogin'}",
    minchar_company: "{l s='Company name cannot be less than #d characters.' mod='advancelogin'}",
    maxchar_company: "{l s='Company name cannot be greater than #d characters.' mod='advancelogin'}",
    invalid_phone: "{l s='Phone number is invalid.' mod='advancelogin'}",
    empty_phone: "{l s='Please enter phone number.' mod='advancelogin'}",
    minchar_phone: "{l s='Phone number cannot be less than #d characters.' mod='advancelogin'}",
    maxchar_phone: "{l s='Phone number cannot be greater than #d characters.' mod='advancelogin'}",
    empty_brand: "{l s='Please enter brand name.' mod='advancelogin'}",
    maxchar_brand: "{l s='Brand name cannot be greater than #d characters.' mod='advancelogin'}",
    minchar_brand: "{l s='Brand name cannot be less than #d characters.' mod='advancelogin'}",
    empty_shipment: "{l s='Please enter Shimpment.' mod='advancelogin'}",
    maxchar_shipment: "{l s='Shipment cannot be greater than #d characters.' mod='advancelogin'}",
    minchar_shipment: "{l s='Shipment cannot be less than #d characters.' mod='advancelogin'}",
    invalid_ip: "{l s='Invalid IP format.' mod='advancelogin'}",
    invalid_url: "{l s='Invalid URL format.' mod='advancelogin'}",
    empty_url: "{l s='Please enter URL.' mod='advancelogin'}",
    empty_amount: "{l s='Amount cannot be empty.' mod='advancelogin'}",
    valid_amount: "{l s='Amount should be numeric.' mod='advancelogin'}",
    max_email: "{l s='Email cannot be greater than #d characters.' mod='advancelogin'}",
    specialchar_zip: "{l s='Zip should not have special characters.' mod='advancelogin'}",
    specialchar_sku: "{l s='SKU should not have special characters.' mod='advancelogin'}",
    max_url: "{l s='URL cannot be greater than #d characters.' mod='advancelogin'}",
    valid_percentage: "{l s='Percentage should be in number.' mod='advancelogin'}",
    between_percentage: "{l s='Percentage should be between 0 and 100.' mod='advancelogin'}",
    maxchar_size: "{l s='Size cannot be greater than #d characters.' mod='advancelogin'}",
    specialchar_size: "{l s='Size should not have special characters.' mod='advancelogin'}",
    specialchar_upc: "{l s='UPC should not have special characters.' mod='advancelogin'}",
    maxchar_upc: "{l s='UPC cannot be greater than #d characters.' mod='advancelogin'}",
    specialchar_ean: "{l s='EAN should not have special characters.' mod='advancelogin'}",
    maxchar_ean: "{l s='EAN cannot be greater than #d characters.' mod='advancelogin'}",
    specialchar_bar: "{l s='Barcode should not have special characters.' mod='advancelogin'}",
    maxchar_bar: "{l s='Barcode cannot be greater than #d characters.' mod='advancelogin'}",
    positive_amount: "{l s='Amount should be positive.' mod='advancelogin'}",
    maxchar_color: "{l s='Color could not be greater than #d characters.' mod='advancelogin'}",
    invalid_color: "{l s='Color is not valid.' mod='advancelogin'}",
    specialchar: "{l s='Special characters are not allowed.' mod='advancelogin'}",
    script: "{l s='Script tags are not allowed.' mod='advancelogin'}",
    style: "{l s='Style tags are not allowed.' mod='advancelogin'}",
    iframe: "{l s='Iframe tags are not allowed.' mod='advancelogin'}",
    not_image: "{l s='Uploaded file is not an image' mod='advancelogin'}",
    image_size: "{l s='Uploaded file size must be less than #d.' mod='advancelogin'}",
    html_tags: "{l s='Field should not contain HTML tags.' mod='advancelogin'}",
    number_pos: "{l s='You can enter only positive numbers.' mod='advancelogin'}",
    invalid_separator: "{l s='Invalid comma (#d) separated values.' mod='advancelogin'}"
});
</script>



{*
    * DISCLAIMER
    *
    * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
    * versions in the future. If you wish to customize PrestaShop for your
    * needs please refer tohttp://www.prestashop.com for more information.
    * We offer the best and most useful modules PrestaShop and modifications for your online store.
    *
    * @category  PrestaShop Module
    * @author    knowband.com <support@knowband.com>
    * @copyright 2015 Knowband
    * @license   see file: LICENSE.txt
    *
    * Description
    *
    * Admin tpl file
    *}