
{extends file="helpers/form/form.tpl"}

{block name="defaultForm"}
    <script>
    var advance_login_action = '{$action|escape:'quotes':'UTF-8'}';
    var default_image = '{$advance_login['default']|escape:'htmlall':'UTF-8'}';
    var enable_disable_right_column= '{$enable_disable_right_column|escape:'htmlall':'UTF-8'}'
    var msg= "{l s='Upload image only' mod='advancelogin'}";
    var general_settings='{$general_settings|escape:'htmlall':'UTF-8'}';
    var facebook_settings='{$facebook_settings|escape:'htmlall':'UTF-8'}';
    var google_settings='{$google_settings|escape:'htmlall':'UTF-8'}';
    var confirmation_text ="{l s='Are you sure you want to remove the image?' mod='advancelogin'}";
    </script>
        <div class='row'>
            <div class="productTabs col-lg-2 col-md-3">
                <div class="list-group">
                    {$i=1}
                    {foreach $product_tabs key=numStep item=tab}
                            <a class="list-group-item {if $tab.selected|escape:'htmlall':'UTF-8'}active{/if}" id="link-{$tab.id|escape:'htmlall':'UTF-8'}" onclick="change_tab(this,{$i|escape:'htmlall':'UTF-8'});">{$tab.name|escape:'htmlall':'UTF-8'}</a>
                            {$i=$i+1}
                    {/foreach}
                </div>
            </div>
                {$form} {*Variable contains html content, escape not required*}
                {$view} {*Variable contains html content, escape not required*}
        </div>
{/block}


{*
    * DISCLAIMER
    *
    * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
    * versions in the future. If you wish to customize PrestaShop for your
    * needs please refer tohttp://www.prestashop.com for more information.
    * We offer the best and most useful modules PrestaShop and modifications for your online store.
    *
    * @category  PrestaShop Module
    * @author    knowband.com <support@knowband.com>
    * @copyright 2015 Knowband
    * @license   see file: LICENSE.txt
    *
    * Description
    *
    * Admin tpl file
    *}