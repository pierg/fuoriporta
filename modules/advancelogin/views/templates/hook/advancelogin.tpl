<script>
    var loginaction = '{$login_action|escape:'quotes':'UTF-8'}';
    // changes by rishabh jain
    
    var is_tb = '{$is_tb|escape:'quotes':'UTF-8'}';
//error messages for velovalidation.js
    velovalidation.setErrorLanguage({
    empty_field: "{l s='Field cannot be empty.' mod='advancelogin'}",
    validate_email: "{l s='Please enter a valid Email.' mod='advancelogin'}",
    not_image: "{l s='Uploaded file is not an image' mod='advancelogin'}",
    image_size: "{l s='Uploaded file size must be less than #d.' mod='advancelogin'}"
});
</script>
{if isset($rightsideimage) && $rightsideimage!=""}  
    <div class="vss-ui-dialog dialogNewLoginSignup" id="vss_login_popup" style="visibility: visible; display: none;">
        <div class="shield" onclick="closeLoginPopup();"></div>
        <div class="window alpha30 window-absolute large">
            <div class="content">
                <div class="dialog-body" style="display:block;">
                    <div id="login-signup-newDialog" class="line" style="display: block; background: url('{$path|escape:'htmlall':'UTF-8'}advancelogin/views/img/uploads/{$backgroundimage|escape:'htmlall':'UTF-8'}');">
                        <div class="line" id="line">
                            <div class="size1of2 vss-inline-block login-vmiddle">
                                <div class="new-login-dialog-wrap vss-hidden" style="display: block;">

                                    <span id="loginWithOtpFlow" class="vss-hidden" data-socialid="" data-showpopup=""></span>
                                    <div>
                                        <div class="login-wrap">
                                            <div class="alert alert-danger login_error" style="display:none;color:white;">
                                                <p class="err_text"></p>
			                    </div>
                                            <div class="new-login-form">
                                                 <div id="error_div"></div>
                                                <div class="title vss-font-18 bmargin10 vss-bold">{l s='LOG IN' mod='advancelogin'}</div>
                                               
                                                <form id="advance_login_form">
                                                    <div class="login-input-wrap">
                                                        <input type="text" class="vss-input login-form-input_large user-email" id="useremail" name="login_email" onkeyup="enablelogin();" autocomplete="off" placeholder="{l s='Enter email' mod='advancelogin'}" readonly onfocus="this.removeAttribute('readonly');" />
                                                    </div>
                                                    <div class="tmargin10 login-input-wrap">
                                                        <input type="password" class="vss-input login-form-input_large user-pwd" id="userpwd" name="login_password" autocomplete="off" style="margin-top:10px;" placeholder="{l s='Enter password' mod='advancelogin'}" readonly onfocus="this.removeAttribute('readonly');" />
                                                    </div>
                                                </form>
                                                <div class="tmargin20 login-btn-wrap">
                                                    <input type="button" class="submit-btn login-btn btn" style="font-size:16px;" value="{l s='LOGIN' mod='advancelogin'}" onmouseover="change_color();" onmouseout="change_color_normal();" id="login_btn" onclick="validate_entry()" disabled="disabled">
                                                    <div class="loading_block">
                                                        <div class="loading_image" ><img src='{$path|escape:'htmlall':'UTF-8'}advancelogin/views/img/front/loading.gif' height="30px">
                                                     </div>
                                                    </div>
                                                </div>
                                                <div class="signup_with_rightimage">
                                                   <a href="{$forgot_password_action|escape:'quotes':'UTF-8'}" class="" >{l s='Forgot Password?' mod='advancelogin'}</a>  
                                                </div>
                                                <p class="signup_text frgt_pswd">{l s='Do not have an account?' mod='advancelogin'}</p><a href="{$sign_up_action|escape:'quotes':'UTF-8'}" class="wh_sign" style="color:skyblue; font-size:14px;">{l s='Sign up' mod='advancelogin'}</a>
                                                <div class="login-social-wrap">
                                                    {if $imagetype!='do_not_show'}
                                                        {if $facebook_action!="" && $google_action!=""}
                                                        <div class="login-bottom-msg tmargin30 vss-font-12 txt_fb_google" style="font-size:14px;">{l s='Sign In Using Social Account' mod='advancelogin'}</div>
                                                        {/if}
                                                    {if $imagetype=='large_buttons'}
                                                        <div class="tmargin10 login-btn-wrap txt_fb_google">
                                                            {if $facebook_action!=""}
                                                            <a href="{$facebook_action|escape:'quotes':'UTF-8'}" onclick="return !window.open(this.href, 'popup','width=450,height=300,left=500,top=500')" class="vss-button-social rmargin10 button_social"  title="{l s='Facebook' mod='advancelogin'}"><img src='{$path|escape:'htmlall':'UTF-8'}advancelogin/views/img/admin/buttons/facebook_large.png' alt="Facebook"></a>
                                                            {/if}
                                                            {if $google_action!="" && $facebook_action!=""}
                                                            <a href="{$google_action|escape:'quotes':'UTF-8'}" onclick="return !window.open(this.href, 'popup','width=500,height=500,left=500,top=500')" class="vss-button-social go_mar button_social"  title="{l s='Google' mod='advancelogin'}" ><img src='{$path|escape:'htmlall':'UTF-8'}advancelogin/views/img/admin/buttons/google_large.png' alt="Google"></a>
                                                            {/if}
                                                            {if $google_action!="" && $facebook_action==""}
                                                            <a href="{$google_action|escape:'quotes':'UTF-8'}" onclick="return !window.open(this.href, 'popup','width=500,height=500,left=500,top=500')" class="vss-button-social rmargin10 button_social"  title="{l s='Google' mod='advancelogin'}" ><img src='{$path|escape:'htmlall':'UTF-8'}advancelogin/views/img/admin/buttons/google_large.png' alt="Google"></a>
                                                            {/if}
                                                        </div>
                                                    {else}
                                                        <div class="tmargin10 login-btn-wrap txt_fb_google_small">
                                                            {if $facebook_action!=""}
                                                            <a href="{$facebook_action|escape:'quotes':'UTF-8'}" onclick="return !window.open(this.href, 'popup','width=450,height=300,left=500,top=500')" class="vss-button-social_small rmargin10" title="{l s='Facebook' mod='advancelogin'}"><img src='{$path|escape:'htmlall':'UTF-8'}advancelogin/views/img/admin/buttons/fb_small.png' alt="Facebook"></a>
                                                            {/if}
                                                            {if $google_action!=""}
                                                            <a href="{$google_action|escape:'quotes':'UTF-8'}" onclick="return !window.open(this.href, 'popup','width=500,height=500,left=500,top=500')" class="vss-button-social_small go_mar_small"   title="{l s='Google' mod='advancelogin'}" ><img src='{$path|escape:'htmlall':'UTF-8'}advancelogin/views/img/admin/buttons/google_small.png' alt="Google"></a>
                                                            {/if}
                                                        </div>
                                                    {/if}
                                                    {/if}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        {if $rightsideimage!=""}                        
                        <div class="divider-div vss-inline-block divider"> 
                            <div class="right_image"><img src='{$path|escape:'htmlall':'UTF-8'}advancelogin/views/img/uploads/{$rightsideimage|escape:'htmlall':'UTF-8'}' style="height:200px;margin-top:-31px;" alt="Right column image">
                            </div>
                        </div>
                        <div class="right_image" id="right_image" style="display:none;"><img id="image" src='{$path|escape:'htmlall':'UTF-8'}advancelogin/views/img/uploads/{$rightsideimage|escape:'htmlall':'UTF-8'}' alt="Right column image">
                        </div> 
                        {/if}
                    </div>
                </div>
            </div>
            <span class="close-icon close" onclick="closeLoginPopup()" title="{l s='close' mod='advancelogin'}"></span>
        </div>
    </div>
{else}
    <div class="vss-ui-dialog dialogNewLoginSignup" id="vss_login_popup" style="visibility: visible; display: none;">
        <div class="shield" onclick="closeLoginPopup();"></div>
        <div class="window alpha30 window-absolute small">
            <div class="content">
                <div class="dialog-body" style="display:block;">
                    <div id="login-signup-newDialog" class="line" style="display: block; background: url('{$path|escape:'htmlall':'UTF-8'}advancelogin/views/img/uploads/{$backgroundimage|escape:'htmlall':'UTF-8'}');">
                        <div class="line" id="line">
                            <div class="size1of2 vss-inline-block login-vmiddle">
                                <div class="new-login-dialog-wrap vss-hidden" style="display: block;">

                                    <span id="loginWithOtpFlow" class="vss-hidden" data-socialid="" data-showpopup=""></span>
                                    <div>
                                        <div class="login-wrap">
                                            <div class="alert alert-danger login_error" style="display:none;color:white;">
                                                <p class="err_text"></p>
			                    </div>
                                            <div class="new-login-form">
                                                <div id="error_div"></div>
                                                <div class="title vss-font-18 bmargin10 vss-bold">{l s='LOGIN' mod='advancelogin'}</div>
                                                
                                                <form  id="advance_login_form">
                                                    <div class="login-input-wrap">
                                                        <input type="text" class="vss-input login-form-input user-email" id="useremail" name="login_email" onkeyup="enablelogin();" autocomplete="off" placeholder="{l s='Enter email' mod='advancelogin'}" readonly onfocus="this.removeAttribute('readonly');">
                                                    </div>
                                                    <div class="tmargin10 login-input-wrap">
                                                        <input type="password" class="vss-input login-form-input user-pwd" id="userpwd" name="login_password" autocomplete="off" style="margin-top:10px;" placeholder="{l s='Enter password' mod='advancelogin'}" readonly onfocus="this.removeAttribute('readonly');">
                                                    </div>
                                                </form>

                                                <div class="login-btn-wrap login_small">
                                                    <input type="button" class="submit-btn login-btn btn" style="font-size:16px;" value="{l s='LOGIN' mod='advancelogin'}" onmouseover="change_color();" onmouseout="change_color_normal();" id="login_btn" onclick="validate_entry()" disabled="disabled">
                                                     <div class="loading_block">
                                                        <div class="loading_image"><img src='{$path|escape:'htmlall':'UTF-8'}advancelogin/views/img/front/loading.gif' height="30px">
                                                     </div>
                                                    </div>
                                                </div>
                                                <div class="signup_without_rightimage">
                                                   <a href="{$forgot_password_action|escape:'quotes':'UTF-8'}" class="top" >{l s='Forgot Password?' mod='advancelogin'}</a>  
                                                </div>
                                                <p class="signup_text frgt_pswd top_sign">{l s='Do not have an account?' mod='advancelogin'}</p><a href="{$sign_up_action|escape:'quotes':'UTF-8'}" class="wh_sign" style="color:skyblue; font-size:14px;">{l s='Sign up' mod='advancelogin'}</a>
                                                <div class="login-social-wrap">
                                                    {if $imagetype!='do_not_show'}
                                                    {if $facebook_action!="" && $google_action!=""}
                                                    <div class="login-bottom-msg tmargin30 vss-font-12 txt_fb_google" style="font-size:14px;">{l s='Sign In using social account' mod='advancelogin'}</div>
                                                    {/if}
                                                    {if $imagetype=='large_buttons'}
                                                        <div class="tmargin10 login-btn-wrap txt_fb_google">
                                                            {if $facebook_action!=""}
                                                            <a href="{$facebook_action|escape:'quotes':'UTF-8'}" onclick="return !window.open(this.href, 'popup','width=450,height=300,left=500,top=500')" class="vss-button-social rmargin10 button_social" title="{l s='Facebook' mod='advancelogin'}"><img src='{$path|escape:'htmlall':'UTF-8'}advancelogin/views/img/admin/buttons/facebook_large.png' alt="Facebook"></a>
                                                            {/if}
                                                            {if $google_action!="" && $facebook_action!=""}
                                                            <a href="{$google_action|escape:'quotes':'UTF-8'}" onclick="return !window.open(this.href, 'popup','width=500,height=500,left=500,top=500')" class="vss-button-social go_mar button_social"  title="{l s='Google' mod='advancelogin'}" ><img src='{$path|escape:'htmlall':'UTF-8'}advancelogin/views/img/admin/buttons/google_large.png' alt="Google"></a>
                                                            {/if}
                                                            {if $google_action!="" && $facebook_action==""}
                                                            <a href="{$google_action|escape:'quotes':'UTF-8'}" onclick="return !window.open(this.href, 'popup','width=500,height=500,left=500,top=500')" class="vss-button-social rmargin10 button_social"  title="{l s='Google' mod='advancelogin'}" ><img src='{$path|escape:'htmlall':'UTF-8'}advancelogin/views/img/admin/buttons/google_large.png' alt="Google"></a>
                                                            {/if}
                                                        </div>
                                                    {else}
                                                        <div class="tmargin10 login-btn-wrap txt_fb_google_small">
                                                            {if $facebook_action!=""}
                                                            <a href="{$facebook_action|escape:'quotes':'UTF-8'}" onclick="return !window.open(this.href, 'popup','width=450,height=300,left=500,top=500')" class="vss-button-social_small rmargin10"  title="{l s='Facebook' mod='advancelogin'}"><img src='{$path|escape:'htmlall':'UTF-8'}advancelogin/views/img/admin/buttons/fb_small.png' alt="Facebook"></a>
                                                            {/if}
                                                            {if $google_action!="" && $facebook_action!=""}
                                                            <a href="{$google_action|escape:'quotes':'UTF-8'}" onclick="return !window.open(this.href, 'popup','width=500,height=500,left=500,top=500')" class="vss-button-social_small go_mar_small" title="{l s='Google' mod='advancelogin'}" ><img src='{$path|escape:'htmlall':'UTF-8'}advancelogin/views/img/admin/buttons/google_small.png' alt="Google"></a>
                                                            {/if}
                                                            {if $google_action!="" && $facebook_action==""}
                                                            <a href="{$google_action|escape:'quotes':'UTF-8'}" onclick="return !window.open(this.href, 'popup','width=500,height=500,left=500,top=500')" class="vss-button-social_small rmargin10" title="{l s='Google' mod='advancelogin'}" ><img src='{$path|escape:'htmlall':'UTF-8'}advancelogin/views/img/admin/buttons/google_small.png' alt="Google"></a>
                                                            {/if}
                                                        </div>
                                                    {/if}
                                                    {/if}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <span class="close-icon close" onclick="closeLoginPopup()" title="{l s='close' mod='advancelogin'}"></span>
        </div>
    </div>     
{/if}
</form>

    {*
    * DISCLAIMER
    *
    * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
    * versions in the future. If you wish to customize PrestaShop for your
    * needs please refer tohttp://www.prestashop.com for more information.
    * We offer the best and most useful modules PrestaShop and modifications for your online store.
    *
    * @category  PrestaShop Module
    * @author    knowband.com <support@knowband.com>
    * @copyright 2015 Knowband
    * @license   see file: LICENSE.txt
    *
    * Description
    *
    * Admin tpl file
    *}