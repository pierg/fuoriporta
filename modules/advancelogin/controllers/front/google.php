<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * @author    knowband.com <support@knowband.com>
 * @copyright 2015 Knowband
 * @license   see file: LICENSE.txt
 * @category  PrestaShop Module
 */

class AdvanceLoginGoogleModuleFrontController extends ModuleFrontController
{
    public function init()
    {
        parent::init();

        include_once(_PS_MODULE_DIR_ . 'advancelogin/libraries/http.php');
        include_once(_PS_MODULE_DIR_ . 'advancelogin/libraries/oauth_client.php');
    }
    public function initContent()
    {
        //parent::initContent();
        $platform = Tools::getValue('type');
        $platform = trim($platform);

        if ($platform == 'google') {
            $user_data = $this->googleLogin();
        }
        if (empty($user_data)) {
            $user_data = $this->googleLogin();
        }

        if (count($user_data) > 0) {
            $social_data = array();
            $social_data['first_name'] = $user_data->given_name;
            $social_data['last_name'] = $user_data->family_name;
            $social_data['email'] = $user_data->email;
            $social_data['gender'] = 0;
            $social_data['username'] = $user_data->given_name;
            $obj = new AdvanceLogin();
            $result = $obj->addUser($social_data, 'Google');
            if ($result == 1) {
                echo '<script> window.opener.location.reload(true);
                                window.close();</script>';
            } else {
                $y = Configuration::get('PS_SSL_ENABLED');
                Tools::redirect($this->context->link->getModuleLink('advancelogin', 'error', array(), $y));
            }
        } else {
            echo '<script>window.close();</script>';
        }
    }

    public function googleLogin()
    {
        $settings = Configuration::get('Advance_Login');
        $loginizer_data = Tools::unSerialize($settings);
        $y = Configuration::get('PS_SSL_ENABLED');
        $user = '';
        $client = new oauth_client_class;
        $client->server = 'Google';

        $client->offline = true;

        $client->debug = false;
        $client->debug_http = true;
        $client->redirect_uri = $this->context->link->getModuleLink('advancelogin', 'google', array(), $y);

        $lang_str = '&id_lang=' . $this->context->language->id;
        $client->redirect_uri = str_replace($lang_str, '', $client->redirect_uri);

        $lang_str = '/' . $this->context->language->iso_code . '/';
        $client->redirect_uri = str_replace($lang_str, '/', $client->redirect_uri);

        $client->client_id = trim($loginizer_data['google_plus_client_id']);
        $client->client_secret = $loginizer_data['google_plus_client_secret'];

        if (Tools::strlen($client->client_id) == 0 || Tools::strlen($client->client_secret) == 0) {
            Tools::redirect($this->context->link->getModuleLink('advancelogin', 'credentials', array(), $y));
        }
        /* API permissions
         */
        $client->scope = 'https://www.googleapis.com/auth/userinfo.email'
                . ' https://www.googleapis.com/auth/userinfo.profile';
        if (($success = $client->Initialize())) {
            if (($success = $client->Process())) {
                if (Tools::strlen($client->authorization_error)) {
                    $client->error = $client->authorization_error;
                    $success = false;
                } else if (Tools::strlen($client->access_token)) {
                    $success = $client->CallAPI(
                        'https://www.googleapis.com/oauth2/v1/userinfo',
                        'GET',
                        array(),
                        array('FailOnAccessError' => true),
                        $user
                    );
                    return $user;
                }
            }
            $success = $client->Finalize($success);
        }
        if ($client->exit) {
            exit;
        }
    }
}
