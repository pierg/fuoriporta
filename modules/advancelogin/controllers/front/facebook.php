<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * @author    knowband.com <support@knowband.com>
 * @copyright 2015 Knowband
 * @license   see file: LICENSE.txt
 * @category  PrestaShop Module
 */

class AdvanceLoginFacebookModuleFrontController extends ModuleFrontController
{
    public function init()
    {
            parent::init();

            include_once(_PS_MODULE_DIR_ . 'advancelogin/libraries/http.php');
            include_once(_PS_MODULE_DIR_ . 'advancelogin/libraries/oauth_client.php');
    }
    public function initContent()
    {
        //parent::initContent();

        $platform = Tools::getValue('type');
        $platform = trim($platform);

        if ($platform == 'fb') {
            $user_data = $this->facebookLogin();
        } elseif (empty($user_data)) {
            $user_data = $this->facebookLogin();
        }

        if (Tools::isSubmit('login_email')) {
            $email = Tools::getValue('login_email');
            if (Customer::customerExists($email)) {
                $errormsg = $this->module->l('Email already exist please choose another one');
                $this->context->smarty->assign('errormsg', $errormsg);
                if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
                        $custom_ssl_var = 1;
                }
                if ((bool)Configuration::get('PS_SSL_ENABLED') && $custom_ssl_var == 1) {
                    $module_dir = _PS_BASE_URL_SSL_.__PS_BASE_URI__.str_replace(_PS_ROOT_DIR_.'/', '', _PS_MODULE_DIR_);
                } else {
                    $module_dir = _PS_BASE_URL_.__PS_BASE_URI__.str_replace(_PS_ROOT_DIR_.'/', '', _PS_MODULE_DIR_);
                }
                $this->context->smarty->assign('modulepath', $module_dir);
                $this->setTemplate('facebook-email.tpl');
            } else {
                $user_data = $this->facebookLogin();
                $unique_id = $user_data->id;
                $query = 'select email from '._DB_PREFIX_.'advancelogin_mapping where unique_id="'.pSQL($unique_id).'"';
                $result = Db::getInstance()->getRow($query);
                if (!$result) {
                    $sql = 'insert into '._DB_PREFIX_.'advancelogin_mapping '
                            . '(email,unique_id) values ('
                            . '"'.pSQL($email).'","'.pSQL($unique_id).'")';
                    Db::getInstance()->execute($sql);
                }
            }
        }
        if (count($user_data) > 0) {
            $query = 'select email from '._DB_PREFIX_.'advancelogin_mapping where unique_id="'.pSQL($user_data->id).'"';
            $result = Db::getInstance()->getRow($query);
            $social_data = array();
            $social_data['first_name'] = $user_data->first_name;
            $social_data['last_name'] = $user_data->last_name;
            if ((isset($user_data->email) && !empty($user_data->email))) {
                $social_data['email'] = $user_data->email;
                $flag = true;
            } elseif ((!isset($user_data->email) || empty($user_data->email)) && $result!=0) {
                $social_data['email'] = $result['email'];
                $flag = true;
            } elseif ((!isset($user_data->email) || empty($user_data->email)) && $result==0) {
                $flag = false;
                $client = new oauth_client_class;
                $client->SessionExpireOut();
                $error = $this->module->l("Login failed!!Email not Found.");
                echo '<script> window.opener.document.getElementById("error_div").innerHTML="'. $error . '";window.close();   </script>';
                //window.opener.document.$("#advance_login_form").append("<p>This is the source window!</p>");window.close();
                ///window.opener.document.getElementById("advance_login_form").after("<span class="kb_error_message">" + "Email Error." + "</span>");
//                if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
//                        $custom_ssl_var = 1;
//                }
//                if ((bool)Configuration::get('PS_SSL_ENABLED') && $custom_ssl_var == 1) {
//                    $module_dir = _PS_BASE_URL_SSL_.__PS_BASE_URI__.str_replace(_PS_ROOT_DIR_.'/', '', _PS_MODULE_DIR_);
//                } else {
//                    $module_dir = _PS_BASE_URL_.__PS_BASE_URI__.str_replace(_PS_ROOT_DIR_.'/', '', _PS_MODULE_DIR_);
//                }
//                $this->context->smarty->assign('modulepath', $module_dir);
//                $this->setTemplate('facebook-email.tpl');
            }
            if ($flag) {
                $social_data['gender'] = ($user_data->gender == 'male') ? 0 : 1;
                $social_data['username'] = $user_data->first_name;
                $obj = new AdvanceLogin();
                $result = $obj->addUser($social_data, 'Facebook');
                if ($result == 1) {
                        echo '<script> window.opener.location.reload(true);
                                            window.close();</script>';
                } else {
                    $y = Configuration::get('PS_SSL_ENABLED');
                    Tools::redirect($this->context->link->getModuleLink('advancelogin', 'error', array(), $y));
                }
            }
        } else {
            echo '<script>window.close();</script>';
        }
    }

    public function facebookLogin()
    {
        $settings = Configuration::get('Advance_Login');
        $loginizer_data = Tools::unSerialize($settings);
        $y = Configuration::get('PS_SSL_ENABLED');

        $user = '';
        $client = new oauth_client_class;
        $client->debug = false;
        $client->debug_http = true;
        $client->server = 'Facebook';
        $client->redirect_uri = $this->context->link->getModuleLink('advancelogin', 'facebook', array(), $y);

        $lang_str = '&id_lang=' . $this->context->language->id;
        $client->redirect_uri = str_replace($lang_str, '', $client->redirect_uri);

        $lang_str = '/' . $this->context->language->iso_code . '/';
        $client->redirect_uri = str_replace($lang_str, '/', $client->redirect_uri);

        $client->client_id = $loginizer_data['facebook_app_id'];
        $client->client_secret = $loginizer_data['facebook_app_secret'];

        if (Tools::strlen($client->client_id) == 0 || Tools::strlen($client->client_secret) == 0) {
            Tools::redirect($this->context->link->getModuleLink('advancelogin', 'credentials', array(), $y));
        }

        /* API permissions
         */
        $client->scope = 'email';
        //$client->scope = 'user_birthday'; use this scope to get whole profile information along with birth date
        if (($success = $client->Initialize())) {
            if (($success = $client->Process())) {
                if (Tools::strlen($client->access_token)) {
                    try {
                        $success = $client->CallAPI(
                            'https://graph.facebook.com/me?fields=email,first_name,last_name,gender',
                            'GET',
                            array(),
                            array('FailOnAccessError' => true),
                            $user
                        );
                    } catch (Exception $e) {
                        $user = '';
                    }
                    return $user;
                }
            }
            $success = $client->Finalize($success);
        }
        if ($client->exit) {
            exit;
        }
    }
}
