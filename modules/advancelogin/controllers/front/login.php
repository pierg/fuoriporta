<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * @author    knowband.com <support@knowband.com>
 * @copyright 2015 Knowband
 * @license   see file: LICENSE.txt
 * @category  PrestaShop Module
 */

class AdvanceLoginLoginModuleFrontController extends ModuleFrontController
{
    public function init()
    {
        parent::init();
    }
    public function setMedia()
    {
        parent::setMedia();
    }
    public function initContent()
    {
        parent::initContent();
    }
    public function postProcess()
    {
        parent::postProcess();
        if (Tools::getvalue('ajax')) {
            $error = "";
            Hook::exec('actionBeforeAuthentication');
            $passwd = trim(Tools::getValue('login_password'));
            $_POST['passwd'] = null;
            $email = trim(Tools::getValue('login_email'));
            
            /*Knowband validation start*/
            if (empty($email)) {
                $error = $this->module->l('An email address required.', 'login');
            } elseif (!Validate::isEmail($email)) {
                $error = $this->module->l('Invalid email address.', 'login');
            } elseif (empty($passwd)) {
                $error = $this->module->l('Password is required.', 'login');
            } elseif (!Validate::isPasswd($passwd)) {
                $error = $this->module->l('Your username or password is incorrect.', 'login');
            /*Knowband validation end*/
            } else {
                $customer = new Customer();
                $authentication = $customer->getByEmail(trim($email), trim($passwd));
                if (isset($authentication->active) && !$authentication->active) {
                    $error = $this->module->l('Your account isn\'t available at this time, please contact us', 'login');
                } elseif (!$authentication || !$customer->id) {
                    $error = $this->module->l('Authentication failed.', 'login');
                } else {
                    $this->context->cookie->id_compare = isset($this->context->cookie->id_compare) ?
                            $this->context->cookie->id_compare :
                            CompareProduct::getIdCompareByIdCustomer($customer->id);
                    $this->context->cookie->id_customer = (int) ($customer->id);
                    $this->context->cookie->customer_lastname = $customer->lastname;
                    $this->context->cookie->customer_firstname = $customer->firstname;
                    $this->context->cookie->logged = 1;
                    $customer->logged = 1;
                    $this->context->cookie->is_guest = $customer->isGuest();
                    $this->context->cookie->passwd = $customer->passwd;
                    $this->context->cookie->email = $customer->email;

                    // Add customer to the context
                    $this->context->customer = $customer;
//
                    if (Configuration::get('PS_CART_FOLLOWING') &&
                            (empty($this->context->cookie->id_cart)
                            || Cart::getNbProducts($this->context->cookie->id_cart) == 0)
                            && $id_cart =
                            (int) Cart::lastNoneOrderedCart($this->context->customer->id)) {
                        $this->context->cart = new Cart($id_cart);
                    } else {
                        $id_carrier = (int) $this->context->cart->id_carrier;
                        $this->context->cart->id_carrier = 0;
                        $this->context->cart->setDeliveryOption(null);
                        $this->context->cart->id_address_delivery =
                                (int) Address::getFirstCustomerAddressId((int) ($customer->id));
                        $this->context->cart->id_address_invoice =
                                (int) Address::getFirstCustomerAddressId((int) ($customer->id));
                    }
                    $this->context->cart->id_customer = (int) $customer->id;
                    $this->context->cart->secure_key = $customer->secure_key;

                    if ($this->ajax && isset($id_carrier)
                            && $id_carrier && Configuration::get('PS_ORDER_PROCESS_TYPE')) {
                        $delivery_option = array($this->context->cart->id_address_delivery => $id_carrier . ',');
                        $this->context->cart->setDeliveryOption($delivery_option);
                    }

                    $this->context->cart->save();
                    $this->context->cookie->id_cart = (int) $this->context->cart->id;
                    $this->context->cookie->write();
                    $this->context->cart->autosetProductAddress();

                    Hook::exec('actionAuthentication', array('customer' => $this->context->customer));

                    // Login information have changed, so we check if the cart rules still apply
                    CartRule::autoRemoveFromCart($this->context);
                    CartRule::autoAddToCart($this->context);

                    if (!$this->ajax) {
                        $back = Tools::getValue('back', 'my-account');

                        if ($back == Tools::secureReferrer($back)) {
                            Tools::redirect(html_entity_decode($back));
                        }

                        Tools::redirect(
                            'index.php?'
                            . 'controller='
                            . (
                            ($this->authRedirection !== false)
                            ?
                            urlencode($this->authRedirection) : $back)
                        );
                    }
                }
            }
            if ($error != "") {
                echo $error;
                die;
            }
        }
    }
}
