<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer tohttp://www.prestashop.com for more information.
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * @author    knowband.com <support@knowband.com>
 * @copyright 2015 Knowband
 * @license   see file: LICENSE.txt
 * @category  PrestaShop Module
 *
 * Description
 *
 * Admin can change text color,translations,font style
 */

class AdvanceLogin extends Module
{

    private $my_module_settings = array();
    private $my_module_settings1 = array();
    private $my_module_settings2 = array();
    private $advance_login_values_bannerimage1 = array();
    private $advance_login_values_bannerimage2 = array();

    public function __construct()
    {
        if (!class_exists('oauth_client_class')) {
            include_once(_PS_MODULE_DIR_ . 'advancelogin/libraries/oauth_client.php');
        }
        $this->name = 'advancelogin';
        $this->tab = 'front_office_features';
        $this->version = '1.0.5';
        $this->author = 'knowband';
        $this->need_instance = 0;
        $this->module_key = '3c2d4b598c64c1add4c8d85b054cb63c';
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Advance Login');
        
        // Text chnaged by S Anand on 15 May 2017
        $this->description = $this->l('Adds a login pop up box when user clicks on sign in button');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (!Configuration::get('Advance_Login')) {
            $this->warning = $this->l('No name provided');
        }
    }

    public function install()
    {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }
        if (!parent::install() || !$this->registerHook('Header') || !$this->registerHook('actionCustomerLogoutBefore')) {
            return false;
        }
        // Email datatype changed to varchar(100) by S Anand on 15 May 2017
        $create_table = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'advancelogin_mapping` (
			`id` int(11) NOT NULL auto_increment,
			`email` varchar(100) NOT NULL,
			`unique_id` varchar(50) NOT NULL,
			PRIMARY KEY  (`id`)
		      )';
        Db::getInstance()->execute($create_table);
        $module_dir = $this->getModulePath();
        
        $defaultsettings = $this->getDefaultSettings();
        $defaultsettings = serialize($defaultsettings);
        Configuration::updateValue('bannerimage1', $module_dir . 'advancelogin/views/img/uploads/');
        Configuration::updateValue('bannerimage2', $module_dir . 'advancelogin/views/img/uploads/');
        Configuration::updateValue('Advance_Login', $defaultsettings);
        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall() || !$this->unregisterHook('actionCustomerLogoutBefore')) {
            return false;
        }
        return true;
    }

    public function getContent()
    {
        $output = null;
        $error = null;
        
        $module_dir = $this->getModulePath();
        // Start of Code to remove images on click of remove button
//        if (Tools::getvalue('ajax')) {
//            $id = Tools::getvalue('id');
//            if ($id == 1) {
//                $config = Configuration::get('Advance_Login');
//                $this->advance_login_values_bannerimage1 = Tools::unSerialize($config);
//                $filename = _PS_MODULE_DIR_ . '/advancelogin/views/img/uploads/'
//                        . $this->advance_login_values_bannerimage1['bannerimage1'];
//                unlink($filename);
//                unset($this->advance_login_values_bannerimage1['bannerimage1']);
//                $content = serialize($this->advance_login_values_bannerimage1);
//                Configuration::updateValue('Advance_Login', $content);
//                echo $this->displayConfirmation($this->l('Backgorund image has been removed successfully.'));
//            }
//            if ($id == 2) {
//                $config = Configuration::get('Advance_Login');
//                $this->advance_login_values_bannerimage2 = Tools::unSerialize($config);
//                $filename = _PS_MODULE_DIR_ . '/advancelogin/views/img/uploads/'
//                        . $this->advance_login_values_bannerimage2['bannerimage2'];
//                unlink($filename);
//                unset($this->advance_login_values_bannerimage2['bannerimage2']);
//                $content = serialize($this->advance_login_values_bannerimage2);
//                Configuration::updateValue('Advance_Login', $content);
//                echo $this->displayConfirmation($this->l('Right column image has been removed successfully.'));
//            }
//        }
        // End of Code to remove images on click of remove button
        // Start of Code to save settings
        if (Tools::isSubmit('advance_login')) {
            $flag = true;
            $config = Configuration::get('Advance_Login');
            $this->my_module_settings = Tools::unSerialize($config);
            $formvalue = Tools::getValue('advance_login');
            
            
            //BOC To remove previous image
            if (Tools::getValue('previous_value1') == '1') {
                $config = Configuration::get('Advance_Login');
                $this->advance_login_values_bannerimage1 = Tools::unSerialize($config);
                $filename = _PS_MODULE_DIR_ . '/advancelogin/views/img/uploads/'
                        . $this->advance_login_values_bannerimage1['bannerimage1'];
                unlink($filename);
                unset($this->advance_login_values_bannerimage1['bannerimage1']);
                $content = serialize($this->advance_login_values_bannerimage1);
                Configuration::updateValue('Advance_Login', $content);
            } else {
                if (isset($this->my_module_settings['bannerimage1'])) {
                        $formvalue['bannerimage1'] = $this->my_module_settings['bannerimage1'];
                }
            }
                
            if (Tools::getValue('previous_value2') == '1') {
                $config = Configuration::get('Advance_Login');
                $this->advance_login_values_bannerimage2 = Tools::unSerialize($config);
                $filename = _PS_MODULE_DIR_ . '/advancelogin/views/img/uploads/'
                        . $this->advance_login_values_bannerimage2['bannerimage2'];
                unlink($filename);
                unset($this->advance_login_values_bannerimage2['bannerimage2']);
                $content = serialize($this->advance_login_values_bannerimage2);
                Configuration::updateValue('Advance_Login', $content);
            } else {
                if (isset($this->my_module_settings['bannerimage2'])) {
                    $formvalue['bannerimage2'] = $this->my_module_settings['bannerimage2'];
                }
            }

            //EOC To remove previous image
            /*Knowband image validation start*/
            if ($_FILES['file1']['name'] != "") {
                $id = 1;
                if ($_FILES['file1']['size'] == 0) {
                    $error = $this->l("Background image is not correct");
                } else {
                    $allowed_exts = array('gif', 'jpeg', 'jpg', 'png', 'JPG', 'PNG', 'GIF', 'JPEG');
                    $extension = explode('.', $_FILES['file1']['name']);
                    $extension = end($extension);
                    $extension = trim($extension);
                    if ((($_FILES['file1']['type'] == 'image/jpg')
                            || ($_FILES['file1']['type'] == 'image/jpeg')
                            || ($_FILES['file1']['type'] == 'image/gif')
                            || ($_FILES['file1']['type'] == 'image/png'))
                            && ($_FILES['file1']['size'] < 2097152)
                            && in_array($extension, $allowed_exts)) {
                        $allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
                        $detectedType = exif_imagetype($_FILES['file1']['tmp_name']);
                        if (in_array($detectedType, $allowedTypes)=== false) {
                            $error = $this->l(" Background image is not correct");
                        } else if ($_FILES['file1']['error'] > 0) {
                            $error = $this->l("Background image is not correct");
                        } else {
                            $mask = _PS_MODULE_DIR_ . 'advancelogin/views/img/uploads/banner' . trim($id) .'_'. time() . '.*';
                            $matches = glob($mask);
                            if (count($matches) > 0) {
                                array_map('unlink', $matches);
                            }
                            move_uploaded_file(
                                $_FILES['file1']['tmp_name'],
                                _PS_MODULE_DIR_ . 'advancelogin/views/img/uploads/banner' . trim($id) .'_'. time() . '.'
                                . $extension
                            );
                            $file1 = trim('banner' . $id .'_'.time() .'.' . $extension);
                            $formvalue['bannerimage1'] = $file1;
                            $formvalue['bannerimage1_name'] = $_FILES['file1']['name'];
                        }
                    } else {
                        $error = $this->l("Background image is not correct");
                    }
                }
            }
            if ($_FILES['file2']['name'] != "") {
                $id = 2;
                if ($_FILES['file2']['size'] == 0) {
                    $error = $this->l("Right column image is not correct");
                } else {
                    $allowed_exts = array('gif', 'jpeg', 'jpg', 'png', 'JPG', 'PNG', 'GIF', 'JPEG');
                    $extension = explode('.', $_FILES['file2']['name']);
                    $extension = end($extension);
                    $extension = trim($extension);
                    if ((($_FILES['file2']['type'] == 'image/jpg')
                            || ($_FILES['file2']['type'] == 'image/jpeg')
                            || ($_FILES['file2']['type'] == 'image/gif')
                            || ($_FILES['file2']['type'] == 'image/png'))
                            && ($_FILES['file2']['size'] < 2097152)
                            && in_array($extension, $allowed_exts)) {
                        $allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
                        $detectedType = exif_imagetype($_FILES['file2']['tmp_name']);
                        if (in_array($detectedType, $allowedTypes)=== false) {
                            $error = $this->l(" Right column image is not correct");
                        } else if ($_FILES['file2']['error'] > 0) {
                            $error = $this->l("Right column image is not correct");
                        } else {
                            $mask = _PS_MODULE_DIR_ . 'advancelogin/views/img/uploads/banner' . trim($id) .'_'. time() . '.*';
                            $matches = glob($mask);
                            if (count($matches) > 0) {
                                array_map('unlink', $matches);
                            }
                            move_uploaded_file(
                                $_FILES['file2']['tmp_name'],
                                _PS_MODULE_DIR_ . 'advancelogin/views/img/uploads/banner' . trim($id) .'_'. time() . '.'
                                . $extension
                            );
                            $file2 = trim('banner' . $id .'_'. time() .'.' . $extension);
                            $formvalue['bannerimage2'] = $file2;
                        }
                    } else {
                        $error = $this->l("Right column image is not correct");
                    }
                }
            }
            /*Knowband image validation end*/
            /*Knowband validation start*/
            $formvalue['default'] = $module_dir . 'advancelogin/views/img/admin/default.jpg';
            $formvalue['facebook_image'] = $module_dir . 'advancelogin/views/img/admin/buttons/fb_small.png';
            $formvalue['google_plus_image'] = $module_dir . 'advancelogin/views/img/admin/buttons/google_small.png';
            $formvalue['facebook_large_image'] = $module_dir . 'advancelogin/views/img/admin/buttons/fb_large.png';
            $formvalue['google_plus_large_image'] = $module_dir .
                    'advancelogin/views/img/admin/buttons/google_large.png';
            if (isset($formvalue['google_plus_status'])
                    && $formvalue['google_plus_status'] == 1
                    && (trim($formvalue['google_plus_client_id']) == ""
                    || trim($formvalue['google_plus_client_secret']) == "")) {
                $output .= $this->displayError($this->l("Credentials are required to enable Google login"));
                $flag = false;
            }
            if (isset($formvalue['facebook_status'])
                    && $formvalue['facebook_status'] == 1
                    && (trim($formvalue['facebook_app_id']) == ""
                    || trim($formvalue['facebook_app_secret']) == "")) {
                $output .= $this->displayError($this->l("Credentials are required to enable Facebook login"));
                $flag = false;
            }
            if ($flag == true) {
                $content = serialize($formvalue);
                Configuration::updateValue('Advance_Login', $content);
                if ($error != null) {
                    $output .= $this->displayWarning($this->l('Configuration has been saved successfully but ').$error);
                } else {
                    $output .= $this->displayConfirmation($this->l('Configuration has been saved successfully.'));
                }
            }
        }
        // End of Code to save settings
        /*Knowband validation end*/
        $config = Configuration::get('Advance_Login');
        $this->my_module_settings = Tools::unSerialize($config);
        if (isset($this->my_module_settings['bannerimage1'])) {
            $image1=$module_dir . 'advancelogin/views/img/uploads/'.$this->my_module_settings['bannerimage1'];
        } else {
            $image1=$module_dir . 'advancelogin/views/img/admin/default.jpg';
        }
        $image_url1 = "<img id='bannerimage1' src='".$image1."' width='100px;' height='100px;'>";
        if (isset($this->my_module_settings['bannerimage2'])) {
            $image2=$module_dir . 'advancelogin/views/img/uploads/'.$this->my_module_settings['bannerimage2'];
        } else {
            $image2=$module_dir . 'advancelogin/views/img/admin/default.jpg';
        }
        $image_url2 = "<img id='bannerimage2' src='".$image2."' width='100px;' height='100px;'>";
        $action= AdminController::$currentIndex . '&token=' . Tools::getAdminTokenLite('AdminModules');
        
        
        $this->context->controller->addCSS($this->_path . 'views/css/admin/advance_login.css');
        $this->context->controller->addJs($this->_path . 'views/js/admin/advance_login.js');
        $this->context->controller->addjs($this->_path . 'views/js/velovalidation.js');
        
        $this->context->smarty->assign(array(
            'languages' => Language::getLanguages(true),
            'advance_login' => $this->my_module_settings,
            'action' => $action,
            'path' => $module_dir . 'advancelogin/views/img/uploads/',
        ));
        $this->available_tabs_lang = array(
            'General Settings' => $this->l('General Settings'),
            'Facebook Settings' => $this->l('Facebook Settings'),
            'Google Plus Settings' => $this->l('Google Plus Settings'),
        );
        $this->available_tabs = array('General Settings', 'Facebook Settings', 'Google Plus Settings');
        
        $product_tabs = array();
        
        $this->tab_display='General Settings';
        foreach ($this->available_tabs as $product_tab) {
                    $product_tabs[$product_tab] = array(
                        'id' => $product_tab,
                        'selected' => (Tools::strtolower($product_tab) == Tools::strtolower($this->tab_display)
                            || (isset($this->tab_display_module) && 'module'.$this->tab_display_module
                                == Tools::strtolower($product_tab))),
                        'name' => $this->available_tabs_lang[$product_tab],
                        'href' => AdminController::$currentIndex . '&token=' . Tools::getAdminTokenLite('AdminModules'),
                    );
        }
        
        $options = array(
            array(
                'id_show_on_login_page' => 'do_not_show',
                'name' => $this->l('Do not show')
            ),
            array(
                'id_show_on_login_page' => 'small_buttons',
                'name' => $this->l('Small Buttons')
            ),
            array(
                'id_show_on_login_page' => 'large_buttons',
                'name' => $this->l('Large Buttons')
            ),
        );
        $options_right = array(
            array(
                'id_show_right_column' => 'enable',
                'name' => $this->l('Enable')
            ),
            array(
                'id_show_right_column' => 'disable',
                'name' => $this->l('Disable')
            ),
        );
        
        $this->fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('General Settings'),
                ),
                'input' => array(
                    array(
                        'label' => $this->l('Enable/Disable'),
                        'type' => 'switch',
                        'hint' => $this->l('Enable/Disable this plugin'),
                        'class' => 'optn_general',
                        'name' => 'advance_login[enable]',
                        'values' => array(
                            array(
                            'value' => 1,
                            ),
                            array(
                            'value' => 0,
                            ),
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Show Button Type'),
                        'name' => 'advance_login[show_on_login_page]',
                        'hint' => $this->l('Choose the size of login buttons i.e facebook and google login buttons'),
                        'class' => 'optn_general',
                        'onchange'=> '',
                        'is_bool' => true,
                        'options' => array(
                            'query'=>$options,
                            'id' => 'id_show_on_login_page',
                            'name'=> 'name',
                        ),
                    ),
                    array(
                        'type' => 'file',
                        'label' => $this->l('Background Image'),
                        'name' => 'file1',
                        'image' => $image_url1 ? $image_url1 : false,
                        'lang' => true,
                        'display_image' => true,
                        'col' => '9',
                        'hint' => $this->l('Upload the background image of the login pop up')
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Show Right Column'),
                        'name' => 'advance_login[show_right_column]',
                        'hint' => $this->l('Enable the right column'),
                        'class' => 'optn_general',
                        'onchange'=> 'enable_right_column(this);',
                        'is_bool' => true,
                        'options' => array(
                            'query'=>$options_right,
                            'id' => 'id_show_right_column',
                            'name'=> 'name',
                        ),
                    ),
                    array(
                        'type' => 'file',
                        'label' => $this->l('Right Column Image'),
                        'name' => 'file2',
                        'required' => true,
                        'image' => $image_url2 ? $image_url2 : false,
                        'lang' => true,
                        'desc' => $this->l('Best size 300 * 300 pixels.'),
                        'display_image' => true,
                        'col' => '9',
                        'hint' => $this->l('Upload image for the right column in the login pop up')
                    ),
                    array(
                        'label' => $this->l('Enable Facebook Login'),
                        'type' => 'switch',
                        'hint' => $this->l('Enable/Disable Facebook Login'),
                        'class' => 'optn_facebook',
                        'name' => 'advance_login[facebook_status]',
                        'values' => array(
                            array(
                            'value' => 1,
                            ),
                            array(
                            'value' => 0,
                            ),
                        ),
                    ),
                    array(
                        'label' => $this->l('Facebook App ID'),
                        'type' => 'text',
                        'hint' => $this->l('Enter the Facebook app id'),
                        'class' => 'optn_facebook',
                        'name' => 'advance_login[facebook_app_id]',
                    ),
                    array(
                        'label' => $this->l('Facebook App Secret'),
                        'type' => 'text',
                        'hint' => $this->l('Enter the Facebook app secret'),
                        'class' => 'optn_facebook',
                        'name' => 'advance_login[facebook_app_secret]',
                    ),
                    array(
                        'label' => $this->l('Enable Google Plus Login'),
                        'type' => 'switch',
                        'hint' => $this->l('Enable/Disable Google Plus Login'),
                        'class' => 'optn_google_plus',
                        'name' => 'advance_login[google_plus_status]',
                        'values' => array(
                            array(
                            'value' => 1,
                            ),
                            array(
                            'value' => 0,
                            ),
                        ),
                    ),
                    array(
                        'label' => $this->l('Google Plus Client ID'),
                        'type' => 'text',
                        'hint' => $this->l('Enter the Google plus client id'),
                        'class' => 'optn_google_plus',
                        'name' => 'advance_login[google_plus_client_id]',
                    ),
                    array(
                        'label' => $this->l('Google Plus Client Secret'),
                        'type' => 'text',
                        'hint' => $this->l('Enter the Google plus client secret'),
                        'class' => 'optn_google_plus',
                        'name' => 'advance_login[google_plus_client_secret]',
                    ),
                    array(
                        'label' => '',
                        'type' => 'hidden',
                        'name' => 'previous_value1',
                    ),
                    array(
                        'label' => '',
                        'type' => 'hidden',
                        'name' => 'previous_value2',
                    ),
                 ),
                    'submit' => array(
                        'title' => $this->l('   Save   '),
                        'class' => 'btn btn-default pull-right advancesave'
                    ),
               ),
            );
        $field_value=
            array(
            'advance_login[enable]' => $this->my_module_settings['enable'],
            'advance_login[show_on_login_page]' => $this->my_module_settings['show_on_login_page'],
            'advance_login[show_right_column]' => $this->my_module_settings['show_right_column'],
            'advance_login[facebook_status]' => $this->my_module_settings['facebook_status'],
            'advance_login[facebook_app_id]' => $this->my_module_settings['facebook_app_id'],
            'advance_login[facebook_app_secret]' => $this->my_module_settings['facebook_app_secret'],
            'advance_login[google_plus_status]' => $this->my_module_settings['google_plus_status'],
            'advance_login[google_plus_client_id]' => $this->my_module_settings['google_plus_client_id'],
            'advance_login[google_plus_client_secret]' => $this->my_module_settings['google_plus_client_secret'],
            'previous_value1' => '',
            'previous_value2'=> ''
                );
        
        $languages = Language::getLanguages();
        
        foreach ($languages as $k => $language) {
                            $languages[$k]['is_default'] =
                                    ((int)($language['id_lang'] == $this->context->language->id));
        }
        
        $helper = new HelperForm();
        $helper->module = $this;
        $helper->fields_value = $field_value;
        $helper->name_controller = $this->name;
        $helper->languages = $languages;
        $helper->default_form_language = $this->context->language->id;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->show_cancel_button= false;
        $helper->submit_action = $action;
        $form=$helper->generateForm(array($this->fields_form));
        
        $helper = new HelperView();
        $helper->module = $this;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->current = AdminController::$currentIndex . '&configure=' . $this->name;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->override_folder='helpers/';
        $helper->base_folder='view/';
        $helper->base_tpl = 'view_custom.tpl';
        
        $view = $helper->generateView();
        
        $this->context->smarty->assign('view', $view);
        $this->context->smarty->assign('product_tabs', $product_tabs);
        $this->context->smarty->assign('form', $form);
        $this->context->smarty->assign('firstCall', false);
        $this->context->smarty->assign('general_settings', $this->l('General Settings'));
        $this->context->smarty->assign('facebook_settings', $this->l('Facebook Settings'));
        $this->context->smarty->assign('google_settings', $this->l('Google Plus Settings'));
        $this->context->smarty->assign('enable_disable_right_column', $this->my_module_settings['show_right_column']);
        
        $tpl='Form_custom.tpl';
        $helper= new Helper();
        $helper->module=$this;
        $helper->override_folder='helpers/';
        $helper->base_folder='form/';
        $helper->setTpl($tpl);
        $tpl=$helper->generate();
        
        $output.=$tpl;
        return $output;
    }
    public function displayWarning($warn)
    {
        $str_output = '';
        if (!empty($warn)) {
            $str_output .= '<div class="module_warning alert alert-warning">
			<button type="button" class="close" data-dismiss="alert">×</button>'.
                        $warn.'</div>';
        }
       
        return $str_output;
    }

    public function addUser($user_data)
    {
        $col_exist=0;
        if (Customer::customerExists(strip_tags($user_data['email']))) {
            $customer_obj = new Customer();
            $customer_tmp = $customer_obj->getByEmail($user_data['email']);

            $customer = new Customer($customer_tmp->id);

            //Update Context
            $this->context->customer = $customer;
            $this->context->smarty->assign('confirmation', 1);
            $this->context->cookie->id_customer = (int) $customer->id;
            $this->context->cookie->customer_lastname = $customer->lastname;
            $this->context->cookie->customer_firstname = $customer->firstname;
            $this->context->cookie->passwd = $customer->passwd;
            $this->context->cookie->logged = 1;
            $this->context->cookie->email = $customer->email;
            $this->context->cookie->is_guest = $customer->is_guest;

            //Cart
            if (Configuration::get('PS_CART_FOLLOWING')
                    && (empty($this->context->cookie->id_cart)
                    || Cart::getNbProducts($this->context->cookie->id_cart) == 0)
                    && $id_cart = (int) Cart::lastNoneOrderedCart($this->context->customer->id)) {
                $this->context->cart = new Cart($id_cart);
            } else {
                $id_carrier = (int) $this->context->cart->id_carrier;
                $this->context->cart->id_carrier = 0;
                $this->context->cart->setDeliveryOption(null);
                $this->context->cart->id_address_delivery = (int)
                        Address::getFirstCustomerAddressId((int) $customer->id);
                $this->context->cart->id_address_invoice = (int)
                        Address::getFirstCustomerAddressId((int) $customer->id);
            }
            $this->context->cart->secure_key = $customer->secure_key;

            if (isset($id_carrier) && $id_carrier && Configuration::get('PS_ORDER_PROCESS_TYPE')) {
                $delivery_option = array($this->context->cart->id_address_delivery => $id_carrier . ',');
                $this->context->cart->setDeliveryOption($delivery_option);
            }
            $this->context->cart->save();
            $this->context->cookie->id_cart = (int) $this->context->cart->id;
            $this->context->cookie->write();
            $this->context->cart->autosetProductAddress();
        } else {
            $insertion_time = date('Y-m-d H:i:s', time());
            $original_passd = Tools::substr(md5(uniqid(mt_rand(), true)), 0, 8);
            $passd = Tools::encrypt($original_passd);
            $secure_key = md5(uniqid(rand(), true));
            $gender_qry = '(select id_gender from ' . _DB_PREFIX_ . 'gender '
                    . 'where type = ' . pSQL($user_data['gender']) . ')';
            $gender = Db::getInstance()->getRow($gender_qry);
            if (empty($gender)) {
                $gender['id_gender'] = 0;
            }
            $sql = 'INSERT INTO ' . _DB_PREFIX_ . 'customer SET 
				id_shop_group = ' . (int) $this->context->shop->id_shop_group . ', 
				id_shop = ' . (int) $this->context->shop->id . ', 
				id_gender = ' . (int) $gender['id_gender'] . ', 
				id_default_group = ' . (int) Configuration::get('PS_CUSTOMER_GROUP') . ',';
            if ($col_exist == 1) {
                $sql .= 'id_lang = ' . (int) $this->context->language->id . ',';
            }
            $sql .= 'id_risk = 0, 
				firstname = "' . pSQL(strip_tags($user_data['first_name'])) . '", 
				lastname = "' . pSQL(strip_tags($user_data['last_name'])) . '", 
				email = "' . pSQL(strip_tags($user_data['email'])) . '", 
				passwd = "' . pSQL($passd) . '", 
				max_payment_days = 0, 
				secure_key = "' . pSQL($secure_key) . '", 
				active = 1, date_add = "' . pSQL($insertion_time) . '", date_upd = "' . pSQL($insertion_time) . '"';

            Db::getInstance()->execute($sql);
            $id_customer = Db::getInstance()->Insert_ID();
            $customer = new Customer();
            $customer->id = $id_customer;
            $customer->firstname = ucwords($user_data['first_name']);
            $customer->lastname = ucwords($user_data['last_name']);
            $customer->passwd = $passd;
            $customer->email = $user_data['email'];
            $customer->secure_key = $secure_key;
            $customer->birthday = '';
            $customer->is_guest = 0;
            $customer->active = 1;
            $customer->logged = 1;

            $customer->cleanGroups();
            $customer->addGroups(array((int) Configuration::get('PS_CUSTOMER_GROUP')));

            $this->sendConfirmationMail($customer, $original_passd);
            //Update Context
            $this->context->customer = $customer;
            $this->context->smarty->assign('confirmation', 1);
            $this->context->cookie->id_customer = (int) $customer->id;
            $this->context->cookie->customer_lastname = $customer->lastname;
            $this->context->cookie->customer_firstname = $customer->firstname;
            $this->context->cookie->passwd = $customer->passwd;
            $this->context->cookie->logged = 1;
            $this->context->cookie->email = $customer->email;
            $this->context->cookie->is_guest = $customer->is_guest;
            //Cart
            if (Configuration::get('PS_CART_FOLLOWING')
                    && (empty($this->context->cookie->id_cart)
                    || Cart::getNbProducts($this->context->cookie->id_cart) == 0)
                    && $id_cart = (int) Cart::lastNoneOrderedCart($this->context->customer->id)) {
                $this->context->cart = new Cart($id_cart);
            } else {
                $id_carrier = (int) $this->context->cart->id_carrier;
                $this->context->cart->id_carrier = 0;
                $this->context->cart->setDeliveryOption(null);
                $this->context->cart->id_address_delivery = (int)
                        Address::getFirstCustomerAddressId((int) $customer->id);
                $this->context->cart->id_address_invoice = (int)
                        Address::getFirstCustomerAddressId((int) $customer->id);
            }
            $this->context->cart->secure_key = $customer->secure_key;

            if (isset($id_carrier) && $id_carrier && Configuration::get('PS_ORDER_PROCESS_TYPE')) {
                $delivery_option = array($this->context->cart->id_address_delivery => $id_carrier . ',');
                $this->context->cart->setDeliveryOption($delivery_option);
            }
            $this->context->cart->save();
            $this->context->cookie->id_cart = (int) $this->context->cart->id;
            $this->context->cookie->write();
            $this->context->cart->autosetProductAddress();
        }
        return 1;
    }
    private function getModuleDirUrl()
    {
        $module_dir = '';
        if ($this->checkSecureUrl()) {
            $module_dir = _PS_BASE_URL_SSL_ . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', _PS_MODULE_DIR_);
        } else {
            $module_dir = _PS_BASE_URL_ . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', _PS_MODULE_DIR_);
        }
        return $module_dir;
    }

    private function checkSecureUrl()
    {
        $custom_ssl_var = 0;
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            $custom_ssl_var = 1;
        }
        if ((bool) Configuration::get('PS_SSL_ENABLED') && $custom_ssl_var == 1) {
            return true;
        } else {
            return false;
        }
    }

    protected function sendConfirmationMail($customer, $passd)
    {
        if (!Configuration::get('PS_CUSTOMER_CREATION_EMAIL')) {
            return true;
        }

        $contact_us_link = '';
        if (_PS_VERSION_ >= 1.5) {
            $contact_us_link = $this->context->link->getPageLink('contact');
        } else {
            $contact_us_link = $this->context->link->getPageLink('contact-form');
        }
        return Mail::Send(
            $this->context->language->id,
            'account',
            Mail::l('Welcome!'),
            array(
                    '{firstname}' => $customer->firstname,
                    '{lastname}' => $customer->lastname,
                    '{email}' => $customer->email,
                    '{passwd}' => $passd,
                    '{contact_us_link}' => $contact_us_link,
                    '{new_arrivals_link}' => $this->context->link->getPageLink('new-products'),
                    '{specials_link}' => $this->context->link->getPageLink('prices-drop'),
                    '{best_seller_link}' => $this->context->link->getPageLink('best-sales'),
                    '{module_img_url}' => $this->getModuleDirUrl() . 'advancelogin/views/img/'
                    ),
            $customer->email,
            $customer->firstname . ' ' . $customer->lastname,
            null,
            null,
            null,
            null,
            dirname(__FILE__) . '/mails/'
        );
    }

    // Send Registration mail
//    protected function sendConfirmationMail($customer, $passd)
//    {
//        if (!Configuration::get('PS_CUSTOMER_CREATION_EMAIL')) {
//            return true;
//        }
//
//        return Mail::Send(
//            $this->context->language->id,
//            'account',
//            Mail::l('Welcome!'),
//            array(
//            '{firstname}' => $customer->firstname,
//            '{lastname}' => $customer->lastname,
//            '{email}' => $customer->email,
//            '{passwd}' => $passd),
//            $customer->email,
//            $customer->firstname . ' ' . $customer->lastname,
//            null,
//            null,
//            null,
//            null,
//            dirname(__FILE__) . '/mails/'
//        );
//    }
    // Default FB and Google settings
    private function defaultTitles($tag)
    {
        $module_dir = $this->getModulePath();
        
        $titles = array(
            'facebook_status' => 0,
            'facebook_app_id' => '',
            'facebook_app_secret' => '',
            'google_plus_status' => 0,
            'google_plus_client_id' => '',
            'google_plus_client_secret' => '',
            'google_plus_image' => $module_dir . 'advancelogin/views/img/admin/buttons/google_small.png',
            'google_plus_large_image' => $module_dir . 'advancelogin/views/img/admin/buttons/google_large.png',
            'facebook_image' => $module_dir . 'advancelogin/views/img/admin/buttons/fb_small.png',
            'facebook_large_image' => $module_dir . 'advancelogin/views/img/admin/buttons/fb_large.png',
        );
        return $titles[$tag];
    }

    // Function to get default setting values
    private function getDefaultSettings()
    {
        $module_dir = $this->getModulePath();
        
        $settings = array(
            'enable' => 0,
            'show_right_column' => 'disable',
            'default' => $module_dir . 'advancelogin/views/img/admin/default.jpg',
            'bannerimage1' => 'default.jpg',
            'show_on_login_page' => 'do_not_show',
        );
        $settings['facebook_status'] = $this->defaultTitles('facebook_status');
        $settings['google_plus_status'] = $this->defaultTitles('google_plus_status');
        $settings['facebook_app_id'] = $this->defaultTitles('facebook_app_id');
        $settings['facebook_app_secret'] = $this->defaultTitles('facebook_app_secret');
        $settings['facebook_image'] = $this->defaultTitles('facebook_image');
        $settings['facebook_large_image'] = $this->defaultTitles('facebook_large_image');
        $settings['google_plus_client_id'] = $this->defaultTitles('google_plus_client_id');
        $settings['google_plus_client_secret'] = $this->defaultTitles('google_plus_client_secret');
        $settings['google_plus_image'] = $this->defaultTitles('google_plus_image');
        $settings['google_plus_large_image'] = $this->defaultTitles('google_plus_large_image');
        return $settings;
    }
    // To Get Module path
    private function getModulePath()
    {
        $custom_ssl_var = 0;
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            $custom_ssl_var = 1;
        }
        if ((bool) Configuration::get('PS_SSL_ENABLED') && $custom_ssl_var == 1) {
            $module_dir = _PS_BASE_URL_SSL_ . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', _PS_MODULE_DIR_);
        } else {
            $module_dir = _PS_BASE_URL_ . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', _PS_MODULE_DIR_);
        }
        return $module_dir;
    }
    public function hookdisplayHeader()
    {
        $custom_ssl_var = 0;
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            $custom_ssl_var = 1;
        }
        if ((bool) Configuration::get('PS_SSL_ENABLED') && $custom_ssl_var == 1) {
            $module_dir = _PS_BASE_URL_SSL_ . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', _PS_MODULE_DIR_);
        } else {
            $module_dir = _PS_BASE_URL_ . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', _PS_MODULE_DIR_);
        }
        $config = Configuration::get('Advance_Login');
        $this->my_module_settings = Tools::unSerialize($config);
        $settings = $this->my_module_settings;
        if (!$this->context->customer->isLogged()) {
            if (isset($settings['enable']) && $settings['enable'] == 1) {
                if (isset($settings['show_right_column']) && $settings['show_right_column'] == 'enable') {
                    if (isset($settings['bannerimage2'])) {
                        $this->context->smarty->assign('rightsideimage', $settings['bannerimage2']);
                    } else {
                        $this->context->smarty->assign('rightsideimage', '');
                    }
                }
                $link = new Link();
                $h = Configuration::get('PS_SSL_ENABLED');
                $login_action = __PS_BASE_URI__ . 'index.php?fc=module&module=advancelogin&controller=login';
                if (isset($settings['facebook_status']) && $settings['facebook_status'] == 1) {
                    $facebook_action = $link->getModuleLink('advancelogin', 'facebook', array(), $h) . "?type=fb";
                } else {
                    $facebook_action = "";
                }
                if (isset($settings['google_plus_status']) && $settings['google_plus_status'] == 1) {
                    $google_action = $link->getModuleLink('advancelogin', 'google', array(), $h) . "?type=google";
                } else {
                    $google_action = "";
                }
                $sign_up_action = $link->getPageLink('authentication');
                $forgot_password_action = $link->getPageLink('password');
                $this->context->smarty->assign('path', $module_dir);
                $this->context->smarty->assign('login_action', $login_action);
                $this->context->smarty->assign('sign_up_action', $sign_up_action);
                $this->context->smarty->assign('facebook_action', $facebook_action);
                $this->context->smarty->assign('google_action', $google_action);
                $this->context->smarty->assign('forgot_password_action', $forgot_password_action);
                $this->context->smarty->assign('imagetype', $settings['show_on_login_page']);
                if (isset($settings['bannerimage1'])) {
                    $this->context->smarty->assign('backgroundimage', $settings['bannerimage1']);
                } else {
                    $this->context->smarty->assign('backgroundimage', '');
                }
                $this->context->controller->addCSS($this->_path . 'views/css/front/advance_login.css');
                $this->context->controller->addjs($this->_path . 'views/js/front/advance_login.js');
                $this->context->controller->addjs($this->_path . 'views/js/velovalidation.js');
                /* changes by rishabh jain
                 * To add compatibility with thirtybees
                 */
                
                if (defined('_TB_VERSION_')) {
                    $this->context->smarty->assign('is_tb', 1);
                } else {
                    $this->context->smarty->assign('is_tb', 0);
                }
                return $this->display(__FILE__, 'views/templates/hook/advancelogin.tpl');
            }
        }
    }
//For social login
    public function hookActionCustomerLogoutBefore()
    {
        $outh_client_obj = new oauth_client_class();
        $outh_client_obj->SessionExpireOut();
    }
}
