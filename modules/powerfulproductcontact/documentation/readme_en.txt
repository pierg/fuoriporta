Installation
============

The installation of this module is pretty easy.
All you have to do is to go in your modules listing in your Prestashop backoffice, click in "Add a new module", select the archive and voilà ! :)

Once installed, you will be able to configure it.
You should normally be on the configuration page of the module right after it's installation.
If that's not the case, simply go in Modules, search for our module and click on "Configure."

Configuration
=============

The configuration page is splitted in two categories : Settings and Fields.

Settings
--------

The settings are here to manage how the form works.
For that, you'll be able to define various informations, including :

 * The contact whom will receive the notifications. (You can manage contact directly in Prestashop, in Clients > Contacts)
 * The title of your form
 * A message to display just before the fields (optionnal)
 * A thank you message (optionnal)
 * The position of the form in the product page, You can choose between :
    * The left column of the product page (Before the "Print" button)
    * The right column (After the "Add to card" block)
    * The footer (after the description, before the tabs.)
 * The categories in which the form will be available.


Managing the fields
-------------------

When creating/upadting a field, you must fill various values.

You need to know the specific field "values", that is only used in these type of field :

 * Select : The field values will be a list of comma separated values that will be related to every entry in the select field.
 * Radio : The field values will be a list of comma separated values that will be displayed as each radio butons
 * File : The field values will be a list of comma separated values that represent the allowed file formats, without the dot.

The "extra" part is here to let you customize the field by adding more attributes.
You can for example set the extra to "multiple" to add a "multiple" select choice.


Contact
=======

You find a bug, you have some problems configuring/installing/making this module works?
Feel free to contact me at https://addons.prestashop.com/contact-community.php?id_product=17761

I'll do my best to help you.


Thanks !
========

You purchased my module, and for that, I wanted to thank you !
I hope you'll find what you need.
