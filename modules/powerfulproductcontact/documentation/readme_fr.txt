Installation
============

L'installation de ce module est très simple.
Rendez-vous simplement dans l'administration de votre site Prestashop, onglet "Module", cliquez sur "Ajouter un nouveau module", sélectionnez l'archive et voilà ! :)

Une fois installé, vous serez en mesure de le configurer.
Vous devriez être sur la page de configuration du module juste après son installation.
Si ce n'est pas le cas, cherchez simplement le module depuis la page de Modules de Prestashop et cliquez sur configurer.

Configuration
=============

La page de configuration est séparée en deux parties : Paramètres et Champs.

Partie paramètres
-----------------

La partie paramètres permet de configurer le module et son comportement dans la page produit.
Pour cela vous pouvez définir diverses informations, dont :

 * Le contact qui recevra les soumissions. Vous pouvez gérer les contacts dans la partie Client > Contacts de Prestashop.
 * Le titre du formulaire
 * Un message qui sera affiché juste avant les champs (optionnel).
 * Un message de remerciement (optionnel). Ce message sera affiché à vos clients une fois le formulaire soumis.
 * La position du formulaire dans la page produit. Vous avez la possibilité entre :
     * Dans la colonne de gauche (Avant le bouton imprimer)
     * Dans la colonne de droite (après la partie "Ajouter au panier")
     * En pied de page, juste après la description et avant les onglets.
 * La/Les catégories ou le formulaire sera visible.


Gestion des champs
------------------

Lors de la création/modification d'un champ, vous pouvez remplir les différentes valeurs.
Il faut savoir que le champs "Valeurs" n'est utilisé que si le type est défini à :

 * Select : Le champs valeurs correspondra alors à une liste de valeurs, séparée par des virgules
 * Radio : Le champs valeurs correspondra alors à une liste de boutons radio à afficher avec le texte à droite, séparé par des virgules
 * Fichier : Une liste d'extensions autorisées, séparée par des virgules, sans le point.

La partie extra permet d'ajouter des attributs au champ, tel que la valeur "multiple" pour permettre une sélection multiple.


Contact
=======

Vous avez découvert un bug, vous avez des problèmes pour installer/configurer/utiliser ce module ?
N'hésitez pas à me contacter via Addons :
https://addons.prestashop.com/contact-community.php?id_product=17761

Et je ferai de mon mieux pour vous aider.


Merci !
=======

Vous avez acheté un de mes modules, et pour ca je tenais à vous remercier.
