Installation
============

Die Installation dieses Moduls ist recht einfach.
Alles was Sie tun müssen, ist, in Ihre Module Auflistung in Ihrem Prestashop Backoffice gehen, klicken Sie in "Hinzufügen eines neuen Moduls", wählen Sie das Archiv und voilà! :)

Einmal installiert, werden Sie in der Lage, es zu konfigurieren.
Sie sollte in der Regel auf der Konfigurationsseite des Moduls direkt nach der Installation ist es zu sein.
Wenn das nicht der Fall ist, einfach in Module zu gehen, suchen Sie nach unserem Modul, und klicken Sie auf "Konfigurieren".

Configuration
=============

Einstellungen und Felder: Die Konfigurationsseite ist in zwei Kategorien aufgeteilt.

Einstellungen
--------

Die Einstellungen sind hier, um zu verwalten, wie das Formular funktioniert.
Dafür werden Sie in der Lage sein verschiedene Informationen, und zwar auch:

 * Der Kontakt, den werden die Benachrichtigungen zu erhalten. (Sie können Ihre Kontaktdaten direkt im Prestashop verwalten, in Clients> Kontakte)
 * Der Titel des Formulars
 * Eine Mitteilung an, kurz bevor die Felder angezeigt werden (Optionnal)
 * Ein Dankeschön-Nachricht (Optionnal)
 * Die Position der Form, in der Produktseite, können Sie wählen zwischen:
    * In der linken Spalte der Produktseite (vor der Schaltfläche "Drucken")
    * Die rechte Spalte (nach dem "zu Karte hinzufügen" Block)
    * Die Fußzeile (nach der Beschreibung vor den Registerkarten.)
 * Die Kategorien, in denen die Form zur Verfügung stehen.


Verwalten Sie die Felder
-------------------

Beim Erstellen / upadting ein Feld, müssen Sie verschiedene Werte zu füllen.

Sie müssen die spezifischen Bereich "Werte" wissen, dass nur in dieser Art von Feld benutzt:

 * Wählen Sie: Die Feldwerte wird eine Liste der durch Komma getrennte Werte, die für jeden Eintrag in der Auswahlfeld bezogen werden wird.
 * Radio: Die Feldwerte wird eine Liste der durch Komma getrennte Werte, die als je Funk butons angezeigt werden wird
 * File: Die Feldwerte wird eine Liste der durch Komma getrennte Werte, die die zulässigen Dateiformate darstellen, ohne den Punkt zu sein.

Die "extra" Teil ist hier das Feld anpassen, indem Sie mehrere Attribute zu lassen.
Sie können beispielsweise festgelegt, um die extra "multiple", um eine "multiple" erlesene Auswahl hinzuzufügen.


Berührung
=======

Sie einen Fehler finden, haben Sie einige Probleme bei der Konfiguration / Installation / machen dieses Modul funktioniert?
Fühlen Sie sich frei, mich zu kontaktieren https://addons.prestashop.com/contact-community.php?id_product=17761

Ich werde mein Bestes tun, um Ihnen zu helfen.


Vielen Dank !
========

Sie kaufte meine Modul, und dafür wollte ich Ihnen danken!
Ich hoffe, Sie finden, was Sie brauchen.
