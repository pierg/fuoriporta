Installazione
============

L'installazione di questo modulo è abbastanza facile.
Tutto quello che dovete fare è andare nel vostro moduli elenco nel tuo backoffice Prestashop, fate clic su "Aggiungi un nuovo modulo", selezionare l'archivio e voilà! :)

Una volta installato, si sarà in grado di configurarlo.
Di norma deve essere nella pagina di configurazione del modulo subito dopo è l'installazione.
Se questo non è il caso, basta andare in moduli, cercare il nostro modulo e fare clic su "Configura".

Configurazione
=============

La pagina di configurazione è diviso in due categorie: Impostazioni e Campi.

Impostazioni
--------

Le impostazioni sono qui per gestire il funzionamento del modulo.
Per questo, sarete in grado di definire varie informazioni, tra cui:

 * Il contatto chi riceverà le notifiche. (È possibile gestire direttamente in contatto Prestashop, a Clienti> Contatti)
 * Il titolo del modulo
 * Un messaggio da visualizzare appena prima che i campi (facoltativo)
 * Un grazie messaggio (facoltativo)
 * La posizione del modulo nella pagina del prodotto, è possibile scegliere tra:
    * La colonna di sinistra della pagina del prodotto (prima il pulsante "Stampa")
    * La colonna di destra (dopo il blocco "Aggiungi carta")
    * Il piè di pagina (dopo la descrizione, prima che le schede.)
 * Le categorie in cui la forma saranno disponibili.


Gestire i campi
-------------------

Quando si crea / upadting un campo, è necessario compilare i vari valori.

È necessario conoscere i "valori" di campo specifico, che viene utilizzato solo in questo tipo di campo:

 * Seleziona: I valori di campo sarà un elenco di valori separati da virgola che saranno legati a ogni voce nel campo di selezione.
 * Radio: I valori di campo sarà un elenco di valori separati da virgola che verrà visualizzato come ogni butons radiofoniche
 * File: I valori di campo sarà un elenco di valori separati da virgola che rappresentano i formati di file consentiti, senza il punto.

La parte "extra" è qui che consentono di personalizzare il campo con l'aggiunta di più attributi.
Ad esempio, è possibile impostare un extra per "multipla" per aggiungere una "multipla" selezionare la scelta.


Contatto
=======

Si trova un bug, avete alcuni problemi la configurazione / installazione / rendendo Questo modulo funziona?
Non esitate a contattarmi al https://addons.prestashop.com/contact-community.php?id_product=17761

Farò del mio meglio per aiutarvi.


Grazie!
========

Hai acquistato il mio modulo, e per questo, ho voluto grazie!
Spero che troverete quello che vi serve.
