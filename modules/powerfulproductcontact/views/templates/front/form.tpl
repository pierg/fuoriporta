{**
* @package   Powerful Product contact
* @author    Cyril Nicodème <contact@prestaddons.net>
* @copyright Copyright (C) June 2014 cnicodeme.com <@email:contact@prestaddons.net>. All rights reserved.
* @since     2014-09-19
* @version   1.8.6
* @license   Nicodème Cyril
*}

{if isset($errors) && $errors}

	{literal}
		<script>
	        $(document).ready(function (){

	                $('html, body').animate({
	                    scrollTop: $(".ppc-forms").offset().top
	                }, 2000);

	        });
	    </script>
	{/literal}

{/if}

{if isset($successSent)}
	{if isset($successMessage) && !empty($successMessage)}
		{literal}
			<script>
		        $(document).ready(function (){

		                $('html, body').animate({
		                    scrollTop: $("#toptop").offset().top
		                }, 2000);



		        });



		    </script>
		{/literal}
	{/if}
{/if}

<div id="toptop">

{if isset($successSent)}
	{if isset($successMessage) && !empty($successMessage)}
	<div class="alert alert-info">{$successMessage|escape:'quotes':'UTF-8'}</div>
	{else}
	<p class="alert alert-info">{l s='Your message has been successfully sent to our team.' mod='powerfulproductcontact'}</p>
	{/if}
	{/if}
<style>
#field_privacy{

	margin: 0px !important;
	    height: auto !important;


}
</style>
{if ($id_customer)}
	<button  id="showmenu" class="btn btn-lg btn-primary" data-toggle="modal" data-toggle="modal" data-target="#Modal" type="button"> {$title|escape:'quotes':'UTF-8'}</button >
{else}
	<a class="login btn btn-lg btn-primary" href="javascript:void(0)" rel="nofollow" title="Accedi al tuo account cliente" id="modal_trigger" onclick="showLoginPopup()">
		{$title|escape:'quotes':'UTF-8'}
	</a>
{/if}

<form id="foo" action="{$request_uri|escape:'quotes':'UTF-8'}" method="post" class="std ppc-forms zx" enctype="multipart/form-data" id="contact">
		<fieldset>
			<div class="headline">
				{*<h3>{$title|escape:'quotes':'UTF-8'}</h3>*}


			</div>


			<div class="fmenu" style="display: none;">
					{include file="$tpl_dir./errors.tpl"}

					{if isset($message)}
					{$message|escape:'quotes':'UTF-8'}
					{/if}

					{foreach from=$fields item=field} {* {$field|@var_dump} *}


						{if ($field.name=="privacy")}

							<fieldset class="account_creation customerprivacy">
								<h3 class="page-subheading">
									Privacy dei dati dei clienti
								</h3>
								<div style="width:21px; float:left;">
									<div class="required checkbox">
										<div class="checker" id="uniform-customer_privacy">
											<span>
											<input type="checkbox" value="1" id="field_privacy" required class="form-control" name="privacy" autocomplete="off">
											</span>
										</div>
									</div>
								</div>
								<div style="width: 92%; float: left; margin-top: 8px;">
							        <label for="field_privacy" style="font-weight: normal;">
							        	<p>
							        		<span>
												HO LETTO E ACCETTO <a href="{$link->getCmsLink(2)}" title="Privacy" target="_blank" rel="nofollow">IL TRATTAMENTO DEI MIEI DATI PERSONALI </a> <em class="required">*</em>
									        </span>
									    </p>
									</label>
								</div>
							</fieldset>



						{else}

							{if $field.type == 'recaptcha'}
							<p class="recpatcha">
								{$field.element|escape:'quotes':'UTF-8'}
							</p>
							{elseif $field.name == 'email'}
								<p class="{$field.type|escape:'html':'UTF-8'}" style="display: none">
									<label for="{$field.id|escape:'quotes':'UTF-8'}">{$field.label|escape:'quotes':'UTF-8'}{if $field.required} <em class="required">*</em>{/if}</label>
									<input type="email" name="email" id="field_email" required="" class="form-control" value="{$email_customer}">

								</p>
							{elseif $field.type != 'hidden'}
							<p class="{$field.type|escape:'html':'UTF-8'}">
								<label for="{$field.id|escape:'quotes':'UTF-8'}">{$field.label|escape:'quotes':'UTF-8'}{if $field.required} <em class="required">*</em>{/if}</label>
								{$field.element|escape:'quotes':'UTF-8'}
							</p>
							{else}
							{$field.element|escape:'quotes':'UTF-8'}
							{/if}
						{/if}

					{/foreach}




					<p class="submit">
						<input type="submit" name="submitMessage" id="submitMessage" value="{l s='Send' mod='powerfulproductcontact'}" {if $isv16}class="button btn btn-default button-medium" style="padding: 10px 20px"{else}class="button_large"{/if} />
					</p>
			</div>
		</fieldset>
	</form>
</div>
