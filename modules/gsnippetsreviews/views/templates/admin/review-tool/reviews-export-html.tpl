{*
* 2003-2017 Business Tech
*
* @author Business Tech SARL
* @copyright  2003-2017 Business Tech SARL
*}

<div id="gsr" class="bootstrap">
	{if !empty($bUpdate)}
		<div class="clr_20"></div>

		{if !empty($iSuccess)}
			<div class="alert alert-success col-xs-12 col-sm-12 col-md-12 col-lg-12">
				{if !empty($sLanguage)}
					{l s='You have exported your reviews for this language:' mod='gsnippetsreviews'} <b>{$sLanguage}</b>
					<div class="clr_10"></div>
				{/if}
				{l s='The csv file to export your reviews has been created well in the "export" folder (into our module folder) with the current date. You could download it here:' mod='gsnippetsreviews'} <a href="{$sFileUrl|escape:'htmlall':'UTF-8'}{$sFilename|escape:'htmlall':'UTF-8'}{$sExtension|escape:'htmlall':'UTF-8'}" target="_blank"><b>{$sFilename}</b></a>
			</div>

			<div class="clr_10"></div>

			{if !empty($aReviews)}
				<div class="center">
					<span onclick="$('#bt_export-reviews').slideToggle();" class="btn btn-info btn-lg">{l s='See the list of the reviews you just have exported' mod='gsnippetsreviews'}&nbsp;<span class="icon-eye-open"></span></span>
				</div>

				<div id="bt_export-reviews" style="display: none;">
					<div class="clr_20"></div>

					<div class="alert alert-info col-xs-12 col-sm-12 col-md-12 col-lg-12">
						{l s='NOTE: The existing columns into the CSV files are organized according to our review system. It is also possible some review lines get only a rating, and not a related title and comment, it\'s based on the way of the module has been configured and if you have forced customers to write a review or not' mod='gsnippetsreviews'}
					</div>

					<div class="clr_10"></div>

					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<table class="table table-striped table-responsive">
							<thead>
							<tr>
								<th class="col-xs-12 col-sm-12 col-md-1 col-lg-1 title_box center"><strong>{l s='Shop name' mod='gsnippetsreviews'}</strong></th>
								<th class="col-xs-12 col-sm-12 col-md-1 col-lg-1 title_box center"><strong>{l s='Product name' mod='gsnippetsreviews'}</strong></th>
								<th class="col-xs-12 col-sm-12 col-md-1 col-lg-1 title_box center"><strong>{l s='Adding date' mod='gsnippetsreviews'}</strong></th>
								<th class="col-xs-12 col-sm-12 col-md-1 col-lg-2 title_box center"><strong>{l s='Customer name' mod='gsnippetsreviews'}</strong></th>
								<th class="col-xs-12 col-sm-12 col-md-1 col-lg-1 title_box center"><strong>{l s='Customer e-mail' mod='gsnippetsreviews'}</strong></th>
								<th class="col-xs-12 col-sm-12 col-md-1 col-lg-1 title_box center"><strong>{l s='Language' mod='gsnippetsreviews'}</strong></th>
								<th class="col-xs-12 col-sm-12 col-md-1 col-lg-1 title_box center"><strong>{l s='Review status' mod='gsnippetsreviews'}</strong></th>
								<th class="col-xs-12 col-sm-12 col-md-1 col-lg-1 title_box center"><strong>{l s='Rating' mod='gsnippetsreviews'}</strong></th>
								<th class="col-xs-12 col-sm-12 col-md-1 col-lg-1 title_box center"><strong>{l s='Review title' mod='gsnippetsreviews'}</strong></th>
								<th class="col-xs-12 col-sm-12 col-md-1 col-lg-2 title_box center"><strong>{l s='Review comment' mod='gsnippetsreviews'}</strong></th>
							</tr>
							</thead>
							{foreach from=$aReviews item=text item=aItem}
								<tr>
									<td class="col-xs-12 col-sm-12 col-md-1 col-lg-1 center">
										{$aItem.shopName}
									</td>
									<td class="col-xs-12 col-sm-12 col-md-1 col-lg-1 center">
										{$aItem.name}
									</td>
									<td class="col-xs-12 col-sm-12 col-md-1 col-lg-1 center">
										{$aItem.dateAdd}
									</td>
									<td class="col-xs-12 col-sm-12 col-md-2 col-lg-2 center">
										{if !empty($aItem.firstname)}{$aItem.firstname|escape:'UTF-8'}{/if}{if !empty($aItem.lastname)} {$aItem.lastname|upper|escape:'UTF-8'}{/if}
									</td>
									<td class="col-xs-12 col-sm-12 col-md-1 col-lg-1 center">
										{$aItem.email}
									</td>
									<td class="col-xs-12 col-sm-12 col-md-1 col-lg-1 center">
										{$aItem.langTitle}
									</td>
									<td class="col-xs-12 col-sm-12 col-md-1 col-lg-1 center">
										{if !empty($aItem.status)}{l s='Active' mod='gsnippetsreviews'}{else}{l s='Deactive' mod='gsnippetsreviews'}{/if}
									</td>
									<td class="col-xs-12 col-sm-12 col-md-1 col-lg-1 center">
										{$aItem.note} / {$iMaxRating|intval}
									</td>
									<td class="col-xs-12 col-sm-12 col-md-1 col-lg-1 center">
										{if !empty($aItem.review.data.sTitle)}{$aItem.review.data.sTitle}{/if}
									</td>
									<td class="col-xs-12 col-sm-12 col-md-2 col-lg-2 center">
										{if !empty($aItem.review.data.sComment)}{$aItem.review.data.sComment}{/if}
									</td>
								</tr>
							{/foreach}
						</table>
					</div>
				</div>
			{/if}
		{else}
			<div class="alert alert-danger col-xs-12 col-sm-12 col-md-12 col-lg-12">
				{l s='We couldn\'t write the csv file into the "export" folder! Here is the file name we tried to write and its path:' mod='gsnippetsreviews'} <b>{$sFilename}</b>
			</div>
		{/if}
	{elseif !empty($aErrors)}
		{include file="`$sErrorInclude`" aErrors=$aErrors}
	{/if}
</div>