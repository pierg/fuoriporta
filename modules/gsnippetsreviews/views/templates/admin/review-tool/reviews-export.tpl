{*
* 2003-2017 Business Tech
*
* @author Business Tech SARL
* @copyright  2003-2017 Business Tech SARL
*}
<script type="text/javascript">
	{literal}
	var oExportCallBack =
	[{
		//'name' : 'updatePrerequisites',
		//'url' : '{/literal}{$sURI}{literal}',
		//'params' : '{/literal}{$sCtrlParamName|escape:'htmlall':'UTF-8'}{literal}={/literal}{$sController|escape:'htmlall':'UTF-8'}{literal}&sAction=display&sType=prerequisites',
		//'toShow' : 'bt_prerequisites-settings',
		//'toHide' : 'bt_prerequisites-settings',
		//'bFancybox' : false,
		//'bFancyboxActivity' : false,
		//'sLoadbar' : null,
		//'sScrollTo' : null,
		//'oCallBack' : {}
	}];
	{/literal}
</script>

<div class="bootstrap">
	<form class="form-horizontal col-xs-12 col-md-12 col-lg-12" action="{$sURI|escape:'htmlall':'UTF-8'}" method="post" id="bt_export-form" name="bt_export-form">
		<input type="hidden" name="sAction" value="{$aQueryParams.export.action|escape:'htmlall':'UTF-8'}" />
		<input type="hidden" name="sType" value="{$aQueryParams.export.type|escape:'htmlall':'UTF-8'}" />
		<input type="hidden" name="sDisplay" id="sExportDisplay" value="{if !empty($sDisplay)}{$sDisplay|escape:'htmlall':'UTF-8'}{else}export{/if}" />

		{if empty($sDisplay) || (!empty($sDisplay) && $sDisplay == 'export')}
			<h3><i class="icon icon-upload"></i>&nbsp;{l s='Export your reviews' mod='gsnippetsreviews'}</h3>
			<div class="clr_10"></div>
			{if !empty($bUpdate)}
				{include file="`$sConfirmInclude`"}
				<div class="clr_10"></div>
			{elseif !empty($aErrors)}
				{include file="`$sErrorInclude`" aErrors=$aErrors}
				<div class="clr_10"></div>
			{/if}

			<div class="clr_10"></div>

			<div class="form-group">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="alert alert-info">
						{l s='Technically speaking, you can export the entire list of your ratings / reviews, or decide to export them by language. A CSV file will be generated and download automatically when you click on the export button.' mod='gsnippetsreviews'}
					</div>
				</div>
			</div>

			<div class="clr_10"></div>

			<div class="form-group">
				<label class="control-label col-xs-12 col-sm-12 col-md-2 col-lg-2">
					<b>{l s='Export your reviews' mod='gsnippetsreviews'}</b> :
				</label>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<table class="table table-striped col-xs-12 bt-table-info">
						<tr>
							<td class="center"><i class="icon icon-download-alt"></i>&nbsp;<b>{l s='Total reviews to export' mod='gsnippetsreviews'}</b></td>
							<td class="center"><i class="icon icon-filter"></i>&nbsp;<b>{l s='Language' mod='gsnippetsreviews'}</b></td>
							<td class="center"><i class="icon icon-file"></i>&nbsp;<b>{l s='UTF-8' mod='gsnippetsreviews'}</b></td>
							<td class="center"><b>{l s='Action' mod='gsnippetsreviews'}</b></td>
						</tr>
						<tr>
							<td class="center">
								<div class="filter">
									{if !empty($iTotalReviews)}{$iTotalReviews|intval}{else}{l s='There aren\'t any reviews' mod='gsnippetsreviews'}{/if}
								</div>
							</td>
							<td class="center">
								<div class="filter">
									<select name="bt_export-review-lang" id="bt_export-review-lang">
										<option value="0"> -- {l s='all' mod='gsnippetsreviews'} --</option>
										{foreach from=$aLangs item=aLang}
											<option value="{$aLang.id_lang|escape:'htmlall':'UTF-8'}">{$aLang.name|escape:'htmlall':'UTF-8'}</option>
										{/foreach}
									</select>
								</div>
							</td>
							<td class="center">
								<div class="filter">
									<input type="checkbox" name="bt_export-utf8" id="bt_export-utf8" checked="checked" />
								</div>
							</td>
							<td class="center">
								<input type="button" name="bt_basics-button" value="{l s='export' mod='gsnippetsreviews'}" class="btn btn-success btn-mini" onclick="oGsr.form('bt_export-form', '{$sURI|escape:'htmlall':'UTF-8'}', null, 'bt_export-result', 'bt_export-result', false, false, null, 'export', 'export');return false;" />
							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="clr_10"></div>

			<div class="form-group">
				<label class="control-label col-xs-12 col-sm-12 col-md-2 col-lg-2"><b>{l s='Your reviews list' mod='gsnippetsreviews'}</b> :</label>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 center">
					<div class="alert alert-info">{l s='A list of your reviews will be displayed below in order to check in the list what you just have exported!' mod='gsnippetsreviews'}</div>
				</div>
			</div>

			<div id="bt_loading-div-export" style="display: none;">
				<div class="alert alert-info">
					<p style="text-align: center !important;"><img src="{$sLoader|escape:'htmlall':'UTF-8'}" alt="Loading" /></p><div class="clr_20"></div>
					<p style="text-align: center !important;">{l s='Your reviews export is in progress' mod='gsnippetsreviews'}</p>
				</div>
			</div>

			<div class="clr_10"></div>

			<div class="form-group" id="bt_export-result">
			</div>


			<div class="clr_10"></div>
		{/if}
		<div class="clr_20"></div>
		<div class="clr_hr"></div>
		<div class="clr_20"></div>

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-11 col-lg-11">
				<div id="bt_error-export"></div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
		</div>
	</form>

	<div class="clr_20"></div>
</div>

{literal}
<script type="text/javascript">
	//bootstrap components init
	$(document).ready(function() {
		$('.label-tooltip, .help-tooltip').tooltip();
	});
</script>
{/literal}