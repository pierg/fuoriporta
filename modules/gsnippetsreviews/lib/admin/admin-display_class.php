<?php
/**
 * admin-display_class.php file defines method to display content tabs of admin page
 */

class BT_AdminDisplay implements BT_IAdmin
{
	/**
	 * @var array $aFlagIds : array for all flag ids used in option translation
	 */
	protected $aFlagIds = array();

	/**
	 *
	 */
	private function __construct()
	{

	}

	/**
	 *
	 */
	public function __destruct()
	{

	}

	/**
	 * Display all configured data admin tabs
	 * 
	 * @param string $sType => define which method to execute
	 * @param array $aParam
	 * @return array
	 */
	public function run($sType, array $aParam = null)
	{
		// set variables
		$aDisplayInfo = array();

		if (empty($sType)) {
			$sType = 'tabs';
		}

		switch ($sType) {
			case 'tabs' :               // use case - display first page with all tabs
			case 'snippets' :           // use case - display snippets settings page
			case 'reviews' :            // use case - display reviews settings page
			case 'facebookReviews' :    // use case - display FB reviews settings page
			case 'emailReviews' :       // use case - display email reviews settings page
			case 'vouchers' :           // use case - display vouchers settings page
			case 'cronReport' :         // use case - display last cron report
			case 'commentsImport' :     // use case - display comments product import
			case 'ordersSelect' :       // use case - display orders select
				// require voucher class - to factorise
				require_once(_GSR_PATH_LIB_VOUCHER . 'voucher_class.php');

				// set flag ids used in almost cases
				$this->setFlagIds();

				// execute match function
				$aDisplayInfo = call_user_func_array(array($this, 'display' . ucfirst($sType)), array($aParam));

				// check if 1.5 and multishop active and if group is selected
				$aDisplayInfo['assign']['bMultiShop'] = BT_GsrModuleTools::checkGroupMultiShop();
				break;
			default :
				break;
		}
		// use case - generic assign
		if (!empty($aDisplayInfo)) {
			$aDisplayInfo['assign'] = array_merge($aDisplayInfo['assign'], $this->assign());
		}

		return (
			$aDisplayInfo
		);
	}

	/**
	 * Assigns transverse data
	 *
	 * @return array
	 */
	private function assign()
	{
		// add jquery sortable plugin
		$aJsCss = Media::getJqueryPluginPath('sortable');
		Context::getContext()->controller->addJS($aJsCss['js']);

		// set smarty variables
		$aAssign = array(
			'sURI' 			    => BT_GsrModuleTools::truncateUri(array('&iPage', '&sAction')),
			'aQueryParams' 	    => $GLOBALS['GSR_REQUEST_PARAMS'],
			'sDisplay'          => Tools::getValue('sDisplay'),
			'iDefaultLang'      => Configuration::get('PS_DEFAULT_LANG'),
			'iCurrentLang' 	    => intval(GSnippetsReviews::$iCurrentLang),
			'sCurrentLang' 	    => GSnippetsReviews::$sCurrentLang,
			'sCurrentIso'       => Language::getIsoById(GSnippetsReviews::$iCurrentLang),
			'aLangs'            => Language::getLanguages(),
			'sFlagIds' 	        => $this->getFlagIds(),
			'aFlagIds' 	        => $this->aFlagIds,
			'sLoader'           => _GSR_URL_IMG . _GSR_LOADER_GIF,
			'sLoaderLarge'      => _GSR_URL_IMG . _GSR_LOADER_GIF,
			'bCountryRestriction' => GSnippetsReviews::$conf['GSR_APPLY_RESTRICTION'],
			'sHeaderInclude'    => BT_GsrModuleTools::getTemplatePath(_GSR_PATH_TPL_NAME . _GSR_TPL_ADMIN_PATH . _GSR_TPL_HEADER),
			'sErrorInclude'     => BT_GsrModuleTools::getTemplatePath(_GSR_PATH_TPL_NAME . _GSR_TPL_ADMIN_PATH . _GSR_TPL_ERROR),
			'sConfirmInclude'   => BT_GsrModuleTools::getTemplatePath(_GSR_PATH_TPL_NAME . _GSR_TPL_ADMIN_PATH . _GSR_TPL_CONFIRM),
		);

		return $aAssign;
	}

	/**
	 * Display admin's first page with all tabs
	 *
	 * @param array $aPost
	 * @return array
	 */
	private function displayTabs(array $aPost = null)
	{
		// set smarty variables
		$aAssign = array(
			'sDocUri'           => _MODULE_DIR_ . _GSR_MODULE_SET_NAME . '/',
			'sDocName'          => 'readme_' . ((GSnippetsReviews::$sCurrentLang == 'fr')? 'fr' : 'en') . '.pdf',
			'sContactUs'        =>  _GSR_SUPPORT_BT ?  _GSR_SUPPORT_URL . ((GSnippetsReviews::$sCurrentLang == 'fr')? 'fr/contactez-nous' : 'en/contact-us') :  _GSR_SUPPORT_URL . ((GSnippetsReviews::$sCurrentLang == 'fr')? 'fr/ecrire-au-developpeur?id_product=' . _GSR_SUPPORT_ID  : 'en/write-to-developper?id_product=' ._GSR_SUPPORT_ID),
			'sRateUrl'          =>  _GSR_SUPPORT_BT ? _GSR_SUPPORT_URL . ((GSnippetsReviews::$sCurrentLang == 'fr')? 'fr/modules-prestashop-reseaux-sociaux-facebook/50-module-prestashop-publicites-de-produits-facebook-pixel-facebook-0656272916497.html' : 'en/prestashop-modules-social-networks-facebook/50-prestashop-addon-facebook-product-ads-facebook-pixel-0656272916497.html') : _GSR_SUPPORT_URL . ((GSnippetsReviews::$sCurrentLang == 'fr')? '/fr/ratings.php'  : '/en/ratings.php'),
			'sCrossSellingUrl'  => _GSR_SUPPORT_BT ? _GSR_SUPPORT_URL .  '?utm_campaign=internal-module-ad&utm_source=banniere&utm_medium=' . _GSR_MODULE_SET_NAME  : _GSR_SUPPORT_URL . '/6_business-tech',
			'sCrossSellingImg'  => (GSnippetsReviews::$sCurrentLang == 'fr') ? _GSR_URL_IMG . 'admin/module_banner_cross_selling_FR.jpg' : _GSR_URL_IMG .'admin/module_banner_cross_selling_EN.jpg',
			'bHideConfiguration'=> BT_GsrWarning::create()->bStopExecution,
			'sModuleVersion'    => GSnippetsReviews::$oModule->version,
		);

		// check if comments product and import are already made and done
		if ((!GSnippetsReviews::$conf['GSR_COMMENTS_IMPORT'] || _GSR_MOCK_IMPORT_DEBUG)
			&& BT_GsrModuleTools::isInstalled('productcomments', array(), false, true)
		) {
			// include
			require_once(_GSR_PATH_LIB . 'module-dao_class.php');

			// get number of reviews to import
			$iCountReviews = BT_GsrModuleDao::getModuleProductComments(true, 2, _GSR_MOCK_IMPORT_DEBUG);

			if (!empty($iCountReviews)) {
				$aAssign['bCommentsImport'] = true;
				$aAssign['iReviews'] = $iCountReviews;
				$aAssign['bAddCssModule'] = true;
			}
		}

		// use case - get display data of snippets settings
		$aData = $this->displaySnippets($aPost);

		$aAssign = array_merge($aAssign, $aData['assign']);

		// use case - get display data of reviews settings
		$aData = $this->displayReviews($aPost);

		$aAssign = array_merge($aAssign, $aData['assign']);

		// use case - get display data of email reviews settings
		$aData = $this->displayEmailReviews($aPost);

		$aAssign = array_merge($aAssign, $aData['assign']);

		// use case - get display data for vouchers settings
		$aData = $this->displayVouchers($aPost);

		$aAssign = array_merge($aAssign, $aData['assign']);

		// use case - get display data of FB reviews settings
		$aData = $this->displayFacebookReviews($aPost);

		$aAssign = array_merge($aAssign, $aData['assign']);

		// assign all included templates files
		$aAssign['sWelcome'] = BT_GsrModuleTools::getTemplatePath(_GSR_PATH_TPL_NAME . _GSR_TPL_ADMIN_PATH . _GSR_TPL_WELCOME);
		$aAssign['sSnippetsInclude'] = BT_GsrModuleTools::getTemplatePath(_GSR_PATH_TPL_NAME . _GSR_TPL_ADMIN_PATH . _GSR_TPL_SNIPPETS_SETTINGS);
		$aAssign['sReviewsInclude'] = BT_GsrModuleTools::getTemplatePath(_GSR_PATH_TPL_NAME . _GSR_TPL_ADMIN_PATH . _GSR_TPL_REVIEWS_SETTINGS);
		$aAssign['sEmailsInclude'] = BT_GsrModuleTools::getTemplatePath(_GSR_PATH_TPL_NAME . _GSR_TPL_ADMIN_PATH . _GSR_TPL_EMAIL_REVIEWS);
		$aAssign['sVouchersInclude'] = BT_GsrModuleTools::getTemplatePath(_GSR_PATH_TPL_NAME . _GSR_TPL_ADMIN_PATH . _GSR_TPL_FB_VOUCHERS_SETTINGS);
		$aAssign['sFacebookInclude'] = BT_GsrModuleTools::getTemplatePath(_GSR_PATH_TPL_NAME . _GSR_TPL_ADMIN_PATH . _GSR_TPL_FB_REVIEWS_SETTINGS);
		$aAssign['sModuleVersion'] = GSnippetsReviews::$oModule->version;

		// set css and js use
		$GLOBALS['GSR_USE_JS_CSS']['bUseJqueryUI'] = true;

		return (
			array(
				'tpl'		=> _GSR_TPL_ADMIN_PATH . _GSR_TPL_BODY,
				'assign'	=> array_merge($aAssign, $GLOBALS['GSR_USE_JS_CSS']),
			)
		);
	}

	/**
	 * Display snippets settings
	 *
	 * @param array $aPost
	 * @return array
	 */
	private function displaySnippets(array $aPost = null)
	{
		// translate desc title
		BT_GsrModuleTools::translateDescTitle();

		// translate badge styles' title
		BT_GsrModuleTools::translateBadgeStylesTitle();

		// translate badge pages' title
		BT_GsrModuleTools::translateBadgePagesTitle();

		// get badge pages and its styles
		foreach ($GLOBALS['GSR_BADGE_PAGES'] as $sType => &$aValue) {
			if ($sType == 'product' && GSnippetsReviews::$conf['GSR_DISPLAY_PROD_RS']) {
				$aValue['use'] = true;
			}
			if (!empty($aValue['use'])) {
				foreach ($aValue['allow'] as &$aPosition) {
					if (array_key_exists($aPosition['position'], $GLOBALS['GSR_BADGE_STYLES'])) {
						$aPosition['title'] = $GLOBALS['GSR_BADGE_STYLES'][$aPosition['position']];
					}
				}
			}
		}

		// set smarty variables
		$aAssign = array(
			'sCtrlParamName' 	        => _GSR_PARAM_CTRL_NAME,
			'sController' 	            => _GSR_ADMIN_CTRL,
			'bDisplayProductRichSnippets'=> GSnippetsReviews::$conf['GSR_DISPLAY_PROD_RS'],
			'bDisplayDesc'              => GSnippetsReviews::$conf['GSR_DISPLAY_PROD_DESC'],
			'aDesc'                     => BT_GsrModuleTools::getSortDesc(),
			'bDisplayBrand'             => GSnippetsReviews::$conf['GSR_DISPLAY_PROD_BRAND'],
			'bDisplayCat'               => GSnippetsReviews::$conf['GSR_DISPLAY_PROD_CAT'],
			'bDisplayBreadcrumb'        => GSnippetsReviews::$conf['GSR_DISPLAY_PROD_BREADCRUMB'],
			'bDisplayIdentifier'        => GSnippetsReviews::$conf['GSR_DISPLAY_PROD_IDENTIFIER'],
			'bDisplaySupplier'          => GSnippetsReviews::$conf['GSR_DISPLAY_PROD_SUPPLIER'],
			'bDisplayCondition'         => GSnippetsReviews::$conf['GSR_DISPLAY_PROD_COND'],
			'sOfferType'                => GSnippetsReviews::$conf['GSR_PRODUCT_OFFERS'],
			'bDisplaySeller'            => GSnippetsReviews::$conf['GSR_DISPLAY_PROD_SELLER'],
			'bDisplayUntilDate'         => GSnippetsReviews::$conf['GSR_DISPLAY_PROD_UNTIL_DATE'],
			'bDisplayAvaibility'        => GSnippetsReviews::$conf['GSR_DISPLAY_PROD_AVAILABILITY'],
			'bDisplayHighPrice'         => GSnippetsReviews::$conf['GSR_DISPLAY_PROD_HIGH_PRICE'],
			'bDisplayOfferCount'        => GSnippetsReviews::$conf['GSR_DISPLAY_PROD_OFFER_COUNT'],
			'sReviewType'               => GSnippetsReviews::$conf['GSR_RVW_TYPE'],
			'bDisplayRating'            => GSnippetsReviews::$conf['GSR_PROD_RATING'],
			'bDisplayReviewDate'        => GSnippetsReviews::$conf['GSR_PROD_RVW_DATE'],
			'bDisplayReviewTitle'       => GSnippetsReviews::$conf['GSR_PROD_RVW_TITLE'],
			'bDisplayReviewDesc'        => GSnippetsReviews::$conf['GSR_PROD_RVW_DESC'],
			'bDisplayReviewAggregate'   => GSnippetsReviews::$conf['GSR_PROD_RVW_AGGREGATE'],
			'bDisplayBadge'             => GSnippetsReviews::$conf['GSR_DISPLAY_BADGE'],
			'aBadges'                   => (!empty(GSnippetsReviews::$conf['GSR_BADGES'])? unserialize(GSnippetsReviews::$conf['GSR_BADGES']) : array()),
			'aBadgePages'               => $GLOBALS['GSR_BADGE_PAGES'],
		);

		return (
			array(
				'tpl'	    => _GSR_TPL_ADMIN_PATH . _GSR_TPL_SNIPPETS_SETTINGS,
				'assign'	=> $aAssign,
			)
		);
	}

	/**
	 * Display reviews settings
	 *
	 * @param array $aPost
	 * @return array
	 */
	private function displayReviews(array $aPost = null)
	{
		// translate hook title
		BT_GsrModuleTools::translateHookTitle();

		// translate authorized people's titles
		BT_GsrModuleTools::translateAuthorize();

		// translate reviews display mode's titles
		BT_GsrModuleTools::translateReviewsDisplayMode();

		// translate last review block pages' title
		BT_GsrModuleTools::translateLastBlockPagesTitle();

		// translate last review block pages' title
		BT_GsrModuleTools::translateLastBlockPosTitle();

		// get last review block pages and its styles
		foreach ($GLOBALS['GSR_LAST_RVW_BLOCK_PAGES'] as $sType => &$aValue) {
			if (!empty($aValue['use'])) {
				foreach ($aValue['allow'] as &$aPosition) {
					if (array_key_exists($aPosition['position'], $GLOBALS['GSR_AVAILABLE_BLOCK_POS'])) {
						$aPosition['title'] = $GLOBALS['GSR_AVAILABLE_BLOCK_POS'][$aPosition['position']];
					}
				}
			}
		}

		// include
		require_once(_GSR_PATH_LIB_COMMON . 'dir-reader.class.php');

		// get images files
		$aImgFiles = BT_DirReader::create()->run(array('path' => _GSR_PATH_ROOT . _GSR_PATH_VIEWS . _GSR_PATH_IMG . 'picto/', 'recursive' => true, 'extension' => 'png', 'subpath' => true, 'subpathname' => true));

		// use case - sort by folder name
		foreach ($aImgFiles as $k => $v) {
			if (!strstr($v['subpath'], '.AppleDouble')
				&& !strstr($v['subpath'], 'thumbs')
			) {
				$aImgFiles[$v['subpath']] = $v;
			}
			unset($aImgFiles[$k]);
		}
		ksort($aImgFiles);

		// detect if share voucher is activated
		$bShareVoucher = BT_GsrModuleTools::getEnableVouchers('share');

		// set smarty variables
		$aAssign = array(
			'sAdmniTabUrl'          => $_SERVER['SCRIPT_NAME'] . '?controller=AdminModerationTool&token=' . Tools::getAdminTokenLite('AdminModerationTool'),
			'aReviewHooks'          => BT_GsrModuleTools::getUsableHooks($GLOBALS['GSR_REVIEWS_HOOKS']),
			'sHook'                 => GSnippetsReviews::$conf['GSR_HOOK'],
			'aAuthorize'            => $GLOBALS['GSR_AUTHORIZE'],
			'sAuthorizeReview'      => GSnippetsReviews::$conf['GSR_COMMENTS_USER'],
			'aReviewsMode'          => $GLOBALS['GSR_REVIEWS_DISPLAY_MODE'],
			'sDisplayReviewMode'    => GSnippetsReviews::$conf['GSR_REVIEWS_DISPLAY_MODE'],
			'bAdminApproval'        => GSnippetsReviews::$conf['GSR_COMMENTS_APPROVAL'],
			'bReviewModifyTool'     => GSnippetsReviews::$conf['GSR_MODIFY_REVIEW'],
			'aCmsList'              => CMS::getLinks(GSnippetsReviews::$iCurrentLang),
			'iReviewCmsPage'        => GSnippetsReviews::$conf['GSR_RVW_CMS_ID'],
			'bEnableRatings'        => GSnippetsReviews::$conf['GSR_ENABLE_RATINGS'],
			'bEnableCustLang'       => GSnippetsReviews::$conf['GSR_ENABLE_RVW_CUST_LANG'],
			'bEnableComments'       => GSnippetsReviews::$conf['GSR_ENABLE_COMMENTS'],
			'bForceComments'        => GSnippetsReviews::$conf['GSR_FORCE_COMMENTS'],
			'bDisplayReviews'       => GSnippetsReviews::$conf['GSR_DISPLAY_REVIEWS'],
			'bShareVoucher'         => (($bShareVoucher == true)? true : false),
			'bEnableSocialButton'   => GSnippetsReviews::$conf['GSR_DISPLAY_SOCIAL_BUTTON'],
			'bCountBoxButton'       => GSnippetsReviews::$conf['GSR_COUNT_BOX_BUTTON'],
			'iFbButtonType'         => GSnippetsReviews::$conf['GSR_FB_BUTTON_TYPE'],
			'iReviewsPerPage'       => GSnippetsReviews::$conf['GSR_NB_REVIEWS_PROD_PAGE'],
			'iReviewsListPerPage'   => GSnippetsReviews::$conf['GSR_NB_REVIEWS_PAGE'],
			'bDisplayPhoto'         => GSnippetsReviews::$conf['GSR_DISPLAY_PHOTO_REVIEWS'],
			'bDisplayReportButton'  => GSnippetsReviews::$conf['GSR_DISPLAY_REPORT_BUTTON'],
			'bDisplayAddress'       => GSnippetsReviews::$conf['GSR_DISPLAY_ADDRESS'],
			'iNbModerateReviews'    => GSnippetsReviews::$conf['GSR_NB_REVIEWS_MODERATION'],
			'iNbProductSlider'      => GSnippetsReviews::$conf['GSR_NB_PROD_SLIDER'],
			'iNbProductReviewed'    => GSnippetsReviews::$conf['GSR_NB_PROD_REVIEWED'],
			'aEltPerPage'           => $GLOBALS['GSR_NB_REVIEWS_VALUES'],
			'aSliderOpts'           => $GLOBALS['GSR_SLIDER_OPTS'],
			'iSliderWidth'          => GSnippetsReviews::$conf['GSR_SLIDER_WIDTH'],
			'iSliderSpeed'          => GSnippetsReviews::$conf['GSR_SLIDER_SPEED'],
			'iSliderPause'          => GSnippetsReviews::$conf['GSR_SLIDER_PAUSE'],
			'sReviewProdImg'        => GSnippetsReviews::$conf['GSR_RVW_PROD_IMG'],
			'sReviewListProdImg'    => GSnippetsReviews::$conf['GSR_RVW_LIST_PROD_IMG'],
			'sSliderProdImg'        => GSnippetsReviews::$conf['GSR_SLIDER_PROD_IMG'],
			'bLastRvwBlockFirst'    => GSnippetsReviews::$conf['GSR_LAST_RVW_BLOCK_FIRST'],
			'bDisplayLastRvwBlock'  => GSnippetsReviews::$conf['GSR_DISPLAY_LAST_RVW_BLOCK'],
			'sLastReviewHook'       => GSnippetsReviews::$conf['GSR_LAST_RVW_BLOCK_HOOK'],
			'iNbLastReviews'        => GSnippetsReviews::$conf['GSR_NB_LAST_REVIEWS'],
			'aNbLastReviews'        => array(1,2,3,4,5,6,7,8,9,10,15,20),
			'aLastBlockPos'         => (!empty(GSnippetsReviews::$conf['GSR_LAST_RVW_BLOCK'])? unserialize(GSnippetsReviews::$conf['GSR_LAST_RVW_BLOCK']) : array()),
			'aLastBlockPages'       => $GLOBALS['GSR_LAST_RVW_BLOCK_PAGES'],
			'bDisplayStarsInList'   => GSnippetsReviews::$conf['GSR_DISPLAY_HOOK_REVIEW_STARS'],
			'bUseSnippetsProdList'  => GSnippetsReviews::$conf['GSR_SNIPPETS_PRODLIST'],
			'bHasSnippetsProdList'  => GSnippetsReviews::$conf['GSR_HAS_SNIPPETS_PRODLIST'],
			'bDisplayEmptyRating'   => GSnippetsReviews::$conf['GSR_DISP_EMPTY_RATING'],
			'bDisplayBeFirstMessage'=> GSnippetsReviews::$conf['GSR_DISP_BEFIRST_MSG'],
			'iStarDisplayMode'      => GSnippetsReviews::$conf['GSR_DISP_STAR_RATING_MODE'],
			'aStarsPaddingLeft'     => $GLOBALS['GSR_STAR_PADDING_VALUES'],
			'iStarPaddingLeft'      => GSnippetsReviews::$conf['GSR_STARS_PADDING_LEFT'],
			'aStarSizes'            => $GLOBALS['GSR_STAR_SIZE_VALUES'],
			'iSelectStarSize'       => GSnippetsReviews::$conf['GSR_STARS_SIZE'],
			'aTextSizes'            => $GLOBALS['GSR_TEXT_SIZE_VALUES'],
			'iSelectTextSize'       => GSnippetsReviews::$conf['GSR_TEXT_SIZE'],
			'aImages'               => $aImgFiles,
			'sPicto'                => GSnippetsReviews::$conf['GSR_PICTO'],
			'bUseFontAwesome'       => GSnippetsReviews::$conf['GSR_USE_FONTAWESOME'],
			'bPS17'                 => GSnippetsReviews::$bCompare17,
			'bPS1710'               => GSnippetsReviews::$bCompare1710,
		);

		// use case - detect if be first message is translated with empty stars - moderation and modify review options activated
		$aAssign['aBeFirst'] = $this->getDefaultTranslations('BEFIRST_SENTENCE', 'BEFIRST_DEFAULT_TRANSLATE');
		$aAssign['aReviewModerationTxt'] = $this->getDefaultTranslations('RVW_MODERATION_TEXT', 'MODERATION_DEFAULT_TRANSLATE');
		$aAssign['aReviewModifyTxt'] = $this->getDefaultTranslations('RVW_MODIFY_TEXT', 'MODERATION_MODIFY_DEFAULT_TRANSLATE');

		// get all active languages in order to loop on field form which need to manage translation
		$aAssign['aLangs'] = (array)Language::getLanguages();

		foreach ($aAssign['aLangs'] as $aLang) {
			if (!isset($aAssign['aBeFirst'][$aLang['id_lang']])) {
				$aAssign['aBeFirst'][$aLang['id_lang']] = $GLOBALS['GSR_BEFIRST_DEFAULT_TRANSLATE']['en'];
			}
			if (!isset($aAssign['aReviewModerationTxt'][$aLang['id_lang']])) {
				$aAssign['aReviewModerationTxt'][$aLang['id_lang']] = $GLOBALS['GSR_MODERATION_DEFAULT_TRANSLATE']['en'];
			}
			if (!isset($aAssign['aReviewModifyTxt'][$aLang['id_lang']])) {
				$aAssign['aReviewModifyTxt'][$aLang['id_lang']] = $GLOBALS['GSR_MODERATION_MODIFY_DEFAULT_TRANSLATE']['en'];
			}
		}

		// get image size available
		$aAvailableTypes = ImageType::getImagesTypes('products');

		foreach ($aAvailableTypes as $key => $aImageSize) {
			$aAssign['aImageSize'][$key] = $aImageSize['name'];
		}

		unset($aAvailableTypes);

		$aAssign['sIncludingCode'] = htmlentities(
			'{literal}' . "\n"
			. '<div id="productRating{/literal}{$product.id_product|intval}{literal}"></div>' . "\n"
			. '<script>' . "\n"
			. ' $(document).ready(function(){' . "\n"
			. '     oGsr.getProductAverage({/literal}{$product.id_product|intval}{literal}, \'\');' . "\n"
			. ' });' . "\n"
			. '</script>' . "\n"
			. '{/literal}');

		return (
			array(
				'tpl'       => _GSR_TPL_ADMIN_PATH . _GSR_TPL_REVIEWS_SETTINGS,
				'assign'    => $aAssign,
			)
		);
	}


	/**
	 * Display email reviews settings
	 *
	 * @param array $aPost
	 * @return array
	 */
	private function displayEmailReviews(array $aPost = null)
	{
		// include
		require_once(_GSR_PATH_LIB . 'module-dao_class.php');
		require_once(_GSR_PATH_LIB_COMMON . 'serialize.class.php');

		// get pre-selection
		$aSelection = !empty(GSnippetsReviews::$conf['GSR_STATUS_SELECTION'])? unserialize(GSnippetsReviews::$conf['GSR_STATUS_SELECTION']) : GSnippetsReviews::$conf['GSR_STATUS_SELECTION'];

		// set cron secure key for the first time before updating options
		if (empty(GSnippetsReviews::$conf['GSR_CRON_SECURE_KEY'])) {
			Configuration::updateValue('GSR_CRON_SECURE_KEY', md5(_GSR_MODULE_NAME . Configuration::get('PS_SHOP_NAME') . rand(0, 1000)));
			// get configuration options
			BT_GsrModuleTools::getConfiguration();
		}

		// set smarty variables
		$aAssign = array(
			'sCronUrl'              => Configuration::get('PS_SHOP_DOMAIN'),
			'sToday'                => date('Y-m-d H:i:s', time()),
			'bEnableRatings'        => GSnippetsReviews::$conf['GSR_ENABLE_RATINGS'],
			'bEnableComments'       => GSnippetsReviews::$conf['GSR_ENABLE_COMMENTS'],
			'bDisplayReviews'       => GSnippetsReviews::$conf['GSR_DISPLAY_REVIEWS'],
			'sEmail'                => GSnippetsReviews::$conf['GSR_EMAIL'],
			'bEnableEmail'          => GSnippetsReviews::$conf['GSR_ENABLE_EMAIL'],
			'bEnableReviewEmail'    => GSnippetsReviews::$conf['GSR_ENABLE_RVW_EMAIL'],
			'bEnableCallback'       => GSnippetsReviews::$conf['GSR_ENABLE_CALLBACK'],
			'bOrdersImport'         => GSnippetsReviews::$conf['GSR_ORDERS_IMPORT'],
			'bEnableCarbonCopy'     => GSnippetsReviews::$conf['GSR_ENABLE_REMINDER_MAIL_CC'],
			'sCarbonCopyMail'       => GSnippetsReviews::$conf['GSR_REMINDER_MAIL_CC'],
			'iDelayEmail'           => GSnippetsReviews::$conf['GSR_EMAIL_DELAY'],
			'sSecureKey'            => GSnippetsReviews::$conf['GSR_CRON_SECURE_KEY'],
			'aImgTypes'             => ImageType::getImagesTypes('products'),
			'sProductImgType'       => GSnippetsReviews::$conf['GSR_MAIL_PROD_IMG'],
			'aEmailLangErrors'      => BT_GsrModuleTools::checkMailLanguages(),
			'aStatusSelection'      => $aSelection,
			'aOrderStatusTitle'     => BT_GsrModuleDao::getOrderStatus(),
			'bPsVersion1606'        => (version_compare(_PS_VERSION_, '1.6.0.4', '>')? true : false ),
		);
		unset($aSelection);

		// set the cron file name
		$sCronFile = _GSR_CBK_LOGS . GSnippetsReviews::$iShopId . _GSR_CBK_LOGS_EXT;

		if (file_exists(_GSR_PATH_LOGS . $sCronFile)) {
			$aAssign['aCronReport'] = BT_Serialize::create()->get(file_get_contents(_GSR_PATH_LOGS . $sCronFile));

			if (!is_writable(_GSR_PATH_LOGS . $sCronFile)) {
				$aAssign['bwritableReport'] = false;
				$aAssign['sReportFile'] = _GSR_PATH_LOGS . $sCronFile;
			}
		}

		// use case - detect if review email notification subject has been filled
		$aAssign['aReviewEmailSubject'] = $this->getDefaultTranslations('RVW_EMAIL_SUBJECT', 'NOTIFICATION_DEFAULT_TRANSLATE');
		// use case - detect if after-sales reply notification subject has been filled
		$aAssign['aReplyEmailSubject'] = $this->getDefaultTranslations('REPLY_EMAIL_SUBJECT', 'REPLY_DEFAULT_TRANSLATE');
		// use case - detect if after-sales reply notification text has been filled
		$aAssign['aReplyEmailText'] = $this->getDefaultTranslations('REPLY_EMAIL_TEXT', 'REPLY_TEXT_DEFAULT_TRANSLATE');
		// use case - detect if review email reminder subject has been filled
		$aAssign['aEmailSubject'] = $this->getDefaultTranslations('REMINDER_SUBJECT', 'REMINDER_DEFAULT_TRANSLATE');
		// use case - detect if review email reminder category label has been filled
		$aAssign['aEmailCategoryLabel'] = $this->getDefaultTranslations('REMINDER_MAIL_CAT_LABEL', 'REMINDER_DEFAULT_CAT_LABEL');
		// use case - detect if review email reminder prod label has been filled
		$aAssign['aEmailProductLabel'] = $this->getDefaultTranslations('REMINDER_MAIL_PROD_LABEL', 'REMINDER_DEFAULT_PROD_LABEL');
		// use case - detect if review email reminder sentence has been filled
		$aAssign['aEmailSentence'] = $this->getDefaultTranslations('REMINDER_MAIL_SENTENCE', 'REMINDER_DEFAULT_SENTENCE');

		// get all active languages in order to loop on field form which need to manage translation
		$aAssign['aLangs'] = (array)Language::getLanguages();

		foreach ($aAssign['aLangs'] as $aLang) {
			if (!isset($aAssign['aReviewEmailSubject'][$aLang['id_lang']])) {
				$aAssign['aReviewEmailSubject'][$aLang['id_lang']] = $GLOBALS['GSR_NOTIFICATION_DEFAULT_TRANSLATE']['en'];
			}
			if (!isset($aAssign['aReplyEmailSubject'][$aLang['id_lang']])) {
				$aAssign['aReplyEmailSubject'][$aLang['id_lang']] = $GLOBALS['GSR_REPLY_DEFAULT_TRANSLATE']['en'];
			}
			if (!isset($aAssign['aReplyEmailText'][$aLang['id_lang']])) {
				$aAssign['aReplyEmailText'][$aLang['id_lang']] = $GLOBALS['GSR_REPLY_TEXT_DEFAULT_TRANSLATE']['en'];
			}
			if (empty($aAssign['aEmailSubject'][$aLang['id_lang']])) {
				$aAssign['aEmailSubject'][$aLang['id_lang']] = $GLOBALS['GSR_REMINDER_DEFAULT_TRANSLATE']['en'];
			}
			if (empty($aAssign['aEmailCategoryLabel'][$aLang['id_lang']])) {
				$aAssign['aEmailCategoryLabel'][$aLang['id_lang']] = $GLOBALS['GSR_REMINDER_DEFAULT_CAT_LABEL']['en'];
			}
			if (empty($aAssign['aEmailProductLabel'][$aLang['id_lang']])) {
				$aAssign['aEmailProductLabel'][$aLang['id_lang']] = $GLOBALS['GSR_REMINDER_DEFAULT_PROD_LABEL']['en'];
			}
			if (empty($aAssign['aEmailSentence'][$aLang['id_lang']])) {
				$aAssign['aEmailSentence'][$aLang['id_lang']] = $GLOBALS['GSR_REMINDER_DEFAULT_SENTENCE']['en'];
			}
		}

		return (
			array(
				'tpl'	    => _GSR_TPL_ADMIN_PATH . _GSR_TPL_EMAIL_REVIEWS,
				'assign'	=> $aAssign,
			)
		);
	}


	/**
	 * Display last cron report
	 *
	 * @param array $aPost
	 * @return array
	 */
	private function displayCronReport(array $aPost = null)
	{
		// clean headers
		@ob_end_clean();

		// include
		require_once(_GSR_PATH_LIB_COMMON . 'serialize.class.php');

		// set smarty variables
		$aAssign = array();

		$aAssign['bPsVersion1606'] = version_compare(_PS_VERSION_, '1.6.0.4', '>')? true : false;
		$aAssign['sShopName'] = Configuration::get('PS_SHOP_NAME');

		// set the cron file name
		$sCronFile = _GSR_CBK_LOGS . GSnippetsReviews::$iShopId . _GSR_CBK_LOGS_EXT;

		if (file_exists(_GSR_PATH_LOGS . $sCronFile) && filesize(_GSR_PATH_LOGS . $sCronFile)) {
			$aAssign['aCronReport'] = BT_Serialize::create()->get(file_get_contents(_GSR_PATH_LOGS . $sCronFile));
		}
		else {
			$aAssign['aErrors'][] = array('msg' => GSnippetsReviews::$oModule->l('There is no cron job report for the shop', 'admin-display_class') . ': ' . $aAssign['sShopName'], 'code' => 180) ;
		}

		// force xhr mode activated
		GSnippetsReviews::$sQueryMode = 'xhr';

		return (
			array(
				'tpl'	    => _GSR_TPL_ADMIN_PATH . _GSR_TPL_CRON_REPORT,
				'assign'	=> $aAssign,
			)
		);
	}


	/**
	 * Display settings for vouchers
	 *
	 * @param array $aPost
	 * @return array
	 */
	private function displayVouchers(array $aPost = null)
	{
		// get data for comment voucher
		$aAssign = $this->processVoucher('comment');

		return (
			array(
				'tpl'	    => _GSR_TPL_ADMIN_PATH . _GSR_TPL_FB_VOUCHERS_SETTINGS,
				'assign'	=> $aAssign,
			)
		);
	}


	/**
	 * Process data to return data to display
	 *
	 * @param string $sVoucherType
	 * @return array
	 */
	private function processVoucher($sVoucherType)
	{
		// Prepare Categories tree for display in Associations tab
		$oCatRoot = Category::getRootCategory();

		// get vouchers data
		$aVouchers = BT_Voucher::create()->getSettings();

		// in order to get categories indexed or not per voucher type
		if (!empty($aVouchers[$sVoucherType])) {
			$aFormatCat = $aVouchers[$sVoucherType]['categories'];
		}
		else {
			$aFormatCat = array();
		}

		$sBoTheme = ((Validate::isLoadedObject(Context::getContext()->employee)
			&& !empty(Context::getContext()->employee->bo_theme)) ? Context::getContext()->employee->bo_theme : 'default');

		if (!file_exists(_PS_BO_ALL_THEMES_DIR_ . $sBoTheme . DIRECTORY_SEPARATOR . 'template')){
			$sBoTheme = 'default';
		}

		// add JS
		Context::getContext()->controller->addJS(_PS_BO_ALL_THEMES_DIR_ . $sBoTheme . '/js/tree.js');

		// stock the current controller in order to use products controller for category tree
		$oOldController = Context::getContext()->controller;

		// set products controller
		Context::getContext()->controller = new AdminProductsController();

		$oTree = new HelperTreeCategories('associated-categories-tree-' . $sVoucherType);

		$oTree->setTemplate('tree_associated_categories.tpl')
			->setHeaderTemplate('tree_associated_header.tpl')
			->setRootCategory($oCatRoot->id)
			->setUseCheckBox(true)
			->setUseSearch(false)
			->setSelectedCategories($aFormatCat)
		;

		// translate
		BT_Voucher::create()->translateVouchersType($sVoucherType);

		// set smarty variables
		$aAssign = array(
			'sVoucherType'  => $sVoucherType,
			'sType' . ucfirst($sVoucherType) => $sVoucherType,
			'sVoucherForm'  => BT_GsrModuleTools::getTemplatePath(_GSR_PATH_TPL_NAME . _GSR_TPL_ADMIN_PATH . _GSR_TPL_FB_VOUCHERS_FORM),
			'aCurrencies'   => Currency::getCurrencies(),
			'aFormatCat' . ucfirst($sVoucherType) => $aFormatCat,
			'aVouchersType'     => $GLOBALS['GSR_VOUCHERS_TYPE'],
			'aEnableVouchers'   => BT_GsrModuleTools::getEnableVouchers(),
			'aVouchers'         => $aVouchers,
			'sCategoryTree' . ucfirst($sVoucherType) => $oTree->render(),
		);

		// set again the current controller
		Context::getContext()->controller = $oOldController;

		unset($oCatRoot);
		unset($oTree);
		unset($oOldController);

		// get all active languages in order to loop on field form which need to manage translation
		$aAssign['aLangs'] = Language::getLanguages();

		return $aAssign;
	}

	/**
	 * Display FB reviews settings
	 *
	 * @param array $aPost
	 * @return array
	 */
	private function displayFacebookReviews(array $aPost = null)
	{
		// set smarty variables
		$aAssign = array(
			'bEnableFbPost' 	=> GSnippetsReviews::$conf['GSR_ENABLE_FB_POST'],
			'bFbPsWallPosts' 	=> BT_GsrModuleTools::isInstalled(_GSR_FBWP_NAME, $GLOBALS['GSR_FBWP_KEYS']),
		);

		$aAssign['bFbPsWallPosts'] = true;

		// use case - detect if FB post sentence / label are filled
		if (!empty(GSnippetsReviews::$conf['GSR_FB_POST_PHRASE'])
			&& !empty(GSnippetsReviews::$conf['GSR_FB_POST_LABEL'])
		) {
			$aAssign['aFbPostPhrase'] = unserialize(GSnippetsReviews::$conf['GSR_FB_POST_PHRASE']);
			$aAssign['aFbPostLabel'] = unserialize(GSnippetsReviews::$conf['GSR_FB_POST_LABEL']);
		}
		else {
			foreach ($GLOBALS['GSR_FB_DEFAULT_TRANSLATE'] as $sIsoCode => $aTranslation) {
				$iLangId = BT_GsrModuleTools::getLangId($sIsoCode);

				if ($iLangId) {
					// get Id by iso
					$aAssign['aFbPostPhrase'][$iLangId] = $aTranslation['sentence'];
					$aAssign['aFbPostLabel'][$iLangId] = $aTranslation['label'];
				}
			}
		}

		// get all active languages in order to loop on field form which need to manage translation
		$aAssign['aLangs'] = Language::getLanguages();

		foreach ($aAssign['aLangs'] as $aLang) {
			if (!isset($aAssign['aFbPostPhrase'][$aLang['id_lang']])) {
				$aAssign['aFbPostPhrase'][$aLang['id_lang']] = $GLOBALS['GSR_FB_DEFAULT_TRANSLATE']['en']['sentence'];
			}
			if (!isset($aAssign['aFbPostLabel'][$aLang['id_lang']])) {
				$aAssign['aFbPostLabel'][$aLang['id_lang']] = $GLOBALS['GSR_FB_DEFAULT_TRANSLATE']['en']['label'];
			}
		}

		// get FB voucher data for Fb sharing
		$aAssign = array_merge($aAssign, $this->processVoucher('share'));

		$aAssign['bEnableSocialButton'] = GSnippetsReviews::$conf['GSR_DISPLAY_SOCIAL_BUTTON'];
		$aAssign['bShareVoucher'] = !empty($aAssign['aEnableVouchers']['share'])? true : false;

		return (
			array(
				'tpl'	    => _GSR_TPL_ADMIN_PATH . _GSR_TPL_FB_REVIEWS_SETTINGS,
				'assign'	=> $aAssign,
			)
		);
	}


	/**
	 * Display comments import content
	 *
	 * @return string
	 */
	private function displayCommentsImport()
	{
		// clean headers
		@ob_end_clean();

		// set
		$aAssign = array();

		try {
			// include
			require_once(_GSR_PATH_LIB . 'module-dao_class.php');

			//get reviews from "comments product" module
			$aCountReviews = BT_GsrModuleDao::getModuleProductComments(false, 2, _GSR_MOCK_IMPORT_DEBUG);

			if (!empty($aCountReviews)) {
				$aValidReviews = array();
				$aInvalidReviews = array();

				foreach ($aCountReviews as $aReview) {
					if (isset($aReview['validate']) && $aReview['validate'] == true) {
						$aValidReviews[] = $aReview;
					}
					else {
						$aInvalidReviews[] = $aReview;
					}
				}

				$aAssign['bCommentsImport']     = true;
				$aAssign['iCommentsImportType'] = GSnippetsReviews::$conf['GSR_COMMENTS_IMPORT_TYPE'];
				$aAssign['iMaxRating']          = _GSR_MAX_RATING;
				$aAssign['iTotalReviews']       = count($aCountReviews);
				$aAssign['aValidReviews']       = $aValidReviews;
				$aAssign['iTotalValidReviews']  = count($aValidReviews);
				$aAssign['aInvalidReviews']     = $aInvalidReviews;
				$aAssign['iTotalInvalidReviews']= count($aInvalidReviews);
				$aAssign['sLoadingImg']         = _GSR_URL_IMG . _GSR_LOADER_GIF;
			}
		}
		catch (Exception $e) {
			$aAssign['aErrors'][] = array('msg' => $e->getMessage(), 'code' => $e->getCode());
		}

		// force xhr mode activated
		GSnippetsReviews::$sQueryMode = 'xhr';

		return (
			array(
				'tpl'	    => _GSR_TPL_ADMIN_PATH . _GSR_TPL_REVIEWS_IMPORT,
				'assign'	=> $aAssign,
			)
		);
	}


	/**
	 * Display orders import content
	 *
	 * @return string
	 */
	private function displayOrdersSelect()
	{
		// clean headers
		@ob_end_clean();

		// set
		$aAssign = array();

		try {
			// use case - check enable email
			$sDateFrom = Tools::getValue('dateFrom');
			$sDateTo = Tools::getValue('dateTo');

			if (!empty($sDateFrom)) {
				$iImportDateFrom = BT_GsrModuleTools::getTimeStamp($sDateFrom, 'db');
				// check if the date_to is set
				if (!empty($sDateTo)) {
					$iImportDateTo = BT_GsrModuleTools::getTimeStamp($sDateTo, 'db');
				}
				else {
					$iImportDateTo = time();
					$sDateTo = date('Y-m-d H:i:s', $iImportDateTo);
				}

				if ($iImportDateFrom < $iImportDateTo) {
					// get order status selection
					$aAssign['aStatusSelection'] = !empty(GSnippetsReviews::$conf['GSR_STATUS_SELECTION'])? unserialize(GSnippetsReviews::$conf['GSR_STATUS_SELECTION']) : GSnippetsReviews::$conf['GSR_STATUS_SELECTION'];

					// include
					require_once(_GSR_PATH_LIB . 'module-dao_class.php');

					// count orders of the current shop
					$aOrdersDetail = array('ok' => array(), 'ko' => array());

					$iAlreadySent = 0;

					// get orders from the selected period
					$aOrders = BT_GsrModuleDao::getOrdersIdByDate($sDateFrom, $sDateTo);
					foreach ($aOrders as $iOrder) {
						$oOrder = new Order($iOrder);

						// use case - not valid order
						if (Validate::isLoadedObject($oOrder)) {
							// use case - we exclude all the orders placed on a different shop
							if ($oOrder->id_shop == GSnippetsReviews::$iShopId) {
								// instantiate the customer obj to see if we'll send an email or not
								$oCustomer = new Customer($oOrder->id_customer);

								// use case - not a real customer
								if (Validate::isLoadedObject($oCustomer)) {
									if (!empty($oCustomer->active)) {
										if (in_array($oOrder->current_state, $aAssign['aStatusSelection'])) {
											$aOrdersDetail['ok'][$iOrder] = array(
												'id' => $iOrder,
												'ref' => (!empty($oOrder->reference)? 'Ref: '.$oOrder->reference : '') . ' / ID: '.$iOrder,
												'date' => $oOrder->date_add,
												'customer' => $oCustomer->firstname .  ' ' . $oCustomer->lastname . ' ('.$oCustomer->email.')',
												'state' => 'valid',
											);
											// detect if the reminder has already been sent and how many times
											$aOrderSent = BT_GsrModuleDao::getCallbackDetails($oOrder->id_shop, $oOrder->id);

											if (!empty($aOrderSent)) {
												$iAlreadySent++;
												$aOrdersDetail['ok'][$iOrder]['sent'] = $aOrderSent['count'];
												$aOrdersDetail['ok'][$iOrder]['date_last'] = $aOrderSent['date_last'];
											}
											unset($aOrderSent);
										}
										else {
											$aOrdersDetail['ko'][$iOrder] = array(
												'id' => $iOrder,
												'ref' => (!empty($oOrder->reference)? 'Ref: '.$oOrder->reference : '') . ' / ID: '.$iOrder,
												'date' => $oOrder->date_add,
												'customer' => $oCustomer->firstname .  ' ' . $oCustomer->lastname . ' ('.$oCustomer->email.')',
												'state' => 'not_order_status',
											);
										}
									}
									else {
										$aOrdersDetail['ko'][$iOrder] = array(
											'id' => $iOrder,
											'ref' => (!empty($oOrder->reference)? 'Ref: '.$oOrder->reference : '') . ' / ID: '.$iOrder,
											'date' => $oOrder->date_add,
											'customer' => $oCustomer->firstname .  ' ' . $oCustomer->lastname . ' ('.$oCustomer->email.')',
											'state' => 'not_active_customer',
										);
									}
								}
								else {
									$aOrdersDetail['ko'][$iOrder] = array(
										'id' => $iOrder,
										'state' => 'not_customer',
									);
								}
							}
							else {
								$aOrdersDetail['ko'][$iOrder] = array(
									'id' => $iOrder,
									'state' => 'not_order_shop',
								);
							}
						}
						else {
							$aOrdersDetail['ko'][$iOrder] = array(
								'id' => $iOrder,
								'state' => 'not_order',
							);
						}
						unset($oOrder);
					}
					$aAssign['aOrdersDetail'] = $aOrdersDetail;
					$aAssign['iNbOrders'] = count($aOrdersDetail['ok']) + count($aOrdersDetail['ko']);
					$aAssign['iNbOrdersToSend'] = count($aOrdersDetail['ok']);
					$aAssign['iOrdersSent'] = $iAlreadySent;
					$aAssign['aOrderStatusTitle'] = BT_GsrModuleDao::getOrderStatus();
					$aAssign['sDateFrom'] = $sDateFrom;
					$aAssign['sDateTo'] = $sDateTo;
					$aAssign['sShopName'] = Configuration::get('PS_SHOP_NAME');

					unset($aOrdersDetail);
					unset($aOrders);
				}
				else {
					throw new Exception(GSnippetsReviews::$oModule->l('The orders selection date start should be set as previous date from the date end', 'admin-update_class'), 180);
				}
			}
			else {
				throw new Exception(GSnippetsReviews::$oModule->l('The orders selection date start is not valid', 'admin-update_class'), 181);
			}
		}
		catch (Exception $e) {
			$aAssign['aErrors'][] = array('msg' => $e->getMessage(), 'code' => $e->getCode());
		}

		// force xhr mode activated
		GSnippetsReviews::$sQueryMode = 'xhr';

		return (
			array(
				'tpl'	    => _GSR_TPL_ADMIN_PATH . _GSR_TPL_ORDERS_IMPORT,
				'assign'	=> $aAssign,
			)
		);
	}


	/**
	 * Return the matching requested translations
	 *
	 * @param string $sSerializedVar
	 * @param string $sGlobalVar
	 * @param string $sAssignVar
	 * @return array
	 */
	private function getDefaultTranslations($sSerializedVar, $sGlobalVar)
	{
		$aTranslations = array();

		if (!empty(GSnippetsReviews::$conf['GSR_' . strtoupper($sSerializedVar)])
			&& is_string(GSnippetsReviews::$conf['GSR_' . strtoupper($sSerializedVar)])
		) {
			$aTranslations = unserialize(GSnippetsReviews::$conf['GSR_' . strtoupper($sSerializedVar)]);
		}
		else {
			foreach ($GLOBALS['GSR_' . strtoupper($sGlobalVar)] as $sIsoCode => $sTranslation) {
				$iLangId = BT_GsrModuleTools::getLangId($sIsoCode);

				if ($iLangId) {
					// get Id by iso
					$aTranslations[$iLangId] = $sTranslation;
				}
			}
		}

		return $aTranslations;
	}


	/**
	 * Return ids used for Prestashop flags displaying
	 *
	 * @return string
	 */
	private function getFlagIds()
	{
		// set
		$sFlagIds = '';

		if (!empty($this->aFlagIds)) {
			// loop on each ids
			foreach ($this->aFlagIds as $sId) {
				$sFlagIds .= $sId . '¤';
			}

			$sFlagIds = substr($sFlagIds, 0, (strlen($sFlagIds) - 2));
		}

		return $sFlagIds;
	}

	/**
	 * Set ids used for Prestashop flags displaying
	 */
	private function setFlagIds()
	{
		// set
		$sFlagIds = '';

		$this->aFlagIds = array(
			strtolower(_GSR_MODULE_NAME) . 'EmailTitle',
			strtolower(_GSR_MODULE_NAME) . 'ReviewsEmailTitle',
			strtolower(_GSR_MODULE_NAME) . 'FBPostPhrase',
		);

		// in order to get categories indexed or not by voucher type
		foreach ($GLOBALS['GSR_VOUCHERS_TYPE'] as $sType => $aVoucher) {
			if ($aVoucher['active']) {
				$this->aFlagIds[] = strtolower(_GSR_MODULE_NAME) . 'VoucherDesc' . $sType;
			}
		}
	}

	/**
	 * Set singleton
	 *
	 * @return obj
	 */
	public static function create()
	{
		static $oDisplay;

		if (null === $oDisplay) {
			$oDisplay = new BT_AdminDisplay();
		}
		return $oDisplay;
	}
}