<?php
/**
 * review-generate_class.php file defines method to generate a CSV file to export all the reviews
 */

class BT_ReviewGenerate implements BT_IAdmin
{
	/**
	 * Magic Method __construct
	 */
	private function __construct()
	{

	}

	/**
	 * Magic Method __destruct
	 */
	public function __destruct()
	{

	}

	/**
	 * rDeals with callback reviews and send email to customers
	 *
	 * @param string $sType => define which method to execute
	 * @param array $aParam
	 * @return array
	 */
	public function run($sType, array $aParam = null)
	{
		// set variables
		$aDisplayInfo = array();

		switch ($sType) {
			case 'reviews-csv' : // use case - generate a CSV file of the reviews to export
				$aDisplayInfo = call_user_func_array(array($this, 'generateReviewsCsv'), array($aParam));
				break;
			default :
				break;
		}

		return $aDisplayInfo;
	}

	/**
	 * generate a CSV file
	 *
	 * @param array $aPost
	 * @return array
	 */
	private function generateReviewsCsv(array $aPost)
	{
		try {
			require_once(_GSR_PATH_LIB . 'module-dao_class.php');

			$aAssign = array();

			// get the language
			$iReviewLang = Tools::getValue('bt_export-review-lang');
			$bEncodeUtf8 = Tools::getValue('bt_export-utf8');

			if (!empty($iReviewLang)) {
				$oLang = new Language($iReviewLang);
				$sLanguageName = $oLang->name;
				unset($oLang);
			}

			// set file name
			$sFileName = 'shop' . GSnippetsReviews::$iShopId . '-reviews-export_' . (!empty($iReviewLang) ? Tools::strtolower(BT_GsrModuleTools::getLangIso($iReviewLang)) . '_' : '') . date('Ymd', time());

			$aAssign['sShopName'] = Configuration::get('PS_SHOP_NAME');
			$aAssign['sCR'] = "\n";
			$aAssign['sExtension'] = '.csv';
			$aAssign['sFileUrl'] = _GSR_MODULE_URL . 'export/';
			$aAssign['sFilename'] = $sFileName;
			$aAssign['iMaxRating'] = _GSR_MAX_RATING;

			if (!empty($sLanguageName)) {
				$aAssign['sLanguage'] = $sLanguageName;
			}

			require_once(_GSR_PATH_LIB_REVIEWS . 'review-ctrl_class.php');

			BT_ReviewCtrl::create()->getClass('reply')->oDAO->setField('id', 'AFS_ID as replyId');

			// set params for getting ratings and reviews
			$aRatingParams = array(
				'active'    => 2,
				'product'   => true,
				'customer'  => true,
				'shop'      => true,
				'date'      => 'Y-m-d H:i:s',
				'shopId'    => GSnippetsReviews::$iShopId,
				'orderBy'   => 'dateAdd',
			);
			if (!empty($iReviewLang)) {
				$aRatingParams['langId'] = $iReviewLang;
			}

			// get reviews for moderation
			$aRatings = BT_ReviewCtrl::create()->run('getRatings', $aRatingParams);

			if (count($aRatings)) {
				// set params for getting reviews
				$aReviewParams = array(
					'active' => 2,
					'report' => true,
					'date' => 'Y/m/d',
				);

				foreach ($aRatings as &$aRating) {
					// get customer's back-office link
					$aRating['customerLink'] = Context::getContext()->link->getAdminLink('AdminCustomers') . '&id_customer=' . $aRating['custId'] . '&viewcustomer';

					// format date
					$aRating['dateAddUnix'] = $aRating['dateAdd'];
					$aRating['dateAdd'] = date('Y/m/d', $aRating['dateAdd']);

					// get language name
					$oLang = new Language($aRating['langId']);
					$aRating['langTitle'] = $oLang->name;
					unset($oLang);

					// set rating ID as param
					$aReviewParams['ratingId'] = $aRating['id'];

					// get related review to each rating
					$aRating['review'] = BT_Review::create()->get($aReviewParams);

					if (!empty($aRating['review'])) {
						// set review URL in standalone mode
						$aRating['review']['sReviewUrl']  = Context::getContext()->link->getModuleLink(
							_GSR_MODULE_SET_NAME,
							_GSR_FRONT_CTRL_REVIEW,
							array (
								'iRId' => $aRating['id'],
								'iPId' => $aRating['review']['productId'],
							),
							null,
							$aRating['review']['langId']
						);
						// format comment
						$aRating['review']['data']['sComment'] =  trim($aRating['review']['data']['sComment']);
					}
				}
			}

			// assign reviews
			$aAssign['aReviews'] = $aRatings;

			// assign headers
			$aAssign['aHeaders'] = array(
				'shopName' => GSnippetsReviews::$oModule->l('Shop name', 'review-generate_class'),
				'shopId' => GSnippetsReviews::$oModule->l('Shop id', 'review-generate_class'),
				'prodName' => GSnippetsReviews::$oModule->l('Product name', 'review-generate_class'),
				'prodId' => GSnippetsReviews::$oModule->l('Product ID', 'review-generate_class'),
				'dateAdd' => GSnippetsReviews::$oModule->l('Adding date', 'review-generate_class'),
				'dateUpd' => GSnippetsReviews::$oModule->l('Updating date', 'review-generate_class'),
				'dateUnix' => GSnippetsReviews::$oModule->l('Unix adding date', 'review-generate_class'),
				'custName' => GSnippetsReviews::$oModule->l('Customer name', 'review-generate_class'),
				'custID' => GSnippetsReviews::$oModule->l('Customer ID', 'review-generate_class'),
				'custEmail' => GSnippetsReviews::$oModule->l('Customer e-mail', 'review-generate_class'),
				'langName' => GSnippetsReviews::$oModule->l('Language', 'review-generate_class'),
				'langId' => GSnippetsReviews::$oModule->l('Language ID', 'review-generate_class'),
				'status' => GSnippetsReviews::$oModule->l('Review status', 'review-generate_class'),
				'rating' => GSnippetsReviews::$oModule->l('Rating', 'review-generate_class'),
				'title' => GSnippetsReviews::$oModule->l('Review title', 'review-generate_class'),
				'comment' => GSnippetsReviews::$oModule->l('Review comment', 'review-generate_class'),
			);

			// decode all the text that could use accent or specials characters
			if (empty($bEncodeUtf8)) {
				foreach ($aAssign['aHeaders'] as $sKey => $sTitle) {
					$aAssign['aHeaders'][$sKey] = utf8_decode($sTitle);
				}
			}

			if (!empty($aAssign['aReviews'])) {
				$aReviewsCsv = array();

				// set the file resource
				$rFile = fopen(_GSR_REVIEWS_EXPORT_PATH . $sFileName . '.csv', 'w');

				// write the header
				fputcsv($rFile, $aAssign['aHeaders'], '|');

				foreach ($aAssign['aReviews'] as $iKey => &$aRating) {
					$aReviewsCsv = array(
						'shopName' => (empty($bEncodeUtf8)? utf8_decode($aRating['shopName']) : $aRating['shopName']),
						'shopId' => $aRating['shopId'],
						'name' => (empty($bEncodeUtf8)? utf8_decode($aRating['name']) : $aRating['name']),
						'prodId' => $aRating['prodId'],
						'dateAdd' => $aRating['dateAdd'],
						'dateUpd' => (!empty($aRating['dateUpd'])? $aRating['dateUpd'] : ''),
						'dateAddUnix' => $aRating['dateAddUnix'],
						'firstname' => (!empty($aRating['firstname'])?  empty($bEncodeUtf8)? utf8_decode($aRating['firstname']) : $aRating['firstname'] : ''),
						'lastname' => (!empty($aRating['lastname'])?  empty($bEncodeUtf8)? utf8_decode($aRating['lastname']) : $aRating['lastname'] : ''),
						'custId' => $aRating['custId'],
						'email' => $aRating['email'],
						'langTitle' => (empty($bEncodeUtf8)? utf8_decode($aRating['langTitle']) : $aRating['langTitle']),
						'langId' => $aRating['langId'],
						'status' => $aRating['status'],
						'note' => $aRating['note'],
						'sTitle' => (!empty($aRating['review']['data']['sTitle'])?  empty($bEncodeUtf8)? utf8_decode($aRating['review']['data']['sTitle']) : $aRating['review']['data']['sTitle'] : ''),
						'sComment' => (!empty($aRating['review']['data']['sComment'])?  empty($bEncodeUtf8)? utf8_decode($aRating['review']['data']['sComment']) : $aRating['review']['data']['sComment'] : ''),
					);

					if (!empty($aRating['review']['data']['sComment'])) {
						$aRating['review']['data']['sComment'] = str_replace("\n", "<br/>", trim($aRating['review']['data']['sComment']));
					}

					// write the csv file
					fputcsv($rFile, $aReviewsCsv, '|');
				}
				// close the resource
				fclose($rFile);

				// write file successfully
				$aAssign['iSuccess'] = true;
			}
			else {
				throw new Exception(GSnippetsReviews::$oModule->l('There are no reviews', 'review-display_class') . (!empty($sLanguageName)? ' '. GSnippetsReviews::$oModule->l('for this language', 'review-display_class') .' "'. $sLanguageName .'"' : '') .'.', 101);
			}
		}
		catch (Exception $e) {
			$aAssign['aErrors'][] = array('msg' => $e->getMessage(), 'code' => $e->getCode());
		}

		$aAssign['bUpdate'] = empty($aAssign['aErrors']) ? true : false;
		$aAssign['sErrorInclude'] = BT_GsrModuleTools::getTemplatePath(_GSR_PATH_TPL_NAME . _GSR_TPL_ADMIN_PATH . _GSR_TPL_ERROR);
		$aAssign['sConfirmInclude'] = BT_GsrModuleTools::getTemplatePath(_GSR_PATH_TPL_NAME . _GSR_TPL_ADMIN_PATH . _GSR_TPL_CONFIRM);

		return (
			array(
				'tpl'	    => _GSR_TPL_REVIEWS_TOOL_PATH . _GSR_TPL_REVIEWS_EXPORT_HTML,
				'assign'	=> $aAssign,
			)
		);
	}

	/**
	 * Set singleton
	 *
	 * @return obj
	 */
	public static function create()
	{
		static $oSend;

		if (null === $oSend) {
			$oSend = new BT_ReviewGenerate();
		}

		return $oSend;
	}
}