<?php
/**
 * 2010-2018 Webkul.
 *
 * NOTICE OF LICENSE
 *
 * All right is reserved,
 * Please go through this link for complete license : https://store.webkul.com/license.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this module to newer
 * versions in the future. If you wish to customize this module for your
 * needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
 *
 *  @author    Webkul IN <support@webkul.com>
 *  @copyright 2010-2018 Webkul IN
 *  @license   https://store.webkul.com/license.html
 */

include_once dirname(__FILE__).'/../../config/config.inc.php';
include_once 'wkproductrating.php';

class CronProductRating
{
    /**
     * InitContent function.
     */
    public function __construct()
    {
        if (Configuration::get('WK_PRODUCT_RATING_VOUCHER_TIMEOUT')) {
            $objRatingHistory = new WkRatingHistory();
            $objRateing = new WkRating;
            $allOrers = $objRateing->getAllOrderId();
            if ($allOrers) {
                foreach ($allOrers as $idOrder) {
                    $ratingHistoryDetails = $objRatingHistory->getRatingHistoryNotRatedByOrderId($idOrder['id_order']);
                    if ($ratingHistoryDetails) {
                        $currentDate = date('y-m-d');
                        $days = Configuration::get('WK_PRODUCT_RATING_VOUCHER_TIMEOUT');
                        $expireDate = date('y-m-d', strtotime($ratingHistoryDetails['date_add']. ' + '.$days.' days'));
                        if ($currentDate > $expireDate) {
                            $objRatingHistory = new WkRatingHistory($ratingHistoryDetails['id_rating_history']);
                            $objRatingHistory->is_expire = 1;
                            $objRatingHistory->update();
                        }
                    }
                }
            }
        }
    }
}
new CronProductRating();
