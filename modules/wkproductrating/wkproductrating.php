<?php
/**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

include_once dirname(__FILE__).'/classes/WkRating.php';
include_once dirname(__FILE__).'/classes/WkRatingVoucher.php';
include_once dirname(__FILE__).'/classes/WkRatingHistory.php';


class WkProductRating extends Module
{
    /** @var string sql file name */
    const _INSTALL_SQL_FILE_ = 'install.sql';

    public function __construct()
    {
        $this->name = 'wkproductrating';
        $this->tab = 'front_office_features';
        $this->version = '2.0.0';
        $this->module_key = '7a77e5e1f3646ba3da4a71222cabdb39';
        $this->author = 'Webkul';
        $this->secure_key = Tools::encrypt($this->name);
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;
        $obj_emp = new Employee(1);
        $this->adminEmail = $obj_emp->email;
        parent::__construct();
        $this->displayName = $this->l('Rate Your Bought Product');
        $this->description = $this->l('Customer can comment and rate their bought products.');
        $this->confirmUninstall = $this->l('Are you sure?');
    }

    /**
     * Overriding the Module::install() function
     *
     * @return bool
     * @see Module::install()
     */
    public function install()
    {
        Configuration::updateValue('WK_PRODUCT_RATING_STATUS', 0);
        Configuration::updateValue('WK_PRODUCT_RATING_COMMENT_LIMIT', 1);
        Configuration::updateValue('WK_PRODUCT_RATING_LIST_POS', 1);
        Configuration::updateValue('WK_PRODUCT_RATING_SET_LIMIT', 1000);
        Configuration::updateValue('WK_PRODUCT_RATING_RATINGS', 1);
        Configuration::updateValue('WK_PRODUCT_RATING_COMMENT', 0);
        Configuration::updateValue('WK_PRODUCT_RATING_SET_COMMENT_LIMIT', 10);
        Configuration::updateValue('WK_PRODUCT_RATING_COMMENT_DELETE', 1);
        Configuration::updateValue('WK_REVIEW_STATUS', 5);
        Configuration::updateValue('WK_PRODUCT_RATING_REMINDER_MAIL', 1);
        Configuration::updateValue('WK_PRODUCT_RATING_GIVE_VOUCHER', 1);
        Configuration::updateValue('WK_PRODUCT_RATING_VOUCHER_TIMEOUT', 7);
        Configuration::updateValue('WK_PRODUCT_RATING_ADMIN_NOTI', 1);
        Configuration::updateValue('WK_PRODUCT_RATING_ADMIN_NOTI_VOUCHER', 1);
        Configuration::updateValue('WK_RATING_VOUCHER_HIGHLIGHT', 1);
        Configuration::updateValue('WK_RATING_VOUCHER_PARTIAL_USE', 1);
        Configuration::updateValue('WK_RATING_VOUCHER_PREFIX', 'wk');
        Configuration::updateValue('WK_RATING_VOUCHER_VALIDITY', 30);
        Configuration::updateValue('WK_RATING_VOUCHER_MIN_AMOUNT', 10);
        Configuration::updateValue('WK_RATING_VOUCHER_MIN_AMOUNT_CURRENCY', 1);
        Configuration::updateValue('WK_RATING_VOUCHER_MIN_AMOUNT_TAX', 0);
        Configuration::updateValue('WK_RATING_VOUCHER_MIN_AMOUNT_SHIPPING', 0);
        Configuration::updateValue('WK_RATING_VOUCHER_CATEGORY_RESTRICTION', 0);
        Configuration::updateValue('WK_RATING_VOUCHER_CUMULATIVE', 1);
        Configuration::updateValue('WK_RATING_VOUCHER_CATEGORIES', '');
        Configuration::updateValue('WK_RATING_REWARD_VOUCHER_TYPE', 0);
        Configuration::updateValue('WK_RATING_REWARD_VOUCHER_AMOUNT', 30);
        Configuration::updateValue('WK_RATING_REWARD_VOUCHER_AMOUNT_CURRENCY', 1);
        Configuration::updateValue('WK_RATING_REWARD_VOUCHER_AMOUNT_TAX', 0);

        if (!parent::install()
            || !$this->createTable()
            || !$this->callInstallTab()
            || !$this->callTopRatedMenu()
            || !$this->registerHook('displayAdminAfterHeader')
            || !$this->registerHook('displayOrderDetail')
            || !$this->registerHook('displayLeftColumn')
            || !$this->registerHook('displayRightColumn')
            || !$this->registerHook('actionFrontControllerSetMedia')
            || !$this->registerHook('displayFooterProduct')
            || !$this->registerHook('displayCustomerAccount')
            || !$this->registerHook('actionOrderStatusPostUpdate')
            || !$this->registerHook('displayProductListReviews')
            || !$this->registerHook('actionObjectCustomerDeleteBefore')
        ) {
            return false;
        }

        return true;
    }

    public function callTopRatedMenu()
    {
        if (Module::isEnabled('blocktopmenu')) {
            Configuration::updateValue('Wk_Top_Rated_Menu', 1);
            WkRating::addTopMenuRatedProducts();
        } else {
            Configuration::updateValue('Wk_Top_Rated_Menu', 0);
        }

        return true;
    }

    /**
     * Creating table for module
     *
     * @return bool
     */
    public function createTable()
    {
        if (!file_exists(dirname(__FILE__).'/'.self::_INSTALL_SQL_FILE_)) {
            return false;
        } elseif (!$sql = Tools::file_get_contents(dirname(__FILE__).'/'.self::_INSTALL_SQL_FILE_)) {
            return false;
        }

        $sql = str_replace(array('PREFIX_',  'ENGINE_TYPE'), array(_DB_PREFIX_, _MYSQL_ENGINE_), $sql);
        $sql = preg_split("/;\s*[\r\n]+/", $sql);

        foreach ($sql as $query) {
            if ($query) {
                if (!Db::getInstance()->execute(trim($query))) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Calling installTab() function
     *
     * @return bool
     * @see    Marketplace::installTab()
     */
    public function callInstallTab()
    {
        $this->installTab('AdminWebkulRating', 'Product Rating', 'AdminCatalog');
        $this->installTab('AdminVoucherRating', 'Rating Voucher Details', 'AdminCatalog');
        $this->installHiddenTab('AdminVoucherRatingDetails', 'Voucher History');

        return true;
    }

    /**
     * Undocumented function
     *
     * @param [type] $className
     * @param [type] $tabName
     * @return void
     */
    public function installHiddenTab($className, $tabName)
    {
        //Tab will not display in Back-End, only we can use as an admin controller
        $tabParentId = -1;
        $this->installProductRatingTab($className, $tabName, $tabParentId);
    }

    /**
     * Display multiple tabs on all configuration controllers and seller details controller
     *
     * @return tpl
     */
    public function hookDisplayAdminAfterHeader()
    {
        if ($currentController = Tools::getValue('controller')) {
            $configurationControllers = array(
                'AdminVoucherRating',
                'AdminVoucherRatingDetails'
            );

            if (in_array($currentController, $configurationControllers)) {
                return $this->display(__FILE__, 'displayratingconfigurationtabs.tpl');
            }
        }
    }

    /**
     * InstallProductRatingTab function
     *
     * @param [type] $className
     * @param [type] $tabName
     * @param [type] $tabParentId
     * @param boolean $tabParentName
     * @return void
     */
    public function installProductRatingTab($className, $tabName, $tabParentId, $tabParentName = false)
    {
        $tab = new Tab();
        $tab->active = 1;
        $tab->class_name = $className;
        $tab->name = array();

        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = $tabName;
        }

        if ($tabParentName) {
            $tab->id_parent = (int) Tab::getIdFromClassName($tabParentName);
        } else {
            $tab->id_parent = $tabParentId;
        }

        $tab->module = $this->name;

        return $tab->add();
    }

    /**
     * Install admin tabs
     *
     * @param  string  $className
     * @param  string  $tabName
     * @param  boolean $tabParentName
     * @return  bool
     */
    public function installTab($className, $tabName, $tabParentName = false)
    {
        $tabParentId = 0; //Tab will display in Back-End
        if ($tabParentName) {
            $this->installProductRatingTab($className, $tabName, $tabParentId, $tabParentName);
        } else {
            $this->installProductRatingTab($className, $tabName, $tabParentId);
        }
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'WK_REVIEW_STATUS' => Configuration::get('WK_REVIEW_STATUS'),
            'WK_PRODUCT_RATING_STATUS' => Configuration::get('WK_PRODUCT_RATING_STATUS'),
            'WK_PRODUCT_RATING_COMMENT_LIMIT' => Configuration::get('WK_PRODUCT_RATING_COMMENT_LIMIT'),
            'WK_PRODUCT_RATING_SET_LIMIT' => Configuration::get('WK_PRODUCT_RATING_SET_LIMIT'),
            'WK_PRODUCT_RATING_LIST_POS' => Configuration::get('WK_PRODUCT_RATING_LIST_POS'),
            'WK_PRODUCT_RATING_RATINGS' => Configuration::get('WK_PRODUCT_RATING_RATINGS'),
            'WK_PRODUCT_RATING_COMMENT' => Configuration::get('WK_PRODUCT_RATING_COMMENT'),
            'WK_PRODUCT_RATING_SET_COMMENT_LIMIT' => Configuration::get('WK_PRODUCT_RATING_SET_COMMENT_LIMIT'),
            'WK_PRODUCT_RATING_COMMENT_DELETE' => Configuration::get('WK_PRODUCT_RATING_COMMENT_DELETE'),
            'WK_PRODUCT_RATING_REMINDER_MAIL' => Configuration::get('WK_PRODUCT_RATING_REMINDER_MAIL'),
            'WK_PRODUCT_RATING_GIVE_VOUCHER' => Configuration::get('WK_PRODUCT_RATING_GIVE_VOUCHER'),
            'WK_PRODUCT_RATING_VOUCHER_TIMEOUT' => Configuration::get('WK_PRODUCT_RATING_VOUCHER_TIMEOUT'),
            'WK_PRODUCT_RATING_ADMIN_NOTI' => Configuration::get('WK_PRODUCT_RATING_ADMIN_NOTI'),
            'WK_PRODUCT_RATING_ADMIN_NOTI_VOUCHER' => Configuration::get('WK_PRODUCT_RATING_ADMIN_NOTI_VOUCHER'),
        );
    }

   /**
     * Load the configuration form
     */
    public function getContent()
    {
        $this->_html = '';

        if (Tools::isSubmit('btnSubmit_approvalconfig')) {
            $this->postValidation();
            if (empty($this->postErrors)) {
                $this->postProcess();
            } else {
                foreach ($this->postErrors as $error) {
                    $this->_html .= $this->displayError($error);
                }
            }
        } elseif (Tools::isSubmit('btnSubmit_settingconfig')) {
            $this->postValidation();
            if (empty($this->postErrors)) {
                $this->postProcess();
            } else {
                foreach ($this->postErrors as $error) {
                    $this->_html .= $this->displayError($error);
                }
            }
        } elseif (Tools::isSubmit('btnSubmit_notificationsettings')) {
            $this->postValidation();
            if (!count($this->postErrors)) {
                $this->postProcess();
            } else {
                foreach ($this->postErrors as $error) {
                    $this->_html .= $this->displayError($error);
                }
            }
        } else {
            $this->_html .= '';
        }

        //render form for general configuation
        $this->_html .= $this->renderForm();

        $ratingVoucherConfigs = Configuration::getMultiple(
            array(
                'WK_RATING_VOUCHER_APPROVE',
                'WK_RATING_VOUCHER_HIGHLIGHT',
                'WK_RATING_VOUCHER_PARTIAL_USE',
                'WK_RATING_VOUCHER_PREFIX',
                'WK_RATING_VOUCHER_VALIDITY',
                'WK_RATING_VOUCHER_MIN_AMOUNT',
                'WK_RATING_VOUCHER_MIN_AMOUNT_CURRENCY',
                'WK_RATING_VOUCHER_MIN_AMOUNT_TAX',
                'WK_RATING_VOUCHER_MIN_AMOUNT_SHIPPING',
                'WK_RATING_VOUCHER_CATEGORY_RESTRICTION',
                'WK_RATING_VOUCHER_CUMULATIVE',
                'WK_RATING_VOUCHER_CATEGORIES',
                'WK_RATING_REWARD_VOUCHER_TYPE',
                'WK_RATING_REWARD_VOUCHER_AMOUNT',
                'WK_RATING_REWARD_VOUCHER_AMOUNT_TAX',
                'WK_RATING_REWARD_VOUCHER_AMOUNT_CURRENCY'
            )
        );

        $selectedCategories = Tools::jsonDecode(
            $ratingVoucherConfigs['WK_RATING_VOUCHER_CATEGORIES'],
            true
        );
        if (!$selectedCategories) {
            $selectedCategories = array();
        }

        $root = Category::getRootCategory();
        $category_tree_voucher = new HelperTreeCategories(
            'wk-rating-associated-categories-tree',
            'Associated categories'
        );
        $category_tree_voucher
            ->setRootCategory((int) $root->id)
            ->setUseCheckBox(true)
            ->setUseSearch(true)
            ->setSelectedCategories($selectedCategories);
        $currencies = Currency::getCurrencies(false, true, true);

        $this->context->smarty->assign(
            array(
                'cron_url' => '5 0 * * * curl '.Tools::getShopDomainSsl(true, true).
                __PS_BASE_URI__.'modules/wkproductrating/cronProductRating.php'.'?token='.$this->secure_key,
                'currencies' => $currencies,
                'ratingVoucherConfigs' => $ratingVoucherConfigs,
                'category_tree_voucher' => $category_tree_voucher->render(),
                'WK_PRODUCT_RATING_REMINDER_MAIL' => Configuration::get('WK_PRODUCT_RATING_REMINDER_MAIL'),
                'WK_PRODUCT_RATING_GIVE_VOUCHER'=> Configuration::get('WK_PRODUCT_RATING_GIVE_VOUCHER'),
                'WK_PRODUCT_RATING_VOUCHER_TIMEOUT'=> Configuration::get('WK_PRODUCT_RATING_VOUCHER_TIMEOUT'),
                'WK_PRODUCT_RATING_ADMIN_NOTI'=> Configuration::get('WK_PRODUCT_RATING_ADMIN_NOTI'),
                'WK_PRODUCT_RATING_ADMIN_NOTI_VOUCHER'=> Configuration::get('WK_PRODUCT_RATING_ADMIN_NOTI_VOUCHER')
            )
        );

        $this->_html .= $this->context->smarty->fetch(
            $this->local_path.'views/templates/admin/_partials/rating_config.tpl'
        );

        Media::addJsDef(
            array('WK_PRODUCT_RATING_COMMENT_LIMIT' => Configuration::get('WK_PRODUCT_RATING_COMMENT_LIMIT'))
        );
        $this->context->controller->addJs($this->_path.'views/js/config.js');

        return $this->_html;
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        // Get default language
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $options = OrderState::getOrderStates($this->context->employee->id_lang);

        $fields_form = array();

        $fields_form[0]['form'] = array(
                'legend' => array(
                'title' => $this->l('APPROVAL SETTINGS'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l("Review need to be approve by Admin"),
                        'name' => 'WK_PRODUCT_RATING_STATUS',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            ),
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l("Customer can edit the review"),
                        'name' => 'WK_PRODUCT_RATING_RATINGS',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Customer can delete the review'),
                        'name' => 'WK_PRODUCT_RATING_COMMENT_DELETE',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'lang' => true,
                        'label' => $this->l('Status for review'),
                        'name' => 'WK_REVIEW_STATUS',
                        'desc' => $this->l('Select order status on which customer can give the review.'),
                        'options' => array(
                            'query' => $options,
                            'id' => 'id_order_state',
                            'name' => 'name'
                        ),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'name' => 'btnSubmit_approvalconfig',
                ),
        );

        $listPos = array(
            0 => array('id_group'=>'1', 'name'=>$this->l('left Col')),
            1 => array('id_group'=>'2', 'name'=>$this->l('right Col'))
        );

        $fields_form[1]['form'] = array(
                'legend' => array(
                'title' => $this->l('COMMENTS SETTINGS'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Comment field should be mandatory'),
                        'name' => 'WK_PRODUCT_RATING_COMMENT',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Do you want to set character limit for the comments'),
                        'name' => 'WK_PRODUCT_RATING_COMMENT_LIMIT',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'col' => 1,
                        'class' => 'no_of_cmnt',
                        'type' => 'text',
                        'name' => 'WK_PRODUCT_RATING_SET_LIMIT',
                        'label' => $this->l('allow characters in comment'),
                    ),
                    array(
                        'col' => 1,
                        'type' => 'text',
                        'name' => 'WK_PRODUCT_RATING_SET_COMMENT_LIMIT',
                        'label' => $this->l('Display comments on product page'),
                    ),
                    array(
                        'type' => 'group',
                        'label' => $this->l('Postion to display top Rated Products'),
                        'desc' => $this->l('select Postions where Top Rated Products block should be appeared'),
                        'name' => 'groupBox',
                        'values' => $listPos,
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'name' => 'btnSubmit_settingconfig',
                ),
        );

        $helper = new HelperForm();

        // Module, token and currentIndex
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.
                                '&configure='.$this->name.
                                '&tab_module='.$this->tab.
                                '&module_name='.$this->name;

        // Title and toolbar
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;
        $helper->table = $this->table;
        $helper->identifier = $this->identifier;

        //Language
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm($fields_form);
    }

    public function getConfigFieldsValues()
    {
        $config = array(
            'WK_REVIEW_STATUS' => Configuration::get('WK_REVIEW_STATUS'),
            'WK_PRODUCT_RATING_STATUS' => Configuration::get('WK_PRODUCT_RATING_STATUS'),
            'WK_PRODUCT_RATING_COMMENT_LIMIT' => Configuration::get('WK_PRODUCT_RATING_COMMENT_LIMIT'),
            'WK_PRODUCT_RATING_SET_LIMIT' => Configuration::get('WK_PRODUCT_RATING_SET_LIMIT'),
            'WK_PRODUCT_RATING_RATINGS' => Configuration::get('WK_PRODUCT_RATING_RATINGS'),
            'WK_PRODUCT_RATING_COMMENT' => Configuration::get('WK_PRODUCT_RATING_COMMENT'),
            'WK_PRODUCT_RATING_SET_COMMENT_LIMIT' => Configuration::get('WK_PRODUCT_RATING_SET_COMMENT_LIMIT'),
            'WK_PRODUCT_RATING_LIST_POS' => Configuration::get('WK_PRODUCT_RATING_LIST_POS'),
            'WK_PRODUCT_RATING_COMMENT_DELETE' => Configuration::get('WK_PRODUCT_RATING_COMMENT_DELETE'),
        );
        $listPos = array(
            0 => array('id_group'=>'1', 'name'=>$this->l('left Col')),
            1 => array('id_group'=>'2', 'name'=>$this->l('right Col'))
        );
        if ($listPos) {
            $i = 1;
            $selectedPages = Tools::jsonDecode(Configuration::get('WK_PRODUCT_RATING_LIST_POS'));
            foreach ($listPos as $pages) {
                if (is_array($selectedPages)) {
                    if ($selectedPages && in_array($pages['id_group'], $selectedPages)) {
                        $groupVal = 1;
                    } else {
                        $groupVal = '';
                    }
                } elseif ($selectedPages) {
                    $groupVal = 1;
                } else {
                    $groupVal = '';
                }

                $config['groupBox_'.$i] = $groupVal;

                ++$i;
            }
        }

        return $config;
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        if (Tools::isSubmit('btnSubmit_approvalconfig')) {
            Configuration::updateValue(
                'WK_REVIEW_STATUS',
                Tools::getValue('WK_REVIEW_STATUS')
            );
            Configuration::updateValue(
                'WK_PRODUCT_RATING_STATUS',
                Tools::getValue('WK_PRODUCT_RATING_STATUS')
            );
            Configuration::updateValue(
                'WK_PRODUCT_RATING_RATINGS',
                Tools::getValue('WK_PRODUCT_RATING_RATINGS')
            );
            Configuration::updateValue(
                'WK_PRODUCT_RATING_COMMENT_DELETE',
                Tools::getValue('WK_PRODUCT_RATING_COMMENT_DELETE')
            );
        }
        if (Tools::isSubmit('btnSubmit_settingconfig')) {
            Configuration::updateValue(
                'WK_PRODUCT_RATING_COMMENT_LIMIT',
                Tools::getValue('WK_PRODUCT_RATING_COMMENT_LIMIT')
            );
            Configuration::updateValue(
                'WK_PRODUCT_RATING_SET_LIMIT',
                Tools::getValue('WK_PRODUCT_RATING_SET_LIMIT')
            );
            Configuration::updateValue(
                'WK_PRODUCT_RATING_LIST_POS',
                Tools::jsonEncode(Tools::getValue('groupBox'))
            );
            Configuration::updateValue(
                'WK_PRODUCT_RATING_SET_COMMENT_LIMIT',
                Tools::getValue('WK_PRODUCT_RATING_SET_COMMENT_LIMIT')
            );
            Configuration::updateValue(
                'WK_PRODUCT_RATING_COMMENT',
                Tools::getValue('WK_PRODUCT_RATING_COMMENT')
            );
        }

        if (Tools::isSubmit('btnSubmit_notificationsettings')) {
            $voucherCategories = Tools::getValue('categoryBox');
            $rewardVoucherCategoryRestriction = Tools::getValue('WK_RATING_VOUCHER_CATEGORY_RESTRICTION');
            $ratingTimeOut = Tools::getValue('WK_PRODUCT_RATING_VOUCHER_TIMEOUT');
            $voucherPreFix = Tools::getValue('WK_RATING_VOUCHER_PREFIX');
            $voucherValid = Tools::getValue('WK_RATING_VOUCHER_VALIDITY');
            $voucherAmount = Tools::getValue('WK_RATING_REWARD_VOUCHER_AMOUNT');
            $voucherMinAmount = Tools::getValue('WK_RATING_VOUCHER_MIN_AMOUNT');

            Configuration::updateValue(
                'WK_PRODUCT_RATING_REMINDER_MAIL',
                Tools::getValue('WK_PRODUCT_RATING_REMINDER_MAIL')
            );
            Configuration::updateValue(
                'WK_PRODUCT_RATING_GIVE_VOUCHER',
                Tools::getValue('WK_PRODUCT_RATING_GIVE_VOUCHER')
            );
            Configuration::updateValue(
                'WK_PRODUCT_RATING_VOUCHER_TIMEOUT',
                $ratingTimeOut
            );
            Configuration::updateValue(
                'WK_PRODUCT_RATING_ADMIN_NOTI',
                Tools::getValue('WK_PRODUCT_RATING_ADMIN_NOTI')
            );

            // voucher details
            Configuration::updateValue(
                'WK_RATING_VOUCHER_APPROVE',
                Tools::getValue('WK_RATING_VOUCHER_APPROVE')
            );
            Configuration::updateValue(
                'WK_RATING_VOUCHER_HIGHLIGHT',
                Tools::getValue('WK_RATING_VOUCHER_HIGHLIGHT')
            );
            Configuration::updateValue(
                'WK_RATING_VOUCHER_PARTIAL_USE',
                Tools::getValue('WK_RATING_VOUCHER_PARTIAL_USE')
            );
            Configuration::updateValue(
                'WK_RATING_VOUCHER_PREFIX',
                $voucherPreFix
            );
            Configuration::updateValue(
                'WK_RATING_VOUCHER_VALIDITY',
                $voucherValid
            );
            Configuration::updateValue(
                'WK_RATING_VOUCHER_MIN_AMOUNT',
                $voucherMinAmount
            );
            Configuration::updateValue(
                'WK_RATING_VOUCHER_MIN_AMOUNT_CURRENCY',
                Tools::getValue('WK_RATING_VOUCHER_MIN_AMOUNT_CURRENCY')
            );
            Configuration::updateValue(
                'WK_RATING_VOUCHER_MIN_AMOUNT_TAX',
                Tools::getValue('WK_RATING_VOUCHER_MIN_AMOUNT_TAX')
            );
            Configuration::updateValue(
                'WK_RATING_VOUCHER_MIN_AMOUNT_SHIPPING',
                Tools::getValue('WK_RATING_VOUCHER_MIN_AMOUNT_SHIPPING')
            );
            Configuration::updateValue(
                'WK_RATING_VOUCHER_CATEGORY_RESTRICTION',
                $rewardVoucherCategoryRestriction
            );
            Configuration::updateValue(
                'WK_RATING_VOUCHER_CUMULATIVE',
                Tools::getValue('WK_RATING_VOUCHER_CUMULATIVE')
            );
            Configuration::updatevalue(
                'WK_RATING_VOUCHER_CATEGORIES',
                json_encode($voucherCategories)
            );
            Configuration::updateValue(
                'WK_RATING_REWARD_VOUCHER_TYPE',
                Tools::getValue('WK_RATING_REWARD_VOUCHER_TYPE')
            );
            Configuration::updateValue(
                'WK_RATING_REWARD_VOUCHER_AMOUNT',
                $voucherAmount
            );
            Configuration::updateValue(
                'WK_RATING_REWARD_VOUCHER_AMOUNT_CURRENCY',
                Tools::getValue('WK_RATING_REWARD_VOUCHER_AMOUNT_CURRENCY')
            );
            Configuration::updateValue(
                'WK_RATING_REWARD_VOUCHER_AMOUNT_TAX',
                Tools::getValue('WK_RATING_REWARD_VOUCHER_AMOUNT_TAX')
            );
        }
        $module_config = $this->context->link->getAdminLink('AdminModules');
        Tools::redirectAdmin(
            $module_config.'&configure='
            .$this->name.'&tab_module='
            .$this->tab.'&module_name='
            .$this->name.'&conf=4'
        );
    }

    /**
     * Posts a validation.
     */
    protected function postValidation()
    {
        if (Tools::isSubmit('btnSubmit_notificationsettings')) {
            $voucherCategories = Tools::getValue('categoryBox');
            $rewardVoucherCategoryRestriction = Tools::getValue('WK_RATING_VOUCHER_CATEGORY_RESTRICTION');
            if ($rewardVoucherCategoryRestriction && !$voucherCategories) {
                $this->postErrors[] = $this->l('Please select voucher restricted categories.');
            }

            $ratingTimeOut = Tools::getValue('WK_PRODUCT_RATING_VOUCHER_TIMEOUT');
            $voucherPreFix = Tools::getValue('WK_RATING_VOUCHER_PREFIX');
            $voucherValid = Tools::getValue('WK_RATING_VOUCHER_VALIDITY');
            $voucherAmount = Tools::getValue('WK_RATING_REWARD_VOUCHER_AMOUNT');
            $voucherMinAmount = Tools::getValue('WK_RATING_VOUCHER_MIN_AMOUNT');

            if (!Validate::isInt($ratingTimeOut) ||  $ratingTimeOut < 0) {
                $this->postErrors[] = $this->l('Invalid Time period of rating to receive a voucher.');
            }
            if ($voucherPreFix < 0) {
                $this->postErrors[] = $this->l('Invalid Voucher Code Prefix.');
            }
            if (!Validate::isInt($voucherValid) ||  $voucherValid <= 0) {
                $this->postErrors[] = $this->l('Invalid Voucher Validity.');
            }
            if ($voucherAmount && !Validate::isPrice($voucherAmount)) {
                $this->postErrors[] = $this->l('Voucher amount price is invalid.');
            }
            if ($voucherMinAmount && !Validate::isPrice($voucherMinAmount)) {
                $this->postErrors[] = $this->l('Voucher Min. amount price is invalid.');
            }
        }
        if (Tools::isSubmit('btnSubmit_settingconfig')) {
            $setLimit = Tools::getValue('WK_PRODUCT_RATING_SET_LIMIT');
            $commentShow = Tools::getValue('WK_PRODUCT_RATING_SET_COMMENT_LIMIT');
            if (Tools::getValue('WK_PRODUCT_RATING_COMMENT_LIMIT')) {
                if (empty($setLimit)) {
                    $this->postErrors[] = $this->l("Comment Limit should not be empty.");
                } elseif (!Validate::isUnsignedInt($setLimit)) {
                    $this->postErrors[] = $this->l("Comment Limit should be a valid number only.");
                }
            }
            if (empty($commentShow)) {
                $this->postErrors[] = $this->l("Number of comments on product first page should not be empty.");
            } elseif (!Validate::isUnsignedInt($commentShow)) {
                $this->postErrors[] = $this->l(
                    "Number of comments on product first page should be a valid number only."
                );
            }
        }
    }

    /**
     * HookActionFrontControllerSetMedia is for add js files and set variables in js.
     */
    public function hookActionFrontControllerSetMedia()
    {
        if ('order-detail' === $this->context->controller->php_self
            || 'history' === $this->context->controller->php_self
            || 'guest-tracking' === $this->context->controller->php_self
        ) {
            Media::addJsDef(
                array('WK_PRODUCT_RATING_COMMENT' => Configuration::get('WK_PRODUCT_RATING_COMMENT'),
                    'moduledir' => _MODULE_DIR_.'wkproductrating/libs/rateit/lib/img',
                    'orderUpdateAjaxLink' => $this->context->link->getModuleLink('wkproductrating', 'commentajax')
                )
            );

            if (Tools::getValue('email')
                && 'guest-tracking' === $this->context->controller->php_self
            ) {
                    $email = Tools::getValue('email');
                    $objRating = new WkRating();
                    $idCustomer = $objRating->getCustomerIdByEmail($email);
                    Media::addJsDefL('idCustomer', $idCustomer);
            } else {
                Media::addJsDefL('idCustomer', $this->context->customer->id);
            }

            Media::addJsDefL('idCustomer', $this->context->customer->id);
            Media::addJsDefL(
                'updateMsg',
                $this->l(
                    'Your rating is successfully updated. Please wait while we reload this page.'
                )
            );
            Media::addJsDefL(
                'saveMsg',
                $this->l(
                    'Your rating is successfully saved. Please wait while we reload this page.'
                )
            );
            Media::addJsDefL('errorMsg', $this->l('There are something wrong.'));
            Media::addJsDefL('titleError', $this->l('Title should not be empty.'));
            Media::addJsDefL('ratingFormatError', $this->l('Invalid Rating.'));
            Media::addJsDefL('ratingBlankError', $this->l('Rating should not be empty.'));
            Media::addJsDefL('commentError', $this->l('Comment should not be empty.'));
            Media::addJsDefL('deleteMsg', $this->l('Do you want to delete your review.'));

            $this->context->controller->addCss(_MODULE_DIR_.'wkproductrating/views/css/orderrating.css');
            $this->context->controller->addJS(_MODULE_DIR_.'wkproductrating/views/js/orderrating.js');
            $this->context->controller->addJS(_MODULE_DIR_.'wkproductrating/libs/rateit/lib/jquery.raty.min.js');
        }

        $this->context->controller->addCss(_MODULE_DIR_.'wkproductrating/views/css/top_rated.css');
    }

    /**
     * HookActionOrderStatusPostUpdate -> when order status change,
     *
     * @param type $params new order state and order id
     *
     * @return type description
     */
    public function hookActionOrderStatusPostUpdate($params)
    {
        if (Configuration::get('WK_PRODUCT_RATING_REMINDER_MAIL')) {
            $currentOrderState = $params['newOrderStatus']->id;
            $idOrder = $params['id_order'];

            if ($currentOrderState == Configuration::get('WK_REVIEW_STATUS')) {
                if (Configuration::get('WK_RATING_REWARD_VOUCHER_TYPE') == 0) {
                    $amountType = ' %';
                } else {
                    $objCurrency = new Currency(Configuration::get('WK_RATING_REWARD_VOUCHER_AMOUNT_CURRENCY'));
                    $amountType = $objCurrency->sign;
                    if (Configuration::get('WK_RATING_REWARD_VOUCHER_AMOUNT_TAX')) {
                        $amountType = $amountType.$this->l(' Tax included');
                    } else {
                        $amountType = $amountType.$this->l(' Tax excluded');
                    }
                }
                $amount = Configuration::get('WK_RATING_REWARD_VOUCHER_AMOUNT').$amountType;
                $this->context->smarty->assign(
                    array(
                        'amountType' => $amountType,
                        'amount' => $amount,
                        'giveVoucher'=> Configuration::get('WK_PRODUCT_RATING_GIVE_VOUCHER'),
                        'voucherTimeOut'=> Configuration::get('WK_PRODUCT_RATING_VOUCHER_TIMEOUT'),
                    )
                );
                $emailContent = $this->context->smarty->fetch(
                    $this->local_path.'views/templates/admin/email_content.tpl'
                );
                //mail for can review on this Product
                $mailParams = array(
                    '{customer_name}' => $this->context->customer->firstname.
                        ' '.$this->context->customer->lastname,
                    '{email_content}' => $emailContent
                );
                Mail::Send(
                    $this->context->language->id,
                    'give_rating_customer', //Specify the template file name
                    Mail::l('Thank You For Buying From us', $this->context->language->id),
                    //Mail subject with translation
                    $mailParams,
                    $this->context->customer->email,
                    null,
                    null,
                    null,
                    null,
                    null,
                    _PS_MODULE_DIR_.'wkproductrating/mails/',
                    false,
                    null,
                    null
                );

                if (Configuration::get('WK_PRODUCT_RATING_GIVE_VOUCHER')) {
                    $objRatingHistory = new WkRatingHistory();
                    $objRatingHistory->id_order = $idOrder;
                    $objRatingHistory->is_rate = 0;
                    $objRatingHistory->is_mail_send = 1;
                    $objRatingHistory->active = 1;
                    $objRatingHistory->save();
                }
            }
        }
    }

    /**
     * HookDisplayOrderDetail is for display at order detail page
     * @param  array $params Product details
     * @return HTML         orderrating.tpl includes all front end view
     */
    public function hookDisplayOrderDetail($params)
    {
        $idOrder = $params['order']->id;
        $idCustomer = $params['order']->id_customer;
        $currentState = $params['order']->current_state;
        if ($idOrder != ""
            && $currentState == Configuration::get('WK_REVIEW_STATUS')
        ) {
            $objRating = new WkRating();
            $orderDetails =  $objRating->getOrderDetailsByIdOrder($idOrder);
            if ($orderDetails) {
                foreach ($orderDetails as $key => $order) {
                    $productRating = $objRating->getRatingByIdProductAndIdCustomer($order['id_product'], $idCustomer);
                    $orderDetails[$key]['rating_available'] = 0;
                    if ($productRating) {
                        $orderDetails[$key]['rating_available'] = 1;
                        $this->context->smarty->assign('ratedIdOrder', $productRating['id_order']);
                    }
                }
                $this->context->smarty->assign(
                    array(
                        'link' => $this->context->link,
                        'limit' => Configuration::get('WK_PRODUCT_RATING_COMMENT_LIMIT'),
                        'limitValue' => Configuration::get('WK_PRODUCT_RATING_SET_LIMIT'),
                        'canEditRating' => Configuration::get('WK_PRODUCT_RATING_RATINGS'),
                        'canDeleteRating' => Configuration::get('WK_PRODUCT_RATING_COMMENT_DELETE'),
                        'commentValid' => Configuration::get('WK_PRODUCT_RATING_COMMENT'),
                        'multipleComment' => Configuration::get('WK_PRODUCT_RATING_MULTIPLE_RATINGS'),
                        'loadingImage' => _MODULE_DIR_.'wkproductrating/views/img/loading-small.gif',
                        'id_order' => $idOrder,
                        'idCustomer' => $idCustomer,
                        'orderDetails' => $orderDetails
                    )
                );

                return $this->display(__FILE__, 'orderrating.tpl');
            }
        }
    }

    /**
     * hookDisplayFooterProduct for display buyer's comments at product page
     * @param  array $params Product details
     * @return HTML         productfooter.tpl
     */
    public function hookDisplayFooterProduct($params)
    {
        $idProduct = Tools::getValue('id_product');
        $commentLimit = Configuration::get('WK_PRODUCT_RATING_SET_COMMENT_LIMIT');
        if ($idProduct) {
            $objRating = new WkRating();
            $tableContents = $objRating->getRatingDetailsByIdProduct($idProduct, $commentLimit, 0);
            $allContents = $objRating->getRatingDetailsByIdProduct($idProduct, 0, 0);
            $avgRating = $objRating->getAverageRatingByProductId($idProduct);
            $this->context->smarty->assign(
                array(
                    'tableContents' => $tableContents,
                    'avgRating' => $avgRating,
                    'idProduct' => $idProduct,
                    'commentLimit' => $commentLimit,
                    'allContents' => $allContents
                )
            );

            $this->context->controller->addCss(_MODULE_DIR_.'wkproductrating/views/css/productfooter.css');
            return $this->display(__FILE__, 'productfooter.tpl');
        }
    }

    /**
     * [hookDisplayCustomerAccount]
     * @return [type] [Add new tab in Customer My Account page]
     */
    public function hookDisplayCustomerAccount()
    {
        return $this->display(__FILE__, 'my-account.tpl');
    }

    public function hookDisplayLeftColumn()
    {
        if (Configuration::get('WK_PRODUCT_RATING_LIST_POS')
            && is_array(Tools::jsonDecode(Configuration::get('WK_PRODUCT_RATING_LIST_POS')))
        ) {
            if (in_array(1, Tools::jsonDecode(Configuration::get('WK_PRODUCT_RATING_LIST_POS')))) {
                return $this->showTopProductList();
            }
        } elseif (Configuration::get('WK_PRODUCT_RATING_LIST_POS') == 1) {
            return $this->showTopProductList();
        }
    }

    public function hookDisplayRightColumn()
    {
        if (Configuration::get('WK_PRODUCT_RATING_LIST_POS')
            && is_array(Tools::jsonDecode(Configuration::get('WK_PRODUCT_RATING_LIST_POS')))
        ) {
            if (in_array(2, Tools::jsonDecode(Configuration::get('WK_PRODUCT_RATING_LIST_POS')))) {
                return $this->showTopProductList();
            }
        } elseif (Configuration::get('WK_PRODUCT_RATING_LIST_POS') == 2) {
            return $this->showTopProductList();
        }
    }

    public function showTopProductList()
    {
        $idLang = $this->context->language->id;
        $objRating = new WkRating();
        $ratedProducts = $objRating->getAllReatedProduct();
        $productDetails = array();
        if ($ratedProducts) {
            foreach ($ratedProducts as $key => $topRate) {
                $objProduct = new Product($topRate['id_product']);
                $productDetails[$key]['id_product'] = $topRate['id_product'];
                $productDetails[$key]['name'] = $objProduct->name[$idLang];
                $productDetails[$key]['id_image'] = Product::getCover($topRate['id_product'])['id_image'];
                $productDetails[$key]['avj_rating'] = $objRating->getAverageRatingByProductId(
                    $topRate['id_product']
                )['averageRating'];
                $productDetails[$key]['price_static'] = Tools::displayPrice(
                    $objProduct->getPriceStatic($topRate['id_product'])
                );
            }

            $this->context->smarty->assign('productDetails', $productDetails);

            return $this->display(__FILE__, 'topProductList.tpl');
        }
    }

    /**
     * [hookDisplayProductListReviews -> Display the timer on category page].
     *
     * @return [html] [Display view of a timer]
     */
    public function hookDisplayProductListReviews($params)
    {
        if ($idProduct = $params['product']['id_product']) {
            $objRattings = new WkRating();
            $ratings = $objRattings->getAverageRatingByProductId($idProduct);
            if ($ratings && $ratings['averageRating']) {
                $this->context->smarty->assign('averageRating', $ratings['averageRating']);

                return $this->display(__FILE__, 'homeProductAvjRate.tpl');
            }
        }
    }

    // hook to delete all the ratting of the customer on customer delete
    public function hookActionObjectCustomerDeleteBefore($params)
    {
        if (isset($params['object']) && $idCustomer = $params['object']->id) {
            $objRatting = new WkRating();
            $objRattingVoucher = new WkRatingVoucher();
            if (!$objRatting->deleteByIdCustomer($idCustomer)) {
                $this->context->controller->errors[] = $this->l('Some error occurred while deleting customer\'s data.');
            }
            if (!$objRattingVoucher->deleteByIdCustomer($idCustomer)) {
                $this->context->controller->errors[] = $this->l('Some error occurred while deleting customer\'s data.');
            }
        }
    }

    public function deleteConfigKeys()
    {
        if (Module::isEnabled('blocktopmenu')) {
            WkRating::removeTopMenuAuction();
        }

        $var = array(
            'WK_PRODUCT_RATING_REMINDER_MAIL',
            'WK_PRODUCT_RATING_GIVE_VOUCHER',
            'WK_PRODUCT_RATING_VOUCHER_TIMEOUT',
            'WK_PRODUCT_RATING_ADMIN_NOTI',
            'WK_PRODUCT_RATING_ADMIN_NOTI_VOUCHER',
            'WK_REVIEW_STATUS',
            'WK_PRODUCT_RATING_STATUS',
            'WK_PRODUCT_RATING_RATINGS',
            'WK_PRODUCT_RATING_COMMENT_DELETE',
            'WK_PRODUCT_RATING_COMMENT_LIMIT',
            'WK_PRODUCT_RATING_SET_LIMIT',
            'WK_PRODUCT_RATING_LIST_POS',
            'WK_PRODUCT_RATING_SET_COMMENT_LIMIT',
            'WK_PRODUCT_RATING_COMMENT',
            'WK_RATING_VOUCHER_APPROVE',
            'WK_RATING_VOUCHER_HIGHLIGHT',
            'WK_RATING_VOUCHER_PARTIAL_USE',
            'WK_RATING_VOUCHER_PREFIX',
            'WK_RATING_VOUCHER_VALIDITY',
            'WK_RATING_VOUCHER_MIN_AMOUNT',
            'WK_RATING_VOUCHER_MIN_AMOUNT_CURRENCY',
            'WK_RATING_VOUCHER_MIN_AMOUNT_TAX',
            'WK_RATING_VOUCHER_MIN_AMOUNT_SHIPPING',
            'WK_RATING_VOUCHER_CATEGORY_RESTRICTION',
            'WK_RATING_VOUCHER_CUMULATIVE',
            'WK_RATING_VOUCHER_CATEGORIES',
            'WK_RATING_REWARD_VOUCHER_TYPE',
            'WK_RATING_REWARD_VOUCHER_AMOUNT',
            'WK_RATING_REWARD_VOUCHER_AMOUNT_TAX',
            'WK_RATING_REWARD_VOUCHER_AMOUNT_CURRENCY',
            'Wk_Top_Rated_Menu_ID',
        );

        foreach ($var as $key) {
            if (!Configuration::deleteByName($key)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Overriding Module::uninstall()
     *
     * @return bool
     * @see Module::uninstall()
     */
    public function uninstall()
    {
        if (!parent::uninstall()
            || !$this->uninstallTab()
            || !$this->deleteConfigKeys()
            || !$this->deleteTables()
        ) {
            return false;
        }
        return true;
    }

    public function callRatingMenu()
    {
        if (Module::isEnabled('blocktopmenu')) {
            Configuration::updateValue('Wk_Rating_Menu', 1);
            WkRating::addTopMenuRatedProducts();
        } else {
            Configuration::updateValue('Wk_Rating_Menu', 0);
        }

        return true;
    }

    /**
     * Uninstall admin tabs
     *
     * @return bool
     */
    public function uninstallTab()
    {
        $moduleTabs = Tab::getCollectionFromModule($this->name);
        if (!empty($moduleTabs)) {
            foreach ($moduleTabs as $moduleTab) {
                $moduleTab->delete();
            }
        }
        return true;
    }

    /**
     * Delete module tables
     *
     * @return bool
     */
    protected function deleteTables()
    {
        return Db::getInstance()->execute(
            'DROP TABLE IF EXISTS
            `'._DB_PREFIX_.'wk_product_rating`,
            `'._DB_PREFIX_.'wk_product_rating_voucher`,
            `'._DB_PREFIX_.'wk_product_rating_history`'
        );
    }
}
