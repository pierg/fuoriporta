- Rate Your Bought Products V2.0.0 compatible with PrestaShop V1.6.1.19
- Rate Your Bought Products V5.0.0 compatible with PrestaShop V1.7.3.4

### Important Note:

### User Guide Documentation:
https://webkul.com/blog/prestashop-rate-your-bought-product/

### Support:
Find us our support policy – https://store.webkul.com/support.html/

### Refund:
Find us our refund policy – https://store.webkul.com/refund-policy.html/
