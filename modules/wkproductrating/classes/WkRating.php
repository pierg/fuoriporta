<?php
/**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

class WkRating extends ObjectModel
{
    public $id_customer;
    public $id_product;
    public $id_order;
    public $rating;
    public $title;
    public $comment;
    public $active;
    public $date_add;
    public $date_upd;

    public static $definition = array(
        'table' => 'wk_product_rating',
        'primary' => 'id_product_rating',
        'multilang' => false,
        'fields' => array(
            'id_customer' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt',
                'required' => true, 'size' => 10
            ),
            'id_product' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt',
                'required' => true,
                'size' => 10
            ),
            'id_order' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt',
                'required' => true,
                'size' => 10
            ),
            'rating' => array(
                'type' => self::TYPE_FLOAT,
                'validate' => 'isFloat',
                'required' => true
            ),
            'title' => array(
                'type' => self::TYPE_STRING,
                'validate' => 'isMessage'
            ),
            'comment' => array(
                'type' => self::TYPE_STRING,
                'validate' => 'isMessage'
            ),
            'active' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isInt'
            ),
            'date_add' => array(
                'type' => self::TYPE_DATE,
                'validate' => 'isDateFormat',
                'required' => false
            ),
            'date_upd' => array(
                'type' => self::TYPE_DATE,
                'validate' => 'isDateFormat',
                'required' => false
            ),
        ),
    );

    /**
     * Gets the total comment count by identifier product.
     *
     * @param INT $idProduct The identifier product
     *
     * @return INT The total comment count by identifier product.
     */
    public function getTotalCommentCountByIdProduct($idProduct)
    {
        return Db::getInstance()->getValue(
            'SELECT count(`id_product_rating`)
            FROM '._DB_PREFIX_.'wk_product_rating
            WHERE `id_product` = '.(int) $idProduct.
            ' AND `active` = 1'
        );
    }

    /**
     * Gets the rating by identifier product and identifier customer.
     *
     * @param int $idProduct  The identifier product
     * @param int $idCustomer The identifier customer
     *
     * @return array  The rating by identifier product and identifier customer.
     */
    public function getRatingByIdProductAndIdCustomer($idProduct, $idCustomer)
    {
        return Db::getInstance()->getRow(
            'SELECT *
            FROM '._DB_PREFIX_.'wk_product_rating
            WHERE `id_product` = '.(int) $idProduct.'
            AND `id_customer` = '.(int) $idCustomer
        );
    }

    /**
     * Gets the order details by identifier order.
     *
     * @param int $idOrder The identifier order
     *
     * @return array The order details by identifier order.
     */
    public function getOrderDetailsByIdOrder($idOrder)
    {
        return Db::getInstance()->executeS(
            'SELECT wkpr.*, od.`product_id` as `id_product`, o.`id_customer`, pl.`name`, o.`id_order`
            FROM `'._DB_PREFIX_.'orders` AS o
            JOIN `'._DB_PREFIX_.'order_detail` AS od
            ON (o.`id_order` = od.`id_order`)
            JOIN '._DB_PREFIX_.'product_lang pl
            ON (pl.`id_product` = od.`product_id`)
            LEFT JOIN `'._DB_PREFIX_.'wk_product_rating` AS wkpr
            ON (od.`product_id` = wkpr.`id_product` AND od.`id_order` = wkpr.`id_order`)
            WHERE od.`id_order` = '.(int) $idOrder.'
            GROUP BY od.`product_id`'
        );
    }

    /**
     * Gets the customer identifier by order identifier.
     *
     * @param int $idOrder The order identifier
     *
     * @return int  The customer identifier by order identifier.
     */
    public function getCustomerIdByOrderId($idOrder)
    {
        return Db::getInstance()->getRow(
            'SELECT `id_customer`
            FROM `'._DB_PREFIX_.'orders`
            WHERE `id_order` ='.(int) $idOrder
        );
    }

    /**
     * Gets the rating details by identifier product.
     *
     * @param integer $idProduct The identifier product
     * @param integer $isLimited Indicates if limited
     * @param integer $endLimit  The end limit
     *
     * @return array The rating details by identifier product.
     */
    public function getRatingDetailsByIdProduct($idProduct, $isLimited, $endLimit)
    {
        if ($idProduct) {
            $sql = 'SELECT *, pr.`date_upd`
                    FROM '._DB_PREFIX_.'wk_product_rating pr
                    JOIN '._DB_PREFIX_.'customer cu
                    ON (pr.id_customer = cu.id_customer)
                    WHERE id_product = '.(int)$idProduct.
                    ' and pr.active = 1';
            if ($isLimited !=0 && $endLimit == 0) {
                $sql .= ' LIMIT '.(int)$isLimited;
            } elseif ($endLimit) {
                $sql .= ' LIMIT '.(int)$isLimited.',' .(int)$endLimit;
            }

            return Db::getInstance()->executeS($sql);
        }
    }

    /**
     * Gets all order identifier.
     *
     * @return array  All order identifier.
     */
    public function getAllOrderId()
    {
        return Db::getInstance()->executeS(
            'SELECT `id_order`
            FROM '._DB_PREFIX_.'orders
            ORDER BY `id_order` ASC'
        );
    }

    /**
     * Gets the product name by order identifier.
     *
     * @param int $idOrder The identifier order
     *
     * @return array  The product name by order identifier.
     */
    public function getProductNameByOrderId($idOrder)
    {
        return Db::getInstance()->executeS(
            'SELECT pl.`id_product`, pl.`name`
            FROM `'._DB_PREFIX_.'product_lang` AS pl
            JOIN `'._DB_PREFIX_.'order_detail` AS od
            ON (pl.`id_product` = od.`product_id`)
            LEFT JOIN `'._DB_PREFIX_.'wk_product_rating` as wkpr
            ON (pl.`id_product` = wkpr.`id_product`)
            AND (od.`id_order` = wkpr.`id_order`)
            WHERE od.`id_order` = '.(int) $idOrder.'
            AND wkpr.`id_product_rating` IS NULL
            GROUP BY pl.`id_product`'
        );
    }

    /**
     * Gets all details of rating by identifier.
     *
     * @param INT $id The identifier
     *
     * @return INT  All details of rating by identifier.
     */
    public function getAllDetailsOfRatingById($id)
    {
        return Db::getInstance()->getRow(
            'SELECT *
            FROM `'._DB_PREFIX_.'wk_product_rating`
            WHERE `id_product_rating` = '.(int) $id
        );
    }

    /**
     * GetAverageRatingByProductId average rating of a product
     *
     * @param int $idProduct id_product
     *
     * @return int  Average Rating
     */
    public function getAverageRatingByProductId($idProduct)
    {
        return Db::getInstance()->getRow(
            'SELECT ROUND(AVG(rating), 0) as averageRating
            FROM `'._DB_PREFIX_.'wk_product_rating`
            WHERE `id_product` = '.(int) $idProduct.'
            AND `active` = 1'
        );
    }

    /**
    * GetCustomerIdByEmail for guest geting CustomerId
    *
    * @param string $email email of the guest

    * @return string email
    */
    public function getCustomerIdByEmail($email)
    {
        return Db::getInstance()->getValue(
            'SELECT `id_customer`
            FROM '._DB_PREFIX_.'customer
            WHERE `email` = \''.pSQL($email).'\''
        );
    }

    /**
     * Get Comments by CustomerId
     *
     * @param  [type] $idCustomer
     * @return void
     */
    public function getAllCommentsByIdCustomer($idCustomer)
    {
        return Db::getInstance()->executeS(
            'SELECT *
            FROM `'._DB_PREFIX_.'wk_product_rating`
            WHERE `id_customer` = '.(int) $idCustomer
        );
    }

    public static function addTopMenuRatedProducts()
    {
        $linksLabel = array();
        $labels = array();

        $link = new Link();
        $topRatedPageLink = $link->getModuleLink('wkproductrating', 'topRatedProducts');

        $languages = Language::getLanguages();
        foreach ($languages as $val) {
            $linksLabel[$val['id_lang']] = $topRatedPageLink;
            $labels[$val['id_lang']] = 'Top Rated';
        }

        $shops = Shop::getContextListShopID();
        foreach ($shops as $shopId) {
            $idLinksMenutop = self::addMenu($linksLabel, $labels, (int)$shopId, 1);
            if ($idLinksMenutop) {
                $newItem = 'LNK'.$idLinksMenutop;
                //Add new menu with available menus
                $lastItems = Configuration::get('MOD_BLOCKTOPMENU_ITEMS');
                $itemsArr = explode(',', $lastItems);
                $itemsArr[] = $newItem;

                $shopGroupId = Shop::getGroupFromShop($shopId);
                Configuration::updateValue(
                    'MOD_BLOCKTOPMENU_ITEMS',
                    (string)implode(',', $itemsArr),
                    false,
                    (int)$shopGroupId,
                    (int)$shopId
                );

                Configuration::updateValue('Wk_Top_Rated_Menu_ID', $idLinksMenutop);
            }
        }

        self::clearMainMenuCache();
    }

    public static function clearMainMenuCache()
    {
        $dir = _PS_CACHE_DIR_ . DIRECTORY_SEPARATOR . 'ps_mainmenu';
        if (!is_dir($dir)) {
            return;
        }

        foreach (scandir($dir) as $entry) {
            if (preg_match('/\.json$/', $entry)) {
                unlink($dir . DIRECTORY_SEPARATOR . $entry);
            }
        }
    }

    public static function addMenu($link, $label, $idShop, $newWindow = 0)
    {
        if (!is_array($label)) {
            return false;
        }
        if (!is_array($link)) {
            return false;
        }

        Db::getInstance()->insert(
            'linksmenutop',
            array(
                'new_window'=>(int)$newWindow,
                'id_shop' => (int)$idShop
            )
        );

        $idLinksMenutop = Db::getInstance()->Insert_ID();

        foreach ($label as $idLang => $label) {
            Db::getInstance()->insert(
                'linksmenutop_lang',
                array(
                    'id_linksmenutop'=>(int)$idLinksMenutop,
                    'id_lang'=>(int)$idLang,
                    'id_shop'=>(int)$idShop,
                    'label'=>pSQL($label),
                    'link'=>pSQL($link[$idLang])
                )
            );
        }

        if ($idLinksMenutop) {
            return $idLinksMenutop;
        } else {
            return false;
        }
    }

    /**
     * [removeTopMenuAuction - Remove Auction menu when module is uninstall]
     * @return [type] [description]
     */
    public static function removeTopMenuAuction()
    {
        $topRatedMenuId = Configuration::get('Wk_Top_Rated_Menu_ID');
        if ($topRatedMenuId) {
            $ratingItem = 'LNK'.$topRatedMenuId;

            $lastItems = Configuration::get('MOD_BLOCKTOPMENU_ITEMS');
            $itemsArr = explode(',', $lastItems);
            if (($itemkey = array_search($ratingItem, $itemsArr)) !== false) {
                unset($itemsArr[$itemkey]);
            }

            self::updateTopMenuItems($itemsArr);

            //Remove from database
            Db::getInstance()->delete('linksmenutop', 'id_linksmenutop = '.(int)$topRatedMenuId);
            Db::getInstance()->delete('linksmenutop_lang', 'id_linksmenutop = '.(int)$topRatedMenuId);
        }

        return true;
    }

    public static function updateTopMenuItems($itemsArr)
    {
        $shops = Shop::getContextListShopID();
        foreach ($shops as $shopId) {
            $shopGroupId = Shop::getGroupFromShop($shopId);

            if (count($shops) == 1) {
                if (is_array($itemsArr) && count($itemsArr)) {
                    Configuration::updateValue(
                        'MOD_BLOCKTOPMENU_ITEMS',
                        (string)implode(',', $itemsArr),
                        false,
                        (int)$shopGroupId,
                        (int)$shopId
                    );
                } else {
                    Configuration::updateValue('MOD_BLOCKTOPMENU_ITEMS', '', false, (int)$shopGroupId, (int)$shopId);
                }
            }
        }

        self::clearMainMenuCache();

        return true;
    }

    public function getAllReatedProduct()
    {
        return Db::getInstance()->executeS(
            'SELECT *
            FROM `'._DB_PREFIX_.'wk_product_rating`
            WHERE `active` = 1
            GROUP BY `id_product`
            ORDER BY `rating` DESC'
        );
    }

    public function deleteByIdCustomer($idCustomer)
    {
        return Db::getInstance()->delete(
            'wk_product_rating',
            '`id_customer`='.(int)$idCustomer
        );
    }
}
