<?php
/**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

class WkRatingHistory extends ObjectModel
{
    public $id_order;
    public $is_rate;
    public $is_expire;
    public $active;
    public $date_add;
    public $date_upd;

    public static $definition = array (
        'table' => 'wk_product_rating_history',
        'primary' => 'id_rating_history',
        'multilang' => false,
        'fields' => array (
            'id_order' => array (
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt',
                'required' => true,
                'size' => 10
            ),
            'is_rate' => array (
                'type' => self::TYPE_INT,
                'validate' => 'isInt'
            ),
            'is_expire' => array (
                'type' => self::TYPE_INT,
                'validate' => 'isInt'
            ),
            'active' => array (
                'type' => self::TYPE_INT,
                'validate' => 'isInt'
            ),
            'date_add' => array (
                'type' => self::TYPE_DATE,
                'validate' => 'isDateFormat',
                'required' => false
            ),
            'date_upd' => array (
                'type' => self::TYPE_DATE,
                'validate' => 'isDateFormat',
                'required' => false
            ),
        ),
    );

    public function getRatingHistoryByOrderId($idOrder)
    {
        return Db::getInstance()->getRow(
            'SELECT *
            FROM `'._DB_PREFIX_.'wk_product_rating_history`
            WHERE `id_order` = '.(int) $idOrder
        );
    }

    public function getRatingHistoryNotRatedByOrderId($idOrder)
    {
        return Db::getInstance()->getRow(
            'SELECT *
            FROM `'._DB_PREFIX_.'wk_product_rating_history`
            WHERE `id_order` = '.(int) $idOrder.'
            AND `is_rate` = 0'
        );
    }

    public function isGiveVoucherDateExpireByOrderId($idOrder)
    {
        return Db::getInstance()->getRow(
            'SELECT *
            FROM `'._DB_PREFIX_.'wk_product_rating_history`
            WHERE `id_order` = '.(int) $idOrder.'
            AND `is_expire` = 1'
        );
    }
}
