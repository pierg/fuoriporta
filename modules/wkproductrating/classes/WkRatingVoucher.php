<?php
/**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

class WkRatingVoucher extends ObjectModel
{
    public $id_customer;
    public $id_order;
    public $id_product;
    public $code;
    public $state;
    public $is_generated;
    public $id_cart_rule;
    public $active;
    public $date_add;
    public $date_upd;

    public static $definition = array(
        'table' => 'wk_product_rating_voucher',
        'primary' => 'id_rating_voucher',
        'multilang' => false,
        'fields' => array(
            'id_customer' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt',
                'required' => true,
                'size' => 10
            ),
            'id_order' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt',
                'required' => true,
                'size' => 10
            ),
            'id_product' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt',
                'required' => true,
                'size' => 10
            ),
            'code' => array(
                'type' => self::TYPE_STRING,
                'required' => true,
            ),
            'state' => array(
                'type' => self::TYPE_INT,
                'required' => true,
                'validate' => 'isInt'
            ),
            'is_generated' => array(
                'type' => self::TYPE_INT,
                'required' => true,
                'validate' => 'isInt'
            ),
            'id_cart_rule' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isInt'
            ),
            'active' => array(
                'type' => self::TYPE_INT,
                'required' => true,
                'validate' => 'isInt'
            ),
            'date_add' => array(
                'type' => self::TYPE_DATE,
                'validate' => 'isDateFormat',
                'required' => false
            ),
            'date_upd' => array(
                'type' => self::TYPE_DATE,
                'validate' => 'isDateFormat',
                'required' => false
            ),
        ),
    );

    public function voucherRequestAlreadyGenerated($idOrder, $idProduct)
    {
        $sql = Db::getInstance()->getRow(
            'SELECT *
            FROM '._DB_PREFIX_.'wk_product_rating_voucher
            WHERE `id_product` = '.(int) $idProduct.'
            AND `id_order` = '.(int) $idOrder
        );

        if ($sql) {
            return $sql;
        } else {
            return false;
        }
    }

    public function isVoucherRequestByCode($code)
    {
        $sql = Db::getInstance()->getRow(
            'SELECT *
            FROM '._DB_PREFIX_.'wk_product_rating_voucher
            WHERE `code` = \''.pSQL($code).'\''
        );

        if ($sql == '') {
            return false;
        }

        return $sql;
    }

    /**
     * Undocumented function
     *
     * @param [type] $idCustomer
     * @return void
     */
    public function genrateRatingVoucherForCustomer($idCustomer, $code)
    {
        $objProductRating = new WkProductRating();
        $name = $objProductRating->l('Rating Voucher.');
        $amount = Configuration::get('WK_RATING_REWARD_VOUCHER_AMOUNT');
        $validity = Configuration::get('WK_RATING_VOUCHER_VALIDITY');
        $idCurrency = Configuration::get('WK_RATING_REWARD_VOUCHER_AMOUNT_CURRENCY');

        if (!$idCustomer || !$name || !$amount) {
            return false;
        }
        if (!$idCurrency) {
            $idCurrency = Configuration::get('PS_CURRENCY_DEFAULT');
        }
        $dateFrom = date('Y-m-d');
        if ($validity > 0) {
            $dateTo = date('Y-m-d', strtotime('+'.$validity.' day', strtotime($dateFrom)));
        } else {
            $dateTo = date('Y-m-d', strtotime('2099-12-31'));
        }

        while (1) {
            if (!(int)CartRule::getIdByCode($code)) {
                break;
            }
        }

        $objCartRule = new CartRule();
        if (Configuration::get('WK_RATING_VOUCHER_CUMULATIVE')) {
            $WK_RATING_VOUCHER_CUMULATIVE = 0;
        } else {
            $WK_RATING_VOUCHER_CUMULATIVE = 1;
        }
        foreach (Language::getLanguages(true) as $lang) {
            $objCartRule->name[$lang['id_lang']] = $name;
        }
        $objCartRule->date_from = $dateFrom;
        $objCartRule->id_customer = $idCustomer;
        $objCartRule->date_to = $dateTo;
        $objCartRule->code = $code;
        if (Configuration::get('WK_RATING_REWARD_VOUCHER_TYPE') == 0) {
            $objCartRule->reduction_percent = $amount;
        } else {
            $objCartRule->reduction_amount = $amount;
            $objCartRule->reduction_tax = Configuration::get('WK_RATING_VOUCHER_AMOUNT_TAX');
            $objCartRule->reduction_currency = Configuration::get('WK_RATING_REWARD_VOUCHER_AMOUNT_CURRENCY');
        }
        $objCartRule->quantity = 1;
        $objCartRule->quantity_per_user = 1;
        $objCartRule->priority = 1;
        $objCartRule->partial_use = Configuration::get('WK_RATING_VOUCHER_PARTIAL_USE');
        $objCartRule->minimum_amount = Configuration::get('WK_RATING_VOUCHER_MIN_AMOUNT');
        $objCartRule->minimum_amount_tax = Configuration::get('WK_RATING_VOUCHER_MIN_AMOUNT_TAX');
        $objCartRule->minimum_amount_currency = Configuration::get('WK_RATING_VOUCHER_MIN_AMOUNT_CURRENCY');
        $objCartRule->minimum_amount_shipping = Configuration::get('WK_RATING_VOUCHER_MIN_AMOUNT_SHIPPING');
        $objCartRule->highlight = Configuration::get('WK_RATING_VOUCHER_HIGHLIGHT');
        $objCartRule->active = 1;
        $objCartRule->cart_rule_restriction = $WK_RATING_VOUCHER_CUMULATIVE;

        if (Configuration::get('WK_RATING_VOUCHER_CATEGORY_RESTRICTION')
            && ($WK_RATING_VOUCHER_CATEGORIES = Configuration::get(
                'WK_RATING_VOUCHER_CATEGORIES'
            ))
        ) {
            $objCartRule->product_restriction = 1;
        }
        if ($objCartRule->save()) {
            $idCartRule = $objCartRule->id;

            // Entery in category restriction tables
            if (Configuration::get('WK_RATING_VOUCHER_CATEGORY_RESTRICTION')
                && $WK_RATING_VOUCHER_CATEGORIES
            ) {
                Db::getInstance()->execute(
                    'INSERT INTO `'._DB_PREFIX_.'cart_rule_product_rule_group` (`id_cart_rule`, `quantity`)
                    VALUES ('.(int)$idCartRule.', 1)'
                );
                $idProductRuleGroup = Db::getInstance()->Insert_ID();
                if ($idProductRuleGroup) {
                    Db::getInstance()->execute(
                        'INSERT INTO `'._DB_PREFIX_.'cart_rule_product_rule` (`id_product_rule_group`, `type`)
                        VALUES ('.(int)$idProductRuleGroup.', "categories")'
                    );
                    $idProductRule = Db::getInstance()->Insert_ID();
                    if ($idProductRule) {
                        $values = array();
                        $WK_RATING_VOUCHER_CATEGORIES = json_decode(
                            $WK_RATING_VOUCHER_CATEGORIES
                        );
                        foreach ($WK_RATING_VOUCHER_CATEGORIES as $idCategory) {
                            $values[] = '('.(int)$idProductRule.','.(int)$idCategory.')';
                        }
                        if (count($values)) {
                            Db::getInstance()->execute(
                                'INSERT INTO `'._DB_PREFIX_.'cart_rule_product_rule_value`
                                 (`id_product_rule`, `id_item`) VALUES '.implode(',', $values)
                            );
                        }
                    }
                }
            }
            if (!$WK_RATING_VOUCHER_CUMULATIVE) {
                Db::getInstance()->execute(
                    'INSERT INTO `'._DB_PREFIX_.'cart_rule_combination` (`id_cart_rule_1`, `id_cart_rule_2`) (
                     SELECT id_cart_rule, '.(int)$idCartRule.'
                     FROM `'._DB_PREFIX_.'cart_rule` WHERE cart_rule_restriction = 1)'
                );
            } else {
                $ruleCombinations = Db::getInstance()->executeS(
                    'SELECT cr.id_cart_rule
                    FROM '._DB_PREFIX_.'cart_rule cr
                    WHERE cr.id_cart_rule != '.(int)$idCartRule.'
                    AND cr.cart_rule_restriction = 0
                    AND NOT EXISTS (
                        SELECT 1
                        FROM '._DB_PREFIX_.'cart_rule_combination
                        WHERE cr.id_cart_rule = '._DB_PREFIX_.'cart_rule_combination.id_cart_rule_2
                        AND '.(int)$idCartRule.' = id_cart_rule_1
                    )
                    AND NOT EXISTS (
                        SELECT 1
                        FROM '._DB_PREFIX_.'cart_rule_combination
                        WHERE cr.id_cart_rule = '._DB_PREFIX_.'cart_rule_combination.id_cart_rule_1
                        AND '.(int)$idCartRule.' = id_cart_rule_2
                    )'
                );
                foreach ($ruleCombinations as $incompatibleRule) {
                    Db::getInstance()->execute(
                        'UPDATE `'._DB_PREFIX_.'cart_rule` SET cart_rule_restriction = 1
                         WHERE id_cart_rule = '.(int)$incompatibleRule['id_cart_rule'].' LIMIT 1'
                    );
                    Db::getInstance()->execute(
                        'INSERT IGNORE INTO `'._DB_PREFIX_.'cart_rule_combination`
                        (`id_cart_rule_1`, `id_cart_rule_2`)
                        (SELECT id_cart_rule, '.(int)$incompatibleRule['id_cart_rule'].'
                        FROM `'._DB_PREFIX_.'cart_rule`
                        WHERE active = 1
                        AND id_cart_rule != '.(int)$idCartRule.'
                        AND id_cart_rule != '.(int)$incompatibleRule['id_cart_rule'].')'
                    );
                }
            }
            // admin mail for voucher Genrate
            $objCustomer = new Customer($idCustomer);
            if (Configuration::get('WK_PRODUCT_RATING_ADMIN_NOTI_VOUCHER')) {
                $mailParams = array(
                    '{customer_name}' => $objCustomer->firstname.
                        ' '.$objCustomer->lastname,
                    '{voucher_code}' => $code
                );
                Mail::Send(
                    Configuration::get('PS_LANG_DEFAULT'),
                    'rating_voucher_genrate_admin', //Specify the template file name
                    Mail::l(
                        'Voucher Genrate for Rating a Product for ',
                        Configuration::get('PS_LANG_DEFAULT')
                    ).$objCustomer->firstname.' '.$objCustomer->lastname,
                    //Mail subject with translation
                    $mailParams,
                    $objProductRating->adminEmail,
                    null,
                    null,
                    null,
                    null,
                    null,
                    _PS_MODULE_DIR_.'wkproductrating/mails/',
                    false,
                    null,
                    null
                );
            }
            // customer mail for voucher Genrate
            if (Configuration::get('WK_RATING_REWARD_VOUCHER_TYPE') == 0) {
                $amountType = ' %';
            } else {
                $objCurrency = new Currency(Configuration::get('WK_RATING_REWARD_VOUCHER_AMOUNT_CURRENCY'));
                $amountType = $objCurrency->sign;
                if (Configuration::get('WK_RATING_REWARD_VOUCHER_AMOUNT_TAX')) {
                    $amountType = $amountType.''.Mail::l(' Tax included');
                } else {
                    $amountType = $amountType.''.Mail::l(' Tax excluded');
                }
            }
            $mailParams = array(
                '{customer_name}' => $objCustomer->firstname.' '.$objCustomer->lastname,
                '{voucher_values}' => $amount.' '.$amountType,
                '{voucher_code}' => $code,
                '{voucher_validity}' => $validity
            );
            Mail::Send(
                Configuration::get('PS_LANG_DEFAULT'),
                'rating_voucher_genrate_customer', //Specify the template file name
                Mail::l('Voucher Code', Configuration::get('PS_LANG_DEFAULT')),
                //Mail subject with translation
                $mailParams,
                $objCustomer->email,
                null,
                null,
                null,
                null,
                null,
                _PS_MODULE_DIR_.'wkproductrating/mails/',
                false,
                null,
                null
            );

            return $objCartRule->id;
        }

        return false;
    }

    public function deleteByIdCustomer($idCustomer)
    {
        return Db::getInstance()->delete(
            'wk_product_rating_voucher',
            '`id_customer`='.(int)$idCustomer
        );
    }
}
