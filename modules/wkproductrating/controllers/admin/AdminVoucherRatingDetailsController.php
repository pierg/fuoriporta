<?php
/**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

class AdminVoucherRatingDetailsController extends ModuleAdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'wk_product_rating_voucher';
        $this->className = 'WkRatingVoucher';
        $this->context = Context::getContext();
        $this->identifier = 'id_rating_voucher';
        $this->_select .= 'CONCAT(cust.firstname, " ", cust.lastname) as customer_name, cr.code, cr.reduction_amount,
        cr.reduction_percent';
        $this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'cart_rule` cr ON (cr.`id_cart_rule` = a.`id_cart_rule`)';
        $this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'customer` cust ON (cust.`id_customer` = a.`id_customer`)';

        parent::__construct();

        $this->fields_list = array(
            'id_rating_voucher' => array(
                'title' => $this->l('ID'),
                'align' => 'center',
            ),
            'id_cart_rule' => array(
                'title' => $this->l('ID cart Rule'),
                'align' => 'center',
                'callback' => 'getCartRuleLink',
            ),
            'customer_name' => array(
                'title' => $this->l('Customer Name'),
                'callback' => 'getCustomerViewLink',
            ),
            'code' => array(
                'title' => $this->l('Code'),
            ),
            'reduction_amount' => array(
                'title' => $this->l('Discount Price'),
                'type' => 'price',
            ),
            'reduction_percent' => array(
                'title' => $this->l('Discount Percentage'),
            ),
        );
    }

    public function getVoucherType($voucherType, $row)
    {
        $voucherName = '-';
        if ($row['is_redeem_voucher'] && isset($voucherType)) {
            $voucherName = $this->l('Redeem Voucher');
        } else {
            $voucherName = $this->l('Affiliate Share Voucher');
        }

        return $voucherName;
    }

    public function getCustomerViewLink($customerName, $row)
    {
        $linkCustomer = '';
        if ($idCustomer = $row['id_customer']) {
            $objCustomer = new Customer($idCustomer);
            if (Validate::isLoadedObject($objCustomer)) {
                $linkCustomer = $customerName.' (<a href="'.$this->context->link->getAdminLink('AdminCustomers').
                '&id_customer='.$idCustomer.'&viewcustomer">';
                $linkCustomer .= '#'.$idCustomer;
                $linkCustomer .= '</a>)';
            }
        }
        return $linkCustomer;
    }

    public function getCartRuleLink($idCartRule)
    {
        $linkCartRules = '';
        if ($idCartRule) {
            if (Validate::isLoadedObject(new CartRule($idCartRule))) {
                $linkCartRules = '<a target="blank" href="'.$this->context->link->getAdminLink('AdminCartRules').
                '&id_cart_rule='.$idCartRule.'&updatecart_rule">';
                $linkCartRules .= '#'.$idCartRule;
                $linkCartRules .= '</a>';
            }
        }
        return $linkCartRules;
    }

    /**
     * RenderList function
     *
     * @return void
     */
    public function renderView()
    {
        $id = Tools::getValue('id_rating_voucher');
        $objRewardVoucher = new WkRatingVoucher($id);
        if ($idCartRule = $objRewardVoucher->id_cart_rule) {
            Tools::redirectAdmin(
                $this->context->link->getAdminLink('AdminCartRules').'&id_cart_rule='.$idCartRule.'&updatecart_rule'
            );
        }
    }

    /**
     * RenderList function
     *
     * @return void
     */
    public function renderList()
    {
        $this->addRowAction('view');

        return parent::renderList();
    }

    /**
     * InitPageHeaderToolbar function for back office icon
     *
     * @return void
     */
    public function initPageHeaderToolbar()
    {
        unset($this->toolbar_btn['new']);

        parent::initPageHeaderToolbar();
    }
}
