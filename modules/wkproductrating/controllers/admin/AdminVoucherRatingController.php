<?php
/**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

class AdminVoucherRatingController extends ModuleAdminController
{
    /**
     * Constructer
     */
    public function __construct()
    {
        $this->bootstrap = true;
        $this->lang = false;
        $this->table = 'wk_product_rating_voucher';
        $this->className = 'WkRatingVoucher';
        $this->context = Context::getContext();
        $this->identifier = 'id_rating_voucher';
        parent::__construct();
        $this->_select = ' pl.name as `name`,
                        CONCAT(cs.firstname, cs.lastname) as customerName';
        $this->_join .= 'LEFT JOIN '._DB_PREFIX_.'customer cs ON (cs.id_customer = a.id_customer)
                        LEFT JOIN '._DB_PREFIX_.'product pr ON (pr.id_product = a.id_product)
                        LEFT JOIN '._DB_PREFIX_.'product_lang pl ON (pl.id_product = pr.id_product)';

        $this->_where = ' AND pl. id_lang = '.(int) $this->context->language->id;
        // $this->processResetFilters();

        $this->fields_list = array(
            'id_rating_voucher' => array(
                'title' => $this->l('ID'),
                'align' => 'center',
                'class' => 'fixed-width-xs',
            ),
            'id_order' => array(
                'title' => $this->l('Order Id'),
                'align' => 'center',
                'filter_key' => 'a!id_order',
                'havingFilter' => true,
            ),
            'name' => array(
                'title' => $this->l('Product Name'),
                'align' => 'center',
                'filter_key' => 'pl!name',
                'having_filter' => true
            ),
            'customerName' => array(
                'title' => $this->l('Customer Name'),
                'align' => 'center',
                'having_filter' => true,
                'filter_key' => 'cs!firstname'
            ),
            'code' => array(
                'title' => $this->l('code'),
                'align' => 'center',
            ),
            'is_generated' => array(
                'title' => $this->l('Genrated'),
                'active' => 'status',
                'class' => 'fixed-width-xs',
                'align' => 'center',
                'orderby' => false
            ),
            'state' => array(
                'title' => $this->l('State'),
                'align' => 'center',
                'callback' => 'getRatingVoucherState',
                'class' => 'fixed-width-sm',
                'search' => false,
            ),
            'date_upd' => array(
                'title' => $this->l('Date'),
                'type' => 'datetime',
                'class' => 'fixed-width-xs',
                'align' => 'center',
                'filter_key' => 'a!date_upd'
            ),
        );
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'icon' => 'icon-trash',
                'confirm' => $this->l('Delete selected items?')
            )
        );
    }

    public function getRatingVoucherState($echo, $row)
    {
        if (isset($echo)) {
            $eventId = $row['state'];
            if ($eventId == 0) {
                $response = '<span class="badge badge-watting">'.$this->l('Pending').'</span>';
            } elseif ($eventId == 1) {
                $response = '<span class="badge badge-success">'.$this->l('Approved').'</span>';
            } elseif ($eventId == 2) {
                $response = '<span class="badge badge-danger">'.$this->l('Cancle').'</span>';
            }

            return $response;
        }
    }

    /**
     * RenderForm function
     *
     * @return void
     */
    public function renderForm()
    {
        $id = Tools::getValue('id_rating_voucher');
        if ($id) {
            $options = array(
                array(
                    'id' => 0,
                    'name' => $this->l('Pending')
                ),
                array(
                    'id' => 1,
                    'name' => $this->l('Approve')
                ),
                array(
                    'id' => 2,
                    'name' => $this->l('Cancle')
                )
            );
            $voucherRating = new WkRatingVoucher($id);
            $this->fields_value = array(
                'WK_REVIEW_STATUS' => $voucherRating->state,
            );
            $this->fields_form = array(
                'legend' => array(
                    'title' => $this->l('VOUCHER APPROVAL'),
                    'icon' => 'icon-cogs',
                    ),
                    'input' => array(
                        array(
                            'type' => 'select',
                            'lang' => true,
                            'label' => $this->l('Change Voucher Request'),
                            'name' => 'WK_REVIEW_STATUS',
                            'desc' => $this->l('Select Voucher Status'),
                            'options' => array(
                                'query' => $options,
                                'id' => 'id',
                                'name' => 'name'
                            ),
                        ),
                    ),
                    'submit' => array(
                        'title' => $this->l('Save'),
                        'name' => 'btnSubmit_approvalconfig',
                    ),
            );
        }

        return parent::renderForm();
    }

    /**
     * ProcessSave function
     *
     * @return void
     */
    public function processSave()
    {
        $id = Tools::getValue('id_rating_voucher');
        $state = Tools::getValue('WK_REVIEW_STATUS');

        if ($id) {
            $objRatingVoucher = new WkRatingVoucher($id);
            $objRatingVoucher->state = $state;
            if ($objRatingVoucher->is_generated == 0) {
                if ($state == 1) {
                    if ($idCartRule = $objRatingVoucher->genrateRatingVoucherForCustomer(
                        $objRatingVoucher->id_customer,
                        $objRatingVoucher->code
                    )
                    ) {
                        $objRatingVoucher->is_generated = 1;
                        $objRatingVoucher->id_cart_rule = $idCartRule;
                    }
                }
            }

            $objRatingVoucher->save();
            Tools::redirectAdmin(self::$currentIndex.'&conf=4&token='.$this->token);
        }

        parent::postProcess();
    }

    /**
     * RenderList function
     *
     * @return void
     */
    public function renderList()
    {
        $this->addRowAction('edit');
        $this->addRowAction('delete');

        return parent::renderList();
    }

    /**
     * InitPageHeaderToolbar function for hide back office icon
     *
     * @return void
     */
    public function initPageHeaderToolbar()
    {
        unset($this->toolbar_btn['new']);

        parent::initPageHeaderToolbar();
    }

    public function processDelete()
    {
        parent::processDelete();

        if (Validate::isLoadedObject($object = $this->loadObject())) {
            if ($idRatingVoucher = $object->id) {
                $objRewardVoucher = new WkRatingVoucher($idRatingVoucher);
                if ($idCartRule = $objRewardVoucher->id_cart_rule) {
                    $cartRule = new CartRule($idCartRule);
                    if (!$cartRule->delete()) {
                        $this->errors[] = $this->l('Some error occurred while deleting cart rule. Please try again.');
                    }
                } else {
                    $this->errors[] = $this->l('Cart rule not found. Please try again.');
                }
                if (!count($this->errors)) {
                    Tools::redirectAdmin(self::$currentIndex.'&conf=1&token='.$this->token);
                }
            }
        }
    }
}
