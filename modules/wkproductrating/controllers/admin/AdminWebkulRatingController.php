<?php
/**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

class AdminWebkulRatingController extends ModuleAdminController
{
    /**
     * Constructer
     */
    public function __construct()
    {
        $this->bootstrap = true;
        $this->lang = false;
        $this->table = 'wk_product_rating';
        $this->className = 'WkRating';
        $this->context = Context::getContext();
        $this->identifier = 'id_product_rating';
        parent::__construct();
        $this->_select = ' pl.name as `name`,
                        CONCAT(cs.firstname, cs.lastname) as customerName';
        $this->_join .= 'LEFT JOIN '._DB_PREFIX_.'customer cs ON (cs.id_customer = a.id_customer)
                        LEFT JOIN '._DB_PREFIX_.'product pr ON (pr.id_product = a.id_product)
                        LEFT JOIN '._DB_PREFIX_.'product_lang pl ON (pl.id_product = pr.id_product)';

        $this->_where = ' AND pl. id_lang = '.(int) $this->context->language->id;
        $this->fields_list = array(
            'id_product_rating' => array(
                'title' => $this->l('ID'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'id_order' => array(
                'title' => $this->l('Order Id'),
                'align' => 'center',
            ),
            'id_product' => array(
                'title' => $this->l('Product Id'),
                'align' => 'center',
                'filter_key' => 'a!id_product'
            ),
            'name' => array(
                'title' => $this->l('Product Name'),
                'align' => 'center',
                'filter_key' => 'pl!name',
                'having_filter' => true
            ),
            'id_customer' => array(
                'title' => $this->l('Customer Id'),
                'filter_key' => 'a!id_customer',
                'align' => 'center',
            ),
            'customerName' => array(
                'title' => $this->l('Customer Name'),
                'align' => 'center',
                'having_filter' => true,
                'filter_key' => 'cs!firstname'
            ),
            'rating' => array(
                'title' => $this->l('Rating'),
                'align' => 'center',
                'callback' => 'showRatingStar'
            ),
            'title' => array(
                'title' => $this->l('Title'),
                'align' => 'center',
            ),
            'active' => array(
                'title' => $this->l('Status'),
                'active' => 'status',
                'type' => 'bool',
                'class' => 'fixed-width-xs',
                'align' => 'center',
                'orderby' => false
            ),
            'date_add' => array(
                'title' => $this->l('Date'),
                'type' => 'datetime',
                'class' => 'fixed-width-xs',
                'align' => 'center',
                'filter_key' => 'a!date_add'
            ),
        );
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'icon' => 'icon-trash',
                'confirm' => $this->l('Delete selected items?')
            )
        );
    }

    /**
     * ShowRatingStar function is for showing star images
     *
     * @param  INT $val value
     *
     */
    public function showRatingStar($val)
    {
        $this->context->smarty->assign(
            array(
                'rating' => $val,
                'rating_start_path' => _MODULE_DIR_.$this->module->name.
                '/libs/rateit/lib/img'
            )
        );

        return $this->context->smarty->fetch(
            _PS_MODULE_DIR_.
            'wkproductrating/views/templates/admin/product_rating.tpl'
        );
    }

    /**
     * RenderList function
     *
     * @return void
     */
    public function renderList()
    {
        $this->addRowAction('edit');
        $this->addRowAction('view');
        $this->addRowAction('delete');

        return parent::renderList();
    }

    /**
     * InitPageHeaderToolbar function for back office icon
     *
     * @return void
     */
    public function initPageHeaderToolbar()
    {
        if (empty($this->display)) {
            $this->page_header_toolbar_btn['new_category'] = array(
                'href' => self::$currentIndex.'&add'.$this->table.'&token='.$this->token,
                'desc' => $this->l('Add new review', null, null, false),
                'icon' => 'process-icon-new'
            );
        }

        parent::initPageHeaderToolbar();
    }

    /**
     * RenderView function
     *
     * @return void
     */
    public function renderView()
    {
        $id = Tools::getValue('id_product_rating');
        $objView = new WkRating();
        $viewComment = $objView->getAllDetailsOfRatingById($id);
        $this->context->smarty->assign('viewComment', $viewComment);

        return parent::renderView();
    }

    /**
     * RenderForm function
     *
     * @return void
     */
    public function renderForm()
    {
        if (!($this->loadObject(true))) {
            return;
        }
        $this->fields_form = array(
            'submit' => array(
                'title' => $this->l('Save'),
            )
        );

        $ratingObj = new WkRating();
        $orderDetails =  $ratingObj->getAllOrderId();
        $this->context->smarty->assign('order', $orderDetails);
        $id = Tools::getValue('id_product_rating');
        if ($id) {
            $ratingDetails = $ratingObj->getAllDetailsOfRatingById($id);
            if ($ratingDetails) {
                $this->context->smarty->assign('ratingDetails', $ratingDetails);
            }
        }
        $this->context->smarty->assign('loadingImage', _MODULE_DIR_.'wkproductrating/views/img/loading-small.gif');
        $this->context->smarty->assign('commentValid', Configuration::get('WK_PRODUCT_RATING_COMMENT'));

        return parent::renderForm();
    }

    /**
     * SetMedia function for set JS and Css
     *
     * @return void
     */
    public function setMedia()
    {
        parent::setMedia();

        $this->context->controller->addJS(_MODULE_DIR_.'wkproductrating/libs/rateit/lib/jquery.raty.min.js');
        $this->context->controller->addJS(_MODULE_DIR_.'wkproductrating/views/js/adminForm.js');
        $this->context->controller->addCss(_MODULE_DIR_.'wkproductrating/views/css/product_rating.css');

        Media::addJsDef(
            array(
                'controler' =>  $this->context->link->getAdminLink('AdminWebkulRating'),
                'moduledir' => _MODULE_DIR_.'wkproductrating/libs/rateit/lib/img',
                'selectProduct' => $this->l('Select Product')
            )
        );
    }

    /**
     * ProcessSave function
     *
     * @return void
     */
    public function processSave()
    {
        $idProductRating = Tools::getValue('id_product_rating');
        $idOrder = Tools::getValue('id_order');
        $objCustomer = new WkRating();
        $customer = $objCustomer->getCustomerIdByOrderId($idOrder);
        $idCustomer = $customer['id_customer'];
        $idProduct = Tools::getValue('id_product');
        $title = Tools::getValue('title');
        $rating = Tools::getValue('rating');
        $comment = Tools::getValue('comment');
        $active = Tools::getValue('active');

        if (!($idOrder)) {
            $this->errors[] = $this->l("Order id should not be empty.");
        }
        if (!($idCustomer)) {
            $this->errors[] = $this->l("Customer id should not be empty.");
        }
        if (!($idProduct)) {
            $this->errors[] = $this->l("Product should not be empty.");
        }
        if (!($title)) {
            $this->errors[] = $this->l("Title should not be empty.");
        }
        if (!($rating)) {
            $this->errors[] = $this->l("Rating should not be empty.");
        } elseif (!Validate::isInt($rating)) {
            $this->errors[] = $this->l("Invalid Rating.");
        }
        if (!($comment)) {
            $this->errors[] = $this->l("Comment should not be empty.");
        }

        if (empty($this->errors)) {
            if ($idProductRating) {
                $objRating = new WkRating($idProductRating);
            } else {
                $objRating = new WkRating();
                $objRating->id_customer = $idCustomer;
                $objRating->id_order = $idOrder;
                $objRating->id_product = $idProduct;
                $objRating->active = $active;
            }
            $objRating->title = $title;
            $objRating->rating = $rating;
            $objRating->comment = $comment;
            $objRating->save();

            if ($idProductRating) {
                Tools::redirectAdmin(
                    $this->context->link->getAdminLink('AdminWebkulRating').'&conf=4'
                );
            } else {
                Tools::redirectAdmin(
                    $this->context->link->getAdminLink('AdminWebkulRating').'&conf=3'
                );
            }
        } else {
            if ($idProductRating) {
                $this->display = 'edit';
            } else {
                $this->display = 'add';
            }

            return $this->errors;
        }
    }

    /**
     * Ajax for geting all Products Name of the selected ID Order
     *
     * @return array          all product names of that order
     */
    public function ajaxProcessProductChose()
    {
        $idOrder = Tools::getValue('id_order');

        if ($idOrder) {
            $objProduct = new WkRating();
            $productNames = $objProduct->getProductNameByOrderId($idOrder);
            if ($productNames) {
                echo json_encode($productNames);
            } else {
                echo json_encode(1);
            }
        }
    }
}
