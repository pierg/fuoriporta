<?php
/**
 * 2010-2018 Webkul.
 *
 * NOTICE OF LICENSE
 *
 * All right is reserved,
 * Please go through this link for complete license : https://store.webkul.com/license.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this module to newer
 * versions in the future. If you wish to customize this module for your
 * needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
 *
 *  @author    Webkul IN <support@webkul.com>
 *  @copyright 2010-2018 Webkul IN
 *  @license   https://store.webkul.com/license.html
 */

class WkProductRatingMyProductRatingsModuleFrontController extends ModuleFrontController
{
    /**
     * [initContent ]
     * @return [type] [Display Customer Bidding Details in Auction Bid Details page in My Account]
     */
    public function initContent()
    {
        parent::initContent();

        if ($idCustomer = $this->context->customer->id) {
            $objRating = new WkRating();
            $customerOrder = Order::getCustomerOrders($idCustomer);
            $customerRatingDetails = array();
            $idLang = $this->context->language->id;
            if ($customerOrder) {
                foreach ($customerOrder as $keyF => $order) {
                    if ($order['current_state'] == Configuration::get('WK_REVIEW_STATUS')) {
                        $ratingDetails = $objRating->getOrderDetailsByIdOrder($order['id_order']);
                        if ($ratingDetails) {
                            foreach ($ratingDetails as $key => $ratings) {
                                $customerRatingDetails[$keyF][$key] = $ratings;
                                $objProduct = new Product($idProduct = $ratings['id_product']);
                                $customerRatingDetails[$keyF][$key]['product_obj'] = $objProduct;
                                $customerRatingDetails[$keyF][$key]['id_order'] = $order['id_order'];
                                $customerRatingDetails[$keyF][$key]['product_name'] = $objProduct->name[$idLang];
                                $customerRatingDetails[$keyF][$key]['id_image'] =
                                Product::getCover($idProduct)['id_image'];
                                $customerRatingDetails[$keyF][$key]['avj_rating'] =
                                $objRating->getAverageRatingByProductId(
                                    $idProduct
                                )['averageRating'];
                                $customerRatingDetails[$keyF][$key]['price_static'] =
                                Tools::displayPrice($objProduct->getPriceStatic($ratings['id_product']));
                                $customerRatingDetails[$keyF][$key]['rating_available'] =
                                $objRating->getRatingByIdProductAndIdCustomer($idProduct, $idCustomer);
                            }
                        }
                    }
                }
            }

            $this->context->smarty->assign(
                array(
                    'link' => $this->context->link,
                    'customerRatingDetails' => $customerRatingDetails,
                    'multipleComment' => Configuration::get('WK_PRODUCT_RATING_MULTIPLE_RATINGS'),
                    'limit' => Configuration::get('WK_PRODUCT_RATING_COMMENT_LIMIT'),
                    'commentValid' => Configuration::get('WK_PRODUCT_RATING_COMMENT'),
                    'limitValue' => Configuration::get('WK_PRODUCT_RATING_SET_LIMIT'),
                    'canEditRating' => Configuration::get('WK_PRODUCT_RATING_RATINGS'),
                    'canDeleteRating' => Configuration::get('WK_PRODUCT_RATING_COMMENT_DELETE'),
                    'loadingImage' => _MODULE_DIR_.'wkproductrating/views/img/loading-small.gif',
                )
            );

            $this->setTemplate('customerratingaccount.tpl');
        } else {
            Tools::redirect($this->context->link->getPageLink('authentication'));
        }
    }

        /**
     * GetBreadcrumbLinks function for geting all links need in Bread Crumb
     *
     * @return void
     */
    public function getBreadcrumbLinks()
    {
        $objProduct = new Product();
        $idProduct = Tools::getValue('id_product');
        $categoryDetails = $objProduct->getProductCategoriesFull($idProduct);

        $breadcrumb = parent::getBreadcrumbLinks();
        $i = 1;
        foreach ($categoryDetails as $category) {
            if ($i != 1) {
                $breadcrumb['links'][] = array(
                    'title' => $this->module->l($category['name']),
                    'url' => $this->context->link->getCategoryLink($category['id_category']),
                );
            }
            $i++;
        }

        return $breadcrumb;
    }

    public function setMedia()
    {
        parent::setMedia();

        Media::addJsDef(
            array('WK_PRODUCT_RATING_COMMENT' => Configuration::get('WK_PRODUCT_RATING_COMMENT'),
                'moduledir' => _MODULE_DIR_.'wkproductrating/libs/rateit/lib/img',
                'orderUpdateAjaxLink' => $this->context->link->getModuleLink('wkproductrating', 'commentajax'),
                'isLimitedComment' => Configuration::get('WK_PRODUCT_RATING_COMMENT_LIMIT'),
                'commentLimit' => Configuration::get('WK_PRODUCT_RATING_SET_LIMIT'),
                'idCustomer' => $this->context->customer->id
            )
        );
        Media::addJsDefL('updateMsg', $this->module->l('Your Messege is successfully update.'));
        Media::addJsDefL('saveMsg', $this->module->l('Your Messege is successfully saved.'));
        Media::addJsDefL('errorMsg', $this->module->l('There are something wrong.'));
        Media::addJsDefL('titleError', $this->module->l('Title should not be empty.'));
        Media::addJsDefL('ratingFormatError', $this->module->l('Invalid Rating.'));
        Media::addJsDefL('ratingBlankError', $this->module->l('Rating should not be empty.'));
        Media::addJsDefL('commentError', $this->module->l('Comment should not be empty.'));
        Media::addJsDefL('commentLimitError', $this->module->l('Comment Limit exceeded.'));
        Media::addJsDefL('deleteMsg', $this->module->l('Do you want to delete your review.'));

        $this->context->controller->addCss(_MODULE_DIR_.'wkproductrating/views/css/customer_ratings.css');
        $this->context->controller->addJS(_MODULE_DIR_.'wkproductrating/views/js/orderrating.js');
        $this->context->controller->addJS(_MODULE_DIR_.'wkproductrating/libs/rateit/lib/jquery.raty.min.js');
    }
}
