<?php
/**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

class WkProductRatingAllCommentsModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();

        $objRating = new WkRating();
        $idProduct = Tools::getValue('id_product');
        if ($idProduct) {
            $objProduct = new Product($idProduct);
            $page = Tools::getValue('p');
            if (!$page) {
                $page = 1;
            }
            $totalReviewsCount = $objRating->getTotalCommentCountByIdProduct($idProduct);
            if ($totalReviewsCount) {
                $categoryDetails = array();
                $commentLimit = Configuration::get('PS_PRODUCTS_PER_PAGE');
                //comment show on paginatin (per page)

                $startLimit = ($page-1) * $commentLimit;
                //starting comment of a page on pagination

                $this->pagination($totalReviewsCount);
                $idImage = $objProduct->getCover($idProduct);
                //Identifire of Image for image link

                $rootCategory = Category::getRootCategory($this->context->language->id);
                $homeCategoryId = $rootCategory->id_category;
                
                $avgRating = $objRating->getAverageRatingByProductId($idProduct);
                //average Rating

                $categoryDetails = $objProduct->getProductCategoriesFull($idProduct);
                //category Details for Breadcrumb

                $comments = $objRating->
                getRatingDetailsByIdProduct($idProduct, $startLimit, $commentLimit);
                //comments of a page on each pagination

                $this->context->smarty->assign(
                    array(
                        'categoryDetails' => $categoryDetails,
                        'homeCategoryId' => $homeCategoryId,
                        'comments' => $comments,
                        'idImage' => $idImage['id_image'],
                        'idProduct' => $idProduct,
                        'productName' => $objProduct->getProductName($idProduct),
                        'productPrice' => $objProduct->getPriceStatic($idProduct),
                        'avgRating' => $avgRating
                    )
                );

                $this->setTemplate('allcomments.tpl');
            } else {
                Tools::redirect($this->context->link->getProductLink($objProduct));
            }
        } else {
            Tools::redirect($this->context->link->getPageLink('index'));
        }
    }

    /**
     * Sets the media.
     */
    public function setMedia()
    {
        parent::setMedia();
        $this->context->controller->addCss(_MODULE_DIR_.'wkproductrating/views/css/allcomments.css');
    }
}
