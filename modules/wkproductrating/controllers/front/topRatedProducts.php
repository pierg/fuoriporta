<?php
/**
 * 2010-2018 Webkul.
 *
 * NOTICE OF LICENSE
 *
 * All right is reserved,
 * Please go through this link for complete license : https://store.webkul.com/license.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this module to newer
 * versions in the future. If you wish to customize this module for your
 * needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
 *
 *  @author    Webkul IN <support@webkul.com>
 *  @copyright 2010-2018 Webkul IN
 *  @license   https://store.webkul.com/license.html
 */

class WkProductRatingTopRatedProductsModuleFrontController extends ModuleFrontController
{
    public function init()
    {
        parent::init();
        $idLang = $this->context->language->id;
        $objRating = new WkRating();
        $ratedProducts = $objRating->getAllReatedProduct();
        $productDetails = array();
        if ($ratedProducts) {
            foreach ($ratedProducts as $key => $topRate) {
                $objProduct = new Product($topRate['id_product']);
                $productDetails[$key]['id_product'] = $topRate['id_product'];
                $productDetails[$key]['name'] = $objProduct->name[$idLang];
                $productDetails[$key]['id_image'] = Product::getCover($topRate['id_product'])['id_image'];
                $productDetails[$key]['avj_rating'] = $objRating->getAverageRatingByProductId(
                    $topRate['id_product']
                )['averageRating'];
                $productDetails[$key]['price_static'] = Tools::displayPrice(
                    $objProduct->getPriceStatic($topRate['id_product'])
                );
            }
        }
        $this->context->smarty->assign('objProduct', $objProduct);
        $this->context->smarty->assign('productDetails', $productDetails);

        $this->setTemplate('topratedproducts.tpl');
    }

    public function setMedia()
    {
        parent::setMedia();

        $this->context->controller->addCss(_MODULE_DIR_.'wkproductrating/views/css/top_rated.css');
    }
}
