<?php
/**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

class WkProductRatingCommentAjaxModuleFrontController extends ModuleFrontController
{
    public function init()
    {
        $obj_emp = new Employee(1);
        $this->adminEmail = $obj_emp->email;
        $this->idLang = $this->context->language->id;
        $this->customerName = $this->context->customer->firstname.
        ' '.$this->context->customer->lastname;


        parent::init();
    }

    public function initContent()
    {
        parent::initContent();

        $id = Tools::getValue('id_product_rating');
        $task = Tools::getValue('task');

        if ($task == 'edit' || $task == 'save') {
            $id_customer = Tools::getValue('id_customer');
            $id_product = Tools::getValue('id_product');
            $id_order = Tools::getValue('id_order');
            $rating = Tools::getValue('rating');
            $comment = Tools::getValue('comment');
            $title = Tools::getValue('title');
            $status = Configuration::get('WK_PRODUCT_RATING_STATUS');
            $active = 0;
            if ($status == 1) {
                $active = 0;
            } else {
                $active = 1;
            }
            if (!$rating || !$title) {
                die('0');
            }
            if (Configuration::get('WK_PRODUCT_RATING_COMMENT_LIMIT')) {
                $limits = Configuration::get('WK_PRODUCT_RATING_SET_LIMIT');
                if (count($comment) > $limits) {
                    die('0');
                }
            }
            if ($task == 'edit') {
                $ratingVar = new WkRating($id);
                $ratingVar->rating = $rating;
                $ratingVar->title = $title;
                $ratingVar->active = $active;
                $ratingVar->comment = $comment;
                $ratingVar->update();
                die('1');
            } elseif ($task == 'save') {
                $ratingVar = new WkRating();
                $ratingVar->id_customer = $id_customer;
                $ratingVar->id_order = $id_order;
                $ratingVar->id_product = $id_product;
                $ratingVar->active = $active;
                $ratingVar->rating = $rating;
                $ratingVar->title = $title;
                $ratingVar->comment = $comment;
                $ratingVar->save();
                // Admin Mail
                if (Configuration::get('WK_PRODUCT_RATING_ADMIN_NOTI')) {
                    $objProduct = new Product($id_product);
                    $objCustomer = new Customer($id_customer);
                    $customerName = $objCustomer->firstname.' '.$objCustomer->lastname;
                    $mailParams = array(
                        '{customer_name}' => $customerName,
                        '{product_name}' => $objProduct->name
                    );
                    Mail::Send(
                        $this->idLang,
                        'rating_notification_admin', //Specify the template file name
                        Mail::l('One Customer gives Review on ', $this->idLang)
                        .$objProduct->name[1].' ['. $customerName.'] ',
                        //Mail subject with translation
                        $mailParams,
                        $this->adminEmail,
                        null,
                        null,
                        null,
                        null,
                        null,
                        _PS_MODULE_DIR_.'wkproductrating/mails/',
                        false,
                        null,
                        null
                    );
                }
                // Customer Request For voucher
                if (Configuration::get('WK_PRODUCT_RATING_GIVE_VOUCHER')) {
                    $objRatingHis = new WkRatingHistory();
                    $ratingHis = $objRatingHis->getRatingHistoryByOrderId($id_order);
                    if ($ratingHis) {
                        $objRatingHis = new WkRatingHistory($ratingHis['id_rating_history']);
                        $objRatingHis->is_rate = 1;
                        $objRatingHis->update();
                        if (Configuration::get('WK_PRODUCT_RATING_VOUCHER_TIMEOUT')) {
                            if ($objRatingHis->isGiveVoucherDateExpireByOrderId($id_order)) {
                                die('1');
                            }
                        }
                        if (Configuration::get('WK_RATING_VOUCHER_APPROVE')) {
                            $state = 1;
                        } else {
                            $state = 0;
                        }
                        $this->addRatingVoucherRequest(
                            $id_order,
                            $id_customer,
                            $id_product,
                            $state
                        );
                    }
                }
                die('1');
            }
        } elseif ($task == 'delete' && $id) {
            $ratingVar = new WkRating($id);
            die($ratingVar->delete());
        } elseif ($task == 'view' && $id) {
            $objRating = new WkRating();
            $valueRating = $objRating->getAllDetailsOfRatingById($id);
            if ($valueRating) {
                die(Tools::jsonEncode($valueRating));
            }
        }
    }

    /**
     * AddRatingVoucherRequest function
     *
     * @return void
     */
    public function addRatingVoucherRequest($idOrder, $idCustomer, $idProduct, $state)
    {
        $objVoucherRating = new WkRatingVoucher();
        if (!$objVoucherRating->voucherRequestAlreadyGenerated($idOrder, $idProduct)) {
            $objVoucherRating->id_order = $idOrder;
            $objVoucherRating->id_customer = $idCustomer;
            $objVoucherRating->id_product = $idProduct;
            $objVoucherRating->code = $this->generateRandomCode();
            $objVoucherRating->state = $state;
            $objVoucherRating->is_generated = 0;
            $objVoucherRating->active = 1;
            $objVoucherRating->save();
            if ($objVoucherRating->id) {
                //Admin voucher update Mail
                if (!Configuration::get('WK_RATING_VOUCHER_APPROVE')) {
                    $this->getEmailForVoucherRequest();
                }
                if ($objVoucherRating->state == 1) {
                    $objVoucherRating->genrateRatingVoucherForCustomer(
                        $idCustomer,
                        $objVoucherRating->code
                    );
                    $objVoucherRating->is_generated = 1;
                    $objVoucherRating->update();
                }

                return true;
            }
        }
    }

    /**
     * Generate Random Code of length 8
     *
     * @param  integer $length length of the code
     *
     * @return void uniqe code
     */
    public function generateRandomCode($length = 8)
    {
        $objRatingVoucher = new WkRatingVoucher();
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $rand = '';
        for ($i = 0; $i < $length; ++$i) {
            $rand = $rand.$characters[mt_rand(0, Tools::strlen($characters) - 1)];
        }
        $rand = Tools::strtoupper(Configuration::get('WK_RATING_VOUCHER_PREFIX')).$rand;
        if ($objRatingVoucher->isVoucherRequestByCode($rand)
        ) {
            $this->generateRandomCode();
        } else {
            return $rand;
        }
    }


    /**
     * Undocumented function
     *
     * @return void
     */
    public function getEmailForVoucherRequest()
    {
        //mail for can review on this Product
        $mailParams = array(
            '{customer_name}' => $this->customerName,
        );
        Mail::Send(
            $this->idLang,
            'voucher_request_admin', //Specify the template file name
            Mail::l('One Request for Voucher Genrate for Rating a Product', $this->idLang),
            //Mail subject with translation
            $mailParams,
            $this->adminEmail,
            null,
            null,
            null,
            null,
            null,
            _PS_MODULE_DIR_.'wkproductrating/mails/',
            false,
            null,
            null
        );
    }
}
