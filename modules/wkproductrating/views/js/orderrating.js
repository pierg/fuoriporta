/**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

$(document).ready(function(){
    $('#writereview').fancybox({
        width: 600,
        height: 440,
        autoSize : false,
        maxWidth : '100%',
        'hideOnContentClick': false
    });
    $(document).on('click','#writereview',function(){
        $('#addLoading').hide();
        var id_product = $(this).attr('data-id-product');
        var id_order = $(this).attr('data-id-order');
        $("#idProduct").val(id_product);
        $("#idOrder").val(id_order);
    });
    $(document).on('click', '#btnSubmit', function(){
        var title = $("input[name='title']").val();
        var rating = $("input[name='rating_image']").val();
        var comment = $("textarea[name='comment']").val();

        if (rating == "") {
            $('#ratingBlankError').html("<span style='color: red'>"+ratingBlankError+"</span>")
            .fadeIn()
            .delay(3000)
            .fadeOut();

            return false;
        } else if (rating < 0 || rating > 5) {
            $('#ratingBlankError').html("<span style='color: red'>"+ratingFormatError+"</span>")
            .fadeIn()
            .delay(3000)
            .fadeOut();

            return false;
        }

        if (title == "") {
            $('#titleError').html("<span style='color: red'>"+titleError+"</span>")
            .fadeIn()
            .delay(3000)
            .fadeOut();

            return false;
        }

        if (WK_PRODUCT_RATING_COMMENT == 1) {
            if (comment == "") {
            $('#commentError').html("<span style='color: red'>"+commentError+"</span>")
                .fadeIn()
                .delay(3000)
                .fadeOut();

                return false;
            }
        }
        var idOrder = $('#idOrder').val();
        var idProduct = $('#idProduct').val();
        $('#btnSubmit').attr("disabled", true);
        $('#addLoading').show();
        $.ajax({
            type:'post',
            url: orderUpdateAjaxLink,
            data: {'id_customer' : idCustomer,
                'id_product' : idProduct,
                'id_order' : idOrder,
                'title' : title,
                'rating' : rating,
                'comment' : comment,
                'task' : 'save'
            },
            success:function(results){
                $('#addLoading').hide();
                if (1 == parseInt(results)) {
                    $('#ratingMsg')
                    .html("<div class='alert alert-success alert-dismissable'>"+saveMsg+"</div>")
                    .fadeIn()
                    .delay(3000)
                    .fadeOut();
                    setTimeout(location.reload.bind(location), 4000);
                } else {
                    $('#ratingMsg')
                    .html("<div class='alert alert-danger alert-dismissable'>"+errorMsg+"</div>");
                    setTimeout(location.reload.bind(location), 4000);
                }
            }
        })
    });
   $('#editReview').fancybox({
        width: 600,
        height: 440,
        autoSize : false,
        maxWidth : '100%',
        'hideOnContentClick': false
    });
    $(document).on('click','#editReview',function(){
        var id_product_rating = $(this).attr('data-id-product-rating');
        $("#id_product_rating").val(id_product_rating);
        $.ajax({
            type:'post',
            url: orderUpdateAjaxLink,
            data: {
                'id_product_rating' : id_product_rating,
                'task' : 'view'
            },
            success: function(response){
                var result = $.parseJSON(response);
                $("input[name='ratingedit_image']").val(result.rating);
                $("input[name='titleedit']").val(result.title);
                $("textarea[name='commentedit']").val(result.comment);
	            $('#ratingedit_image').raty({
	                path: moduledir,
	                scoreName: 'ratingedit_image',
	                score: result.rating
	            });
                $('#editLoading').hide();
	        }
        });
    });
    $(document).on('click', '#btnEdtSubmit', function(e){
        var id_product_rating = $('#id_product_rating').val();
        var title = $("input[name='titleedit']").val();
        var rating = $("input[name='ratingedit_image']").val();
        var comment = $("textarea[name='commentedit']").val();
        e.preventDefault();


        if (rating == "") {
            $('#ratingEdtBlankError')
            .html("<span style='color: red'>"+ratingBlankError+"</span>")
            .fadeIn()
            .delay(3000)
            .fadeOut();

            return false;
        } else if (rating < 0 || rating > 5) {
            $('#ratingEdtBlankError')
            .html("<span style='color: red'>"+ratingFormatError+"</span>")
            .fadeIn()
            .delay(3000)
            .fadeOut();

            return false;
        }

        if (title == "") {
            $('#titleEdtError')
            .html("<span style='color: red'>"+titleError+"</span>")
            .fadeIn()
            .delay(3000)
            .fadeOut();

            return false;
        }

        if (WK_PRODUCT_RATING_COMMENT == 1) {
            if (comment == "") {
                $('#commentEdtError')
                .html("<span style='color: red'>"+commentError+"</span>")
                .fadeIn()
                .delay(3000)
                .fadeOut();

                return false;
            }
        }
        $('#btnEdtSubmit').attr("disabled", true);
        $('#editLoading').show();
        $.ajax({
                type:'post',
                url: orderUpdateAjaxLink,
                data: {
                    'id_product_rating' : id_product_rating,
                    'title' : title,
                    'rating' : rating,
                    'comment' : comment,
                    'task' : 'edit'
                },
                success:function(results){
                    $('#editLoading').hide();
                    if (1 == parseInt(results)) {
                        $('#edtmsg')
                        .html("<div class='alert alert-success alert-dismissable'>"+updateMsg+"</div>")
                        .fadeIn()
                        .delay(3000)
                        .fadeOut();
                       setTimeout(location.reload.bind(location), 4000);
                    } else {
                        $('#edtmsg')
                        .html("<div class='alert alert-danger alert-dismissable'>"+errorMsg+"</div>");
                        setTimeout(location.reload.bind(location), 4000);
                    }
                }
            })
    });
   $('#view').fancybox({
        width: 550,
        height: 280,
        autoSize : false,
        maxWidth : '100%',
        'hideOnContentClick': false
    });
   $(document).on('click','#view',function(){
        var id_product_rating = $(this).attr('data-id-product-rating');
        $("#id_product_rating").val(id_product_rating);
        imagePath = moduledir+'/star-on.png';
        var id = 'ratingedit_image';
        $.ajax({
            type:'post',
            url: orderUpdateAjaxLink,
            data: {
                'task' : 'view',
                'id_product_rating' : id_product_rating},
            success: function(response){
                var result = $.parseJSON(response);
                $("#titleView").html(result.title);
                var html='';
                for (var i = result.rating - 1; i >= 0; i--) {
                    html += '<img src='+moduledir+'/star-on.png />'
                }
                for (var i = 4 - result.rating; i >= 0; i--) {
                    html += '<img src='+moduledir+'/star-off.png />'
                }
                $("#ratingView").html(html);
                $("#commentView").html(result.comment);
                $('#data').show();
                $('#viewLoading').hide();
            }
        });
    });
   $(document).on('click','#deleteReview',function(){
        var id_product_rating = $(this).attr('data-id-product-rating');
        var del = 1;
        var txt;
        var r = confirm(""+deleteMsg+"");
        if (r == true) {
            $.ajax({
                type:'post',
                url: orderUpdateAjaxLink,
                data: {
                    'task' : 'delete',
                    'id_product_rating' : id_product_rating,
                    'del' : del
                },
                success: function(response){
                    location.reload();
                }
            });
        } else {
            return;
        }
    });
});
