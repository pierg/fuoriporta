/**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

$(document).ready(function(){
	if (WK_PRODUCT_RATING_COMMENT_LIMIT == 1) {
			$(".no_of_cmnt").parent().parent().show();
        } else {
			$(".no_of_cmnt").parent().parent().hide();
        }
    $('input[name="WK_PRODUCT_RATING_COMMENT_LIMIT"]').on('change',function(){
        var inputValue = $(this).attr("value");
        if (inputValue == 1) {
			$(".no_of_cmnt").parent().parent().show();
        } else {
			$(".no_of_cmnt").parent().parent().hide();
        }
    });
    var giveVoucher = $('input[name="WK_PRODUCT_RATING_GIVE_VOUCHER"]:checked').val();
    if (giveVoucher == 1) {
        $('.wk_rating_voucher_time').slideDown();
    } else {
        $('.wk_rating_voucher_time').slideUp();
    }
    $('input[name="WK_PRODUCT_RATING_GIVE_VOUCHER"]').on('change',function(){
        thisGiveVoucher = $(this).val();
        if (thisGiveVoucher == 1) {
            $('.wk_rating_voucher_time').slideDown();
        } else {
            $('.wk_rating_voucher_time').slideUp();
        }
    })
    $('input[name="WK_RATING_VOUCHER_CATEGORY_RESTRICTION"]').on('change',function(){
        voucherCatRestrictions = $(this).val();
        if (voucherCatRestrictions == 1) {
            $('#wk_withdrawl_category_block_cont').slideDown();
        } else {
            $('#wk_withdrawl_category_block_cont').slideUp();
        }
    })
    var voucherType = $('input[name="WK_RATING_REWARD_VOUCHER_TYPE"]:checked').val();
    if (voucherType == 1) {
        $('.wk_rating_voucher_fixed').slideDown();
    } else {
        $('.wk_rating_voucher_fixed').slideUp();
    }
    $('input[name="WK_RATING_REWARD_VOUCHER_TYPE"]').on('change',function(){
        voucherType = $(this).val();
        if (voucherType == 1) {
            $('.wk_rating_voucher_fixed').slideDown();
        } else {
            $('.wk_rating_voucher_fixed').slideUp();
        }
    })
});