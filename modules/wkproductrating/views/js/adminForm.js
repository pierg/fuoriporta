/**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

$(document).ready(function(){
	$(document).on('change', '#idOrder', function(){
		$('#errorMsg').hide();
		$('#loading').show();
		var order = $('#idOrder').val();
		$.ajax({
			type:"POST",
			url: controler,
			dataType: 'json',
			data:{
				action: 'ProductChose',
				ajax : true,
				'id_order' : order},
			success : function(response){
				$('#loading').hide();
            	if (response == 1) {
            		$('#errorMsg').show();
            	}
				var html = "<option value=''>"+selectProduct+"</option>";
                $.each(response,function(key, value){
					html += '<option value='+value.id_product+'>('+value.id_product+') '+value.name+'</option>';
				});
			$('#id_product').html(html);
			}
		});
	});
});