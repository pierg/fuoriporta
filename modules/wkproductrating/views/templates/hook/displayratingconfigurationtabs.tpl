{*
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*}

<style>
	.wk-configuration {
		margin-bottom:10px;
		padding:0px !important;;
	}
	.wk-configuration li {
		border-right: 1px solid #ccc;
		padding: 4px;
		margin-left:0px !important;
	}
</style>

<ul class="panel nav nav-pills wk-configuration">
	<li role="presentation" {if isset($smarty.get.controller)}{if $smarty.get.controller  == 'AdminVoucherRating'}class="active"{/if}{/if}>
		<a href="{$link->getAdminLink('AdminVoucherRating')|escape:'htmlall':'UTF-8'}">
			<i class="icon icon-money-bill"></i>
			{l s='Rating Voucher Details' mod='wkproductrating'}
		</a>
	</li>
	<li role="presentation" {if isset($smarty.get.controller)}{if $smarty.get.controller  == 'AdminVoucherRatingDetails'}class="active"{/if}{/if}>
		<a href="{$link->getAdminLink('AdminVoucherRatingDetails')|escape:'htmlall':'UTF-8'}">
			<i class="icon icon-money-bill"></i>
			{l s='Voucher History' mod='wkproductrating'}
		</a>
	</li>
</ul>
