{*
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All rights is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*}

<div class="mp_tab_block" style="float: left;width: 100%;">
    <li>
		<a title="{l s='My Reviews' mod='wkproductrating'}" href="{if isset($dashboard_link)}{$dashboard_link|escape:'htmlall':'UTF-8'}{else}{$link->getModuleLink('wkproductrating', 'myproductratings')|escape:'htmlall':'UTF-8'}{/if}">
			<i class="icon-star"></i>
			<span class="link-item">
				{l s='My Reviews' mod='wkproductrating'}
			</span>
		</a>
    </li>
</div>
