{*
**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*
*}

<div id="viewed-products_block_left" class="block">
	<p class="title_block">{l s='Top Rated Products' mod='wkproductrating'}</p>
	<div class="block_content products-block">
		<ul>
			{foreach from=$productDetails item=viewedProduct name=myLoop}
				<li class="clearfix{if $smarty.foreach.myLoop.last} last_item{elseif $smarty.foreach.myLoop.first} first_item{else} item{/if}">
					<a
					class="products-block-image"
					href="{$link->getProductLink($viewedProduct.id_product)|escape:'html':'UTF-8'}"
					title="{l s='More about %s' mod='wkproductrating' sprintf=[$viewedProduct.name|escape:'html':'UTF-8']}" >
						<img
						src="{if isset($viewedProduct.id_image) && $viewedProduct.id_image}{$link->getImageLink($viewedProduct.name, $viewedProduct.id_image, 'small_default')|escape:'htmlall':'UTF-8'}{else}{$img_prod_dir|escape:'htmlall':'UTF-8'}{$lang_iso|escape:'htmlall':'UTF-8'}-default-medium_default.jpg{/if}"
						alt="{$viewedProduct.name|escape:'html':'UTF-8'}" />
					</a>
					<div class="product-content">
						<h5>
							<a class="product-name"
							href="{$link->getProductLink($viewedProduct.id_product)|escape:'html':'UTF-8'}"
							title="{l s='More about %s' mod='wkproductrating' sprintf=[$viewedProduct.name|escape:'html':'UTF-8']}">
								{$viewedProduct.name|truncate:25:'...'|escape:'html':'UTF-8'}
							</a>
						</h5>
                        <div class='product-content'>
                            <div id="showStars">
                            {if isset($viewedProduct.avj_rating)}
                                {assign var=i value=0}
                                {while $i != $viewedProduct.avj_rating}
                                    <img class="starSize" src="{$smarty.const._MODULE_DIR_|escape:'htmlall':'UTF-8'}/wkproductrating/views/img/star-on.png" />
                                    {assign var=i value=$i+1}
                                {/while}
                                    {assign var=k value=0}
                                    {assign var=j value=5-$viewedProduct.avj_rating}
                                {while $k!=$j}
                                    <img class="starSize" src="{$smarty.const._MODULE_DIR_|escape:'htmlall':'UTF-8'}/wkproductrating/views/img/star-off.png" />
                                    {assign var=k value=$k+1}
                                {/while}
                                ({$viewedProduct.avj_rating|escape:'htmlall':'UTF-8'}/5)
                            {/if}
                            </div>
                            <div class='wk_rating_top_price'>{$viewedProduct.price_static|escape:'htmlall':'UTF-8'}</div></a>
                        </div>
					</div>
				</li>
			{/foreach}
		</ul>
	</div>
</div>
