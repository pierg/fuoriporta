{*
**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*
*}

<div>
	<h3 class="page-subheading">YOUR REVIEWS</h3>
</div>
<div>
	<table style="table-layout: fixed;" class="table table-hover productTable">
		<tr>
			<th>{l s='Product Name' mod='wkproductrating'}</th>
			<th>{l s='Review Title' mod='wkproductrating'}</th>
			<th>{l s='Review' mod='wkproductrating'}</th>
			<th>{l s='Posted On' mod='wkproductrating'}</th>
			<th>{l s='Status' mod='wkproductrating'}</th>
			<th>{l s='Action' mod='wkproductrating'}</th>
		</tr>
		{foreach $orderDetails as $order}
			<tr>
				{if $order['id_product_rating'] == ""}
					{if ($order['rating_available'] == 0) || ($order['rating_available'] == 1 && $multipleComment == 1)}
						<td>{$order['name']|escape:'html':'UTF-8'}</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>
							<a class="btn btn-default button button-small" data-id-product="{$order['id_product']|escape:'html':'UTF-8'}" data-id-order="{$id_order|escape:'html':'UTF-8'}" id="writereview" href="#box">
								<span>{l s='Write a Review' mod='wkproductrating'}</span>
							</a>
					{else}
						<td>{$order['name']|escape:'html':'UTF-8'}</td>
						<td>-</td>
						<td>{l s=' You already rate this product see this on Order reference: ' mod='wkproductrating'}{$orderRefrence|escape:'html':'UTF-8'}</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
					{/if}
				{else}
					<td>{$order['name']|escape:'html':'UTF-8'}</td>
					<td>
						<strong>{$order['title']|escape:'html':'UTF-8'}</strong>
					</td>
					<td>
						<div id="showStar">
							{assign var=i value=0}
							{while $i != $order['rating']}
								<img class="starSize" src="{$smarty.const._MODULE_DIR_|escape:'htmlall':'UTF-8'}/wkproductrating/views/img/star-on.png" />
								{assign var=i value=$i+1}
							{/while}
								{assign var=k value=0}
								{assign var=j value=5-$order['rating']|escape:'htmlall':'UTF-8'}
							{while $k!=$j}
								<img class="starSize" src="{$smarty.const._MODULE_DIR_|escape:'htmlall':'UTF-8'}/wkproductrating/views/img/star-off.png" />
								{assign var=k value=$k+1}
							{/while}
						</div>
					</td>
					<td>
						<span>
							<i class="icon-calendar"></i> {dateFormat date=$order.date_upd full=0|escape:'htmlall':'UTF-8'} - <i class="icon-time"></i> {$order.date_upd|substr:11:5|escape:'htmlall':'UTF-8'}
						</span>

					</td>
					<td>{if $order['active'] == 1}{l s='Active' mod='wkproductrating'}{else}{l s='Pending' mod='wkproductrating'}{/if}</td>
					<td>
						<a class="btn btn-default button button-small smallButton" id="view" data-id-product-rating="{$order['id_product_rating']|escape:'html':'UTF-8'}" href="#viewBox">
							<span><i class="icon icon-eye-open iconbtn"></i></span>
						</a>
						<span id="demo"></span>
						{if $canEditRating == 1}
							<a class="btn btn-default button button-small smallButton" id="editReview" data-id-product-rating="{$order['id_product_rating']|escape:'html':'UTF-8'}" href="#editbox">
								<span><i class="icon icon-pencil iconbtn"></i></span>
							</a>
						{/if}
						{if $canDeleteRating == 1}
							<a class="btn btn-default button button-small smallButton" id="deleteReview" data-id-product-rating="{$order['id_product_rating']|escape:'html':'UTF-8'}">
								<span><i class="icon icon-trash iconbtn"></i></span>
							</a>
					</td>
					{/if}
				{/if}
			</tr>
		{/foreach}
	</table>
</div>

<div class="col-lg-11" id="box" style="display: none;">
	<div class="panel-heading boxHeading">
		<img class="lodingImage" id="addLoading" src="{$loadingImage|escape:'htmlall':'UTF-8'}" alt="{l s='loading...' mod='wkproductrating'}">
		<h3>
			<i class="icon icon-pencil"></i>{l s=' WRITE REVIEW' mod='wkproductrating'}
		</h3>
	</div>
		<p id="ratingMsg"></p>
	<div class="panel-content">
		<form id="testForm" class="form-horizontal col-lg-offset-1" action="" method="post" name="commentform">

			<div class="form-group">
				<label for="rating" class="control-label required">{l s='Rating' mod='wkproductrating'}</label>
				<span id="rating_image"></span>
				<p id="ratingBlankError"></p>
			</div>

			<div class="form-group">
				<label for="title" class="required">{l s='Title' mod='wkproductrating'}</label>
				<input type="text" class="form-control" name="title" data-toggle="tooltip" data-placement="top" title="{l s='Title' mod='wkproductrating'}">
				<p id="titleError"></p>
			</div>

			<div class="form-group">
				{if $limit == 1}
					<label for="comment"  {if $commentValid == 1} class="required" {/if}>{l s='Comment' mod='wkproductrating'}</label>
					<textarea name="comment" class="form-control" rows="3" maxlength="{$limitValue|escape:'htmlall':'UTF-8'}" data-toggle="tooltip" data-placement="top" title="{l s='Comment' mod='wkproductrating'}"></textarea>
				{else}
					<label for="comment" {if $commentValid == 1} class="required" {/if}>{l s='Comment' mod='wkproductrating'}</label>
					<textarea name="comment" class="form-control" rows="3" data-toggle="tooltip" data-placement="top" title="{l s='Comment' mod='wkproductrating'}"></textarea>
				{/if}
				<p id="commentError"></p>
			</div>
			<input type="hidden" id="idOrder" name="idOrder" value={$id_order|escape:'htmlall':'UTF-8'}>
			<input type="hidden" name="idProduct" id="idProduct">
			<div class="form-group">
				<label class="required">
					{l s='Required fields.' mod='wkproductrating'}
				</label>
				<div>
					<button id="btnSubmit" type="button" class="button btn btn-default button-medium" name="btnSubmit" data-toggle="tooltip" data-placement="top" title="{l s='Submit' mod='wkproductrating'}"><span>{l s='Save' mod='wkproductrating'}</span></button>
				</div>
			</div>
		</form>
	</div>
</div>

<div class="col-lg-11" id="editbox" style="display: none;">
	<div class="panel-heading boxHeading col-lg-12">
		<h3>
			<i class="icon icon-pencil"></i>{l s=' EDIT REVIEW' mod='wkproductrating'}
		</h3>
	</div>
	<p id="edtmsg"></p>
	<form id="edtForm" class="form-horizontal col-lg-offset-1" action="" method="post" name="commenteditform">
		<img class="lodingImage" id="editLoading" src={$loadingImage|escape:'htmlall':'UTF-8'} alt="{l s='loading...' mod='wkproductrating'}">
		<div class="form-group">
			<label for="ratingedt" class="control-label required">{l s='Rating' mod='wkproductrating'}</label>
			<span id="ratingedit_image"></span>
			<p id="ratingEdtBlankError"></p>
		</div>
		<div class="form-group">
			<label for="titleedit" class="required">{l s='Title' mod='wkproductrating'}</label>
			<input type="text" class="form-control" name="titleedit" data-toggle="tooltip" data-placement="top" title="{l s='Title' mod='wkproductrating'}">
			<p id="titleEdtError"></p>
		</div>

		<div class="form-group">
			{if $limit == 1}
				<label for="commentedit" {if $commentValid == 1} class="required" {/if}>{l s='Comment' mod='wkproductrating'}</label>
				<textarea name="commentedit" class="form-control" rows="3" maxlength="{$limitValue|escape:'htmlall':'UTF-8'}" data-toggle="tooltip" data-placement="top" title="{l s='Comment' mod='wkproductrating'}"></textarea>
			{else}
				<label for="commentedit" {if $commentValid == 1} class="required" {/if}>{l s='Comment' mod='wkproductrating'}</label>
				<textarea name="commentedit" class="form-control" rows="3" data-toggle="tooltip" data-placement="top" title="{l s='Comment' mod='wkproductrating'}"></textarea>
			{/if}
			<p id="commentEdtError"></p>
		</div>
		<input type="hidden" name="id_product_rating" id="id_product_rating">
		<div class="form-group">
			<label class="required">
				{l s='Required fields.' mod='wkproductrating'}
			</label>
		</div>
		<div class="form-group">
			<button id="btnEdtSubmit" type="submit" class="button btn btn-default button-medium" name="btnEdtSubmit" data-toggle="tooltip" data-placement="top" title="{l s='Submit' mod='wkproductrating'}"><span>{l s='Update' mod='wkproductrating'}</span></button>
		</div>
	</form>
</div>

<div id="viewBox" style="display: none;">
	<img class="lodingImage" id="viewLoading" src="{$loadingImage|escape:'htmlall':'UTF-8'}" alt="{l s='loading...' mod='wkproductrating'}">
	<div class="clearfix col-lg-12" id="data">
		<div class="panel-heading boxHeading">
			<h3>
				<i class="icon icon-eye-open"></i>{l s=' YOUR REVIEW' mod='wkproductrating'}
			</h3>
		</div>
		<div class="form-horizontal viewForm">
			<div class="row">
				<label class="control-label col-lg-1">{l s='Rating' mod='wkproductrating'}:</label>
				<div class="txt col-lg-9 col-lg-offset-2">
					<p class="form-control-static" id="ratingView"></p>
				</div>
			</div>
			<div class="row">
				<label class="control-label col-lg-1">{l s='Title' mod='wkproductrating'}:</label>
				<div class="txt col-lg-9 col-lg-offset-3">
					<p class="form-control-static" id="titleView"></p>
				</div>
			</div>
			<div class="row">
				<label class="control-label col-lg-1">{l s='Comment' mod='wkproductrating'}:</label>
				<div class="txt col-lg-9 col-lg-offset-3">
					<p class="form-control-static" id="commentView"></p>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
        $('#rating_image').raty({
            path: moduledir,
            scoreName: 'rating_image',
        });
	});
</script>
