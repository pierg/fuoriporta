{*
**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*
*}

{if $tableContents}
<section class="page-product-box">
	<h3 class="page-product-heading"><i class="icon icon-star"></i>{l s=' AVERAGE REVIEW ' mod='wkproductrating'}{$avgRating.averageRating|escape:'htmlall':'UTF-8'}</h3>
</section>

{foreach $tableContents as $comment}
	<div class="fullDiv">
		<div class="row">
			<div class="sideBox col-md-1">
				<span class="{if $comment.rating<3}alert-danger{elseif $comment.rating<4}alert-warning{else}alert-success{/if} ratingBox">{$comment.rating|escape:'htmlall':'UTF-8'}</span>
			</div>
			<div class="col-md-8">
				<div id="ratingHeader">
					<div id="showStar">
						{assign var=i value=0}
						{while $i != $comment.rating}
							<img class="starSize" src="{$smarty.const._MODULE_DIR_|escape:'htmlall':'UTF-8'}/wkproductrating/views/img/star-on.png" />
							{assign var=i value=$i+1}
						{/while}
							{assign var=k value=0}	
							{assign var=j value=5-$comment.rating}
						{while $k!=$j}
							<img class="starSize" src="{$smarty.const._MODULE_DIR_|escape:'htmlall':'UTF-8'}/wkproductrating/views/img/star-off.png" />
							{assign var=k value=$k+1}
						{/while}
					</div>
					<strong>{$comment['title']|escape:'htmlall':'UTF-8'}</strong>
				</div>
				<div class="field">
					<p>{$comment['comment']|escape:'htmlall':'UTF-8'}</p>
				</div>
				<div class="field row">
					<span class="col-md-3">
						{l s='By' mod='wkproductrating'}
						<strong>{$comment['firstname']|escape:'htmlall':'UTF-8'} {$comment['lastname']|escape:'htmlall':'UTF-8'}</strong>
					</span>
					<span class="col-md-4">
						{l s='Posted on:' mod='wkproductrating'}
						<strong>
							<i class="icon-calendar"></i> 
							{dateFormat date=$comment.date_upd full=0|escape:'htmlall':'UTF-8'} - 
							<i class="icon-time"></i> 
							{$comment.date_upd|substr:11:5|escape:'htmlall':'UTF-8'}
						</strong>
					</span>
				</div>
			</div>
		</div>
	</div>
{/foreach}
	{if $allContents|@count gt $commentLimit}
	<div class="row col-md-8">
		<a class="button btn btn-default button-medium viewAll" href="{$link->getModuleLink('wkproductrating', 'allcomments', ['id_product' => $idProduct|escape:'htmlall':'UTF-8'])}">
			<span>{l s='View All (' mod='wkproductrating'}{$allContents|@count|escape:'html':'UTF-8'}
			{l s=' Reviews)' mod='wkproductrating'}</span>
		</a>
	</div>
	{/if}
</div>
{/if}