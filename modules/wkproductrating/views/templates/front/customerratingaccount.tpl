{*
**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*
*}

{capture name=path}
	<a href="{$link->getPageLink('my-account')|escape:'htmlall':'UTF-8'}">
		{l s='My account' mod='wkproductrating'}
	</a>
	<span class="navigation-pipe">{$navigationPipe|escape:'htmlall':'UTF-8'}</span>
	{l s='Reviews' mod='wkproductrating'}
{/capture}

<div class='wk_my_ratings_products container'>
	<div class='wk_rating_heading'>
		<h2>{l s='YOUR REVIEWS' mod='wkproductrating'}</h2>
	</div>
	<p>{l s='Please give your reviews.' mod='wkproductrating'}</p>
	<div class='row'>
		{if $customerRatingDetails}
			<div class='col-md-6 col-sm-6'>
				{assign var=countProductLeft value=0}
				{foreach $customerRatingDetails as $productDetails}
					{foreach $productDetails as $productDetail}
						{assign var=countProductLeft value=$countProductLeft+1}
						{if $countProductLeft % 2 != 0}
							<div class="wkcard wk_rating_product_details {if isset($productDetail.id_product_rating)}wk_rating_review_avl {/if}">
								<div class='col-md-4'>
									<a href="{$link->getProductLink($productDetail.product_obj)|escape:'htmlall':'UTF-8'}"><img src="{if isset($productDetail.id_image) && $productDetail.id_image}{$link->getImageLink($productDetail.name, $productDetail.id_image, 'small_default')|escape:'htmlall':'UTF-8'}{else}{$img_prod_dir|escape:'htmlall':'UTF-8'}{$lang_iso|escape:'htmlall':'UTF-8'}-default-small_default.jpg{/if}" alt="{$productDetail.name|escape:'htmlall':'UTF-8'}"/></a>
								</div>
								<div class='row col-md-8'>
									<a href="{$link->getProductLink($productDetail.product_obj)|escape:'htmlall':'UTF-8'}"><p class='col-md-12 wk_rating_prod_name'>{$productDetail.name|escape:'htmlall':'UTF-8'}</p></a>
									<p class='col-md-12 wk_rating_prod_price'>{$productDetail.price_static|escape:'htmlall':'UTF-8'}</p>
									<div class='col-md-12' id="showStar">
									{if isset($productDetail.avj_rating)}
										{assign var=i value=0}
										{while $i != $productDetail.avj_rating}
											<img class="starSize" src="{$smarty.const._MODULE_DIR_|escape:'htmlall':'UTF-8'}/wkproductrating/views/img/star-on.png" />
											{assign var=i value=$i+1}
										{/while}
											{assign var=k value=0}
											{assign var=j value=5-$productDetail.avj_rating}
										{while $k!=$j}
											<img class="starSize" src="{$smarty.const._MODULE_DIR_|escape:'htmlall':'UTF-8'}/wkproductrating/views/img/star-off.png" />
											{assign var=k value=$k+1}
										{/while}
										({$productDetail.avj_rating|escape:'htmlall':'UTF-8'}/5)
									{/if}
									</div>
									{if !isset($productDetail.id_product_rating)}
										<div class='text-sm-center'>
											<a class='btn btn-default button button-small wk_rating_write_btn' data-target="#box" data-id-product="{$productDetail['id_product']|escape:'html':'UTF-8'}" data-id-order="{$productDetail['id_order']|escape:'htmlall':'UTF-8'}" id="writereview" href="#box"><span>{l s='NEED YOUR WORDS' mod='wkproductrating'}</span></a>
										</div>
									{/if}
								</div>
								{if isset($productDetail.id_product_rating)}
									<div class='row wk_rating_review_details'>
										<div class='col-md-12 col-sm-12 wk_rating_your_review'><span>{l s='YOUR REVIEW' mod='wkproductrating'}</span></div>
										<div class='col-md-12'>
											<span class='col-md-4 col-sm-4 wk_rating_keys'>{l s='Title: ' mod='wkproductrating'}</span>
											<span class='col-md-8 col-sm-8'>{$productDetail.title|escape:'htmlall':'UTF-8'}</span>
										</div>
										<div class='col-md-12'>
											<span class='col-md-4 col-sm-4 wk_rating_keys'>{l s='Rating: ' mod='wkproductrating'}</span>
											<span class='col-md-8 col-sm-8' id="showStar">
													{assign var=i value=0}
													{while $i != $productDetail.rating}
															<img class="starSize" src="{$smarty.const._MODULE_DIR_|escape:'htmlall':'UTF-8'}/wkproductrating/views/img/star-on.png" />
															{assign var=i value=$i+1}
													{/while}
															{assign var=k value=0}
															{assign var=j value=5-$productDetail.rating}
													{while $k!=$j}
															<img class="starSize" src="{$smarty.const._MODULE_DIR_|escape:'htmlall':'UTF-8'}/wkproductrating/views/img/star-off.png" />
															{assign var=k value=$k+1}
													{/while}
													({if $productDetail.rating}{$productDetail.rating|escape:'htmlall':'UTF-8'}{else}0{/if}/5)
											</span>
										</div>
										<div class='col-md-12'>
											<span class='col-md-4 col-sm-4 wk_rating_keys'>{l s='Status: ' mod='wkproductrating'}</span>
											{if $productDetail.active == 0}
												<span class='wkpending col-md-8 col-sm-8'>{l s='Pending' mod='wkproductrating'}</span>
											{else}
												<span class='wkapproved col-md-8 col-sm-8'>{l s='Approved' mod='wkproductrating'}</span>
											{/if}
										</div>
										<div class='col-md-6 col-sm-6'><span class='wk_rating_keys'>{l s='Comment: ' mod='wkproductrating'}</span></div>
										<div class='col-md-6 col-sm-6 wk_rating_smallButton'>
											<a class="btn btn-default button button-small smallButton" id="view" data-id-product-rating="{$productDetail['id_product_rating']|escape:'html':'UTF-8'}" href="#viewBox">
												<span><i class="icon icon-eye-open iconbtn"></i></span>
											</a>
											<span id="demo"></span>
											{if $canEditRating == 1}
												<a class="btn btn-default button button-small smallButton" id="editReview" data-id-product-rating="{$productDetail['id_product_rating']|escape:'html':'UTF-8'}" href="#editbox">
													<span><i class="icon icon-pencil iconbtn"></i></span>
												</a>
											{/if}
											{if $canDeleteRating == 1}
												<a class="btn btn-default button button-small smallButton" id="deleteReview" data-id-product-rating="{$productDetail.id_product_rating|escape:'htmlall':'UTF-8'}">
													<span><i class="icon icon-trash iconbtn"></i></span>
												</a>
											{/if}
										</div>
										<div class='wk_rating_comment col-md-12 col-sm-12'>
											<p>{$productDetail.comment|escape:'htmlall':'UTF-8'}</p>
										</div>
									</div>
								{/if}
							</div>
						{/if}
					{/foreach}
				{/foreach}
			</div>
			<div class='col-md-6 col-sm-6'>
				{assign var=countProductRight value=0}
					{foreach $customerRatingDetails as $productDetails}
						{foreach $productDetails as $productDetail}
							{assign var=countProductRight value=$countProductRight+1}
							{if $countProductRight % 2 == 0}
								<div class="wkcard wk_rating_product_details {if isset($productDetail.id_product_rating)}wk_rating_review_avl {/if}">
								<div class='col-md-4'>
									<a href="{$link->getProductLink($productDetail.product_obj)|escape:'htmlall':'UTF-8'}"><img src="{if isset($productDetail.id_image) && $productDetail.id_image}{$link->getImageLink($productDetail.name, $productDetail.id_image, 'small_default')|escape:'htmlall':'UTF-8'}{else}{$img_prod_dir|escape:'htmlall':'UTF-8'}{$lang_iso|escape:'htmlall':'UTF-8'}-default-small_default.jpg{/if}" alt="{$productDetail.name|escape:'htmlall':'UTF-8'}"/></a>
								</div>
								<div class='row col-md-8'>
									<a href="{$link->getProductLink($productDetail.product_obj)|escape:'htmlall':'UTF-8'}"><p class='col-md-12 wk_rating_prod_name'>{$productDetail.name|escape:'htmlall':'UTF-8'}</p></a>
									<p class='col-md-12 wk_rating_prod_price'>{$productDetail.price_static|escape:'htmlall':'UTF-8'}</p>
									<div class='col-md-12' id="showStar">
									{if isset($productDetail.avj_rating)}
										{assign var=i value=0}
										{while $i != $productDetail.avj_rating}
											<img class="starSize" src="{$smarty.const._MODULE_DIR_|escape:'htmlall':'UTF-8'}/wkproductrating/views/img/star-on.png" />
											{assign var=i value=$i+1}
										{/while}
											{assign var=k value=0}
											{assign var=j value=5-$productDetail.avj_rating}
										{while $k!=$j}
											<img class="starSize" src="{$smarty.const._MODULE_DIR_|escape:'htmlall':'UTF-8'}/wkproductrating/views/img/star-off.png" />
											{assign var=k value=$k+1}
										{/while}
										({$productDetail.avj_rating|escape:'htmlall':'UTF-8'}/5)
									{/if}
									</div>
									{if !isset($productDetail.id_product_rating)}
										<div class='text-sm-center'>
											<a class='btn btn-default button button-small wk_rating_write_btn' data-target="#box" data-id-product="{$productDetail['id_product']|escape:'html':'UTF-8'}" data-id-order="{$productDetail['id_order']|escape:'htmlall':'UTF-8'}" id="writereview" href="#box"><span>{l s='NEED YOUR WORDS' mod='wkproductrating'}</span></a>
										</div>
									{/if}
								</div>
								{if isset($productDetail.id_product_rating)}
									<div class='row wk_rating_review_details'>
										<div class='col-md-12 col-sm-12 wk_rating_your_review'><span>{l s='YOUR REVIEW' mod='wkproductrating'}</span></div>
										<div class='col-md-12'>
											<span class='col-md-4 col-sm-4 wk_rating_keys'>{l s='Title: ' mod='wkproductrating'}</span>
											<span class='col-md-8 col-sm-8'>{$productDetail.title|escape:'htmlall':'UTF-8'}</span>
										</div>
										<div class='col-md-12'>
											<span class='col-md-4 col-sm-4 wk_rating_keys'>{l s='Rating: ' mod='wkproductrating'}</span>
											<span class='col-md-8 col-sm-8' id="showStar">
													{assign var=i value=0}
													{while $i != $productDetail.rating}
															<img class="starSize" src="{$smarty.const._MODULE_DIR_|escape:'htmlall':'UTF-8'}/wkproductrating/views/img/star-on.png" />
															{assign var=i value=$i+1}
													{/while}
															{assign var=k value=0}
															{assign var=j value=5-$productDetail.rating}
													{while $k!=$j}
															<img class="starSize" src="{$smarty.const._MODULE_DIR_|escape:'htmlall':'UTF-8'}/wkproductrating/views/img/star-off.png" />
															{assign var=k value=$k+1}
													{/while}
													({if $productDetail.rating}{$productDetail.rating|escape:'htmlall':'UTF-8'}{else}0{/if}/5)
											</span>
										</div>
										<div class='col-md-12'>
											<span class='col-md-4 col-sm-4 wk_rating_keys'>{l s='Status: ' mod='wkproductrating'}</span>
											{if $productDetail.active == 0}
												<span class='wkpending col-md-8 col-sm-8'>{l s='Pending' mod='wkproductrating'}</span>
											{else}
												<span class='wkapproved col-md-8 col-sm-8'>{l s='Approved' mod='wkproductrating'}</span>
											{/if}
										</div>
										<div class='col-md-6 col-sm-6'><span class='wk_rating_keys'>{l s='Comment: ' mod='wkproductrating'}</span></div>
										<div class='col-md-6 col-sm-6 wk_rating_smallButton'>
											<a class="btn btn-default button button-small smallButton" id="view" data-id-product-rating="{$productDetail['id_product_rating']|escape:'html':'UTF-8'}" href="#viewBox">
												<span><i class="icon icon-eye-open iconbtn"></i></span>
											</a>
											<span id="demo"></span>
											{if $canEditRating == 1}
												<a class="btn btn-default button button-small smallButton" id="editReview" data-id-product-rating="{$productDetail['id_product_rating']|escape:'html':'UTF-8'}" href="#editbox">
													<span><i class="icon icon-pencil iconbtn"></i></span>
												</a>
											{/if}
											{if $canDeleteRating == 1}
												<a class="btn btn-default button button-small smallButton" id="deleteReview" data-id-product-rating="{$productDetail.id_product_rating|escape:'htmlall':'UTF-8'}">
													<span><i class="icon icon-trash iconbtn"></i></span>
												</a>
											{/if}
										</div>
										<div class='wk_rating_comment col-md-12 col-sm-12'>
											<p>{$productDetail.comment|escape:'htmlall':'UTF-8'}</p>
										</div>
									</div>
								{/if}
							</div>
							{/if}
						{/foreach}
					{/foreach}
				</div>
			</div>
		{else}
			<div class="alert alert-warning">
				<p>{l s='There are no Product for Reviews.' mod='wkproductrating'}</p>
			</div>
		{/if}
	</div>
</div>

{* Comment Box *}

<div class="col-lg-11" id="box" style="display: none;">
	<div class="panel-heading boxHeading">
		<img class="lodingImage" id="addLoading" src="{$loadingImage|escape:'htmlall':'UTF-8'}" alt="{l s='loading...' mod='wkproductrating'}">
		<h3>
			<i class="icon icon-pencil"></i>{l s=' WRITE REVIEW' mod='wkproductrating'}
		</h3>
	</div>
		<p id="ratingMsg"></p>
	<div class="panel-content">
		<form id="testForm" class="form-horizontal col-lg-offset-1" action="" method="post" name="commentform">
			<div class="form-group">
				<label for="rating" class="control-label required">{l s='Rating' mod='wkproductrating'}</label>
				<span id="rating_image"></span>
				<p id="ratingBlankError"></p>
			</div>
			<div class="form-group">
				<label for="title" class="required">{l s='Title' mod='wkproductrating'}</label>
				<input type="text" class="form-control" name="title" data-toggle="tooltip" data-placement="top" title="{l s='Title' mod='wkproductrating'}">
				<p id="titleError"></p>
			</div>
			<div class="form-group">
				{if $limit == 1}
					<label for="comment"  {if $commentValid == 1} class="required" {/if}>{l s='Comment' mod='wkproductrating'}</label>
					<textarea name="comment" class="form-control" rows="3" maxlength="{$limitValue|escape:'htmlall':'UTF-8'}" data-toggle="tooltip" data-placement="top" title="{l s='Comment' mod='wkproductrating'}"></textarea>
				{else}
					<label for="comment" {if $commentValid == 1} class="required" {/if}>{l s='Comment' mod='wkproductrating'}</label>
					<textarea name="comment" class="form-control" rows="3" data-toggle="tooltip" data-placement="top" title="{l s='Comment' mod='wkproductrating'}"></textarea>
				{/if}
				<p id="commentError"></p>
			</div>
			<input type="hidden" id="idOrder" name="idOrder">
			<input type="hidden" name="idProduct" id="idProduct">
			<div class="form-group">
				<label class="required">
					{l s='Required fields.' mod='wkproductrating'}
				</label>
				<div>
					<button id="btnSubmit" type="button" class="button btn btn-default button-medium" name="btnSubmit" data-toggle="tooltip" data-placement="top" title="{l s='Submit' mod='wkproductrating'}"><span>{l s='Save' mod='wkproductrating'}</span></button>
				</div>
			</div>
		</form>
	</div>
</div>

<div class="col-lg-11" id="editbox" style="display: none;">
	<div class="panel-heading boxHeading col-lg-12">
		<h3>
			<i class="icon icon-pencil"></i>{l s=' EDIT REVIEW' mod='wkproductrating'}
		</h3>
	</div>
	<p id="edtmsg"></p>
	<form id="edtForm" class="form-horizontal col-lg-offset-1" action="" method="post" name="commenteditform">
		<img class="lodingImage" id="editLoading" src={$loadingImage|escape:'htmlall':'UTF-8'} alt="{l s='loading...' mod='wkproductrating'}">
		<div class="form-group">
			<label for="ratingedt" class="control-label required">{l s='Rating' mod='wkproductrating'}</label>
			<span id="ratingedit_image"></span>
			<p id="ratingEdtBlankError"></p>
		</div>
		<div class="form-group">
			<label for="titleedit" class="required">{l s='Title' mod='wkproductrating'}</label>
			<input type="text" class="form-control" name="titleedit" data-toggle="tooltip" data-placement="top" title="{l s='Title' mod='wkproductrating'}">
			<p id="titleEdtError"></p>
		</div>

		<div class="form-group">
			{if $limit == 1}
				<label for="commentedit" {if $commentValid == 1} class="required" {/if}>{l s='Comment' mod='wkproductrating'}</label>
				<textarea name="commentedit" class="form-control" rows="3" maxlength="{$limitValue|escape:'htmlall':'UTF-8'}" data-toggle="tooltip" data-placement="top" title="{l s='Comment' mod='wkproductrating'}"></textarea>
			{else}
				<label for="commentedit" {if $commentValid == 1} class="required" {/if}>{l s='Comment' mod='wkproductrating'}</label>
				<textarea name="commentedit" class="form-control" rows="3" data-toggle="tooltip" data-placement="top" title="{l s='Comment' mod='wkproductrating'}"></textarea>
			{/if}
			<p id="commentEdtError"></p>
		</div>
		<input type="hidden" name="id_product_rating" id="id_product_rating">
		<div class="form-group">
			<label class="required">
				{l s='Required fields.' mod='wkproductrating'}
			</label>
		</div>
		<div class="form-group">
			<button id="btnEdtSubmit" type="submit" class="button btn btn-default button-medium" name="btnEdtSubmit" data-toggle="tooltip" data-placement="top" title="{l s='Submit' mod='wkproductrating'}"><span>{l s='Update' mod='wkproductrating'}</span></button>
		</div>
	</form>
</div>

<div id="viewBox" style="display: none;">
	<img class="lodingImage" id="viewLoading" src="{$loadingImage|escape:'htmlall':'UTF-8'}" alt="{l s='loading...' mod='wkproductrating'}">
	<div class="clearfix col-lg-12" id="data">
		<div class="panel-heading boxHeading">
			<h3>
				<i class="icon icon-eye-open"></i>{l s=' YOUR REVIEW' mod='wkproductrating'}
			</h3>
		</div>
		<div class="form-horizontal viewForm">
			<div class="row">
				<label class="control-label col-lg-1">{l s='Rating' mod='wkproductrating'}:</label>
				<div class="txt col-lg-9 col-lg-offset-2">
					<p class="form-control-static" id="ratingView"></p>
				</div>
			</div>
			<div class="row">
				<label class="control-label col-lg-1">{l s='Title' mod='wkproductrating'}:</label>
				<div class="txt col-lg-9 col-lg-offset-3">
					<p class="form-control-static" id="titleView"></p>
				</div>
			</div>
			<div class="row">
				<label class="control-label col-lg-1">{l s='Comment' mod='wkproductrating'}:</label>
				<div class="txt col-lg-9 col-lg-offset-3">
					<p class="form-control-static" id="commentView"></p>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
        $('#rating_image').raty({
            path: moduledir,
            scoreName: 'rating_image',
        });
	});
</script>
