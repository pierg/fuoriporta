{*
**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*
*}

<div class='wk_top_rated_products'>
    <div class='wk_rating_heading'>
        <h2>{l s='TOP RATED PRODUCTS' mod='wkproductrating'}</h2>
    </div>
    <div class='wk_top_rated_products_grid'>
        <div class='row'>
            {if $productDetails}
            {foreach $productDetails as $product}
                <div class='col-md-4'>
                    <article class='wkcard wk_rating_top_product'>
                        <a href="{$link->getProductLink($product.id_product)|escape:'htmlall':'UTF-8'}"><img class='wk_rating_product_image' src="{if isset($product.id_image) && $product.id_image}{$link->getImageLink($product.name, $product.id_image, 'home_default')|escape:'htmlall':'UTF-8'}{else}{$img_prod_dir|escape:'htmlall':'UTF-8'}{$lang_iso|escape:'htmlall':'UTF-8'}-default-home_default.jpg{/if}" alt="{$product.name|escape:'htmlall':'UTF-8'}"/>
                        <div class='wk_rating_top_name'>{$product.name|truncate:30:"...":true|escape:'htmlall':'UTF-8'}</div>
                        <div class='wk_showStar_grid' id="showStar">
                        {if isset($product.avj_rating)}
                            {assign var=i value=0}
                            {while $i != $product.avj_rating}
                                <img class="starSize" src="{$smarty.const._MODULE_DIR_|escape:'htmlall':'UTF-8'}/wkproductrating/views/img/star-on.png" />
                                {assign var=i value=$i+1}
                            {/while}
                                {assign var=k value=0}
                                {assign var=j value=5-$product.avj_rating}
                            {while $k!=$j}
                                <img class="starSize" src="{$smarty.const._MODULE_DIR_|escape:'htmlall':'UTF-8'}/wkproductrating/views/img/star-off.png" />
                                {assign var=k value=$k+1}
                            {/while}
                            ({$product.avj_rating|escape:'htmlall':'UTF-8'}/5)
                        {/if}
                        </div>
                        <div class='wk_rating_top_price_grid'>{$product.price_static|escape:'htmlall':'UTF-8'}</div></a>
                    </article>
                </div>
            {/foreach}
            {else}
            <div class="alert alert-warning">
                <p>{l s='There are no Product Reviews.' mod='wkproductrating'}</p>
            </div>
            {/if}
        </div>
    </div>
</div>
