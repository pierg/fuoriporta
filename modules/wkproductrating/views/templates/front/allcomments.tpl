{*
**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*
*}

{extends file=$layout}
{block name='content'}
<div class="productDetails">
	<div class="row card">
		<div class="details col-md-8">
			<div id="productImage" class="col-md-4">
				<a href="{$link->getProductLink($idProduct)|escape:'htmlall':'UTF-8'}"><img src="{$link->getImageLink($productName, $idImage)|escape:'htmlall':'UTF-8'}" alt="{$productName|escape:'htmlall':'UTF-8'}"/></a>
			</div>
			<div id="nameAndPrice" class="col-md-4">
				<h3><a href="{$link->getProductLink($idProduct)|escape:'htmlall':'UTF-8'}">{$productName|escape:'htmlall':'UTF-8'}</a></h3>
				<div id="ratingDiv">
					<span id="avgRate" style="color: {if $avgRating.averageRating < 3}red{elseif $avgRating.averageRating < 4}rgb(254,145,38){else}rgb(85,198,94){/if};">
						{$avgRating.averageRating|escape:'htmlall':'UTF-8'}
					</span>
					<div id="avg">
						<span>{l s='Average Rating' mod='wkproductrating'}</span>
					</div>
					<div id="totalRevies" class="">
						{$comments|@count|escape:'htmlall':'UTF-8'} {l s='Reviews' mod='wkproductrating'}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="allComments">
	{foreach $comments as $comment}
		<div class="fullDiv">
		<div class="card">
			<div class="row">
				<div class="sideBox col-md-1">
					<span class="{if $comment.rating<3}alert-danger{elseif $comment.rating<4}alert-warning{else}alert-success{/if} ratingBox">{$comment.rating|escape:'htmlall':'UTF-8'}</span>
				</div>
				<div class="col-md-11">
					<div id="showStar">
						{assign var=i value=0}
						{while $i != $comment.rating}
							<img class="starSize" src="{$smarty.const._MODULE_DIR_|escape:'htmlall':'UTF-8'}/wkproductrating/views/img/star-on.png" />
							{assign var=i value=$i+1}
						{/while}
							{assign var=k value=0}
							{assign var=j value=5-$comment.rating}
						{while $k!=$j}
							<img class="starSize" src="{$smarty.const._MODULE_DIR_|escape:'htmlall':'UTF-8'}/wkproductrating/views/img/star-off.png" />
							{assign var=k value=$k+1}
						{/while}
					</div>
					<strong>{$comment['title']|escape:'htmlall':'UTF-8'}</strong>
					<div class="field">
						<p>{$comment['comment']|escape:'htmlall':'UTF-8'}</p>
					</div>
					<div class="field">
						{l s='By' mod='wkproductrating'}
						<strong>{$comment['firstname']|escape:'htmlall':'UTF-8'} {$comment['lastname']|escape:'htmlall':'UTF-8'}</strong>
						&nbsp;
						&nbsp;
						{l s='Posted on' mod='wkproductrating'}:
						<span>
							<strong>
								<i class="material-icons">&#xE916;</i> {dateFormat date=$comment.date_upd full=0} <i class="material-icons">&#xE192;</i> {$comment.date_upd|substr:11:5|escape:'htmlall':'UTF-8'}
							</strong>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	{/foreach}
</div>
{/block}
