{*
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All rights is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*}

<div id="showStar">
	{assign var=i value=0}
	{while $i != $rating}
		<img class="starSize" width="18" height="18" src="{$smarty.const._MODULE_DIR_|escape:'htmlall':'UTF-8'}/wkproductrating/views/img/star-on.png" />
		{assign var=i value=$i+1}
	{/while}
		{assign var=k value=0}	
		{assign var=j value=5-$rating|escape:'htmlall':'UTF-8'}
	{while $k!=$j}
		<img class="starSize" width="18" height="18" src="{$smarty.const._MODULE_DIR_|escape:'htmlall':'UTF-8'}/wkproductrating/views/img/star-off.png" />
		{assign var=k value=$k+1}
	{/while}
</div>
