{*
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All rights is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*}

{if $giveVoucher}
    <p>{l s='Also, ' mod='wkproductrating'}
    {if $voucherTimeOut}{l s='If you leave your review in next %d Days, than' sprintf=[$voucherTimeOut] mod='wkproductrating'}{/if}
    {l s='we will email you a %s off discount code as a thank you.' sprintf=[$amount] mod='wkproductrating'}</p>

    <p>{l s='we will send the discount code to the email address you use to write your review.' mod='wkproductrating'}</p>
    <p>{l s='Thanks again for the support!!' mod='wkproductrating'}
{/if}