{*
**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*
*}

<div class="panel clearfix">
	<div class="panel-heading">
		<i class="icon-user"></i>
		{if isset($ratingDetails)}
			{l s='Edit Product Rating' mod='wkproductrating'}
		{else}
			{l s='Add Product Rating' mod='wkproductrating'}
		{/if}
	</div>
	<div class="control-label">
		<form id="{$table|escape:'htmlall':'UTF-8'}_form" class="defaultForm {$name_controller|escape:'htmlall':'UTF-8'} form-horizontal" action="{$current|escape:'htmlall':'UTF-8'}&{if !empty($submit_action)}{$submit_action|escape:'htmlall':'UTF-8'}{/if}&token={$token}" method="post" enctype="multipart/form-data">
			{if isset($ratingDetails)}
				<div class="form-group">
				  	<label class="control-label col-lg-2 required">
				  		{l s='Order Id:' mod='wkproductrating'}
				  	</label>
			  		<div class="col-lg-2">
			  			<input type="hidden" name="id_order" id="idOrder" value="{$ratingDetails['id_order']|escape:'htmlall':'UTF-8'}">
			  			<p class="form-control-static">{$ratingDetails['id_order']|escape:'htmlall':'UTF-8'}</p>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2 required">
				  		{l s='Product: ' mod='wkproductrating'}
				  	</label>
			  		<div class="col-lg-2">
			  			<input type="hidden" name="id_product" id="id_product" value="{$ratingDetails['id_product']|escape:'htmlall':'UTF-8'}">
			  			<p class="form-control-static">{$ratingDetails['id_product']|escape:'htmlall':'UTF-8'}</p>
					</div>
				</div>

				<div>
					<input type="hidden" name="id_product_rating" value="{$ratingDetails['id_product_rating']|escape:'htmlall':'UTF-8'}">
				</div>
			{else}
				<div class="form-group row">
				    <label class="control-label col-lg-2">
				        <span class="label-tooltip" data-toggle="tooltip" data-html="true" title="{l s='Select whether products comments displayed or not on product page.' mod='wkproductrating'}">{l s='Status: ' mod='wkproductrating'}</span>
				    </label>
				    <div class="col-lg-3">
				        <span class="switch prestashop-switch fixed-width-lg">
				            <input type="radio" value="1" id="active_on" name="active"
				            {if isset($smarty.post.active)} {if $smarty.post.active == 1} checked="checked" {/if} {elseif isset($ratingDetails) && $ratingDetails['active'] == 1} checked="checked" {else} checked="checked"{/if} />
				            <label class="radioCheck" for="active_on">{l s='Yes' mod='wkproductrating'}</label>
				            <input type="radio" value="0" id="active_off" name="active" {if isset($smarty.post.active)} {if $smarty.post.active == 0} checked="checked" {/if} {elseif isset($ratingDetails) && $ratingDetails['active'] == 0} checked="checked" {/if} />
				            <label class="radioCheck" for="active_off">{l s='No' mod='wkproductrating'}</label>
				            <a class="slide-button btn"></a>
				        </span>
				    </div>
				</div>

				<div class="form-group">
				  	<label class="control-label col-lg-2 required">
				  		<span class="label-tooltip" data-toggle="tooltip" data-html="true" title="{l s='Select order Id of customer.' mod='wkproductrating'}">{l s='Order Id:' mod='wkproductrating'}</span>
				  	</label>
			  		<div class="dropdown col-lg-8">
			  			<select id="idOrder" name="id_order">
					      <option>{l s='Select Order' mod='wkproductrating'}</option>
						{foreach $order as $value}
					      <option>{$value.id_order|escape:'htmlall':'UTF-8'}</option>
						{/foreach}
					    </select>
					</div>
				</div>

				<div class="form-group">
			  		<span id="errorMsg" class="col-lg-12 col-lg-offset-2" style="display: none; color: red">
			  			<p>
			  				{l s='All Product of this Order have already been reviewed.' mod='wkproductrating'}
			  			</p>
			  		</span>

		  			<label class="control-label col-lg-2 required">
				  		<span class="label-tooltip" data-toggle="tooltip" data-html="true" title="{l s='Select Product for Rating.' mod='wkproductrating'}">{l s='Product:' mod='wkproductrating'}</span>
				  	</label>
			  		<div class="dropdown col-lg-8">
					    <img style="display: none; position: absolute; margin-left: 823px" id="loading" src="{$loadingImage|escape:'htmlall':'UTF-8'}" alt="{l s='loading...' mod='wkproductrating'}">
			  			<select id="id_product" name="id_product">
					      <option></option>
					    </select>
					</div>
				</div>
			{/if}
			<div class="form-group">
				<label class="control-label col-lg-2 required">
			  		<span class="label-tooltip" data-toggle="tooltip" data-html="true" title="{l s='Give a Rating.' mod='wkproductrating'}">{l s='Rating:' mod='wkproductrating'}</span>
			  	</label>
				<div class="col-lg-2">
				{if isset($ratingDetails)}
		    		<span id="rating" value="{$ratingDetails['rating']|escape:'htmlall':'UTF-8'}"></span>
				{else}
		    		<span id="rating"></span>
		    	{/if}
		    	</div>
		    </div>

			<div class="form-group">
				<label class="control-label col-lg-2 required">
			  		<span class="label-tooltip" data-toggle="tooltip" data-html="true" title="{l s='Review title.' mod='wkproductrating'}">{l s='Title:' mod='wkproductrating'}</span>
			  	</label>
			  	<div  class="col-lg-8">
			  		<input type="text" name="title" value="{if isset($ratingDetails)}{$ratingDetails['title']|escape:'htmlall':'UTF-8'}{/if}">
			  	</div>
			</div>

			<div class="form-group">
				<label class="control-label col-lg-2 required">
			  		<span class="label-tooltip" data-toggle="tooltip" data-html="true" title="{l s='Review comment.' mod='wkproductrating'}">{l s='Comment:' mod='wkproductrating'}</span>
			  	</label>
		  		<textarea rows="4" class="col-lg-8" name="comment">{if isset($ratingDetails)}{$ratingDetails['comment']|escape:'htmlall':'UTF-8'}{/if}</textarea>
			</div>
		<div class="panel-footer">
			<a href="{$link->getAdminLink('AdminWebkulRating')|escape:'html':'UTF-8'}" class="btn btn-default">
				<i class="process-icon-cancel"></i> {l s='Cancel' mod='wkproductrating'}
			</a>
			<button type="submit" name="btnSubmit" class="btn btn-default pull-right">
				<i class="process-icon-save"></i>{l s='Save' mod='wkproductrating'}
			</button>
		</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
        $('#rating').raty({
            path: moduledir,
            scoreName: 'rating',
            {if isset($ratingDetails)} score: {$ratingDetails['rating']|escape:'htmlall':'UTF-8'} {/if}
        });
	});
</script>