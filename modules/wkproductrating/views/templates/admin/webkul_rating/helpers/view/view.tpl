{*
**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*
*}

<div class="panel clearfix">
	<div class="panel-heading">
		<i class="icon-user"></i>
		{l s='View Review Details' mod='wkproductrating'}
	</div>
	<div class="form-horizontal">
		<div class="row">
			<label class="control-label col-lg-3">{l s='Order Id:' mod='wkproductrating'}</label>
			<div class="col-lg-9">
			<p class="form-control-static">{$viewComment['id_order']|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Product Id:' mod='wkproductrating'}</label>
			<div class="col-lg-9">
			<p class="form-control-static">{$viewComment['id_product']|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Customer Id:' mod='wkproductrating'}</label>
			<div class="col-lg-9">
			<p class="form-control-static">{$viewComment['id_customer']|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Rating:' mod='wkproductrating'}</label>
			<div class="col-lg-9">
				<div id="showStar">
					<p class="form-control-static">
						{assign var=i value=0}
						{while $i != $viewComment['rating']}
							<img width="18" height="18" class="starSize" src="{$smarty.const._MODULE_DIR_|escape:'htmlall':'UTF-8'}/wkproductrating/views/img/star-on.png" />
							{assign var=i value=$i+1}
						{/while}
							{assign var=k value=0}
							{assign var=j value=5-$viewComment['rating']|escape:'htmlall':'UTF-8'}
						{while $k!=$j}
							<img width="18" height="18" class="starSize" src="{$smarty.const._MODULE_DIR_|escape:'htmlall':'UTF-8'}/wkproductrating/views/img/star-off.png" />
							{assign var=k value=$k+1}
						{/while}
					</p>
				</div>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Title:' mod='wkproductrating'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">{$viewComment['title']|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>
		<div class="row">
			<label class="control-label col-lg-3">{l s='Comment:' mod='wkproductrating'}</label>
			<div class="col-lg-9">
				<p class="form-control-static" style="text-align: justify;">{$viewComment['comment']|escape:'htmlall':'UTF-8'}</p>
			</div>
		</div>

		<div class="row">
			<label class="control-label col-lg-3">{l s='Status:' mod='wkproductrating'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">
					{if $viewComment['active']}
					<span class="label label-success">
						<i class="icon-check"></i>
						{l s='Active' mod='wkproductrating'}
					</span>
					{else}
						<span class="label label-danger">
							<i class="icon-remove"></i>
							{l s='Pending' mod='wkproductrating'}
						</span>
					{/if}
				</p>
			</div>
		</div>

		<div class="row">
			<label class="control-label col-lg-3">{l s='Posted On:' mod='wkproductrating'}</label>
			<div class="col-lg-9">
				<p class="form-control-static">
					<i class="icon-calendar"></i> {dateFormat date=$viewComment.date_upd full=0|escape:'htmlall':'UTF-8'} - <i class="icon-time"></i> {$viewComment.date_upd|substr:11:5|escape:'htmlall':'UTF-8'}
				</p>
			</div>
		</div>
	</div>
</div>
