{*
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All rights is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*}

<div class="form-group clearfix">
	<label class="col-sm-4 control-label" >
		<span class="label-tooltip" data-toggle="tooltip" data-html="true" title="" data-original-title="{l s='Admin will recive an email when customer gives rating on prchased products.' mod='wkproductrating'}">{l s='Email on customer review' mod='wkproductrating'}</span>
	</label>
	<div class="col-sm-8">
		<span class="switch prestashop-switch fixed-width-lg">
			<input type="radio" value="1" id="WK_PRODUCT_RATING_ADMIN_NOTI_on" name="WK_PRODUCT_RATING_ADMIN_NOTI"
			{if $WK_PRODUCT_RATING_ADMIN_NOTI == 1}
				checked="checked"
			{/if}>
			<label for="WK_PRODUCT_RATING_ADMIN_NOTI_on">{l s='Yes' mod='wkproductrating'}</label>
			<input type="radio" value="0" id="WK_PRODUCT_RATING_ADMIN_NOTI_off" name="WK_PRODUCT_RATING_ADMIN_NOTI"
			{if $WK_PRODUCT_RATING_ADMIN_NOTI == 0}
				checked="checked"
			{/if}>
			<label for="WK_PRODUCT_RATING_ADMIN_NOTI_off">{l s='No' mod='wkproductrating'}</label>
			<a class="slide-button btn"></a>
		</span>
	</div>
</div>

<div class="form-group clearfix">
	<label class="col-sm-4 control-label" >
		<span class="label-tooltip" data-toggle="tooltip" data-html="true" title="" data-original-title="{l s='Admin will recive an email when voucher gets genrated for customer.' mod='wkproductrating'}">{l s='Email on Voucher Creation' mod='wkproductrating'}</span>
	</label>
	<div class="col-sm-8">
		<span class="switch prestashop-switch fixed-width-lg">
			<input type="radio" value="1" id="WK_PRODUCT_RATING_ADMIN_NOTI_VOUCHER_on" name="WK_PRODUCT_RATING_ADMIN_NOTI_VOUCHER"
			{if $WK_PRODUCT_RATING_ADMIN_NOTI_VOUCHER == 1}
				checked="checked"
			{/if}>
			<label for="WK_PRODUCT_RATING_ADMIN_NOTI_VOUCHER_on">{l s='Yes' mod='wkproductrating'}</label>
			<input type="radio" value="0" id="WK_PRODUCT_RATING_ADMIN_NOTI_VOUCHER_off" name="WK_PRODUCT_RATING_ADMIN_NOTI_VOUCHER"
			{if $WK_PRODUCT_RATING_ADMIN_NOTI_VOUCHER == 0}
				checked="checked"
			{/if}>
			<label for="WK_PRODUCT_RATING_ADMIN_NOTI_VOUCHER_off">{l s='No' mod='wkproductrating'}</label>
			<a class="slide-button btn"></a>
		</span>
	</div>
</div>