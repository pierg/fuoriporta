{*
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All rights is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*}

<div class="panel">
	<div class="panel-heading">
		<i class="icon-info-circle"></i>&nbsp; {l s='Emails And Reminder Settings' mod='wkproductrating'}
	</div>
    <form method="post" name="notificationsettings">
        <ul class="nav nav-tabs">
            <li class="nav-item active">
                <a class="nav-link" href="#admin-notification" data-toggle="tab">
                    {l s='Admin Notification Settings' mod='wkproductrating'}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#customer-notification" data-toggle="tab">
                    {l s='Customer Notification Settings' mod='wkproductrating'}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#voucher-notification" data-toggle="tab">
                    {l s='Voucher Settings' mod='wkproductrating'}
                </a>
            </li>
        </ul>
        <div class="tab-content panel collapse in">
            <div class="tab-pane fade in active" id="admin-notification">
                {include file="./admin_notification.tpl"}
            </div>
            <div class="tab-pane fade in" id="customer-notification">
                {include file="./customer_notification.tpl"}
            </div>
            <div class="tab-pane clearfix fade in" id="voucher-notification">
                {include file="./voucher_settings.tpl"}
            </div>
        </div>
        <div class="panel-footer">
			<button class="btn btn-default pull-right" name="btnSubmit_notificationsettings" value="1" type="submit">
				<i class="process-icon-save"></i> {l s='Save' mod='wkproductrating'}
			</button>
		</div>
	</form>
</div>
