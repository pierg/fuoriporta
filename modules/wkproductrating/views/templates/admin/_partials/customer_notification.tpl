{*
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All rights is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*}

<div class="form-group clearfix">
	<label class="col-sm-4 control-label" >
		<span class="label-tooltip" data-toggle="tooltip" data-html="true" title="" data-original-title="{l s='Send Rating Reminder mail when some one able to give review on Product after buy.' mod='wkproductrating'}">{l s='Rating Reminder mail' mod='wkproductrating'}</span>
	</label>
	<div class="col-sm-6">
		<span class="switch prestashop-switch fixed-width-lg">
			<input type="radio" value="1" id="WK_PRODUCT_RATING_REMINDER_MAIL_on" name="WK_PRODUCT_RATING_REMINDER_MAIL"
			{if $WK_PRODUCT_RATING_REMINDER_MAIL == 1}
				checked="checked"
			{/if}>
			<label for="WK_PRODUCT_RATING_REMINDER_MAIL_on">{l s='Yes' mod='wkproductrating'}</label>
			<input type="radio" value="0" id="WK_PRODUCT_RATING_REMINDER_MAIL_off" name="WK_PRODUCT_RATING_REMINDER_MAIL"
			{if $WK_PRODUCT_RATING_REMINDER_MAIL == 0}
				checked="checked"
			{/if}>
			<label for="WK_PRODUCT_RATING_REMINDER_MAIL_off">{l s='No' mod='wkproductrating'}</label>
			<a class="slide-button btn"></a>
		</span>
	</div>
</div>

<div class="form-group clearfix">
	<label class="col-sm-4 control-label" >
		<span class="label-tooltip" data-toggle="tooltip" data-html="true" title="" data-original-title="{l s='Mail to the customer to give the review on bought products.' mod='wkproductrating'}">{l s='Discount for the customer who rate' mod='wkproductrating'}</span>
	</label>
	<div class="col-sm-6">
		<span class="switch prestashop-switch fixed-width-lg">
			<input type="radio" value="1" id="WK_PRODUCT_RATING_GIVE_VOUCHER_on" name="WK_PRODUCT_RATING_GIVE_VOUCHER"
			{if $WK_PRODUCT_RATING_GIVE_VOUCHER == 1}
				checked="checked"
			{/if}>
			<label for="WK_PRODUCT_RATING_GIVE_VOUCHER_on">{l s='Yes' mod='wkproductrating'}</label>
			<input type="radio" value="0" id="WK_PRODUCT_RATING_GIVE_VOUCHER_off" name="WK_PRODUCT_RATING_GIVE_VOUCHER"
			{if $WK_PRODUCT_RATING_GIVE_VOUCHER == 0}
				checked="checked"
			{/if}>
			<label for="WK_PRODUCT_RATING_GIVE_VOUCHER_off">{l s='No' mod='wkproductrating'}</label>
			<a class="slide-button btn"></a>
		</span>
	</div>
</div>

<div class="form-group clearfix wk_rating_voucher_time">
    <label class="control-label col-lg-4">
        <span class="label-tooltip" data-toggle="tooltip" data-html="true" title="" data-original-title="{l s='Customer will recive voucher once he will give the rating.' mod='wkproductrating'}">{l s='Time period of rating to receive a voucher' mod='wkproductrating'}</span>
    </label>
    <div class="col-lg-8">
		<div class='col-md-2 input-group'>
            <input class='form-control' type="text" name="WK_PRODUCT_RATING_VOUCHER_TIMEOUT" value="{$WK_PRODUCT_RATING_VOUCHER_TIMEOUT|escape:'htmlall':'UTF-8'}">
            <span class='input-group-addon'>{l s='Days' mod='wkproductrating'}</span>
        </div>
    </div>
    <div class="help-block col-lg-8 col-lg-offset-4">
        {l s='Set to 0 if you want to apply this for unlimited time period.' mod='wkproductrating'}
    </div>
</div>

<div class="form-group clearfix">
	<div class="panel-body">
		<div class="alert alert-info">
			<p>{l s='To expire the Time period of rating to receive a voucher ' mod='wkproductrating'}</p>
			<br />
			<p>{l s='Please set the CRON, insert the following line in your cron tasks manager:' mod='wkproductrating'}</p>
			<br />
			<ul class="list-unstyled">
				<li><code>{$cron_url|escape:'html':'UTF-8'}</code></li>
			</ul>
		</div>
	</div>
</div>
