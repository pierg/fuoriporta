{*
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All rights is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*}

<div class="form-group row">
    <label class="control-label col-lg-3">
        <span class="label-tooltip" data-toggle="tooltip"
        title="{l s='auto approve and genrate Reward voucher.' mod='wkproductrating'}">
            {l s='Auto Approve Reward Voucher' mod='wkproductrating'}
        </span>
    </label>
    <div class="col-lg-9">
        <span class="switch prestashop-switch fixed-width-lg">
            <input type="radio" name="WK_RATING_VOUCHER_APPROVE" id="WK_RATING_VOUCHER_APPROVE_on" value="1"
                {if isset($smarty.post.WK_RATING_VOUCHER_APPROVE)}
                {if $smarty.post.WK_RATING_VOUCHER_APPROVE}
                    checked="checked"
                {/if}
            {elseif $ratingVoucherConfigs['WK_RATING_VOUCHER_APPROVE']}
                checked="checked"
            {/if}/>
            <label for="WK_RATING_VOUCHER_APPROVE_on">{l s='Yes' mod='wkproductrating'}</label>
            <input type="radio" name="WK_RATING_VOUCHER_APPROVE" id="WK_RATING_VOUCHER_APPROVE_off" value="0"
            {if isset($smarty.post.WK_RATING_VOUCHER_APPROVE)}
                {if !$smarty.post.WK_RATING_VOUCHER_APPROVE}
                    checked="checked"
                {/if}
            {elseif !$ratingVoucherConfigs['WK_RATING_VOUCHER_APPROVE']}
                checked="checked"
            {/if}/>
            <label for="WK_RATING_VOUCHER_APPROVE_off">{l s='No' mod='wkproductrating'}</label>
            <a class="slide-button btn"></a>
        </span>
    </div>
</div>

<div class="form-group row">
    <label class="control-label col-lg-3">
        <span class="label-tooltip" data-toggle="tooltip"
        title="{l s='If the voucher is not yet in the cart, it will be displayed in the cart summary.' mod='wkproductrating'}">
            {l s='Highlight' mod='wkproductrating'}
        </span>
    </label>
    <div class="col-lg-9">
        <span class="switch prestashop-switch fixed-width-lg">
            <input type="radio" name="WK_RATING_VOUCHER_HIGHLIGHT" id="WK_RATING_VOUCHER_HIGHLIGHT_on" value="1"
                {if isset($smarty.post.WK_RATING_VOUCHER_HIGHLIGHT)}
                {if $smarty.post.WK_RATING_VOUCHER_HIGHLIGHT}
                    checked="checked"
                {/if}
            {elseif $ratingVoucherConfigs['WK_RATING_VOUCHER_HIGHLIGHT']}
                checked="checked"
            {/if}/>
            <label for="WK_RATING_VOUCHER_HIGHLIGHT_on">{l s='Yes' mod='wkproductrating'}</label>
            <input type="radio" name="WK_RATING_VOUCHER_HIGHLIGHT" id="WK_RATING_VOUCHER_HIGHLIGHT_off" value="0"
            {if isset($smarty.post.WK_RATING_VOUCHER_HIGHLIGHT)}
                {if !$smarty.post.WK_RATING_VOUCHER_HIGHLIGHT}
                    checked="checked"
                {/if}
            {elseif !$ratingVoucherConfigs['WK_RATING_VOUCHER_HIGHLIGHT']}
                checked="checked"
            {/if}/>
            <label for="WK_RATING_VOUCHER_HIGHLIGHT_off">{l s='No' mod='wkproductrating'}</label>
            <a class="slide-button btn"></a>
        </span>
    </div>
</div>

<div class="form-group row">
    <label class="control-label col-lg-3">
        <span class="label-tooltip" data-toggle="tooltip"
        title="{l s='Only applicable if the voucher value is greater than the cart total.' mod='wkproductrating'}
        {l s='If you do not allow partial use, the voucher value will be lowered to the total order amount. If you allow partial use, however, a new voucher will be created with the remainder.' mod='wkproductrating'}">
            {l s='Partial Use' mod='wkproductrating'}
        </span>
    </label>
    <div class="col-lg-9">
        <span class="switch prestashop-switch fixed-width-lg">
            <input type="radio" name="WK_RATING_VOUCHER_PARTIAL_USE" id="WK_RATING_VOUCHER_PARTIAL_USE_on" value="1" {if isset($smarty.post.WK_RATING_VOUCHER_PARTIAL_USE)}
                {if $smarty.post.WK_RATING_VOUCHER_PARTIAL_USE}
                    checked="checked"
                {/if}
            {elseif $ratingVoucherConfigs['WK_RATING_VOUCHER_PARTIAL_USE']}
                checked="checked"
            {/if}/>
            <label class="t" for="WK_RATING_VOUCHER_PARTIAL_USE_on">{l s='Yes' mod='wkproductrating'}</label>
            <input type="radio" name="WK_RATING_VOUCHER_PARTIAL_USE" id="WK_RATING_VOUCHER_PARTIAL_USE_off" value="0"
            {if isset($smarty.post.WK_RATING_VOUCHER_PARTIAL_USE)}
                {if !$smarty.post.WK_RATING_VOUCHER_PARTIAL_USE}
                    checked="checked"
                {/if}
            {elseif !$ratingVoucherConfigs['WK_RATING_VOUCHER_PARTIAL_USE']}
                checked="checked"
            {/if}/>
            <label class="t" for="WK_RATING_VOUCHER_PARTIAL_USE_off">{l s='No' mod='wkproductrating'}</label>
            <a class="slide-button btn"></a>
        </span>
    </div>
</div>
<div class="form-group row">
    <label class="control-label col-lg-3">
        <span class="label-tooltip" data-toggle="tooltip"
        title="{l s='This text will be prefixed in the voucher code created by the customers for his reward.' mod='wkproductrating'}">
            {l s='Voucher Code Prefix' mod='wkproductrating'}
        </span>
    </label>
    <div class="col-lg-3">
        <input type="text" name="WK_RATING_VOUCHER_PREFIX"
        {if isset($smarty.post.WK_RATING_VOUCHER_PREFIX)}
            value="{$smarty.post.WK_RATING_VOUCHER_PREFIX|escape:'htmlall':'UTF-8'}"
        {elseif isset($ratingVoucherConfigs['WK_RATING_VOUCHER_PREFIX'])}
            value="{$ratingVoucherConfigs['WK_RATING_VOUCHER_PREFIX']|escape:'htmlall':'UTF-8'}"
        {/if}>
    </div>
</div>
<div class="form-group row">
    <label class="control-label col-lg-3">
        <span class="label-tooltip" data-toggle="tooltip"
            title="{l s='If voucher is created by using reward of different validities. In this case calidity of voucher will be taken from this configuration option.' mod='wkproductrating'}">
            {l s='Voucher Validity' mod='wkproductrating'}
        </span>
    </label>
    <div class="col-lg-2" id="reward_vouchers_validity">
        <div class="input-group">
            <input placeholder="{l s='Enter validity' mod='wkproductrating'}" type="text" name="WK_RATING_VOUCHER_VALIDITY"
            {if isset($smarty.post.WK_RATING_VOUCHER_VALIDITY)}
                value="{$smarty.post.WK_RATING_VOUCHER_VALIDITY|escape:'htmlall':'UTF-8'}"
            {elseif isset($ratingVoucherConfigs['WK_RATING_VOUCHER_VALIDITY'])}
                value="{$ratingVoucherConfigs['WK_RATING_VOUCHER_VALIDITY']|escape:'htmlall':'UTF-8'}"
            {/if}>
            <span class="input-group-addon">{l s='Days' mod='wkproductrating'}</span>
        </div>
    </div>
</div>
<div class="form-group">
	<label class="control-label col-lg-3">
		<span class="label-tooltip" data-toggle="tooltip"
			title="{l s='Choose the discount Type' mod='wkproductrating'}">
			{l s='Apply a discount' mod='wkproductrating'}
		</span>
	</label>
	<div class="col-lg-9">
		<div class="radio">
			<label for="apply_discount_percent">
				<input type="radio" name="WK_RATING_REWARD_VOUCHER_TYPE" value="0" {if $ratingVoucherConfigs['WK_RATING_REWARD_VOUCHER_TYPE'] == 0}checked{/if}>
				{l s='Percent (%)' mod='wkproductrating'}
			</label>
		</div>
		<div class="radio">
			<label for="apply_discount_amount">
				<input type="radio" name="WK_RATING_REWARD_VOUCHER_TYPE" value="1" {if $ratingVoucherConfigs['WK_RATING_REWARD_VOUCHER_TYPE'] == 1}checked{/if}>
				{l s='Amount' mod='wkproductrating'}
			</label>
		</div>
	</div>
</div>
<div class="form-group row" id="wk_amount_type_fixed">
	<label class="control-label col-lg-3">
		<span class="label-tooltip" data-toggle="tooltip"
		title="{l s='Voucher Amount.' mod='wkproductrating'}">
			{l s='Value' mod='wkproductrating'}
		</span>
	</label>
	<div class="col-lg-2">
		<input type="text" name="WK_RATING_REWARD_VOUCHER_AMOUNT"
		{if isset($smarty.post.WK_RATING_REWARD_VOUCHER_AMOUNT)}
			value="{$smarty.post.WK_RATING_REWARD_VOUCHER_AMOUNT|escape:'htmlall':'UTF-8'}"
		{elseif isset($ratingVoucherConfigs['WK_RATING_REWARD_VOUCHER_AMOUNT'])}
			value="{$ratingVoucherConfigs['WK_RATING_REWARD_VOUCHER_AMOUNT']|escape:'htmlall':'UTF-8'}"
		{/if}>
	</div>
	<div class="col-lg-2 wk_rating_voucher_fixed">
		<select name="WK_RATING_REWARD_VOUCHER_AMOUNT_CURRENCY">
			{foreach from=$currencies item='currency'}
				<option value="{$currency.id_currency|intval|escape:'htmlall':'UTF-8'}"
				{if isset($ratingVoucherConfigs['WK_RATING_REWARD_VOUCHER_AMOUNT_CURRENCY'])
				&& $ratingVoucherConfigs['WK_RATING_REWARD_VOUCHER_AMOUNT_CURRENCY'] == $currency.id_currency}
					selected="selected"
				{/if}>
					{$currency.iso_code|escape:'htmlall':'UTF-8'}
				</option>
			{/foreach}
		</select>
	</div>
	<div class="col-lg-2 wk_rating_voucher_fixed">
		<select name="WK_RATING_REWARD_VOUCHER_AMOUNT_TAX">
			<option value="0"
				{if isset($ratingVoucherConfigs['WK_RATING_REWARD_VOUCHER_AMOUNT_TAX'])
					&& $ratingVoucherConfigs['WK_RATING_REWARD_VOUCHER_AMOUNT_TAX'] == 0}
					selected="selected"
				{/if}>
				{l s='Tax excluded' mod='wkproductrating'}
			</option>
			<option value="1"
				{if isset($ratingVoucherConfigs['WK_RATING_REWARD_VOUCHER_AMOUNT_TAX'])
					&& $ratingVoucherConfigs['WK_RATING_REWARD_VOUCHER_AMOUNT_TAX'] == 1}
					selected="selected"
				{/if}>
				{l s='Tax included' mod='wkproductrating'}
			</option>
		</select>
	</div>
</div>
<div class="form-group row">
    <label class="control-label col-lg-3">
        <span class="label-tooltip" data-toggle="tooltip"
            title="{l s='You can choose a minimum amount for the cart either with or without the taxes and shipping.' mod='wkproductrating'}">
            {l s='Minimum Amount' mod='wkproductrating'}
        </span>
    </label>
    <div class="col-lg-9">
        <div class="row">
            <div class="col-lg-2">
                <input type="text" name="WK_RATING_VOUCHER_MIN_AMOUNT"
                {if isset($smarty.post.WK_RATING_VOUCHER_MIN_AMOUNT)}
                    value="{$smarty.post.WK_RATING_VOUCHER_MIN_AMOUNT|escape:'htmlall':'UTF-8'}"
                {elseif isset($ratingVoucherConfigs['WK_RATING_VOUCHER_MIN_AMOUNT'])}
                    value="{$ratingVoucherConfigs['WK_RATING_VOUCHER_MIN_AMOUNT']|escape:'htmlall':'UTF-8'}"
                {/if}>
            </div>
            <div class="col-lg-2">
                <select name="WK_RATING_VOUCHER_MIN_AMOUNT_CURRENCY">
                    {foreach from=$currencies item='currency'}
                        <option value="{$currency.id_currency|intval}"
                        {if isset($ratingVoucherConfigs['WK_RATING_VOUCHER_MIN_AMOUNT_CURRENCY'])
                        && $ratingVoucherConfigs['WK_RATING_VOUCHER_MIN_AMOUNT_CURRENCY'] == $currency.id_currency}
                            selected="selected"
                        {/if}>
                            {$currency.iso_code|escape:'htmlall':'UTF-8'}
                        </option>
                    {/foreach}
                </select>
            </div>
            <div class="col-lg-3">
                <select name="WK_RATING_VOUCHER_MIN_AMOUNT_TAX">
                    <option value="0"
                        {if isset($ratingVoucherConfigs['WK_RATING_VOUCHER_MIN_AMOUNT_TAX'])
                            && $ratingVoucherConfigs['WK_RATING_VOUCHER_MIN_AMOUNT_TAX'] == 0}
                            selected="selected"
                        {/if}>
                        {l s='Tax excluded' mod='wkproductrating'}
                    </option>
                    <option value="1"
                        {if isset($ratingVoucherConfigs['WK_RATING_VOUCHER_MIN_AMOUNT_TAX'])
                            && $ratingVoucherConfigs['WK_RATING_VOUCHER_MIN_AMOUNT_TAX'] == 1}
                            selected="selected"
                        {/if}>
                        {l s='Tax included' mod='wkproductrating'}
                    </option>
                </select>
            </div>
            <div class="col-lg-3">
                <select name="WK_RATING_VOUCHER_MIN_AMOUNT_SHIPPING">
                    <option value="0"
                        {if isset($ratingVoucherConfigs['WK_RATING_VOUCHER_MIN_AMOUNT_SHIPPING'])
                            && $ratingVoucherConfigs['WK_RATING_VOUCHER_MIN_AMOUNT_SHIPPING'] == 0}
                            selected="selected"
                        {/if}>
                        {l s='Shipping excluded' mod='wkproductrating'}
                    </option>
                    <option value="1"
                        {if isset($ratingVoucherConfigs['WK_RATING_VOUCHER_MIN_AMOUNT_SHIPPING'])
                            && $ratingVoucherConfigs['WK_RATING_VOUCHER_MIN_AMOUNT_SHIPPING'] == 1}
                            selected="selected"
                        {/if}>
                        {l s='Shipping included' mod='wkproductrating'}
                    </option>
                </select>
            </div>
        </div>
    </div>
</div>
<div class="form-group row">
    <label class="control-label col-lg-3">
        <span class="label-tooltip" data-toggle="tooltip"
            title="{l s='if disabled, vouchers created for customers commistion will not be used with other vouchers.' mod='wkproductrating'}">
            {l s='Compatibility with other cart rules' mod='wkproductrating'}
        </span>
    </label>
    <div class="col-lg-9">
        <span class="switch prestashop-switch fixed-width-lg">
            <input type="radio" name="WK_RATING_VOUCHER_CUMULATIVE" id="WK_RATING_VOUCHER_CUMULATIVE_on" value="1" checked="checked">
            <label class="t" for="WK_RATING_VOUCHER_CUMULATIVE_on">
                {l s='Yes' mod='wkproductrating'}
            </label>
            <input type="radio" name="WK_RATING_VOUCHER_CUMULATIVE" id="WK_RATING_VOUCHER_CUMULATIVE_off" value="0"
            {if isset($smarty.post.WK_RATING_VOUCHER_CUMULATIVE)}
                {if !$smarty.post.WK_RATING_VOUCHER_CUMULATIVE}
                    checked="checked"
                {/if}
            {elseif !$ratingVoucherConfigs['WK_RATING_VOUCHER_CUMULATIVE']}
                checked="checked"
            {/if}/>
            <label class="t" for="WK_RATING_VOUCHER_CUMULATIVE_off">
                {l s='No' mod='wkproductrating'}
            </label>
            <a class="slide-button btn"></a>
        </span>
    </div>
</div>
<div class="form-group row">
    <label class="control-label col-lg-3" for="category_block">
        <span class="label-tooltip" data-toggle="tooltip"
            title="{l s='Set yes, if you want category restriction in voucher.' mod='wkproductrating'}">
            {l s='Categories Restriction' mod='wkproductrating'}
        </span>
    </label>
    <div class="col-lg-9">
        <div class="row">
            <div class="col-lg-12" style="margin-bottom: 15px;">
                <span class="switch prestashop-switch fixed-width-lg">
                    <input type="radio" name="WK_RATING_VOUCHER_CATEGORY_RESTRICTION" id="WK_RATING_VOUCHER_CATEGORY_RESTRICTION_on" value="1"
                    {if isset($smarty.post.WK_RATING_VOUCHER_CATEGORY_RESTRICTION)}
                        {if $smarty.post.WK_RATING_VOUCHER_CATEGORY_RESTRICTION}
                            checked="checked"
                        {/if}
                    {elseif $ratingVoucherConfigs['WK_RATING_VOUCHER_CATEGORY_RESTRICTION']}
                        checked="checked"
                    {/if}/>
                    <label for="WK_RATING_VOUCHER_CATEGORY_RESTRICTION_on">{l s='Yes' mod='wkproductrating'}</label>
                    <input type="radio" name="WK_RATING_VOUCHER_CATEGORY_RESTRICTION" id="WK_RATING_VOUCHER_CATEGORY_RESTRICTION_off" value="0"
                    {if isset($smarty.post.WK_RATING_VOUCHER_CATEGORY_RESTRICTION)}
                        {if !$smarty.post.WK_RATING_VOUCHER_CATEGORY_RESTRICTION}
                            checked="checked"
                        {/if}
                    {elseif !$ratingVoucherConfigs['WK_RATING_VOUCHER_CATEGORY_RESTRICTION']}
                        checked="checked"
                    {/if}/>
                    <label for="WK_RATING_VOUCHER_CATEGORY_RESTRICTION_off">{l s='No' mod='wkproductrating'}</label>
                    <a class="slide-button btn"></a>
                </span>
            </div>
            <div class="col-lg-12" id="wk_withdrawl_category_block_cont"
			{if isset($ratingVoucherConfigs['WK_RATING_VOUCHER_CATEGORY_RESTRICTION']) && !$ratingVoucherConfigs['WK_RATING_VOUCHER_CATEGORY_RESTRICTION']}
				style="display:none;"
			{/if}>
                <div id="wk_withdrawl_category_block">
                    {$category_tree_voucher|escape:'htmlall':'UTF-8'}
                </div>
            </div>
        </div>
    </div>
</div>
