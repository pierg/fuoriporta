<?php
/**
 *  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
 *
 * @author    Línea Gráfica E.C.E. S.L.
 * @copyright Lineagrafica.es - Línea Gráfica E.C.E. S.L. all rights reserved.
 * @license   https://www.lineagrafica.es/licenses/license_en.pdf https://www.lineagrafica.es/licenses/license_es.pdf https://www.lineagrafica.es/licenses/license_fr.pdf
 */

class LGProductComment extends ObjectModel
{
    /** @var string Name */
    public $date;
    public $id_customer;
    public $id_product;
    public $stars;
    public $comment;
    public $id_lang;
    public $active;
    public $position;
    public $title;
    public $answer;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'lgcomments_productcomments',
        'primary' => 'id_lgcomments_productcomments',
        'multilang' => false,
        'fields' => array(
            'date' => array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'required' => true),
            'id_customer' => array('type' => self::TYPE_INT, 'required' => true),
            'id_product' => array('type' => self::TYPE_INT, 'required' => true),
            'stars' => array('type' => self::TYPE_INT, 'required' => true),
            'comment' => array('type' => self::TYPE_HTML, 'required' => true),
            'id_lang' => array('type' => self::TYPE_INT, 'required' => true),
            'active' => array('type' => self::TYPE_INT, 'required' => true),
            'position' => array('type' => self::TYPE_INT, 'required' => true),
            'title' => array('type' => self::TYPE_STRING, 'required' => true),
            'answer' => array('type' => self::TYPE_HTML, 'required' => false),
        )
    );
}
