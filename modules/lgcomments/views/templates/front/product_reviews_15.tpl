{*
*  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
*
*  @author    Línea Gráfica E.C.E. S.L.
*  @copyright Lineagrafica.es - Línea Gráfica E.C.E. S.L. all rights reserved.
*  @license   https://www.lineagrafica.es/licenses/license_en.pdf https://www.lineagrafica.es/licenses/license_es.pdf https://www.lineagrafica.es/licenses/license_fr.pdf
*}

{if $tab_type|intval  != 1}
{if $numlgcomments > 0 or $productform}
<script src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/js/product_reviews.js" type="text/javascript"></script>
<link href="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/css/product_reviews_15.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
    {literal}
    $(function(){
        $('a[href=#reset]').click(function(){
            $('table.five-stars').css('display', 'table');
            $('table.four-stars').css('display', 'table');
            $('table.three-stars').css('display', 'table');
            $('table.two-stars').css('display', 'table');
            $('table.one-star').css('display', 'table');
            $('table.zero-star').css('display', 'table');
            $('div#more_less').css('display', 'table');
            x={/literal}{$defaultdisplay|escape:'htmlall':'UTF-8'}{literal};
            $('#idTab798 table.productComment').not(':lt('+x+')').hide();
        });
    });
    $(document).ready(function () {
        x={/literal}{$defaultdisplay|escape:'htmlall':'UTF-8'}{literal};
        numberComment = $("#idTab798 table.productComment").size();
        $('#idTab798 table.productComment').not(':lt('+x+')').hide();
        $('#displayMore').click(function () {
            x= (x+{/literal}{$extradisplay|escape:'htmlall':'UTF-8'}{literal} <= numberComment) ? x+{/literal}{$extradisplay|escape:'htmlall':'UTF-8'}{literal} : numberComment;
            $('#idTab798 table:lt('+x+')').show();
        });
        $('#displayLess').click(function () {
            x={/literal}{$defaultdisplay|escape:'htmlall':'UTF-8'}{literal};
            $('#idTab798 table').not(':lt('+x+')').hide();
        });
    });
    {/literal}
    $(document).ready(function() {
        $("a#send_product_review").fancybox({
            'href'   : '#form_review',
            'autoScale':'true'
        });
    });
    function checkFields()
    {
        if($('#score_product').val() == '' || $('#title_product').val() == '' || $('#comment_product').val() == '')
        {				
            alert('{l s='All the fields are mandatory' mod='lgcomments'}');
        }
        else
        {				
            sendProductReview("{$link->getModuleLink('lgcomments', 'reviews', ['action'=>'sendReview', 'ajax' => '1'], $is_https)|escape:'quotes':'UTF-8'}");
            $.fancybox.close();
            var myalert = "{l s='The review has been correctly sent' mod='lgcomments'}";
            alert(myalert);
            location.reload();
        }
    }
    $(document).ready(function() {
    
        $("a.iframe").fancybox({
            'type' : 'iframe',
            'width':600,
            'height':600
        });
    });
    $('#cerrar_enviado').click(function(){
        $.fancybox.close;
    });
    function abrir(url) {
        open(url,'','top=300,left=300,width=600,height=600,scrollbars=yes') ;
    }
    function changestars(value1,value2)
    {
        $('#'+value2).attr('src','{$modules_dir|escape:'htmlall':'UTF-8'}lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/'+value1+'stars.png');
    }
</script>

<div id="idTab798" class="tab-pane">
<section class="table">
    {if $productfilter and $numlgcomments > $productfilternb}
        <div class="lgcomment_summary">
            <div class="commentfilter"><span style="text-transform:uppercase;font-weight:bold;">{l s='Filter reviews' mod='lgcomments'}</span></div>
            <div class="commentfilter"><a href="#five-stars" {if $fivestars == 0}style="pointer-events: none;"{/if}><img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/10stars.png" width="80%"> ({$fivestars|escape:'htmlall':'UTF-8'})</a></div>
            <div class="commentfilter"><a href="#four-stars" {if $fourstars == 0}style="pointer-events: none;"{/if}><img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/8stars.png" width="80%">({$fourstars|escape:'htmlall':'UTF-8'})</a></div>
            <div class="commentfilter"><a href="#three-stars" {if $threestars == 0}style="pointer-events: none;"{/if}><img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/6stars.png" width="80%">({$threestars|escape:'htmlall':'UTF-8'})</a></div>
            <div class="commentfilter"><a href="#two-stars" {if $twostars == 0}style="pointer-events: none;"{/if}><img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/4stars.png" width="80%">({$twostars|escape:'htmlall':'UTF-8'})</a></div>
            <div class="commentfilter"><a href="#one-star" {if $onestar == 0}style="pointer-events: none;"{/if}><img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/2stars.png" width="80%">({$onestar|escape:'htmlall':'UTF-8'})</a></div>
            <div class="commentfilter"><a href="#zero-star" {if $zerostar == 0}style="pointer-events: none;"{/if}><img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/0stars.png" width="80%">({$zerostar|escape:'htmlall':'UTF-8'})</a></div>
            <div class="commentfilter"><a href="#reset"><i class="icon-refresh"></i>  {l s='Reset' mod='lgcomments'}</a></div>
            <div style="clear:both;"></div>
        </div><br>
    {/if} 

    {if $productform}
        <br>
        <p style="text-align:center;"><a id="send_product_review" href="#form_review" class="lgcomment_button"><i class="icon-pencil"></i> {l s='Click here to leave a review' mod='lgcomments'}</a></p><br>
    {/if}

    {foreach from=$lgcomments item=lgcomment}
        <table class="productComment {if $lgcomment.stars >= 10}five-stars{elseif $lgcomment.stars >= 8 and $lgcomment.stars < 10}four-stars{elseif $lgcomment.stars >= 6 and $lgcomment.stars < 8}three-stars{elseif $lgcomment.stars >= 4 and $lgcomment.stars < 6}two-stars{elseif $lgcomment.stars >= 2 and $lgcomment.stars < 4}one-star{elseif $lgcomment.stars >= 0 and $lgcomment.stars < 2}zero-star{/if}">
            <tr>
                <td class="comment_column"><img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/{$lgcomment.stars|escape:'htmlall':'UTF-8'}stars.png" style="width:{$starsize|escape:'htmlall':'UTF-8'}px"></td>  
                <td><span class="comment_title">{stripslashes($lgcomment.title|escape:'quotes':'UTF-8')}</span></td> 
            </tr>
            <tr>
                <td>{$lgcomment.customer|escape:'quotes':'UTF-8'}&nbsp;{if isset($lgcomment.date) && $lgcomment.date|trim != ''}{l s='on' mod='lgcomments'} {$lgcomment.date|date_format:"$dateformat"|escape:'quotes':'UTF-8'}{/if}</td>
                <td><span class="comment_content">{stripslashes($lgcomment.comment|escape:'quotes':'UTF-8')}</span></td>
            </tr>
        {if $lgcomment.answer and $lgcomment.answer != '<p>0</p>'}
            <tr>
                <td></td>
                <td><span class="comment_answer">{l s='Answer:' mod='lgcomments'} {stripslashes($lgcomment.answer|escape:'quotes':'UTF-8')}</span></td>
            </tr>
        {/if}
            <tr height="10px;"><td colspan="2"></td></tr>
        </table>
    {/foreach}
    {if $defaultdisplay < $numlgcomments}
        <div id="more_less">
            <button class="button btn btn-default button button-small" id="displayMore"><span><i class="icon-plus-square"></i> {l s='Display more' mod='lgcomments'}</span></button>&nbsp;&nbsp;
            <button class="button btn btn-default button button-small" id="displayLess"><span><i class="icon-minus-square"></i> {l s='Display less' mod='lgcomments'}</span></button>
        </div>
    {/if}

    <div style="display:none">
        <div id="form_review" class="ps16">
            {if !$logged}
                <p style="font-size:16px; padding:20px; text-align:center;"><a href="{$link->getPageLink('authentication', true)|escape:'htmlall':'UTF-8'}?back={if $smarty.server.SERVER_PROTOCOL|strstr:'https'}https://{else}http://{/if}{$smarty.server.HTTP_HOST|escape:'htmlall':'UTF-8'}{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}"><i class="icon-sign-in"></i><br>{l s='You need to be logged in to be able to leave a review' mod='lgcomments'}</a></p>
            {elseif $alreadyreviewed}
                <p style="font-size:16px; padding:20px; text-align:center;"><br>{l s='You have already written a review about this product' mod='lgcomments'}</p>
            {else}
                <h3>{l s='Write a review' mod='lgcomments'}</h3><br>
                <div class="bloque_form">
                    <div style="float:left;">
                        <select name="score_product" id="score_product" onchange="changestars(this.value,'stars_store');">
                            <option value="0">{if $ratingscale == 5}0/5{elseif $ratingscale == 10}0/10{elseif $ratingscale == 20}0/20{else}0/10{/if}</option>
                            <option value="1">{if $ratingscale == 5}0,5/5{elseif $ratingscale == 10}1/10{elseif $ratingscale == 20}2/20{else}1/10{/if}</option>
                            <option value="2">{if $ratingscale == 5}1/5{elseif $ratingscale == 10}2/10{elseif $ratingscale == 20}4/20{else}2/10{/if}</option>
                            <option value="3">{if $ratingscale == 5}1,5/5{elseif $ratingscale == 10}3/10{elseif $ratingscale == 20}6/20{else}3/10{/if}</option>
                            <option value="4">{if $ratingscale == 5}2/5{elseif $ratingscale == 10}4/10{elseif $ratingscale == 20}8/20{else}4/10{/if}</option>
                            <option value="5">{if $ratingscale == 5}2,5/5{elseif $ratingscale == 10}5/10{elseif $ratingscale == 20}10/20{else}5/10{/if}</option>
                            <option value="6">{if $ratingscale == 5}3/5{elseif $ratingscale == 10}6/10{elseif $ratingscale == 20}12/20{else}6/10{/if}</option>
                            <option value="7">{if $ratingscale == 5}3,5/5{elseif $ratingscale == 10}7/10{elseif $ratingscale == 20}14/20{else}7/10{/if}</option>
                            <option value="8">{if $ratingscale == 5}4/5{elseif $ratingscale == 10}8/10{elseif $ratingscale == 20}16/20{else}8/10{/if}</option>
                            <option value="9">{if $ratingscale == 5}4,5/5{elseif $ratingscale == 10}9/10{elseif $ratingscale == 20}18/20{else}9/10{/if}</option>
                            <option value="10" selected>{if $ratingscale == 5}5/5{elseif $ratingscale == 10}10/10{elseif $ratingscale == 20}20/20{else}10/10{/if}</option>
                        </select>
                    </div>
                    <div style="float:left;"><img style="width:{$starsize|escape:'htmlall':'UTF-8'}px" src="{$modules_dir|escape:'htmlall':'UTF-8'}lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/10stars.png" id="stars_store" alt="rating"></div>
                    <div style="clear:both;"></div>
                </div><br>
                <div class="bloque_form">
                    <label>{l s='Title:' mod='lgcomments'} </label><textarea maxlength="50" id="title_product" name="title_product" style="width:100%;height:25px;" required></textarea>
                </div><br>
                <div class="bloque_form">
                    <label>{l s='Comment:' mod='lgcomments'} </label><textarea id="comment_product" name="comment_product" style="width:100%;height:100px;" required></textarea>
                </div>
                <input type="hidden" name="isocode" id="isocode" value="{$lang_iso|escape:'htmlall':'UTF-8'}"/>
                <input type="hidden" name="customerid" id="customerid" value="{$cookie->id_customer|escape:'htmlall':'UTF-8'}"/>
                <div class="clear"></div><br>
                <div><a id="submit_review" onclick="checkFields();" class="btn btn-default button button-small"><span>{l s='Send' mod='lgcomments'}</span></a></div>
            {/if}
        </div>
    </div>
</section>
</div>
<div class="clear clearfix"></div>
{/if}
{/if}
