{*
*  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
*
*  @author    Línea Gráfica E.C.E. S.L.
*  @copyright Lineagrafica.es - Línea Gráfica E.C.E. S.L. all rights reserved.
*  @license   https://www.lineagrafica.es/licenses/license_en.pdf https://www.lineagrafica.es/licenses/license_es.pdf https://www.lineagrafica.es/licenses/license_fr.pdf
*}

{if $ps16 && $displayslider}
    <script type="text/javascript" src="{$modules_dir|escape:'htmlall':'UTF-8'}lgcomments/views/js/responsiveCarousel.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{$modules_dir|escape:'htmlall':'UTF-8'}lgcomments/views/css/responsiveCarousel.css" />
    <div id="w">
        <br><br>
        <div>
            <div id="w-title"><a href="{$link->getModuleLink('lgcomments','reviews')|escape:'htmlall':'UTF-8'}">{l s='Last reviews' mod='lgcomments'}</a></div>
            <div id="w-more"><a href="{$link->getModuleLink('lgcomments','reviews')|escape:'htmlall':'UTF-8'}">{l s='see more' mod='lgcomments'} &gt;&gt;</a></div>
        </div>
        <div class="crsl-items" data-navigation="navbtns">
            <div class="crsl-wrap">
            {foreach from=$allreviews item=lgcomment}
                <div class="crsl-item">
                    <div class="crsl-title">{$lgcomment.title|truncate:'50':'...'|escape:'htmlall':'UTF-8'}
                    </div>
                    <div class="crsl-thumbnail">
                        <img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/{$lgcomment.stars|escape:'htmlall':'UTF-8'}stars.png" alt="rating">
                    </div>
                    <div class="crsl-comment">
                        {$lgcomment.comment|strip_tags|truncate:'200':'...'|escape:'htmlall':'UTF-8'}
                    </div>
                    <span class="crsl-name">{$lgcomment.customer|escape:'htmlall':'UTF-8'}</span>
                    <span class="crsl-date">{$lgcomment.date|date_format:"$dateformat"|escape:'htmlall':'UTF-8'}</span>
                </div>
            {/foreach}
            </div>
        </div>
        <div style="clear:both;"></div>
        <div id="crsl-nav">
            <nav class="slidernav">
                <div id="navbtns" class="clearfix">
                    <a href="#" class="previous"><img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/chevron-left.png" alt="previous"></a>
                    <a href="#" class="next"><img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/chevron-right.png" alt="next"></a>
                </div>
            </nav>
        </div>
    </div>
    <script type="text/javascript">
    {literal}
        $(function(){
          $('.crsl-items').lgcarousel({
            autoRotate: 3000,
            speed: 1000,
            visible: {/literal}{$sliderblocks|escape:'htmlall':'UTF-8'}{literal},
            itemMinWidth: 180,
            itemEqualHeight: 300,
            itemMargin: 9,
          });
          
          $("a[href=#]").on('click', function(e) {
            e.preventDefault();
          });
        });
    {/literal}
    </script>
{/if}

{if $displaysnippets && $numerocomentarios}
    <div itemscope="itemscope" itemtype="http://schema.org/LocalBusiness" style="text-align:center;">
        <meta itemprop="image" content="{if !$ps16}{$base_url|escape:'quotes':'UTF-8'}{/if}{$logo_url|escape:'htmlall':'UTF-8'}"/>
        <meta itemprop="name" content="{$shop_name|escape:'quotes':'UTF-8'}"/>
        <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
            {if $address_street1}<meta itemprop="streetAddress"
              content="
              {if $address_street1}{$address_street1|escape:'quotes':'UTF-8'}, {/if}
              {if $address_street2}{$address_street2|escape:'quotes':'UTF-8'}{/if}
              "/>
            {/if}
            {if $address_zip}<meta itemprop="postalCode" content="{$address_zip|escape:'quotes':'UTF-8'}"/> {/if}
            {if $address_city}<meta itemprop="addressLocality" content="{$address_city|escape:'quotes':'UTF-8'}"/> {/if}
            {if $address_state}<meta itemprop="addressRegion" content="{$address_state|escape:'quotes':'UTF-8'}"/> {/if}
            {if $address_country}<meta itemprop="postalCode" content="{$address_country|escape:'quotes':'UTF-8'}"/> {/if}
        </span>
        {if $address_phone}<meta itemprop="telephone" content="{$address_phone|escape:'quotes':'UTF-8'}"/> {/if}
        {if $price_range}<meta itemprop="priceRange" content="{$price_range|escape:'quotes':'UTF-8'}"/> {/if}
        <span itemprop="aggregateRating" itemscope="itemscope" itemtype="http://schema.org/AggregateRating">
            {if $ratingscale == 5}
                <meta content="{$mediacomentarios2/2|escape:'quotes':'UTF-8'}" itemprop="ratingValue" />
                <meta content="5" itemprop="bestRating" />
            {elseif $ratingscale == 10}
                <meta content="{$mediacomentarios2|escape:'quotes':'UTF-8'}" itemprop="ratingValue" />
                <meta content="10" itemprop="bestRating" />
            {elseif $ratingscale == 20}
                <meta content="{$mediacomentarios2*2|escape:'quotes':'UTF-8'}" itemprop="ratingValue" />
                <meta content="20" itemprop="bestRating" />
            {else}
                <meta content="{$mediacomentarios2|escape:'quotes':'UTF-8'}" itemprop="ratingValue" />
                <meta content="10" itemprop="bestRating" />
            {/if}
            <meta content="{$numerocomentarios|escape:'quotes':'UTF-8'}" itemprop="ratingCount" />
        </span>
    </div>
{/if}
