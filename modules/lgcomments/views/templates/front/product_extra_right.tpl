{*
*  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
*
*  @author    Línea Gráfica E.C.E. S.L.
*  @copyright Lineagrafica.es - Línea Gráfica E.C.E. S.L. all rights reserved.
*  @license   https://www.lineagrafica.es/licenses/license_en.pdf https://www.lineagrafica.es/licenses/license_es.pdf https://www.lineagrafica.es/licenses/license_fr.pdf
*}

{if isset($comments) && $comments}

    {if $comment_tab == 1}
        <script type="text/javascript">
        {literal}
        jQuery(document).ready(function() {
            $('.comment_anchor').click(function(e) {
            var activeDestination = $(this).attr("href");
                $('html,body').animate({
                scrollTop: $(activeDestination).offset().top -30}, 1000);
                    e.preventDefault();
            });
        });
        {/literal}
        </script>
    {/if}
    {if $comment_tab == 2}
        <script type="text/javascript">
        {literal}
        $(function(){
        $('a[href=#idTab798]').click(function(){
            $('*[id^="idTab"]').addClass('block_hidden_only_for_screen');
            $('div#idTab798').removeClass('block_hidden_only_for_screen');
            $('ul#more_info_tabs a[href^="#idTab"]').removeClass('selected');
            $('a[href="#idTab798"]').addClass('selected');
            $('div.page-product-box').removeClass('active');
            $('div#idTab798').addClass('active');
            $('li.active').removeClass('active');
            $(this).addClass('active');
        });
        });
        $(document).ready(function () {
            if(window.location.href.indexOf("#idTab798") >= 0){
                $('*[id^="idTab"]').addClass('block_hidden_only_for_screen');
                $('div#idTab798').removeClass('block_hidden_only_for_screen');
                $('ul#more_info_tabs a[href^="#idTab"]').removeClass('selected');
                $('a[href="#idTab798"]').addClass('selected');
                $('div.page-product-box').removeClass('active');
                $('div#idTab798').addClass('active');
                $('li.active').removeClass('active');
                $(this).addClass('active');
            }
        });
        jQuery(document).ready(function() {
            $('.comment_anchor').click(function(e) {
            var activeDestination = $(this).attr("href");
                $('html,body').animate({
                scrollTop: $(activeDestination).offset().top -60}, 1000);
                    e.preventDefault();
            });
        });
        {/literal}
        </script>
    {/if}

    <div class="review-avg">
        <div style="display:block; margin: {$prodtopmargin|escape:'htmlall':'UTF-8'}px auto {$prodbotmargin|escape:'htmlall':'UTF-8'}px auto;">
            {if $comments == 1}
                {if $content_only}
                    <a href="{$productlink|escape:'htmlall':'UTF-8'}#idTab798" target="_top" class="comment_anchor">
                {else}
                    <a href="#idTab798" class="comment_anchor">
                {/if}
                <img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/{$averagecomments|escape:'htmlall':'UTF-8'}stars.png" alt="rating" style="width:{$starsize|escape:'htmlall':'UTF-8'}px">
                <span style="width:150px; text-align:center; font-weight:bold;"></span></a>
            {/if}
            {if $comments > 1}
                {if $content_only}
                    <a href="{$productlink|escape:'htmlall':'UTF-8'}#idTab798" target="_top" class="comment_anchor">
                {else}
                    <a href="#idTab798" class="comment_anchor">
                {/if}
                <img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/{$averagecomments|escape:'htmlall':'UTF-8'}stars.png" alt="rating" style="width:{$starsize|escape:'htmlall':'UTF-8'}px">
                <span style="width:150px; text-align:center; font-weight:bold;">{l s='Read the' mod='lgcomments'} {$comments|escape:'htmlall':'UTF-8'} {l s='reviews' mod='lgcomments'}</span></a>
            {/if}
            {if $displaysnippets && $comments}
                <div id="googleRichSnippets" style="text-align:center;">
                <span itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating" style="text-align:center;">
                    {if $ratingscale == 5}
                        {l s='Average rating:' mod='lgcomments'} <span itemprop="ratingValue">{$averagecomments/2|escape:'quotes':'UTF-8'}</span>/<span itemprop="bestRating">5</span> -
                    {elseif $ratingscale == 10}
                        {l s='Average rating:' mod='lgcomments'} <span itemprop="ratingValue">{$averagecomments|escape:'quotes':'UTF-8'}</span>/<span itemprop="bestRating">10</span> -
                    {elseif $ratingscale == 20}
                        {l s='Average rating:' mod='lgcomments'} <span itemprop="ratingValue">{($averagecomments*2)|round:0|escape:'quotes':'UTF-8'}</span>/<span itemprop="bestRating">20</span> -
                    {else}
                        {l s='Average rating:' mod='lgcomments'} <span itemprop="ratingValue">{$averagecomments|escape:'quotes':'UTF-8'}</span>/<span itemprop="bestRating">10</span> -
                    {/if}
                    {l s='Number of reviews:' mod='lgcomments'}<span itemprop="ratingCount"> {$comments|escape:'htmlall':'UTF-8'}</span>
                </span>
                </div>
            {/if}
            {if $zerostar && !$comments}
            <div>
                <div style="display:table; margin: {$prodtopmargin|escape:'htmlall':'UTF-8'}px auto {$prodbotmargin|escape:'htmlall':'UTF-8'}px auto;">
                    <img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/0stars.png" alt="rating" style="width:{$starsize|escape:'htmlall':'UTF-8'}px">
                    <span style="width:150px; text-align:center; font-weight:bold;">{l s='No review at the moment' mod='lgcomments'}</span>
                </div>
            </div>
            {/if}
        </div>
    </div>

{/if}
