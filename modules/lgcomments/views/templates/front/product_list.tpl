{*
*  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
*
*  @author    Línea Gráfica E.C.E. S.L.
*  @copyright Lineagrafica.es - Línea Gráfica E.C.E. S.L. all rights reserved.
*  @license   https://www.lineagrafica.es/licenses/license_en.pdf https://www.lineagrafica.es/licenses/license_es.pdf https://www.lineagrafica.es/licenses/license_fr.pdf
*}

{if isset($comments) && $comments}
<div>
	<div style="display:table; margin: {$cattopmargin|escape:'htmlall':'UTF-8'}px auto {$catbotmargin|escape:'htmlall':'UTF-8'}px auto;">
		<a href="{$productlink|escape:'htmlall':'UTF-8'}#idTab798" class="comment_anchor">
        <img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/{$averagecomments|escape:'htmlall':'UTF-8'}stars.png" alt="rating" style="width:{$starsize|escape:'htmlall':'UTF-8'}px">
		{if $comments == 1}
		<span style="width:100px; text-align:center;">{l s='1 review' mod='lgcomments'}</span>
		{/if}
		{if $comments > 1}
		<span style="width:100px; text-align:center;">{$comments|escape:'htmlall':'UTF-8'} {l s='reviews' mod='lgcomments'}</span>
		{/if}
        </a>
	</div>
</div>
{/if}
{if $zerostar && !$comments}
<div>
	<div style="display:table; margin: {$cattopmargin|escape:'htmlall':'UTF-8'}px auto {$catbotmargin|escape:'htmlall':'UTF-8'}px auto;">
    <img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/0stars.png" alt="rating" style="width:{$starsize|escape:'htmlall':'UTF-8'}px">
    <span style="width:100px; text-align:center;">{l s='0 review' mod='lgcomments'}</span>
	</div>
</div>
{/if}
