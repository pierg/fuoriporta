{*
*  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
*
*  @author    Línea Gráfica E.C.E. S.L.
*  @copyright Lineagrafica.es - Línea Gráfica E.C.E. S.L. all rights reserved.
*  @license   https://www.lineagrafica.es/licenses/license_en.pdf https://www.lineagrafica.es/licenses/license_es.pdf https://www.lineagrafica.es/licenses/license_fr.pdf
*}

<script src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/js/store_reviews.js" type="text/javascript"></script>
<link href="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/css/store_reviews.css" rel="stylesheet" type="text/css" />
<style type="text/css">
{literal}
.lgcomment_block {
    background: none repeat scroll 0 0 {/literal}#{$storebackcolor|escape:'htmlall':'UTF-8'}{literal};
    color: {/literal}#{$storetextcolor|escape:'htmlall':'UTF-8'}{literal};
    border: 1px solid #cdcdcd;
    display: table;
    float: left;
    margin: 10px 2.5%;
    padding: 20px 25px;
	{/literal}
    {if $ps16}
	width: 45%;
    height:200px;
	{else}
	width: 90%;
	{/if}
	{literal}
	font-family:"Open Sans",sans-serif;
}
.lgcomment_summary {
    background: none repeat scroll 0 0 {/literal}#{$storebackcolor|escape:'htmlall':'UTF-8'}{literal};
    color: {/literal}#{$storetextcolor|escape:'htmlall':'UTF-8'}{literal};
    border: 2px solid #cdcdcd;
    display: table;
    margin: 10px 2.5%;
    padding: 20px 25px;
	font-family:"Open Sans",sans-serif;
	{/literal}
    {if $ps16}
	width: 95%;
	{else}
	width: 90%;
	{/if}
	{literal}
}
#lgcomment_summary_block1 {
	{/literal}
    {if $ps16}
	width:47%;
    margin-right:3%;
    float:left;
	{else}
	width: 100%;
    float:none;
	{/if}
	{literal}
}
#lgcomment_summary_block2 {
	{/literal}
    {if $ps16}
	width:47%;
    padding-left:3%;
    float:left;
    border-left: 1px solid #cdcdcd;
	{else}
	width: 100%;
    float:none;
    border-top: 1px solid #cdcdcd;
    margin-top:10px;
    padding-top:10px;
	{/if}
	{literal}
}
.lgcomment_button {
    padding: 15px;
    font-size: 16px;
    line-height: 18px;
    border: 1px solid #d6d4d4;
    background: none repeat scroll 0 0 {/literal}#{$storebackcolor|escape:'htmlall':'UTF-8'}{literal};
    color: {/literal}#{$storetextcolor|escape:'htmlall':'UTF-8'}{literal};
    border-radius:10px;
}
{/literal}
{if $ps16}
{literal}
@media (max-width: 768px) {
    .lgcomment_block{
    display:table;
    clear:both;
    float:none;
    width:95%;
}
#lgcomment_summary_block1 {
    width:100%;
    margin-right:0;
    float:none;
}
#lgcomment_summary_block2 {
    width:100%;
    padding-left:0;
    float:none;
    border-left:0;
    border-top: 1px solid #b8b8b8;
    margin-top:10px;
    padding-top:10px;

}
{/literal}
{/if}
</style>
<script type="text/javascript">
{literal}
jQuery(function(){
    var truncate = $('div.comment');
    var truncate2 = $('div.answer');
    truncate.each(function(){    
        var t = $(this).text();        
        if(t.length < 200) return;
        $(this).html(
            t.slice(0,200)+'<span>... </span><a href="#" class="more1"><b>({/literal}{l s='more' mod='lgcomments'}{literal})</b></a>'+
            '<span style="display:none;">'+ t.slice(200,t.length)+' <a href="#" class="less1"><b>({/literal}{l s='less' mod='lgcomments'}{literal})</b></a></span>'
        );
    });
    truncate2.each(function(){    
        var t = $(this).text();        
        if(t.length < 200) return;
        $(this).html(
            t.slice(0,200)+'<span>... </span><a href="#" class="more2"><b>({/literal}{l s='more' mod='lgcomments'}{literal})</b></a>'+
            '<span style="display:none;">'+ t.slice(200,t.length)+' <a href="#" class="less2"><b>({/literal}{l s='less' mod='lgcomments'}{literal})</b></a></span>'
        );
    });
    $('a.more1', truncate).click(function(event){
        event.preventDefault();
        $(this).hide().prev().hide();
        $(this).next().show();        
    });
    $('a.less1', truncate).click(function(event){
        event.preventDefault();
        $(this).parent().hide().prev().show().prev().show();    
    });
    $('a.more2', truncate2).click(function(event){
        event.preventDefault();
        $(this).hide().prev().hide();
        $(this).next().show();        
    });
    $('a.less2', truncate2).click(function(event){
        event.preventDefault();
        $(this).parent().hide().prev().show().prev().show();    
    });
});
{/literal}
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("a#send_store_review").fancybox({
            'href'   : '#form_review',
            'autoScale':'true'
        });
    });
    function checkFields()
    {
        if($('#score_store').val() == '' || $('#title_store').val() == '' || $('#comment_store').val() == '')
        {				
            alert('{l s='All the fields are mandatory' mod='lgcomments'}');
        }
        else
        {				
            sendStoreReview("{$link->getModuleLink('lgcomments', 'reviews', ['action'=>'sendReview', 'ajax' => '1'], $is_https)|escape:'quotes':'UTF-8'}");
            $.fancybox.close();
            var myalert = "{l s='The review has been correctly sent' mod='lgcomments'}";
            alert(myalert);
            location.reload();
        }
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
    
        $("a.iframe").fancybox({
            'type' : 'iframe',
            'width':600,
            'height':600
        });
    });
    $('#cerrar_enviado').click(function(){
        $.fancybox.close;
    });
    function abrir(url) {
        open(url,'','top=300,left=300,width=600,height=600,scrollbars=yes') ;
    }
</script>
<script type="text/javascript">
    function changestars(value1,value2)
    {
        $('#'+value2).attr('src','{$modules_dir|escape:'htmlall':'UTF-8'}lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/'+value1+'stars.png');
    }
</script>
        
{capture name=path}{l s='Customer reviews' mod='lgcomments'}{/capture}
<div class="lgcomments_container clear clearfix row">
    <h1 class="page-heading">{l s='Customer reviews about' mod='lgcomments'}  {$shop_name|escape:'htmlall':'UTF-8'}</h1>
    {if $storefilter and $numerocomentarios}
    <div class="lgcomment_summary">
        <div id="lgcomment_summary_block1">
            <table width="100%">
                <tr>
                    <td colspan="4" style="text-align:center;padding-bottom:5px;" class="lgcomment_summary_title">{l s='Summary' mod='lgcomments'}</td>
                </tr>
                <tr>
                    <td><img class="logo" src="{$logo_url|escape:'htmlall':'UTF-8'}" alt="{$shop_name|escape:'html':'UTF-8'}" width="100"></td>
                    <td><img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/positive.png" alt="positive" width="25"></td>
                    <td><b>{(($fivestars+$fourstars)/$numerocomentarios*100)|round:1|escape:'htmlall':'UTF-8'}%</b></td>
                    <td>({($fivestars+$fourstars)|escape:'htmlall':'UTF-8'} {l s='reviews' mod='lgcomments'})</td>
                </tr>
                <tr>
                    {if $ratingscale == 5}
                        <td>{l s='Average rating:' mod='lgcomments'} <b>{$mediacomentarios2/2|escape:'htmlall':'UTF-8'}/5</b></td>
                    {elseif $ratingscale == 10}
                        <td>{l s='Average rating:' mod='lgcomments'} <b>{$mediacomentarios2|escape:'htmlall':'UTF-8'}/10</b></td>
                    {elseif $ratingscale == 20}
                        <td>{l s='Average rating:' mod='lgcomments'} <b>{($mediacomentarios2*2)|round:0|escape:'htmlall':'UTF-8'}/20</b></td>
                    {else}
                        <td>{l s='Average rating:' mod='lgcomments'} <b>{$mediacomentarios2|escape:'htmlall':'UTF-8'}/10</b></td>
                    {/if}
                    <td><img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/neutral.png" alt="neutral" width="25"></td>
                    <td><b>{(($threestars+$twostars)/$numerocomentarios*100)|round:1|escape:'htmlall':'UTF-8'}%</b></td>
                    <td>({($threestars+$twostars)|escape:'htmlall':'UTF-8'} {l s='reviews' mod='lgcomments'})</td>
                </tr>
                <tr>
                    <td>{l s='Number of reviews:' mod='lgcomments'} {$numerocomentarios|escape:'htmlall':'UTF-8'}</td>
                    <td><img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/negative.png" alt="negative" width="25"></td>
                    <td><b>{(($onestar+$zerostar)/$numerocomentarios*100)|round:1|escape:'htmlall':'UTF-8'}%</b></td>
                    <td>({($onestar+$zerostar)|escape:'htmlall':'UTF-8'} {l s='reviews' mod='lgcomments'})</td>
                </tr>
            </table>
        </div>
        <div id="lgcomment_summary_block2">
            <table width="100%">
                <tr>
                    <td colspan="2" style="text-align:center;padding-bottom:5px;">
                        <span class="lgcomment_summary_title">{l s='Filter reviews' mod='lgcomments'}</span>
                        &nbsp;<a href="{$link->getModuleLink('lgcomments','reviews')|escape:'htmlall':'UTF-8'}">(<i class="icon-refresh"></i>&nbsp;{l s='Reset' mod='lgcomments'}</a>)
                    </td>
                </tr><tr>
                    <td>
                        <a href="{$link->getModuleLink('lgcomments','reviews')|escape:'htmlall':'UTF-8'}?star=five" {if $fivestars == 0}style="pointer-events: none;"{/if}>
                        <img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/10stars.png" alt="five stars" style="width:{$starsize|escape:'htmlall':'UTF-8'}px">
                        ({$fivestars|escape:'htmlall':'UTF-8'})
                        </a>
                    </td>
                    <td>
                        <a href="{$link->getModuleLink('lgcomments','reviews')|escape:'htmlall':'UTF-8'}?star=four" {if $fourstars == 0}style="pointer-events: none;"{/if}>
                        <img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/8stars.png" alt="four stars" style="width:{$starsize|escape:'htmlall':'UTF-8'}px">
                        ({$fourstars|escape:'htmlall':'UTF-8'})
                        </a>
                    </td>
                </tr><tr>
                    <td>
                        <a href="{$link->getModuleLink('lgcomments','reviews')|escape:'htmlall':'UTF-8'}?star=three" {if $threestars == 0}style="pointer-events: none;"{/if}>
                        <img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/6stars.png" alt="three stars" style="width:{$starsize|escape:'htmlall':'UTF-8'}px">
                        ({$threestars|escape:'htmlall':'UTF-8'})
                        </a>
                    </td>
                    <td>
                        <a href="{$link->getModuleLink('lgcomments','reviews')|escape:'htmlall':'UTF-8'}?star=two" {if $twostars == 0}style="pointer-events: none;"{/if}>
                        <img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/4stars.png" alt="two stars" style="width:{$starsize|escape:'htmlall':'UTF-8'}px">
                        ({$twostars|escape:'htmlall':'UTF-8'})
                        </a>
                    </td>
                </tr><tr>
                    <td>
                        <a href="{$link->getModuleLink('lgcomments','reviews')|escape:'htmlall':'UTF-8'}?star=one" {if $onestar == 0}style="pointer-events: none;"{/if}>
                        <img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/2stars.png" alt="one star" style="width:{$starsize|escape:'htmlall':'UTF-8'}px">
                        ({$onestar|escape:'htmlall':'UTF-8'})
                        </a>
                    </td>
                    <td>
                        <a href="{$link->getModuleLink('lgcomments','reviews')|escape:'htmlall':'UTF-8'}?star=zero" {if $zerostar == 0}style="pointer-events: none;"{/if}>
                        <img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/0stars.png" alt="zero star" style="width:{$starsize|escape:'htmlall':'UTF-8'}px">
                        ({$zerostar|escape:'htmlall':'UTF-8'})
                        </a>
                    </td>
                </tr>
            </table>   
        </div>
    </div>
    {/if}
    <div class="clear clearfix"></div><br>

    {if $storeform}
        <p style="text-align:center;">
            <a id="send_store_review" href="#form_review" class="lgcomment_button">
                <i class="icon-pencil"></i> {l s='Click here to leave a review' mod='lgcomments'}
            </a>
        </p><br>
    {/if}

    {if empty($smarty.get.star)}
        {counter start=0 assign=commentCounter}
        {foreach from=$reviewsall item=lgcomments}{counter}
            <div class="lgcomment_block" >
                <div class="title">{stripslashes($lgcomments.title|truncate:'50':'...'|escape:'quotes':'UTF-8')}</div>
                <div class="rating_img"><img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/{$lgcomments.stars|escape:'htmlall':'UTF-8'}stars.png" alt="rating" style="width:{$starsize|escape:'htmlall':'UTF-8'}px" ></div>
                <div class="comment">{nl2br(stripslashes($lgcomments.comment|escape:'quotes':'UTF-8'))}</div>
                <div class="credits">
                    <span class="name">{$lgcomments.customer|escape:'quotes':'UTF-8'}</span><span class="date">{$lgcomments.date|date_format:"$dateformat"|escape:'quotes':'UTF-8'}</span>
                    <div class="clear clearfix"></div>
                </div>
                {if $lgcomments.answer and $lgcomments.answer != '<p>0</p>'}
                    <div class="answer"><b>{l s='Answer:' mod='lgcomments'}</b> {nl2br(stripslashes($lgcomments.answer|escape:'quotes':'UTF-8'))}</div>
                {/if}
            </div>
            {if $commentCounter % 2 == 0}
                <div class="lgcomment_linebreak"></div>
            {/if}
        {/foreach}
    {else}
        {if $smarty.get.star == five}
            {counter start=0 assign=commentCounter}
            {foreach from=$reviews5 item=lgcomments}{counter}
                <div class="lgcomment_block" >
                    <div class="title">{stripslashes($lgcomments.title|truncate:'50':'...'|escape:'quotes':'UTF-8')}</div>
                    <div class="rating_img"><img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/{$lgcomments.stars|escape:'htmlall':'UTF-8'}stars.png" alt="rating" style="width:{$starsize|escape:'htmlall':'UTF-8'}px"></div>
                    <div class="comment">{nl2br(stripslashes($lgcomments.comment|escape:'quotes':'UTF-8'))}</div>
                    <div class="credits">
                        <span class="name">{$lgcomments.customer|escape:'quotes':'UTF-8'}</span><span class="date">{$lgcomments.date|date_format:"$dateformat"|escape:'quotes':'UTF-8'}</span>
                        <div class="clear clearfix"></div>
                    </div>
                    {if $lgcomments.answer}
                        <div class="answer"><b>{l s='Answer:' mod='lgcomments'}</b> {nl2br(stripslashes($lgcomments.answer|escape:'quotes':'UTF-8'))}</div>
                    {/if}
                </div>
                {if $commentCounter % 2 == 0}
                    <div class="lgcomment_linebreak"></div>
                {/if}
            {/foreach}
        {/if}
        {if $smarty.get.star == four}
            {counter start=0 assign=commentCounter}
            {foreach from=$reviews4 item=lgcomments}{counter}
                <div class="lgcomment_block" >
                    <div class="title">{stripslashes($lgcomments.title|truncate:'50':'...'|escape:'quotes':'UTF-8')}</div>
                    <div class="rating_img"><img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/{$lgcomments.stars|escape:'htmlall':'UTF-8'}stars.png" alt="rating" style="width:{$starsize|escape:'htmlall':'UTF-8'}px"></div>
                    <div class="comment">{nl2br(stripslashes($lgcomments.comment|escape:'quotes':'UTF-8'))}</div>
                    <div class="credits">
                        <span class="name">{$lgcomments.customer|escape:'quotes':'UTF-8'}</span><span class="date">{$lgcomments.date|date_format:"$dateformat"|escape:'quotes':'UTF-8'}</span>
                        <div class="clear clearfix"></div>
                    </div>
                    {if $lgcomments.answer}
                        <div class="answer"><b>{l s='Answer:' mod='lgcomments'}</b> {nl2br(stripslashes($lgcomments.answer|escape:'quotes':'UTF-8'))}</div>
                    {/if}
                </div>
                {if $commentCounter % 2 == 0}
                    <div class="lgcomment_linebreak"></div>
                {/if}
            {/foreach}
        {/if}
        {if $smarty.get.star == three}
            {counter start=0 assign=commentCounter}
            {foreach from=$reviews3 item=lgcomments}{counter}
                <div class="lgcomment_block" >
                    <div class="title">{stripslashes($lgcomments.title|truncate:'50':'...'|escape:'quotes':'UTF-8')}</div>
                    <div class="rating_img"><img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/{$lgcomments.stars|escape:'htmlall':'UTF-8'}stars.png" alt="rating" style="width:{$starsize|escape:'htmlall':'UTF-8'}px"></div>
                    <div class="comment">{nl2br(stripslashes($lgcomments.comment|escape:'quotes':'UTF-8'))}</div>
                    <div class="credits">
                        <span class="name">{$lgcomments.customer|escape:'quotes':'UTF-8'}</span><span class="date">{$lgcomments.date|date_format:"$dateformat"|escape:'quotes':'UTF-8'}</span>
                        <div class="clear clearfix"></div>
                    </div>
                    {if $lgcomments.answer}
                        <div class="answer"><b>{l s='Answer:' mod='lgcomments'}</b> {nl2br(stripslashes($lgcomments.answer|escape:'quotes':'UTF-8'))}</div>
                    {/if}
                </div>
                {if $commentCounter % 2 == 0}
                    <div class="lgcomment_linebreak"></div>
                {/if}
            {/foreach}
        {/if}
        {if $smarty.get.star == two}
            {counter start=0 assign=commentCounter}
            {foreach from=$reviews2 item=lgcomments}{counter}
                <div class="lgcomment_block" >
                    <div class="title">{stripslashes($lgcomments.title|truncate:'50':'...'|escape:'quotes':'UTF-8')}</div>
                    <div class="rating_img"><img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/{$lgcomments.stars|escape:'htmlall':'UTF-8'}stars.png" alt="rating" style="width:{$starsize|escape:'htmlall':'UTF-8'}px"></div>
                    <div class="comment">{nl2br(stripslashes($lgcomments.comment|escape:'quotes':'UTF-8'))}</div>
                    <div class="credits">
                        <span class="name">{$lgcomments.customer|escape:'quotes':'UTF-8'}</span><span class="date">{$lgcomments.date|date_format:"$dateformat"|escape:'quotes':'UTF-8'}</span>
                        <div class="clear clearfix"></div>
                    </div>
                    {if $lgcomments.answer}
                        <div class="answer"><b>{l s='Answer:' mod='lgcomments'}</b> {nl2br(stripslashes($lgcomments.answer|escape:'quotes':'UTF-8'))}</div>
                    {/if}
                </div>
                {if $commentCounter % 2 == 0}
                    <div class="lgcomment_linebreak"></div>
                {/if}
            {/foreach}
        {/if}
        {if $smarty.get.star == one}
            {counter start=0 assign=commentCounter}
            {foreach from=$reviews1 item=lgcomments}{counter}
                <div class="lgcomment_block" >
                    <div class="title">{stripslashes($lgcomments.title|truncate:'50':'...'|escape:'quotes':'UTF-8')}</div>
                    <div class="rating_img"><img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/{$lgcomments.stars|escape:'htmlall':'UTF-8'}stars.png" alt="rating" style="width:{$starsize|escape:'htmlall':'UTF-8'}px"></div>
                    <div class="comment">{nl2br(stripslashes($lgcomments.comment|escape:'quotes':'UTF-8'))}</div>
                    <div class="credits">
                        <span class="name">{$lgcomments.customer|escape:'quotes':'UTF-8'}</span><span class="date">{$lgcomments.date|date_format:"$dateformat"|escape:'quotes':'UTF-8'}</span>
                        <div class="clear clearfix"></div>
                    </div>
                    {if $lgcomments.answer}
                        <div class="answer"><b>{l s='Answer:' mod='lgcomments'}</b> {nl2br(stripslashes($lgcomments.answer|escape:'quotes':'UTF-8'))}</div>
                    {/if}
                </div>
                {if $commentCounter % 2 == 0}
                    <div class="lgcomment_linebreak"></div>
                {/if}
            {/foreach}
        {/if}
        {if $smarty.get.star == zero}
            {counter start=0 assign=commentCounter}
            {foreach from=$reviews0 item=lgcomments}{counter}
                <div class="lgcomment_block" >
                    <div class="title">{stripslashes($lgcomments.title|truncate:'50':'...'|escape:'quotes':'UTF-8')}</div>
                    <div class="rating_img"><img src="{$content_dir|escape:'htmlall':'UTF-8'}modules/lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/{$lgcomments.stars|escape:'htmlall':'UTF-8'}stars.png" alt="rating" style="width:{$starsize|escape:'htmlall':'UTF-8'}px"></div>
                    <div class="comment">{nl2br(stripslashes($lgcomments.comment|escape:'quotes':'UTF-8'))}</div>
                    <div class="credits">
                        <span class="name">{$lgcomments.customer|escape:'quotes':'UTF-8'}</span><span class="date">{$lgcomments.date|date_format:"$dateformat"|escape:'quotes':'UTF-8'}</span>
                        <div class="clear clearfix"></div>
                    </div>
                    {if $lgcomments.answer}
                        <div class="answer"><b>{l s='Answer:' mod='lgcomments'}</b> {nl2br(stripslashes($lgcomments.answer|escape:'quotes':'UTF-8'))}</div>
                    {/if}
                </div>
                {if $commentCounter % 2 == 0}
                    <div class="lgcomment_linebreak"></div>
                {/if}
            {/foreach}
        {/if}
    {/if}

    <div class="clear clearfix row"></div>
    <div class="row">
    {if $displaysnippets && $lgcomments}
        <div itemscope itemtype="http://schema.org/Organization" style="text-align:center;margin:25px 0;">
            <span itemprop="name">{$shop_name|escape:'quotes':'UTF-8'}</span> - 
        <span itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
        {if $ratingscale == 5}
            {l s='Average rating:' mod='lgcomments'} <span itemprop="ratingValue">{$mediacomentarios2/2|escape:'quotes':'UTF-8'}</span>/<span itemprop="bestRating">5</span> -
        {elseif $ratingscale == 10}
            {l s='Average rating:' mod='lgcomments'} <span itemprop="ratingValue">{$mediacomentarios2|escape:'quotes':'UTF-8'}</span>/<span itemprop="bestRating">10</span> -
        {elseif $ratingscale == 20}
            {l s='Average rating:' mod='lgcomments'} <span itemprop="ratingValue">{($mediacomentarios2*2)|round:0|escape:'quotes':'UTF-8'}</span>/<span itemprop="bestRating">20</span> -
        {else}
            {l s='Average rating:' mod='lgcomments'} <span itemprop="ratingValue">{$mediacomentarios2|escape:'quotes':'UTF-8'}</span>/<span itemprop="bestRating">10</span> -
        {/if}
        {l s='Number of reviews:' mod='lgcomments'} <span itemprop="ratingCount">{$numerocomentarios|escape:'quotes':'UTF-8'}</span>
        </span>
        </div>
    {/if}
    </div>
    
    <div class="clear clearfix row">
        <div class="content_sortPagiBar clearfix">
            <div class="bottom-pagination-content clearfix">
            {if empty($smarty.get.star)}
                {assign var="pages_nb" value="{($allstars/$reviewsbypage)|escape:'htmlall':'UTF-8'}"}
                {assign var="stop" value="{($allstars/$reviewsbypage)|ceil|escape:'htmlall':'UTF-8'}"}
                {if ({$reviewsbypage}-{$allstars}) >= 0}
                    {assign var="p" value="0"}
                {/if}
            {else}
                {if $smarty.get.star == five}
                {assign var="pages_nb" value="{($fivestars/$reviewsbypage)|escape:'htmlall':'UTF-8'}"}
                {assign var="stop" value="{($fivestars/$reviewsbypage)|ceil|escape:'htmlall':'UTF-8'}"}
                    {if ({$reviewsbypage}-{$fivestars}) >= 0}
                        {assign var="p" value="0"}
                    {/if}
                {/if}
                {if $smarty.get.star == four}
                {assign var="pages_nb" value="{($fourstars/$reviewsbypage)|escape:'htmlall':'UTF-8'}"}
                {assign var="stop" value="{($fourstars/$reviewsbypage)|ceil|escape:'htmlall':'UTF-8'}"}
                    {if ({$reviewsbypage}-{$fourstars}) >= 0}
                        {assign var="p" value="0"}
                    {/if}
                {/if}
                {if $smarty.get.star == three}
                {assign var="pages_nb" value="{($threestars/$reviewsbypage)|escape:'htmlall':'UTF-8'}"}
                {assign var="stop" value="{($threestars/$reviewsbypage)|ceil|escape:'htmlall':'UTF-8'}"}
                    {if ({$reviewsbypage}-{$threestars}) >= 0}
                        {assign var="p" value="0"}
                    {/if}
                {/if}
                {if $smarty.get.star == two}
                {assign var="pages_nb" value="{($twostars/$reviewsbypage)|escape:'htmlall':'UTF-8'}"}
                {assign var="stop" value="{($twostars/$reviewsbypage)|ceil|escape:'htmlall':'UTF-8'}"}
                    {if ({$reviewsbypage}-{$twostars}) >= 0}
                        {assign var="p" value="0"}
                    {/if}
                {/if}
                {if $smarty.get.star == one}
                {assign var="pages_nb" value="{($onestar/$reviewsbypage)|escape:'htmlall':'UTF-8'}"}
                {assign var="stop" value="{($onestar/$reviewsbypage)|ceil|escape:'htmlall':'UTF-8'}"}
                    {if ({$reviewsbypage}-{$onestar}) >= 0}
                        {assign var="p" value="0"}
                    {/if}
                {/if}
                {if $smarty.get.star == zero}
                {assign var="pages_nb" value="{($zerostar/$reviewsbypage)|escape:'htmlall':'UTF-8'}"}
                {assign var="stop" value="{($onestar/$reviewsbypage)|ceil|escape:'htmlall':'UTF-8'}"}
                    {if ({$reviewsbypage}-{$zerostar}) >= 0}
                        {assign var="p" value="0"}
                    {/if}
                {/if}
            {/if}
            {include file="$tpl_dir./pagination.tpl"}
            </div>
        </div>
    </div>
    
    <div style="display:none">
        <div id="form_review" class="ps16">
            {if !$logged}
                <p style="font-size:16px; padding:20px; text-align:center;">
                    <a href="{$link->getPageLink('authentication', true)|escape:'htmlall':'UTF-8'}?back={if $smarty.server.SERVER_PROTOCOL|strstr:'https'}https://{else}http://{/if}{$smarty.server.HTTP_HOST|escape:'htmlall':'UTF-8'}{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}">
                        <i class="icon-sign-in"></i><br>{l s='You need to be logged in to be able to leave a review' mod='lgcomments'}
                    </a>
                </p>
            {elseif $alreadyreviewed}
                <p style="font-size:16px; padding:20px; text-align:center;"><br>{l s='You have already written a review about the shop' mod='lgcomments'}</p>
            {else}
                <h3>{l s='Write a review' mod='lgcomments'}</h3><br>
                <div class="bloque_form">
                    <div style="float:left;">
                        <select name="score_store" id="score_store" onchange="changestars(this.value,'stars_store');">
                            <option value="0">{if $ratingscale == 5}0/5{elseif $ratingscale == 10}0/10{elseif $ratingscale == 20}0/20{else}0/10{/if}</option>
                            <option value="1">{if $ratingscale == 5}0,5/5{elseif $ratingscale == 10}1/10{elseif $ratingscale == 20}2/20{else}1/10{/if}</option>
                            <option value="2">{if $ratingscale == 5}1/5{elseif $ratingscale == 10}2/10{elseif $ratingscale == 20}4/20{else}2/10{/if}</option>
                            <option value="3">{if $ratingscale == 5}1,5/5{elseif $ratingscale == 10}3/10{elseif $ratingscale == 20}6/20{else}3/10{/if}</option>
                            <option value="4">{if $ratingscale == 5}2/5{elseif $ratingscale == 10}4/10{elseif $ratingscale == 20}8/20{else}4/10{/if}</option>
                            <option value="5">{if $ratingscale == 5}2,5/5{elseif $ratingscale == 10}5/10{elseif $ratingscale == 20}10/20{else}5/10{/if}</option>
                            <option value="6">{if $ratingscale == 5}3/5{elseif $ratingscale == 10}6/10{elseif $ratingscale == 20}12/20{else}6/10{/if}</option>
                            <option value="7">{if $ratingscale == 5}3,5/5{elseif $ratingscale == 10}7/10{elseif $ratingscale == 20}14/20{else}7/10{/if}</option>
                            <option value="8">{if $ratingscale == 5}4/5{elseif $ratingscale == 10}8/10{elseif $ratingscale == 20}16/20{else}8/10{/if}</option>
                            <option value="9">{if $ratingscale == 5}4,5/5{elseif $ratingscale == 10}9/10{elseif $ratingscale == 20}18/20{else}9/10{/if}</option>
                            <option value="10" selected>{if $ratingscale == 5}5/5{elseif $ratingscale == 10}10/10{elseif $ratingscale == 20}20/20{else}10/10{/if}</option>
                        </select>
                    </div>
                    <div style="float:left;"><img style="width:{$starsize|escape:'htmlall':'UTF-8'}px" src="{$modules_dir|escape:'htmlall':'UTF-8'}lgcomments/views/img/stars/{$starstyle|escape:'htmlall':'UTF-8'}/{$starcolor|escape:'htmlall':'UTF-8'}/10stars.png" id="stars_store" alt="rating"></div>
                    <div style="clear:both;"></div>
                </div><br>
                <div class="bloque_form">
                    <label>{l s='Title:' mod='lgcomments'} </label><textarea maxlength="50" id="title_store" name="title_store" style="width:100%;height:25px;" required></textarea>
                </div><br>
                <div class="bloque_form">
                    <label>{l s='Comment:' mod='lgcomments'} </label><textarea id="comment_store" name="comment_store" style="width:100%;height:100px;" required></textarea>
                </div>
                <input type="hidden" name="isocode" id="isocode" value="{$lang_iso|escape:'htmlall':'UTF-8'}"/>
                <input type="hidden" name="customerid" id="customerid" value="{$cookie->id_customer|escape:'htmlall':'UTF-8'}"/>
                <div class="clear"></div><br>
                <div><a id="submit_review" onclick="checkFields();" class="btn btn-default button button-small"><span>{l s='Send' mod='lgcomments'}</span></a></div>
            {/if}
        </div>
    </div>
</div>
