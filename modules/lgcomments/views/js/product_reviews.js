/**
*  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
*  
*  @author    Línea Gráfica E.C.E. S.L.
*  @copyright Lineagrafica.es - Línea Gráfica E.C.E. S.L. all rights reserved.
*  @license   https://www.lineagrafica.es/licenses/license_en.pdf https://www.lineagrafica.es/licenses/license_es.pdf https://www.lineagrafica.es/licenses/license_fr.pdf
*/

function sendProductReview(file)
{   //alert(file);
	score_product = $('#score_product').val();
	title_product = $('#title_product').val();
    comment_product = $('#comment_product').val().split("\n").join(" ");
    isocode = $('#isocode').val();
    customerid = $('#customerid').val();
    $.ajax({
        type: "POST",
        url: file,
        data: {'score_product': score_product, 'title_product': title_product, 'comment_product': comment_product, 'isocode': isocode, 'customerid': customerid, 'id_product': id_product, 'send_product_review' : true},
    }).done(function(status, data) {
        $( this ).addClass( "done" );
        if (typeof(data.status) != 'undefined' && data.status == 'ok')
        {
            $('#score_product').val('');
            $('#title_product').val('');
            $('#comment_product').val('');
            $('#isocode').val('');
            $('#customerid').val('');
        }
    });
}

$(function(){
    $('a[href=#five-stars]').click(function(){
        $('table.five-stars').css('display', 'table');
        $('table.four-stars').css('display', 'none');
        $('table.three-stars').css('display', 'none');
        $('table.two-stars').css('display', 'none');
        $('table.one-star').css('display', 'none');
        $('table.zero-star').css('display', 'none');
        $('div#more_less').css('display', 'none');
    });
});
$(function(){
    $('a[href=#four-stars]').click(function(){
        $('table.five-stars').css('display', 'none');
        $('table.four-stars').css('display', 'table');
        $('table.three-stars').css('display', 'none');
        $('table.two-stars').css('display', 'none');
        $('table.one-star').css('display', 'none');
        $('table.zero-star').css('display', 'none');
        $('div#more_less').css('display', 'none');
    });
});
$(function(){
    $('a[href=#three-stars]').click(function(){
        $('table.five-stars').css('display', 'none');
        $('table.four-stars').css('display', 'none');
        $('table.three-stars').css('display', 'table');
        $('table.two-stars').css('display', 'none');
        $('table.one-star').css('display', 'none');
        $('table.zero-star').css('display', 'none');
        $('div#more_less').css('display', 'none');
    });
});
$(function(){
    $('a[href=#two-stars]').click(function(){
        $('table.five-stars').css('display', 'none');
        $('table.four-stars').css('display', 'none');
        $('table.three-stars').css('display', 'none');
        $('table.two-stars').css('display', 'table');
        $('table.one-star').css('display', 'none');
        $('table.zero-star').css('display', 'none');
        $('div#more_less').css('display', 'none');
    });
});
$(function(){
    $('a[href=#one-star]').click(function(){
        $('table.five-stars').css('display', 'none');
        $('table.four-stars').css('display', 'none');
        $('table.three-stars').css('display', 'none');
        $('table.two-stars').css('display', 'none');
        $('table.one-star').css('display', 'table');
        $('table.zero-star').css('display', 'none');
        $('div#more_less').css('display', 'none');
    });
});
$(function(){
    $('a[href=#zero-star]').click(function(){
        $('table.five-stars').css('display', 'none');
        $('table.four-stars').css('display', 'none');
        $('table.three-stars').css('display', 'none');
        $('table.two-stars').css('display', 'none');
        $('table.one-star').css('display', 'none');
        $('table.zero-star').css('display', 'table');
        $('div#more_less').css('display', 'none');
    });
});
