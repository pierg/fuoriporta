/**
*  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
*  
*  @author    Línea Gráfica E.C.E. S.L.
*  @copyright Lineagrafica.es - Línea Gráfica E.C.E. S.L. all rights reserved.
*  @license   https://www.lineagrafica.es/licenses/license_en.pdf https://www.lineagrafica.es/licenses/license_es.pdf https://www.lineagrafica.es/licenses/license_fr.pdf
*/

function sendStoreReview(file)
{   //alert(file);
	score_store = $('#score_store').val();
	title_store = $('#title_store').val();
    comment_store = $('#comment_store').val().split("\n").join(" ");
    isocode = $('#isocode').val();
    customerid = $('#customerid').val();
    $.ajax({
        type: "POST",
        url: file,
        data: {'score_store': score_store, 'title_store': title_store, 'comment_store': comment_store, 'isocode': isocode, 'customerid': customerid, 'send_store_review' : true},
    }).done(function(status, data) {
        $( this ).addClass( "done" );
        if (typeof(data.status) != 'undefined' && data.status == 'ok')
        {
            $('#score_store').val('');
            $('#title_store').val('');
            $('#comment_store').val('');
            $('#isocode').val('');
            $('#customerid').val('');
        }
    });
}
