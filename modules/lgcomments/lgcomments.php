<?php
/**
 *  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
 *
 * @author    Línea Gráfica E.C.E. S.L.
 * @copyright Lineagrafica.es - Línea Gráfica E.C.E. S.L. all rights reserved.
 * @license   https://www.lineagrafica.es/licenses/license_en.pdf https://www.lineagrafica.es/licenses/license_es.pdf https://www.lineagrafica.es/licenses/license_fr.pdf
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class LGComments extends Module
{
    public $bootstrap;

    public function __construct()
    {
        $this->name = 'lgcomments';
        $this->tab = 'advertising_marketing';
        $this->version = '1.5.9';
        $this->author = 'Línea Gráfica';
        $this->need_instance = 0;
        $this->module_key = '7a311a563a0daa4a8636f6a5ec27c0e6';
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Store Reviews, Product Reviews and Google Rich Snippets');
        $this->description = $this->l('Get your own system of reviews about your store and products.');
        if (self::isInstalled($this->name)) {
            $this->id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
            $this->iso_lang = pSQL(Language::getIsoById($this->id_lang));
        }
        $this->initContext();
    }

    /* Retrocompatibility 1.4/1.5 */
    private function initContext()
    {
        $this->context = Context::getContext();
    }

    public function install()
    {
        if (
            !parent::install()
            || !$this->registerHook('footer')
            || !$this->registerHook('productTab')
            || !$this->registerHook('productTabContent')
            || !$this->registerHook('extraRight')
            || !$this->registerHook('displayProductListReviews')
            || !$this->registerHook('displayHome')
            || !$this->registerHook('displayCustomerAccount')
        ) {
            return false;
        }
        if (substr_count(_PS_VERSION_, '1.6') > 0) {
            $tab_type = 1;
        } else {
            $tab_type = 2;
        }
        $queries = array(
            'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'lgcomments_orders` (
				  `id_order` int(11) NOT NULL,
				  `id_customer` int(11) NOT NULL,
				  `date_email` datetime NOT NULL,
				  `hash` varchar(100) NOT NULL,
				  `voted` int(11) NOT NULL,
                  `sent` int(11) NOT NULL,
                  `date_email2` datetime NOT NULL,
				  UNIQUE KEY `id_order` (`id_order`),
				  KEY `id_customer` (`id_customer`,`hash`,`voted`)
				) ENGINE='.(defined('ENGINE_TYPE') ? ENGINE_TYPE : 'Innodb').' CHARSET=utf8',
            'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'lgcomments_status` (
				  `id_order_status` int(11) NOT NULL AUTO_INCREMENT,
				  PRIMARY KEY (`id_order_status`)
				  )
				 ENGINE='.(defined('ENGINE_TYPE') ? ENGINE_TYPE : 'Innodb'),
            'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'lgcomments_productcomments` (
				  `date` date NOT NULL,
				  `id_customer` int(11) NOT NULL,
				  `id_product` int(11) NOT NULL,
				  `stars` int(11) NOT NULL,
				  `comment` text NOT NULL,
				  `id_lang` int(11) NOT NULL,
				  `active` int(1) NOT NULL,
				  `position` int(11) NOT NULL,
				  `title` text NOT NULL,
                  `answer` text,
				  `id_lgcomments_productcomments` int(11) NOT NULL AUTO_INCREMENT,
				  PRIMARY KEY (`id_lgcomments_productcomments`),
				  KEY `date` (`date`,`id_customer`,`id_product`,`stars`,`id_lang`,`active`,`position`)
				  )
				  ENGINE='.(defined('ENGINE_TYPE') ? ENGINE_TYPE : 'Innodb').' CHARSET=utf8',
            'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'lgcomments_storecomments` (
				  `date` date NOT NULL,
				  `id_customer` int(11) NOT NULL,
				  `id_order` int(11) NOT NULL,
				  `stars` int(11) NOT NULL,
				  `comment` text NOT NULL,
				  `id_lang` int(11) NOT NULL,
				  `active` int(1) NOT NULL,
				  `position` int(11) NOT NULL,
				  `title` text NOT NULL,
				  `answer` text,
				  `id_lgcomments_storecomments` int(11) NOT NULL AUTO_INCREMENT,
				  PRIMARY KEY (`id_lgcomments_storecomments`),
				  KEY `date` (`date`,`id_customer`,`id_order`,`stars`,`id_lang`,`active`,`position`)
				  )
				  ENGINE='.(defined('ENGINE_TYPE') ? ENGINE_TYPE : 'Innodb').' CHARSET=utf8',
            'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'lgcomments_customergroups` (
				  `id_customer_group` int(11) NOT NULL AUTO_INCREMENT,
				  PRIMARY KEY (`id_customer_group`)
				  )
				 ENGINE='.(defined('ENGINE_TYPE') ? ENGINE_TYPE : 'Innodb'),
            'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'lgcomments_multistore` (
				  `id_shop` int(11) NOT NULL AUTO_INCREMENT,
				  PRIMARY KEY (`id_shop`)
				  )
				 ENGINE='.(defined('ENGINE_TYPE') ? ENGINE_TYPE : 'Innodb'),
        );

        foreach ($queries as $query) {
            if (!Db::getInstance()->Execute($query)) {
                parent::uninstall();

                return false;
            }
        }
        $default_email = Configuration::get('PS_SHOP_EMAIL');
        /* Default value */
        if (!Configuration::updateValue('PS_LGCOMMENTS_DISPLAY', '1')
            || !Configuration::updateValue('PS_LGCOMMENTS_DISPLAY_TYPE', '1')
            || !Configuration::updateValue('PS_LGCOMMENTS_DISPLAY_SIDE', '5')
            || !Configuration::updateValue('PS_LGCOMMENTS_DISPLAY_LANGUAGE', '0')
            || !Configuration::updateValue('PS_LGCOMMENTS_PER_PAGE', '20')
            || !Configuration::updateValue('PS_LGCOMMENTS_TEXTCOLOR', '777777')
            || !Configuration::updateValue('PS_LGCOMMENTS_TEXTCOLOR2', '777777')
            || !Configuration::updateValue('PS_LGCOMMENTS_BACKCOLOR2', 'FBFBFB')
            || !Configuration::updateValue('PS_LGCOMMENTS_DISPLAY_COMMENTS', '1')
            || !Configuration::updateValue('PS_LGCOMMENTS_DISPLAY_DEFAULT', '3')
            || !Configuration::updateValue('PS_LGCOMMENTS_DISPLAY_MORE', '10')
            || !Configuration::updateValue('PS_LGCOMMENTS_DISPLAY_ZEROSTAR', '0')
            || !Configuration::updateValue('PS_LGCOMMENTS_DISPLAY_LANGUAGE2', '0')
            || !Configuration::updateValue('PS_LGCOMMENTS_DISPLAY_SNIPPETS', '0')
            || !Configuration::updateValue('PS_LGCOMMENTS_DISPLAY_SNIPPETS2', '0')
            || !Configuration::updateValue('PS_LGCOMMENTS_PRICE_RANGE', '$$')
            || !Configuration::updateValue('PS_LGCOMMENTS_DISPLAY_ORDER', '2')
            || !Configuration::updateValue('PS_LGCOMMENTS_DISPLAY_ORDER2', '2')
            || !Configuration::updateValue('PS_LGCOMMENTS_DISPLAY_SLIDER', '1')
            || !Configuration::updateValue('PS_LGCOMMENTS_SLIDER_BLOCKS', '4')
            || !Configuration::updateValue('PS_LGCOMMENTS_SLIDER_TOTAL', '12')
            || !Configuration::updateValue('PS_LGCOMMENTS_OPINION_FORM', '1')
            || !Configuration::updateValue('PS_LGCOMMENTS_SCALE', '10')
            || !Configuration::updateValue('PS_LGCOMMENTS_CATTOPMARGIN', '-10')
            || !Configuration::updateValue('PS_LGCOMMENTS_CATBOTMARGIN', '10')
            || !Configuration::updateValue('PS_LGCOMMENTS_PRODTOPMARGIN', '5')
            || !Configuration::updateValue('PS_LGCOMMENTS_PRODBOTMARGIN', '5')
            || !Configuration::updateValue('PS_LGCOMMENTS_CROSS', '1')
            || !Configuration::updateValue('PS_LGCOMMENTS_STORE_FILTER', '1')
            || !Configuration::updateValue('PS_LGCOMMENTS_PRODUCT_FILTER', '1')
            || !Configuration::updateValue('PS_LGCOMMENTS_PRODUCT_FILTER_NB', '3')
            || !Configuration::updateValue('PS_LGCOMMENTS_STORE_FORM', '1')
            || !Configuration::updateValue('PS_LGCOMMENTS_PRODUCT_FORM', '1')
            || !Configuration::updateValue('PS_LGCOMMENTS_BGDESIGN1', 'vertical')
            || !Configuration::updateValue('PS_LGCOMMENTS_BGDESIGN2', 'greylight')
            || !Configuration::updateValue('PS_LGCOMMENTS_STARDESIGN1', 'plain')
            || !Configuration::updateValue('PS_LGCOMMENTS_STARDESIGN2', 'yellow')
            || !Configuration::updateValue('PS_LGCOMMENTS_STARSIZE', '120')
            || !Configuration::updateValue(
                'PS_LGCOMMENTS_CSS_CONF',
                serialize($this->getExtraRightCSSConfig('vertical'))
            )
            || !Configuration::updateValue('PS_LGCOMMENTS_BACKGROUND5', 'f6f6f6')
            || !Configuration::updateValue('PS_LGCOMMENTS_BORDERSIZE5', '1')
            || !Configuration::updateValue('PS_LGCOMMENTS_TAB_CONTENT', $tab_type)
            || !Configuration::updateValue('PS_LGCOMMENTS_BORDERCOLOR5', '555555')
            || !Configuration::updateValue('PS_LGCOMMENTS_RATECOLOR5', '555555')
            || !Configuration::updateValue('PS_LGCOMMENTS_RATESIZE5', '22')
            || !Configuration::updateValue('PS_LGCOMMENTS_RATEFAMILY5', 'arial')
            || !Configuration::updateValue('PS_LGCOMMENTS_COMMENTCOLOR5', '555555')
            || !Configuration::updateValue('PS_LGCOMMENTS_COMMENTSIZE5', '18')
            || !Configuration::updateValue('PS_LGCOMMENTS_COMMENTFAMILY5', 'arial')
            || !Configuration::updateValue('PS_LGCOMMENTS_COMMENTALIGN5', 'center')
            || !Configuration::updateValue('PS_LGCOMMENTS_DATECOLOR5', '8C8C8C')
            || !Configuration::updateValue('PS_LGCOMMENTS_DATESIZE5', '12')
            || !Configuration::updateValue('PS_LGCOMMENTS_DATEFAMILY5', 'arial')
            || !Configuration::updateValue('PS_LGCOMMENTS_DATEALIGN5', 'left')
            || !Configuration::updateValue('PS_LGCOMMENTS_EMAIL_ALERTS', '1')
            || !Configuration::updateValue('PS_LGCOMMENTS_EMAIL_CRON', $default_email)
            || !Configuration::updateValue(
                'PS_LGCOMMENTS_SUBJECT_CRON',
                $this->l('The opinion requests have been sent correctly')
            )
            || !Configuration::updateValue(
                'PS_LGCOMMENTS_SUBJECT_NEWREVIEWS',
                $this->l('You have received new reviews')
            )
            || !Configuration::updateValue('PS_LGCOMMENTS_DIAS', '30')
            || !Configuration::updateValue('PS_LGCOMMENTS_DIAS2', '7')
            || !Configuration::updateValue('PS_LGCOMMENTS_EMAIL_TWICE', '0')
            || !Configuration::updateValue('PS_LGCOMMENTS_DAYS_AFTER', '10')
            || !Configuration::updateValue('PS_LGCOMMENTS_VALIDATION', '1')
            || !Configuration::updateValue('PS_LGCOMMENTS_BOXES', '1')
            || !Configuration::updateValue('PS_LGCOMMENTS_TOP6', '70')
            || !Configuration::updateValue('PS_LGCOMMENTS_LEFT6', '0')
        ) {
            return false;
        }

        $this->installModuleTab('AdminLGCommentsStore', $this->l('Store reviews'));
        $this->installModuleTab('AdminLGCommentsProducts', $this->l('Products reviews'));

        $pid = Db::getInstance()->getValue(
            'SELECT id_product '.
            'FROM '._DB_PREFIX_.'product '.
            'WHERE active = 1'
        );
        /* Store comment by default */
        Db::getInstance()->Execute('
			INSERT INTO `'._DB_PREFIX_.'lgcomments_storecomments`
			VALUES
			(
                NOW(),
                \'1\',
                \'1\',
                \'10\',
                \'This is a test comment\',
                \'1\',
                \'1\',
                \'1\',
                \'Test comment\',
                \'0\',
                \'1\'
            )
        ');
        /* Product comment by default */
        Db::getInstance()->Execute('
			INSERT INTO `'._DB_PREFIX_.'lgcomments_productcomments`
			VALUES
            (
                NOW(),
                \'1\',
                \''.$pid.'\',
                \'10\',
                \'This is a test comment\',
                \'1\',
                \'1\',
                \'1\',
                \'Test comment\',
                \'0\',
                \'1\'
            )
        ');
        /* One status selected by default */
        Db::getInstance()->Execute('
			INSERT INTO `'._DB_PREFIX_.'lgcomments_status`
			VALUES
			(\'5\')');
        /* One group selected by default */
        Db::getInstance()->Execute('
			INSERT INTO `'._DB_PREFIX_.'lgcomments_customergroups`
			VALUES
			(\'3\')');
        /* All shops selected by default */
        $shops = Db::getInstance()->executeS('
            SELECT `id_shop`
            FROM `'._DB_PREFIX_.'shop`
            ');
        foreach ($shops as $shop) {
            Db::getInstance()->Execute('
                INSERT INTO `'._DB_PREFIX_.'lgcomments_multistore`
                VALUES
                ('.(int)$shop['id_shop'].')');
        }
        /* Add meta tags for the shop review page */
        if (substr_count(_PS_VERSION_, '1.6') > 0) {
            Db::getInstance()->Execute('
                INSERT INTO `'._DB_PREFIX_.'meta` (page, configurable)
                VALUES
                (\'module-lgcomments-reviews\', \'1\')');
            $id_meta = Db::getInstance()->getValue(
                'SELECT id_meta '.
                'FROM '._DB_PREFIX_.'meta '.
                'WHERE page = "module-lgcomments-reviews"'
            );
            $shops = Db::getInstance()->executeS('
                SELECT `id_shop`
                FROM `'._DB_PREFIX_.'shop`
                ');
            $themes = Db::getInstance()->executeS(
                'SELECT * '.
                'FROM `'._DB_PREFIX_.'theme`'
            );
            foreach ($themes as $theme) {
                Db::getInstance()->Execute('
                INSERT INTO `'._DB_PREFIX_.'theme_meta` (id_theme, id_meta, left_column, right_column)
                VALUES
                (\''.(int)$theme['id_theme'].'\', \''.(int)$id_meta.'\', \'0\', \'0\')');
            }
            foreach ($shops as $shop) {
                $languages = Language::getLanguages();
                foreach ($languages as $language) {
                    if ($language['iso_code'] == 'en') {
                        Db::getInstance()->Execute(
                            'INSERT INTO `'._DB_PREFIX_.'meta_lang`
                            VALUES (
                                \''.(int)$id_meta.'\',
                                \''.(int)$shop['id_shop'].'\',
                                \''.(int)$language['id_lang'].'\',
                                \'Customer reviews about '.Configuration::get('PS_SHOP_NAME').'\',
                                \'Read all the reviews written by customers about our shop\',
                                \'reviews, comments, ratings, customers, shop\',
                                \'store-reviews\'
                            )'
                        );
                        if (
                            !Configuration::updateValue(
                                'PS_LGCOMMENTS_SUBJECT'.$language['iso_code'],
                                'We want to hear from you'
                            )
                            || !Configuration::updateValue(
                                'PS_LGCOMMENTS_SUBJECT2'.$language['iso_code'],
                                'Thank you for your review'
                            )
                            || !Configuration::updateValue(
                                'PS_LGCOMMENTS_SUBJECT3'.$language['iso_code'],
                                'You have received an answer to your review'
                            )
                        ) {
                            return false;
                        }
                    } elseif ($language['iso_code'] == 'es') {
                        Db::getInstance()->Execute(
                            'INSERT INTO `'._DB_PREFIX_.'meta_lang`
                            VALUES (
                                \''.(int)$id_meta.'\',
                                \''.(int)$shop['id_shop'].'\',
                                \''.(int)$language['id_lang'].'\',
                                \'Opiniones de clientes sobre '.Configuration::get('PS_SHOP_NAME').'\',
                                \'Descubre todas las opiniones de clientes sobre nuestra tienda\',
                                \'opiniones, comentarios, valoraciones, clientes, tienda\',
                                \'opiniones-tienda\'
                            )'
                        );
                        if (
                            !Configuration::updateValue(
                                'PS_LGCOMMENTS_SUBJECT'.$language['iso_code'],
                                'Tu opinión nos interesa'
                            )
                            || !Configuration::updateValue(
                                'PS_LGCOMMENTS_SUBJECT2'.$language['iso_code'],
                                'Muchas gracias por tu opinión'
                            )
                            || !Configuration::updateValue(
                                'PS_LGCOMMENTS_SUBJECT3'.$language['iso_code'],
                                'Has recibido una respuesta a tu comentario'
                            )
                        ) {
                            return false;
                        }
                    } elseif ($language['iso_code'] == 'fr') {
                        Db::getInstance()->Execute(
                            'INSERT INTO `'._DB_PREFIX_.'meta_lang`
                            VALUES (
                                \''.(int)$id_meta.'\',
                                \''.(int)$shop['id_shop'].'\',
                                \''.(int)$language['id_lang'].'\',
                                \'Avis clients sur '.Configuration::get('PS_SHOP_NAME').'\',
                                \'Découvrez tous les avis rédigés par les clients à propos de notre boutique\',
                                \'avis, commentaires, notes, clients, boutique\',
                                \'avis-boutique\'
                            )'
                        );
                        if (
                            !Configuration::updateValue(
                                'PS_LGCOMMENTS_SUBJECT'.$language['iso_code'],
                                'Votre avis nous intéresse'
                            )
                            || !Configuration::updateValue(
                                'PS_LGCOMMENTS_SUBJECT2'.$language['iso_code'],
                                'Merci beaucoup pour votre avis'
                            )
                            || !Configuration::updateValue(
                                'PS_LGCOMMENTS_SUBJECT3'.$language['iso_code'],
                                'Vous avez reçu une réponse à votre commentaire'
                            )
                        ) {
                            return false;
                        }
                    } elseif ($language['iso_code'] == 'it') {
                        Db::getInstance()->Execute(
                            'INSERT INTO `'._DB_PREFIX_.'meta_lang`
                            VALUES (
                                \''.(int)$id_meta.'\',
                                \''.(int)$shop['id_shop'].'\',
                                \''.(int)$language['id_lang'].'\',
                                \'Recensioni di clienti su '.Configuration::get('PS_SHOP_NAME').'\',
                                \'Leggi tutte le recensioni scritte dai clienti sul nostro negozio\',
                                \'recensioni, commenti, voti, clienti, negozio\',
                                \'recensioni-negozio\'
                            )'
                        );
                        if (
                            !Configuration::updateValue(
                                'PS_LGCOMMENTS_SUBJECT'.$language['iso_code'],
                                'La tua opinione conta'
                            )
                            || !Configuration::updateValue(
                                'PS_LGCOMMENTS_SUBJECT2'.$language['iso_code'],
                                'Grazie per la tua opinione'
                            )
                            || !Configuration::updateValue(
                                'PS_LGCOMMENTS_SUBJECT3'.$language['iso_code'],
                                'Hai ricevuto una risposta alla tua opinione'
                            )
                        ) {
                            return false;
                        }
                    } else {
                        Db::getInstance()->Execute(
                            'INSERT INTO `'._DB_PREFIX_.'meta_lang`
                            VALUES (
                                \''.(int)$id_meta.'\',
                                \''.(int)$shop['id_shop'].'\',
                                \''.(int)$language['id_lang'].'\',
                                \'Customer reviews about '.Configuration::get('PS_SHOP_NAME').'\',
                                \'Read all the reviews written by customers about our shop\',
                                \'reviews, comments, ratings, customers, shop\',
                                \'store-reviews\'
                            )'
                        );
                        if (
                            !Configuration::updateValue(
                                'PS_LGCOMMENTS_SUBJECT'.$language['iso_code'],
                                'We want to hear from you'
                            )
                            || !Configuration::updateValue(
                                'PS_LGCOMMENTS_SUBJECT2'.$language['iso_code'],
                                'Thank you for your review'
                            )
                            || !Configuration::updateValue(
                                'PS_LGCOMMENTS_SUBJECT3'.$language['iso_code'],
                                'You have received an answer to your review'
                            )
                        ) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    public function hookCustomerAccount($params)
    {
        return $this->display(__FILE__, 'views/templates/front/account_button.tpl');
    }

    public function uninstall()
    {
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'lgcomments_orders`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'lgcomments_status`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'lgcomments_productcomments`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'lgcomments_storecomments`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'lgcomments_customergroups`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'lgcomments_multistore`');
        $sql = 'SELECT `id_tab` FROM `'._DB_PREFIX_.'tab` WHERE `module` = "'.pSQL($this->name).'"';
        $result = Db::getInstance()->ExecuteS($sql);
        if ($result && count($result)) {
            foreach ($result as $tabData) {
                $tab = new Tab($tabData['id_tab']);

                if (Validate::isLoadedObject($tab)) {
                    $tab->delete();
                }
            }
        }
        if (substr_count(_PS_VERSION_, '1.6') > 0) {
            $id_meta = Db::getInstance()->getValue(
                'SELECT id_meta '.
                'FROM '._DB_PREFIX_.'meta '.
                'WHERE page = "module-lgcomments-reviews"'
            );
            Db::getInstance()->Execute('DELETE FROM '._DB_PREFIX_.'meta WHERE id_meta = '.(int)$id_meta.'');
            Db::getInstance()->Execute('DELETE FROM '._DB_PREFIX_.'meta_lang WHERE id_meta = '.(int)$id_meta.'');
            Db::getInstance()->Execute('DELETE FROM '._DB_PREFIX_.'theme_meta WHERE id_meta = '.(int)$id_meta.'');
        }

        return parent::uninstall();
    }

    private function installModuleTab($class, $name)
    {
        $sql = '
	SELECT `id_tab` FROM `'._DB_PREFIX_.'tab` WHERE `class_name` = "AdminCatalog"';

        $tabParent = (int)(Db::getInstance()->getValue($sql));

        if (!is_array($name)) {
            $name = self::getMultilangField($name);
        }
        $tab = new Tab();
        $tab->name = $name;
        $tab->class_name = $class;
        $tab->module = $this->name;
        $tab->id_parent = $tabParent;
        return $tab->save();
    }

    private static function getMultilangField($field)
    {
        $languages = Language::getLanguages();
        $res = array();

        foreach ($languages as $lang) {
            $res[$lang['id_lang']] = $field;
        }
        return $res;
    }

    private function formatBootstrap($text)
    {
        $text = str_replace('<fieldset>', '<div class="panel">', $text);
        $text = str_replace(
            '<fieldset style="background:#DFF2BF;color:#4F8A10;border:1px solid #4F8A10;">',
            '<div class="panel"  style="background:#DFF2BF;color:#4F8A10;border:1px solid #4F8A10;">',
            $text
        );
        $text = str_replace('</fieldset>', '</div>', $text);
        $text = str_replace('<legend>', '<h3>', $text);
        $text = str_replace('</legend>', '</h3>', $text);
        return $text;
    }

    /* CONFIGURATION MODULE */

    private function getCurrencySign()
    {
        $sign = Db::getInstance()->getValue(
            'SELECT sign '.
            'FROM '._DB_PREFIX_.'currency '.
            'WHERE id_currency = '.Configuration::get('PS_CURRENCY_DEFAULT')
        );
        return $sign;
    }

    private function getAllStoreComments()
    {
        $storeComments = Db::getInstance()->ExecuteS(
            'SELECT * '.
            'FROM '._DB_PREFIX_.'lgcomments_storecomments '.
            'ORDER BY id_lgcomments_storecomments ASC'
        );
        return $storeComments;
    }

    private function getAllProductComments()
    {
        $productComments = Db::getInstance()->ExecuteS(
            'SELECT * '.
            'FROM '._DB_PREFIX_.'lgcomments_productcomments '.
            'ORDER BY id_lgcomments_productcomments ASC'
        );
        return $productComments;
    }

    private function getOrdersStatus()
    {
        $estados = Db::getInstance()->ExecuteS(
            'SELECT * '.
            'FROM '._DB_PREFIX_.'order_state_lang osl '.
            'INNER JOIN '._DB_PREFIX_.'order_state os '.
            'ON osl.id_order_state = os.id_order_state '.
            'WHERE osl.id_lang = '.(int)$this->context->language->id.' '.
            'ORDER BY osl.id_order_state ASC'
        );
        return $estados;
    }

    private function getLGCommentsOrderStatus($id_state)
    {
        $estado = Db::getInstance()->ExecuteS(
            'SELECT * '.
            'FROM '._DB_PREFIX_.'lgcomments_status '.
            'WHERE id_order_status = '.(int)$id_state
        );
        return $estado;
    }

    private function checkIfEmptyStore()
    {
        $checkS = Db::getInstance()->getValue(
            'SELECT COUNT(*) '.
            'FROM '._DB_PREFIX_.'lgcomments_storecomments'
        );
        return $checkS;
    }

    private function checkIfEmptyProduct()
    {
        $checkP = Db::getInstance()->getValue(
            'SELECT COUNT(*) '.
            'FROM '._DB_PREFIX_.'lgcomments_productcomments'
        );
        return $checkP;
    }

    private function checkIfEmptyStatus()
    {
        $checkE = Db::getInstance()->getValue(
            'SELECT COUNT(*) '.
            'FROM '._DB_PREFIX_.'lgcomments_status'
        );
        return $checkE;
    }

    private function checkIfEmptyGroup()
    {
        $checkG = Db::getInstance()->getValue(
            'SELECT COUNT(*) '.
            'FROM '._DB_PREFIX_.'lgcomments_customergroups'
        );
        return $checkG;
    }

    private function checkIfEmptyMultistore()
    {
        $checkM = Db::getInstance()->getValue(
            'SELECT COUNT(*) '.
            'FROM '._DB_PREFIX_.'lgcomments_multistore'
        );
        return $checkM;
    }

    private function getHookList()
    {
        $hookList = Db::getInstance()->ExecuteS(
            'SELECT h.name '.
            'FROM '._DB_PREFIX_.'hook h '.
            'INNER JOIN '._DB_PREFIX_.'hook_module hm '.
            'ON h.id_hook = hm.id_hook '.
            'INNER JOIN '._DB_PREFIX_.'module m '.
            'ON hm.id_module = m.id_module '.
            'WHERE m.name = "lgcomments" '.
            'AND hm.id_shop = '.(int)$this->context->shop->id
        );
        return $hookList;
    }

    private function getCustomerGroups()
    {
        $grupo = Db::getInstance()->ExecuteS(
            'SELECT * '.
            'FROM '._DB_PREFIX_.'group_lang '.
            'WHERE id_lang = '.(int)$this->context->language->id
        );
        return $grupo;
    }

    private function getCustomerGroupsByGroup($id_group)
    {
        $grupog = Db::getInstance()->ExecuteS(
            'SELECT * '.
            'FROM '._DB_PREFIX_.'lgcomments_customergroups '.
            'WHERE id_customer_group = '.(int)$id_group
        );
        return $grupog;
    }

    private function getCustomerGroupsByCustomer($customer_id)
    {
        $groups = Db::getInstance()->ExecuteS(
            'SELECT DISTINCT lcg.*, gl.name '.
            'FROM '._DB_PREFIX_.'lgcomments_customergroups lcg '.
            'INNER JOIN '._DB_PREFIX_.'group_lang gl ON lcg.id_customer_group = gl.id_group '.
            'RIGHT JOIN '._DB_PREFIX_.'customer_group cg ON gl.id_group = cg.id_group '.
            'WHERE gl.id_lang = '.(int)$this->context->language->id.' '.
            'AND cg.id_customer = '.(int)$customer_id
        );
        return $groups;
    }

    private function getShops()
    {
        $shops = Db::getInstance()->ExecuteS(
            'SELECT * '.
            'FROM '._DB_PREFIX_.'shop'
        );
        return $shops;
    }

    private function getSelectedShops($id_shop)
    {
        $shop = Db::getInstance()->ExecuteS(
            'SELECT * '.
            'FROM '._DB_PREFIX_.'lgcomments_multistore '.
            'WHERE id_shop = '.(int)$id_shop
        );
        return $shop;
    }

    private function getShopsByCustomer($id_customer)
    {
        $cshop = Db::getInstance()->ExecuteS(
            'SELECT DISTINCT lm.*, s.name '.
            'FROM '._DB_PREFIX_.'lgcomments_multistore lm '.
            'LEFT JOIN '._DB_PREFIX_.'customer c ON lm.id_shop = c.id_shop '.
            'LEFT JOIN '._DB_PREFIX_.'shop s ON lm.id_shop = s.id_shop '.
            'WHERE c.id_customer = '.(int)$id_customer
        );
        return $cshop;
    }

    private function getCorrespondingOrders($date1, $date2)
    {
        if (Configuration::get('PS_LGCOMMENTS_BOXES') == 2) {
            $boxes_checked = 'AND c.newsletter = 1 ';
        } elseif (Configuration::get('PS_LGCOMMENTS_BOXES') == 3) {
            $boxes_checked = 'AND c.optin = 1 ';
        } elseif (Configuration::get('PS_LGCOMMENTS_BOXES') == 4) {
            $boxes_checked = 'AND c.newsletter = 1 AND c.optin = 1 ';
        } else {
            $boxes_checked = '';
        }
        $orderList = Db::getInstance()->ExecuteS(
            'SELECT DISTINCT o.id_order, o.id_customer, o.reference, o.date_add, osl.name as statusname, '.
            'lo.date_email, lo.voted, lo.sent, lo.date_email2, os.color, c.newsletter, c.optin, '.
            'CONCAT(c.firstname, \' \', (SUBSTRING(c.lastname,1,1)), \'.\') as customer '.
            'FROM '._DB_PREFIX_.'orders o '.
            'INNER JOIN '._DB_PREFIX_.'lgcomments_status ek ON o.current_state = ek.id_order_status '.
            'INNER JOIN '._DB_PREFIX_.'order_state_lang osl ON o.current_state = osl.id_order_state '.
            'INNER JOIN '._DB_PREFIX_.'order_state os ON osl.id_order_state = os.id_order_state '.
            'LEFT JOIN '._DB_PREFIX_.'lgcomments_orders lo ON o.id_order = lo.id_order '.
            'RIGHT JOIN '._DB_PREFIX_.'customer_group cg ON o.id_customer = cg.id_customer '.
            'RIGHT JOIN '._DB_PREFIX_.'customer c ON o.id_customer = c.id_customer '.
            'INNER JOIN '._DB_PREFIX_.'lgcomments_customergroups lcg ON cg.id_group = lcg.id_customer_group '.
            'INNER JOIN '._DB_PREFIX_.'group_lang gl ON cg.id_group = gl.id_group '.
            'RIGHT JOIN '._DB_PREFIX_.'lgcomments_multistore lm ON o.id_shop = lm.id_shop '.
            'WHERE o.date_add >= DATE_SUB(NOW(),INTERVAL '.(int)$date1.' DAY) '.
            'AND o.date_add <= DATE_SUB(NOW(),INTERVAL '.(int)$date2.' DAY) '.
            'AND osl.id_lang = '.(int)$this->context->language->id.' '.
            'AND gl.id_lang = '.(int)$this->context->language->id.' '.
            $boxes_checked.
            'ORDER BY o.id_order DESC'
        );
        return $orderList;
    }

    private function getCorrespondingStatus()
    {
        $statusList = Db::getInstance()->ExecuteS(
            'SELECT osl.name, os.color '.
            'FROM '._DB_PREFIX_.'order_state_lang osl '.
            'INNER JOIN '._DB_PREFIX_.'lgcomments_status lgs ON osl.id_order_state = lgs.id_order_status '.
            'INNER JOIN '._DB_PREFIX_.'order_state os ON osl.id_order_state = os.id_order_state '.
            'WHERE osl.id_lang = '.(int)$this->context->language->id.''
        );
        return $statusList;
    }

    private function getCorrespondingGroups()
    {
        $groupList = Db::getInstance()->ExecuteS(
            'SELECT DISTINCT lcg.*, gl.name '.
            'FROM '._DB_PREFIX_.'lgcomments_customergroups lcg '.
            'INNER JOIN '._DB_PREFIX_.'group_lang gl ON lcg.id_customer_group = gl.id_group '.
            'WHERE gl.id_lang = '.(int)$this->context->language->id.''
        );
        return $groupList;
    }

    private function getCorrespondingShops()
    {
        $shopList = Db::getInstance()->ExecuteS(
            'SELECT DISTINCT lm.*, s.name '.
            'FROM '._DB_PREFIX_.'lgcomments_multistore lm '.
            'INNER JOIN '._DB_PREFIX_.'shop s ON lm.id_shop = s.id_shop '
        );
        return $shopList;
    }

    /* PRODUCT COMMENTS */

    private function getCountProdByRate($id_product, $min, $max)
    {
        $rates = Db::getInstance()->getvalue(
            'SELECT COUNT(*) '.
            'FROM '._DB_PREFIX_.'lgcomments_productcomments '.
            'WHERE id_product = '.(int)$id_product.' '.
            'AND active = 1 '.
            'AND stars >= '.(int)$min.' AND stars < '.(int)$max
        );
        return $rates;
    }

    private function getCountProdByRateAndLang($id_product, $min, $max)
    {
        $ratesL = Db::getInstance()->getvalue(
            'SELECT COUNT(*) '.
            'FROM '._DB_PREFIX_.'lgcomments_productcomments '.
            'WHERE id_product = '.(int)$id_product.' '.
            'AND active = 1 '.
            'AND id_lang = '.(int)$this->context->language->id.' '.
            'AND stars >= '.(int)$min.' AND stars < '.(int)$max.''
        );
        return $ratesL;
    }

    private function getProdComments($id_product, $order)
    {
        $comments = Db::getInstance()->executeS(
            'SELECT pc.*, CONCAT(c.firstname, \' \', (SUBSTRING(c.lastname,1,1)), \'.\') as customer '.
            'FROM '._DB_PREFIX_.'lgcomments_productcomments pc '.
            'LEFT JOIN '._DB_PREFIX_.'customer as c ON pc.id_customer = c.id_customer '.
            'WHERE pc.id_product = '.(int)$id_product.' '.
            'AND pc.active = 1 '.
            'ORDER BY pc.position '.$order
        );
        return $comments;
    }

    private function getProdCommentsByLang($id_product, $order)
    {
        $commentsL = Db::getInstance()->executeS(
            'SELECT pc.*, CONCAT(c.firstname, \' \', (SUBSTRING(c.lastname,1,1)), \'.\') as customer '.
            'FROM '._DB_PREFIX_.'lgcomments_productcomments pc '.
            'LEFT JOIN '._DB_PREFIX_.'customer as c ON pc.id_customer = c.id_customer '.
            'WHERE pc.id_product = '.(int)$id_product.' '.
            'AND pc.id_lang = '.(int)$this->context->language->id.' '.
            'AND pc.active = 1 '.
            'ORDER BY pc.position '.$order
        );
        return $commentsL;
    }

    private function getCountProdComments($id_product)
    {
        $count = Db::getInstance()->getvalue(
            'SELECT COUNT(*) '.
            'FROM '._DB_PREFIX_.'lgcomments_productcomments '.
            'WHERE id_product = '.(int)$id_product.' '.
            'AND active = 1'
        );
        return $count;
    }

    private function getCountProdCommentsByLang($id_product)
    {
        $countL = Db::getInstance()->getvalue(
            'SELECT COUNT(*) '.
            'FROM '._DB_PREFIX_.'lgcomments_productcomments '.
            'WHERE id_product = '.(int)$id_product.' '.
            'AND active = 1 '.
            'AND id_lang = '.(int)$this->context->language->id.''
        );
        return $countL;
    }

    private function getSumProdComments($id_product)
    {
        $total = Db::getInstance()->getvalue(
            'SELECT SUM(stars) AS totalcomentarios '.
            'FROM '._DB_PREFIX_.'lgcomments_productcomments '.
            'WHERE id_product = '.(int)$id_product.''.
            ' AND active = 1'
        );
        return $total;
    }

    private function getSumProdCommentsByLang($id_product)
    {
        $totalL = Db::getInstance()->getvalue(
            'SELECT SUM(stars) AS totalcomentarios '.
            'FROM '._DB_PREFIX_.'lgcomments_productcomments '.
            'WHERE id_product = '.(int)$id_product.''.
            ' AND id_lang = '.(int)$this->context->language->id.
            ' AND active = 1'
        );
        return $totalL;
    }

    private function getProductName($id_product)
    {
        $name = Db::getInstance()->getValue(
            'SELECT name '.
            'FROM '._DB_PREFIX_.'product_lang '.
            'WHERE id_product = '.(int)$id_product.
            ' AND id_lang = '.(int)$this->context->language->id
        );
        return $name;
    }

    private function getProductRewrite($id_product)
    {
        $rewrite = Db::getInstance()->getValue(
            'SELECT link_rewrite '.
            'FROM '._DB_PREFIX_.'product_lang '.
            'WHERE id_product = '.(int)$id_product.
            ' AND id_lang = '.(int)$this->context->language->id
        );
        return $rewrite;
    }

    private function checkIfProdAlreadyReviewed($id_product)
    {
        $check = Db::getInstance()->getValue(
            'SELECT COUNT(id_lgcomments_productcomments) '.
            'FROM '._DB_PREFIX_.'lgcomments_productcomments '.
            'WHERE id_customer = '.(int)$this->context->customer->id.' '.
            'AND id_product = '.(int)$id_product
        );
        return $check;
    }

    /* SHOP COMMENTS */

    private function getSumShopComments()
    {
        $sum = Db::getInstance()->getValue(
            'SELECT SUM(stars) AS totalcomentarios '.
            'FROM '._DB_PREFIX_.'lgcomments_storecomments '.
            'WHERE active = 1'
        );
        return $sum;
    }

    private function getSumShopCommentsByLang()
    {
        $sumL = Db::getInstance()->getValue(
            'SELECT SUM(stars) AS totalcomentarios '.
            'FROM '._DB_PREFIX_.'lgcomments_storecomments '.
            'WHERE active = 1'.
            ' AND id_lang = '.(int)$this->context->language->id
        );
        return $sumL;
    }

    private function getCountShopComments()
    {
        $count = Db::getInstance()->getValue(
            'SELECT COUNT(id_lgcomments_storecomments) AS total '.
            'FROM '._DB_PREFIX_.'lgcomments_storecomments '.
            'WHERE active = 1'
        );
        return $count;
    }

    private function getCountShopCommentsByLang()
    {
        $countL = Db::getInstance()->getValue(
            'SELECT COUNT(id_lgcomments_storecomments) AS total '.
            'FROM '._DB_PREFIX_.'lgcomments_storecomments '.
            'WHERE active = 1 '.
            'AND id_lang = '.(int)$this->context->language->id.''
        );
        return $countL;
    }

    private function getRandomShopComment()
    {
        $random = Db::getInstance()->executeS(
            'SELECT comment '.
            'FROM '._DB_PREFIX_.'lgcomments_storecomments '.
            'WHERE active = 1 '.
            'AND stars > 6 '.
            'ORDER BY RAND()'
        );
        return $random;
    }

    private function getRandomShopCommentByLang()
    {
        $randomL = Db::getInstance()->executeS(
            'SELECT comment '.
            'FROM '._DB_PREFIX_.'lgcomments_storecomments '.
            'WHERE active = 1 '.
            'AND stars > 6 '.
            'AND id_lang = '.(int)$this->context->language->id.' '.
            'ORDER BY RAND()'
        );
        return $randomL;
    }

    private function getSliderShopComments($number)
    {
        $slider = Db::getInstance()->executeS(
            'SELECT st.*, CONCAT(c.firstname, \' \', (SUBSTRING(c.lastname,1,1)), \'.\') as customer '.
            'FROM `'._DB_PREFIX_.'lgcomments_storecomments` st '.
            'LEFT JOIN `'._DB_PREFIX_.'customer` c ON st.`id_customer` = c.`id_customer` '.
            'WHERE st.active = 1 '.
            'ORDER BY st.date DESC '.
            'LIMIT '.(int)$number
        );
        return $slider;
    }

    private function getSliderShopCommentsByLang($number)
    {
        $sliderL = Db::getInstance()->executeS(
            'SELECT st.*, CONCAT(c.firstname, \' \', (SUBSTRING(c.lastname,1,1)), \'.\') as customer '.
            'FROM `'._DB_PREFIX_.'lgcomments_storecomments` st '.
            'LEFT JOIN `'._DB_PREFIX_.'customer` c ON st.`id_customer` = c.`id_customer` '.
            'WHERE st.active = 1 '.
            'AND st.id_lang = '.(int)$this->context->language->id.' '.
            'ORDER BY st.date DESC '.
            'LIMIT '.(int)$number
        );
        return $sliderL;
    }

    private function getDateFormat()
    {
        $format = Db::getInstance()->getValue(
            'SELECT date_format_lite '.
            'FROM '._DB_PREFIX_.'lang '.
            'WHERE id_lang = '.(int)$this->context->language->id
        );
        return $format;
    }

    private function getDateFormatFull()
    {
        $format = Db::getInstance()->getValue(
            'SELECT date_format_full '.
            'FROM '._DB_PREFIX_.'lang '.
            'WHERE id_lang = '.(int)$this->context->language->id
        );
        return $format;
    }

    /* BANNER */

    private function getP()
    {
        $default_lang = $this->context->language->id;
        $lang         = Language::getIsoById($default_lang);
        $pl           = array('es','fr');
        if (!in_array($lang, $pl)) {
            $lang = 'en';
        }
        $this->context->controller->addCSS(_MODULE_DIR_.$this->name.'/views/css/publi/style.css');
        $base = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off')  ?
            'https://'.$this->context->shop->domain_ssl :
            'http://'.$this->context->shop->domain);
        if (version_compare(_PS_VERSION_, '1.5.0', '>')) {
            $uri = $base.$this->context->shop->getBaseURI();
        } else {
            $uri = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off')  ?
                    'https://'._PS_SHOP_DOMAIN_SSL_DOMAIN_:
                    'http://'._PS_SHOP_DOMAIN_).__PS_BASE_URI__;
        }
        $path = _PS_MODULE_DIR_.$this->name
            .DIRECTORY_SEPARATOR.'views'
            .DIRECTORY_SEPARATOR.'publi'
            .DIRECTORY_SEPARATOR.$lang
            .DIRECTORY_SEPARATOR.'index.php';
        $object = Tools::file_get_contents($path);
        $object = str_replace('src="/modules/', 'src="'.$uri.'modules/', $object);

        return $object;
    }

    public function getContent()
    {
        $tokenC = Tools::getAdminTokenLite('AdminCustomers');
        $tokenO = Tools::getAdminTokenLite('AdminOrders');
        $tokenL = Tools::getAdminTokenLite('AdminLanguages');
        $tokenPC = Tools::getAdminTokenLite('AdminLGCommentsProducts');
        $tokenSC = Tools::getAdminTokenLite('AdminLGCommentsStore');
        $tokenPe = Tools::getAdminTokenLite('AdminPerformance');
        $tokenPr = Tools::getAdminTokenLite('AdminProducts');
        $tokenM = Tools::getAdminTokenLite('AdminModulesPositions');
        $tokenE = Tools::getAdminTokenLite('AdminEmails');
        $secureKey = md5(_COOKIE_KEY_.Configuration::get('PS_SHOP_NAME'));

        if (version_compare(_PS_VERSION_, '1.6.0', '<')) {
            $this->context->controller->addJS(_MODULE_DIR_.$this->name.'/views/js/bootstrap.js');
            $this->context->controller->addJS(_MODULE_DIR_.$this->name.'/views/js/admin15.js');
            $this->context->controller->addCSS(_MODULE_DIR_.$this->name.'/views/css/admin15.css');
        }

        if (!Configuration::get('PS_LGCOMMENTS_STARS_TYPE')) {
            Configuration::updateValue('PS_LGCOMMENTS_STARS_TYPE', '1');
        }

        $this->_html = $this->getP().'
        <link type="text/css" rel="stylesheet" href="../modules/'.$this->name.'/views/css/lgcomments.css" media="all">
        <script type="text/javascript" src="'.__PS_BASE_URI__.'modules/'.$this->name.'/views/js/jscolor.js"></script>
        <script type="text/javascript" src="'.__PS_BASE_URI__.'modules/'.$this->name.'/views/js/menu.js"></script>
        <h2>'.$this->displayName.'</h2><br>';

        // Module configuration
        if (Tools::isSubmit('submitLGCommentsGeneral')) {
            Configuration::updateValue('PS_LGCOMMENTS_STARDESIGN1', Tools::getValue('stardesign1'));
            Configuration::updateValue('PS_LGCOMMENTS_STARDESIGN2', Tools::getValue('stardesign2'));
            Configuration::updateValue('PS_LGCOMMENTS_STARSIZE', Tools::getValue('starsize'));
            Configuration::updateValue('PS_LGCOMMENTS_SCALE', Tools::getValue('PS_LGCOMMENTS_SCALE'));
            Configuration::updateValue(
                'PS_LGCOMMENTS_DISPLAY_ZEROSTAR',
                Tools::getValue('PS_LGCOMMENTS_DISPLAY_ZEROSTAR')
            );
            Configuration::updateValue('PS_LGCOMMENTS_CATTOPMARGIN', Tools::getValue('PS_LGCOMMENTS_CATTOPMARGIN'));
            Configuration::updateValue('PS_LGCOMMENTS_CATBOTMARGIN', Tools::getValue('PS_LGCOMMENTS_CATBOTMARGIN'));
            Configuration::updateValue('PS_LGCOMMENTS_PRODTOPMARGIN', Tools::getValue('PS_LGCOMMENTS_PRODTOPMARGIN'));
            Configuration::updateValue('PS_LGCOMMENTS_PRODBOTMARGIN', Tools::getValue('PS_LGCOMMENTS_PRODBOTMARGIN'));
            $this->_html .= $this->displayConfirmation($this->l('Rating configuration updated'));
        } if (Tools::isSubmit('submitLGCommentsWidget')) {
            Configuration::updateValue('PS_LGCOMMENTS_DISPLAY', Tools::getValue('lgcomments_display'));
            Configuration::updateValue(
                'PS_LGCOMMENTS_DISPLAY_TYPE',
                Tools::getValue('PS_LGCOMMENTS_DISPLAY_TYPE')
            );
            Configuration::updateValue(
                'PS_LGCOMMENTS_DISPLAY_SIDE',
                Tools::getValue('PS_LGCOMMENTS_DISPLAY_SIDE')
            );
            Configuration::updateValue('PS_LGCOMMENTS_BGDESIGN1', Tools::getValue('bgdesign1'));
            Configuration::updateValue('PS_LGCOMMENTS_BGDESIGN2', Tools::getValue('bgdesign2'));
            $type = explode('-', Tools::getValue('bgdesign1', 'bubble-yellow.png'));
            Configuration::updateValue(
                'PS_LGCOMMENTS_CSS_CONF',
                serialize($this->getExtraRightCSSConfig($type[0]))
            );
            Configuration::updateValue(
                'PS_LGCOMMENTS_CROSS',
                Tools::getValue('lgcomments_display_cross')
            );
            Configuration::updateValue(
                'PS_LGCOMMENTS_TEXTCOLOR',
                Tools::getValue('widget_text_color')
            );
            $this->_html .= $this->displayConfirmation($this->l('Widget configuration updated'));
        } if (Tools::isSubmit('submitLGCommentsHomepage')) {
            Configuration::updateValue(
                'PS_LGCOMMENTS_DISPLAY_SLIDER',
                Tools::getValue('PS_LGCOMMENTS_DISPLAY_SLIDER')
            );
            Configuration::updateValue(
                'PS_LGCOMMENTS_SLIDER_BLOCKS',
                Tools::getValue('PS_LGCOMMENTS_SLIDER_BLOCKS')
            );
            Configuration::updateValue(
                'PS_LGCOMMENTS_SLIDER_TOTAL',
                Tools::getValue('PS_LGCOMMENTS_SLIDER_TOTAL')
            );
            $this->_html .= $this->displayConfirmation($this->l('Homepage configuration updated'));
        } if (Tools::isSubmit('submitLGCommentsStore')) {
            Configuration::updateValue(
                'PS_LGCOMMENTS_STORE_FILTER',
                Tools::getValue('lgcomments_store_filter')
            );
            Configuration::updateValue(
                'PS_LGCOMMENTS_TEXTCOLOR2',
                Tools::getValue('store_text_color')
            );
            Configuration::updateValue(
                'PS_LGCOMMENTS_BACKCOLOR2',
                Tools::getValue('store_back_color')
            );
            Configuration::updateValue(
                'PS_LGCOMMENTS_PER_PAGE',
                Tools::getValue('store_comments_per_page')
            );
            Configuration::updateValue(
                'PS_LGCOMMENTS_DISPLAY_ORDER',
                Tools::getValue('PS_LGCOMMENTS_DISPLAY_ORDER')
            );
            Configuration::updateValue(
                'PS_LGCOMMENTS_DISPLAY_LANGUAGE',
                Tools::getValue('PS_LGCOMMENTS_DISPLAY_LANGUAGE')
            );
            Configuration::updateValue(
                'PS_LGCOMMENTS_STORE_FORM',
                Tools::getValue('PS_LGCOMMENTS_STORE_FORM')
            );
            $this->_html .= $this->displayConfirmation($this->l('Store review configuration updated'));
        } if (Tools::isSubmit('submitLGCommentsProducts')) {
            Configuration::updateValue(
                'PS_LGCOMMENTS_DISPLAY_COMMENTS',
                Tools::getValue('lgcomments_display_comments')
            );
            Configuration::updateValue(
                'PS_LGCOMMENTS_TAB_CONTENT',
                Tools::getValue('PS_LGCOMMENTS_TAB_CONTENT', 1)
            );
            Configuration::updateValue(
                'PS_LGCOMMENTS_PRODUCT_FILTER',
                Tools::getValue('lgcomments_product_filter')
            );
            Configuration::updateValue(
                'PS_LGCOMMENTS_PRODUCT_FILTER_NB',
                Tools::getValue('lgcomments_product_filter_nb')
            );
            Configuration::updateValue(
                'PS_LGCOMMENTS_DISPLAY_DEFAULT',
                Tools::getValue('lgcomments_display_default')
            );
            Configuration::updateValue(
                'PS_LGCOMMENTS_DISPLAY_MORE',
                Tools::getValue('lgcomments_display_more')
            );
            Configuration::updateValue(
                'PS_LGCOMMENTS_DISPLAY_ORDER2',
                Tools::getValue('PS_LGCOMMENTS_DISPLAY_ORDER2')
            );
            Configuration::updateValue(
                'PS_LGCOMMENTS_DISPLAY_LANGUAGE2',
                Tools::getValue('PS_LGCOMMENTS_DISPLAY_LANGUAGE2')
            );
            Configuration::updateValue(
                'PS_LGCOMMENTS_PRODUCT_FORM',
                Tools::getValue('PS_LGCOMMENTS_PRODUCT_FORM')
            );
            $this->_html .= Module::DisplayConfirmation($this->l('Product review configuration updated'));
        } if (Tools::isSubmit('submitLGCommentsSnippets')) {
            Configuration::updateValue(
                'PS_LGCOMMENTS_DISPLAY_SNIPPETS',
                Tools::getValue('lgcomments_display_snippets')
            );
            Configuration::updateValue(
                'PS_LGCOMMENTS_DISPLAY_SNIPPETS2',
                Tools::getValue('lgcomments_display_snippets2')
            );
            Configuration::updateValue(
                'PS_LGCOMMENTS_PRICE_RANGE',
                Tools::getValue('PS_LGCOMMENTS_PRICE_RANGE')
            );
            $this->_html .= $this->displayConfirmation($this->l('Snippet configuration updated'));
        } if (Tools::isSubmit('submitLGCommentsSend')) {
            // Email confirmation (Cron)
            Configuration::updateValue(
                'PS_LGCOMMENTS_EMAIL_ALERTS',
                Tools::getValue('lgcomments_email_alerts')
            );
            $email_cron = Tools::getValue('lgcomments_email_cron');
            if (!empty($email_cron) && !Validate::isEmail($email_cron)) {
                Configuration::updateValue('PS_LGCOMMENTS_EMAIL_CRON', '');
            } else {
                Configuration::updateValue(
                    'PS_LGCOMMENTS_EMAIL_CRON',
                    Tools::getValue('lgcomments_email_cron')
                );
            }
            Configuration::updateValue(
                'PS_LGCOMMENTS_SUBJECT_CRON',
                Tools::getValue('subjectcron')
            );
            Configuration::updateValue(
                'PS_LGCOMMENTS_SUBJECT_NEWREVIEWS',
                Tools::getValue('subjectreviews')
            );
            $this->_html .= $this->displayConfirmation($this->l('Cron configuration updated'));
        } if (Tools::isSubmit('submitLGCommentsConfigure')) {
            // Number of days
            $dias = abs(Tools::getValue('lgcomments_dias'));
            if ($dias == 0) {
                $dias = 1;
            }
            Configuration::updateValue('PS_LGCOMMENTS_DIAS', $dias);
            $dias2 = abs(Tools::getValue('lgcomments_dias2'));
            Configuration::updateValue('PS_LGCOMMENTS_DIAS2', $dias2);
            // Order status
            Db::getInstance()->Execute('TRUNCATE TABLE '._DB_PREFIX_.'lgcomments_status');
            foreach ($this->getOrdersStatus() as $estado) {
                if (Tools::getValue('estado'.$estado['id_order_state']) == 1) {
                    Db::getInstance()->Execute(
                        'INSERT INTO '._DB_PREFIX_.'lgcomments_status '.
                        'VALUES ('.(int)$estado['id_order_state'].')'
                    );
                }
            }
            // Shops
            Db::getInstance()->Execute('TRUNCATE TABLE '._DB_PREFIX_.'lgcomments_multistore');
            foreach ($this->getShops() as $shop) {
                if (Tools::getValue('shop'.$shop['id_shop']) == 1) {
                    Db::getInstance()->Execute(
                        'INSERT INTO '._DB_PREFIX_.'lgcomments_multistore '.
                        'VALUES ('.(int)$shop['id_shop'].')'
                    );
                }
            }
            // Boxes checked
            Configuration::updateValue('PS_LGCOMMENTS_BOXES', Tools::getValue('PS_LGCOMMENTS_BOXES'));
            // Customer groups
            $customerGroups = Db::getInstance()->ExecuteS(
                'SELECT * '.
                'FROM '._DB_PREFIX_.'group_lang '.
                'WHERE id_lang = '.(int)$this->context->language->id
            );
            Db::getInstance()->Execute('TRUNCATE TABLE '._DB_PREFIX_.'lgcomments_customergroups');
            foreach ($customerGroups as $cGroup) {
                if (Tools::getValue('group'.$cGroup['id_group']) == 1) {
                    Db::getInstance()->Execute(
                        'INSERT INTO '._DB_PREFIX_.'lgcomments_customergroups '.
                        'VALUES ('.(int)$cGroup['id_group'].')'
                    );
                }
            }
            // Email subject
            $langs = Language::getLanguages();
            foreach ($langs as $lang) {
                Configuration::updateValue(
                    'PS_LGCOMMENTS_SUBJECT'.$lang['iso_code'],
                    pSQL(Tools::getValue('subject'.$lang['iso_code']))
                );
                Configuration::updateValue(
                    'PS_LGCOMMENTS_SUBJECT2'.$lang['iso_code'],
                    pSQL(Tools::getValue('subject2'.$lang['iso_code']))
                );
                Configuration::updateValue(
                    'PS_LGCOMMENTS_SUBJECT3'.$lang['iso_code'],
                    pSQL(Tools::getValue('subject3'.$lang['iso_code']))
                );
            }
            // Second email
            Configuration::updateValue(
                'PS_LGCOMMENTS_EMAIL_TWICE',
                Tools::getValue('lgcomments_email_twice')
            );
            Configuration::updateValue(
                'PS_LGCOMMENTS_DAYS_AFTER',
                Tools::getValue('lgcomments_days_after')
            );
            $this->_html .= $this->displayConfirmation($this->l('Email configuration updated'));
        } if (Tools::isSubmit('submitLGCommentsManage')) {
            // Opinion form
            Configuration::updateValue(
                'PS_LGCOMMENTS_OPINION_FORM',
                Tools::getValue('PS_LGCOMMENTS_OPINION_FORM')
            );
            // Validation
            Configuration::updateValue(
                'PS_LGCOMMENTS_VALIDATION',
                Tools::getValue('lgcomments_validation')
            );
            $this->_html .= $this->displayConfirmation($this->l('General review configuration updated'));
        }
        // Import product comments
        if (Tools::isSubmit('productCSV')) {
            $separator1 = Tools::getValue('separator1');
            if ($separator1 == 2) {
                $sp = ',';
            } else {
                $sp = ';';
            }
            $encoding1 = Tools::getValue('encoding1');
            if (is_uploaded_file($_FILES['csv1']['tmp_name'])) {
                $type = explode(".", $_FILES['csv1']['name']);
                if (Tools::strtolower(end($type)) == 'csv') {
                    if (
                        move_uploaded_file(
                            $_FILES['csv1']['tmp_name'],
                            dirname(__FILE__).'/csv/'.$_FILES['csv1']['name']
                        )
                    ) {
                        $archivo = $_FILES['csv1']['name'];
                        $fp = fopen(dirname(__FILE__).'/csv/'.$archivo, 'r');
                        while (($datos = fgetcsv($fp, 1000, ''.$sp.'')) !== false) {
                            $datos[0] = str_replace('/', '-', $datos[0]);
                            $date = strtotime($datos[0]);
                            $date = date('Y-m-d', $date);
                            if ($encoding1 == 2) {
                                $csv_comment = mb_convert_encoding($datos[4], 'UTF-8', 'auto');
                                $csv_title = mb_convert_encoding($datos[8], 'UTF-8', 'auto');
                                $csv_answer = mb_convert_encoding($datos[9], 'UTF-8', 'auto');
                            } else {
                                $csv_comment = utf8_encode($datos[4]);
                                $csv_title = utf8_encode($datos[8]);
                                $csv_answer = utf8_encode($datos[9]);
                            }
                            Db::getInstance()->Execute(
                                'INSERT INTO '._DB_PREFIX_.'lgcomments_productcomments '.
                                '(date, id_customer, id_product, stars, comment, id_lang, '.
                                'active, position, title, answer ) '.
                                'VALUES (
                                    \''.$date.'\',
                                    \''.(int)$datos[1].'\',
                                    \''.(int)$datos[2].'\',
                                    \''.(int)$datos[3].'\',
                                    \''.pSQL($csv_comment).'\',
                                    \''.(int)$datos[5].'\',
                                    \''.(int)$datos[6].'\',
                                    \''.(int)$datos[7].'\',
                                    \''.pSQL($csv_title).'\',
                                    \''.pSQL($csv_answer).'\'
                                )'
                            );
                        }
                        fclose($fp);
                        $this->_html .= Module::DisplayConfirmation(
                            $this->l('The comments have been successfully added').'.&nbsp;
                            <a href="index.php?controller=AdminLGCommentsProducts&token='.$tokenPC.'" target="_blank">
                            '.$this->l('Click here to manage your product reviews').'.
                            </a>'
                        );
                    }
                } else {
                    $this->_html .= Module::DisplayError(
                        $this->l('The format of the file is not valid, it must be saved in ".csv" format.')
                    );
                }
            } else {
                $this->_html .= Module::DisplayError($this->l('An error occurred while uploading the CSV file'));
            }
        }
        // Import store comments
        if (Tools::isSubmit('storeCSV')) {
            $separator2 = Tools::getValue('separator2');
            if ($separator2 == 2) {
                $sp = ',';
            } else {
                $sp = ';';
            }
            $encoding2 = Tools::getValue('encoding2');
            if (is_uploaded_file($_FILES['csv2']['tmp_name'])) {
                $type = explode(".", $_FILES['csv2']['name']);
                if (Tools::strtolower(end($type)) == 'csv') {
                    if (
                        move_uploaded_file(
                            $_FILES['csv2']['tmp_name'],
                            dirname(__FILE__).'/csv/'.$_FILES['csv2']['name']
                        )
                    ) {
                        $archivo = $_FILES['csv2']['name'];
                        $fp = fopen(dirname(__FILE__).'/csv/'.$archivo, 'r');
                        while (($datos = fgetcsv($fp, 1000, ''.$sp.'')) !== false) {
                            $datos[0] = str_replace('/', '-', $datos[0]);
                            $date = strtotime($datos[0]);
                            $date = date('Y-m-d', $date);
                            if ($encoding2 == 2) {
                                $csv_comment = mb_convert_encoding($datos[4], 'UTF-8', 'auto');
                                $csv_title = mb_convert_encoding($datos[8], 'UTF-8', 'auto');
                                $csv_answer = mb_convert_encoding($datos[9], 'UTF-8', 'auto');
                            } else {
                                $csv_comment = utf8_encode($datos[4]);
                                $csv_title = utf8_encode($datos[8]);
                                $csv_answer = utf8_encode($datos[9]);
                            }
                            Db::getInstance()->Execute(
                                'INSERT INTO '._DB_PREFIX_.'lgcomments_storecomments '.
                                '(date, id_customer, id_order, stars, comment, id_lang, active, '.
                                'position, title, answer ) '.
                                'VALUES (
                                    \''.$date.'\',
                                    \''.(int)$datos[1].'\',
                                    \''.(int)$datos[2].'\',
                                    \''.(int)$datos[3].'\',
                                    \''.pSQL($csv_comment).'\',
                                    \''.(int)$datos[5].'\',
                                    \''.(int)$datos[6].'\',
                                    \''.(int)$datos[7].'\',
                                    \''.pSQL($csv_title).'\',
                                    \''.pSQL($csv_answer).'\'
                                )'
                            );
                        }
                        fclose($fp);
                        $this->_html .= Module::DisplayConfirmation(
                            $this->l('The comments have been successfully added').'.&nbsp;
                            <a href="index.php?controller=AdminLGCommentsStore&token='.$tokenSC.'" target="_blank">
                            '.$this->l('Click here to manage your store reviews').'.
                            </a>'
                        );
                    }
                } else {
                    $this->_html .= Module::DisplayError(
                        $this->l('The format of the file is not valid, it must be saved in ".csv" format.')
                    );
                }
            } else {
                $this->_html .= Module::DisplayError($this->l('An error occurred while uploading the CSV file'));
            }
        }
        // Export product comments
        if (Tools::isSubmit('exportProductCSV')) {
            $separator1 = Tools::getValue('separator1');
            if ($separator1 == 2) {
                $sp = ',';
            } else {
                $sp = ';';
            }
            $ln = "\n";
            $fp = fopen(_PS_ROOT_DIR_.'/modules/'.$this->name.'/csv/save_products.csv', 'w');
            $prodComments = $this->getAllProductComments();
            foreach ($prodComments as $prodComment) {
                fwrite(
                    $fp,
                    $prodComment['date'].$sp.$prodComment['id_customer'].$sp.$prodComment['id_product'].
                    $sp.$prodComment['stars'].$sp.utf8_decode($prodComment['comment']).''.
                    $sp.$prodComment['id_lang'].$sp.$prodComment['active'].$sp.$prodComment['position'].
                    $sp.utf8_decode($prodComment['title']).$sp.utf8_decode($prodComment['answer']).$ln
                );
            }
            fclose($fp);
            if ($prodComments != false) {
                $this->_html .= Module::DisplayConfirmation(
                    $this->l('The CSV file has been successfully generated,').'&nbsp;
                    <a href=../modules/'.$this->name.'/csv/save_products.csv>&nbsp;
                    '.$this->l('click here to download it').'
                    </a>.'
                );
            } else {
                $this->_html .= Module::DisplayError($this->l('There are no product comments to export'));
            }
        }
        // Export store comments
        if (Tools::isSubmit('exportStoreCSV')) {
            $separator2 = Tools::getValue('separator2');
            if ($separator2 == 2) {
                $sp = ',';
            } else {
                $sp = ';';
            }
            $ln = "\n";
            $fp = fopen(_PS_ROOT_DIR_.'/modules/'.$this->name.'/csv/save_store.csv', 'w');
            $storeComments = $this->getAllStoreComments();
            foreach ($storeComments as $storeComment) {
                fwrite(
                    $fp,
                    $storeComment['date'].$sp.$storeComment['id_customer'].$sp.$storeComment['id_order'].
                    $sp.$storeComment['stars'].$sp.utf8_decode($storeComment['comment']).''.
                    $sp.$storeComment['id_lang'].$sp.$storeComment['active'].$sp.$storeComment['position'].
                    $sp.utf8_decode($storeComment['title']).$sp.utf8_decode($storeComment['answer']).$ln
                );
            }
            fclose($fp);
            if ($storeComments != false) {
                $this->_html .= Module::DisplayConfirmation(
                    $this->l('The CSV file has been successfully generated,').'&nbsp;
                    <a href=../modules/'.$this->name.'/csv/save_store.csv>
                    '.$this->l('click here to download it').'
                    </a>.'
                );
            } else {
                $this->_html .= Module::DisplayError($this->l('There are no store comments to export'));
            }
        }
        // Display error messages
        if ((int)Configuration::get('PS_DISABLE_NON_NATIVE_MODULE') > 0) {
            $this->_html .= $this->displayError(
                $this->l('Non PrestaShop modules are currently disabled on your store.').'&nbsp;'.
                $this->l('Please change the configuration').'&nbsp;
                <a href="index.php?tab=AdminPerformance&token='.$tokenPe.'" target="_blank">
                '.$this->l('here').'
                </a>'
            );
        }
        /*if (!is_dir('../modules/'.$this->name.'/mails/'.$this->iso_lang.'/')) {
            $this->_html .= $this->displayError(
                $this->l('The email templates are missing for this language').' "'.$this->iso_lang
            );
        }*/
        $checkIfEmpty = $this->checkIfEmptyStore();
        if ($checkIfEmpty == false) {
            $this->_html .= $this->displayError(
                '<a href="index.php?controller=AdminLGCommentsStore&token='.$tokenSC.'" target="_blank" class="redtext">
                '.$this->l('You haven\'t received any store reviews at the moment.').'</a>&nbsp;
                '.$this->l('Please take a look at our').'&nbsp;
                <a href="../modules/'.$this->name.'/readme/readme_'.$this->l('en').'.pdf#page=11" target="_blank">
                '.$this->l('PDF documentation').'</a>&nbsp;
                '.$this->l('to see how to send opinion requests to your customers.').''
            );
        }
        $checkIfEmpty2 = $this->checkIfEmptyProduct();
        if ($checkIfEmpty2 == false) {
            $this->_html .= $this->displayError(
                '<a href="index.php?controller=AdminLGCommentsProducts&token='.$tokenPC.'" target="_blank">
                '.$this->l('You haven\'t received any product reviews at the moment.').'</a>&nbsp;
                '.$this->l('Please take a look at our').'&nbsp;
                <a href="../modules/'.$this->name.'/readme/readme_'.$this->l('en').'.pdf#page=11" target="_blank">
                '.$this->l('PDF documentation').'</a>&nbsp;
                '.$this->l('to see how to send opinion requests to your customers.').''
            );
        }
        $checkIfEmpty3 = $this->checkIfEmptyStatus();
        if ($checkIfEmpty3 == false) {
            $this->_html .= $this->displayError(
                $this->l('You must select at least one order status').'&nbsp;'.
                $this->l('to be able to send opinion requests to your customers.')
            );
        }
        $checkIfEmpty4 = $this->checkIfEmptyGroup();
        if ($checkIfEmpty4 == false) {
            $this->_html .= $this->displayError(
                $this->l('You must select at least one group of customers').'&nbsp;'.
                $this->l('to be able to send opinion requests to your customers.')
            );
        }
        $checkIfEmpty5 = $this->checkIfEmptyMultistore();
        if ($checkIfEmpty5 == false) {
            $this->_html .= $this->displayError(
                $this->l('You must select at least one shop').'&nbsp;'.
                $this->l('to be able to send opinion requests to your customers.')
            );
        }
        // Check if the widget is inserted into several hooks at the time
        $moduleHook = $this->getHookList();
        $hookFooter = 0;
        $hookLeft = 0;
        $hookRight = 0;
        foreach ($moduleHook as $mHook) {
            if (in_array('displayFooter', $mHook)) {
                $hookFooter++;
            } if (in_array('displayLeftColumn', $mHook)) {
                $hookLeft++;
            } if (in_array('displayRightColumn', $mHook)) {
                $hookRight++;
            }
        }
        $widgetHook = $hookFooter + $hookLeft + $hookRight;
        if ($hookFooter) {
            $hookFooterName = ' - displayFooter';
        } else {
            $hookFooterName = '';
        } if ($hookLeft) {
            $hookLeftName = ' - displayLeftColumn';
        } else {
            $hookLeftName = '';
        } if ($hookRight) {
            $hookRightName = ' - displayRightColumn';
        } else {
            $hookRightName = '';
        }
        if ($widgetHook > 1) {
            $this->_html .= $this->displayError(
                $this->l('The store widget is called in several hooks at once').
                '&nbsp;('.$hookFooterName.$hookLeftName.$hookRightName.').&nbsp;'.
                $this->l('Please remove it from the extra hooks').
                '<a href="index.php?tab=AdminModulesPositions&token='.$tokenM.'" target="_blank">
                '.$this->l('here').'</a>&nbsp;'.
                $this->l('(the widget can only be called in one hook at once).')
            );
        }
        $customerGroups = $this->getCustomerGroups();
        $shopList = $this->getShops();
        Tools::getAdminToken(
            'AdminLGCommentsProducts'.
            (int)(Tab::getIdFromClassName('AdminLGCommentsProducts')).
            (int)($this->context->employee->id)
        );
        $curr = $this->getCurrencySign();

        // Check orders
        $days1 = Configuration::get('PS_LGCOMMENTS_DIAS');
        $days2 = Configuration::get('PS_LGCOMMENTS_DIAS2');
        $days3 = Configuration::get('PS_LGCOMMENTS_DAYS_AFTER');
        $days4 = $days2 + $days3;
        $date1 = new DateTime('now');
        $date1->sub(new DateInterval('P'.$days1.'D'));
        $date2 = new DateTime('now');
        $date2->sub(new DateInterval('P'.$days2.'D'));
        $date3 = new DateTime('now');
        $date3->sub(new DateInterval('P'.$days4.'D'));
        $orderList = $this->getCorrespondingOrders($days1, $days2);
        $allStatus = $this->getCorrespondingStatus();
        $allGroups = $this->getCorrespondingGroups();
        $allShops = $this->getCorrespondingShops();
        $modpath = _PS_BASE_URL_._MODULE_DIR_.$this->name;

        // menu bar
        $this->_html .= '
        <div id="menubar">
            <fieldset>
                <a id="button-general-config" class="button btn btn-default lgmenu ">
                    <i class="icon-star"></i>&nbsp;'.$this->l('Ratings').'
                </a>
                <a id="button-store-widget" class="button btn btn-default lgmenu">
                    <i class="icon-picture-o"></i>&nbsp;'.$this->l('Store widget').'
                </a>
                <a id="button-homepage" class="button btn btn-default lgmenu">
                    <i class="icon-play-circle-o"></i>&nbsp;'.$this->l('Homepage slider').'
                </a>
                <a id="button-review-page" class="button btn btn-default lgmenu">
                    <i class="icon-comment-o"></i>&nbsp;'.$this->l('Store review page').'
                </a>
                <a id="button-product-reviews" class="button btn btn-default lgmenu">
                    <i class="icon-comment"></i>&nbsp;'.$this->l('Product reviews').'
                </a>
                <a id="button-rich-snippets" class="button btn btn-default lgmenu">
                    <i class="icon-google"></i>&nbsp;'.$this->l('Google Rich Snippets').'
                </a><br><br>
                <a id="button-send-email" class="button btn btn-default lgmenu">
                    <i class="icon-envelope"></i>&nbsp;'.$this->l('Send emails').'
                </a>
                <a id="button-configure-email" class="button btn btn-default lgmenu">
                    <i class="icon-wrench"></i>&nbsp;'.$this->l('Configure emails').'
                </a>
                <a id="button-order-list" class="button btn btn-default lgmenu">
                    <i class="icon-shopping-cart"></i>&nbsp;'.$this->l('Corresponding orders').'
                </a>
                <a id="button-upload-store" class="button btn btn-default lgmenu">
                    <i class="icon-comments-o"></i>&nbsp;'.$this->l('Import store reviews').'
                </a>
                <a id="button-upload-products" class="button btn btn-default lgmenu">
                    <i class="icon-comments"></i>&nbsp;'.$this->l('Import product reviews').'
                </a>
                <a id="button-manage-reviews" class="button btn btn-default lgmenu">
                    <i class="icon-pencil"></i>&nbsp;'.$this->l('Manage reviews').'
                </a>
            </fieldset>
        </div>';

        // RATINGS
        $this->_html .= '
		<div id="general-config">
        <form action="'.htmlentities($_SERVER['REQUEST_URI']).'" method="post">
        <fieldset>
            <legend>
                <a href="../modules/'.$this->name.'/readme/readme_'.$this->l('en').'.pdf#page=4" target="_blank">
                    <span class="lglarge"><i class="icon-star"></i>
                        &nbsp;'.$this->l('Ratings').'
                        &nbsp;<img src="../modules/'.$this->name.'/views/img/info.png">
                    </span>
                </a>
            </legend><br>
            <h3><label>&nbsp;&nbsp; '.$this->l('Choose the design of your stars:').'</label></h3>
            <div class="lgoverflow">
            <table class="table" style="max-width:1000px;">
                <tr>
                    <td class="lgmenu lgupper lgcenter lgbold">'.$this->l('Style').'</td>
                    <td class="lgmenu lgupper lgcenter lgbold">'.$this->l('Color').'</td>
                    <td class="lgmenu lgupper lgcenter lgbold">'.$this->l('Size').'</td>
                    <td class="lgmenu lgupper lgcenter lgbold">'.$this->l('Preview').'</td>
                </tr>
                <tr>
                    <td class="lgmenu">
                        <select name="stardesign1" id="stardesign1" class="lgmenu" onchange="changeStar(this.value);">
                        <option '.((Configuration::get('PS_LGCOMMENTS_STARDESIGN1') == "circle")
                        ? 'selected="selected"' : '').' value="circle">'.$this->l('Circle').'
                        </option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_STARDESIGN1') == "plain")
                        ? 'selected="selected"' : '').' value="plain">'.$this->l('Plain').'
                        </option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_STARDESIGN1') == "square")
                        ? 'selected="selected"' : '').' value="square">'.$this->l('Square').'
                        </option>
                        </select>
                    </td>
                    <td class="lgmenu">
                        <select name="stardesign2" id="stardesign2" class="lgmenu" onchange="changeStar(this.value);">
                        <option '.((Configuration::get('PS_LGCOMMENTS_STARDESIGN2') == "yellow")
                        ? 'selected="selected"' : '').' value="yellow">'.$this->l('Yellow').'
                        </option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_STARDESIGN2') == "orange")
                        ? 'selected="selected"' : '').' value="orange">'.$this->l('Orange').'
                        </option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_STARDESIGN2') == "red")
                        ? 'selected="selected"' : '').' value="red">'.$this->l('Red').'
                        </option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_STARDESIGN2') == "pink")
                        ? 'selected="selected"' : '').' value="pink">'.$this->l('Pink').'
                        </option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_STARDESIGN2') == "purple")
                        ? 'selected="selected"' : '').' value="purple">'.$this->l('Purple').'
                        </option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_STARDESIGN2') == "greenlight")
                        ? 'selected="selected"' : '').' value="greenlight">'.$this->l('Green light').'
                        </option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_STARDESIGN2') == "bluelight")
                        ? 'selected="selected"' : '').' value="bluelight">'.$this->l('Blue light').'
                        </option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_STARDESIGN2') == "bluedark")
                        ? 'selected="selected"' : '').' value="bluedark">'.$this->l('Blue dark').'
                        </option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_STARDESIGN2') == "grey")
                        ? 'selected="selected"' : '').' value="grey">'.$this->l('Grey').'
                        </option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_STARDESIGN2') == "black")
                        ? 'selected="selected"' : '').' value="black">'.$this->l('Black').'
                        </option>
                        </select>
                    </td>
                    <td class="lgmenu">
                        <select name="starsize" id="starsize" class="lgmenu" onchange="changeStar(this.value);">
                        <option '.((Configuration::get('PS_LGCOMMENTS_STARSIZE') == 80) ? 'selected="selected"' : '').'
                        value="80">80px</option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_STARSIZE') == 90) ? 'selected="selected"' : '').'
                        value="90">90px</option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_STARSIZE') == 100) ? 'selected="selected"' : '').'
                        value="100">100px</option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_STARSIZE') == 110) ? 'selected="selected"' : '').'
                        value="110">110px</option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_STARSIZE') == 120) ? 'selected="selected"' : '').'
                        value="120">120px</option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_STARSIZE') == 130) ? 'selected="selected"' : '').'
                        value="130">130px</option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_STARSIZE') == 140) ? 'selected="selected"' : '').'
                        value="140">140px</option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_STARSIZE') == 150) ? 'selected="selected"' : '').'
                        value="150">150px</option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_STARSIZE') == 160) ? 'selected="selected"' : '').'
                        value="160">160px</option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_STARSIZE') == 170) ? 'selected="selected"' : '').'
                        value="170">170px</option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_STARSIZE') == 180) ? 'selected="selected"' : '').'
                        value="180">180px</option>
                        </select>
                    </td>
                    <td class="lgmenu lgcenter">
                        <img src="'.$modpath.'/views/img/stars/'.
                        Configuration::get('PS_LGCOMMENTS_STARDESIGN1').'/'.
                        Configuration::get('PS_LGCOMMENTS_STARDESIGN2').'/9stars.png"
                        id="stardesignimage" class="lgwidget lgcenter"
                        width="'.Configuration::get('PS_LGCOMMENTS_STARSIZE').'">
                    </td>
                </tr>
            </table>
            </div>
            <div class="lgclear"></div><br><br><br>
            <h3>
                <span class="lgfloat">
                    <label>&nbsp;&nbsp;'.$this->l('Choose your rating scale (front-office only)').'&nbsp;&nbsp;</label>
                </span>
                <select id="PS_LGCOMMENTS_SCALE" class="lgfloat fixed-width-xl" name="PS_LGCOMMENTS_SCALE">
                    <option '.((Configuration::get('PS_LGCOMMENTS_SCALE') == 5) ? 'selected="selected"' : '').'
                    value="5" onclick="enableField()">'.$this->l('From 0 to 5 (ex: 4,5/5)').'</option>
                    <option '.((Configuration::get('PS_LGCOMMENTS_SCALE') == 10) ? 'selected="selected"' : '').'
                    value="10" onclick="disableField()">'.$this->l('From 0 to 10 (ex: 9/10)').'</option>
                    <option '.((Configuration::get('PS_LGCOMMENTS_SCALE') == 20) ? 'selected="selected"' : '').'
                    value="20" onclick="enableField()">'.$this->l('From 0 to 20 (ex: 18/20)').'</option>
                </select>
            </h3>
            <div class="lgclear"></div><br><br>
            <h3>
                <label class="lgfloat">
                    &nbsp;&nbsp;
                    '.$this->l('Display stars for the products without review').'
                    &nbsp;&nbsp;
                </label>
                <span class="switch prestashop-switch fixed-width-lg lgfloat">
                    <input type="radio" name="PS_LGCOMMENTS_DISPLAY_ZEROSTAR" id="lgcomments_display_zerostar_on"
                    value="1" '.(Configuration::get('PS_LGCOMMENTS_DISPLAY_ZEROSTAR') == 1
                    ? 'checked="checked"' : '').' />
                    <label for="lgcomments_display_zerostar_on" class="lgbutton">'.$this->l('Yes').'</label>
                    <input type="radio" name="PS_LGCOMMENTS_DISPLAY_ZEROSTAR" id="lgcomments_display_zerostar_off"
                    value="0" '.(Configuration::get('PS_LGCOMMENTS_DISPLAY_ZEROSTAR') == 0
                    ? 'checked="checked"' : '').' />
                    <label for="lgcomments_display_zerostar_off" class="lgbutton">'.$this->l('No').'</label>
                    <a class="slide-button btn"></a>
                </span>
                <span class="lgfloat" style="margin-left:20px;">
                    <img src="'.$modpath.'/views/img/stars/'.
                    Configuration::get('PS_LGCOMMENTS_STARDESIGN1').'/'.
                    Configuration::get('PS_LGCOMMENTS_STARDESIGN2').'/0stars.png"
                    id="starzero" class="lgwidget lgcenter"
                    width="'.Configuration::get('PS_LGCOMMENTS_STARSIZE').'">
                </span>
            </h3>
            <div class="lgclear"></div><br><br>
            <h3>
            <span class="lgfloat">
                <label>&nbsp;&nbsp;
                    '.$this->l('Margin around stars (product list):').'&nbsp;
                </label>
            </span>
            <span class="lgfloat">
                &nbsp;&nbsp;'.$this->l('Top').'&nbsp;
            </span>
            <span class="lgfloat lgbutton input-group">
                <span class="input-group-addon">px
                </span>
                <input id="PS_LGCOMMENTS_CATTOPMARGIN" type="text"
                value="'.(int)Configuration::get('PS_LGCOMMENTS_CATTOPMARGIN').'" name="PS_LGCOMMENTS_CATTOPMARGIN">
            </span>
            <span class="lgfloat">
                &nbsp;&nbsp;'.$this->l('Bottom').'&nbsp;
            </span>
            <span class="lgfloat lgbutton input-group">
                <span class="input-group-addon">px
                </span>
                <input id="PS_LGCOMMENTS_CATBOTMARGIN" type="text"
                value="'.(int)Configuration::get('PS_LGCOMMENTS_CATBOTMARGIN').'" name="PS_LGCOMMENTS_CATBOTMARGIN">
            </span>
            </h3>
            <div class="lgclear"></div><br><br>
            <h3>
            <span class="lgfloat">
                <label>&nbsp;&nbsp;
                    '.$this->l('Margin around stars (top of product sheet):').'&nbsp;
                </label>
            </span>
            <span class="lgfloat">
                &nbsp;&nbsp;'.$this->l('Top').'&nbsp;
            </span>
            <span class="lgfloat lgbutton input-group">
                <span class="input-group-addon">px
                </span>
                <input id="PS_LGCOMMENTS_PRODTOPMARGIN" type="text"
                value="'.(int)Configuration::get('PS_LGCOMMENTS_PRODTOPMARGIN').'" name="PS_LGCOMMENTS_PRODTOPMARGIN">
            </span>
            <span class="lgfloat">
                &nbsp;&nbsp;'.$this->l('Bottom').'&nbsp;
            </span>
            <span class="lgfloat lgbutton input-group">
                <span class="input-group-addon">px
                </span>
                <input id="PS_LGCOMMENTS_PRODBOTMARGIN" type="text"
                value="'.(int)Configuration::get('PS_LGCOMMENTS_PRODBOTMARGIN').'" name="PS_LGCOMMENTS_PRODBOTMARGIN">
            </span>
            </h3>
            <div class="lgclear"></div>
            <br>
            <button class="button btn btn-default" type="submit" name="submitLGCommentsGeneral">
                <i class="process-icon-save"></i>'.$this->l('Save').'
            </button>
        </fieldset>
        </form>
        </div>';

        // STORE WIDGET
        $this->_html .= '
		<div id="store-widget">
            <form action="'.htmlentities($_SERVER['REQUEST_URI']).'" method="post">
			<fieldset>
				<legend>
                    <a href="../modules/'.$this->name.'/readme/readme_'.$this->l('en').'.pdf#page=5" target="_blank">
                        <span class="lglarge"><i class="icon-picture-o"></i>
                            &nbsp;'.$this->l('Store widget').'
                            &nbsp;<img src="../modules/'.$this->name.'/views/img/info.png">
                        </span>
                    </a>
                </legend><br>
                <h3>
                    <span class="lgfloat"><label>&nbsp;&nbsp;
                        '.$this->l('Display the store widget').'&nbsp;&nbsp;
                    </label></span>
                    <span class="lgfloat switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="lgcomments_display" id="lgcomments_display_on" value="1"
                        '.(Configuration::get('PS_LGCOMMENTS_DISPLAY') == 1 ? 'checked="checked"' : '').' />
                        <label for="lgcomments_display_on" class="lgbutton">'.$this->l('Yes').'</label>
                        <input type="radio" name="lgcomments_display" id="lgcomments_display_off" value="0"
                        '.(Configuration::get('PS_LGCOMMENTS_DISPLAY') == 0 ? 'checked="checked"' : '').' />
                        <label for="lgcomments_display_off" class="lgbutton">'.$this->l('No').'</label>
                        <a class="slide-button btn"></a>
                    </span>
                </h3>
                <div class="lgclear"></div><br><br>
                <h3>
                    <span class="lgfloat"><label>&nbsp;&nbsp;
                        '.$this->l('Where to display the widget').'&nbsp;&nbsp;
                    </label></span>
                    <select id="PS_LGCOMMENTS_DISPLAY_TYPE" class="lgfloat fixed-width-xl"
                    name="PS_LGCOMMENTS_DISPLAY_TYPE" style="margin-right:10px;">
                        <option '.((Configuration::get('PS_LGCOMMENTS_DISPLAY_TYPE') == 1)
                        ? 'selected="selected"' : '').' value="1" onclick="enableField()">
                            '.$this->l('On the side of the screen').'
                        </option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_DISPLAY_TYPE') == 2)
                        ? 'selected="selected"' : '').'  value="2" onclick="disableField()">
                            '.$this->l('Inside a column or footer').'
                        </option>
                    </select>
                    <select id="PS_LGCOMMENTS_DISPLAY_SIDE" class="lgfloat fixed-width-xl"
                    name="PS_LGCOMMENTS_DISPLAY_SIDE"';
        if (Configuration::get('PS_LGCOMMENTS_DISPLAY_TYPE') == 2) {
            $this->_html .= 'disabled';
        }
        $this->_html .= '>
                    <option '.((Configuration::get('PS_LGCOMMENTS_DISPLAY_SIDE') == 1)
                    ? 'selected="selected"' : '').' value="1">
                        '.$this->l('Top left').'
                    </option>
                    <option '.((Configuration::get('PS_LGCOMMENTS_DISPLAY_SIDE') == 2)
                    ? 'selected="selected"' : '').' value="2">
                        '.$this->l('Middle left').'
                    </option>
                    <option '.((Configuration::get('PS_LGCOMMENTS_DISPLAY_SIDE') == 3)
                    ? 'selected="selected"' : '').' value="3">
                        '.$this->l('Bottom left').'
                    </option>
                    <option '.((Configuration::get('PS_LGCOMMENTS_DISPLAY_SIDE') == 4)
                    ? 'selected="selected"' : '').' value="4">
                        '.$this->l('Top right').'
                    </option>
                    <option '.((Configuration::get('PS_LGCOMMENTS_DISPLAY_SIDE') == 5)
                    ? 'selected="selected"' : '').' value="5">
                        '.$this->l('Middle right').'
                    </option>
                    <option '.((Configuration::get('PS_LGCOMMENTS_DISPLAY_SIDE') == 6)
                    ? 'selected="selected"' : '').' value="6">
                        '.$this->l('Bottom right').'
                    </option>
                    </select>
                </h3>
                <div class="alert alert-info">
                    <p>'.$this->l('The widget is currently inserted into the hook:').'&nbsp;';
        foreach ($moduleHook as $mHook) {
            if (in_array('displayFooter', $mHook)) {
                    $this->_html .= '- <b>displayFooter</b> ';
            } if (in_array('displayLeftColumn', $mHook)) {
                    $this->_html .= '- <b>displayLeftColumn</b> ';
            } if (in_array('displayRightColumn', $mHook)) {
                    $this->_html .= '- <b>displayRightColumn</b> ';
            }
        }
        $this->_html .= '
                    </p>
                    <p>
                    '.$this->l('If you want to change the widget hook,').'&nbsp;
                    <a href="index.php?controller=AdminModulesPositions&addToHook&token='.$tokenM.'" target="_blank">
                    &nbsp;'.$this->l('go to this page and insert it into the hook').'
                    &nbsp;'.$this->l('"displayFooter", "displayLeftColumn" or "displayRightColumn"').'.
                    </a>
                    </p>
                </div>
                <div class="lgclear"></div>
                <script type="text/javascript">
                function disableField() {
                document.getElementById("PS_LGCOMMENTS_DISPLAY_SIDE").disabled=true;
                document.getElementById("lgcomments_display_cross_on").disabled=true;
                document.getElementById("lgcomments_display_cross_off").disabled=true;
                }
                function enableField() {
                document.getElementById("PS_LGCOMMENTS_DISPLAY_SIDE").disabled=false;
                document.getElementById("lgcomments_display_cross_on").disabled=false;
                document.getElementById("lgcomments_display_cross_off").disabled=false;
                }
                </script>
                <div class="lgclear"></div><br><br>
                <h3><label>
                    &nbsp;&nbsp; '.$this->l('Choose the design of your store widget:').'
                </label></h3>
                <div class="lgoverflow">
                <table class="table" style="max-width:1000px;">
                    <tr>
                        <td class="lgmenu lgupper lgcenter lgbold">'.$this->l('Style').'</td>
                        <td class="lgmenu lgupper lgcenter lgbold">'.$this->l('Color').'</td>
                        <td class="lgmenu lgupper lgcenter lgbold">'.$this->l('Preview').'</td>
                    </tr>
                    <tr>
                        <td class="lgmenu">
                            <select name="bgdesign1" id="bgdesign1" class="lgmenu" onchange="changeBg(this.value);">
                            <option '.((Configuration::get('PS_LGCOMMENTS_BGDESIGN1') == "bubble")
                            ? 'selected="selected"' : '').' value="bubble">'.$this->l('Bubble').'
                            </option>
                            <option '.((Configuration::get('PS_LGCOMMENTS_BGDESIGN1') == "customer")
                            ? 'selected="selected"' : '').' value="customer">'.$this->l('Customer').'
                            </option>
                            <option '.((Configuration::get('PS_LGCOMMENTS_BGDESIGN1') == "horizontal")
                            ? 'selected="selected"' : '').' value="horizontal">'.$this->l('Horizontal').'
                            </option>
                            <option '.((Configuration::get('PS_LGCOMMENTS_BGDESIGN1') == "letter")
                            ? 'selected="selected"' : '').' value="letter">'.$this->l('Letter').'
                            </option>
                            <option '.((Configuration::get('PS_LGCOMMENTS_BGDESIGN1') == "pentagon")
                            ? 'selected="selected"' : '').' value="pentagon">'.$this->l('Pentagon').'
                            </option>
                            <option '.((Configuration::get('PS_LGCOMMENTS_BGDESIGN1') == "shop")
                            ? 'selected="selected"' : '').' value="shop">'.$this->l('Shop').'
                            </option>
                            <option '.((Configuration::get('PS_LGCOMMENTS_BGDESIGN1') == "vertical")
                            ? 'selected="selected"' : '').' value="vertical">'.$this->l('Vertical').'
                            </option>
                            </select>
                        </td>
                        <td class="lgmenu">
                            <select name="bgdesign2" id="bgdesign2" class="lgmenu" onchange="changeBg(this.value);">
                            <option '.((Configuration::get('PS_LGCOMMENTS_BGDESIGN2') == "yellow")
                            ? 'selected="selected"' : '').' value="yellow">'.$this->l('Yellow').'
                            </option>
                            <option '.((Configuration::get('PS_LGCOMMENTS_BGDESIGN2') == "orange")
                            ? 'selected="selected"' : '').' value="orange">'.$this->l('Orange').'
                            </option>
                            <option '.((Configuration::get('PS_LGCOMMENTS_BGDESIGN2') == "red")
                            ? 'selected="selected"' : '').' value="red">'.$this->l('Red').'
                            </option>
                            <option '.((Configuration::get('PS_LGCOMMENTS_BGDESIGN2') == "pink")
                            ? 'selected="selected"' : '').' value="pink">'.$this->l('Pink').'
                            </option>
                            <option '.((Configuration::get('PS_LGCOMMENTS_BGDESIGN2') == "greenlight")
                            ? 'selected="selected"' : '').' value="greenlight">'.$this->l('Green light').'
                            </option>
                            <option '.((Configuration::get('PS_LGCOMMENTS_BGDESIGN2') == "greendark")
                            ? 'selected="selected"' : '').' value="greendark">'.$this->l('Green dark').'
                            </option>
                            <option '.((Configuration::get('PS_LGCOMMENTS_BGDESIGN2') == "bluelight")
                            ? 'selected="selected"' : '').' value="bluelight">'.$this->l('Blue light').'
                            </option>
                            <option '.((Configuration::get('PS_LGCOMMENTS_BGDESIGN2') == "bluedark")
                            ? 'selected="selected"' : '').' value="bluedark">'.$this->l('Blue dark').'
                            </option>
                            <option '.((Configuration::get('PS_LGCOMMENTS_BGDESIGN2') == "purple")
                            ? 'selected="selected"' : '').' value="purple">'.$this->l('Purple').'
                            </option>
                            <option '.((Configuration::get('PS_LGCOMMENTS_BGDESIGN2') == "brownlight")
                            ? 'selected="selected"' : '').' value="brownlight">'.$this->l('Brown light').'
                            </option>
                            <option '.((Configuration::get('PS_LGCOMMENTS_BGDESIGN2') == "browndark")
                            ? 'selected="selected"' : '').' value="browndark">'.$this->l('Brown dark').'
                            </option>
                            <option '.((Configuration::get('PS_LGCOMMENTS_BGDESIGN2') == "beige")
                            ? 'selected="selected"' : '').' value="beige">'.$this->l('Beige').'
                            </option>
                            <option '.((Configuration::get('PS_LGCOMMENTS_BGDESIGN2') == "greylight")
                            ? 'selected="selected"' : '').' value="greylight">'.$this->l('Grey light').'
                            </option>
                            <option '.((Configuration::get('PS_LGCOMMENTS_BGDESIGN2') == "greydark")
                            ? 'selected="selected"' : '').' value="greydark">'.$this->l('Grey dark').'
                            </option>
                            <option '.((Configuration::get('PS_LGCOMMENTS_BGDESIGN2') == "black")
                            ? 'selected="selected"' : '').' value="black">'.$this->l('Black').'
                            </option>
                            </select>
                        </td>
                        <td class="lgmenu lgcenter">
                            <img src="'.$modpath.'/views/img/bg/'.
                            Configuration::get('PS_LGCOMMENTS_BGDESIGN1').'-'.
                            Configuration::get('PS_LGCOMMENTS_BGDESIGN2').'.png"
                            id="bgdesignimage" class="lgwidget">
                        </td>
                    </tr>
                </table>
                </div>
                <br><br>
                <h3>
                    <span class="lgfloat"><label>
                        &nbsp;&nbsp;'.$this->l('Display a cross to hide the widget').'&nbsp;&nbsp;
                    </label></span>
                    <span class="switch prestashop-switch fixed-width-lg lgfloat">
                        <input type="radio" name="lgcomments_display_cross" id="lgcomments_display_cross_on"
                        value="1" '.(Configuration::get('PS_LGCOMMENTS_CROSS') == 1 ? 'checked="checked"' : '').' />
                        <label for="lgcomments_display_cross_on" class="lgbutton">'.$this->l('Yes').'</label>
                        <input type="radio" name="lgcomments_display_cross" id="lgcomments_display_cross_off"
                        value="0" '.(Configuration::get('PS_LGCOMMENTS_CROSS') == 0 ? 'checked="checked"' : '').' />
                        <label for="lgcomments_display_cross_off" class="lgbutton">'.$this->l('No').'</label>
                        <a class="slide-button btn"></a>
                    </span>
                </h3>
                <div class="lgclear"></div><br><br>
                <h3>
                <span class="lgfloat"><label>
                    &nbsp;&nbsp;'.$this->l('Color of the widget title').'&nbsp;&nbsp;
                </label></span>
                <span class="lgbutton lgfloat"">
                <input id="widget_text_color" name="widget_text_color" class="color" type="text"
                value="'.Configuration::get('PS_LGCOMMENTS_TEXTCOLOR').'">
                </span>
                </h3>
                <button class="button btn btn-default" type="submit" name="submitLGCommentsWidget">
                    <i class="process-icon-save"></i>'.$this->l('Save').'
                </button>
            </fieldset>
            </form>
        </div>';

        // HOMEPAGE
        $this->_html .= '
		<div id="homepage">
            <form action="'.htmlentities($_SERVER['REQUEST_URI']).'" method="post">
			<fieldset>
				<legend>
                    <a href="../modules/'.$this->name.'/readme/readme_'.$this->l('en').'.pdf#page=6" target="_blank">
                        <span class="lglarge"><i class="icon-play-circle-o"></i>
                            &nbsp;'.$this->l('Homepage slider').'
                            &nbsp;<img src="../modules/'.$this->name.'/views/img/info.png">
                        </span>
                    </a>
                </legend><br>
                <h3>
                    <label class="lgfloat">
                        &nbsp;&nbsp;'.$this->l('Display store review slider').'
                        &nbsp;('.$this->l('PS 1.6 only').')&nbsp;&nbsp;
                    </label>
                    <span class="switch prestashop-switch fixed-width-lg lgfloat">
                        <input type="radio" name="PS_LGCOMMENTS_DISPLAY_SLIDER" id="lgcomments_display_slider_on"
                        value="1" '.(Configuration::get('PS_LGCOMMENTS_DISPLAY_SLIDER') == 1
                        ? 'checked="checked"' : '').' />
                        <label for="lgcomments_display_slider_on" class="lgbutton">'.$this->l('Yes').'</label>
                        <input type="radio" name="PS_LGCOMMENTS_DISPLAY_SLIDER" id="lgcomments_display_slider_off"
                        value="0" '.(Configuration::get('PS_LGCOMMENTS_DISPLAY_SLIDER') == 0
                        ? 'checked="checked"' : '').' />
                        <label for="lgcomments_display_slider_off" class="lgbutton">'.$this->l('No').'</label>
                        <a class="slide-button btn"></a>
                    </span>
                </h3>
                <div class="lgclear"></div><br><br>
                <h3>
                    <span class="lgfloat"><label>&nbsp;&nbsp;
                        '.$this->l('Number of review blocks in the slider').'&nbsp;&nbsp;
                    </label></span>
                    <select id="PS_LGCOMMENTS_SLIDER_BLOCKS" class="lgfloat fixed-width-xl"
                    name="PS_LGCOMMENTS_SLIDER_BLOCKS">
                        <option '.((Configuration::get('PS_LGCOMMENTS_SLIDER_BLOCKS') == 1)
                        ? 'selected="selected"' : '').' value="1">1</option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_SLIDER_BLOCKS') == 2)
                        ? 'selected="selected"' : '').'  value="2">2</option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_SLIDER_BLOCKS') == 3)
                        ? 'selected="selected"' : '').' value="3">3</option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_SLIDER_BLOCKS') == 4)
                        ? 'selected="selected"' : '').'  value="4">4</option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_SLIDER_BLOCKS') == 5)
                        ? 'selected="selected"' : '').' value="5">5</option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_SLIDER_BLOCKS') == 6)
                        ? 'selected="selected"' : '').'  value="6">6</option>
                    </select>
                </h3>
                <div class="lgclear"></div><br><br>
                <h3>
                <span class="lgfloat"><label>&nbsp;&nbsp;
                    '.$this->l('Display the last').'&nbsp;&nbsp;
                </label></span>
                <span class="lgfloat lgbutton">
                <input id="PS_LGCOMMENTS_SLIDER_TOTAL" type="text"
                value="'.(int)Configuration::get('PS_LGCOMMENTS_SLIDER_TOTAL').'" name="PS_LGCOMMENTS_SLIDER_TOTAL">
                </span>
                <span class="lgfloat">
                    <label>&nbsp;&nbsp;'.$this->l('reviews in the slider').'&nbsp;&nbsp;</label>
                </span>
                </h3>
                <button class="button btn btn-default" type="submit" name="submitLGCommentsHomepage">
                    <i class="process-icon-save"></i>'.$this->l('Save').'
                </button>
            </fieldset>
            </form>
        </div>';

        // STORE REVIEW PAGE
        $this->_html .= '
		<div id="review-page">
            <form action="'.htmlentities($_SERVER['REQUEST_URI']).'" method="post">
			<fieldset>
				<legend>
                    <a href="../modules/'.$this->name.'/readme/readme_'.$this->l('en').'.pdf#page=7" target="_blank">
                        <span class="lglarge"><i class="icon-comment-o"></i>
                            &nbsp;'.$this->l('Store review page').'
                            &nbsp;<img src="../modules/'.$this->name.'/views/img/info.png">
                        </span>
                    </a>
                </legend><br>
                <h3>
                    <label class="lgfloat">
                        &nbsp;&nbsp;
                        '.$this->l('Allow customers to leave reviews directly from the store review page').'
                        &nbsp;&nbsp;
                    </label>
                    <span class="switch prestashop-switch fixed-width-lg lgfloat">
                        <input type="radio" name="PS_LGCOMMENTS_STORE_FORM" id="lgcomments_store_form_on" value="1"
                        '.(Configuration::get('PS_LGCOMMENTS_STORE_FORM') == 1 ? 'checked="checked"' : '').' />
                        <label for="lgcomments_store_form_on" class="lgbutton">'.$this->l('Yes').'</label>
                        <input type="radio" name="PS_LGCOMMENTS_STORE_FORM" id="lgcomments_store_form_off" value="0"
                        '.(Configuration::get('PS_LGCOMMENTS_STORE_FORM') == 0 ? 'checked="checked"' : '').' />
                        <label for="lgcomments_store_form_off" class="lgbutton">'.$this->l('No').'</label>
                        <a class="slide-button btn"></a>
                    </span>
                </h3>
                <div class="lgclear"></div><br><br>
                <h3>
                    <span class="lgfloat">
                        <label>&nbsp;&nbsp;'.$this->l('Display summary and filter').'&nbsp;&nbsp;</label>
                    </span>
                    <span class="switch prestashop-switch fixed-width-lg lgfloat">
                        <input type="radio" name="lgcomments_store_filter" id="lgcomments_store_filter_on" value="1"
                        '.(Configuration::get('PS_LGCOMMENTS_STORE_FILTER') == 1 ? 'checked="checked"' : '').' />
                        <label for="lgcomments_store_filter_on" class="lgbutton">'.$this->l('Yes').'</label>
                        <input type="radio" name="lgcomments_store_filter" id="lgcomments_store_filter_off" value="0"
                        '.(Configuration::get('PS_LGCOMMENTS_STORE_FILTER') == 0 ? 'checked="checked"' : '').' />
                        <label for="lgcomments_store_filter_off" class="lgbutton">'.$this->l('No').'</label>
                        <a class="slide-button btn"></a>
                    </span>
                </h3>
                <div class="lgclear"></div><br><br>
                <h3>
                    <label class="lgfloat">&nbsp;&nbsp;
                        '.$this->l('Multilingual shop: Display the store reviews by language').'&nbsp;&nbsp;
                    </label>
                    <span class="switch prestashop-switch fixed-width-lg lgfloat" margin-left:10px;">
                        <input type="radio" name="PS_LGCOMMENTS_DISPLAY_LANGUAGE" id="lgcomments_display_language_on"
                        value="1" '.(Configuration::get('PS_LGCOMMENTS_DISPLAY_LANGUAGE') == 1
                        ? 'checked="checked"' : '').' />
                        <label for="lgcomments_display_language_on" class="lgbutton">'.$this->l('Yes').'</label>
                        <input type="radio" name="PS_LGCOMMENTS_DISPLAY_LANGUAGE" id="lgcomments_display_language_off"
                        value="0" '.(Configuration::get('PS_LGCOMMENTS_DISPLAY_LANGUAGE') == 0
                        ? 'checked="checked"' : '').' />
                        <label for="lgcomments_display_language_off" class="lgbutton">'.$this->l('No').'</label>
                        <a class="slide-button btn"></a>
                    </span>
                </h3>
                <div class="lgclear"></div><br><br>
                <h3><span class="lgfloat">
                    <label>&nbsp;&nbsp;'.$this->l('Text color of the store review blocks').'&nbsp;&nbsp;</label>
                </span>
                <span class="lgfloat lgbutton">
                <input id="store_text_color" name="store_text_color" class="color" type="text"
                value="'.Configuration::get('PS_LGCOMMENTS_TEXTCOLOR2').'">
                </span>
                </h3>
                <div class="lgclear"></div><br><br>
                <h3><span class="lgfloat">
                    <label>&nbsp;&nbsp;'.$this->l('Background color of the store review blocks').'&nbsp;&nbsp;</label>
                </span>
                <span class="lgfloat lgbutton">
                <input id="store_back_color" name="store_back_color" class="color" type="text"
                value="'.Configuration::get('PS_LGCOMMENTS_BACKCOLOR2').'">
                </span>
                </h3>
                <div class="lgclear"></div><br><br>
                <h3>
                <span class="lgfloat">
                    <label>&nbsp;&nbsp;'.$this->l('Number of store reviews displayed by page').'&nbsp;&nbsp;</label>
                </span>
                <span class="lgfloat lgbutton">
                <input id="store_comments_per_page" type="text"
                value="'.(int)Configuration::get('PS_LGCOMMENTS_PER_PAGE').'"
                name="store_comments_per_page">
                </span>
                </h3>
                <div class="lgclear"></div><br><br>
                <h3>
                <label class="lgfloat">
                    &nbsp;&nbsp; '.$this->l('Order of the shop reviews ').'&nbsp;&nbsp;
                </label>
                <select id="PS_LGCOMMENTS_DISPLAY_ORDER" class="lgfloat fixed-width-xl"
                name="PS_LGCOMMENTS_DISPLAY_ORDER">
                    <option '.((Configuration::get('PS_LGCOMMENTS_DISPLAY_ORDER') == 1)
                    ? 'selected="selected"' : '').'  value="1">'.$this->l('Ascending').'</option>
                    <option '.((Configuration::get('PS_LGCOMMENTS_DISPLAY_ORDER') == 2)
                    ? 'selected="selected"' : '').' value="2">'.$this->l('Descending').'</option>
                    </select>
                </h3>
                <button class="button btn btn-default" type="submit" name="submitLGCommentsStore">
                    <i class="process-icon-save"></i>'.$this->l('Save').'
                </button>
            </fieldset>
            </form>
        </div>';

        // PRODUCT REVIEWS
        $this->_html .= '
		<div id="product-reviews">
            <form action="'.htmlentities($_SERVER['REQUEST_URI']).'" method="post">
			<fieldset>
				<legend>
                    <a href="../modules/'.$this->name.'/readme/readme_'.$this->l('en').'.pdf#page=8"
                    target="_blank">
                        <span class="lglarge"><i class="icon-comment"></i>
                            &nbsp;'.$this->l('Product reviews').'
                            &nbsp;<img src="../modules/'.$this->name.'/views/img/info.png">
                        </span>
                    </a>
                </legend><br>
                <h3>
                    <span class="lgfloat">
                        <label>&nbsp;&nbsp;'.$this->l('Display the product reviews').'&nbsp;&nbsp;</label>
                    </span>
                    <span class="switch prestashop-switch fixed-width-lg lgfloat">
                        <input type="radio" name="lgcomments_display_comments" id="lgcomments_display_comments_on"
                        value="1" '.(Configuration::get('PS_LGCOMMENTS_DISPLAY_COMMENTS') == 1
                        ? 'checked="checked"' : '').' />
                        <label for="lgcomments_display_comments_on" class="lgbutton">'.$this->l('Yes').'</label>
                        <input type="radio" name="lgcomments_display_comments" id="lgcomments_display_comments_off"
                        value="0" '.(Configuration::get('PS_LGCOMMENTS_DISPLAY_COMMENTS') == 0
                        ? 'checked="checked"' : '').' />
                        <label for="lgcomments_display_comments_off" class="lgbutton">'.$this->l('No').'</label>
                        <a class="slide-button btn"></a>
                    </span>
                </h3>
                <div class="alert alert-info">
                '.$this->l('You need to have received and enabled at least one review for a product').'
                &nbsp;'.$this->l('to be able to see the \"Reviews\" section on its product sheet.').'
                &nbsp;<a href="index.php?controller=AdminLGCommentsProducts&token='.$tokenPC.'" target="_blank">
                '.$this->l('You can check it here.').'
                </a>
                </div>
                <div class="lgclear"></div><br><br>
                <h3>
                <label class="lgfloat"">
                    &nbsp;&nbsp;'.$this->l('How to display the reviews on the product sheets').'&nbsp;&nbsp;
                </label>
                <select id="PS_LGCOMMENTS_TAB_CONTENT" class="lgfloat fixed-width-xl"
                name="PS_LGCOMMENTS_TAB_CONTENT">
                        <option '.((Configuration::get('PS_LGCOMMENTS_TAB_CONTENT') == 1)
                        ? 'selected="selected"' : '').'  value="1">'.$this->l('In a new block').'</option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_TAB_CONTENT') == 2)
                        ? 'selected="selected"' : '').' value="2">'.$this->l('In a new tab').'</option>
                        </select>
                </h3>
                <div class="lgclear"></div><br><br>
                <h3>
                <label class="lgfloat">
                    &nbsp;&nbsp;
                    '.$this->l('Allow customers to leave reviews directly from the product sheets').'
                    &nbsp;&nbsp;
                </label>
                    <span class="switch prestashop-switch fixed-width-lg lgfloat">
                        <input type="radio" name="PS_LGCOMMENTS_PRODUCT_FORM" id="lgcomments_product_form_on"
                        value="1" '.(Configuration::get('PS_LGCOMMENTS_PRODUCT_FORM') == 1
                        ? 'checked="checked"' : '').' />
                        <label for="lgcomments_product_form_on" class="lgbutton">'.$this->l('Yes').'</label>
                        <input type="radio" name="PS_LGCOMMENTS_PRODUCT_FORM" id="lgcomments_product_form_off"
                        value="0" '.(Configuration::get('PS_LGCOMMENTS_PRODUCT_FORM') == 0
                        ? 'checked="checked"' : '').' />
                        <label for="lgcomments_product_form_off" class="lgbutton">'.$this->l('No').'</label>
                        <a class="slide-button btn"></a>
                    </span>
                </h3>
                <div class="lgclear"></div><br><br>
                <h3>
                <span class="lgfloat">
                    <label>&nbsp;&nbsp;'.$this->l('Display filter').'&nbsp;&nbsp;</label>
                </span>
                <span class="switch prestashop-switch fixed-width-lg lgfloat">
                    <input type="radio" name="lgcomments_product_filter" id="lgcomments_product_filter_on"
                    value="1" '.(Configuration::get('PS_LGCOMMENTS_PRODUCT_FILTER') == 1
                    ? 'checked="checked"' : '').' />
                    <label for="lgcomments_product_filter_on" class="lgbutton">'.$this->l('Yes').'</label>
                    <input type="radio" name="lgcomments_product_filter" id="lgcomments_product_filter_off"
                    value="0" '.(Configuration::get('PS_LGCOMMENTS_PRODUCT_FILTER') == 0
                    ? 'checked="checked"' : '').' />
                    <label for="lgcomments_product_filter_off" class="lgbutton">'.$this->l('No').'</label>
                    <a class="slide-button btn"></a>
                </span>
                <span class="lgfloat">
                    <label>&nbsp;&nbsp;'.$this->l('only when a product has more than').'&nbsp;&nbsp;</label>
                </span>
                <span class="lgfloat lgbutton">
                <input id="lgcomments_product_filter_nb" type="text"
                value="'.(int)Configuration::get('PS_LGCOMMENTS_PRODUCT_FILTER_NB').'"
                name="lgcomments_product_filter_nb">
                </span>
                <span class="lgfloat">
                    <label>&nbsp;&nbsp;'.$this->l('reviews').'&nbsp;&nbsp;</label>
                </span>
                </h3>
                <div class="lgclear"></div><br><br>
                <h3>
                <span class="lgfloat">
                    <label>
                        &nbsp;&nbsp;'.$this->l('Number of product reviews displayed by default').'&nbsp;&nbsp;
                    </label>
                </span>
                <span class="lgfloat lgbutton">
                <input id="lgcomments_display_default" type="text"
                value="'.(int)Configuration::get('PS_LGCOMMENTS_DISPLAY_DEFAULT').'"
                name="lgcomments_display_default">
                </span>
                </h3>
                <div class="lgclear"></div><br><br>
                <h3>
                <span class="lgfloat">
                <label>
                    &nbsp;&nbsp;'.$this->l('Number of extra product reviews (display more)').'&nbsp;&nbsp;
                </label>
                </span>
                <span class="lgfloat lgbutton">
                <input id="lgcomments_display_more" type="text"
                value="'.(int)Configuration::get('PS_LGCOMMENTS_DISPLAY_MORE').'" name="lgcomments_display_more">
                </span>
                </h3>
                <div class="lgclear"></div><br><br>
                <h3>
                    <label class="lgfloat">
                        &nbsp;&nbsp; '.$this->l('Order of the product reviews ').'&nbsp;&nbsp;
                    </label>
                    <select id="PS_LGCOMMENTS_DISPLAY_ORDER2" class="lgfloat fixed-width-xl"
                    name="PS_LGCOMMENTS_DISPLAY_ORDER2">
                        <option '.((Configuration::get('PS_LGCOMMENTS_DISPLAY_ORDER2') == 1)
                        ? 'selected="selected"' : '').'  value="1">'.$this->l('Ascending').'</option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_DISPLAY_ORDER2') == 2)
                        ? 'selected="selected"' : '').' value="2">'.$this->l('Descending').'</option>
                    </select>
                </h3>
                <div class="lgclear"></div><br><br>
                <h3>
                    <label class="lgfloat">
                        &nbsp;&nbsp;
                        '.$this->l('Multilingual shop: Display the product reviews by language').'
                        &nbsp;&nbsp;
                    </label>
                    <span class="switch prestashop-switch fixed-width-lg lgfloat">
                        <input type="radio" name="PS_LGCOMMENTS_DISPLAY_LANGUAGE2" id="lgcomments_display_language2_on"
                        value="1" '.(Configuration::get('PS_LGCOMMENTS_DISPLAY_LANGUAGE2') == 1
                        ? 'checked="checked"' : '').' />
                        <label for="lgcomments_display_language2_on" class="lgbutton">'.$this->l('Yes').'</label>
                        <input type="radio" name="PS_LGCOMMENTS_DISPLAY_LANGUAGE2" id="lgcomments_display_language2_off"
                        value="0" '.(Configuration::get('PS_LGCOMMENTS_DISPLAY_LANGUAGE2') == 0
                        ? 'checked="checked"' : '').' />
                        <label for="lgcomments_display_language2_off" class="lgbutton">'.$this->l('No').'</label>
                        <a class="slide-button btn"></a>
                    </span>
                </h3>
                <div class="lgclear"></div><br>
                <button class="button btn btn-default" type="submit" name="submitLGCommentsProducts">
                    <i class="process-icon-save"></i>'.$this->l('Save').'
                </button>
            </fieldset>
            </form>
        </div>';

        // GOOGLE RICH SNIPPETS
        $this->_html .= '
		<div id="rich-snippets">
            <form action="'.htmlentities($_SERVER['REQUEST_URI']).'" method="post">
			<fieldset>
				<legend>
                    <a href="../modules/'.$this->name.'/readme/readme_'.$this->l('en').'.pdf#page=10" target="_blank">
                        <span class=lglarge"><i class="icon-google"></i>
                            &nbsp;'.$this->l('Google Rich Snippets').'
                            &nbsp;<img src="../modules/'.$this->name.'/views/img/info.png">
                        </span>
                    </a>
                </legend><br>
                <h3>
                    <label class="lgfloat">
                        &nbsp;&nbsp;'.$this->l('Enable snippets for product reviews').'&nbsp;&nbsp;
                    </label>
                    <span class="switch prestashop-switch fixed-width-lg lgfloat">
                        <input type="radio" name="lgcomments_display_snippets2" id="lgcomments_display_snippets2_on"
                        value="1" '.(Configuration::get('PS_LGCOMMENTS_DISPLAY_SNIPPETS2') == 1
                        ? 'checked="checked"' : '').' />
                        <label for="lgcomments_display_snippets2_on" class="lgbutton">'.$this->l('Yes').'</label>
                        <input type="radio" name="lgcomments_display_snippets2" id="lgcomments_display_snippets2_off"
                        value="0" '.(Configuration::get('PS_LGCOMMENTS_DISPLAY_SNIPPETS2') == 0
                        ? 'checked="checked"' : '').' />
                        <label for="lgcomments_display_snippets2_off" class="lgbutton">'.$this->l('No').'</label>
                        <a class="slide-button btn"></a>
                    </span>
                </h3>
                <div class="lgclear"></div><br><br>
                <h3>
                    <label class="lgfloat">
                        &nbsp;&nbsp;'.$this->l('Enable snippets for shop reviews').'&nbsp;&nbsp;
                    </label>
                    <span class="switch prestashop-switch fixed-width-lg lgfloat">
                        <input type="radio" name="lgcomments_display_snippets" id="lgcomments_display_snippets_on"
                        value="1" '.(Configuration::get('PS_LGCOMMENTS_DISPLAY_SNIPPETS') == 1
                        ? 'checked="checked"' : '').' />
                        <label for="lgcomments_display_snippets_on" class="lgbutton">'.$this->l('Yes').'</label>
                        <input type="radio" name="lgcomments_display_snippets" id="lgcomments_display_snippets_off"
                        value="0" '.(Configuration::get('PS_LGCOMMENTS_DISPLAY_SNIPPETS') == 0
                        ? 'checked="checked"' : '').' />
                        <label for="lgcomments_display_snippets_off" class="lgbutton"">'.$this->l('No').'</label>
                        <a class="slide-button btn"></a>
                    </span>
                </h3>
                <div class="lgclear"></div><br><br>
                <h3>
                <label class="lgfloat"">
                    &nbsp;&nbsp;'.$this->l('Choose the price range (for homepage snippets)').'&nbsp;&nbsp;
                </label>
                <select id="PS_LGCOMMENTS_PRICE_RANGE" class="lgfloat fixed-width-xl"
                name="PS_LGCOMMENTS_PRICE_RANGE">
                        <option '.((Configuration::get('PS_LGCOMMENTS_PRICE_RANGE') == ''.$curr.'')
                        ? 'selected="selected"' : '').'  value="'.$curr.'">
                        '.$curr.' - '.$this->l('Low').'</option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_PRICE_RANGE') == ''.$curr.$curr.'')
                        ? 'selected="selected"' : '').' value="'.$curr.$curr.'">
                        '.$curr.$curr.' - '.$this->l('Moderate').'</option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_PRICE_RANGE') == ''.$curr.$curr.$curr.'')
                        ? 'selected="selected"' : '').'  value="'.$curr.$curr.$curr.'">
                        '.$curr.$curr.$curr.' - '.$this->l('High').'</option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_PRICE_RANGE') == ''.$curr.$curr.$curr.$curr.'')
                        ? 'selected="selected"' : '').' value="'.$curr.$curr.$curr.$curr.'">
                        '.$curr.$curr.$curr.$curr.' - '.$this->l('Very high').'</option>
                        </select>
                </h3>
                <div class="lgclear"></div><br>
                <div class="alert alert-info">
                <u><b>'.$this->l('About Google Rich Snippets:').'</b></u><br>
                1 - '.$this->l('Please note that the snippets don\'t appear immediately on Google search results.').
                '&nbsp;'.$this->l('You need to wait until Google bots visit your shop, take the snippets into account').
                '.<br>
                2 - '.$this->l('You can search on Google the website named').
                '&nbsp;'.$this->l('"Structured Data Testing Tool - Google" to test your snippets.').
                '&nbsp;'.$this->l('Submit your urls and make sure there is no error and only one snippet per page.').'
                </div>
                <button class="button btn btn-default" type="submit" name="submitLGCommentsSnippets">
                    <i class="process-icon-save"></i>'.$this->l('Save').'
                </button>
                </form>
			</fieldset>
		</div>';

        // SEND EMAILS
        $this->_html .= '
		<div id="send-email">
        <form action="'.htmlentities($_SERVER['REQUEST_URI']).'" method="post">
			<fieldset>
                <legend>
                    <a href="../modules/'.$this->name.'/readme/readme_'.$this->l('en').'.pdf#page=11" target="_blank">
                        <span class="lglarge"><i class="icon-envelope"></i>
                            &nbsp;'.$this->l('Send emails').'
                            &nbsp;<img src="../modules/'.$this->name.'/views/img/info.png">
                        </span>
                    </a>
                </legend>
                <div class="alert alert-info"><br>
                <h3><label>
                    &nbsp;&nbsp;'.$this->l('Sending emails to your customers is very easy. You just need to:').'
                </label></h3>
                <p>
                    '.$this->l('1 - Go to the tab').
                    '&nbsp;<span class="lgbold lgupper">'.$this->l('Configure emails').
                    '</span>&nbsp;'.$this->l('and choose for which orders you want to send emails.').'
                </p>
                <p>
                    '.$this->l('2 - Go to the tab').
                    '&nbsp;<span class="lgbold lgupper">'.$this->l('Corresponding orders').
                    '</span>&nbsp;'.$this->l('and make sure that you have orders that correspond').
                    '&nbsp;'.$this->l('(emails are sent only for the orders that correspond to').
                    '&nbsp;<span class="lgunderline">'.$this->l('ALL the selected criteria').
                    '</span>).
                </p>
                <p>
                    '.$this->l('3 - Then click on the').
                    '&nbsp;<span class="lgbold lgupper">'.$this->l('Cron URL').'</span>&nbsp;'.
                    $this->l('below, you will get a blank page and the emails will be automatically sent').
                    '&nbsp;'.$this->l('(if an email has already been sent for an order, it won\'t be sent again).').'
                    
                </p>
                <p>
                    '.$this->l('4 - You will get a confirmation at the email indicated below').
                    '&nbsp;'.$this->l('telling you for which orders the emails have been sent.').'
                </p>
                <p>
                    '.$this->l('5 - Wait until your customers leave a comment,').
                    '&nbsp;'.$this->l('you will receive a notification by email every time a comment is posted.').'
                </p>
                <p>
                    '.$this->l('6 - Once you have received comments, you can publish them').'&nbsp;
                    <a href="index.php?controller=AdminLGCommentsStore&token='.$tokenSC.'" target="_blank">
                    '.$this->l('from this page (for shop reviews)').'
                    </a>
                    &nbsp;'.$this->l('and').'&nbsp;
                    <a href="index.php?controller=AdminLGCommentsProducts&token='.$tokenPC.'" target="_blank">
                    '.$this->l('from this page (for product reviews)').'
                    </a>
                </p>
                </div><br>
                <div style="font-size:16px;">
                <h3 class="lgoverflow">
                <label for="lgcomments_cron_url">
                    <i class="icon-chevron-circle-right"></i> '.$this->l('Cron URL:').'
                </label>
                <a href="'.$modpath.'/lgcommentscron.php?secureKey='.$secureKey.'" target="_blank">
                <span class="lglowercase">
                '.$modpath.'/lgcommentscron.php?secure</span>K<span class="lglowercase">ey='.$secureKey.'
                </span>
                </a></span></h3></div>
                <br>
                <div class="alert alert-info"><br>
                    <h3><label>&nbsp;&nbsp;'.$this->l('No emails sent?').'</label></h3>
                    1 - '.$this->l('Go to the tab').'&nbsp;<span class="lgbold lgupper">'.
                    $this->l('Corresponding orders').
                    '</span>&nbsp'.$this->l('and make sure that you have orders').
                    '&nbsp;'.$this->l('that correspond to the selected criteria.').'<br>
                    2 - <a href="index.php?controller=AdminEmails&token='.$tokenE.'#mail_fieldset_test"
                    target="_blank">'.
                    $this->l('Click here').'&nbsp;'.
                    $this->l('and test your email configuration.').'</a><br>
                    3 - '.$this->l('Connect to your FTP, enter the folder').'&nbsp;
                    <span class="lgbold">/modules/lgcomments/mails/</span>&nbsp;'.
                    $this->l('and make sure to have a template folder for all your language codes.').'<br>
                </div>
                <br><br>
                <h3>
                <span class="lgfloat">
                    <label><i class="icon-bell"></i> '.$this->l('Enable email alerts').'&nbsp;&nbsp;</label>
                </span>
                <span class="switch prestashop-switch fixed-width-lg lgfloat" style="margin-right:10px;">
                <input type="radio" name="lgcomments_email_alerts" id="lgcomments_email_alerts_on" value="1"
                '.(Configuration::get('PS_LGCOMMENTS_EMAIL_ALERTS') == 1 ? 'checked="checked"' : '').' />
                <label for="lgcomments_email_alerts_on" class="lgbutton">'.$this->l('Yes').'</label>
                <input type="radio" name="lgcomments_email_alerts" id="lgcomments_email_alerts_off" value="0"
                '.(Configuration::get('PS_LGCOMMENTS_EMAIL_ALERTS') == 0 ? 'checked="checked"' : '').' />
                <label for="lgcomments_email_alerts_off" class="lgbutton">'.$this->l('No').'</label>
                <a class="slide-button btn"></a>
                </span>
                <span class="lgfloat lgmenu">
                <input id="lgcomments_email_cron2" type="text" name="lgcomments_email_cron"
                value="'.Configuration::get('PS_LGCOMMENTS_EMAIL_CRON').'" /></h3>
                </span>
                </h3>
                <div class="lgclear"></div>
                <div class="alert alert-info">
                '.$this->l('You will get an email every time the cron is executed').
                '&nbsp;'.$this->l('and every time a comment is written by a customer').'.
                </div><br><br>
            <h3>
                <label for="lgcomments_subject">
                    <i class="icon-flag"></i> '.$this->l('Alert subjects:').'
                </label>
            </h3>
            <div class="lgoverflow">
            <table class="table">
                <tr>
                    <th>'.$this->l('Confirmation of emails sent').'</th>
                    <th>'.$this->l('New reviews received').'</th>
                </tr>
                <tr>
                    <td>
                        <input type="text" name="subjectcron"
                        value="'.Configuration::get('PS_LGCOMMENTS_SUBJECT_CRON').'">
                    </td>
                    <td>
                        <input type="text" name="subjectreviews"
                        value="'.Configuration::get('PS_LGCOMMENTS_SUBJECT_NEWREVIEWS').'">
                    </td>
                </tr>
            </table>
            <div class="alert alert-info">
            '.$this->l('Choose the subject of the emails that will be sent to the email address above').'
            &nbsp;'.$this->l('every time the cron is executed or a new review is sent.').'
            </div>
            </div>
            <div class="lgclear"></div><br>
                <button class="button btn btn-default" type="submit" name="submitLGCommentsSend">
                    <i class="process-icon-save"></i>'.$this->l('Save').'
                </button>
			</fieldset>
        </form>
		</div>';

        // CONFIGURE EMAILS
        $this->_html .= '
		<div id="configure-email">
        <form action="'.htmlentities($_SERVER['REQUEST_URI']).'" method="post">
        <fieldset>
            <legend>
                <a href="../modules/'.$this->name.'/readme/readme_'.$this->l('en').'.pdf#page=12" target="_blank">
                    <span class="lglarge"><i class="icon-wrench"></i>
                        &nbsp;'.$this->l('Configure emails').'
                        &nbsp;<img src="../modules/'.$this->name.'/views/img/info.png">
                    </span>
                </a>
            </legend><br>
            <h3>
                <span class="lgfloat">
                <label for="lgcomments_groups">
                    <i class="icon-users"></i>
                    &nbsp;'.$this->l('Send emails to the selected groups of customers only').'
                </label>
                </span>
            </h3>
            <div>
            <table class="table">';
        foreach ($customerGroups as $cGroup) {
            $this->_html .= '
                <tr>
                    <td width="50px;"><input type="checkbox" name="group'.$cGroup['id_group'].'" value="1"';
            if ($this->getCustomerGroupsByGroup($cGroup['id_group'])) {
                $this->_html .= ' checked=checked';
            }
            $this->_html .= '></td>
                    <td><span>'.$cGroup['name'].'</span></td>
                </tr>';
        }
        $this->_html .= '
            </table>
            </div>
            <div class="alert alert-info">
                '.$this->l('This feature prevents groups of customers to leave a review about their orders.').'
            </div>
            <div class="lgclear"></div><br><br>
            <h3>
                <span class="lgfloat">
                <label for="lgcomments_shops">
                    <i class="icon-sitemap"></i>
                    &nbsp;'.$this->l('Send emails to the customers of the selected shops only').'
                </label>
                </span>
            </h3>
            <div>
            <table class="table">';
        foreach ($shopList as $shop) {
            $this->_html .= '
                <tr>
                    <td width="50px;"><input type="checkbox" name="shop'.$shop['id_shop'].'" value="1"';
            if ($this->getSelectedShops($shop['id_shop'])) {
                $this->_html .= ' checked=checked';
            }
            $this->_html .= '></td>
                    <td><span>'.$shop['name'].'</span></td>
                </tr>';
        }
        $this->_html .= '
            </table>
            </div>
            <div class="alert alert-info">
                '.$this->l('This feature prevents you from sending emails to the customers').
                '&nbsp;'.$this->l('of a shop on which the module is disabled (multistore mode).').'
            </div>
            <div class="lgclear"></div><br><br><br>
            <h3>
                <span class="lgfloat">
                    <label><i class="icon-check-square-o"></i>
                    &nbsp;'.$this->l('Send emails only to the customers who have tick the box:').'
                    &nbsp;&nbsp;</label>
                </span>
                <select id="PS_LGCOMMENTS_BOXES" class="lgfloat fixed-width-xl"
                name="PS_LGCOMMENTS_BOXES">
                    <option '.((Configuration::get('PS_LGCOMMENTS_BOXES') == 1)
                    ? 'selected="selected"' : '').' value="1">
                        '.$this->l('All customers').'
                    </option>
                    <option '.((Configuration::get('PS_LGCOMMENTS_BOXES') == 2)
                    ? 'selected="selected"' : '').' value="2">
                        '.$this->l('Newsletters').'
                    </option>
                    <option '.((Configuration::get('PS_LGCOMMENTS_BOXES') == 3)
                    ? 'selected="selected"' : '').' value="3">
                        '.$this->l('Opt-in').'
                    </option>
                    <option '.((Configuration::get('PS_LGCOMMENTS_BOXES') == 4)
                    ? 'selected="selected"' : '').' value="4">
                        '.$this->l('Newsletters + Opt-in').'
                    </option>
                </select>
            </h3>
            <div class="alert alert-info">
            '.$this->l('This feature prevents the module to send emails to the customers who have not').
            '&nbsp;'.$this->l('accepted to receive newsletters or partner offers.').'
            </div>
            <div class="lgclear"></div><br><br><br>
            <h3><span class="lgfloat"><label for="lgcomments_days"><i class="icon-calendar"></i>
            &nbsp;'.$this->l('Send emails only for the orders that are more than').'&nbsp;&nbsp;</label></span>
            <span class="lgfloat lgdays">
            <input id="lgcomments_dias2" type="text" name="lgcomments_dias2"
            value="'.Configuration::get('PS_LGCOMMENTS_DIAS2').'"  required />
            </span>
            <span class="lgfloat"><label>&nbsp;&nbsp;'.$this->l('days old and less than').'&nbsp;&nbsp;</label></span>
            <span class="lgfloat lgdays">
            <input id="lgcomments_dias" type="text" name="lgcomments_dias"
            value="'.Configuration::get('PS_LGCOMMENTS_DIAS').'"  required />
            </span>
            <span class="lgfloat"><label>&nbsp;&nbsp;'.$this->l('days old').'</label></span>
            </h3>
            <div class="alert alert-info lgclear">
                '.$this->l('This feature prevents customers to leave a review for very recent and old orders.').'
            </div>
            <div class="lgclear"></div><br><br><br>
            <h3><span class="lgfloat">
            <label><i class="icon-calendar"></i>
                &nbsp;'.$this->l('Send emails a second time').'&nbsp;&nbsp;
            </label></span>
            <span class="switch prestashop-switch fixed-width-lg lgfloat">
            <input type="radio" name="lgcomments_email_twice" id="lgcomments_email_twice_on" value="1"
            '.(Configuration::get('PS_LGCOMMENTS_EMAIL_TWICE') == 1 ? 'checked="checked"' : '').' />
            <label for="lgcomments_email_twice_on" class="lgbutton">'.$this->l('Yes').'</label>
            <input type="radio" name="lgcomments_email_twice" id="lgcomments_email_twice_off" value="0"
            '.(Configuration::get('PS_LGCOMMENTS_EMAIL_TWICE') == 0 ? 'checked="checked"' : '').' />
            <label for="lgcomments_email_twice_off" class="lgbutton">'.$this->l('No').'</label>
            <a class="slide-button btn"></a>
            </span>
            <span class="lgfloat">
                <label>&nbsp;&nbsp;'.$this->l('at least').'&nbsp;&nbsp</label>
            </span>
            <span class="lgfloat lgdays">
            <input id="lgcomments_days_after" type="text" name="lgcomments_days_after"
            value="'.Configuration::get('PS_LGCOMMENTS_DAYS_AFTER').'" required />
            </span>
            <span class="lgfloat"><label>
            &nbsp;&nbsp;'.$this->l('days after the first emails were sent').'
            </label></span>
            </h3>
            <div class="alert alert-info lgclear">
            '.$this->l('This feature allows you to send a second time').
            '&nbsp;'.$this->l('the opinion request emails that have already been sent.').'
            </div>
            <div class="lgclear"></div><br><br><br>
            <h3><span class="lgfloat"><label for="lgcomments_estados">
            <i class="icon-history"></i>
            &nbsp;'.$this->l('Send emails only for the orders with the current selected status:').'
            </h3></label></span>
            <div class="alert alert-info">
                '.$this->l('This feature prevents customers to leave a review').
                '&nbsp;'.$this->l('for orders that they haven\'t received yet.').'
            </div>
            <div>
                <table class="table">';
        foreach ($this->getOrdersStatus() as $estado) {
            $this->_html .= '
                    <tr>
                        <td width="50px;">
                            <input type="checkbox" name="estado'.$estado['id_order_state'].'" value="1"';
            if ($this->getLGCommentsOrderStatus($estado['id_order_state'])) {
                $this->_html .= ' checked=checked';
            }
            $this->_html .= '></td>
                        <td>
                            <span style="background-color:'.$estado['color'].'" class="lgstatus">
                                '.$estado['name'].'
                            </span>
                        </td>
                    </tr>';
        }
        $this->_html .= '
                </table>
            </div>
            <div class="lgclear"></div><br><br><br>
            <h3>
                <label for="lgcomments_subject">
                    <i class="icon-flag"></i> '.$this->l('Choose the email subjects for your customers:').'
                </label>
            </h3>
            <div class="alert alert-info">
            '.$this->l('Choose the subject of the emails sent to your customers to ask for their opinions,').'
            &nbsp;'.$this->l('to say thank you and to send an answer to their review.').'
            </div>
            <div class="lgoverflow">
            <table class="table">
                <tr>
                    <th>'.$this->l('Language').'</th>
                    <th colspan="2">'.$this->l('Opinion request email').'</th>
                    <th colspan="2">'.$this->l('Thank you email').'</th>
                    <th colspan="2">'.$this->l('Opinion answer email').'</th>
                </tr>';
        $langs = Language::getLanguages();
        foreach ($langs as $lang) {
            $this->_html .= '
                <tr>
                    <td class="lgbutton">
                    '.$lang['name'].'</td>
                    <td class="lgmenu">
                        <input type="text" name="subject'.$lang['iso_code'].'"
                        value="'.Configuration::get('PS_LGCOMMENTS_SUBJECT'.$lang['iso_code']).'">
                    </td>
                    <td class="lgbutton">
                        <a href="../modules/'.$this->name.'/mails/'.$lang['iso_code'].'/opinion-request.html"
                        target="_blank">'.$this->l('Template').' ('.$lang['iso_code'].')</a>
                    </td>
                    <td class="lgmenu">
                        <input type="text" name="subject2'.$lang['iso_code'].'"
                        value="'.Configuration::get('PS_LGCOMMENTS_SUBJECT2'.$lang['iso_code']).'">
                    </td>
                    <td class="lgbutton">
                        <a href="../modules/'.$this->name.'/mails/'.$lang['iso_code'].'/thank-you.html"
                        target="_blank">'.$this->l('Template').' ('.$lang['iso_code'].')</a>
                    </td>
                    <td class="lgmenu">
                        <input type="text" name="subject3'.$lang['iso_code'].'"
                        value="'.Configuration::get('PS_LGCOMMENTS_SUBJECT3'.$lang['iso_code']).'">
                    </td>
                    <td class="lgbutton">
                        <a href="../modules/'.$this->name.'/mails/'.$lang['iso_code'].'/send-answer.html"
                        target="_blank">'.$this->l('Template').' ('.$lang['iso_code'].')</a>
                    </td>
                </tr>';
        }
        $this->_html .= '
                <tr>
                    <th>'.$this->l('FTP Location').'</th>
                    <th colspan="2">/modules/lgcomments/mails/../opinion-request.html</th>
                    <th colspan="2">/modules/lgcomments/mails/../thank-you.html</th>
                    <th colspan="2">/modules/lgcomments/mails/../send-answer.html</th>
                </tr>
            </table>
            <br>
            </div>
            <div class="lgclear"></div>
            <div class="alert alert-info">
            <u><b>'.$this->l('About email templates:').'</b></u><br>
            '.$this->l('The variables {shop_name}, {shop_logo} and {storename} are specific to your shop,').'
            &nbsp;'.$this->l('the variables {firstname} and {link} are specific to each customer').'
            &nbsp;'.$this->l('and the variables {object}, {stars}, {title}, {comment} and {answer}').'
            &nbsp;'.$this->l('are specific to each review.').'
            <br>
            '.$this->l('These variables will be automatically substituted by the corresponding data').
            '&nbsp;'.$this->l('before sending the opinion email to your customers').'
            <br>
            '.$this->l('To modify the content of these emails,').
            '&nbsp;'.$this->l('connect to your FTP and follow the paths indicated above').'
            </div>
            <div class="lgclear"></div><br><br>
            <button class="button btn btn-default" type="submit" name="submitLGCommentsConfigure">
                <i class="process-icon-save"></i>'.$this->l('Save').'
            </button>
        </fieldset>
        </form>
        </div>';

        // CORRESPONDING ORDERS
        $this->_html .= '
		<div id="order-list">
        <fieldset>
            <legend>
                <a href="../modules/'.$this->name.'/readme/readme_'.$this->l('en').'.pdf#page=12" target="_blank">
                    <span class="lglarge"><i class="icon-shopping-cart"></i>
                        &nbsp;'.$this->l('Corresponding orders').'
                        &nbsp;<img src="../modules/'.$this->name.'/views/img/info.png">
                    </span>
                </a>
            </legend>
            <h2>'.$this->l('ORDER CRITERIA:').'</h2><br>
            <h3 class="lgoverflow">
                <i class="icon-caret-right"></i>&nbsp;
                '.$this->l('Customer groups').':';
        if ($allGroups) {
            foreach ($allGroups as $group) {
                $this->_html .= '
                - <span class="lgsilverbutton">
                    '.$group['name'].'
                </span>';
            }
        } else {
            $this->_html .= '
                <span class="lgred">
                    '.$this->l('No customer group selected.').
                    '&nbsp;'.$this->l('You must select at least one customer group in the module').'
                </span>';
        }
        $this->_html .= '
            </h3>
            <h3 class="lgoverflow">
                <i class="icon-caret-right"></i>&nbsp;
                '.$this->l('Shops').':';
        if ($allShops) {
            foreach ($allShops as $shop) {
                $this->_html .= '
                - <span class="lgsilverbutton">
                    '.$shop['name'].'
                </span>';
            }
        } else {
            $this->_html .= '
                <span class="lgred">
                    '.$this->l('No shop selected.').
                    '&nbsp;'.$this->l('You must select at least one shop in the module').'
                </span>';
        }
        $this->_html .= '
            </h3>
            <h3 class="lgoverflow">
                <i class="icon-caret-right"></i>&nbsp;
                '.$this->l('Boxes checked').':';
        if (Configuration::get('PS_LGCOMMENTS_BOXES') == 2) {
            $this->_html .= '
                <span class="lgsilverbutton">
                    '.$this->l('Newsletters').'
                </span>';
        } elseif (Configuration::get('PS_LGCOMMENTS_BOXES') == 3) {
            $this->_html .= '
                <span class="lgsilverbutton">
                    '.$this->l('Opt-in').'
                </span>';
        } elseif (Configuration::get('PS_LGCOMMENTS_BOXES') == 4) {
            $this->_html .= '
                <span class="lgsilverbutton">
                    '.$this->l('Newsletters + Opt-in').'
                </span>';
        } else {
            $this->_html .= '
                <span class="lgsilverbutton">
                    '.$this->l('All customers').'
                </span>';
        }
        $this->_html .= '
            </h3>
            <h3>
                <i class="icon-caret-right"></i>&nbsp;
                '.$this->l('Date').' : '.$this->l('from').'&nbsp;
                <span class="lgsilverbutton">
                    '.$date1->format($this->getDateFormatFull()).'
                </span>
                    &nbsp;'.$this->l('to').'&nbsp;
                <span class="lgsilverbutton">
                    '.$date2->format($this->getDateFormatFull()).'
                </span>
            </h3>
            <h3>
                <i class="icon-caret-right"></i>&nbsp;
                '.$this->l('Send emails a second time').':';
        if (Configuration::get('PS_LGCOMMENTS_EMAIL_TWICE')) {
            $this->_html .= '
                <span class="list-action-enable action-enabled" style="display:inline;margin:0;">
                    <i class="icon-check"></i>
                </span>
                    &nbsp;'.$this->l('only for the orders whose first email was sent before').'&nbsp;
                <span class="lgsilverbutton">
                    '.$date3->format($this->getDateFormatFull()).'
                </span>';
        } else {
            $this->_html .= '
                <span class="list-action-enable action-disabled" style="display:inline;margin:0;">
                    <i class="icon-remove"></i>
                </span>';
        }
        $this->_html .= '
            </h3>
            <h3 class="lgoverflow">
                <i class="icon-caret-right"></i>&nbsp;
                '.$this->l('Status').':';
        if ($allStatus) {
            foreach ($allStatus as $status) {
                $this->_html .= '
                - <span style="background-color:'.$status['color'].';" class="lgstatus">
                    '.$status['name'].'
                </span>';
            }
        } else {
            $this->_html .= '
                <span class="lgred">
                    '.$this->l('No status selected. You must select at least one status in the module').'
                </span>';
        }
        $this->_html .= '
            </h3>
            <br>';

        if ($orderList) {
            $this->_html .= '
            <div class="lgoverflow">
            <table border="1" class="table">
                <tr>
                    <th class="lgupper" colspan="5" style="text-align:center;">'.$this->l('Order').'</th>
                    <th class="lgupper" colspan="4" style="text-align:center;">'.$this->l('Customer').'</th>
                    <th class="lgupper" colspan="3" style="text-align:center;">'.$this->l('Email').'</th>
                </tr>
                <tr>
                    <th class="lgupper">'.$this->l('ID').'</th>
                    <th class="lgupper">'.$this->l('Reference').'</th>
                    <th class="lgupper">'.$this->l('Date').'</th>
                    <th class="lgupper">'.$this->l('Status').'</th>
                    <th class="lgupper">'.$this->l('Shop').'</th>
                    <th class="lgupper">'.$this->l('Customer').'</th>
                    <th class="lgupper">'.$this->l('Group(s)').'</th>
                    <th class="lgupper">'.$this->l('Newsletter').'</th>
                    <th class="lgupper">'.$this->l('Opt-in').'</th>
                    <th class="lgupper">'.$this->l('Email sent once?').'</th>
                    <th class="lgupper">'.$this->l('Email sent twice?').'</th>
                    <th class="lgupper">'.$this->l('Review already written?').'</th>
                </tr>';
            foreach ($orderList as $order) {
                $this->_html .= '
                <tr>
                    <td>'.$order['id_order'].'</td>
                    <td>'.$order['reference'].'</td>
                    <td>'.date($this->getDateFormatFull(), strtotime($order['date_add'])).'</td>
                    <td>
                        <span style="background-color:'.$order['color'].'" class="lgstatus">
                            '.$order['statusname'].'
                        </span>
                    </td>
                    <td>';
                $shops = $this->getShopsByCustomer($order['id_customer']);
                foreach ($shops as $shop) {
                    $this->_html .= '
                        <span class="lgsilverbutton">'.$shop['name'].'</span> ';
                }
                $this->_html .= '
                    </td>
                    <td>'.$order['customer'].'</td>
                    <td>';
                $groups = $this->getCustomerGroupsByCustomer($order['id_customer']);
                foreach ($groups as $group) {
                    $this->_html .= '
                        - <span class="lgsilverbutton">'.$group['name'].'</span> ';
                }
                $this->_html .= '
                    </td>
                    <td class="lgcenter">';
                if ($order['newsletter']) {
                    $this->_html .= '
                        <span class="list-action-enable action-enabled">
                            <i class="icon-check"></i>
                        </span>';
                } else {
                    $this->_html .= '
                        <span class="list-action-enable action-disabled">
                            <i class="icon-remove"></i>
                        </span>';
                }
                $this->_html .= '
                    </td>
                    <td class="lgcenter">';
                if ($order['optin']) {
                    $this->_html .= '
                        <span class="list-action-enable action-enabled">
                            <i class="icon-check"></i>
                        </span>';
                } else {
                    $this->_html .= '
                        <span class="list-action-enable action-disabled">
                            <i class="icon-remove"></i>
                        </span>';
                }
                $this->_html .= '
                    </td>
                    <td class="lgcenter">';
                if ($order['date_email']) {
                    $this->_html .= '
                        <span class="list-action-enable action-enabled">
                            <i class="icon-check"></i>
                        </span> '.date("d/m/Y H:i", strtotime($order['date_email']));
                } else {
                    $this->_html .= '
                        <span class="list-action-enable action-disabled">
                            <i class="icon-remove"></i>
                        </span>';
                }
                $this->_html .= '
                    </td>
                    <td class="lgcenter">';
                if ($order['sent'] == 2) {
                    $this->_html .= '
                        <span class="list-action-enable action-enabled">
                            <i class="icon-check"></i>
                        </span> '.date("d/m/Y H:i", strtotime($order['date_email2']));
                } elseif ($order['sent'] == 0 or $order['voted']) {
                    $this->_html .= '
                        <span class="list-action-enable action-disabled">
                            <i class="icon-minus"></i>
                        </span>';
                } else {
                    $this->_html .= '
                        <span class="list-action-enable action-disabled">
                            <i class="icon-remove"></i>
                        </span>';
                }
                $this->_html .= '
                    </td>
                    <td class="lgcenter">';
                if ($order['voted']) {
                    $this->_html .= '
                        <span class="list-action-enable action-enabled">
                            <i class="icon-check"></i>
                        </span>';
                } elseif ($order['sent'] == 0) {
                    $this->_html .= '
                        <span class="list-action-enable action-disabled">
                            <i class="icon-minus"></i>
                        </span>';
                } else {
                    $this->_html .= '
                        <span class="list-action-enable action-disabled">
                            <i class="icon-remove"></i>
                        </span>';
                }
                $this->_html .= '
                    </td>
                </tr>';
            }
            $this->_html .= '
            </table>
            </div>';
        } else {
            $this->_html .= '
            <span class="lgred"><h2>
            '.$this->l('You don\'t have any order that corresponds to the criteria above.').
            '&nbsp;'.$this->l('Please modify your settings and expand your range of selection.').
            '</h2></span>';
        }
        $this->_html .= '
        </fieldset>
        </div>';

        // IMPORT STORE COMMENTS
        $this->_html .= '
		<div id="upload-store">
        <fieldset>
            <legend>
                <a href="../modules/'.$this->name.'/readme/readme_'.$this->l('en').'.pdf#page=14" target="_blank">
                    <span class="lglarge"><i class="icon-comments-o"></i>
                        &nbsp;'.$this->l('Import store reviews').'
                        &nbsp;<img src="../modules/'.$this->name.'/views/img/info.png">
                    </span>
                </a>
            </legend>
            <div id="csv_uploader">
            <form method="post" action="'.$_SERVER['REQUEST_URI'].'" enctype="multipart/form-data">
                <br>
                <h3>
                    <label>
                        <i class="icon-exclamation-triangle"></i>
                        &nbsp;'.$this->l('You must respect the following rules to upload the comments correctly:').'
                    </label>
                </h3>
                <div class="lgoverflow">
                <table style="width:900px;" class="table lgcenter" border="1">
                    <tr>
                        <th style="text-align:center;" class="lgupper">'.$this->l('Column').' A</th>
                        <th style="text-align:center;" class="lgupper">'.$this->l('Column').' B</th>
                        <th style="text-align:center;" class="lgupper">'.$this->l('Column').' C</th>
                        <th style="text-align:center;" class="lgupper">'.$this->l('Column').' D</th>
                        <th style="text-align:center;" class="lgupper">'.$this->l('Column').' E</th>
                        <th style="text-align:center;" class="lgupper">'.$this->l('Column').' F</th>
                        <th style="text-align:center;" class="lgupper">'.$this->l('Column').' G</th>
                        <th style="text-align:center;" class="lgupper">'.$this->l('Column').' H</th>
                        <th style="text-align:center;" class="lgupper">'.$this->l('Column').' I</th>
                        <th style="text-align:center;" class="lgupper">'.$this->l('Column').' J</th>
                    </tr>
                    <tr>
                        <td class="lgpadding">
                            <span class="toolTip">
                            <a href="#csv_uploader">'.$this->l('Date').'</a>
                            <span class="tooltipDesc">
                            '.$this->l('In the column A of your CSV file, add the date of each comment.').'
                            &nbsp;'.$this->l('Important: use the format dd/mm/yyyy (ex:15/04/2016)').'
                            </span>
                            </span>
                        </td>
                        <td class="lgpadding">
                            <span class="toolTip">
                            <a href="#csv_uploader">'.$this->l('Customer ID').'</a>
                            <span class="tooltipDesc">
                            '.$this->l('In the column B of your CSV file, add the ID of the customer').'
                            &nbsp;'.$this->l('who wrote the comment (you can find it').'
                            <a href="index.php?tab=AdminCustomers&token='.$tokenC.'" target="_blank" class="lgbold">
                                '.$this->l('on this page').'
                            </a>)
                            </span>
                            </span>
                        </td>
                        <td class="lgpadding">
                            <span class="toolTip">
                            <a href="#csv_uploader">'.$this->l('Order ID').'</a>
                            <span class="tooltipDesc">
                            '.$this->l('In the column C of your CSV file, add the ID of the order').'
                            &nbsp;'.$this->l('for which the comment was written (you can find it').'&nbsp;
                            <a href="index.php?tab=AdminOrders&token='.$tokenO.'" target="_blank" class="lgbold">
                            '.$this->l('on this page').'
                            </a>)
                            </span>
                            </span>
                        </td>
                        <td class="lgpadding">
                            <span class="toolTip">
                            <a href="#csv_uploader">'.$this->l('Rating').'</a>
                            <span class="tooltipDesc">
                            '.$this->l('In the column D of your CSV file, add the rating on the shop').'
                            &nbsp;'.$this->l('(on a scale of /10)').'
                            </span>
                            </span>
                        </td>
                        <td class="lgpadding">
                            <span class="toolTip">
                            <a href="#csv_uploader">'.$this->l('Comment').'</a>
                            <span class="tooltipDesc">
                            '.$this->l('In the column E of your CSV file, add the comment').'
                            &nbsp;'.$this->l('that has been written about the shop').'
                            </span>
                            </span>
                        </td>
                        <td class="lgpadding">
                            <span class="toolTip">
                            <a href="#csv_uploader">'.$this->l('Language ID').'</a>
                            <span class="tooltipDesc">
                            '.$this->l('In the column F of your CSV file, add the language ID').'
                            &nbsp;'.$this->l('in which the comment was written (you can find it').'&nbsp;
                            <a href="index.php?tab=AdminLanguages&token='.$tokenL.'" target="_blank" class="lgbold">
                            '.$this->l('on this page').'
                            </a>)
                            </span>
                            </span>
                        </td>
                        <td class="lgpadding">
                            <span class="toolTip">
                            <a href="#csv_uploader">'.$this->l('Status').'</a>
                            <span class="tooltipDesc">
                            '.$this->l('In the column G of your CSV file, add the status of the comment').'
                            &nbsp;'.$this->l('("1" for enabled and "0" for disabled)').'
                            </span>
                            </span>
                        </td>
                        <td>
                            <span class="toolTip">
                            <a href="#csv_uploader">'.$this->l('Position').'</a>
                            <span class="tooltipDesc">
                            '.$this->l('In the column H of your CSV file, add the position of the comment').'
                            &nbsp;'.$this->l('(compared to the other comments about the shop)').'
                            </span></span>
                        </td>
                        <td class="lgpadding">
                            <span class="toolTip"><a href="#csv_uploader">'.$this->l('Title').'</a>
                            <span class="tooltipDesc">
                            '.$this->l('In the column I of your CSV file, add the title of the comment').'
                            &nbsp;'.$this->l('(it will appear in bold before the comment)').'
                            </span></span>
                        </td>
                        <td class="lgpadding">
                            <span class="toolTip"><a href="#csv_uploader">'.$this->l('Answer').'</a>
                            <span class="tooltipDesc">
                            '.$this->l('In the column J of your CSV file, add the answer to the comment').
                            '&nbsp;'.$this->l('(optional, use "0" if there is no answer)').'
                            </span>
                            </span>
                        </td>
                    </tr>
                </table>
                <div class="lgclear"></div>
                <br>
                <div class="alert alert-info">
                    - '.$this->l('Move your mouse over the table to get more information.').'<br>
                    - <a href="../modules/'.$this->name.'/csv/example_store.csv">
                    '.$this->l('Click here to download an example of CSV file').
                    '&nbsp;'.$this->l('(you can write your comments directly in it)').'.
                    </a>
                </div>
                </div>
                <br><br>
                <h3>
                    <span class="lgfloat">
                    <label>
                        <i class="icon-file-excel-o"></i>
                        &nbsp;'.$this->l('Select your file').'&nbsp;&nbsp;
                    </label>
                    </span>
                    <input type="file" name="csv2" id="csv2" class="btn btn-default lgfloat"><br>
                </h3>
                <div class="alert alert-info">
                '.$this->l('The file must be in.csv format and respect the structure indicated above (10 columns).').'
                </div>
                <div class="lgclear"></div><br><br>
                <h3>
                    <span class="lgfloat">
                    <label>
                        <i class="icon-scissors"></i>
                        &nbsp;'.$this->l('Indicate the separator of your CSV file (important)').'&nbsp;&nbsp;
                    </label>
                    </span>
                    <select id="separator2" class="lgfloat fixed-width-xl" name="separator2">
                        <option value="1">
                            '.$this->l('Semi-colon').'
                        </option>
                        <option value="2">
                            '.$this->l('Comma').'
                        </option>
                    </select>
                </h3>
                <div class="alert alert-info">
                    - '.$this->l('Open your csv file with a text editor ("Notepad" for example)').'
                    &nbsp;'.$this->l('and check if the elements are separated with a semi-colon or comma.').'<br>
                    - '.$this->l('If you use a comma separator, please remove all the comma from the titles,').'
                    &nbsp;'.$this->l('comments and answers in the CSV file to import the reviews correctly.').'
                </div>
                <div class="lgclear"></div><br><br>
                <h3>
                    <span class="lgfloat">
                    <label>
                        <i class="icon-key"></i>
                        &nbsp;'.$this->l('Character encoding').'&nbsp;&nbsp;
                    </label>
                    </span>
                    <select id="encoding2" class="lgfloat fixed-width-xl" name="encoding2">
                        <option value="1">
                            '.$this->l('Latin/roman alphabet').'
                        </option>
                        <option value="2">
                            '.$this->l('Other alphabets (East Europe, Cyrillic, Arabic, Greek, Chinese...)').'
                        </option>
                    </select>
                </h3>
                <div class="alert alert-info">
                    '.$this->l('Choose the alphabet used in your CSV file in order to import the file correctly').'
                    &nbsp;'.$this->l('and avoid character encoding problem.').'
                </div>
                <br>
                <div class="lgclear"></div><br>
                <label from="exportStoreCSV"></label>
                <button class="button btn btn-default lgfloatright" type="submit" name="exportStoreCSV">
                    <i class="process-icon-download"></i> '.$this->l('Export all store comments').'
                </button>
                <label from="storeCSV"></label>
                <button class="button btn btn-default" type="submit" name="storeCSV" >
                    <i class="process-icon-import"></i> '.$this->l('Import the store comments').'
                </button>
            </form>
            </div>
        </fieldset>
        </div>';

        // IMPORT PRODUCT COMMENTS
        $this->_html .= '
		<div id="upload-products">
        <fieldset>
            <legend>
                <a href="../modules/'.$this->name.'/readme/readme_'.$this->l('en').'.pdf#page=14" target="_blank">
                    <span class="lglarge"><i class="icon-comments"></i>
                        &nbsp;'.$this->l('Import product reviews').'
                        &nbsp;<img src="../modules/'.$this->name.'/views/img/info.png">
                    </span>
                </a>
            </legend>
            <div id="csv_uploader">
            <form method="post" action="'.$_SERVER['REQUEST_URI'].'" enctype="multipart/form-data">
                <br>
                <h3>
                    <label>
                        <i class="icon-exclamation-triangle"></i>
                        &nbsp;'.$this->l('You must respect the following rules to upload the comments correctly:').'
                    </label>
                </h3>
                <div class="lgoverflow">
                <table style="width:900px;" class="table lgcenter" border="1">
                    <tr>
                        <th style="text-align:center;" class="lgupper">'.$this->l('Column').' A</th>
                        <th style="text-align:center;" class="lgupper">'.$this->l('Column').' B</th>
                        <th style="text-align:center;" class="lgupper">'.$this->l('Column').' C</th>
                        <th style="text-align:center;" class="lgupper">'.$this->l('Column').' D</th>
                        <th style="text-align:center;" class="lgupper">'.$this->l('Column').' E</th>
                        <th style="text-align:center;" class="lgupper">'.$this->l('Column').' F</th>
                        <th style="text-align:center;" class="lgupper">'.$this->l('Column').' G</th>
                        <th style="text-align:center;" class="lgupper">'.$this->l('Column').' H</th>
                        <th style="text-align:center;" class="lgupper">'.$this->l('Column').' I</th>
                        <th style="text-align:center;" class="lgupper">'.$this->l('Column').' J</th>
                    </tr>
                    <tr>
                        <td class="lgpadding">
                            <span class="toolTip"><a href="#csv_uploader">'.$this->l('Date').'</a>
                            <span class="tooltipDesc">
                                '.$this->l('In the column A of your CSV file, add the date of each comment.').
                                '&nbsp;'.$this->l('Important: use the format dd/mm/yyyy (ex:15/04/2016)').'
                            </span></span>
                        </td>
                        <td class="lgpadding">
                            <span class="toolTip"><a href="#csv_uploader">'.$this->l('Customer ID').'</a>
                            <span class="tooltipDesc">
                            '.$this->l('In the column B of your CSV file, add the ID of the customer').
                            '&nbsp;'.$this->l('who wrote the comment (you can find it').'
                            <a href="index.php?tab=AdminCustomers&token='.$tokenC.'" target="_blank" class="lgbold">
                            '.$this->l('on this page').'
                            </a>)
                            </span>
                            </span>
                        </td>
                        <td class="lgpadding">
                            <span class="toolTip"><a href="#csv_uploader">'.$this->l('Product ID').'</a>
                            <span class="tooltipDesc">
                            '.$this->l('In the column C of your CSV file, add the ID of the product').
                            '&nbsp'.$this->l('for which the comment was written (you can find it').'
                            <a href="index.php?tab=AdminProducts&token='.$tokenPr.'" target="_blank" class="lgbold">
                            '.$this->l('on this page').'
                            </a>)
                            </span>
                            </span>
                        </td>
                        <td class="lgpadding">
                            <span class="toolTip"><a href="#csv_uploader">'.$this->l('Rating').'</a>
                            <span class="tooltipDesc">
                            '.$this->l('In the column D of your CSV file, add the rating on the product').
                            '&nbsp'.$this->l('(on a scale of /10)').'
                            </span>
                            </span>
                        </td>
                        <td class="lgpadding">
                            <span class="toolTip"><a href="#csv_uploader">'.$this->l('Comment').'</a>
                            <span class="tooltipDesc">
                            '.$this->l('In the column E of your CSV file, add the comment').
                            '&nbsp'.$this->l('that has been written about the product').'
                            </span></span>
                        </td>
                        <td class="lgpadding">
                            <span class="toolTip"><a href="#csv_uploader">'.$this->l('Language ID').'</a>
                            <span class="tooltipDesc">
                            '.$this->l('In the column F of your CSV file, add the language ID').
                            '&nbsp'.$this->l('in which the comment was written (you can find it').'&nbsp
                            <a href="index.php?tab=AdminLanguages&token='.$tokenL.'" target="_blank" class="lgbold">
                            '.$this->l('on this page').'
                            </a>)
                            </span>
                            </span>
                        </td>
                        <td class="lgpadding">
                            <span class="toolTip"><a href="#csv_uploader">'.$this->l('Status').'</a>
                            <span class="tooltipDesc">
                            '.$this->l('In the column G of your CSV file, add the status of the comment').
                            '&nbsp'.$this->l('("1" for enabled and "0" for disabled)').'
                            </span>
                            </span>
                        </td>
                        <td class="lgpadding">
                            <span class="toolTip"><a href="#csv_uploader">'.$this->l('Position').'</a>
                            <span class="tooltipDesc">
                            '.$this->l('In the column H of your CSV file, add the position of the comment').
                            '&nbsp'.$this->l('(compared to the other comments for the same product)').'
                            </span></span>
                        </td>
                        <td class="lgpadding">
                            <span class="toolTip"><a href="#csv_uploader">'.$this->l('Title').'</a>
                            <span class="tooltipDesc">
                            '.$this->l('In the column I of your CSV file, add the title of the comment').
                            '&nbsp'.$this->l('(it will appear in bold before the comment)').'
                            </span>
                            </span>
                        </td>
                        <td class="lgpadding">
                            <span class="toolTip"><a href="#csv_uploader">'.$this->l('Answer').'</a>
                            <span class="tooltipDesc">
                            '.$this->l('In the column J of your CSV file, add the answer to the comment').
                            '&nbsp'.$this->l('(optional, use "0" if there is no answer)').'
                            </span>
                            </span>
                        </td>
                    </tr>
                </table>
                <div class="lgclear"></div>
                <br>
                <div class="alert alert-info">
                    - '.$this->l('Move your mouse over the table to get more information.').'<br>
                    </a>
                    - <a href="../modules/'.$this->name.'/csv/example_products.csv">
                    '.$this->l('Click here to download an example of CSV file').
                    '&nbsp;'.$this->l('(you can write your comments directly in it)').'.
                    </a>
                </div>
                </div>
                <br><br>
                <h3>
                    <span class="lgfloat">
                    <label>
                        <i class="icon-file-excel-o"></i>
                        &nbsp;'.$this->l('Select your file').'&nbsp;&nbsp;
                    </label>
                    </span>
                    <input type="file" name="csv1" id="csv1" class="btn btn-default lgfloat"><br>
                </h3>
                <div class="alert alert-info">
                '.$this->l('The file must be in.csv format and respect the structure indicated above (10 columns).').'
                </div>
                <div class="lgclear"></div><br><br>
                <h3>
                    <span class="lgfloat">
                    <label>
                        <i class="icon-scissors"></i>
                        &nbsp;'.$this->l('Indicate the separator of your CSV file (important)').'&nbsp;&nbsp;
                    </label>
                    </span>
                    <select id="separator1" class="lgfloat fixed-width-xl" name="separator1">
                        <option value="1">
                            '.$this->l('Semi-colon').'
                        </option>
                        <option value="2">
                            '.$this->l('Comma').'
                        </option>
                    </select>
                </h3>
                <div class="alert alert-info">
                    - '.$this->l('Open your csv file with a text editor ("Notepad" for example)').'
                    &nbsp;'.$this->l('and check if the elements are separated with a semi-colon or comma.').'<br>
                    - '.$this->l('If you use a comma separator, please remove all the comma from the titles,').'
                    &nbsp;'.$this->l('comments and answers in the CSV file to import the reviews correctly.').'
                </div>
                <div class="lgclear"></div><br><br>
                <h3>
                    <span class="lgfloat">
                    <label>
                        <i class="icon-key"></i>
                        &nbsp;'.$this->l('Character encoding').'&nbsp;&nbsp;
                    </label>
                    </span>
                    <select id="encoding1" class="lgfloat fixed-width-xl" name="encoding1">
                        <option value="1">
                            '.$this->l('Latin/roman alphabet').'
                        </option>
                        <option value="2">
                            '.$this->l('Other alphabets (East Europe, Cyrillic, Arabic, Greek, Chinese...)').'
                        </option>
                    </select>
                </h3>
                <div class="alert alert-info">
                    '.$this->l('Choose the alphabet used in your CSV file in order to import the file correctly').'
                    &nbsp;'.$this->l('and avoid character encoding problem.').'
                </div>
                <br>
                <div class="lgclear"></div><br>
                <label from="exportProductCSV"></label>
                <button class="button btn btn-default lgfloatright" type="submit" name="exportProductCSV">
                    <i class="process-icon-download"></i> '.$this->l('Export all product comments').'
                </button>
                <label from="productCSV"></label>
                <button class="button btn btn-default" type="submit" name="productCSV" >
                    <i class="process-icon-import"></i> '.$this->l('Import the product comments').'
                </button>
            </form>
            </div>
        </fieldset>
        </div>';

        // // MANAGE REVIEWS
        $this->_html .= '
		<div id="manage-reviews">
            <form action="'.htmlentities($_SERVER['REQUEST_URI']).'" method="post">
            <fieldset>
                <legend>
                <a href="../modules/'.$this->name.'/readme/readme_'.$this->l('en').'.pdf#page=16" target="_blank">
                    <span class="lglarge"><i class="icon-pencil"></i>
                        &nbsp;'.$this->l('Manage reviews').'
                        &nbsp;<img src="../modules/'.$this->name.'/views/img/info.png">
                    </span>
                </a>
                </legend><br>
                <div>
                <p class="clear">
                    <a href="index.php?controller=AdminLGCommentsStore&token='.$tokenSC.'" target="_blank">
                        <button class="button btn btn-default" type="button">
                            <i class="icon-pencil"></i>
                            &nbsp;'.$this->l('Click here to manage your store reviews').'
                        </button>
                    </a>&nbsp;&nbsp;
                    <a href="index.php?controller=AdminLGCommentsProducts&token='.$tokenPC.'" target="_blank">
                        <button class="button btn btn-default" type="button">
                            <i class="icon-pencil"></i>
                            &nbsp;'.$this->l('Click here to manage your product reviews').'
                        </button>
                    </a>
                </p>
                <div class="lgclear"></div><br><br><br>
                <h3>
                    <span class="lgfloat"><label>&nbsp;&nbsp;
                        '.$this->l('Require validation before publishing comments').'&nbsp;&nbsp;
                     </label></span>
                    <span class="switch prestashop-switch fixed-width-lg lgfloat">
                        <input type="radio" name="lgcomments_validation" id="lgcomments_validation_on" value="1"
                        '.(Configuration::get('PS_LGCOMMENTS_VALIDATION') == 1 ? 'checked="checked"' : '').' />
                        <label for="lgcomments_validation_on" class="lgbutton">'.$this->l('Yes').'</label>
                        <input type="radio" name="lgcomments_validation" id="lgcomments_validation_off" value="0"
                        '.(Configuration::get('PS_LGCOMMENTS_VALIDATION') == 0 ? 'checked="checked"' : '').' />
                        <label for="lgcomments_validation_off" class="lgbutton">'.$this->l('No').'</label>
                        <a class="slide-button btn"></a>
                    </span>
                </h3>
                <div class="alert alert-info lgclear">
                '.$this->l('Enable this option if you want to check').
                '&nbsp;'.$this->l('and validate the comments before publishing them.').'
                &nbsp;
                '.$this->l('Disable this option if you want to publish the comments').
                '&nbsp;'.$this->l('automatically without any validation.').'
                </div>
                <div class="lgclear"></div><br><br>
                <h3>
                    <span class="lgfloat">
                        <label>&nbsp;&nbsp;'.$this->l('Allow customers to write:').'&nbsp;&nbsp;</label>
                    </span>
                    <select id="PS_LGCOMMENTS_OPINION_FORM" class="lgfloat fixed-width-xl"
                    name="PS_LGCOMMENTS_OPINION_FORM">
                        <option '.((Configuration::get('PS_LGCOMMENTS_OPINION_FORM') == 1)
                        ? 'selected="selected"' : '').' value="1">
                            '.$this->l('Store and product reviews').'
                        </option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_OPINION_FORM') == 2)
                        ? 'selected="selected"' : '').' value="2">
                            '.$this->l('Store reviews only').'
                        </option>
                        <option '.((Configuration::get('PS_LGCOMMENTS_OPINION_FORM') == 3)
                        ? 'selected="selected"' : '').' value="3">
                            '.$this->l('Product reviews only').'
                        </option>
                    </select>
                </h3>
                <br>
                <button class="button btn btn-default" type="submit" name="submitLGCommentsManage">
                    <i class="process-icon-save"></i>'.$this->l('Save').'
                </button>
                </div>
			</fieldset>
		</div>';

        // jquery select bg comment image
        $this->_html .= '
		<script type="text/javascript">
			function changeBg()
			{
                var bgdesign1 = document.getElementById("bgdesign1");
                var bgdesign2 = document.getElementById("bgdesign2");
                var bgdesign1 = bgdesign1.value;
                var bgdesign2 = bgdesign2.value;
				$("#bgdesignimage").attr("src","'.$modpath.'/views/img/bg/"+bgdesign1+"-"+bgdesign2+".png");
			}
			function changeStar()
			{
                var stardesign1 = document.getElementById("stardesign1");
                var stardesign2 = document.getElementById("stardesign2");
                var starsize = document.getElementById("starsize");
                var stardesign1 = stardesign1.value;
                var stardesign2 = stardesign2.value;
                var starsize = starsize.value;
				$("#stardesignimage").attr("src","'.$modpath.'/views/img/stars/"+stardesign1+"/"+stardesign2+"/9stars.png");
                $("#stardesignimage").attr("width",""+starsize+"");
                $("#starzero").attr("src","'.$modpath.'/views/img/stars/"+stardesign1+"/"+stardesign2+"/0stars.png");
                $("#starzero").attr("width",""+starsize+"");
			}
		</script>
		';
        if ($this->bootstrap == true) {
            $this->_html = $this->formatBootstrap($this->_html);
        }
        return $this->_html;
    }

    /* Product sheet reviews 1.6 */
    public function hookProductTab($params)
    {
        if (!Configuration::get('PS_LGCOMMENTS_DISPLAY_COMMENTS')) {
            return;
        }
        $alreadyreviewed = $this->checkIfProdAlreadyReviewed(Tools::getValue('id_product', 0));
        if (Configuration::get('PS_LGCOMMENTS_DISPLAY_LANGUAGE2') == 1) {
            if (Configuration::get('PS_LGCOMMENTS_DISPLAY_ORDER2') == 1) {
                $lgcomments = $this->getProdCommentsByLang(Tools::getValue('id_product', 0), 'ASC');
            } else {
                $lgcomments = $this->getProdCommentsByLang(Tools::getValue('id_product', 0), 'DESC');
            }
            $totalcomentarios = $this->getSumProdCommentsByLang(Tools::getValue('id_product', 0));
            $numerocomentarios = $this->getCountProdCommentsByLang(Tools::getValue('id_product', 0));
            $mediacomentarios = @round($totalcomentarios / $numerocomentarios);
            $nombreproducto = $this->getProductName(Tools::getValue('id_product'));
            $fivestars = $this->getCountProdByRateAndLang(Tools::getValue('id_product'), '10', '11');
            $fourstars = $this->getCountProdByRateAndLang(Tools::getValue('id_product'), '8', '10');
            $threestars = $this->getCountProdByRateAndLang(Tools::getValue('id_product'), '6', '8');
            $twostars = $this->getCountProdByRateAndLang(Tools::getValue('id_product'), '4', '6');
            $onestar = $this->getCountProdByRateAndLang(Tools::getValue('id_product'), '2', '4');
            $zerostar = $this->getCountProdByRateAndLang(Tools::getValue('id_product'), '0', '2');
        } else {
            if (Configuration::get('PS_LGCOMMENTS_DISPLAY_ORDER2') == 1) {
                $lgcomments = $this->getProdComments(Tools::getValue('id_product', 0), 'ASC');
            } else {
                $lgcomments = $this->getProdComments(Tools::getValue('id_product', 0), 'DESC');
            }
            $totalcomentarios = $this->getSumProdComments(Tools::getValue('id_product', 0));
            $numerocomentarios = $this->getCountProdComments(Tools::getValue('id_product', 0));
            $mediacomentarios = @round($totalcomentarios / $numerocomentarios);
            $nombreproducto = $this->getProductName(Tools::getValue('id_product'));
            $fivestars = $this->getCountProdByRate(Tools::getValue('id_product'), '10', '11');
            $fourstars = $this->getCountProdByRate(Tools::getValue('id_product'), '8', '10');
            $threestars = $this->getCountProdByRate(Tools::getValue('id_product'), '6', '8');
            $twostars = $this->getCountProdByRate(Tools::getValue('id_product'), '4', '6');
            $onestar = $this->getCountProdByRate(Tools::getValue('id_product'), '2', '4');
            $zerostar = $this->getCountProdByRate(Tools::getValue('id_product'), '0', '2');
        }
        $this->context->smarty->assign(array(
            'lgcomments' => $lgcomments,
            'nombreproducto' => $nombreproducto,
            'totalcomentarios' => $totalcomentarios,
            'mediacomentarios' => $mediacomentarios,
            'numlgcomments' => $numerocomentarios,
            'fivestars' => $fivestars,
            'fourstars' => $fourstars,
            'threestars' => $threestars,
            'twostars' => $twostars,
            'onestar' => $onestar,
            'zerostar' => $zerostar,
            'alreadyreviewed' => $alreadyreviewed,
            'starstyle' => Configuration::get('PS_LGCOMMENTS_STARDESIGN1'),
            'starcolor' => Configuration::get('PS_LGCOMMENTS_STARDESIGN2'),
            'starsize' => Configuration::get('PS_LGCOMMENTS_STARSIZE'),
            'ratingscale' => Configuration::get('PS_LGCOMMENTS_SCALE'),
            'tab_type' => Configuration::get('PS_LGCOMMENTS_TAB_CONTENT'),
            'displaysnippets' => Configuration::get('PS_LGCOMMENTS_DISPLAY_SNIPPETS2'),
            'defaultdisplay' => Configuration::get('PS_LGCOMMENTS_DISPLAY_DEFAULT'),
            'extradisplay' => Configuration::get('PS_LGCOMMENTS_DISPLAY_MORE'),
            'productfilter' => Configuration::get('PS_LGCOMMENTS_PRODUCT_FILTER'),
            'productfilternb' => Configuration::get('PS_LGCOMMENTS_PRODUCT_FILTER_NB'),
            'productform' => Configuration::get('PS_LGCOMMENTS_PRODUCT_FORM'),
            'dateformat' => $this->getDateFormat(),
            'is_https' =>
            (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"? 'true':'false'),
        ));
        return ($this->display(__FILE__, '/views/templates/front/product_reviews_16.tpl'));
    }

    /* Product sheet reviews 1.5 */
    public function hookProductTabContent($params)
    {
        if (!Configuration::get('PS_LGCOMMENTS_DISPLAY_COMMENTS')) {
            return;
        }
        $alreadyreviewed = $this->checkIfProdAlreadyReviewed(Tools::getValue('id_product', 0));
        if (Configuration::get('PS_LGCOMMENTS_DISPLAY_LANGUAGE2') == 1) {
            if (Configuration::get('PS_LGCOMMENTS_DISPLAY_ORDER2') == 1) {
                $lgcomments = $this->getProdCommentsByLang(Tools::getValue('id_product', 0), 'ASC');
            } else {
                $lgcomments = $this->getProdCommentsByLang(Tools::getValue('id_product', 0), 'DESC');
            }
            $totalcomentarios = $this->getSumProdCommentsByLang(Tools::getValue('id_product', 0));
            $numerocomentarios = $this->getCountProdCommentsByLang(Tools::getValue('id_product', 0));
            $mediacomentarios = @round($totalcomentarios / $numerocomentarios);
            $nombreproducto = $this->getProductName(Tools::getValue('id_product'));
            $fivestars = $this->getCountProdByRateAndLang(Tools::getValue('id_product'), '10', '11');
            $fourstars = $this->getCountProdByRateAndLang(Tools::getValue('id_product'), '8', '10');
            $threestars = $this->getCountProdByRateAndLang(Tools::getValue('id_product'), '6', '8');
            $twostars = $this->getCountProdByRateAndLang(Tools::getValue('id_product'), '4', '6');
            $onestar = $this->getCountProdByRateAndLang(Tools::getValue('id_product'), '2', '4');
            $zerostar = $this->getCountProdByRateAndLang(Tools::getValue('id_product'), '0', '2');
        } else {
            if (Configuration::get('PS_LGCOMMENTS_DISPLAY_ORDER2') == 1) {
                $lgcomments = $this->getProdComments(Tools::getValue('id_product', 0), 'ASC');
            } else {
                $lgcomments = $this->getProdComments(Tools::getValue('id_product', 0), 'DESC');
            }
            $totalcomentarios = $this->getSumProdComments(Tools::getValue('id_product', 0));
            $numerocomentarios = $this->getCountProdComments(Tools::getValue('id_product', 0));
            $mediacomentarios = @round($totalcomentarios / $numerocomentarios);
            $nombreproducto = $this->getProductName(Tools::getValue('id_product'));
            $fivestars = $this->getCountProdByRate(Tools::getValue('id_product'), '10', '11');
            $fourstars = $this->getCountProdByRate(Tools::getValue('id_product'), '8', '10');
            $threestars = $this->getCountProdByRate(Tools::getValue('id_product'), '6', '8');
            $twostars = $this->getCountProdByRate(Tools::getValue('id_product'), '4', '6');
            $onestar = $this->getCountProdByRate(Tools::getValue('id_product'), '2', '4');
            $zerostar = $this->getCountProdByRate(Tools::getValue('id_product'), '0', '2');
        }
        $this->context->smarty->assign(array(
            'lgcomments' => $lgcomments,
            'nombreproducto' => $nombreproducto,
            'totalcomentarios' => $totalcomentarios,
            'mediacomentarios' => $mediacomentarios,
            'numlgcomments' => $numerocomentarios,
            'fivestars' => $fivestars,
            'fourstars' => $fourstars,
            'threestars' => $threestars,
            'twostars' => $twostars,
            'onestar' => $onestar,
            'zerostar' => $zerostar,
            'alreadyreviewed' => $alreadyreviewed,
            'starstyle' => Configuration::get('PS_LGCOMMENTS_STARDESIGN1'),
            'starcolor' => Configuration::get('PS_LGCOMMENTS_STARDESIGN2'),
            'starsize' => Configuration::get('PS_LGCOMMENTS_STARSIZE'),
            'ratingscale' => Configuration::get('PS_LGCOMMENTS_SCALE'),
            'tab_type' => Configuration::get('PS_LGCOMMENTS_TAB_CONTENT'),
            'displaysnippets' => Configuration::get('PS_LGCOMMENTS_DISPLAY_SNIPPETS2'),
            'defaultdisplay' => Configuration::get('PS_LGCOMMENTS_DISPLAY_DEFAULT'),
            'extradisplay' => Configuration::get('PS_LGCOMMENTS_DISPLAY_MORE'),
            'productfilter' => Configuration::get('PS_LGCOMMENTS_PRODUCT_FILTER'),
            'productfilternb' => Configuration::get('PS_LGCOMMENTS_PRODUCT_FILTER_NB'),
            'productform' => Configuration::get('PS_LGCOMMENTS_PRODUCT_FORM'),
            'dateformat' => $this->getDateFormat(),
            'is_https' =>
            (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"? 'true':'false'),
        ));
        return ($this->display(__FILE__, '/views/templates/front/product_reviews_15.tpl'));
    }

    /* Product sheet top ratings */
    public function hookExtraRight($params)
    {
        if (!Configuration::get('PS_LGCOMMENTS_DISPLAY_COMMENTS')) {
            return;
        }
        if (Configuration::get('PS_LGCOMMENTS_DISPLAY_LANGUAGE2') == 1) {
            $countcomments =  $this->getCountProdCommentsByLang(Tools::getValue('id_product', 0));
            $sumcomments = $this->getSumProdCommentsByLang(Tools::getValue('id_product', 0));
            $averagecomments = @round($sumcomments / $countcomments);
        } else {
            $countcomments = $this->getCountProdComments(Tools::getValue('id_product', 0));
            $sumcomments = $this->getSumProdComments(Tools::getValue('id_product', 0));
            $averagecomments = @round($sumcomments / $countcomments);
        }
        $zerostar = Configuration::get('PS_LGCOMMENTS_DISPLAY_ZEROSTAR');
        $productrewrite = $this->getProductRewrite(Tools::getValue('id_product', 0));
        $productlink = $this->context->link->getProductLink(Tools::getValue('id_product', 0), $productrewrite);
        $prodtopmargin = Configuration::get('PS_LGCOMMENTS_PRODTOPMARGIN');
        $prodbotmargin = Configuration::get('PS_LGCOMMENTS_PRODBOTMARGIN');
        if ($countcomments) {
            $this->context->smarty->assign(array(
                'comments' => $countcomments,
                'sumcomments' => $sumcomments,
                'averagecomments' => $averagecomments,
                'displaysnippets' => Configuration::get('PS_LGCOMMENTS_DISPLAY_SNIPPETS2'),
                'comment_tab' => Configuration::get('PS_LGCOMMENTS_TAB_CONTENT'),
                'starstyle' => Configuration::get('PS_LGCOMMENTS_STARDESIGN1'),
                'starcolor' => Configuration::get('PS_LGCOMMENTS_STARDESIGN2'),
                'starsize' => Configuration::get('PS_LGCOMMENTS_STARSIZE'),
                'ratingscale' => Configuration::get('PS_LGCOMMENTS_SCALE'),
                'zerostar' => $zerostar,
                'productlink' => $productlink,
                'prodtopmargin' => $prodtopmargin,
                'prodbotmargin' => $prodbotmargin
            ));
            return $this->display(__FILE__, '/views/templates/front/product_extra_right.tpl');
        } if (!$countcomments and $zerostar) {
            $this->context->smarty->assign(array(
                'comments' => $countcomments,
                'starstyle' => Configuration::get('PS_LGCOMMENTS_STARDESIGN1'),
                'starcolor' => Configuration::get('PS_LGCOMMENTS_STARDESIGN2'),
                'starsize' => Configuration::get('PS_LGCOMMENTS_STARSIZE'),
                'ratingscale' => Configuration::get('PS_LGCOMMENTS_SCALE'),
                'zerostar' => $zerostar,
                'productlink' => $productlink,
                'prodtopmargin' => $prodtopmargin,
                'prodbotmargin' => $prodbotmargin
            ));
            return $this->display(__FILE__, '/views/templates/front/product_extra_right.tpl');
        }
    }

    /* Categories product ratings */
    public function hookDisplayProductListReviews($params)
    {
        if (!Configuration::get('PS_LGCOMMENTS_DISPLAY_COMMENTS')) {
            return;
        }
        $id_product = (int)$params['product']['id_product'];
        if (Configuration::get('PS_LGCOMMENTS_DISPLAY_LANGUAGE2') == 1) {
            $countcomments = $this->getCountProdCommentsByLang($id_product);
            $sumcomments = $this->getSumProdCommentsByLang($id_product);
            $averagecomments = @round($sumcomments / $countcomments);
        } else {
            $countcomments = $this->getCountProdComments($id_product);
            $sumcomments = $this->getSumProdComments($id_product);
            $averagecomments = @round($sumcomments / $countcomments);
        }
        $zerostar = Configuration::get('PS_LGCOMMENTS_DISPLAY_ZEROSTAR');
        $productrewrite = $this->getProductRewrite($id_product);
        $productlink = $this->context->link->getProductLink($id_product, $productrewrite);
        $cattopmargin = Configuration::get('PS_LGCOMMENTS_CATTOPMARGIN');
        $catbotmargin = Configuration::get('PS_LGCOMMENTS_CATBOTMARGIN');
        if (!$this->isCached('product_list.tpl', $this->getCacheId($id_product))) {
            if ($countcomments) {
                $this->context->smarty->assign(array(
                    'comments' => $countcomments,
                    'sumcomments' => $sumcomments,
                    'averagecomments' => $averagecomments,
                    'starstyle' => Configuration::get('PS_LGCOMMENTS_STARDESIGN1'),
                    'starcolor' => Configuration::get('PS_LGCOMMENTS_STARDESIGN2'),
                    'starsize' => Configuration::get('PS_LGCOMMENTS_STARSIZE'),
                    'zerostar' => $zerostar,
                    'productlink' => $productlink,
                    'cattopmargin' => $cattopmargin,
                    'catbotmargin' => $catbotmargin
                ));
                return $this->display(__FILE__, '/views/templates/front/product_list.tpl');
            } if (!$countcomments and $zerostar) {
                $this->context->smarty->assign(array(
                    'comments' => $countcomments,
                    'starstyle' => Configuration::get('PS_LGCOMMENTS_STARDESIGN1'),
                    'starcolor' => Configuration::get('PS_LGCOMMENTS_STARDESIGN2'),
                    'starsize' => Configuration::get('PS_LGCOMMENTS_STARSIZE'),
                    'zerostar' => $zerostar,
                    'productlink' => $productlink,
                    'cattopmargin' => $cattopmargin,
                    'catbotmargin' => $catbotmargin
                ));
                return $this->display(__FILE__, '/views/templates/front/product_list.tpl');
            }
        }
    }

    /* Store widget */
    public function hookFooter($params)
    {
        $this->context->controller->addJQuery();
        if (!Configuration::get('PS_LGCOMMENTS_DISPLAY')) {
            return;
        } if (Configuration::get('PS_LGCOMMENTS_DISPLAY_LANGUAGE') == 1) {
            $totalcomentarios = $this->getSumShopCommentsByLang();
            $numerocomentarios = $this->getCountShopCommentsByLang();
            $mediacomentarios = @round($totalcomentarios / $numerocomentarios);
            $mediacomentarios2 = @round($totalcomentarios / $numerocomentarios, 1);
            $comentarioazar = $this->getRandomShopCommentByLang();
        } else {
            $totalcomentarios = $this->getSumShopComments();
            $numerocomentarios = $this->getCountShopComments();
            $mediacomentarios = @round($totalcomentarios / $numerocomentarios);
            $mediacomentarios2 = @round($totalcomentarios / $numerocomentarios, 1);
            $comentarioazar = $this->getRandomShopComment();
        }
        $reviewpage = _PS_BASE_URL_.__PS_BASE_URI__.'module/lgcomments/reviews';
        $css_config = unserialize(Configuration::get('PS_LGCOMMENTS_CSS_CONF', ''));
        if (empty($css_config)) {
            $css_config = $this->getExtraRightCSSConfig('customer');
        }
        /* How to display the store review page */
        if (substr_count(_PS_VERSION_, '1.6') > 0) {
            $ps16 = true;
        } else {
            $ps16 = false;
        }
        $this->context->smarty->assign(array(
            'ps16' => $ps16,
            'numerocomentarios' => $numerocomentarios,
            'mediacomentarios' => $mediacomentarios,
            'mediacomentarios2' => $mediacomentarios2,
            'comentarioazar' => $comentarioazar,
            'reviewpage' => $reviewpage,
            'display_type' => Configuration::get('PS_LGCOMMENTS_DISPLAY_TYPE'),
            'display_side' => Configuration::get('PS_LGCOMMENTS_DISPLAY_SIDE'),
            'starstyle' => Configuration::get('PS_LGCOMMENTS_STARDESIGN1'),
            'starcolor' => Configuration::get('PS_LGCOMMENTS_STARDESIGN2'),
            'starsize' => Configuration::get('PS_LGCOMMENTS_STARSIZE'),
            'ratingscale' => Configuration::get('PS_LGCOMMENTS_SCALE'),
            'displaycross' => Configuration::get('PS_LGCOMMENTS_CROSS'),
            'bgdesign1' => Configuration::get('PS_LGCOMMENTS_BGDESIGN1'),
            'bgdesign2' => Configuration::get('PS_LGCOMMENTS_BGDESIGN2'),
            'bgwidth' => $css_config['widget']['width'],
            'bgheight' => $css_config['widget']['height'],
            'top0' => $css_config['title']['top'],
            'left0' => $css_config['title']['left'],
            'color0' => '#777777',
            'width0' => $css_config['title']['width'],
            'textalign0' => $css_config['title']['text-align'],
            'fontfamily0' => $css_config['title']['font-family'],
            'fontsize0' => $css_config['title']['font-size'],
            'fontweight0' => $css_config['title']['font-weight'],
            'lineheight0' => $css_config['title']['line-height'],
            'rotate0' => $css_config['title']['rotate'],
            'top1' => $css_config['rating']['top'],
            'left1' => $css_config['rating']['left'],
            'color1' => '#777777',
            'width1' => $css_config['rating']['width'],
            'textalign1' => $css_config['rating']['text-align'],
            'fontfamily1' => $css_config['rating']['font-family'],
            'fontsize1' => $css_config['rating']['font-size'],
            'fontweight1' => $css_config['rating']['font-weight'],
            'top2' => $css_config['review']['top'],
            'left2' => $css_config['review']['left'],
            'color2' => '#777777',
            'width2' => $css_config['review']['width'],
            'textalign2' => $css_config['review']['text-align'],
            'fontfamily2' => $css_config['review']['font-family'],
            'fontsize2' => $css_config['review']['font-size'],
            'fontweight2' => $css_config['review']['font-weight'],
            'top3' => $css_config['stars']['top'],
            'left3' => $css_config['stars']['left'],
            'width3' => $css_config['stars']['width'],
            'rotate3' => $css_config['stars']['rotate'],
            'top4' => $css_config['see-more']['top'],
            'left4' => $css_config['see-more']['left'],
            'color4' => '#777777',
            'width4' => $css_config['see-more']['width'],
            'textalign4' => $css_config['see-more']['text-align'],
            'fontfamily4' => $css_config['see-more']['font-family'],
            'fontsize4' => $css_config['see-more']['font-size'],
            'fontweight4' => $css_config['see-more']['font-weight'],
            'background5' => Configuration::get('PS_LGCOMMENTS_BACKGROUND5'),
            'bordersize5' => Configuration::get('PS_LGCOMMENTS_BORDERSIZE5'),
            'bordercolor5' => Configuration::get('PS_LGCOMMENTS_BORDERCOLOR5'),
            'ratecolor5' => Configuration::get('PS_LGCOMMENTS_RATECOLOR5'),
            'ratesize5' => Configuration::get('PS_LGCOMMENTS_RATESIZE5'),
            'ratefamily5' => Configuration::get('PS_LGCOMMENTS_RATEFAMILY5'),
            'commentcolor5' => Configuration::get('PS_LGCOMMENTS_COMMENTCOLOR5'),
            'commentsize5' => Configuration::get('PS_LGCOMMENTS_COMMENTSIZE5'),
            'commentfamily5' => Configuration::get('PS_LGCOMMENTS_COMMENTFAMILY5'),
            'commentalign5' => Configuration::get('PS_LGCOMMENTS_COMMENTALIGN5'),
            'datecolor5' => Configuration::get('PS_LGCOMMENTS_DATECOLOR5'),
            'datesize5' => Configuration::get('PS_LGCOMMENTS_DATESIZE5'),
            'datefamily5' => Configuration::get('PS_LGCOMMENTS_DATEFAMILY5'),
            'datealign5' => Configuration::get('PS_LGCOMMENTS_DATEALIGN5'),
            'top6' => Configuration::get('PS_LGCOMMENTS_TOP6'),
            'left6' => Configuration::get('PS_LGCOMMENTS_LEFT6'),
            'top7' => $css_config['cross']['top'],
            'right7' => $css_config['cross']['right'],
            'widgettextcolor' => Configuration::get('PS_LGCOMMENTS_TEXTCOLOR'),
            'path_lgcomments' => _MODULE_DIR_.$this->name
        ));
        if ($totalcomentarios > 0) {
            return ($this->display(__FILE__, '/views/templates/front/store_widget.tpl'));
        }
    }

    public function hookLeftColumn($params)
    {
        return $this->hookFooter($params);
    }

    public function hookRightColumn($params)
    {
        return $this->hookFooter($params);
    }

    /* Store comments on homepage */
    public function hookDisplayHome($params)
    {
        $slidertotal = Configuration::get('PS_LGCOMMENTS_SLIDER_TOTAL');
        if (substr_count(_PS_VERSION_, '1.6') > 0) {
            $ps16 = true;
        } else {
            $ps16 = false;
        }
        $base = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off')  ?
            'https://'.$this->context->shop->domain_ssl :
            'http://'.$this->context->shop->domain);
        if (Configuration::get('PS_LGCOMMENTS_DISPLAY_LANGUAGE') == 1) {
            $totalcomentarios = $this->getSumShopCommentsByLang();
            $numerocomentarios = $this->getCountShopCommentsByLang();
            $mediacomentarios = @round($totalcomentarios / $numerocomentarios);
            $mediacomentarios2 = @round($totalcomentarios / $numerocomentarios, 1);
            $allreviews = $this->getSliderShopCommentsByLang($slidertotal);
        } else {
            $totalcomentarios = $this->getSumShopComments();
            $numerocomentarios = $this->getCountShopComments();
            $mediacomentarios = @round($totalcomentarios / $numerocomentarios);
            $mediacomentarios2 = @round($totalcomentarios / $numerocomentarios, 1);
            $allreviews = $this->getSliderShopComments($slidertotal);
        }
        $this->context->smarty->assign(array(
            'numerocomentarios' => $numerocomentarios,
            'mediacomentarios' => $mediacomentarios,
            'mediacomentarios2' => $mediacomentarios2,
            'allreviews' => $allreviews,
            'ps16' => $ps16,
            'displaysnippets' => Configuration::get('PS_LGCOMMENTS_DISPLAY_SNIPPETS'),
            'storename' => Configuration::get('PS_SHOP_NAME'),
            'address_street1' => Configuration::get('PS_SHOP_ADDR1'),
            'address_street2' => Configuration::get('PS_SHOP_ADDR2'),
            'address_zip' => Configuration::get('PS_SHOP_CODE'),
            'address_city' => Configuration::get('PS_SHOP_CITY'),
            'address_state' => Configuration::get('PS_SHOP_STATE'),
            'address_country' => Configuration::get('PS_SHOP_COUNTRY'),
            'address_phone' => Configuration::get('PS_SHOP_PHONE'),
            'price_range' => Configuration::get('PS_LGCOMMENTS_PRICE_RANGE'),
            'starstyle' => Configuration::get('PS_LGCOMMENTS_STARDESIGN1'),
            'starcolor' => Configuration::get('PS_LGCOMMENTS_STARDESIGN2'),
            'starsize' => Configuration::get('PS_LGCOMMENTS_STARSIZE'),
            'ratingscale' => Configuration::get('PS_LGCOMMENTS_SCALE'),
            'displayslider' => Configuration::get('PS_LGCOMMENTS_DISPLAY_SLIDER'),
            'sliderblocks' => Configuration::get('PS_LGCOMMENTS_SLIDER_BLOCKS'),
            'base_url' => $base,
            'dateformat' => $this->getDateFormat()
        ));
        return $this->display(__FILE__, '/views/templates/front/home_reviews.tpl');
    }

    /* How to display the widget */
    public function getExtraRightCSSConfig($type)
    {
        if (
            substr_count(_PS_VERSION_, '1.6') > 0
            and Configuration::get('PS_LGCOMMENTS_DISPLAY_TYPE') == 2
        ) {
            return $this->getExtraRightCSSConfig16($type);
        } elseif (
            substr_count(_PS_VERSION_, '1.6') > 0
            and Configuration::get('PS_LGCOMMENTS_DISPLAY_TYPE') == 1
        ) {
            return $this->getExtraRightCSSConfig15($type);
        } else {
            return $this->getExtraRightCSSConfig15($type);
        }
    }

    /* Widget configuration for PS 1.6 column */
    public function getExtraRightCSSConfig16($type)
    {
        $config = array();
        // Bubble widget (PS 1.6 column)
        $config['bubble'] = array(
            'title' => array(
                'top' => '45',
                'left' => '12',
                'width' => '130',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '20',
                'font-weight' => 'bold',
                'line-height' => '26',
                'rotate' => '0',
            ),
            'widget' => array(
                'width' => '270',
                'height' => '270',
            ),
            'rating' => array(
                'top' => '60',
                'left' => '150',
                'width' => '100',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '35',
                'font-weight' => 'normal',
            ),
            'review' => array(
                'top' => '165',
                'left' => '30',
                'width' => '200',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '17',
                'font-weight' => 'normal',
            ),
            'stars' => array(
                'top' => '115',
                'left' => '30',
                'width' => '200',
                'rotate' => '0',
            ),
            'see-more' => array(
                'top' => '220',
                'left' => '120',
                'width' => '120',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '20',
                'font-weight' => 'bold',
            ),
            'cross' => array(
                'top' => '20',
                'right' => '20',
            ),
        );
        // Customer widget (PS 1.6 column)
        $config['customer'] = array(
            'widget' => array(
                'width' => '270',
                'height' => '354',
            ),
            'title' => array(
                'top' => '30',
                'left' => '10',
                'width' => '240',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '21',
                'font-weight' => 'bold',
                'line-height' => '22',
                'rotate' => '0',
            ),
            'rating' => array(
                'top' => '100',
                'left' => '80',
                'width' => '100',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '32',
                'font-weight' => 'normal',
            ),
            'review' => array(
                'top' => '195',
                'left' => '88',
                'width' => '150',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '18',
                'font-weight' => 'normal',
            ),
            'stars' => array(
                'top' => '135',
                'left' => '45',
                'width' => '200',
                'rotate' => '0',
            ),
            'see-more' => array(
                'top' => '300',
                'left' => '120',
                'width' => '120',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '20',
                'font-weight' => 'bold',
            ),
            'cross' => array(
                'top' => '20',
                'right' => '20',
            ),
        );
        // Horizontal widget (PS 1.6 column)
        $config['horizontal'] = array(
            'widget' => array(
                'width' => '250',
                'height' => '100',
            ),
            'title' => array(
                'top' => '20',
                'left' => '12',
                'width' => '220',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '20',
                'font-weight' => 'bold',
                'line-height' => '20',
                'rotate' => '0',
            ),
            'rating' => array(
                'top' => '0',
                'left' => '0',
                'width' => '0',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '0',
                'font-weight' => 'normal',
            ),
            'review' => array(
                'top' => '0',
                'left' => '0',
                'width' => '0',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '0',
                'font-weight' => 'normal',
            ),
            'stars' => array(
                'top' => '45',
                'left' => '40',
                'width' => '170',
                'rotate' => '0',
            ),
            'see-more' => array(
                'top' => '0',
                'left' => '0',
                'width' => '0',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '0',
                'font-weight' => 'bold',
            ),
            'cross' => array(
                'top' => '15',
                'right' => '0',
            ),
        );
        // Letter widget (PS 1.6 column)
        $config['letter'] = array(
            'widget' => array(
                'width' => '270',
                'height' => '340',
            ),
            'title' => array(
                'top' => '45',
                'left' => '25',
                'width' => '140',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '24',
                'font-weight' => 'bold',
                'line-height' => '28',
                'rotate' => '0',
            ),
            'rating' => array(
                'top' => '45',
                'left' => '170',
                'width' => '90',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '29',
                'font-weight' => 'normal',
            ),
            'review' => array(
                'top' => '195',
                'left' => '40',
                'width' => '190',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '20',
                'font-weight' => 'normal',
            ),
            'stars' => array(
                'top' => '120',
                'left' => '35',
                'width' => '200',
                'rotate' => '0',
            ),
            'see-more' => array(
                'top' => '295',
                'left' => '120',
                'width' => '120',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '22',
                'font-weight' => 'bold',
            ),
            'cross' => array(
                'top' => '20',
                'right' => '20',
            ),
        );
        // Pentagon widget (PS 1.6 column)
        $config['pentagon'] = array(
            'widget' => array(
                'width' => '270',
                'height' => '297',
            ),
            'title' => array(
                'top' => '0',
                'left' => '0',
                'width' => '270',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '24',
                'font-weight' => 'bold',
                'line-height' => '24',
                'rotate' => '0',
            ),
            'rating' => array(
                'top' => '95',
                'left' => '85',
                'width' => '100',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '29',
                'font-weight' => 'normal',
            ),
            'review' => array(
                'top' => '190',
                'left' => '45',
                'width' => '175',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '17',
                'font-weight' => 'normal',
            ),
            'stars' => array(
                'top' => '145',
                'left' => '35',
                'width' => '200',
                'rotate' => '0',
            ),
            'see-more' => array(
                'top' => '255',
                'left' => '100',
                'width' => '120',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '22',
                'font-weight' => 'bold',
            ),
            'cross' => array(
                'top' => '20',
                'right' => '20',
            ),
        );
        // Shop widget (PS 1.6 column)
        $config['shop'] = array(
            'widget' => array(
                'width' => '270',
                'height' => '347',
            ),
            'title' => array(
                'top' => '32',
                'left' => '15',
                'width' => '240',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '21',
                'font-weight' => 'bold',
                'line-height' => '22',
                'rotate' => '0',
            ),
            'rating' => array(
                'top' => '95',
                'left' => '95',
                'width' => '80',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '26',
                'font-weight' => 'normal',
            ),
            'review' => array(
                'top' => '200',
                'left' => '42',
                'width' => '190',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '19',
                'font-weight' => 'normal',
            ),
            'stars' => array(
                'top' => '140',
                'left' => '35',
                'width' => '200',
                'rotate' => '0',
            ),
            'see-more' => array(
                'top' => '305',
                'left' => '130',
                'width' => '120',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '22',
                'font-weight' => 'bold',
            ),
            'cross' => array(
                'top' => '20',
                'right' => '20',
            ),
        );
        // Vertical widget (PS 1.6 column)
        $config['vertical'] = array(
            'widget' => array(
                'width' => '100',
                'height' => '250',
            ),
            'title' => array(
                'top' => '115',
                'left' => '-85',
                'width' => '230',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '20',
                'font-weight' => 'bold',
                'line-height' => '20',
                'rotate' => '1',
            ),
            'rating' => array(
                'top' => '0',
                'left' => '0',
                'width' => '0',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '0',
                'font-weight' => 'normal',
            ),
            'review' => array(
                'top' => '0',
                'left' => '0',
                'width' => '0',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '0',
                'font-weight' => 'normal',
            ),
            'stars' => array(
                'top' => '105',
                'left' => '-20',
                'width' => '170',
                'rotate' => '1',
            ),
            'see-more' => array(
                'top' => '0',
                'left' => '0',
                'width' => '0',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '0',
                'font-weight' => 'bold',
            ),
            'cross' => array(
                'top' => '20',
                'right' => '5',
            ),
        );

        return isset($config[$type]) ? $config[$type] : array();
    }

    /* Widget configuration for PS 1.6 side and PS 1.5 */
    public function getExtraRightCSSConfig15($type)
    {
        $config = array();
        // Bubble widget (PS 1.6 side and PS 1.5)
        $config['bubble'] = array(
            'widget' => array(
                'width' => '200',
                'height' => '203',
            ),
            'title' => array(
                'top' => '30',
                'left' => '10',
                'width' => '95',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '15',
                'font-weight' => 'bold',
                'line-height' => '22',
                'rotate' => '0',
            ),
            'rating' => array(
                'top' => '45',
                'left' => '115',
                'width' => '60',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '25',
                'font-weight' => 'normal',
            ),
            'review' => array(
                'top' => '125',
                'left' => '10',
                'width' => '170',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '12',
                'font-weight' => 'normal',
            ),
            'stars' => array(
                'top' => '85',
                'left' => '20',
                'width' => '150',
                'rotate' => '0',
            ),
            'see-more' => array(
                'top' => '165',
                'left' => '60',
                'width' => '120',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '15',
                'font-weight' => 'bold',
            ),
            'cross' => array(
                'top' => '25',
                'right' => '15',
            ),
        );
        // Customer widget (PS 1.6 side and PS 1.5)
        $config['customer'] = array(
            'widget' => array(
                'width' => '200',
                'height' => '262',
            ),
            'title' => array(
                'top' => '23',
                'left' => '5',
                'width' => '180',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '16',
                'font-weight' => 'bold',
                'line-height' => '16',
                'rotate' => '0',
            ),
            'rating' => array(
                'top' => '70',
                'left' => '6',
                'width' => '180',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '24',
                'font-weight' => 'normal',
            ),
            'review' => array(
                'top' => '142',
                'left' => '63',
                'width' => '115',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '12',
                'font-weight' => 'normal',
            ),
            'stars' => array(
                'top' => '100',
                'left' => '32',
                'width' => '150',
                'rotate' => '0',
            ),
            'see-more' => array(
                'top' => '225',
                'left' => '70',
                'width' => '120',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '16',
                'font-weight' => 'bold',
            ),
            'cross' => array(
                'top' => '20',
                'right' => '10',
            ),
        );
        // Horizontal widget (PS 1.6 side and PS 1.5)
        $config['horizontal'] = array(
            'widget' => array(
                'width' => '200',
                'height' => '80',
            ),
            'title' => array(
                'top' => '15',
                'left' => '12',
                'width' => '180',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '16',
                'font-weight' => 'bold',
                'line-height' => '16',
                'rotate' => '0',
            ),
            'rating' => array(
                'top' => '0',
                'left' => '0',
                'width' => '0',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '0',
                'font-weight' => 'normal',
            ),
            'review' => array(
                'top' => '0',
                'left' => '0',
                'width' => '0',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '0',
                'font-weight' => 'normal',
            ),
            'stars' => array(
                'top' => '35',
                'left' => '25',
                'width' => '150',
                'rotate' => '0',
            ),
            'see-more' => array(
                'top' => '0',
                'left' => '0',
                'width' => '0',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '0',
                'font-weight' => 'bold',
            ),
            'cross' => array(
                'top' => '15',
                'right' => '0',
            ),
        );
        // Letter widget (PS 1.6 side and PS 1.5)
        $config['letter'] = array(
            'widget' => array(
                'width' => '200',
                'height' => '252',
            ),
            'title' => array(
                'top' => '35',
                'left' => '12',
                'width' => '110',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '18',
                'font-weight' => 'bold',
                'line-height' => '22',
                'rotate' => '0',
            ),
            'rating' => array(
                'top' => '32',
                'left' => '128',
                'width' => '60',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '21',
                'font-weight' => 'normal',
            ),
            'review' => array(
                'top' => '145',
                'left' => '30',
                'width' => '140',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '13',
                'font-weight' => 'normal',
            ),
            'stars' => array(
                'top' => '88',
                'left' => '20',
                'width' => '160',
                'rotate' => '0',
            ),
            'see-more' => array(
                'top' => '220',
                'left' => '70',
                'width' => '110',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '16',
                'font-weight' => 'bold',
            ),
            'cross' => array(
                'top' => '25',
                'right' => '5',
            ),
        );
        // Pentagon widget (PS 1.6 side and PS 1.5)
        $config['pentagon'] = array(
            'widget' => array(
                'width' => '200',
                'height' => '220',
            ),
            'title' => array(
                'top' => '3',
                'left' => '0',
                'width' => '200',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '18',
                'font-weight' => 'bold',
                'line-height' => '18',
                'rotate' => '0',
            ),
            'rating' => array(
                'top' => '68',
                'left' => '71',
                'width' => '60',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '21',
                'font-weight' => 'normal',
            ),
            'review' => array(
                'top' => '140',
                'left' => '22',
                'width' => '158',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '12',
                'font-weight' => 'normal',
            ),
            'stars' => array(
                'top' => '105',
                'left' => '20',
                'width' => '160',
                'rotate' => '0',
            ),
            'see-more' => array(
                'top' => '190',
                'left' => '50',
                'width' => '120',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '16',
                'font-weight' => 'bold',
            ),
            'cross' => array(
                'top' => '0',
                'right' => '5',
            ),
        );
        // Shop widget (PS 1.6 side and PS 1.5)
        $config['shop'] = array(
            'widget' => array(
                'width' => '200',
                'height' => '257',
            ),
            'title' => array(
                'top' => '22',
                'left' => '15',
                'width' => '170',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '15',
                'font-weight' => 'bold',
                'line-height' => '17',
                'rotate' => '0',
            ),
            'rating' => array(
                'top' => '70',
                'left' => '70',
                'width' => '60',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '19',
                'font-weight' => 'normal',
            ),
            'review' => array(
                'top' => '145',
                'left' => '30',
                'width' => '140',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '13',
                'font-weight' => 'normal',
            ),
            'stars' => array(
                'top' => '100',
                'left' => '20',
                'width' => '160',
                'rotate' => '0',
            ),
            'see-more' => array(
                'top' => '225',
                'left' => '70',
                'width' => '120',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '17',
                'font-weight' => 'bold',
            ),
            'cross' => array(
                'top' => '17',
                'right' => '5',
            ),
        );
        // Vertical widget (PS 1.6 side and PS 1.5)
        $config['vertical'] = array(
            'widget' => array(
                'width' => '80',
                'height' => '200',
            ),
            'title' => array(
                'top' => '92',
                'left' => '-65',
                'width' => '180',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '16',
                'font-weight' => 'bold',
                'line-height' => '16',
                'rotate' => '1',
            ),
            'rating' => array(
                'top' => '0',
                'left' => '0',
                'width' => '0',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '0',
                'font-weight' => 'normal',
            ),
            'review' => array(
                'top' => '0',
                'left' => '0',
                'width' => '0',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '0',
                'font-weight' => 'normal',
            ),
            'stars' => array(
                'top' => '85',
                'left' => '-20',
                'width' => '150',
                'rotate' => '1',
            ),
            'see-more' => array(
                'top' => '0',
                'left' => '0',
                'width' => '0',
                'text-align' => 'Center',
                'font-family' => 'Arial',
                'font-size' => '0',
                'font-weight' => 'bold',
            ),
            'cross' => array(
                'top' => '20',
                'right' => '5',
            ),
        );

        return isset($config[$type]) ? $config[$type] : array();
    }
}
