Nouvel avis reçu
Le client {customer_firstname} {customer_lastname} vient de rédiger un avis sur le produit suivant : {product_details}
Vous pouvez vérifier, éditer et publier ces avis depuis le menu "Catalogue > Avis Produits" de votre back-office.