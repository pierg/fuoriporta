You have received a new review
The customer {customer_firstname} {customer_lastname} has written a review about your shop: {shop_details}
You can check, edit and publish the reviews from the menu "Catalog > Store Reviews" of your back-office.