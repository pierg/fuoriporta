<?php
/**
 *  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
 *
 * @author    Línea Gráfica E.C.E. S.L.
 * @copyright Lineagrafica.es - Línea Gráfica E.C.E. S.L. all rights reserved.
 * @license   https://www.lineagrafica.es/licenses/license_en.pdf https://www.lineagrafica.es/licenses/license_es.pdf https://www.lineagrafica.es/licenses/license_fr.pdf
 */

class LgCommentsAccountreviewsModuleFrontController extends ModuleFrontController
{
    public $ssl = true;
    public $display_column_left = false;

    public function __construct()
    {
        parent::__construct();
        $this->context = Context::getContext();
    }

    /**
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();
        $this->assign();
    }

    private function getDateFormat()
    {
        $format = Db::getInstance()->getValue(
            'SELECT date_format_lite '.
            'FROM '._DB_PREFIX_.'lang '.
            'WHERE id_lang = '.(int)$this->context->language->id
        );
        return $format;
    }

    /**
     * Assign wishlist template
     */
    public function assign()
    {
        $lgreviews = Db::getInstance()->ExecuteS(
            'SELECT * '.
            'FROM '._DB_PREFIX_.'lgcomments_orders lo '.
            'LEFT JOIN '._DB_PREFIX_.'orders o ON lo.id_order = o.id_order '.
            'LEFT JOIN '._DB_PREFIX_.'currency c ON o.id_currency = c.id_currency '.
            'WHERE lo.id_customer = '.(int)$this->context->customer->id.' '.
            'ORDER BY lo.id_order DESC'
        );

        $this->context->smarty->assign(
            array(
                'lgreviews' => $lgreviews,
                'dateformat' => $this->getDateFormat()
            )
        );

        $this->setTemplate('account_reviews.tpl');
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addJquery();
    }
}
