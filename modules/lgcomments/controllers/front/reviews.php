<?php
/**
 *  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
 *
 * @author    Línea Gráfica E.C.E. S.L.
 * @copyright Lineagrafica.es - Línea Gráfica E.C.E. S.L. all rights reserved.
 * @license   https://www.lineagrafica.es/licenses/license_en.pdf https://www.lineagrafica.es/licenses/license_es.pdf https://www.lineagrafica.es/licenses/license_fr.pdf
 */

class LGCommentsReviewsModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();
        $this->assignCommentsList();
        $this->context->smarty->assign('current_theme_dir', _THEME_DIR_);
        $this->addJqueryPlugin('fancybox');
        $this->setTemplate('store_reviews.tpl');

        if (Tools::getValue('action') == 'sendReview') {
            $this->sendReview();
        }
    }

    private function getLGCommentsByRate($min, $max, $order)
    {
        $p = $this->p;
        $n = $this->n;
        $rates = Db::getInstance()->ExecuteS(
            'SELECT st.*, CONCAT(c.firstname, \' \', (SUBSTRING(c.lastname,1,1)), \'.\') as customer '.
            'FROM `'._DB_PREFIX_.'lgcomments_storecomments` st '.
            'LEFT JOIN `'._DB_PREFIX_.'customer` c ON st.`id_customer` = c.`id_customer` '.
            'WHERE st.active = 1 '.
            'AND st.stars >= '.(int)$min.' AND st.stars < '.(int)$max.' '.
            'ORDER BY st.position '.$order.' '.
            'LIMIT '.(((int)$p - 1) * (int)$n).','.(int)$n
        );
        return $rates;
    }

    private function getLGCommentsByRateAndLang($min, $max, $order)
    {
        $p = $this->p;
        $n = $this->n;
        $ratesL = Db::getInstance()->ExecuteS(
            'SELECT st.*, CONCAT(c.firstname, \' \', (SUBSTRING(c.lastname,1,1)), \'.\') as customer '.
            'FROM `'._DB_PREFIX_.'lgcomments_storecomments` st '.
            'LEFT JOIN `'._DB_PREFIX_.'customer` c ON st.`id_customer` = c.`id_customer` '.
            'WHERE st.active = 1 '.
            'AND st.id_lang = '.(int)$this->context->language->id.' '.
            'AND st.stars >= '.(int)$min.' AND st.stars < '.(int)$max.' '.
            'ORDER BY st.position '.$order.' '.
            'LIMIT '.(((int)$p - 1) * (int)$n).','.(int)$n
        );
        return $ratesL;
    }

    private function getLGCountByRate($min, $max)
    {
        $count = Db::getInstance()->getValue(
            'SELECT COUNT(id_lgcomments_storecomments) AS total '.
            'FROM '._DB_PREFIX_.'lgcomments_storecomments '.
            'WHERE active = 1 '.
            'AND stars >= '.(int)$min.' '.
            'AND stars < '.(int)$max.''
        );
        return $count;
    }

    private function getLGCountByRateAndLang($min, $max)
    {
        $countL = Db::getInstance()->getValue(
            'SELECT COUNT(id_lgcomments_storecomments) AS total '.
            'FROM '._DB_PREFIX_.'lgcomments_storecomments '.
            'WHERE active = 1 '.
            'AND id_lang = '.(int)$this->context->language->id.' '.
            'AND stars >= '.(int)$min.' '.
            'AND stars < '.(int)$max.''
        );
        return $countL;
    }

    private function getLGSum()
    {
        $sum = Db::getInstance()->getValue(
            'SELECT SUM(stars) AS totalcomentarios '.
            'FROM '._DB_PREFIX_.'lgcomments_storecomments '.
            'WHERE active = 1'
        );
        return $sum;
    }

    private function getLGSumByLang()
    {
        $sumL = Db::getInstance()->getValue(
            'SELECT SUM(stars) AS totalcomentarios '.
            'FROM '._DB_PREFIX_.'lgcomments_storecomments '.
            'WHERE active = 1'.
            ' AND id_lang = '.(int)$this->context->language->id
        );
        return $sumL;
    }

    private function checkIfAlreadyReviewed()
    {
        $check = Db::getInstance()->getValue(
            'SELECT COUNT(id_lgcomments_storecomments) '.
            'FROM '._DB_PREFIX_.'lgcomments_storecomments '.
            'WHERE id_customer = '.(int)$this->context->customer->id
        );
        return $check;
    }

    private function getMaxPosition()
    {
        $maxposition = Db::getInstance()->getValue(
            'SELECT MAX(`position`) '.
            'FROM '._DB_PREFIX_.'lgcomments_storecomments'
        );
        return $maxposition;
    }

    private function getCustomerEmail($id)
    {
        $customerEmail = Db::getInstance()->getValue(
            'SELECT email '.
            'FROM '._DB_PREFIX_.'customer '.
            'WHERE id_customer = '.(int)$id
        );
        return $customerEmail;
    }

    private function getFirstname($id)
    {
        $firstname = Db::getInstance()->getValue(
            'SELECT firstname '.
            'FROM '._DB_PREFIX_.'customer '.
            'WHERE id_customer = '.(int)$id
        );
        return $firstname;
    }

    private function getLastname($id)
    {
        $lastname = Db::getInstance()->getValue(
            'SELECT lastname '.
            'FROM '._DB_PREFIX_.'customer '.
            'WHERE id_customer = '.(int)$id
        );
        return $lastname;
    }

    private function getProductName($product_id, $lang_id)
    {
        $productName = Db::getInstance()->getValue(
            'SELECT name '.
            'FROM '._DB_PREFIX_.'product_lang '.
            'WHERE id_product = '.(int)$product_id.' '.
            'AND id_lang = '.(int)$lang_id
        );
        return $productName;
    }

    private function getDateFormat()
    {
        $format = Db::getInstance()->getValue(
            'SELECT date_format_lite '.
            'FROM '._DB_PREFIX_.'lang '.
            'WHERE id_lang = '.(int)$this->context->language->id
        );
        return $format;
    }

    /* Assign list of products template vars */
    public function assignCommentsList()
    {
        $this->nbProducts = $this->getComments(null, null, null, false);
        $this->pagination((int)$this->nbProducts); // Pagination must be call after "getProducts"
        $reviews = $this->getComments(
            $this->context->language->id,
            (int)Tools::getValue('p', 1),
            (int)Tools::getValue('n', (int)Configuration::get('PS_LGCOMMENTS_PER_PAGE')),
            false
        );

        $this->context->smarty->assign(array(
            'nb_products' => $this->nbProducts,
            'reviews' => $reviews,
        ));
    }

    public function getComments(
        $id_lang,
        $p,
        $n,
        $get_total = false
    ) {
        if ($p < 1) {
            $p = 1;
        }
        /* Return only the number of products */

        if (Configuration::get('PS_LGCOMMENTS_DISPLAY_LANGUAGE') == 1) {
            if ($get_total) {
                $sql = $this->getLGCountByRateAndLang('0', '11');
            }
            $sql = ''.
                'SELECT st.*, CONCAT(c.firstname, \' \', (SUBSTRING(c.lastname,1,1)), \'.\') as customer '.
                'FROM `'._DB_PREFIX_.'lgcomments_storecomments` st '.
                'INNER JOIN `'._DB_PREFIX_.'customer` c ON st.`id_customer` = c.`id_customer` '.
                'WHERE st.id_lang = '.(int)$id_lang.
                ' AND st.active = 1';
        } else {
            if ($get_total) {
                $sql = $this->getLGCountByRate('0', '11');
            }
            $sql = ''.
                'SELECT st.*, CONCAT(c.firstname, \' \', (SUBSTRING(c.lastname,1,1)), \'.\') as customer '.
                'FROM `'._DB_PREFIX_.'lgcomments_storecomments` st '.
                'INNER JOIN `'._DB_PREFIX_.'customer` c ON st.`id_customer` = c.`id_customer` '.
                'WHERE st.active = 1';
        }

        $sql .= ' ORDER BY st.position';
        $sql .= ' LIMIT '.(((int)$p - 1) * (int)$n).','.(int)$n;

        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        if (!$result) {
            return array();
        }

        /* Modify SQL result */
        return $result;
    }

    public function pagination($total_products = null)
    {
        if (!self::$initialized) {
            $this->init();
        } elseif (!$this->context) {
            $this->context = Context::getContext();
        }

        $default_products_per_page = max(1, (int)Configuration::get('PS_LGCOMMENTS_PER_PAGE'));
        $nArray = array($default_products_per_page, $default_products_per_page * 2, $default_products_per_page * 5);

        if ((int)Tools::getValue('n') && (int)$total_products > 0) {
            $nArray[] = $total_products;
        }
        // Retrieve the current number of products per page
        // (either the default, the GET parameter or the one in the cookie)
        $this->n = $default_products_per_page;
        if ((int)Tools::getValue('n') && in_array((int)Tools::getValue('n'), $nArray)) {
            $this->n = (int)Tools::getValue('n');
            if (
                isset($this->context->cookie->nb_item_per_page)
                &&
                in_array($this->context->cookie->nb_item_per_page, $nArray)
            ) {
                $this->n = (int)$this->context->cookie->nb_item_per_page;
            }
        }

        // Retrieve the page number (either the GET parameter or the first page)
        $this->p = (int)Tools::getValue('p', 1);
        // If the parameter is not correct then redirect
        // (do not merge with the previous line, the redirect
        // is required in order to avoid duplicate content)
        if (!is_numeric($this->p) || $this->p < 1) {
            Tools::redirect(self::$link->getPaginationLink(false, false, $this->n, false, 1, false));
        }

        // Remove the page parameter in order to get a clean URL for the pagination template
        $current_url = preg_replace('/(\?)?(&amp;)?p=\d+/', '$1', Tools::htmlentitiesUTF8($_SERVER['REQUEST_URI']));

        if ($this->n != $default_products_per_page) {
            $this->context->cookie->nb_item_per_page = $this->n;
        }

        $pages_nb = ceil($total_products / (int)$this->n);
        if ($this->p > $pages_nb && $total_products != 0) {
            Tools::redirect(self::$link->getPaginationLink(false, false, $this->n, false, $pages_nb, false));
        }

        $range = 2; /* how many pages around page selected */
        $start = (int)($this->p - $range);
        if ($start < 1) {
            $start = 1;
        }
        $stop = (int)($this->p + $range);
        if ($stop > $pages_nb) {
            $stop = (int)$pages_nb;
        }

        if (Configuration::get('PS_LGCOMMENTS_DISPLAY_LANGUAGE') == 1) {
            $totalcomentarios = $this->getLGSumByLang();
            $numerocomentarios = $this->getLGCountByRateAndLang('0', '11');
            $mediacomentarios = @round($totalcomentarios / $numerocomentarios);
            $mediacomentarios2 = @round($totalcomentarios / $numerocomentarios, 1);
            if (Configuration::get('PS_LGCOMMENTS_DISPLAY_ORDER') == 1) {
                $reviewsall = $this->getLGCommentsByRateAndLang('0', '11', 'ASC');
                $reviews5 = $this->getLGCommentsByRateAndLang('10', '11', 'ASC');
                $reviews4 = $this->getLGCommentsByRateAndLang('8', '10', 'ASC');
                $reviews3 = $this->getLGCommentsByRateAndLang('6', '8', 'ASC');
                $reviews2 = $this->getLGCommentsByRateAndLang('4', '6', 'ASC');
                $reviews1 = $this->getLGCommentsByRateAndLang('2', '4', 'ASC');
                $reviews0 = $this->getLGCommentsByRateAndLang('0', '2', 'ASC');
            } if (Configuration::get('PS_LGCOMMENTS_DISPLAY_ORDER') == 2) {
                $reviewsall = $this->getLGCommentsByRateAndLang('0', '11', 'DESC');
                $reviews5 = $this->getLGCommentsByRateAndLang('10', '11', 'DESC');
                $reviews4 = $this->getLGCommentsByRateAndLang('8', '10', 'DESC');
                $reviews3 = $this->getLGCommentsByRateAndLang('6', '8', 'DESC');
                $reviews2 = $this->getLGCommentsByRateAndLang('4', '6', 'DESC');
                $reviews1 = $this->getLGCommentsByRateAndLang('2', '4', 'DESC');
                $reviews0 = $this->getLGCommentsByRateAndLang('0', '2', 'DESC');
            }
            $allstars = $this->getLGCountByRateAndLang('0', '11');
            $fivestars = $this->getLGCountByRateAndLang('10', '11');
            $fourstars = $this->getLGCountByRateAndLang('8', '10');
            $threestars = $this->getLGCountByRateAndLang('6', '8');
            $twostars = $this->getLGCountByRateAndLang('4', '6');
            $onestar = $this->getLGCountByRateAndLang('2', '4');
            $zerostar = $this->getLGCountByRateAndLang('0', '2');
        } else {
            $totalcomentarios = $this->getLGSum();
            $numerocomentarios = $this->getLGCountByRate('0', '11');
            $mediacomentarios = @round($totalcomentarios / $numerocomentarios);
            $mediacomentarios2 = @round($totalcomentarios / $numerocomentarios, 1);
            if (Configuration::get('PS_LGCOMMENTS_DISPLAY_ORDER') == 1) {
                $reviewsall = $this->getLGCommentsByRate('0', '11', 'ASC');
                $reviews5 = $this->getLGCommentsByRate('10', '11', 'ASC');
                $reviews4 = $this->getLGCommentsByRate('8', '10', 'ASC');
                $reviews3 = $this->getLGCommentsByRate('6', '8', 'ASC');
                $reviews2 = $this->getLGCommentsByRate('4', '6', 'ASC');
                $reviews1 = $this->getLGCommentsByRate('2', '4', 'ASC');
                $reviews0 = $this->getLGCommentsByRate('0', '2', 'ASC');
            } if (Configuration::get('PS_LGCOMMENTS_DISPLAY_ORDER') == 2) {
                $reviewsall = $this->getLGCommentsByRate('0', '11', 'DESC');
                $reviews5 = $this->getLGCommentsByRate('10', '11', 'DESC');
                $reviews4 = $this->getLGCommentsByRate('8', '10', 'DESC');
                $reviews3 = $this->getLGCommentsByRate('6', '8', 'DESC');
                $reviews2 = $this->getLGCommentsByRate('4', '6', 'DESC');
                $reviews1 = $this->getLGCommentsByRate('2', '4', 'DESC');
                $reviews0 = $this->getLGCommentsByRate('0', '2', 'DESC');
            }
            $allstars = $this->getLGCountByRate('0', '11');
            $fivestars = $this->getLGCountByRate('10', '11');
            $fourstars = $this->getLGCountByRate('8', '10');
            $threestars = $this->getLGCountByRate('6', '8');
            $twostars = $this->getLGCountByRate('4', '6');
            $onestar = $this->getLGCountByRate('2', '4');
            $zerostar = $this->getLGCountByRate('0', '2');
        } if (substr_count(_PS_VERSION_, '1.6') > 0) {
            $ps16 = true;
        } else {
            $ps16 = false;
        }

        $this->context->smarty->assign(array(
            'reviewsall' => $reviewsall,
            'reviews5' => $reviews5,
            'reviews4' => $reviews4,
            'reviews3' => $reviews3,
            'reviews2' => $reviews2,
            'reviews1' => $reviews1,
            'reviews0' => $reviews0,
            'allstars' => $allstars,
            'fivestars' => $fivestars,
            'fourstars' => $fourstars,
            'threestars' => $threestars,
            'twostars' => $twostars,
            'onestar' => $onestar,
            'zerostar' => $zerostar,
            'nb_products' => $total_products,
            'products_per_page' => $this->n,
            'pages_nb' => $pages_nb,
            'p' => $this->p,
            'n' => $this->n,
            'nArray' => $nArray,
            'range' => $range,
            'start' => $start,
            'stop' => $stop,
            'current_url' => $current_url,
            'ps16' => $ps16,
            'numerocomentarios' => $numerocomentarios,
            'mediacomentarios' => $mediacomentarios,
            'mediacomentarios2' => $mediacomentarios2,
            'starstyle' => Configuration::get('PS_LGCOMMENTS_STARDESIGN1'),
            'starcolor' => Configuration::get('PS_LGCOMMENTS_STARDESIGN2'),
            'starsize' => Configuration::get('PS_LGCOMMENTS_STARSIZE'),
            'ratingscale' => Configuration::get('PS_LGCOMMENTS_SCALE'),
            'shop_name' => Configuration::get('PS_SHOP_NAME'),
            'displaysnippets' => Configuration::get('PS_LGCOMMENTS_DISPLAY_SNIPPETS'),
            'storetextcolor' => Configuration::get('PS_LGCOMMENTS_TEXTCOLOR2'),
            'storebackcolor' => Configuration::get('PS_LGCOMMENTS_BACKCOLOR2'),
            'reviewsbypage' => Configuration::get('PS_LGCOMMENTS_PER_PAGE'),
            'storefilter' => Configuration::get('PS_LGCOMMENTS_STORE_FILTER'),
            'storeform' => Configuration::get('PS_LGCOMMENTS_STORE_FORM'),
            'alreadyreviewed' => $this->checkIfAlreadyReviewed(),
            'dateformat' => $this->getDateFormat(),
            'is_https' =>
            (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"? 'true':'false'),
        ));
    }

    public function sendReview()
    {
        if (Tools::getValue('send_store_review')) {
            $isocode = pSQL(Tools::getValue('isocode', ''));
            $id_lang =(int)Language::getIdByIso(''.$isocode.'');
            $maxposition = $this->getMaxPosition();
            $commentvalidation = Configuration::get('PS_LGCOMMENTS_VALIDATION');
            if ($commentvalidation == 0) {
                $commentstatus = 1;
            } else {
                $commentstatus == 0;
            }
            $params = array();
            $params['date'] = date('Y-m-d', strtotime('now'));
            $params['id_customer'] = pSQL(Tools::getValue('customerid', ''));
            $params['id_order'] = 0;
            $params['stars'] = pSQL(Tools::getValue('score_store', ''));
            $params['comment'] = pSQL(Tools::getValue('comment_store', ''));
            $params['id_lang'] = $id_lang;
            $params['active'] = $commentstatus;
            $params['position'] = ($maxposition + 1);
            $params['title'] = pSQL(Tools::getValue('title_store', ''));
            $params['answer'] = 0;
            $params['id_lgcomments_storecomments'] = '';
            DB::getInstance()->insert('lgcomments_storecomments', $params);
            $customerEmail = $this->getCustomerEmail(Tools::getValue('customerid'));
            $langs = Language::getLanguages();
            foreach ($langs as $lang) {
                if ($id_lang == $lang['id_lang']) {
                    $subject2 = Configuration::get('PS_LGCOMMENTS_SUBJECT2'.$lang['iso_code']);
                }
            }
            $firstname = $this->getFirstname(Tools::getValue('customerid'));
            $templateVars = array(
                '{firstname}' => $firstname,
            );
            // Check if email template exists for current iso code. If not, use English template.
            $module_path = _PS_MODULE_DIR_.'lgcomments/mails/'.Language::getIsoById($id_lang).'/';
            $template_path = _PS_THEME_DIR_.'modules/lgcomments/mails/'.Language::getIsoById($id_lang).'/';
            if (is_dir($module_path) or is_dir($template_path)) {
                $langId = $id_lang;
            } else {
                $langId = (int)Language::getIdByIso('en');
            }
            Mail::Send(
                (int)$langId,
                'thank-you',
                $subject2,
                $templateVars,
                $customerEmail,
                null,
                null,
                Configuration::get('PS_SHOP_NAME'),
                null,
                null,
                dirname(__FILE__).'/../../mails/'
            );
            $email_cron = Configuration::get('PS_LGCOMMENTS_EMAIL_CRON');
            $email_alerts = Configuration::get('PS_LGCOMMENTS_EMAIL_ALERTS');
            $shop_details = '<br>';
            $shop_details .= '<b>'.Tools::getValue('score_store').'/10</b>';
            $shop_details .= '<b>&nbsp;'.Tools::getValue('title_store').'</b><br>';
            $shop_details .= '"'.Tools::getValue('comment_store').'"<br>';
            if ($email_cron and $email_alerts == 1) {
                $templateVars = array(
                    '{customer_firstname}' => $this->getFirstname(Tools::getValue('customerid')),
                    '{customer_lastname}' => $this->getLastname(Tools::getValue('customerid')),
                    '{shop_details}' => $shop_details,
                );
                // Check if email template exists for current iso code. If not, use English template.
                $default = Configuration::get('PS_LANG_DEFAULT');
                $module_path2 = _PS_MODULE_DIR_.'lgcomments/mails/'.Language::getIsoById($default).'/';
                $template_path2 = _PS_THEME_DIR_.'modules/lgcomments/mails/'.Language::getIsoById($default).'/';
                if (is_dir($module_path2) or is_dir($template_path2)) {
                    $langId2 = $default;
                } else {
                    $langId2 = (int)Language::getIdByIso('en');
                }
                Mail::Send(
                    (int)$langId2,
                    'new-review-store',
                    Configuration::get('PS_LGCOMMENTS_SUBJECT_NEWREVIEWS'),
                    $templateVars,
                    $email_cron,
                    null,
                    null,
                    Configuration::get('PS_SHOP_NAME'),
                    null,
                    null,
                    dirname(__FILE__).'/../../mails/'
                );
            }
        }
        if (Tools::getValue('send_product_review')) {
            $isocode = pSQL(Tools::getValue('isocode', ''));
            $id_lang =(int)Language::getIdByIso(''.$isocode.'');
            $maxposition = $this->getMaxPosition();
            $commentvalidation = Configuration::get('PS_LGCOMMENTS_VALIDATION');
            if ($commentvalidation == 0) {
                $commentstatus = 1;
            } else {
                $commentstatus == 0;
            }
            $params = array();
            $params['date'] = date('Y-m-d', strtotime('now'));
            $params['id_customer'] = pSQL(Tools::getValue('customerid', ''));
            $params['id_product'] = pSQL(Tools::getValue('id_product', 0));
            $params['stars'] = pSQL(Tools::getValue('score_product', ''));
            $params['comment'] = pSQL(Tools::getValue('comment_product', ''));
            $params['id_lang'] = $id_lang;
            $params['active'] = $commentstatus;
            $params['position'] = ($maxposition + 1);
            $params['title'] = pSQL(Tools::getValue('title_product', ''));
            $params['answer'] = 0;
            $params['id_lgcomments_productcomments'] = '';
            DB::getInstance()->insert('lgcomments_productcomments', $params);

            $customerEmail = $this->getCustomerEmail(Tools::getValue('customerid'));
            $langs = Language::getLanguages();
            foreach ($langs as $lang) {
                if ($id_lang == $lang['id_lang']) {
                    $subject2 = Configuration::get('PS_LGCOMMENTS_SUBJECT2'.$lang['iso_code']);
                }
            }
            $firstname = $this->getFirstname(Tools::getValue('customerid'));
            $templateVars = array(
                '{firstname}' => $firstname,
            );
            // Check if email template exists for current iso code. If not, use English template.
            $module_path = _PS_MODULE_DIR_.'lgcomments/mails/'.Language::getIsoById($id_lang).'/';
            $template_path = _PS_THEME_DIR_.'modules/lgcomments/mails/'.Language::getIsoById($id_lang).'/';
            if (is_dir($module_path) or is_dir($template_path)) {
                $langId = $id_lang;
            } else {
                $langId = (int)Language::getIdByIso('en');
            }
            Mail::Send(
                (int)$langId,
                'thank-you',
                $subject2,
                $templateVars,
                $customerEmail,
                null,
                null,
                Configuration::get('PS_SHOP_NAME'),
                null,
                null,
                dirname(__FILE__).'/../../mails/'
            );
            $email_cron = Configuration::get('PS_LGCOMMENTS_EMAIL_CRON');
            $email_alerts = Configuration::get('PS_LGCOMMENTS_EMAIL_ALERTS');
            $product_name = $this->getProductName(Tools::getValue('id_product', 0), $id_lang);
            $product_details = '<br>';
            $product_details .= $product_name.'<br>';
            $product_details .= '<b>'.Tools::getValue('score_product').'/10</b>';
            $product_details .= '<b>&nbsp;'.Tools::getValue('title_product').'</b><br>';
            $product_details .= '"'.Tools::getValue('comment_product').'"<br>';
            if ($email_cron and $email_alerts == 1) {
                $templateVars = array(
                    '{customer_firstname}' => $this->getFirstname(Tools::getValue('customerid')),
                    '{customer_lastname}' => $this->getLastname(Tools::getValue('customerid')),
                    '{product_details}' => $product_details,
                );
                // Check if email template exists for current iso code. If not, use English template.
                $default = Configuration::get('PS_LANG_DEFAULT');
                $module_path2 = _PS_MODULE_DIR_.'lgcomments/mails/'.Language::getIsoById($default).'/';
                $template_path2 = _PS_THEME_DIR_.'modules/lgcomments/mails/'.Language::getIsoById($default).'/';
                if (is_dir($module_path2) or is_dir($template_path2)) {
                    $langId2 = $default;
                } else {
                    $langId2 = (int)Language::getIdByIso('en');
                }
                Mail::Send(
                    (int)$langId2,
                    'new-review-product',
                    Configuration::get('PS_LGCOMMENTS_SUBJECT_NEWREVIEWS'),
                    $templateVars,
                    $email_cron,
                    null,
                    null,
                    Configuration::get('PS_SHOP_NAME'),
                    null,
                    null,
                    dirname(__FILE__).'/../../mails/'
                );
            }
        }
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addJquery();
    }
}
