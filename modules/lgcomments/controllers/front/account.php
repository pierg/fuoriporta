<?php
/**
 *  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
 *
 * @author    Línea Gráfica E.C.E. S.L.
 * @copyright Lineagrafica.es - Línea Gráfica E.C.E. S.L. all rights reserved.
 * @license   https://www.lineagrafica.es/licenses/license_en.pdf https://www.lineagrafica.es/licenses/license_es.pdf https://www.lineagrafica.es/licenses/license_fr.pdf
 */

class LGCommentsAccountModuleFrontController extends ModuleFrontController
{
    public $ssl = true;

    public function initContent()
    {
        parent::initContent();
        $id_order = (int)Tools::getValue('id_order');
        $lghash = Tools::getValue('lghash');
        if (
            Db::getInstance()->ExecuteS(
                'SELECT * '.
                'FROM '._DB_PREFIX_.'lgcomments_orders '.
                'WHERE id_order = '.(int)$id_order.
                ' AND hash = \''.pSQL($lghash).'\''
            )
        ) {
            $voted = Db::getInstance()->getValue(
                'SELECT voted '.
                'FROM '._DB_PREFIX_.'lgcomments_orders '.
                'WHERE id_order = '.(int)$id_order.
                ' AND hash = \''.pSQL($lghash).'\''
            );
            if ($voted == 0) {
                $verify = 1;
                $id_lang = Db::getInstance()->getValue(
                    'SELECT id_lang '.
                    'FROM '._DB_PREFIX_.'orders '.
                    'WHERE id_order = '.(int)$id_order
                );
                $id_shop = Db::getInstance()->getValue(
                    'SELECT id_shop '.
                    'FROM '._DB_PREFIX_.'orders '.
                    'WHERE id_order = '.(int)$id_order
                );
                $sql_product = 'SELECT * '.
                    'FROM '._DB_PREFIX_.'order_detail od '.
                    'INNER JOIN '._DB_PREFIX_.'product_lang pl '.
                    'ON od.product_id = pl.id_product '.
                    'WHERE od.id_order = '.(int)$id_order.
                    ' AND pl.id_lang = '.(int)$id_lang.
                    ' AND pl.id_shop = '.(int)$id_shop;

                $products = Db::getInstance()->ExecuteS(
                    $sql_product
                );

                // Carlos utrera: Vamos a injectar las imágenes aposteriori para que la consulta no falle
                // cuando no las tiene definidas
                // Código basado en el propio controlador de producto para mostrar al imagen
                foreach ($products as $ip => $product) {
                    $produc_aux = new Product($product['id_product']);
                    $images = $produc_aux->getImages((int)$id_lang);
                    if (isset($images[0])) {
                        $products[$ip]['id_image'] = $images[0]['id_image'];
                    }
                    foreach ($images as $image) {
                        if ($image['cover']) {
                            $products[$ip]['id_image'] = $image['id_image'];
                        }
                    }
                }

                $id_customer = Db::getInstance()->getValue(
                    'SELECT id_customer '.
                    'FROM '._DB_PREFIX_.'orders '.
                    'WHERE id_order = '.(int)$id_order
                );
                if (Tools::getValue('sendcomments')) {
                    $commentvalidation = Configuration::get('PS_LGCOMMENTS_VALIDATION');
                    if ($commentvalidation == 0) {
                        $commentstatus = 1;
                    } else {
                        $commentstatus = 0;
                    }
                    if (Configuration::get('PS_LGCOMMENTS_OPINION_FORM') == (1 or 2)) {
                        $shop_details = '<br>';
                        Db::getInstance()->Execute('
                            INSERT INTO '._DB_PREFIX_.'lgcomments_storecomments (
                                date,
                                id_customer,
                                id_order,
                                stars,
                                comment,
                                id_lang,
                                active,
                                position,
                                title,
                                answer
                            )
                            VALUES (
                                \''.date('Y-m-d').'\',
                                '.(int)$id_customer.',
                                '.(int)$id_order.',
                                '.pSQL(Tools::getValue('score_store')).',
                                \''.pSQL(Tools::getValue('comment_store')).'\',
                                '.(int)$id_lang.', '.(int)$commentstatus.',
                                0,
                                \''.pSQL(Tools::getValue('comment_title')).'\',
                                0
                            )
                        ');
                        $id_lgcomments_storecomments = Db::getInstance()->getValue(
                            'SELECT id_lgcomments_storecomments '.
                            'FROM '._DB_PREFIX_.'lgcomments_storecomments '.
                            'WHERE id_order = '.(int)$id_order
                        );
                        Db::getInstance()->Execute(
                            'UPDATE '._DB_PREFIX_.'lgcomments_storecomments '.
                            'SET position = '.(int)$id_lgcomments_storecomments.' '.
                            'WHERE id_order = '.(int)$id_order.''
                        );
                        $shop_details .= '---<br>';
                        $shop_details .= '<b>'.Tools::getValue('score_store').'/10</b>';
                        $shop_details .= '<b>&nbsp;'.Tools::getValue('comment_title').'</b><br>';
                        $shop_details .= '"'.Tools::getValue('comment_store').'"<br>';
                    }
                    if (Configuration::get('PS_LGCOMMENTS_OPINION_FORM') == (1 or 3)) {
                        $product_details = '<br>';
                        foreach ($products as $product) {
                            $pcode = $product['id_order_detail'].'_'.(int)$product['product_id'];
                            Db::getInstance()->Execute('
                                INSERT INTO '._DB_PREFIX_.'lgcomments_productcomments (
                                    date,
                                    id_customer,
                                    id_product,
                                    stars,
                                    comment,
                                    id_lang,
                                    active,
                                    position,
                                    title,
                                    answer
                                ) VALUES (
                                    \''.date('Y-m-d').'\',
                                    '.(int)$id_customer.',
                                    '.(int)$product['product_id'].',
                                    '.pSQL(Tools::getValue('score_'.$pcode)).',
                                    \''.pSQL(Tools::getValue('comment_'.$pcode)).'\',
                                    '.(int)$id_lang.',
                                    '.(int)$commentstatus.',
                                    0,
                                    \''.pSQL(Tools::getValue('title_'.$pcode)).'\',
                                    0
                                )
                            ');
                            $id_lgcomments_productcomments = Db::getInstance()->getValue(
                                'SELECT id_lgcomments_productcomments '.
                                'FROM '._DB_PREFIX_.'lgcomments_productcomments '.
                                'WHERE id_customer = '.(int)$id_customer.
                                ' AND id_product = '.(int)$product['product_id'].
                                ' ORDER BY id_lgcomments_productcomments DESC '
                            );
                            Db::getInstance()->Execute(
                                'UPDATE '._DB_PREFIX_.'lgcomments_productcomments '.
                                'SET position = '.(int)$id_lgcomments_productcomments.' '.
                                'WHERE id_lgcomments_productcomments = '.(int)$id_lgcomments_productcomments
                            );
                            $product_details .= '---<br>';
                            $product_details .= $product['product_name'].'<br>';
                            $product_details .= '<b>'.Tools::getValue('score_'.$pcode).'/10</b>';
                            $product_details .= '<b>&nbsp;-&nbsp;'.Tools::getValue('title_'.$pcode).'</b><br>';
                            $product_details .= '"'.Tools::getValue('comment_'.$pcode).'"<br>';
                        }
                    }
                    Db::getInstance()->Execute(
                        'UPDATE '._DB_PREFIX_.'lgcomments_orders '.
                        'SET voted = 1 '.
                        'WHERE id_order = '.(int)$id_order.
                        ' AND hash = \''.pSQL($lghash).'\''
                    );
                    $voted = 1;
                    $languageId = Db::getInstance()->getValue(
                        'SELECT id_lang '.
                        'FROM '._DB_PREFIX_.'orders '.
                        'WHERE id_order = '.(int)$id_order
                    );
                    $customerEmail = Db::getInstance()->getValue(
                        'SELECT email '.
                        'FROM '._DB_PREFIX_.'customer '.
                        'WHERE id_customer = '.(int)$id_customer
                    );
                    $langs = Db::getInstance()->ExecuteS('SELECT * FROM '._DB_PREFIX_.'lang');
                    foreach ($langs as $lang) {
                        if ($languageId == $lang['id_lang']) {
                            $subject2 = Configuration::get('PS_LGCOMMENTS_SUBJECT2'.$lang['iso_code']);
                        }
                    }
                    $firstname = Db::getInstance()->getValue(
                        'SELECT firstname '.
                        'FROM '._DB_PREFIX_.'customer '.
                        'WHERE id_customer = '.(int)$id_customer
                    );
                    $templateVars = array(
                        '{firstname}' => $firstname,
                    );
                    // Check if email template exists for current iso code. If not, use English template.
                    $module_path = _PS_MODULE_DIR_.'lgcomments/mails/'.Language::getIsoById($languageId).'/';
                    $template_path = _PS_THEME_DIR_.'modules/lgcomments/mails/'.Language::getIsoById($languageId).'/';
                    if (is_dir($module_path) or is_dir($template_path)) {
                        $langId = $languageId;
                    } else {
                        $langId = (int)Language::getIdByIso('en');
                    }
                    Mail::Send(
                        (int)$langId,
                        'thank-you',
                        $subject2,
                        $templateVars,
                        $customerEmail,
                        null,
                        null,
                        Configuration::get('PS_SHOP_NAME'),
                        null,
                        null,
                        dirname(__FILE__).'/../../mails/'
                    );
                    
                    $email_cron = Configuration::get('PS_LGCOMMENTS_EMAIL_CRON');
                    $email_alerts = Configuration::get('PS_LGCOMMENTS_EMAIL_ALERTS');
                    $customer_name  = Db::getInstance()->getRow(
                        'SELECT c.* '.
                        'FROM '._DB_PREFIX_.'lgcomments_orders lo '.
                        'INNER JOIN '._DB_PREFIX_.'customer as c '.
                        'ON lo.id_customer = c.id_customer '.
                        'WHERE lo.id_order = '.(int)$id_order
                    );
                    if ($email_cron and $email_alerts == 1) {
                        $templateVars = array(
                            '{id_order}' => $id_order,
                            '{customer_firstname}' => $customer_name['firstname'],
                            '{customer_lastname}' => $customer_name['lastname'],
                            '{product_details}' => $product_details,
                            '{shop_details}' => $shop_details,
                        );
                        // Check if email template exists for current iso code. If not, use English template.
                        $default = Configuration::get('PS_LANG_DEFAULT');
                        $module_path2 = _PS_MODULE_DIR_.'lgcomments/mails/'.Language::getIsoById($default).'/';
                        $template_path2 = _PS_THEME_DIR_.'modules/lgcomments/mails/'.Language::getIsoById($default).'/';
                        if (is_dir($module_path2) or is_dir($template_path2)) {
                            $langId2 = $default;
                        } else {
                            $langId2 = (int)Language::getIdByIso('en');
                        }
                        Mail::Send(
                            (int)$langId2,
                            'new-review',
                            Configuration::get('PS_LGCOMMENTS_SUBJECT_NEWREVIEWS'),
                            $templateVars,
                            $email_cron,
                            null,
                            null,
                            Configuration::get('PS_SHOP_NAME'),
                            null,
                            null,
                            dirname(__FILE__).'/../../mails/'
                        );
                    }
                }
                $form_action = $_SERVER['REQUEST_URI'];
                $this->context->smarty->assign(array(
                    'verify' => $verify,
                    'starstyle' => Configuration::get('PS_LGCOMMENTS_STARDESIGN1'),
                    'starcolor' => Configuration::get('PS_LGCOMMENTS_STARDESIGN2'),
                    'starsize' => Configuration::get('PS_LGCOMMENTS_STARSIZE'),
                    'ratingscale' => Configuration::get('PS_LGCOMMENTS_SCALE'),
                    'products' => $products,
                    'form_action' => $form_action,
                    'voted' => $voted,
                    'opinionform' => Configuration::get('PS_LGCOMMENTS_OPINION_FORM'),
                ));
                $this->setTemplate('review_form.tpl');
            } else {
                $verify = 2;
                $this->context->smarty->assign(array(
                    'verify' => $verify,
                ));
                $this->setTemplate('review_form.tpl');
            }
        } else {
            $verify = 0;
            $this->context->smarty->assign(array(
                'verify' => $verify,
            ));
            $this->setTemplate('review_form.tpl');
        }
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addJquery();
    }
}
