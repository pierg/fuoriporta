<?php
/*
* 2010-2012 LaBulle All right reserved
*/

require_once(_PS_MODULE_DIR_ . "myownreservations/myownreservations.php");
require_once(MYOWNRES_PATH . "controllers/admin.php");
require_once(MYOWNRES_PATH . "controllers/planning.php");
require_once(MYOWNRES_PATH . "controllers/ajax_admin.php");


class adminReservationConfig extends AdminController
{
	var $module = "myownreservations";
	var $name = "myownreservations";
	var $multishop_context_group;
	var $multishop_context;
	var $moduleObj;
	var $ruleId;
	var $insertedId=0;

    public function __construct()
    {
        global $cookie, $_LANGADM;

        $this->moduleObj = Module::getInstanceByName('myownreservations');
        $this->moduleObj->checkPro();
        //if (!is_file(_PS_MODULE_DIR_ ."myownreservations/pro/products.php")) 
        $this->multishop_context = true;
        myOwnReservationsController::_setInfos ($this->moduleObj);
        myOwnReservationsController::_construire($this->moduleObj);
		$this->display = 'view';
		$this->class_name = 'adminReservationConfig';
		$this->bootstrap=true;
        parent::__construct();
    }
    
    public function displayErrors() {
	    foreach($this->_errors as $err) echo $err;
    }
    
        public function setMedia()
	{
		parent::setMedia();

		$this->addJqueryPlugin(array('fancybox', 'typewatch', 'growl'));
		$this->addJS(array(
                _PS_JS_DIR_.'tiny_mce/tiny_mce.js',
                _PS_JS_DIR_.'admin/tinymce.inc.js',
            ));
        $this->addJS(_MODULE_DIR_.'myownreservations/views/js/back.js');
	}
    
    public function initPageHeaderToolbar()
	{
		global $cookie;
		$tab = Tools::getValue('myOwnResTab');
		$ctrl = strtolower(Tools::getValue('controller'));
		$field='';
		if ($tab=='timeslots')
			$fieldl = 'TimeSlot';
		if ($tab=='pricesrules') 
			$field = 'Pricesrule';
		if ($tab=='availability') 
			$field = 'Availability';
		if ($ctrl=='adminreservationrules') {
			$tab='products';
			$field = 'Product';
		}
		
		$this->page_header_toolbar_title = $this->moduleObj->l('Reservations Preferences', 'adminReservationConfig');

		$this->page_header_toolbar_btn = myOwnReservationsController::getButtonsList( $this->moduleObj, $this->ruleId);

		if (_PS_VERSION_ >= "1.6.0.0") parent::initPageHeaderToolbar();
		$this->context->smarty->assign('help_link', null);
	}
	


    public function renderView()
	{
		global $cookie;
		$content='';
		$obj = $this->moduleObj;
		
		$header = MyOwnReservationsUtils::displayIncludes();
		$header .= myOwnReservationsController::getModuleHeader($obj, $cookie);
		if ($cookie->id_employee==1) $header .= myOwnReservationsController::configCheckSetup($obj);
		$content .= myOwnReservationsSettingsController::settings($cookie,$obj);

		foreach ($obj->_extensions->getExtByType(extension_type::OBJECT) as $ext)
			if ($myOwnResTab==$ext->name) 
				$content .= $ext->getContent($obj);
			
		return $header.$content;
  	}
  
}
?>