<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class rssFeed extends myOwnExtension {
	
	public function __construct() {
		$this->id=2;
	}
	
	public function setInfos($obj) {
		$this->title=$obj->l('RSS Feed', 'rssFeed');
		$this->description=$obj->l('Publish all incoming reservations of customer on a feed', 'rssFeed');
	}
	
	public function displayOrderConfGlobal($obj, $order) {
		$link = new Link();
		return '<div class="resa_global">	
					<a href="'.MyOwnReservationsUtils::getModuleLink($link, $this->name).'&ref='.$order->reference.'" target="_blank">
					<b>'.$obj->l('RSS', 'rssFeed').'</b>
					<img width="24" height="24" class="imgm" alt="" src="'.__PS_BASE_URI__.'modules/myownreservations/extensions/rssFeed.png" style="border:none">
					'.$obj->l('Subscribe to your incoming reservations feed', 'rssFeed').'
					</a>
				</div>';
	}
	
	public function execConfigNotif($obj, $mainProduct) {
		return true;
	}
	
	public function displayConfigNotif($obj, $idProductFamilly) {
		return '';
	}
	
	public function ajax($obj, $cookie) {
		global $cookie;
		$ref = Tools::getValue("ref");
		$id_customer=$cookie->id_customer;
		$reservations=array();
		$productsIds=array();
		
		$order_collection=Order::getByReference($ref);
		if (count($order_collection)>0) {
			$order = $order_collection->getFirst();
			$id_customer=$order->id_customer;
		}
		if (!$id_customer) return $obj->l('Please indicate any order reference as ref= or login', 'rssFeed');
		$customer=new Customer($id_customer);
		return self::getCustomerFeed($obj, $cookie, $customer);
	}
	
	public function getCustomerFeed($obj, $cookie, $customer) {
		$orders = Order::getCustomerOrders($customer->id);
		$refValid = false;
		foreach ($orders as $order) {
			$resas=myOwnResas::getReservations($order["id_order"]);
			foreach ($resas as $resa) {
				if (!$resa->done()) {
					$reservations[$resa->getStartDateTime()]=$resa;
					$productsIds[]=$resa->id_product;
				}
			}
			if ($order["reference"]==$ref) $refValid=true;
		}
		$productsImages=MyOwnReservationsUtils::getProductsImages($productsIds, $cookie->id_lang);

		header("Content-Type: application/rss+xml; charset=utf-8");
		echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
		/*echo '<?xml-stylesheet type="text/css" href="'.__PS_BASE_URI__.'modules/myownreservations/css/rss.css" ?>'."\n";*/
		echo '
	<rss version="2.0" xmlns:media="search.yahoo.com/mrss/">
		<channel>
			<title><![CDATA['.Configuration::get('PS_SHOP_NAME').']]></title>
			<link>'._PS_BASE_URL_.__PS_BASE_URI__.'</link>
			<description>'.$obj->l('Reservations made by', 'iCalendar').' '.ucfirst($customer->firstname).' '.strtoupper($customer->lastname).'</description>
			<language>'.Context::getContext()->language->iso_code.'</language>
			<image>
				<title><![CDATA['.Configuration::get('PS_SHOP_NAME').']]></title>
				<url>'._PS_BASE_URL_.__PS_BASE_URI__.'img/logo.jpg'.'</url>
				<link>'._PS_BASE_URL_.__PS_BASE_URI__.'index.php</link>
			</image>
			';
			
	$link=new Link();
	ksort ( $reservations);
		foreach ($reservations AS $reservation)
		{
			$product= $productsImages[$reservation->id_product];
			$image = Image::getImages((int)($cookie->id_lang), $product->id);
			echo "\t\t<item>\n";
			echo "\t\t\t<title><![CDATA[".$product->name." ".$reservation->toString($obj, $cookie->id_lang, true)."]]></title>\n";
			echo "\t\t\t";
			$cdata = true;
			echo '<description><![CDATA[';
			if (is_array($image) AND sizeof($image))
			{
				$imageObj = new Image($image[0]['id_image']);
				echo '<img src="'._PS_BASE_URL_._THEME_PROD_DIR_.$imageObj->getExistingImgPath().'-small_default.jpg">';
			}
			echo $product->description_short."]]></description>\n";
			echo "<pubDate>".date("Y-m-d H:i:s",$reservation->getEndDateTime())."</pubDate>";
			echo "\t\t\t<link><![CDATA[".str_replace('&amp;', '&', htmlspecialchars($link->getproductLink($product->id, $product->link_rewrite, Category::getLinkRewrite((int)($product->id_category_default), $cookie->id_lang)))).$affiliate."]]></link>\n";
			echo "\t\t</item>\n";
		}
	?>
		</channel>
	</rss>
	<?php
	}
	
	public function execNotification($cookie,$obj,$value) {
		return 'test';
	}
}

?>