<?php
/**
* 2010-2014 LaBulle All right reserved
*/

class sms_type {
    const DIRECT = 'l';
    const TOP = 'n';
    const LOWCOST = 'll';
}

class sms_state {
    const WAIT = 0;
    const SENT = 1;
    const ERROR = 2;
}

class smsAlert extends myOwnExtension {
	const CONFIG_LOGIN = 1;
	const CONFIG_PASSWORD = 2;
	const CONFIG_FROM = 3;
	const CONFIG_MSG_DIRECT = 4;
	const CONFIG_MSG_SCHEDULE = 5;
	const CONFIG_CREDIT_LEFT = 6;
	const CONFIG_CREDIT_DATE = 7;
	const CONFIG_CREDIT_LANG = 8;
	const CONFIG_CREDIT_MAIL = 9;
	
	const DETAIL_ENABLED = 1;
	const DETAIL_SMSID = 2;
	const DETAIL_NUMBER = 3;
	const DETAIL_DELAY = 4;
	const DETAIL_SMSDATE = 5;
	
	const OPTION_PRICE = 1;
	
	public $icon='mobile';
	private $logged = false;
	private $smsToSend = 0;
	private $credits = 0;
	
	
	public static $img="iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
AABHGAAARxgBc4LeuQAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTNAay06AAAAYO
SURBVFiF7ZZpbFxXFcd/9743M15mJjNuugQ78ZTgthJKG0EWUifQBJRGjVO1EKRWLahtWNQmKhJV
pSAhlQhR0YBkiUBBhX5KkArqh5aSGCo5OHRJCC2hTl1DE8d2o3pcL7E9nu1t9/DhjbesNq36Bf7S
kZ7uO///Oe+e+8498L8OtVBCd3f3NfX19Rui0egWrfWyyrJ2XfewUur12traIx9xjpDP5x/3fT9n
jIiIiDGXtqn3nud3lsvl5itpX3YHXNftjEQiK0QkdFYKAUzuGFI4hZT7Qgkx6OTNqPgqVFVDSK5w
AIrF4u54PP7UvBOYmJj4XiKReFJEQGuk2IvfdR9m9CjYoCxA6QuJxiB+qGplvo/d9EMwAAYROHy4
Pbl58+bJyyZQdpx3bdtuQmkk/zbuqytQNqCty23WRSFugE5tILrur0hg0FoxMDBwZ0NDw0sXTaBU
KvVGIpEMloX76kYk30EY/UNAgRR9Yhs6UKn1KKU43dOz8cYbbuiYk0A2m/3B4sWLn8C2cf6QhEgZ
5FKq/wUCD/vmZ7DqH0RpjW1Zak4Cnu+LCHhH1iLFk7OYgjgeWKDsaLjiuKhYBHE8VCwKxkN8QUWj
FY5Byj4qAljRGSnfJbLlLMq+FsdxTicSiSYNcObMmT0mCBBvBDP+DyRQFRMiTT8ivl2I3+UifoIg
7xLfLtiZ3cS3C9Y191O9qZv4VwTUJxA3oGr1QeJfFWq2jmGK7oweMZwXliJWhFgs9ikADVB31VXf
FhHcV+5CJIb4CvEVpuARvelxxn6uKB17mJotnZVTDeIXKZ/YTdXaZykd/SbB6HGqv3QcUSms6zYz
9iuF//4hYp95elpPAgVaIc4wRoT29vbPa4CqWOxasWPI4DEI1LRJEAZL7/LR0TS5A/VQWXNOtuL3
Px+Wd+AV/IE/obSFmRgNOTvG8ftfpPT6I3M0UdUEZ9sQE7CssfFODWBEMAISMGv7FcquYewXitJr
jxL77E9IfcNgylOHxiB+uB1TPABlW4z9UlE+2Ur1F35H/I6jiD9X1wQBRgTf8ywN4JQdV1wHqWma
4yjlIumdQumNpxn/TRSUmk5ATGhhOaj8MQpqlpP+lkPp6B7yh7aiU5/GeP6Mpuujrl6FGDjd0/Om
DZDNZg8uXdpwt173a4L2O6aOBiIxvP426r4T/o/5tq1QaQvGBQpjFT8IxvsQr0wwdIog3096Z8gZ
3389EAuTBCTvIHUr0E6JbS0tLyqA2zbeljn4x0O9gRXDf6YaqmOzeoDBFAooDbomGa4Ucujauc9i
AvAKEE2A8TDlMsoCXZVk5i4R9Np9qMyX8QNj6tLpsAQdf+noy2az74hTwHrEgXOTYFTFLHR1EhVL
VmoNqurCZ8QCOxlyiKKrkqhI5Z1RIBpqVsAn70Vrm107d22oFG0adi436RkTQO0igp8qSKZnCv1h
YDxUwxb07b8Hr0BXV9cLzc233n1+AgBLJnKTA0Hgo2pSyJ+/junaD7XpOdfrAiLDZA7rgb8j8ZtQ
GPr7+t5cufKWVVMe59+p2UXJRGoyNzkixQmk+Wfo7wpq+f3I+DgS6JmmMqu5zJhGfINMjoNno25/
DvWYYKqXE7EUbW2H9q5cecvq2QEvNZBYra2tjz3w4ENPGWMQY1DJOszeBESjc9mBQsWqILEU6jfB
jS2oJesQpwCBg2XZTExMjGcal90KdJ8f6EozYc2+ffsevueee/eitea5jUjufUBAadSi5XDfEfAd
MD5gUCi0Fc4O7/X3v7Ft27ZHz559729MN/GFJQDAv989dTydSq3mtSeRdw6E52FyBP2EIIVRfN/D
cdzcuXPnev554kTbjh0PPV/52vKVtOc1bQxmsy+nU6nVNDZD5wHIj6D2BEh+mGKxNJbJNNYBFtM3
xfxxkcHuQpzp7T0OINd9DimXUV97GcmNoLVNJtO4pOK24OALwdUfDA3LYMGXbOsmGRwakeGRUWle
v37VlakfEYaGRySbHZTsyJhkPxiS/Qd+++OPLTjAW52dbw8NDcvQ0LC81XnyXx9r8ArslpaWrWvW
rPki8zw7/8d88B/dhTsfctlocAAAAABJRU5ErkJggg==";

	public static $delays;
	public static $stateStr;
	public static $messages=array();
	
	protected $login;
	protected $password;
	
	public function __construct() {
		$this->id=4;
		$this->enabled=false;
		$this->login=$this->getConfig(0, self::CONFIG_LOGIN);
		$this->password=$this->getConfig(0, self::CONFIG_PASSWORD);
		if ($this->login!='' && $this->password!='') {
			$this->enabled=true;
		} else {
			$this->needconfig=true;
		}
	}

	public function setInfos($obj) {
		$this->title=$obj->l('SMS Alert', 'smsAlert');
		$this->description=$obj->l('Send a programmed SMS before reservation begin', 'smsAlert');
		$this->feeTitle = $obj->l('SMS Fee', 'attendees'); 
		self::$delays=array (	
				5 => '5 '.$obj->l('min', 'smsAlert'),
				10 => '10 '.$obj->l('min', 'smsAlert'),
				15 => '15 '.$obj->l('min', 'smsAlert'),
				30 => '30 '.$obj->l('min', 'smsAlert'),
				60 => '1 '.$obj->l('hour', 'smsAlert'),
				120 => '2 '.$obj->l('hours', 'smsAlert'),
				180 => '3 '.$obj->l('hours', 'smsAlert'),
				360 => '6 '.$obj->l('hours', 'smsAlert'),
				720 => '12 '.$obj->l('hours', 'smsAlert'),
				1440 => '1 '.$obj->l('day', 'smsAlert'),
				2880 => '2 '.$obj->l('days', 'smsAlert')
			);
		self::$stateStr=array (	
				sms_state::WAIT => $obj->l('Waiting', 'smsAlert'),
				sms_state::SENT => $obj->l('Sent', 'smsAlert'),
				sms_state::ERROR => $obj->l('Error', 'smsAlert')
			);
		$fr = Language::getIdByIso('fr');
		$en = Language::getIdByIso('en');
		$es = Language::getIdByIso('es');
		self::$messages=array (	
			self::CONFIG_MSG_DIRECT => array($fr => 'Votre réservation pour un %product% est planifiée du %start% au %end%',
											 $en => 'Your reservation for %product% is scheduled from %start% to %end%',
											 $es => 'Su reserva para el %product% está prevista de %start% a %end%'),
			self::CONFIG_MSG_SCHEDULE =>  array($fr => 'Votre réservation pour un %product% commence dans %delay%',
											 $en => 'Your reservation for %product% start in %delay%',
											 $es => 'Su reserva para el %product% comienza en %delay%')
		);
			
	}
	
	//-----------------------------------------------------------------------------------------------------------------
	// BACK OFFICE CONFIG
	//-----------------------------------------------------------------------------------------------------------------
	public function execConfigGlobal($cookie,$obj) {
		$output='';
// 		if (Tools::getIsset('smsAlertFrom'))$this->setConfig(0, self::CONFIG_FROM, 0, $_POST['smsAlertFrom']);
		if (Tools::getIsset('smsAlertLogin')) {
// 			$this->setConfig(0, self::CONFIG_LOGIN, 0, '');
// 			$this->setConfig(0, self::CONFIG_PASSWORD, 0, '');
// 			if (Tools::getIsset('smsAlertLogin')) 
			$this->setConfig(0, self::CONFIG_LOGIN, 0, $_POST['smsAlertLogin']);
 			if (Tools::getValue('smsAlertPassword', '')!='')
				$this->setConfig(0, self::CONFIG_PASSWORD, 0, md5($_POST['smsAlertPassword']));
			else $this->setConfig(0, self::CONFIG_PASSWORD, 0, '');
			
		}
		if (Tools::getIsset('smslogin')) {
// 			$this->setConfig(0, self::CONFIG_LOGIN, 0, '');
// 			$this->setConfig(0, self::CONFIG_PASSWORD, 0, '');
// 			if (Tools::getIsset('smsAlertLogin')) 
// 			$this->setConfig(0, self::CONFIG_LOGIN, 0, $_POST['smsAlertLogin']);
//  			if (Tools::getValue('smsAlertPassword', '')!='')
			$this->setConfig(0, self::CONFIG_LOGIN, 0, '');
			$this->setConfig(0, self::CONFIG_PASSWORD, 0, '');
			
		} 
		$this->login=$this->getConfig(0, self::CONFIG_LOGIN);
		$this->password=$this->getConfig(0, self::CONFIG_PASSWORD);
		
		$sheduledsSMS = $this->getDetail(0, self::DETAIL_SMSDATE);
		$smsSent=0;
		if (is_array($sheduledsSMS))
		foreach ($sheduledsSMS as $sheduledSMS) {
			if ($sheduledSMS['value']==0 or strtotime($sheduledSMS['value'])<time()) $smsSent++;
			else $smsToSend++;
		}

// 		if (Tools::getValue('smsAlertPassword', '')!='') $output.='smsAlertPassword';
		$this->logged=false;
		if ($this->login!='' && $this->password!='') {
			$sms = new laBulleSMS($this->login, $this->password);
			$result = $sms->getCredit('n');
			if (stripos($result, "OK")!==false) {
				$this->logged=true;
// 				$status = $obj->l('Ok', 'smsAlert');
				$this->credits = intval(str_ireplace("OK ", "", $result));
				$this->setConfig(0, self::CONFIG_CREDIT_LEFT, 0, $this->credits);
				$this->setConfig(0, self::CONFIG_CREDIT_DATE, 0, date('Y-m-d H:i:s'));
				$this->setConfig(0, self::CONFIG_CREDIT_LANG, 0, $cookie->id_lang);
				if ( $this->credits<$this->smsToSend) $output.= myOwnUtils::displayError($obj->l('Some SMS scheduled won\'t be sent because there is not enough credit', 'smsAlert'));
			}
			else $output.= myOwnUtils::displayError($obj->l('Wrong SMS account login/password', 'smsAlert').' : '.$result);
		}
		return $output;
	}
	public function displayConfigGlobal($cookie,$obj) {
		$output='';
		

		$from=$this->getConfig(0, self::CONFIG_FROM);
// 		$sheduledsSMS = $this->getDetail(0, self::DETAIL_SMSDATE);
		$smsSent=0;

		$this->login=$this->getConfig(0, self::CONFIG_LOGIN);
		$this->password=$this->getConfig(0, self::CONFIG_PASSWORD);
		
		$this->logged=false;
		if ($this->login!='' && $this->password!='') {
			$sms = new laBulleSMS($this->login, $this->password);
			$result = $sms->getCredit('n');
			if (stripos($result, "OK")!==false) {
				$this->logged=true;
// 				$status = $obj->l('Ok', 'smsAlert');
				$this->credits = intval(str_ireplace("OK ", "", $result));
				$this->setConfig(0, self::CONFIG_CREDIT_LEFT, 0, $this->credits);
				$this->setConfig(0, self::CONFIG_CREDIT_DATE, 0, date('Y-m-d H:i:s'));
				$this->setConfig(0, self::CONFIG_CREDIT_LANG, 0, $cookie->id_lang);
// 				if ( $this->credits<$) $output.= myOwnUtils::displayError($obj->l('Some SMS scheduled won\'t be sent because there is not enough credit', 'smsAlert'));
			}
// 			else $output.= myOwnUtils::displayError($obj->l('Wrong SMS account login/password', 'smsAlert').' : '.$result);
		}
		if ($this->logged) $output .= $smsSent.' '.$obj->l('SMS Sent', 'smsAlert').', '.$this->smsToSend.' '.$obj->l('SMS Scheduled', 'smsAlert').'<br/>';
		$output .= '
		<table cellspacing="0" cellpadding="0" style="float:left;width:40%; margin-bottom:0px;margin-top:5px;" align="left" class="table" id="">
			<thead>
				<tr class="nodrag nodrop">
					<th colspan="2">'.$obj->l('SMS Account', 'smsAlert').'</th>
				</tr>			
			</thead>
			<tbody>';
		if ($this->logged && !Tools::getIsset('smslogin'))
			$output .= '
				<tr>
					<td colspan="2" style="border-bottom:none;height:30px;text-align:center">' . $obj->l('Logged as', 'smsAlert') . ' '.$this->login.'</td>
				</tr>
				<tr>
					<td style="color:'.($this->credits ? 'green' : 'red').'">'.$this->credits.' '.$obj->l('SMS remaining', 'smsAlert').'</td>
					<td style="text-align:right"><input class="button" size="20" onclick="document.location.href=document.location.href+\'&smslogin=1\';" type="button" name="change" value="' . $obj->l('Change', 'smsAlert') . '" /></td>
				</tr>';
		else
			$output .= '
				<tr>
					<td style="border-bottom:none;">' . $obj->l('Login', 'smsAlert') . '</td>
					<td style="border-bottom:none;"><input class="upsInput" size="20" type="text" name="smsAlertLogin" value="'.$this->login.'" autocomplete="off" /></td>
				</tr>
				<tr>
					<td style="border-bottom:none;">' . $obj->l('Password', 'smsAlert') . '</td>
					<td style="border-bottom:none;"><input class="upsInput" size="20" type="password" name="smsAlertPassword" value="" autocomplete="off" /></td>
				</tr>
				';
				/*<tr>
					<td colspan="2" style="text-align:center"><input class="button" size="20" type="submit" name="smsAlert" value="' . $obj->l('Login', 'smsAlert') . '" /></td>
				</tr>*/
		$output .= '	
			</tbody>
		</table>
		';
		$output .= '
		<iframe src="http://www.labulle.net/sms.php?lang='.Tools::strtolower(Language::getIsoById($cookie->id_lang)).'" ALLOWTRANSPARENCY="true" frameborder="0" marginwidth="0" marginheight="0" width="58%" height="110" style="float:right;margin-left:10px">Shop</iframe>
		<div style="clear:both"></div>';
		return $output;
	}
	
	public function execConfigNotif($obj, $mainProduct) {
		if (Tools::getIsset('smsAlert_price')) $this->setConfig($mainProduct->sqlId, self::OPTION_PRICE, 0, $_POST['smsAlert_price']);
	}
	
	public function displayConfigNotif($obj, $idProductFamilly) {
		global $cookie;
		$output='';
		
		$price=$this->getConfig($idProductFamilly, self::OPTION_PRICE, 0);

		$output.='
		<ol style="padding:0px;margin-bottom:0px;">
			<li class="myowndlist">
				<label for="name" class="myowndeliveriesSettingsLabel" style="width:130px">' . $obj->l('Cost', 'smsAlert') . ' :&nbsp;</label>
				<div style="display:inline-block">'.myOwnUtils::getInputField('smsAlert_price', number_format ((double)$price, 2), '', 'width:25%', 'upsInput').'
			</li>
		</ol>';
		return $output;
	}
	
	//-----------------------------------------------------------------------------------------------------------------
	// DISPLAY ON FRONT
	//-----------------------------------------------------------------------------------------------------------------
	
	public function displayShoppingCart($obj, $cart=null) {
		global $cookie;
		if ($cart==null) return '';
		$customer = new Customer($cart->id_customer);
		$myOwnCart = $cart->getMyOwnReservationCart();
		$resaNotifs = $this->getNotificationResa($myOwnCart);
	
		if (count($resaNotifs)>0) {
			$earlierResaStart = $this->getEarlierResaStart($myOwnCart);
			$notification_checked = $this->getDetail(-$cart->id, self::DETAIL_ENABLED);
			$notification_number = $this->getDetail(-$cart->id, self::DETAIL_NUMBER);
			$notification_delay = $this->getDetail(-$cart->id, self::DETAIL_DELAY);

			if (Validate::isLoadedObject($customer)) {
	  			$addresses = $customer->getAddresses($cookie->id_lang);
	  			$tels=array();
	  			foreach($addresses as $address) {
	  				$iso = Tools::strtolower(Country::getIsoById($address['id_country']));
			  		if ($address['phone_mobile']!='') {
			  				$number=$address['phone_mobile'];
			  				if ($iso=='fr' && Tools::strlen($number)==10) $number="0033".Tools::substr($number, -9);
			  				$tels[$number] = $address['phone_mobile'].' ('.$obj->l('mobile', 'smsAlert').' '.$address['alias'].')';
		  			}
	  			}
	  			$input_num='<select name="notification_number" id="notification_number" style="padding:0px;width:120px">';
	  			foreach ($tels as $num => $label) $input_num.='<option value="'.$num.'" '.($num==$notification_number ? 'selected' : '').'>'.$label.'</option>';
				$input_num.='</select>';
			}
			$input_delay='<select style="padding:0px" name="notification_delay" id="notification_delay">';
	  		foreach (self::$delays as $delay => $label) if (strtotime('-'.$delay.'min', $earlierResaStart)>time()) $input_delay.='<option value="'.$delay.'" '.($delay==$notification_delay  ? 'selected' : '').'>'.$label.'</option>';
			$input_delay.='</select>';
			
			$script="<script type=\"text/javascript\">
			function submitNotification() {
				$.ajax({
					type: 'POST',
					headers: { \"cache-control\": \"no-cache\" },
					url: baseUri + '?rand=' + new Date().getTime(),
					async: true,
					dataType: 'json',
					cache: false,
					data: 'controller=cart&ajax=true&summary=1&allowSeperatedPackage=true&notification_checked='
						+($('#notification_checked').prop('checked') ? '1' : '0')
						+'&notification_number='+$('#notification_number').val()
						+'&notification_delay='+$('#notification_delay').val()
						+'&token='+static_token
						+'&allow_refresh=1',
					success: function(jsonData)
					{
							updateCartSummary(jsonData.summary);
							updateHookShoppingCartExtra(jsonData.HOOK_SHOPPING_CART_EXTRA);
					}
				});
			}
			$(document).ready(function() {
				$('#notification_checked').on('click', function() {
					submitNotification();
				});
				$('#notification_delay').on('change', function() {
					if ($('#notification_checked').prop('checked')) submitNotification();
				});
				$('#notification_number').on('change', function() {
					if ($('#notification_checked').prop('checked')) submitNotification();
				});
			});
			</script>";
			$myOwnCart = $cart->getMyOwnReservationCart();
			$price=$this->getNotificationResaCost($myOwnCart);

			return $script.'<div class="box" style="margin-bottom:10px;">
				<img src="data:image/png;base64,'.self::$img.'" alt="SMS" style="margin:-10px;padding-right:15px;"/>
				<input type="checkbox" name="notification_checked" id="notification_checked" value="1" '.($notification_checked ? 'checked' : '').' style="display:inline-block"> '.$obj->l('Send me a SMS notification to', 'smsAlert').'&nbsp;&nbsp;'.$input_num.' '.$input_delay.' '.$obj->l('before reservation begin', 'smsAlert').' <div style="float:right"><span class="price"><b>'.($price==0 ? $obj->l('Free', 'smsAlert') : Tools::displayPrice($price)).'</b></span></div>
			</div>';
		}
	}

	
	public function displayOrderConfResa($obj, $resa) {	
		$status = $this->getReservationSMSStatus($obj, $resa);
	  			
		return '<div class="resa_link" style="width:140px">	
					<b>'.$obj->l('SMS Notification', 'smsAlert').'</b><br/>
					<img width="24" height="24" class="imgm" alt="" src="'.__PS_BASE_URI__.'modules/myownreservations/extensions/smsAlert.png" style="border:none"><br/>
					'.$status.' 
					</a>
				</div>';
	}
	
	public function getReservationSMSStatus($obj, $resa) {
		$order = new Order($resa->id_order);
		if (Validate::isLoadedObject($order)) {
			$notification_checked = $this->getDetail(-$order->id_cart, self::DETAIL_ENABLED);
			$notification_number = $this->getDetail(-$order->id_cart, self::DETAIL_NUMBER);
			if (Tools::substr($notification_number, 0,2)=='00') $notification_number='+'.Tools::substr($notification_number, 2);
			$notification_delay = $this->getDetail(-$order->id_cart, self::DETAIL_DELAY);
  		
	  		if ($notification_checked) {
	  			$sms_number = $this->getDetail($resa->sqlId, self::DETAIL_SMSID);
				if ($sms_number=='') $status = $obj->l('Not scheduled (waiting validation)', 'smsAlert').$sms_number;
				else {
					$status = $obj->l('Alert', 'smsAlert').' '.(int)$sms_number.' '.$obj->l('scheduled', 'smsAlert').' '.self::$delays[$notification_delay].' '.$obj->l('before', 'smsAlert');
					$sms = new laBulleSMS($this->login, $this->password);
					$res = $sms->sendStatus($sms_number, "queue", '1');
					$status=self::$stateStr[sms_state::ERROR];
					//syslog(LOG_WARNING, $sms_number.$res);
					if (Tools::strtolower(Tools::substr($res, 0, 2))!="KO") {
						$resTabt=explode("\n", $res);
						$resTab=explode(",", $resTabt[1]);
						if ($resTab[3]==100) {
							$status=self::$stateStr[sms_state::WAIT].' '.$resTab[1];
						} else if ($resTab[3]==200) $status=self::$stateStr[sms_state::SENT];
					}
				}
				
			} else $status = $obj->l('Not selected', 'smsAlert');
		}
		return  $status;
	}
	
	public function displayReservationDetails($obj, $resa, $line=-1) {
		if ($line>-1) return '';
		$status = $this->getReservationSMSStatus($obj, $resa);
		return  '
				<tr class="invoice_line" id="invoice_2">
					<td align="left">'.$obj->l('SMS Notification', 'smsAlert').'</td>
					<td align="right">'.$status.'</td>
				</tr>';
	}
	
	public function execShoppingCart($id_cart) {
		if (Tools::getIsset('notification_checked')) {
			$this->setDetail(-$id_cart, self::DETAIL_ENABLED, 0, (int)$_POST['notification_checked']);
			$this->setDetail(-$id_cart, self::DETAIL_NUMBER, 0, $_POST['notification_number']);
			$this->setDetail(-$id_cart, self::DETAIL_DELAY, 0, $_POST['notification_delay']);
		}
		//$this->checkSMSCredit($obj);
	}
	
	//-----------------------------------------------------------------------------------------------------------------
	//  EXECUTION
	//-----------------------------------------------------------------------------------------------------------------
	
	private function getNotificationResa($myOwnCart) {
		$resaNotifs = array();
		$tdelays = array_keys(self::$delays);
		$minDelay=array_shift($tdelays);
		foreach ($myOwnCart->list as $cartResa)
			if (in_array($this->id, $cartResa->_mainProduct->ids_notifs))
				if (strtotime('-'.$minDelay.'min', $cartResa->getStartDateTime())>time())
					$resaNotifs[$cartResa->sqlId]=$cartResa;

		return $resaNotifs;
	}
	
	private function getEarlierResaStart($myOwnCart) {
		$ealierStart = 0;
		foreach ($this->getNotificationResa($myOwnCart) as $cartResa)
			if ($ealierStart==0 or $ealierStart>$cartResa->getStartDateTime())
				$ealierStart=$cartResa->getStartDateTime();
		return $ealierStart;
	}
	
	private function getNotificationResaCost($myOwnCart) {
		$resaNotifs = $this->getNotificationResa($myOwnCart);
		$cost=0;
		foreach ($resaNotifs as $resaNotif)
			$cost+=$this->getConfig($resaNotif->_mainProduct->sqlId, self::OPTION_PRICE);

		return $cost;
	}
	
	public function getGlobalFee($cartId, $myOwnCart) {
		$notification_checked = $this->getDetail(-$cartId, self::DETAIL_ENABLED);
		if ($notification_checked) return $this->getNotificationResaCost($myOwnCart);
		else return 0;
	}
	
	
	public function postUpdateOrderStatus($obj, $orderId, $newOrderStatus, $reservations) {
		$order = new Order($orderId);
		if (Validate::isLoadedObject($order)) {
			$notification_checked = $this->getDetail(-$order->id_cart, self::DETAIL_ENABLED);
  			
	  		if ($notification_checked) {
				$notification_number = $this->getDetail(-$order->id_cart, self::DETAIL_NUMBER);
				if (Tools::substr($notification_number, 0,2)=='00') $notification_number='+'.Tools::substr($notification_number, 2);
				$notification_delay = $this->getDetail(-$order->id_cart, self::DETAIL_DELAY);
				foreach ($reservations as $reservation) {
					if ($reservation->validated) {
						$notif = (int)$this->getDetail($reservation->sqlId, self::DETAIL_SMSID);

						if (!$notif) {
							$credit = $this->checkSMSCredit($obj);
						//syslog(LOG_WARNING, 'smsAlert(postUpdateOrderStatus):credit='.$credit);
							if ($credit) {
								$start = $reservation->getStartDateTime();
								$schedule = strtotime('-'.$notification_delay.'min', $start);
								$msg=$this->getMsg($obj, $reservation);
							//syslog(LOG_WARNING, 'smsAlert(postUpdateOrderStatus):msg='.$msg);
							//syslog(LOG_WARNING, 'smsAlert(postUpdateOrderStatus):getStartDateTime='.date('Y-m-d H:i:s',$start));
							//syslog(LOG_WARNING, 'smsAlert(postUpdateOrderStatus):schedule='.date('Y-m-d H:i:s',$schedule));
							//syslog(LOG_WARNING, 'smsAlert(postUpdateOrderStatus):notification_number='.$notification_number);
								if ($schedule>time()) $scheduleStr = date('Y-m-d H:i:s', $schedule);
								else $scheduleStr='';
								$sms = new laBulleSMS($this->login, $this->password);
								$response = $sms->sendSms($notification_number, $msg, 'TEXT', '', '1', $scheduleStr);
							//syslog(LOG_WARNING, 'smsAlert(postUpdateOrderStatus):reponse='.$response);
								$result = Tools::strtolower(Tools::substr($response, 0, 2))=='ok';
								if ($result) {
									$smsId = str_ireplace("OK ", "", $response);
									if ($scheduleStr!='') $this->setDetail($reservation->sqlId, self::DETAIL_SMSDATE, 0, $scheduleStr);
									$this->setDetail($reservation->sqlId, self::DETAIL_SMSID, 0, $smsId);
								} else Logger::addLog("myOwnReservations : Erreur de programmation de notification SMS (".$response.")", 2);
							}

						}
					} //else syslog(LOG_WARNING, 'reservation!validated');
				}
			}
		}
	}
		
	public function execNotification($cookie,$obj,$value) {
		return 'test';
	}
	
	private function checkSMSCredit($obj) {
		$date=$this->getConfig(0, self::CONFIG_CREDIT_DATE);

		$sheduledsSMS = $this->getDetail(0, self::DETAIL_SMSDATE);
		$smsSent=0;$this->smsToSend=0;
		foreach ($sheduledsSMS as $sheduledSMS) {
			if (strtotime($sheduledSMS['value'])>strtotime($date) && strtotime($sheduledSMS['value'])<time()) $smsSent++;
			if (strtotime($sheduledSMS['value'])>time()) $this->smsToSend++;
		}
		$credit=(int)$this->getConfig(0, self::CONFIG_CREDIT_LEFT)-$smsSent;
		$message='';
		$leftcredit = $credit-$this->smsToSend;
		//if ($credit<0) $message = $obj->l('Service failure : SMS Notification for reservation', 'smsAlert').' '.$reservation->sqlId.' '.$obj->l('has not been sent because on insufficient credit', 'smsAlert');
		$lastmailcredit=$this->getConfig(0, self::CONFIG_CREDIT_MAIL);
		if (($credit<6 && $lastmailcredit!=5) or ($credit==10 && $lastmailcredit!=$credit)) {
			$message = $obj->l('Warning Low SMS credit count.', 'smsAlert').' '.$obj->l('You can purchase SMS credit by following above url.', 'smsAlert').' '.$obj->l('Your credit count is', 'smsAlert').' : '.$credit;
			if ($this->sendWarningMsg($obj, $obj->l('Low SMS Credit', 'smsAlert'), $message)) $this->setConfig(0, self::CONFIG_CREDIT_MAIL, 0, ($credit<6 ? 5 : $credit));
		} else if ($credit<1) {
			$message = $obj->l('SMS was not sent or scheduled because there is no credit left.', 'smsAlert').' '.$obj->l('You can purchase SMS credit by following above url.', 'smsAlert');
			if ($this->sendWarningMsg($obj, $obj->l('Empty SMS Credit', 'smsAlert'), $message)) $this->setConfig(0, self::CONFIG_CREDIT_MAIL, 0, $credit);
		} else if ($leftcredit<0 && $lastmailcredit!=$credit) {
			$message = $obj->l('Warning More SMS are scheduled than current credit count.', 'smsAlert').' '.$obj->l('You can purchase SMS credit by following above url.', 'smsAlert').' '.$obj->l('Your credit count is', 'smsAlert').' : '.$credit.' ('.$this->smsToSend.' '.$obj->l('scheduled', 'smsAlert').')';
			if ($this->sendWarningMsg($obj, $obj->l('Not enough SMS Credit', 'smsAlert'), $message)) $this->setConfig(0, self::CONFIG_CREDIT_MAIL, 0, $credit);
		}
		return $credit;
	}
	
	private function getMsg($obj, $reservation) {
		$notification_delay='5';
		if (intval($reservation->id_order)>0) {
			$order = new Order($reservation->id_order);
			$notification_delay = $this->getDetail(-$order->id_cart, self::DETAIL_DELAY);
			$order_name=Configuration::get('PS_INVOICE_PREFIX', (int)($order->id_lang)).sprintf('%06d', $order->invoice_number);
			if (_PS_VERSION_ >= "1.5.0.0") $order_name=$order->getUniqReference();
		} else $order_name='EXT';
		
		$msg = myOwnLang::getMessage('NOTIFY_CUST',$order->id_lang);
		$plbla="";
		$start_timselot=$reservation->getStartTimeSlot();
		$end_timselot=$reservation->getEndTimeSlot();
	
		$plbl = MyOwnReservationsUtils::getProductName($reservation->id_product, $order->id_lang);
		$plbla = MyOwnReservationsUtils::getAttributeCombinaisons($reservation->id_product, $reservation->id_product_attribute, $order->id_lang);
		if ($plbla!="") $plbl .= ' '.$plbla;
		
		$msg = str_replace('%product%', $plbl, $msg);
		$msg = str_replace('%re%', '', $msg);
		$msg = str_replace('%delay%', self::$delays[$notification_delay], $msg);

  		return $msg;
	}
	
	public function sendWarningMsg($obj, $title, $message) {
       	$id_lang=$this->getConfig(0, self::CONFIG_CREDIT_LANG);
       	if ((int)$id_lang==0) return false;
      	$varsTpl = array('{email}' => 'http://www.labulle.net/sms.php', '{message}' => (Configuration::get('PS_MAIL_TYPE') == 2 ? $message : str_replace(array("\r\n", "\r", "\n"), '<br />', $message)));
      	if (!Mail::Send($id_lang, 'contact', $title, $varsTpl, Configuration::Get('PS_SHOP_EMAIL'), Configuration::Get('PS_SHOP_NAME')))
      		return false;
		return true;		
	}
}


/**#@+
 * @access	private
 */

/**
 * @global array Array de conversion pour les qualités
 */
$GLOBALS['mobyt_qty'] = array(
		laBulleSMS::SMS_QUALITY_DRT		=> 'l',
		laBulleSMS::SMS_QUALITY_TOP		=> 'n',
		laBulleSMS::SMS_QUALITY_LOWCOST   => 'll',
		laBulleSMS::SMS_QUALITY_DEFAULT		=> 'd'
		
	);

class laBulleSMS
{
	// Version de la classe
	const PHPSMS_VERSION = '1.4.11';
	
	// Serveur
	const SERVER = 'http://sms.labulle.net';
	
	// Type déauthentification basée sur hash md5
	const AUTH_MD5 = 1;
	// Type déauthentification basée sur IP avec mot de passe lisible
	const AUTH_PLAIN = 2;
	
	// Qualité messages Direct 
	const SMS_QUALITY_DRT =	3;
	// Qualité messages  TOP 
	const SMS_QUALITY_TOP = 4;
	// Qualité messages Default
	const SMS_QUALITY_DEFAULT = 5;
	// Qualité messages  Low Cost
	const SMS_QUALITY_LOWCOST = 6;

	var $quality = self::SMS_QUALITY_TOP;
	var $from;
	var $domaine = 'http://sms.labulle.net';
	var $login;
	var $pwd;
	var $udh;
	var $auth = self::AUTH_MD5;
	var $ignoreErr= false;
	
	/**
	 * @param string	Nom de léutilisateur (Identifiant)
	 * @param string	Mot de passe
	 * @param string	en-téte expéditeur
	 *
	 * @see setFrom
	 */
	function __construct($login, $pwd, $from = 'laBulle')
	{
		$this->login = $login;
		$this->pwd = $pwd;
		$this->setFrom($from);
	}
	
	/**
	 * Configurer  en-téte expéditeur
	 *
	 * Léexpéditeur peut contenir max 11 caractéres alphanumériques ou un numéro de téléphone
	 * avec préfixe international. 
	 */
	function setFrom($from)
	{
		$this->from = Tools::substr($from, 0, 14);
	}
	
	// Configurer  l'adresse URL du domaine de léadministrateur/revendeur au quel les éventuels clients devront accéder L'URL doit figurer au format 'http://www.mondomaine.fr'
	function setDomaine($domaine) {
		$this->domaine = $domaine;
	}
	
	// Utiliser l'authentification avec mot de passe
	function setAuthPlain() {
		$this->auth = self::AUTH_PLAIN;
	}
	
	// Utiliser l'authentification basée sur hash md5
	function setAuthMd5() {
		$this->auth = self::AUTH_MD5;
	}
	
	// Configurer la qualité des messages Direct
	function setQualityDirect() {
		$this->quality = self::SMS_QUALITY_DRT;
	}
	
	// Configurer la qualité des messages TOP
	function setQualityTop() {
		$this->quality = self::SMS_QUALITY_TOP;
	}

	// Configurer la qualité des messages Low Cost
	function setQualityLowCost() {
		$this->quality = self::SMS_QUALITY_LOWCOST;
	}
	
	// Configurer la qualité des messages Default
	function setQualityDefault() {
		$this->quality = self::SMS_QUALITY_DEFAULT;
	}

	// Ignore error on rcpt send batch
	function setIgnoreError($ignoreErr) {
		$this->ignoreErr = $ignoreErr;
	}
	
		
	
	/**
	 * Contréler le crédit disponible exprimé en Euros
	 *
	 * @returns mixed OK suivie par un entier correspondant au crédit ou KO en cas déerreur
	 *
	 * @example ControleSMS.php Contréle le crédit résiduel et les messages disponibles
	 */
	function getCredit($type='credit')
	{	
		$fields = array(
				'user'		=> $this->login,
				//'pass'	=> $this->pwd,
				'pass'	=> $this->auth == self::AUTH_MD5 ? '' : $this->pwd,
				'ticket'	=> $this->auth == self::AUTH_MD5 ? md5($this->login.$type.$this->pwd) : ''
			);
		
		$fields['type'] = $type ;
		$fields['domaine'] = $this->domaine;
		$fields['path'] = '/sms/credit.php';
		
		return trim($this->httpPost($fields));
	}


	/**
	 * Envoyer un SMS
	 *
	 *
	 * @param string Numéro de  téléphone avec préfixe international (ex. +336101234567)
	 * @param string Texte du message (max 160 caractéres)
	 * @param string Type de SMS (TEXT | WAPPUSH)
	 * @param string L'adresse URL auquel le téléphone mobile qui reéoit un SMS Wap Push ira se connecter 
	 * @param integer Si le paramétre est égal é 1, le message de retour sera léidentificateur déenvoi, é utiliser en cas de requéte 
	 * déétat déanvoi effectué par POST/GET HTTP  (es. HTTP00000000111)
	 *
	 * @returns string Réponse reéue de la passerelle ("OK ..." o "KO ...")
	 *
	 * @example EnvoiUniqueSMS.php Envoi déun sms simple 
	 */

	function sendSms($rcpt, $text, $operation='TEXT', $url='', $return_id='', $scheduled_datetime='')
	{
		global $mobyt_qty, $mobyt_ops;
		
		$fields = array(
				'user'		=> $this->login,
				//'pass'      => $this->pwd,
				'sender'		=> $this->from,
				'rcpt'		=> $rcpt,
				'data'		=> $text,
				'operation' => $operation,
				'url' => $url,
				'return_id' => $return_id
			);
			
		if ($scheduled_datetime!='') $fields['scheduled_datetime'] = $scheduled_datetime;
		
		if ($this->auth == self::AUTH_MD5)
		{
			$fields['pass'] = '';
			$fields['ticket'] = md5($this->login.$rcpt.$this->from.$text.$mobyt_qty[$this->quality].$this->pwd);
		}
		else
		{
			$fields['pass'] = $this->pwd;
			$fields['ticket'] = '';
		}
		
		if (Tools::getIsset($mobyt_qty[$this->quality]))
			$fields['qty'] = $mobyt_qty[$this->quality];
		
		$fields['domaine'] = $this->domaine;
		
		$fields['path'] = '/sms/send.php';
		
		return trim($this->httpPost($fields));
	}
	
	/**
	 * Envoyer un request MNC
	 *
	 * @param array Array de numéros de téléphone avec préfixe international (ex. +336101234567)
	 * @param integer Si le paramétre est égal é 1, le message de retour sera léidentificateur déenvoi, é utiliser en cas de requéte 
	 * déétat déanvoi effectué par POST/GET HTTP  (es. HTTP00000000111)
	 *
	 * @returns string Réponse reéue de la passerelle ("OK ..." o "KO ...")
	 *
	 * @example EnvoiMNC.php Envoi d'un request MNC 
	 */
	function sendMNC($numbers,$return_id='')
	{
		global $mobyt_qty, $mobyt_ops;
		
		$fields = array(
				'user'		=> $this->login,
				//'pass'		=> $this->pwd,
				'pass'	=> $this->auth == self::AUTH_MD5 ? '' : $this->pwd,
				'ticket'	=> $this->auth == self::AUTH_MD5 ? md5($this->login.$numbers.$this->pwd) : '',
				'numbers'   => $numbers,
				'return_id' => $return_id,
				'ignoreErr' => $this->ignoreErr
			);
		
		$fields['domaine'] = $this->domaine;
		
		$fields['path'] = '/sms/mnc.php';
		
		return trim($this->httpPost($fields));
	}
	
	/**
	 * Report on demand des envois
	 *
	 * @param string Léidentificateur de l'envoi
	 * @param string Le type de report souhaité (queue, notify, mnc)
	 * @param string Le schéma du report (Le paramétre doit actuellement avoir la valeur é1é (un))
	 *
	 * @returns string En cas d'erreur, le script renverra une seule ligne contenant KO ainsi qu'un message d'erreur;
	 * dans le cas contraire il renverra les données du report demandé au format CSV avec les champs séparés par une virgule 
	 * et oé la premiére ligne contiendra le nom des colonnes. 
	 *
	 * @example ReportOnDemande.php Report On Demand FTP/HTTP 
	 */
	function sendStatus($id, $type, $schema='1')
	{
		global $mobyt_qty, $mobyt_ops;
		
		$fields = array(
				'user'		=> $this->login,
				//'pass'	    => $this->pwd,
				'pass'	=> $this->auth == self::AUTH_MD5 ? '' : $this->pwd,
				'ticket'	=> $this->auth == self::AUTH_MD5 ? md5($this->login.$id.$type.$schema.$this->pwd) : '',
				'id'        => $id,
				'type'      => $type,
				'schema'    => $schema
			);
		
		$fields['domaine'] = $this->domaine;
		
		$fields['path'] = '/sms/batch-status.php';
		
		return trim($this->httpPost($fields));
	}

	// Send an HTTP POST request, choosing either cURL or fsockopen
	private function httpPost($fields)
	{
		$qs = array();
		foreach ($fields as $k => $v)
			$qs[] = $k.'='.urlencode($v);
		$qs = join('&', $qs);
		
		if (extension_loaded('curl'))
		if (function_exists('curl_init'))
			return laBulleSMS::httpPostCurl($qs, self::SERVER.$fields['path']);
		
		$errno = $errstr = '';
		if ($fp = @fsockopen(Tools::substr($fields['domaine'],7), 80, $errno, $errstr, 30)) 
		{   
			fputs($fp, "POST ".$fields['path']." HTTP/1.0\r\n");
			fputs($fp, "Host: ".Tools::substr(self::SERVER,7)."\r\n");
			fputs($fp, "User-Agent: phplaBulleSMS/".self::PHPSMS_VERSION."\r\n");
			fputs($fp, "Content-Type: application/x-www-form-urlencoded\r\n");
			fputs($fp, "Content-Length: ".Tools::strlen($qs)."\r\n");
			fputs($fp, "Connection: close\r\n");
			fputs($fp, "\r\n".$qs);
			
			$content = '';
			while (!feof($fp))
				$content .= fgets($fp, 1024);
			
			fclose($fp);
			return preg_replace("/^.*?\r\n\r\n/s", '', $content);
		}
		
		return false;
	}

	// Send an HTTP POST request, through cURL
	private function httpPostCurl($qs, $domaine)
	{   
		if ($ch = @curl_init($domaine))
		{   
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_USERAGENT, 'phplaBulleSMS/'.self::PHPSMS_VERSION.' (curl)');
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $qs);
		
			return curl_exec($ch);
		}
		
		return false;
	}
	
}

?>