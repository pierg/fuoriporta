<?php

class labelTemplate extends label{

	/**
	 * Template
	 */
	function template($x, $y, $dataPrint){
		// Label
		$aff_border = 0;
		$ref_font = max($this->labelWidth, $this->labelHeight);
		$des_font = 0.5* min($this->labelWidth, $this->labelHeight);

		$this->setX($x);
		$this->setY($y, false);

		$this->SetFont("helvetica", "BI", 1.2*$des_font);
		$this->setX($x);
		//$this->Cell(0 , 0,$dataPrint['serial'],$aff_border,1,'L',0);
		//$this->SetFont("helvetica", "BI", $des_font);
		
		$imageRaw = $dataPrint['barcode'];
		$label_ratio = $this->labelWidth/$this->labelHeight;
		$barcode_ratio = imagesX($imageRaw)/imagesY($imageRaw);
		if ($label_ratio < $barcode_ratio) {
			$barcode_height = $this->labelHeight-($this->labelMargin*3);//$this->_Height-$this->_Padding*2-$this->_Line_Height-2;
			$barcode_width = $barcode_height * $barcode_ratio;
		} else {
			$barcode_width = $this->labelWidth-($this->labelMargin*3);//$this->_Height-$this->_Padding*2-$this->_Line_Height-2;
			$barcode_height = $barcode_width / $barcode_ratio;
		}
		$margin_x=intval(($this->labelWidth-$barcode_width)/2);
		$margin_y=intval(($this->labelHeight-$barcode_height)/2);
		$this->setX($x);
		$this->Image('@'.$this->GDImage($imageRaw), $x+$margin_x, $y+$margin_y, $barcode_width, $barcode_height);

	}
	
	function GDImage($im)
    {
        //Put the GD image $im
        ob_start();
        imagepng($im);
        $data = ob_get_contents();      
        ob_end_clean();
        return $data;
    }

}//End of class 

?>