<?php
/*
* 2010-2012 LaBulle All right reserved
*/
require_once(MYOWNRES_PATH . "controllers/planning.php");

class myOwnReservationsProController {

	
	public static function displayAddStock($obj, $dayStr, $productFamillyFilter) {
		global $cookie;
		$_products = $obj->_products;
		$output='';
		$addproduct=Tools::getValue('addproduct');
		$attributes = MyOwnReservationsUtils::getProductAttributes($addproduct);
		$isstock=false;

		foreach ($_products->list as $mainproduct)
			if ($mainproduct->productType==product_type::STOCK)
				$isstock=true;
				
		if (!$isstock)
			$output .= '<div id="addStock" style="display:none;">'.myOwnUtils::displayError($obj->l('No reservation rule with stock occupation', 'stocks')).'</div>';
		else {
			$output .= '
			<form action="'.myOwnReservationsController::getConfUrl('stock', '&products='.$productFamillyFilter).'" method="post">';
			
			if (_PS_VERSION_ < "1.6.0.0")
				$output .= '
				<fieldset id="addStock" style="'.(!isset($_POST["submitAddStock"]) ? 'display:none;' : '').'margin:10px">
					<legend><img src="../img/t/AdminCMS.gif" />'.$obj->l('Add to stock', 'controller').'</legend>';
			else $output .= '
				<div class="panel" id="addStock" style="'.(!isset($_POST["submitAddStock"]) ? 'display:none;' : '').'">
					<h3><i class="icon-barcode"></i> '.$obj->l('Add to stock', 'controller').'</h3>';
	
					$output .= '
					<div style="float:left;width:220px;padding-right:15px">
						<span class="mylabel-tooltip" title="'.myOwnHelp::$adminOptions['stock']['add']['product'].'" >'.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRODUCT]).'</span> : <br />
						'.MyOwnReservationsUtils::insertProductPicker($obj, $cookie->id_lang, "stock_id_product", Tools::getValue('stock_id_product', 0), "stock_id_product_attribute",  Tools::getValue('stock_id_product_attribute', 0), "width:100%", '').'
					</div>
					<div style="float:left;width:120px;padding-right:15px">
							<span class="mylabel-tooltip" title="'.myOwnHelp::$adminOptions['stock']['add']['serial'].'" >'.$obj->l('Serial numbers', 'controller').'</span>
					</div>
					<div style="float:left;width:220px;padding-right:15px">
							<textarea style="width:230px" name="serials" cols="20" rows="4">'.Tools::getValue('serials').'</textarea> 
							<label for="type" class="addTimeSlotsLabel"></label>
					</div>
					<div style="float:right;width:120px;padding-right:15px;text-align:right">
						<span style="color: #7F7F7F;font-size: 0.85em;">'.$obj->l('Add a line for each serial number', 'controller').'</span><br /><br />
						<input type="submit" name="submitAddStock" value="'.$obj->l('Add to stock', 'controller').'" class="button btn btn-default" />
					</div>
					<div style="clear:both"></div>
				'.(_PS_VERSION_ < "1.6.0.0" ? '</fieldset>' : '</div>').'
			</form>';
		}
		return $output;
	}
	
	public static function getExpediaElement($dom, $element) {
		if ($element=='AvailRateUpdateRQ')
			$namespace = 'http://www.expediaconnect.com/EQC/AR/2011/06';
		if ($element=='BookingConfirmRQ')
			$namespace = 'http://www.expediaconnect.com/EQC/BC/2007/09';
		if ($element=='BookingRetrievalRQ')
			$namespace = 'http://www.expediaconnect.com/EQC/BR/2014/01';
		
		$dom->preserveWhiteSpace = false;
		$dom->formatOutput = true;
		$domElement = $dom->createElement($element);
		$domElement->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns', $namespace);
		$saleLogin = $dom->createElement('Authentication');
		$saleLogin->setAttribute('username', Configuration::get('MYOWNRES_SYNC_EQC_LOGIN'));
		$saleLogin->setAttribute('password', Configuration::get('MYOWNRES_SYNC_EQC_PASS'));
		$domElement->appendChild($saleLogin);
		$saleHotel = $dom->createElement('Hotel');
		$saleHotel->setAttribute('id', Configuration::get('MYOWNRES_SYNC_EQC_HOTELID'));
		$domElement->appendChild($saleHotel);
		return $domElement;
	}
	
	public static function editOrderDetails($obj, $carriers, $payment_modules, $resa, $productsFilter=array()) {
		global $cookie;
		$orders = MyOwnReservationsUtils::getOrdersList();
		$external_fields = explode(';', $resa->external_fields);
		$method=Tools::getValue('method');
		$output="";
		if (_PS_VERSION_ < "1.6.0.0") $output .= '<fieldset id="editOrderDetails" style="'.($resa->_saved ? 'display:none' : '').'" >
			<legend><img src="../img/t/AdminParentOrders.gif">'.ucfirst(myOwnLang::$object[MYOWN_OBJ::ORDER]).'</legend>';
		else $output .= '<div>';
		
		$output .= '
		<script type="text/javascript">
		var ordertypeval=1;
		function ordertype(type) {
			ordertypeval=type;
			$(".tab-row.active").removeClass("active");
			$("#order_link_" + type).parent().addClass("active");
			if (type==-1) {
				$("#editCustomer").show();
				$("#orderSel").hide();
				$("#orderNew").show();
				$("#hideOnOrder").hide();
				$("#displayProductDetails").hide();
				$("#reservationDetails").hide();
			} else if (type==0) {
				$("#editCustomer").hide();
				$("#displayProductDetails").show();
				$("#reservationDetails").show();
				$("#hideOnOrder").show();
				$("#orderSel").hide();
				$("#orderNew").hide();
			} else {
				$("#editCustomer").hide();
				$("#displayProductDetails").show();
				$("#reservationDetails").show();
				$("#hideOnOrder").hide();
				$("#orderSel").show();
				$("#orderNew").hide();
			}
		}
		
		</script>';
		if (!$resa->_saved && $method!='external') {
			$output .= '
		<div class="bootstrap">
			<div class="productTabs">
				<ul class="tab nav nav-tabs">
					<li class="tab-row active">
						<a class="tab-page" id="order_link_1" href="javascript:ordertype(1);"><i class="icon-folder-open"></i> '.$obj->l('Existing', 'controller').'</a>
					</li>
					<li class="tab-row">
						<a class="tab-page" id="order_link_-1" href="javascript:ordertype(-1);"><i class="icon-plus-square"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::NNEW]).'</a>
					</li>
					<li class="tab-row">
						<a class="tab-page" id="order_link_0" href="javascript:ordertype(0);"><i class="icon-external-link"></i> '.$obj->l('None', 'controller').'</a>
					</li>
				</ul>
			</div>
			<div class="tab-content">
			
				<div id="orderSel" class="panel" style="box-sizing:border-box;margin-top:0px">
					<div style="float:left;line-height:25px"><span class="tooltip" title="'.myOwnHelp::$admin['newresorder'].'" style="font-size:13px; font-weight:bold">'.ucfirst(myOwnLang::$object[MYOWN_OBJ::ORDER]).'</span></div>
					<div style="float:right;line-height:25px">
						<select id="orderId" name="orderId" style="width:150px" onchange="ordertype($(this).val());">';
				if ($productsFilter==array())
				foreach($orders AS $orderElem) {
					$customer = new Customer($orderElem['id_customer']);
					$output .= '<option value="' . $orderElem['id_order']. '">N°' . $orderElem['id_order']. ' '.$obj->l('by(cust)', 'controller').' '.$customer->firstname.' '.$customer->lastname.'</option>';
				}
				$output .= '
						</select>
					</div>
					<div style="clear:both"></div>
				</div>
				
				<div id="orderNew" class="form-group panel" style="display: none;margin-top:0px">
					<div style="float:left;line-height:25px">
						<span class="mylabel-tooltip" title="'.$obj->l('Search a customer by hitting first later of his name', 'reservations').'">
							'.$obj->l('Customer', 'reservations').'
						</span>
					</div>
						
						<div class="" style="float: right">
							<span class="form-control-static">&nbsp;Ou&nbsp;</span>
							<a style="line-height: 22px;" class="fancybox_customer btn btn-default" href="index.php?controller=AdminCustomers&token='.Tools::getAdminToken('AdminCustomers'.intval(Tab::getIdFromClassName('AdminCustomers')).intval($cookie->id_employee)).'&addcustomer&liteDisplaying=1&submitFormAjax=1#">
								<i class="icon-plus-sign-alt"></i>
								'.ucfirst(myOwnLang::$actions[MYOWN_OPE::ADD]).'
							</a>
						</div>
						<div class="" style="float: right">
							<div class="input-group" style="margin:0px">
								<input type="text" id="scheduleCustomer" value="" style="width:100px">
								<span class="input-group-addon">
									<i class="icon-search"></i>
								</span>
							</div>
						</div>
						<div style="clear:both"></div>
				</div>
				
				<div id="hideOnOrder" class="panel" style="'.(!$resa->_saved && $method!='external' ? 'display:none;' : '').'margin-top:0px">';
		}
		
		if ($resa->_saved) $output .= '<div class="well" style="margin: 0px -10px;">';
		$output .= '				
					<div style="float:left;line-height:25px" class=""><span style="font-size:13px; font-weight:bold">'.$obj->l('Customer', 'controller').'</span></div>
					<div style="float:right;line-height:25px" class="">
						<input size="15" id="customer"  name="customerName" type="text" value="'.$external_fields[0].'" style="display:inline-block;width:120px"> 
						'.$obj->l('via', 'controller').' 
						<select name="carrier" id="carrier" style="display:inline-block;width:100px">';
						$output .= '<option value="0" >'.$obj->l('Other', 'controller').'</option>';
						foreach($carriers AS $carrier)
							$output .= '<option value="' . $carrier['id_carrier']. '"'.($resa->_carrier==$carrier['id_carrier'] ? ' selected' : '').'>' . $carrier['name'] . '</option>';
						$output .= '
						</select>
					</div>
					<div style="clear:both"></div>
					<div style="float:left;line-height:25px" class=""><span style="font-size:13px; font-weight:bold">'.$obj->l('Advance', 'controller').'</span></div>
					<div style="float:right;line-height:25px" class="">
						<input size="10" name="advance" id="advance" type="text" value="'.Tools::ps_round($resa->advance,_PS_PRICE_DISPLAY_PRECISION_).'" style="display:inline-block;width:50px"> '.myOwnUtils::getCurrency().' 
						'.$obj->l('via', 'controller').' 
						<select name="payment" id="payment" style="display:inline-block;width:100px">';
						$output .= '<option value="0" >'.$obj->l('Other', 'controller').'</option>';
						foreach($payment_modules AS $payment)
							$output .= '<option value="' . $payment->id. '"'.($resa->_payment==$payment->id ? ' selected' : '').'>' . $payment->displayName . '</option>';
						$output .= '
						</select>
					</div>
					';
		if ($resa->_saved) $output .= '<div style="clear:both"></div></div>';
		if (!$resa->_saved)
			$output .= '
					<div style="clear:both"></div>
				</div>
			</div>
		</div>';
		
		if (_PS_VERSION_ < "1.6.0.0") 
			$output .= '</div>';
		else $output .= '</fieldset>';
		
		$output .= '<div style="clear:both;height:3px"></div>';
		return $output;
	}
	
	public static function stockItemSelect($obj) {
		$output = '';
		$stockitems=$obj->_stocks->list;
		/*foreach ($obj->_products->list as $mainProduct)
			if ($mainProduct->productType==product_type::STOCK) {
				$stockitems=$obj->_stocks->getStockForProduct($resa->id_product, 0);
				break;
			}*/
		$output .= '<select id="stock_product" style="height:24px;width:80px;font-size:12px;background-color:#FFF;" onchange="stockFilterChange($(this).val());">';
		$output .=  '<option value="0">'.$obj->l('All', 'controller').'</option>';
		foreach($stockitems as $stockitem) {
			if ((int)$stockitem->id_product>0) {
				$mainProd = $obj->_products->getResProductFromProduct($stockitem->id_product);
				if ($mainProd!=null) $stockitem->id_family = $mainProd->sqlId;
			} else $stockitem->id_family = 0;
			$name=$stockitem->name;
			if ($name!="") $name=" (".$name.")";
				$output .=  '<option style="font-size:10px;" value="' .$stockitem->serial. '">' .$stockitem->serial.$name.'</option>';
		}
		$output .=  '<option value="">'.$obj->l('None', 'controller').'</option>';
		$output .=  '
		</select>
		
		<script type="text/javascript">
		var stockItems = JSON.parse(\''.json_encode($stockitems).'\');
		$("#productFamillyFilter, #objectFilter").on("change", function() {
			var id_family = $("#productFamillyFilter").val();
			var id_object = $("#objectFilter").val();
			var contents="<option value=\"\">'.$obj->l('All', 'controller').'</option>";
			for(var i in stockItems) {
				if ((stockItems[i].id_family==0 || id_family==0 || stockItems[i].id_family==id_family)
					&& (stockItems[i].id_product==0 || id_object==0 || stockItems[i].id_product==-id_object)) {
					contents+="<option value=\""+stockItems[i].serial+"\">"+stockItems[i].serial+(stockItems[i].name!="" ? " ("+stockItems[i].name+")" : "")+"</option>";
				}
			}
			contents+="<option value=\"0\">'.$obj->l('None', 'controller').'</option>";
			$("#stock_product").html(contents);
		});
		</script>
		';
		
		return $output;
	}
	
	public static function editStockDetails($obj, $resa, $rights, $factorizedDetails='') {
		global $ids;
		$output='';
		//--------------------------------------------------------- product stock ---------------------------------------------------------
		$stockassigned = explode(";",$resa->stock);
		$obj_exts = $obj->_extensions->getExtByType(extension_type::OBJECT);
		$ext_object = null;
		foreach ($obj_exts as $ext) 
			if (method_exists($ext, "objectSelect"))
				if ($resa->_mainProduct!=null && in_array($ext->id, $resa->_mainProduct->ids_options))
					$ext_object = $ext;
		$stockitems=array();
		if ($resa->_mainProduct->productType==product_type::STOCK)
			$stockitems=$obj->_stocks->getStockForProduct($resa->id_product, ($resa->_mainProduct!=null && $resa->_mainProduct->_optionIgnoreAttrStock ? 0 : $resa->id_product_attribute));
		elseif ($ext_object != null && $resa->id_object)
			$stockitems=$obj->_stocks->getStockForProduct(-$resa->id_object, 0);
		/*if ($resa->_mainProduct->productType==product_type::RESOURCE)
			$stockitems=$obj->_extensions->list[product_type::RESOURCE]->getStockForProduct($resa->id_product, 0);*/

		//if ($resa->_mainProduct->productType==product_type::STOCK || $resa->_mainProduct->productType==product_type::PLACE || $resa->_mainProduct->productType==product_type::RESOURCE) {//count($stockitems)>0) {
				
			$details_title = '';
			$detailsExt = null;
			foreach ($obj->_extensions->getExtByType(extension_type::RESADETAILS) as $ext) 
				if ($resa->_mainProduct->_qtyType==$ext->id)
					if (method_exists($ext, "displayReservationDetails"))
						$detailsExt = $ext;
							
			if ($detailsExt != null)
				$details_title = $detailsExt->displayReservationDetails($obj, $resa, 0);
			if ($details_title == '') 
				$details_title = $obj->l('Item', 'controller');
			
			if ($detailsExt != null)
				$editBtn='<a onclick="$(\'#editProductsDetails\').toggle();$(\'#displayProductSubDetails\').toggle();" class="btn btn-default btn-subpos myown-btn-edit"  style="float:right;">'.(_PS_VERSION_ < "1.6.0.0" ? '<img src="../img/admin/edit.gif">' : '<i class="icon-pencil"></i>').' '.ucfirst(myOwnLang::$actions[MYOWN_OPE::EDIT]).'</a>';
							
			if (_PS_VERSION_ >= "1.6.0.0" && $factorizedDetails=='')
				$output .= '<!--start editStockDetails-->
				<div class="" id="displayProductDetails" style="'.($factorizedDetails!='' ? 'float:left;width:320px' :'' ).'">
					<div class="panel" style="padding: 20px 10px;">
						<div class="panel-heading" style="padding:10px;padding-top:5px;padding-bottom:5px">
							<i class="icon-ticket"></i>
							'.$obj->l('Details', 'controller').$editBtn.'
						</div>';
						
			if ($factorizedDetails=='')
				$output .= '
			<table class="table" width="100%;" cellspacing="0" cellpadding="0" id="displayProductSubDetails" style="background-color:#FFF">
				<thead>
					<tr>
					'.(($rights['delete'] && $detailsExt!=null && $resa->quantity > 1) ? '<th></th>' :'').'
					<th align="left">'.$details_title.'</th>
					<th style="width:30%">'.$obj->l('Assignation', 'controller').'</th>
					</tr>
				</thead>
				<tbody>';
					
			for ($i=1; $i<($resa->quantity+1); $i++) {
				$output .= '
				<tr class="invoice_line" id="">';
				if ($rights['delete'] && $detailsExt!=null && $resa->quantity>1 && $factorizedDetails=='')  {
					$output .=  '
					<td><input type="checkbox" class="itemSelect" name="resa_'.$resa->sqlId.'_'.$i.'" value="48" class="noborder" checked></td>';
				}	
				$output .=  '
					<td style="padding:0px">';
				$qty_details = '';
				if ($detailsExt != null)
					$output .= $detailsExt->displayReservationDetails($obj, $resa, $i);
				else 
					$output .= $obj->l('Item', 'controller').' #'.$i;
				$output .= '
					</td>
					<td align="right" style="padding: 3px 0px;">';
				if ($rights['edit']) {
					$output .= '
						<select id="cproduct'.$resa->sqlId.'_'.$i.'" style="font-size:10px;display:inline-block;width:96px;height:22px;margin-top:3px;margin-bottom:2px" onchange="$(\'input#stock_product'.$resa->sqlId.'_'.$i.'\').val($(this).val());">';
					$output .=  '
							<option value="">'.$obj->l('Select..', 'controller').'</option>';
					foreach($stockitems as $stockitem) {
						$selected=false;
						if (count($stockassigned)>=$i && trim($stockassigned[($i-1)])==trim($stockitem->serial)) $selected=true;
						if ($selected) $scount=0;
						else $scount = myOwnResas::getFlatResaLinesCount(null, null, $resa->_mainProduct, $resa->id_product, $resa->id_product_attribute, strtotime($resa->startDate),  strtotime($resa->endDate), $resa->startTimeslot, $resa->endTimeslot, $resa->id_object, $stockitem->serial);
						//if not reserved or current
						if ($scount==0) {
							$name=$stockitem->name;
							if ($name!="" && $ext_object == null) $name=" (".$name.")";
							$output .=  '
							<option style="font-size:10px;" value="' .$stockitem->serial. '" '.($selected ? ' selected' : '').'>' .($resa->_mainProduct->productType==product_type::STOCK ? $stockitem->serial : '').$name.'</option>';
						}
					}
					$output .=  '
						</select>
						<input '.($ext_object != null ? 'type="hidden"' : '').' id="stock_product'.$resa->sqlId.'_'.$i.'" class="stock_product" name="product'.$resa->sqlId.'_'.$i.'" style="display:inline-block;font-size:10px;margin-left:-97px;width:75px;height:1.2em;border:0;margin-right:25px" value="'.(count($stockassigned)>=$i ? $stockassigned[($i-1)] : '').'"/>';
				} else {
					$output .=  $resa->stock;
				}
				$output .=  '
					</td>';
					
				$output .=  '
			'.($i==1 ? $factorizedDetails : '').'
				</tr>';
			}
		
			if ($factorizedDetails=='') { 
				$output .= '
			</table>';
				if ($detailsExt!=null) {
					//$output .= myOwnReservationsReservationsController::editItemsMenu($obj, $rights, $detailsExt);
					$resaCarts = new myOwnCarts($resa->id_cart, $obj->_products, $obj->_pricerules, $obj->_extensions);
					$output .= '
					<div id="editProductsDetails" class="well" style="margin: 0px 0px;display:none">
					'.$detailsExt->displayReservationDetails($obj, $resa, -1, true, true).'
					</div>';
				}
			}
			if (_PS_VERSION_ >= "1.6.0.0" && $factorizedDetails=='') 
					$output .= '</div></div><!--end editStockDetails-->';
		
		//}
		return $output;
	}
}
	


?>