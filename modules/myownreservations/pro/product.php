<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnProductsRule extends myOwnPlanning {
	
	//Create a product in database
	//-----------------------------------------------------------------------------------------------------------------
	function sqlInsert() {
		$id_shop = 0;
		if (defined('MYOWNRES_SEPARATE_SHOP') && MYOWNRES_SEPARATE_SHOP)
			$id_shop = myOwnUtils::getShop();
		if (is_array($this->ids_users)) $ids_users=implode(',', $this->ids_users);
		else $ids_users="";
		$ids_notifs = implode(',', $this->ids_notifs);
		$ids_options = implode(',', $this->ids_options);
		$this->reservationLengthParams =  $this->_reservationUnit.";".$this->_reservationMinLength.";".$this->_reservationMaxLength.";".$this->_reservationBetweenShift.";".intval($this->_allowResOnHolidays).";".intval($this->_allowResOnRestDays).";".intval($this->_reservationLengthControl).";".intval($this->_lengthHolidays).";".intval($this->_lengthDisabledTimeSlots).";".intval($this->_reservationLengthFreq).";".intval($this->_reservationLengthStrict).";".intval($this->_reservationLengthCanExceed).";".intval($this->_reservationLengthQuantity).";".intval($this->_allowResBetweenDays);

		$this->reservationSelParams = $this->_reservationSelDays.";".$this->_reservationSelWeekStart.";".$this->_reservationSelWeekEnd.";".$this->_reservationSelMonthStart.";".$this->_reservationSelMonthEnd.";".intval($this->_reservationSelMode).";".intval($this->_reservationShowTime).';'.$this->_reservationStartTime.';'.$this->_reservationEndTime.';'.(int)$this->_reservationCutTime.';'.(int)$this->_reservationTimeByDay.';'.(int)$this->_timeSliceLength;
		
		$this->priceParams = $this->_priceUnit.";".intval($this->_priceEndTimeSlot).";".intval($this->_priceDisabledTimeSlots).";".intval($this->_priceStrict).";".intval($this->_pricePreview).";".intval($this->_priceHolidays).";".intval($this->_reservationPriceType).";".intval($this->_priceDoubleIfLength).";".intval($this->_priceCombination).";".intval($this->_coefPerProduct);
		
		$this->qtyParams = $this->_qtyCapacity.";".$this->_qtyDisplay.";".$this->_qtyType.";".$this->_qtyFixed.";".$this->_qtySearch;
		$this->options = "0;".intval($this->_optionAlsoSell).";".$this->_optionValidationStatus;
		$this->depositAdvanceParams = $this->_depositCalc.";".$this->_depositAmount.";".$this->_advanceRate.";".$this->_advanceAmount.";".$this->_depositByProduct;
		$this->productOccupyParams = "0;".$this->_occupyQty.";".$this->_occupyCombinations.";".$this->_occupyMaxOrder.";".$this->_occupyStockAssign;
		
		$query  = 'INSERT INTO '. _DB_PREFIX_ .'myownreservations_products (name, id_shop, email, ids_users, assignment, ids_categories, ids_products, productType, ids_notifs, ids_options, reservationStartType, reservationStartParams, reservationPeriod, reservationLengthParams, reservationSelType, reservationSelPlanning, reservationSelParams, productOccupyParams, timeslots, priceParams, qtyParams, depositAdvanceParams, availability, options) VALUES ("'.$this->name.'", "'.$id_shop.'", "'.$this->email.'", "'.$ids_users.'", "'.$this->assignment.'", "'.$this->ids_categories.'", "'.$this->ids_products.'", "'.$this->productType.'", "'.$ids_notifs.'", "'.$ids_options.'", '.$this->reservationStartType.', "'.$this->reservationStartParams.'", '.$this->reservationPeriod.', "'.$this->reservationLengthParams.'", "'.$this->reservationSelType.'", "'.$this->reservationSelPlanning.'", "'.$this->reservationSelParams.'", "'.$this->productOccupyParams.'", "'.$this->timeslots.'", "'.$this->priceParams.'", "'.$this->qtyParams.'", "'.$this->depositAdvanceParams.'", "'.$this->availability.'", "'.$this->options.'");';

		return Db::getInstance()->Execute($query);
	}
	
	//Update a product in database
	//-----------------------------------------------------------------------------------------------------------------
	function sqlUpdate() {
		if (is_array($this->ids_users)) $ids_users=implode(',', $this->ids_users);
		else $ids_users="";
		$ids_notifs = implode(',', $this->ids_notifs);
		$ids_options = implode(',', $this->ids_options);
		$this->reservationLengthParams =  $this->_reservationUnit.";".$this->_reservationMinLength.";".$this->_reservationMaxLength.";".$this->_reservationBetweenShift.";".intval($this->_allowResOnHolidays).";".intval($this->_allowResOnRestDays).";".intval($this->_reservationLengthControl).";".intval($this->_lengthHolidays).";".intval($this->_lengthDisabledTimeSlots).";".intval($this->_reservationLengthFreq).";".intval($this->_reservationLengthStrict).";".intval($this->_reservationLengthCanExceed).";".intval($this->_reservationLengthQuantity).";".intval($this->_allowResBetweenDays);
		
		$this->reservationSelParams = $this->_reservationSelDays.";".$this->_reservationSelWeekStart.";".$this->_reservationSelWeekEnd.";".$this->_reservationSelMonthStart.";".$this->_reservationSelMonthEnd.";".intval($this->_reservationSelMode).";".intval($this->_reservationShowTime).';'.$this->_reservationStartTime.';'.$this->_reservationEndTime.';'.(int)$this->_reservationCutTime.';'.(int)$this->_reservationTimeByDay.';'.(int)$this->_timeSliceLength;

		$this->priceParams = $this->_priceUnit.";".intval($this->_priceEndTimeSlot).";".intval($this->_priceDisabledTimeSlots).";".intval($this->_priceStrict).";".intval($this->_pricePreview).";".intval($this->_priceHolidays).";".intval($this->_reservationPriceType).";".intval($this->_priceDoubleIfLength).";".intval($this->_priceCombination).";".intval($this->_coefPerProduct);
		
		$this->qtyParams = $this->_qtyCapacity.";".$this->_qtyDisplay.";".$this->_qtyType.";".$this->_qtyFixed.";".$this->_qtySearch;
		$this->options = intval($this->_optionIgnoreAttrStock).";".intval($this->_optionAlsoSell).";".$this->_optionValidationStatus;
		$this->depositAdvanceParams = $this->_depositCalc.";".$this->_depositAmount.";".$this->_advanceRate.";".$this->_advanceAmount.";".$this->_depositByProduct;
		$this->productOccupyParams = "0;".$this->_occupyQty.";".$this->_occupyCombinations.";".$this->_occupyMaxOrder.";".$this->_occupyStockAssign;
		$query  = 'UPDATE '. _DB_PREFIX_ .'myownreservations_products SET name = "'.$this->name.'", id_shop = "'.$this->id_shop.'", email = "'.$this->email.'", ids_users = "'.$ids_users.'", assignment = "'.$this->assignment.'", ids_categories = "'.$this->ids_categories.'", ids_products = "'.$this->ids_products.'", productType = "'.$this->productType.'", ids_notifs = "'.$ids_notifs.'", ids_options = "'.$ids_options.'", reservationStartType = '.$this->reservationStartType.', reservationStartParams = "'.$this->reservationStartParams.'", reservationPeriod = "'.$this->reservationPeriod.'", reservationLengthParams = "'.$this->reservationLengthParams.'", reservationSelType = "'.$this->reservationSelType.'", reservationSelPlanning = "'.$this->reservationSelPlanning.'", reservationSelParams = "'.$this->reservationSelParams.'", productOccupyParams = "'.$this->productOccupyParams.'", timeslots = "'.$this->timeslots.'", priceParams = "'.$this->priceParams.'", qtyParams = "'.$this->qtyParams.'", depositAdvanceParams = "'.$this->depositAdvanceParams.'", availability = "'.$this->availability.'", options = "'.$this->options.'" WHERE id_product = '.$this->sqlId.';';

		return Db::getInstance()->Execute($query);
	}
	
	//Delete a product in database
	//-----------------------------------------------------------------------------------------------------------------
	function sqlDelete() {
		$query='DELETE FROM ' . _DB_PREFIX_ . 'myownreservations_products WHERE id_product = "' . $this->sqlId . '"';
		return Db::getInstance()->Execute($query);
	}

}
?>