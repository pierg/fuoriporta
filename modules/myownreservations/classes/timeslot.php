<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnTimeSlot {
	var $sqlId;
	var $id_object=0;
	var $id_product=0;
	var $name='';
	var $id_family=0;
	var $startTime;
	var $endTime;
	var $type;
	var $days;
	var $week_type=0;
	var $quota=0;
	var $_generated=false;

	//Create a timeslot in database
	//-----------------------------------------------------------------------------------------------------------------
	function sqlInsert() {
		$id_shop = 0;
		if (defined('MYOWNRES_SEPARATE_SHOP') && MYOWNRES_SEPARATE_SHOP)
			$id_shop = myOwnUtils::getShop();
		$query  = 'INSERT INTO '. _DB_PREFIX_ .'myownreservations_timeslot (';
		for($i=1; $i<8; $i++) {
			$query  .= 'day'.$i.', ';
		}
		$query  .= 'name, id_family, id_product, id_object, startTime, endTime, type, week_type, quota'.($id_shop ? ', id_shop' :'').') VALUES (';
		for($i=1; $i<8; $i++) {
			if ($this->days[$i]=="1") $query  .= '1, ';
			if ($this->days[$i]!="1") $query  .= '0, ';
		}
		$query  .= '"'.addslashes ($this->name).'", '.intval($this->id_family).', '.intval($this->id_product).', '.intval($this->id_object).', "'.$this->startTime.'", "'.$this->endTime.'", "'.$this->type.'", "'.$this->week_type.'", "'.(int)$this->quota.'"'.($id_shop ? ', "'.$id_shop.'"' :'').' );';
		return Db::getInstance()->Execute($query);
	}
	
	//Update a timeslot in database
	//-----------------------------------------------------------------------------------------------------------------
	function sqlUpdate() {
		$query  = 'UPDATE '. _DB_PREFIX_ .'myownreservations_timeslot SET ';
		for($i=1; $i<8; $i++) {
			if ($this->days[$i]=="1") $query  .= 'day'.$i.' = "1", ';
			if ($this->days[$i]!="1") $query  .= 'day'.$i.' = "0", ';
		}
		$query  .= 'name = "'.addslashes ($this->name).'", id_family = "'.(int)$this->id_family.'", id_product = "'.$this->id_product.'", id_object = "'.$this->id_object.'", startTime = "'.$this->startTime.'", endTime = "'.$this->endTime.'", type = "'.$this->type.'", week_type = "'.$this->week_type.'" WHERE `id_timeslot` = ' . $this->sqlId . ';';
		
		return Db::getInstance()->Execute($query);
	}
	
	//Delete a timeslot in database
	//-----------------------------------------------------------------------------------------------------------------
	function sqlDelete() {
		$query='DELETE FROM ' . _DB_PREFIX_ . 'myownreservations_timeslot WHERE id_timeslot = "' . $this->sqlId . '"';
		return Db::getInstance()->Execute($query);
	}
	
	//is this timeslot enable on a day
	//week_type 0 disabled, -1 odd, -2 even
	//-----------------------------------------------------------------------------------------------------------------
	function isEnableOnDay($day) {
		return $this->days[date("N",$day)] && ($this->week_type==0 || ($this->week_type < 0 && date("W",$day) % 2 == $this->week_type+2) || ($this->week_type > 0 && date("W",$day) == $this->week_type));
	}
	
	function isEnableOnDayOfWeek($dayOfWeek) {
		return $this->days[$dayOfWeek];
	}
	
	function getAvailableDaysStr() {
		$days = "";
		foreach($this->days AS $day)
			$days.= ";".$day;
		return $days;
	}
	
	public function getLength($divider = 0)
    {
        $start = (strtotime($this->startTime) - strtotime('00:00'));
        $stop = (strtotime($this->endTime) - strtotime('00:00'));
        if ($divider > 0) {
            return ($stop - $start) / $divider;
        } else {
            return $stop - $start;
        }
    }

	//Get start date of this timeslot
	//-----------------------------------------------------------------------------------------------------------------
	function getStartHourTime($day) {
		return strtotime(date('Y-m-d',$day).' '.$this->startTime);
	}
	
	public function getStartTime()
    {
        return strtotime($this->startTime);
    }
	
	public function getStartDate($day)
    {
        return strtotime(date('Y-m-d', $day).' '.$this->startTime);
    }

	//Get end date of this timeslot
	//-----------------------------------------------------------------------------------------------------------------
	
	function getEndHourTime($day) {
		$ret = strtotime(date('Y-m-d',$day).' '.$this->endTime);
		if ($this->endTime=="00:00:00") return strtotime('+1day',$ret);
		return $ret;
	}
	
	public function getEndDate($day)
    {
        return strtotime(date('Y-m-d', $day).' '.$this->endTime);
    }
    
    public function getEndTime($shift=false)
    {
    	if ($this->endTime=='00:00:00' && $shift)
    		return strtotime('+1day', strtotime($this->endTime));
        return strtotime($this->endTime);
    }
	
	//availabilities
	//---------------------------------------------------------------------------------------------------------------
	public function isAvailable($day, $availabilities) {
		$day=strtotime(date("Y-m-d",$day)." 00:00:00");
		return $availabilities->isPeriodAvailable($this->getStartHourTime($day), $this->getEndHourTime($day));
	}
	
	    public function getStartMinutes()
    {
        return (strtotime($this->startTime) - strtotime('00:00')) / 60;
    }

    public function getEndMinutes()
    {
        return (strtotime($this->endTime) - strtotime('00:00')) / 60;
    }
	
	//to String
	//-----------------------------------------------------------------------------------------------------------------
	function getStartHourTimeStr($id_lang) {
		return MyOwnCalendarTool::formatTime($this->startTime,$id_lang);
	}
	
	function getEndHourTimeStr($id_lang) {
		return MyOwnCalendarTool::formatTime($this->endTime,$id_lang);
	}
}

?>