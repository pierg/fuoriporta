<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnExtension {
	public $name;
	public $title;
	public $description;
	public $id;
	public $enabled=true;
	public $needconfig=false;
		
	public function getConfig($id_family=-2, $id_option=0, $id_internal=0, $default='') {
		$list=array();
		if ($id_family==-1)$id_family=0;
		$id_shop = (defined('MYOWNRES_SEPARATE_SHOP') && MYOWNRES_SEPARATE_SHOP ? myOwnUtils::getShop() : 0);
		if ($id_shop==1) $id_shop=0;
		$req="SELECT * FROM `"._DB_PREFIX_."myownreservations_option` WHERE `id_extension` = ".$this->id." AND `id_option` = ".intval($id_option);
		$req.=" AND `id_shop`=".$id_shop;

		if ($id_family>-2) $req .= " AND `id_family` = ".intval($id_family);
		if ($id_internal>-1) $req .= " AND `id_internal` = ".intval($id_internal);
		$optionsSql = Db::getInstance()->ExecuteS($req);

		if ($id_internal>-1 && $id_family>-1) { //count($optionsSql)==1 && removed because case of multiple results per shop
			if ($id_internal==-1) {
				if (isset($optionsSql[0]))
					return $optionsSql[0]['value'];
				else return $default;
			}
			if ($id_option>0 && isset($optionsSql[0]['value'])) return $optionsSql[0]['value'];
			if ($id_option>0 && isset($optionsSql[0])) return $optionsSql[0];
		}
		if ($optionsSql==array()) return $default;
		
		return $optionsSql;
	}
	public function removeConfig($id_family=0, $id_option=0, $id_internal=-1) {
		$req = "DELETE FROM `"._DB_PREFIX_."myownreservations_option` WHERE `id_extension` = ".$this->id." AND `id_family` = ".(int)$id_family." AND `id_option` = ".(int)$id_option;
		if ($id_internal>-1) $req .= " AND `id_internal` = ".(int)$id_internal;

 		return Db::getInstance()->Execute($req);
	}
	
	public function getConfigRaw($id_family=-2, $id_option=0, $id_internal=-1) {
		$list=array();
		$id_shop = (defined('MYOWNRES_SEPARATE_SHOP') && MYOWNRES_SEPARATE_SHOP ? myOwnUtils::getShop() : 0);
		if ($id_family==-1)$id_family=0;
		if ($id_shop==1) $id_shop=0;
		$req="SELECT * FROM `"._DB_PREFIX_."myownreservations_option` WHERE `id_extension` = ".$this->id." AND `id_option` = ".intval($id_option)." AND `id_shop`=".$id_shop;
		if ($id_family>-1) $req .= " AND `id_family` = ".intval($id_family);
		if ($id_internal>-1) $req .= " AND `id_internal` = ".intval($id_internal);
		$optionsSql = Db::getInstance()->ExecuteS($req);

		return $optionsSql;
	}
	
	public function setConfig($id_family=0, $id_option=0, $id_internal=-1, $value) {
		$list=array();
		$id_shop = (defined('MYOWNRES_SEPARATE_SHOP') && MYOWNRES_SEPARATE_SHOP ? myOwnUtils::getShop() : 0);
		if ($id_shop==1) $id_shop=0;
		$sql = "SELECT * FROM `"._DB_PREFIX_."myownreservations_option` WHERE `id_extension` = ".$this->id." AND `id_family` = ".intval($id_family)." AND `id_option` = ".intval($id_option)." AND `id_shop`=".$id_shop;
		if ($id_internal>-1) $sql .= " AND `id_internal` = ".intval($id_internal);
		$optionsSql = Db::getInstance()->ExecuteS($sql);

		if (count($optionsSql)==0) {
 			$req="INSERT INTO  `" . _DB_PREFIX_ ."myownreservations_option` (`id_extension`, `id_family`, `id_option`, `id_internal`, `id_shop`, `type`, `value`) VALUES (".$this->id.", ".$id_family.", ".$id_option.", ".(int)$id_internal.", ".$id_shop.", 0, '".$value."');";
		} else {
			$req = "UPDATE `" . _DB_PREFIX_ ."myownreservations_option` SET  `value` =  '".$value."' WHERE `id_extension` = ". $this->id ." AND `id_family` = ".$id_family." AND `id_option` = ".$id_option." AND `id_internal` = ".(int)$id_internal." AND `id_shop`=".$id_shop;
		}

 		return Db::getInstance()->Execute($req);
	}
	
	public function setConfigInternal($id_family=0, $id_option=0, $id_internal) {
		$list=array();

		$id_shop = (defined('MYOWNRES_SEPARATE_SHOP') && MYOWNRES_SEPARATE_SHOP ? myOwnUtils::getShop() : 0);
		if ($id_shop==1) $id_shop=0;
		$sql = "SELECT * FROM `"._DB_PREFIX_."myownreservations_option` WHERE `id_extension` = ".$this->id." AND `id_family` = ".intval($id_family)." AND `id_option` = ".intval($id_option)." AND `id_shop`=".$id_shop;
		$optionsSql = Db::getInstance()->ExecuteS($sql);

		if (count($optionsSql)==0) {
 			$req="INSERT INTO  `" . _DB_PREFIX_ ."myownreservations_option` (`id_extension`, `id_family`, `id_option`, `id_internal`, `id_shop`, `type`, `value`) VALUES (".$this->id.", ".$id_family.", ".$id_option.", ".(int)$id_internal.", ".$id_shop.", 0, '1');";
		} else {
			$req = "DELETE FROM " . _DB_PREFIX_ ."myownreservations_option WHERE `id_extension` = ". $this->id ." AND `id_family` = ".$id_family." AND `id_option` = ".$id_option." AND `id_shop`=".$id_shop." AND `value` = 1;";
			Db::getInstance()->Execute($req);
			$req = "INSERT INTO  `" . _DB_PREFIX_ ."myownreservations_option` (`id_extension`, `id_family`, `id_option`, `id_internal`, `id_shop`, `type`, `value`) VALUES (".$this->id.", ".$id_family.", ".$id_option.", ".(int)$id_internal.", ".$id_shop.", 0, '1');";
					//UPDATE `" . _DB_PREFIX_ ."myownreservations_option` SET  `id_internal` =  '".(int)$id_internal."' WHERE `id_extension` = ". $this->id ." AND `id_family` = ".$id_family." AND `id_option` = ".$id_option." AND `id_shop`=".$id_shop." AND `value` = 1
		}

 		return Db::getInstance()->Execute($req);
	}
	
	public function getDetail($id_reservation, $id_option=0, $id_internal=-1) {
		$list=array();
		$req="SELECT * FROM `"._DB_PREFIX_."myownreservations_detail` WHERE `id_extension` = ".$this->id." AND `id_option` = ".intval($id_option);
		if (!is_array($id_reservation) && $id_reservation!=0) $req.=" AND `id_reservation` = ".(int)$id_reservation;
		if (is_array($id_reservation) && count($id_reservation)>0) $req.=" AND `id_reservation` IN (". implode(',', $id_reservation).')';
		if ($id_internal!=-1) $req.=" AND `id_internal` = ".(int)$id_internal;
		$optionsSql = Db::getInstance()->ExecuteS($req);

		if ($id_internal==-1) {
			if (isset($optionsSql[0]))
				return $optionsSql[0]['value'];
			else return '';
		}
		if ($id_option>-1 && isset($optionsSql[0])) 
			return $optionsSql[0]['value'];
			
		if ($optionsSql==array()) return '';
		return $optionsSql;
	}
	
	public function getDetails($id_reservation, $id_option=0, $id_internal=-1) {
		$list=array();
		$req="SELECT * FROM `"._DB_PREFIX_."myownreservations_detail` WHERE `id_extension` = ".$this->id." AND `id_option` = ".intval($id_option);
		if (!is_array($id_reservation) && $id_reservation!=0) $req.=" AND `id_reservation` = ".(int)$id_reservation;
		if (is_array($id_reservation) && count($id_reservation)>0) $req.=" AND `id_reservation` IN (". implode(',', $id_reservation).')';
		if ($id_internal!=-1)  $req.=" AND `id_internal` = ".(int)$id_internal;
		$optionsSql = Db::getInstance()->ExecuteS($req);

		foreach($optionsSql as $key => $option) {
			if (!$id_reservation && (int)$option['id_reservation']!=0)
				$nkey = (int)$option['id_reservation'];
			else $nkey = $key;
			if (isset($option))
				$list[$nkey]=$option['value'];
		}
		
		return $list;
	}
	
	public function setDetail($id_reservation=0, $id_option=0, $id_internal=-1, $value) {
		$list=array();
		$req = "SELECT * FROM `"._DB_PREFIX_."myownreservations_detail` WHERE `id_extension` = ".$this->id." AND `id_reservation` = ".(int)$id_reservation." AND `id_option` = ".(int)$id_option;
		if ($id_internal>-1)
			$req .= " AND `id_internal` = ".(int)$id_internal;
		
		$optionsSql = Db::getInstance()->ExecuteS($req);
		if (count($optionsSql)==0) {
 			$req='INSERT INTO  `' . _DB_PREFIX_ .'myownreservations_detail` (`id_extension`, `id_reservation`, `id_option`, `id_internal`, `value`) VALUES ('.$this->id.', '.(int)$id_reservation.', '.$id_option.', '.$id_internal.', "'.$value.'");';
		} else {
			$req = "UPDATE `" . _DB_PREFIX_ ."myownreservations_detail` SET  `value` =  '".$value."' WHERE `id_extension` = ". $this->id ." AND `id_reservation` = ".(int)$id_reservation." AND `id_internal` = ".$id_internal.";";
		}

 		return Db::getInstance()->Execute($req);
	}
	
	public function removeDetail($id_reservation=0, $id_option=0, $id_internal=0) {
		$req = "DELETE FROM `"._DB_PREFIX_."myownreservations_detail` WHERE `id_extension` = ".$this->id." AND `id_reservation` = ".(int)$id_reservation." AND `id_option` = ".(int)$id_option." AND `id_internal` = ".(int)$id_internal;
 		return Db::getInstance()->Execute($req);
	}
	
	public function getImg() {
		if (file_exists(MYOWNRES_PATH . "extensions/".$this->name.".png")) return "extensions/".$this->name.".png";
		else return "img/product.png";
	}
}

?>