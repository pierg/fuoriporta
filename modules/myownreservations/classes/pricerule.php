<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnPricesRule {
	var $sqlId;
	var $name;
	var $id_family=0;
	var $id_object=0;
	//var $id_product;
	var $ids_products=array();
	//var $id_product_attribute;
	var $ids_attributes=array();
	var $type;
	var $ratio;
	var $impact;
	var $overall;
	var $quantity=1;
	var $amount;
	var $params;
	var $enable;
	var $visible;
	var $style;
	
	const RULE_DELAY = 5;
	
	//Create a prices rule in database
	//-----------------------------------------------------------------------------------------------------------------
	function sqlInsert() {
		$id_shop = 0;
		if (defined('MYOWNRES_SEPARATE_SHOP') && MYOWNRES_SEPARATE_SHOP)
			$id_shop = myOwnUtils::getShop();
		$products='';
		if (is_array($this->ids_products)) $products = implode(',', $this->ids_products);
		$attributes='';
		if (is_array($this->ids_attributes)) $attributes = implode(',', $this->ids_attributes);
		$query  = 'INSERT INTO '. _DB_PREFIX_ .'myownreservations_pricesrule (name, id_family, id_object, ids_products, ids_attributes, type, impact, overall, quantity, ratio, amount, params, enable, visible, style'.($id_shop ? ', id_shop' :'').') VALUES ("'.$this->name.'", '.$this->id_family.', '.$this->id_object.', "'.$products.'", "'.$attributes.'", '.(int)$this->type.', '.(int)$this->impact.', '.(int)$this->overall.', '.(int)$this->quantity.', "'.(int)$this->ratio.'", "'.$this->amount.'", "'.$this->params.'", '.(int)$this->enable.', '.(int)$this->visible.', "'.$this->style.'"'.($id_shop ? ', "'.$id_shop.'"' :'').');';
		return Db::getInstance()->Execute($query);
	}
	
	//Update a prices rule in database
	//-----------------------------------------------------------------------------------------------------------------
	function sqlUpdate() {
		$products='';
		if (is_array($this->ids_products)) $products = implode(',', $this->ids_products);
		$attributes='';
		if (is_array($this->ids_attributes)) $attributes = implode(',', $this->ids_attributes);
		$query  = 'UPDATE '. _DB_PREFIX_ .'myownreservations_pricesrule SET name = "'.$this->name.'", id_family = '.$this->id_family.', id_object = '.$this->id_object.', ids_products = "'.$products.'", ids_attributes = "'.$attributes.'", type = '.$this->type.', impact = '.$this->impact.', overall = '.$this->overall.', quantity = '.$this->quantity.', ratio = "'.$this->ratio.'", amount = "'.$this->amount.'", params = "'.$this->params.'", enable = '.$this->enable.', visible = '.$this->visible.', style = "'.$this->style.'" WHERE id_pricesrule='.$this->sqlId.';';

		return Db::getInstance()->Execute($query);
	}
	
	function sqlEnable($enable) {
		$query  = 'UPDATE '. _DB_PREFIX_ .'myownreservations_pricesrule SET enable = "'.$enable.'" WHERE id_pricesrule='.$this->sqlId.';';
		return Db::getInstance()->Execute($query);
	}
	
	function sqlVisible($visible) {
		$query  = 'UPDATE '. _DB_PREFIX_ .'myownreservations_pricesrule SET visible = "'.$visible.'" WHERE id_pricesrule='.$this->sqlId.';';
		return Db::getInstance()->Execute($query);
	}
	
	//Delete a prices rule in database
	//-----------------------------------------------------------------------------------------------------------------
	function sqlDelete() {
		$query='DELETE FROM ' . _DB_PREFIX_ . 'myownreservations_pricesrule WHERE id_pricesrule = "' . $this->sqlId . '"';
		return Db::getInstance()->Execute($query);
	}
	
	function doesApplyToProduct($id_family, $id_product, $id_product_attribute=0) {
		if (intval($this->id_family) == 0 or $id_family == $this->id_family)
			if ($this->ids_products == array() or in_array($id_product, $this->ids_products))
				if ($id_product_attribute==0 or $this->ids_attributes==array() or in_array($id_product_attribute, $this->ids_attributes) ) return true;
		
		return false;
	}
	
	function doesApplyToResa($resa, $cartresas=null) {
		global $cart, $cookie;
		$timeslots = $resa->_mainProduct->_timeslotsObj->list;

		if ($this->enable==1
			&& $this->doesApplyToProduct($resa->_mainProduct->sqlId, $resa->id_product, $resa->id_product_attribute)
			&& ($this->id_object==0 || $this->id_object==$resa->id_object) ) {

			switch ($this->type) {
				case pricesrules_type::LENGTH:
					$params=explode(";",$this->params);
					$minBound=$params[0];
					$maxBound=$params[1];
					$canExceed=($params[2]==1);
					if (count($params)>3) $timeSlot=intval($params[3]);
					else $timeSlot=0;
					if (count($params)>4) $weekDay=intval($params[4]);
					else $weekDay=0;
					$applyOnTsDay=(($timeSlot==0 or $resa->startTimeslot == $timeSlot) && ($weekDay == 0 or date("N",strtotime($resa->startDate))==$weekDay));
					if ($minBound<=1 && $maxBound==0) return $applyOnTsDay;//true
					else {
						$count = $resa->getLength(true);
						if ($canExceed && $count >= $minBound) return $applyOnTsDay;//true
						if ($count >= $minBound && ($maxBound==0 or $count <= $maxBound)) return $applyOnTsDay;//true
						return false;
					}
					break;
				case pricesrules_type::DATES:
					$bounds=explode(";",$this->params);
					if ($bounds[2]==1) {
						if ($resa->_mainProduct->_reservationUnit==reservation_unit::TIMESLOT)
							$count = $resa->countTimeslotsBetween(strtotime($bounds[0]), strtotime($bounds[1]));
						if ($resa->_mainProduct->_reservationUnit!=reservation_unit::TIMESLOT)
							$count = $resa->countDaysBetween(strtotime($bounds[0]), strtotime($bounds[1]));
					} else {
						return (strtotime($resa->endDate) >= strtotime($bounds[0]) && strtotime($resa->startDate) <= strtotime($bounds[1]) && strtotime($resa->endDate) <= strtotime($bounds[1])) ;
					}
					return ($count>0);
					break;
				case pricesrules_type::RECURRING:
					$params=explode(";",$this->params);
					if ($resa->_mainProduct->reservationSelType == reservation_interval::TIMESLICE) //$params[0]>0 && 
						return ($resa->countTimeslicesId($params[0],$params)>0);
					else if ($resa->_mainProduct->_reservationUnit==reservation_unit::TIMESLOT) //$params[0]>0 && 
						return ($resa->countTimeslotId($params[0],$params)>0);
					else return (MyOwnCalendarTool::countWeekDayOfResa($resa, $params[0],$params)>0); 
					break;
				case pricesrules_type::DELAY:
					$bounds=explode(";",$this->params);
					$delay = (abs(time() - strtotime($resa->startDate))/86400);
					return ($delay>$bounds[0]
							 && ($delay<$bounds[1] or $bounds[1]==0)); 
					break;
				case pricesrules_type::QTY:
					$bounds=explode(";",$this->params);

						$cnt  = $resa->quantity;
						if ($cartresas==null && $cart!=null) {
							$carts = $cart->getMyOwnReservationCart();
							$cartresas = $carts->getResas();
						}
						if ($cartresas!=null)
						foreach ($cartresas as $tmp_resa)
							if ($tmp_resa->sqlId != $resa->sqlId)
								if ( (!$bounds[3] or $resa->id_product==$tmp_resa->id_product) and (!$bounds[4] or $resa->id_product_attribute==$tmp_resa->id_product_attribute) )
									if (!$bounds[4] or ($resa->startDate==$tmp_resa->startDate && $resa->endDate==$tmp_resa->endDate && $resa->startTimeslot==$tmp_resa->startTimeslot && $resa->endTimeslot==$tmp_resa->endTimeslot))
									$cnt += $tmp_resa->quantity;
					
					return ($cnt>=$bounds[0]
							 && ($cnt<=$bounds[1] or $bounds[1]==0)); 
					break;
				case pricesrules_type::HISTORY:
					$bounds=explode(";",$this->params);
					$sameprod=$bounds[3];
					$sameattr=$bounds[4];
					$sameattendee = $bounds[5];//$bounds[5];
					$cnt=0;
					$obj = Module::getInstanceByName('myownreservations');
					$ext = $obj->_extensions->getExtByName('attendees');
					if ($cookie->id_customer) $resas = myOwnResas::getReservationsForCustomer($obj, $cookie->id_customer, 'past');
					else return false;
					if ($sameattendee) {
						$curentattendees = $ext->getNames($resa->sqlId);
						foreach ($curentattendees as &$curentattendee)
							$curentattendee['cnt']=0;
					}
					foreach ($resas as $tmp_resa) {
						//echo '<br>_'.$resa->id_product.'('.$tmp_resa->sqlId;
						if ($tmp_resa->sqlId != $resa->sqlId)
							if ( (!$sameprod or $resa->id_product==$tmp_resa->id_product) and (!$sameattr or $resa->id_product_attribute==$tmp_resa->id_product_attribute) ) {
								if (!$sameattendee)
									$cnt += (int)$tmp_resa->getLength();
								else {
									$idcart = myOwnCarts::getResCartIdFromResa($obj, $tmp_resa);
									$oldattendees = $ext->getNames($idcart);
									foreach ($oldattendees as $oldattendee)
										foreach ($curentattendees as &$curentattendee) {
											if (strtolower($curentattendee['lastname'])==strtolower($oldattendee['lastname'])
												&& strtolower($curentattendee['firstname'])==strtolower($oldattendee['firstname']))
												$curentattendee['cnt'] += (int)$tmp_resa->getLength();
										}
								}
							}
					}
					if ($sameattendee) {
						foreach ($curentattendees as $key => &$curentattendee) {
							if ($curentattendee['cnt']>=$bounds[0] && ($curentattendee['cnt']<=$bounds[1] or $bounds[1]==0)) {
								return true;
							}
						}
						return false;
					} else
						return ($cnt>=$bounds[0]
							 && ($cnt<=$bounds[1] or $bounds[1]==0)); 
					break;
			}		
		} else {
			return false;
		}
	}
	
	function getMaxBound() {
		$bounds=explode(";",$this->params);
		if (intval($bounds[1])==0) return $bounds[2];
		return $bounds[1];
	}
	
	function getConditionToString($obj, $mainProduct = null) {
		global $cookie;
		$output='';
		$langue = $cookie->id_lang;
		$params = $params=explode(";",$this->params);
		if ($this->type == pricesrules_type::DATES) 
			$output .= $obj->l('From(date)', 'pricerules').' '.MyOwnCalendarTool::formatDate(strtotime($params[0]),$langue).(array_key_exists(1, $params) ? ' '.$obj->l('to(date)', 'pricerules').' '.MyOwnCalendarTool::formatDate(strtotime($params[1]),$langue) : '');
		if ($this->type == pricesrules_type::LENGTH or $this->type == pricesrules_type::DELAY or $this->type == pricesrules_type::QTY or $this->type == pricesrules_type::HISTORY) 
			if ($params[0]<=1 && $params[1]==0) $output .= $obj->l('For any length', 'pricerules');
			else {
				if (array_key_exists(1, $params) && $params[0]==$params[1]) $output .= $obj->l('For', 'pricerules').' '.$params[0].' ';
				else $output .= $obj->l('From', 'pricerules').' '.$params[0].(array_key_exists(1, $params) ? ' '.$obj->l('to(qty)', 'pricerules').' '.$params[1].' ' : '');
				if ($mainProduct!=null && $this->type != pricesrules_type::QTY) $output .= $produnitStr=myOwnLang::getUnit($mainProduct->_reservationUnit, ($params[1]>1));
			}
		if ($this->type == pricesrules_type::RECURRING) { 
			if (intval($params[0])==0) {
				$output .= $obj->l('For all time slots', 'pricerules').' ';
			} else {
				$output .= $obj->l('For time slot', 'pricerules').' : ';
				if (array_key_exists($params[0],$obj->_timeSlots->list)) $output .= $obj->_timeSlots->list[$params[0]]->name;
				else  $output .= $obj->l('unknown', 'pricerules');
			}
		}
		return $output;
	}
	
	function getInpactToString($mainProduct = null) {
		$impact = '';$calc='';
		if ($this->impact == 0) $impact = '-';
		if ($this->impact == 1) $impact = '=';
		if ($this->impact == 2) $impact = '+';
		if ($this->amount > 0) $calc .= $impact.Tools::ps_round($this->amount, 2).myOwnUtils::getCurrency();
		if ($this->ratio > 0 && $this->impact != 1) $calc .= ' '.$impact.($this->impact!=1 ? number_format((100*$this->ratio),_PS_PRICE_DISPLAY_PRECISION_,'.','').'%' : '');
		if ($mainProduct != null && $this->impact == 3) $calc .= ' /'.myOwnLang::getUnit($mainProduct->_priceUnit);
		return $calc;
	}
	
	function getResaReduc($resa, $availabilities=null) {
		global $cookie;
		$timeslots=$resa->_mainProduct->_timeslotsObj->list;
		//if ($this->doesApplyToResa($resa)) {
			$originalLength=$resa->getLength(MYOWNRES_PRICERULE_LENASPRICE, $availabilities);
			//calculate reduction length
			switch ($this->type) {
				case pricesrules_type::LENGTH:
					$bounds=explode(";",$this->params);
					
					if ($this->overall) {
						$length=$originalLength;
					} else {
						if ($originalLength<$bounds[1]) $length=$originalLength-$bounds[0]+1;
						else $length=$bounds[1]-$bounds[0]+1;
					}
					break;
				case pricesrules_type::DATES:
					$bounds=explode(";",$this->params);
					
					if ($this->overall) {
						$length=$originalLength;
					} else {
						$length=0;
						if ($resa->_mainProduct->_priceUnit==reservation_unit::TIMESLOT)
							$length = $resa->countTimeslotsBetween(strtotime($bounds[0]), strtotime($bounds[1]));
						if ($resa->_mainProduct->_priceUnit!==reservation_unit::TIMESLOT)
							$length = $resa->CountDaysBetween(strtotime($bounds[0]), strtotime($bounds[1]), -3);
							if ($length==-3) $length=$originalLength;
						if ($resa->_mainProduct->_priceUnit==reservation_unit::WEEK) {
							//$length = ceil($originalLength/7);
							$length = $resa->countWeeksBetween(strtotime($bounds[0]), strtotime($bounds[1]));
						}
						if ($resa->_mainProduct->_priceUnit==reservation_unit::MONTH)
							$length = ceil($originalLength/30.4);
					}
					break;
				case pricesrules_type::RECURRING:
					$params=explode(";",$this->params);
					
					if ($this->overall) {
						$length=$resa->getLength();
					} else {
						if ($resa->_mainProduct->_priceUnit==1) $length = MyOwnCalendarTool::countWeekDayOfResa($resa, $params[0],$params);
						else $length = $resa->countTimeslotId($params[0],$params);
					}
					break;
				case pricesrules_type::DELAY:
					$bounds=explode(";",$this->params);
					
					if ($this->overall or $bounds[1]==0) {
						$length=$originalLength;
					} else {
						$delay = (abs(time() - strtotime($resa->startDate))/86400);
						if ($delay+$originalLength<$bounds[1]) $length=$originalLength;
						else $length=$bounds[1]-$delay;
					}
					break;
				case pricesrules_type::QTY:
					return $originalLength;
					break;
				case pricesrules_type::HISTORY:
					$bounds=explode(";",$this->params);
					$sameprod = $bounds[3];
					$sameattr = $bounds[4];
					$sameattendee = $bounds[5];
					$cnt=0;
					$obj = Module::getInstanceByName('myownreservations');
					$ext = $obj->_extensions->getExtByName('attendees');
					if ($cookie->id_customer) $resas = myOwnResas::getReservationsForCustomer($obj, $cookie->id_customer, 'past');
					else return false;
					if ($sameattendee) {
						$curentattendees = $ext->getNames($resa->sqlId);
						foreach ($curentattendees as &$curentattendee)
							$curentattendee['cnt']=0;
					}
					foreach ($resas as $tmp_resa) {
						//echo '<br>_'.$resa->id_product.'('.$tmp_resa->sqlId;
						if ($tmp_resa->sqlId != $resa->sqlId)
							if ( (!$sameprod or $resa->id_product==$tmp_resa->id_product) and (!$sameattr or $resa->id_product_attribute==$tmp_resa->id_product_attribute) ) {
								if (!$sameattendee)
									$cnt += (int)$tmp_resa->getLength();
								else {
									$idcart = myOwnCarts::getResCartIdFromResa($obj, $tmp_resa);
									$oldattendees = $ext->getNames($idcart);
									foreach ($oldattendees as $oldattendee)
										foreach ($curentattendees as &$curentattendee) {
											if (strtolower($curentattendee['lastname'])==strtolower($oldattendee['lastname'])
												&& strtolower($curentattendee['firstname'])==strtolower($oldattendee['firstname']))
												$curentattendee['cnt'] += (int)$tmp_resa->getLength();
										}
								}
							}
					}
					if ($sameattendee) {
						$length=0;
						foreach ($curentattendees as &$curentattendee)
							if ($curentattendee['cnt']>=$bounds[0] && ($curentattendee['cnt']<=$bounds[1] or $bounds[1]==0))
								if ($this->overall) {
									$length+=$originalLength;
								} else {
									if ($originalLength<$bounds[1]) $length+=$originalLength-$bounds[0]+1;
									else $length+=$bounds[1]-$bounds[0]+1;
								}
						return $length/$resa->quantity;
					} else {
						if ($this->overall) {
							$length=$originalLength;
						} else {
							if ($originalLength<$bounds[1]) $length=$originalLength-$bounds[0]+1;
							else $length=$bounds[1]-$bounds[0]+1;
						}
					}
					break;
			}
			return $length;
		/*} else {
			return 0;
		}*/
	}
}



?>