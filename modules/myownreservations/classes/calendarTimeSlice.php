<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnCalendarTimeSlice {
	//public $startTime;
	//public $endTime;
	public $start;
	public $slotEnd;
	public $end;
	public $date;
	public $idTimeSlot;
	public $label;
	public $type=0;
	public $selType='';
	
	public $cart;
	
	public $previous;
	public $next;
	public $css;
	public $availableQuantity=-1;
	public $defaultQuantities=array();
	public $unavailableQuantity=array();//for home display
	public $availableQuantities=array();
	public $availableResources=array();
	public $object=null;
	public $isAvailable=true;
	public $isEnable=true;
	public $isReduc=true;
	public $price;
	public $_highPrice;
	public $key;
	
	public function __construct($date, $timeSlot=null, $type=0, $view=null)
	{
		$this->type = $type;
		if ($view!=null) $this->selType = $view->potentialResa->_selType;
		//for a day summary (when choosing a day before it timeslotss
		if ($timeSlot==null) {
			$this->key=date('Y-m-d',$date);
			$this->idTimeSlot = -1;
			$this->start=strtotime(date('Y-m-d',$date).' 00:00:00');
			$this->slotEnd=strtotime('+1day', $this->start);
			$this->end=$this->slotEnd;
		} else {
			$this->idTimeSlot = $timeSlot->sqlId;
			$this->start = strtotime(date('Y-m-d',$date).' '.$timeSlot->startTime);
			$this->slotEnd = strtotime(date('Y-m-d',$date).' '.$timeSlot->endTime);
			//removed && $view->potentialResa->_mainProduct->reservationSelType!=reservation_interval::WEEK because week availabilities not applied if timeslice end ! timeslice start+1 week
			if ($view!=null && !$this->idTimeSlot && $view->potentialResa->_mainProduct!=null && $view->potentialResa->_selType!="start") 
				$this->end=$view->potentialResa->getNextPeriod($this->start);
			else {
				$this->end=$this->slotEnd;
			}

			if ($this->start == $this->end) {
				//display on calendar a resa with smae start day end end day
				if ($view->potentialResa->_mainProduct!=null && $view->potentialResa->_mainProduct->reservationSelType==reservation_interval::DAY)
						$this->end = strtotime("+1 day",$this->end);
				/*if ($view->potentialResa->_mainProduct!=null && $view->potentialResa->_mainProduct->reservationSelType==reservation_interval::WEEK)
					$this->end = strtotime("+1 week",$this->start);*/  //comented because switching one more week for nothing
					
				if ($view->potentialResa->_mainProduct!=null && $view->potentialResa->_mainProduct->reservationSelType==reservation_interval::MONTH)
					$this->end = strtotime("+1 month",$this->start);
			}
			
			$res_way = Tools::getValue('res_way', '');
			if ($type==2) {
				//if in optional length we cas set only end bound
				if ($res_way=='stop')
					$this->key='0000-00-00_0@'.date('Y-m-d',$this->end).'_'.$timeSlot->sqlId;
				else
					$this->key=date('Y-m-d',$this->end).'_'.$timeSlot->sqlId;
			}
			else  $this->key=date('Y-m-d',$this->start).'_'.$timeSlot->sqlId;
		}
	}
	
	public function getKey() {
		return $this->key.($this->object!=null && $this->selType!= 'start' ? '@'.$this->object->sqlId : '');
	}
	
	public function getStartMinutes() {
		$stab = explode(':', date('H:i', $this->start));
		return (int)$stab[0]*60+(int)$stab[1];
	}
	
	public function getSliceEndMinutes() {
		$stab = explode(':', date('H:i', $this->end));
		return (int)$stab[0]*60+(int)$stab[1];
	}
	
	public function getSlotEndMinutes() {
		$stab = explode(':', date('H:i', $this->slotEnd));
		return (int)$stab[0]*60+(int)$stab[1];
	}
	
	public function getDate() {
		return strtotime(date('Y-m-d',$this->start).' 00:00:00');
	}
	
	public function getEndDate() {
		return strtotime(date('Y-m-d',$this->end).' 00:00:00');
	}
	
	public function getEndTime() {
		return date('H:i:s',$this->end);
	}
	
	public function getStartTime() {
		return date('H:i:s',$this->start);
	}
	
}