<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnLang {
	public static $daysEn = array(1 => 'Monday',2 => 'Tuesday',3 => 'Wednesday',4 => 'Thursday',5 => 'Friday',6 => 'Saturday',7 => 'Sunday');
	public static $daysFr = array(1 => 'Lundi',2 => 'Mardi',3 => 'Mercredi',4 => 'Jeudi',5 => 'Vendredi',6 => 'Samedi',7 => 'Dimanche');
	public static $daysEs = array(1 => 'Lunes',2 => 'Martes',3 => 'Miércoles',4 => 'Jueves',5 => 'Viernes',6 => 'Sábado',7 => 'Domingo');
	public static $daysDe = array(1 => 'Montag',2 => 'Dienstag',3 => 'Mittwoch',4 => 'Donnerstag',5 => 'Freitag',6 => 'Samsag',7 => 'Sonntag');
	public static $daysIt = array(1 => 'Lun',2 => 'Mar',3 => 'Mer',4 => 'Gio',5 => 'Ven',6 => 'Sab',7 => 'Dom');
	public static $daysDa = array(1 => 'Mandag',2 => 'Tirsdag',3 => 'Onsdag',4 => 'Torsdag',5 => 'Fredag',6 => 'Lørdag',7 => 'Søndag');
	public static $daysNl = array(1 => 'Maandag',2 => 'Dinsdag',3 => 'Woensdag',4 => 'Donderdag',5 => 'Vrijdag',6 => 'Zaterdag',7 => 'Zondag');
	public static $daysPt = array(1 => 'Segunda-feira',2 => 'Terça-feira',3 => 'Quarta-feira',4 => 'Quinta-feira',5 => 'Sexta-feira',6 => 'Sábado',7 => 'Domingo');
	public static $daysKp = array(1 => '월요일',2 => '화요일',3 => '수요일',4 => '목요일',5 => '금요일',6 => '토요일',7 => '일요일');
	public static $daysSv = array(1 => 'Måndag',2 => 'Tisdag',3 => 'Onsdag',4 => 'Torsdag',5 => 'Fredag',6 => 'Lördag',7 => 'Söndag');

	public static $monthsEn = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
	public static $monthsFr = array(1 => 'Janvier', 2 => 'Février', 3 => 'Mars', 4 => 'Avril', 5 => 'Mai', 6 => 'Juin', 7 => 'Juillet', 8 => 'Aout', 9 => 'Septembre', 10 => 'Octobre', 11 => 'Novembre', 12 => 'Décembre');
	public static $monthsEs = array(1 => 'Enero', 2 => 'Febrero', 3 => 'Marzo', 4 => 'Abril', 5 => 'Mayo', 6 => 'Junio', 7 => 'Julio', 8 => 'Agosto', 9 => 'Septiembre', 10 => 'Octubre', 11 => 'Noviembre', 12 => 'Diciembre');
	public static $monthsDe = array(1 => 'Januar', 2 => 'Februar', 3 => 'März', 4 => 'April', 5 => 'Mai', 6 => 'Juni', 7 => 'Juli', 8 => 'August', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Dezember');
	public static $monthsIt = array(1 => 'Gennaio', 2 => 'Febbraio', 3 => 'Marzo', 4 => 'Aprile', 5 => 'Maggio', 6 => 'Giugno', 7 => 'Luglio', 8 => 'Agosto', 9 => 'Settembre', 10 => 'Ottobre', 11 => 'Novembre', 12 => 'Dicembre');
	public static $monthsDa = array(1 => 'Januar', 2 => 'Februar', 3 => 'Marts', 4 => 'April', 5 => 'Maj', 6 => 'Juni', 7 => 'Juli', 8 => 'August', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'December');
	public static $monthsNl = array(1 => 'Januari', 2 => 'Februari', 3 => 'Maart', 4 => 'April', 5 => 'Mei', 6 => 'Juni', 7 => 'Juli', 8 => 'Augustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'December');
	public static $monthsPt = array(1 => 'Janeiro', 2 => 'Fevereiro', 3 => 'Março', 4 => 'Abril', 5 => 'Maio', 6 => 'Junho', 7 => 'Julho', 8 => 'Agosto', 9 => 'Setembro', 10 => 'Outubro', 11 => 'Novembro', 12 => 'Dezembro');
	public static $monthsKp = array(1 => '일월', 2 => '이월', 3 => '삼월', 4 => '사월', 5 => '오월', 6 => '유월', 7 => '칠월', 8 => '팔월', 9 => '구월', 10 => '시월', 11 => '십일월', 12 => '십이월');
	public static $monthsSv = array(1 => 'Januari', 2 => 'Februari', 3 => 'Mars', 4 => 'April', 5 => 'Maj', 6 => 'Juni', 7 => 'Juli', 8 => 'Augusti', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'December');

	public static $msg_desactivate_res_en = "Reservations are are not available for the moment.";
	public static $msg_desactivate_res_fr = "Les réservations ne sont pas disponibles pour le moment.";
	public static $msg_desactivate_res_es = "Las reservaciones no están disponibles por el momento.";

	public static $msg_notify_res_en = "Your initial reservation for %product% was rescheduled from the %start% to the %end%";
	public static $msg_notify_res_fr = "Votre réservation initiale pour %product% a été replanifiée du %start% au %end%";
	public static $msg_notify_res_es = "Su reservación inicial por %product% ha sido modificada del %start% al %end%";

	public static $msg_notavailable_en = "Sorry the following bookings are no longer available";
	public static $msg_notavailable_fr = "Désolé les réservations suivantes ne sont plus disponibles";
	public static $msg_notavailable_es = "Los sentimos las reservaciones siguientes no son disponibles";

	public static $msg_notify_prod_en = "A reservation for %product% was %re%scheduled from the %start% to the %end%";
	public static $msg_notify_prod_fr = "Une réservation pour %product% a été %re%planifiée du %start% au %end%";
	public static $msg_notify_prod_es = "Una reservación por %product% ha sido planificada de %start% al %end%";

	public static $msg_notify_cust_en = "Your reservation for %product% starts in %delay%";
	public static $msg_notify_cust_fr = "Votre réservation pour %product% commence dans %delay%";
	public static $msg_notify_cust_es = "Su reserva para el %product% empieza en %delay%";

	public static $lbl_start_en = "Start";
	public static $lbl_start_fr = "Début";
	public static $lbl_start_es = "Comenzando";

	public static $lbl_stop_en = "End";
	public static $lbl_stop_fr = "Fin";
	public static $lbl_stop_es = "Fin";

	public static $priceruleImpacts;
	public static $priceruleTypes;
	public static $priceruleTypDescription;

	public static $reservationInterval;
	public static $reservationIntervalDescription;
	public static $reservationUnitSingle;
	public static $reservationUnitMultiple;
	public static $reservationViews;

	public static $reservationMode;

	public static $productType;
	public static $productTypeDescription;

	public static $reservTypes;
	public static $reservSwitchesNext;
	public static $reservSwitchesPrevious;
	public static $reservQtyType;
	public static $reservPriceType;

	public static $lengthControl;
	public static $qtyControl;
	public static $virtualQuantity;
	public static $stockAssignation;

	public static $depositTypes;
	public static $availabilityTypes;

	public static $operations;
	public static $actions;
	public static $object;
	public static $objects;

	public static $sync_platforms;

	//Translation
	//-----------------------------------------------------------------------------------------------------------------

	public static function translate($obj) {
		self::$sync_platforms = array (
			sync_platforms::ABRITEL => $obj->l('Abritel', 'lang'),
			sync_platforms::AIRBNB => $obj->l('airbnb', 'lang'),
			sync_platforms::BOOKING => $obj->l('Booking.com', 'lang'),
			sync_platforms::TRIPADVISOR => $obj->l('Trip Advisor', 'lang'),

		);
		if (is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php"))
			self::$sync_platforms[sync_platforms::EXPEDIA] = $obj->l('Expedia', 'lang');

		self::$object = array (
			MYOWN_OBJ::PREFIX => $obj->l('The', 'lang'),
			MYOWN_OBJ::TIMESLOT => $obj->l('time slot', 'lang'),
			MYOWN_OBJ::TIMESLICE => $obj->l('time slice', 'lang'),
			MYOWN_OBJ::AVAILABILITY => $obj->l('availability', 'lang'),
			MYOWN_OBJ::FAMILY => $obj->l('family', 'lang'),
			MYOWN_OBJ::PRODUCT => $obj->l('product', 'lang'),
			MYOWN_OBJ::PRICERULE => $obj->l('price rule', 'lang'),
			MYOWN_OBJ::STOCK => $obj->l('stock', 'lang'),
			MYOWN_OBJ::RESVRULE => $obj->l('reservation rule', 'lang'),
			MYOWN_OBJ::PRODFAMILY => $obj->l('product family', 'lang'),
			MYOWN_OBJ::RESV => $obj->l('reservation', 'lang'),
			MYOWN_OBJ::ORDER => $obj->l('order', 'lang'),
			MYOWN_OBJ::STOCKITEM => $obj->l('stock item', 'lang'),
			MYOWN_OBJ::PRICESET => $obj->l('price set', 'lang'),
		);
		self::$objects = array (
			MYOWN_OBJ::PREFIX => $obj->l('The(s)', 'lang'),
			MYOWN_OBJ::TIMESLOT => $obj->l('time slots', 'lang'),
			MYOWN_OBJ::TIMESLICE => $obj->l('time slices', 'lang'),
			MYOWN_OBJ::AVAILABILITY => $obj->l('availabilities', 'lang'),
			MYOWN_OBJ::FAMILY => $obj->l('families', 'lang'),
			MYOWN_OBJ::PRODUCT => $obj->l('products', 'lang'),
			MYOWN_OBJ::PRICERULE => $obj->l('price rules', 'lang'),
			MYOWN_OBJ::STOCK => $obj->l('stocks', 'lang'),
			MYOWN_OBJ::RESVRULE => $obj->l('reservation rules', 'lang'),
			MYOWN_OBJ::PRODFAMILY => $obj->l('product families', 'lang'),
			MYOWN_OBJ::RESV => $obj->l('reservations', 'lang'),
			MYOWN_OBJ::ORDER => $obj->l('orders', 'lang'),
			MYOWN_OBJ::STOCKITEM => $obj->l('stock items', 'lang'),
			MYOWN_OBJ::PRICESET => $obj->l('price sets', 'lang'),
		);

		self::$operations = array (
			MYOWN_OPE::UPDATE => $obj->l('updated', 'lang'),
			MYOWN_OPE::CREATE => $obj->l('created', 'lang'),
			MYOWN_OPE::DELETE => $obj->l('deleted', 'lang'),
			MYOWN_OPE::ADD => $obj->l('added', 'lang'),
			MYOWN_OPE::IMPORT => $obj->l('imported', 'lang'),
			MYOWN_OPE::DISABLE => $obj->l('disabled', 'lang'),
			MYOWN_OPE::ENABLE => $obj->l('enabled', 'lang'),
			MYOWN_OPE::COPY => $obj->l('copied', 'lang'),
			MYOWN_OPE::VALIDATE => $obj->l('validated', 'lang'),
			MYOWN_OPE::UNVALIDATE => $obj->l('unvalidated', 'lang'),
			MYOWN_OPE::SAVE => $obj->l('saved', 'lang'),
			MYOWN_OPE::EXPORT => $obj->l('exported', 'lang'),
			MYOWN_OPE::PPRINT => $obj->l('printed', 'lang'),
			MYOWN_OPE::NORES => $obj->l('There is no', 'lang'),
		);

		self::$actions = array (
			MYOWN_OPE::UPDATE => $obj->l('update', 'lang'),
			MYOWN_OPE::CREATE => $obj->l('create', 'lang'),
			MYOWN_OPE::DELETE => $obj->l('delete', 'lang'),
			MYOWN_OPE::ADD => $obj->l('add', 'lang'),
			MYOWN_OPE::IMPORT => $obj->l('import', 'lang'),
			MYOWN_OPE::DISABLE => $obj->l('disable', 'lang'),
			MYOWN_OPE::ENABLE => $obj->l('enable', 'lang'),
			MYOWN_OPE::COPY => $obj->l('copie', 'lang'),
			MYOWN_OPE::VALIDATE => $obj->l('validate', 'lang'),
			MYOWN_OPE::UNVALIDATE => $obj->l('unvalidate', 'lang'),
			MYOWN_OPE::SAVE => $obj->l('save', 'lang'),
			MYOWN_OPE::SAVESTAY => $obj->l('save and stay', 'lang'),
			MYOWN_OPE::EXPORT => $obj->l('export', 'lang'),
			MYOWN_OPE::PPRINT => $obj->l('print', 'lang'),
			MYOWN_OPE::HELP => $obj->l('help', 'lang'),
			MYOWN_OPE::CANCEL => $obj->l('cancel', 'lang'),
			MYOWN_OPE::EDIT => $obj->l('edit', 'lang'),
			MYOWN_OPE::NNEW => $obj->l('new', 'lang'),
			MYOWN_OPE::FNEW => $obj->l('new(f)', 'lang'),
		);

		self::$priceruleImpacts = array(
			pricesrules_impact::DECREASE 	=> $obj->l('Price decrease', 'lang'),
			pricesrules_impact::SET 		=> $obj->l('Total price', 'lang'),
			pricesrules_impact::INCREASE 	=> $obj->l('Price increase', 'lang'),
			pricesrules_impact::PERIOD	 	=> $obj->l('Unit price', 'lang')
		);

		self::$priceruleTypes = array(
			pricesrules_type::LENGTH 		=> $obj->l('Length', 'lang'),
			pricesrules_type::DATES			=> $obj->l('Dates', 'lang'),
			pricesrules_type::RECURRING		=> $obj->l('Recurring', 'lang'),
			pricesrules_type::DELAY			=> $obj->l('Delay', 'lang'),
			pricesrules_type::QTY			=> $obj->l('Quantity', 'lang'),
			pricesrules_type::HISTORY		=> $obj->l('History', 'lang')
		);

		self::$priceruleTypDescription = array(
			pricesrules_type::LENGTH 		=> $obj->l('Depend on reservation length', 'lang'),
			pricesrules_type::DATES			=> $obj->l('Apply on period between two dates', 'lang'),
			pricesrules_type::RECURRING		=> $obj->l('On specified days of the week and time slot', 'lang'),
			pricesrules_type::DELAY			=> $obj->l('Depend on delay to reservation start', 'lang'),
			pricesrules_type::QTY			=> $obj->l('For reservation quantity', 'lang'),
			pricesrules_type::HISTORY		=> $obj->l('For total reservation length', 'lang')
		);

		self::$lengthControl = array(
			reservation_length::FREE 		=> $obj->l('Unlimited', 'lang'),
			reservation_length::LIMIT		=> $obj->l('Limited in a range', 'lang'),
			reservation_length::FIXED		=> $obj->l('Fixed for all products', 'lang'),
			reservation_length::PRODUCT		=> $obj->l('Fixed for each product', 'lang'),
			reservation_length::NONE 		=> $obj->l('No length', 'lang'),
			reservation_length::OPTIONAL 	=> $obj->l('Optional length', 'lang'),
		);

		self::$reservationMode = array(
			reservation_mode::START_END		=> $obj->l('Start/End', 'lang'),
			reservation_mode::START_LENGTH	=> $obj->l('Start/length', 'lang'),
			reservation_mode::UNIQUE		=> $obj->l('Unique', 'lang'),
			reservation_mode::MULTIPLE		=> $obj->l('Multiple', 'lang')
		);

		self::$qtyControl = array(
			reservation_qty::PRODUCT 	=> $obj->l('Product quantity', 'lang'),
			reservation_qty::FIXED 		=> $obj->l('Fixed', 'lang'),
			reservation_qty::SEARCH		=> $obj->l('Search in attributes', 'lang')
		);

		self::$reservationInterval = array(

			reservation_interval::TIMESLOT 	=> $obj->l('A time slot', 'lang'),
			reservation_interval::TIMESLICE => $obj->l('A time slice', 'lang'),
			reservation_interval::DAY 		=> $obj->l('A day', 'lang'),
			reservation_interval::WEEK 		=> $obj->l('A week', 'lang'),
			reservation_interval::MONTH 	=> $obj->l('A month', 'lang')
		);
		//
		self::$reservationIntervalDescription = array(
			reservation_interval::TIMESLOT 	=> $obj->l('With a specific start and end time', 'lang'),
			reservation_interval::TIMESLICE => $obj->l('Generated time slices with given interval', 'lang'),
			reservation_interval::DAY 		=> $obj->l('End time must be later than start time', 'lang'),
			reservation_interval::WEEK 		=> $obj->l('Can be over 2 calendar weeks', 'lang'),
			reservation_interval::MONTH 	=> $obj->l('With a specific day for start and end', 'lang')
		);

		if (is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php")) {
			self::$productType = array(
				product_type::PRODUCT 		=> $obj->l('Real product', 'lang'),
				product_type::VIRTUAL 		=> $obj->l('Intangible product', 'lang'),
				product_type::STOCK 		=> $obj->l('Stock element', 'lang'),
				product_type::MUTUEL 		=> $obj->l('Mutual products', 'lang'),
				product_type::SHARED 		=> $obj->l('Shared product', 'lang'),
				product_type::AVAILABLE 	=> $obj->l('Variable product', 'lang'),
				product_type::PLACE		 	=> $obj->l('Product on place', 'lang'),
				product_type::RESOURCE		=> $obj->l('Product by resource', 'lang')
			);
		} else {
			self::$productType = array(
				product_type::PRODUCT 		=> $obj->l('Real product', 'lang'),
				product_type::VIRTUAL 		=> $obj->l('Intangible product', 'lang'),
				product_type::AVAILABLE 	=> $obj->l('Variable product', 'lang'),
				product_type::RESOURCE		=> $obj->l('Product by resource', 'lang')
			);
		}

		self::$productTypeDescription = array(
			product_type::PRODUCT 			=> $obj->l('Product quantity is used for max occupation', 'lang'),
			product_type::VIRTUAL 			=> $obj->l('Combination occupation is ignored', 'lang'),
			product_type::STOCK 			=> $obj->l('Each reservation is associated to a product element', 'lang'),
			product_type::MUTUEL 			=> $obj->l('Each product can handle multiple reservations', 'lang'),
			product_type::SHARED 			=> $obj->l('All products of selection share occupation', 'lang'),
			product_type::AVAILABLE 		=> $obj->l('Quantity depends on availabilities', 'lang'),
			product_type::PLACE				=> $obj->l('The occupation depends on selected place stock', 'lang'),
			product_type::RESOURCE			=> $obj->l('The occupation depends on resources availabilities', 'lang')
		);

		self::$reservationViews = array(
			reservation_view::DAY 			=> $obj->l('day', 'lang'),
			reservation_view::WEEK 			=> $obj->l('week', 'lang'),
			reservation_view::TWOWEEKS 		=> $obj->l('2 weeks', 'lang'),
			reservation_view::MONTH 		=> $obj->l('month', 'lang'),
			reservation_view::QUARTER 		=> $obj->l('3 months', 'lang'),
			reservation_view::YEAR 			=> $obj->l('year', 'lang'),
			reservation_view::MINIMAL 		=> $obj->l('dates', 'lang')
		);

		self::$reservTypes = array(
			reservation_start::SAME_DAY		=> $obj->l('The same day', 'lang'),
			reservation_start::AFTER_MINUT	=> $obj->l('After some minutes', 'lang'),
			reservation_start::AFTER_HOURS	=> $obj->l('After some hours', 'lang'),
			reservation_start::AFTER_DAYS	=> $obj->l('After some days', 'lang'),
			reservation_start::NEXT_WEEK	=> $obj->l('The next week', 'lang'),
			reservation_start::SOME_WEEKS	=> $obj->l('After some weeks', 'lang')
		);
		self::$reservSwitchesNext = array(
			reservation_view::DAY 			=> $obj->l('Next day', 'lang'),
			reservation_view::WEEK 			=> $obj->l('Next week', 'lang'),
			reservation_view::TWOWEEKS 		=> $obj->l('Next weeks', 'lang'),
			reservation_view::MONTH 		=> $obj->l('Next month', 'lang'),
			reservation_view::QUARTER 		=> $obj->l('Next months', 'lang'),
			reservation_view::YEAR 			=> $obj->l('Next year', 'lang'),
			reservation_view::MINIMAL 		=> ''
		);
		self::$reservSwitchesPrevious = array(
			reservation_view::DAY 			=> $obj->l('Previous day', 'lang'),
			reservation_view::WEEK 			=> $obj->l('Previous week', 'lang'),
			reservation_view::TWOWEEKS 		=> $obj->l('Previous weeks', 'lang'),
			reservation_view::MONTH 		=> $obj->l('Previous month', 'lang'),
			reservation_view::QUARTER 		=> $obj->l('Previous months', 'lang'),
			reservation_view::YEAR 			=> $obj->l('Previous year', 'lang'),
			reservation_view::MINIMAL 		=> ''
		);

		self::$virtualQuantity = array(
			occupy_combinations::IGNORE 	=> $obj->l('Ignore', 'lang'),
			occupy_combinations::SUM 		=> $obj->l('Check combinations sum', 'lang'),
			occupy_combinations::DEF	 	=> $obj->l('Check default combination', 'lang')
		);

		self::$stockAssignation = array(
			stock_assignation::NONE 		=> $obj->l('None', 'lang'),
			stock_assignation::AUTO 		=> $obj->l('Automatic (at validation)', 'lang'),

		);//stock_assignation::USER	 		=> $obj->l('By customer', 'lang')

		self::$reservPriceType = array(
			reservation_price::PRODUCT_DATE	=> $obj->l('Product price for reservation', 'lang'),
			reservation_price::PRODUCT_TIME	=> $obj->l('Product price by length', 'lang'),
			reservation_price::TABLE_TIME	=> $obj->l('Table Rates by length', 'lang'),
			reservation_price::TABLE_DATE	=> $obj->l('Table Rates by period', 'lang'),
			reservation_price::TABLE_RATE	=> $obj->l('Table Ratio by period and length', 'lang'),
		);

		self::$depositTypes = array(
			deposit_type::ONCE				=> $obj->l('Apply once', 'lang'),
			deposit_type::PRODUCT			=> $obj->l('Per product', 'lang'),
			deposit_type::QTY				=> $obj->l('Per quantity', 'lang'),
			deposit_type::RESA				=> $obj->l('Per reservation', 'lang'),
		);

		self::$availabilityTypes = array(
			availability_type::UNAVAILABLE 	=>	$obj->l('Unavailability', 'lang'),
			availability_type::AVAILABLE 	=>	$obj->l('Availability', 'lang'),
		);

		self::$reservationUnitSingle = array(
			reservation_unit::HOUR 	=>	$obj->l('hour', 'lang'),
			reservation_unit::TIMESLOT =>	$obj->l('slot', 'lang'),
			reservation_unit::MINUTE =>	$obj->l('min', 'lang'),
			reservation_unit::DAY 		=>	$obj->l('day', 'lang'),
			reservation_unit::WEEK 	=>	$obj->l('week', 'lang'),
			reservation_unit::MONTH 	=>	$obj->l('month', 'lang')
		);

		self::$reservationUnitMultiple = array(
			reservation_unit::HOUR 	=>	$obj->l('hours', 'lang'),
			reservation_unit::TIMESLOT =>	$obj->l('slots', 'lang'),
			reservation_unit::MINUTE =>	$obj->l('minutes', 'lang'),
			reservation_unit::DAY 		=>	$obj->l('days', 'lang'),
			reservation_unit::WEEK 	=>	$obj->l('weeks', 'lang'),
			reservation_unit::MONTH 	=>	$obj->l('months', 'lang')
		);
	}

	//Unit
	//-----------------------------------------------------------------------------------------------------------------

	public static function getUnit($unit, $plur=false) {
		if (!$plur) {
			if (array_key_exists($unit, self::$reservationUnitSingle))
				return self::$reservationUnitSingle[$unit];
		} else {
			if (array_key_exists($unit, self::$reservationUnitMultiple))
				return self::$reservationUnitMultiple[$unit];
		}
		return "?";
	}

	//Message
	//-----------------------------------------------------------------------------------------------------------------

	public static function getOperation($obj, $id, $male=true, $single=true) {
		global $cookie;
		$iso = strtolower(Language::getIsoById($cookie->id_lang));
		$out='';
		$out = self::$operations[$id];
		if (!$male && $iso=='fr') $out .= 'e';
		if (!$single && $iso=='fr') $out .= 's';
		return $out;
	}
	public static function getResult($obj, $res, $single=true) {
		if ($res) {
			if ($single) return $obj->l('has been successfully', 'lang');
			else return $obj->l('have been successfully', 'lang');
		} else {
			if ($single) return $obj->l('has not been successfully', 'lang');
			else return $obj->l('have not been successfully', 'lang');
		}
	}

	public static function getMessage($type, $id_lang, $notdefault=false) {
		$head='MYOWNRES_MSG_';
		$iso = strtolower(Language::getIsoById($id_lang));
		$var = 'msg_'.strtolower($type).'_'.$iso;
		$def = 'msg_'.strtolower($type).'_en';

		$msg=Configuration::get($head.$type, $id_lang);
		if ($msg!='') return $msg;
		else if (property_exists('myOwnLang', $var)) return myOwnLang::$$var;
		else if (property_exists('myOwnLang', $def)) return myOwnLang::$$def;
	}

	//Calendar
	//-----------------------------------------------------------------------------------------------------------------

	public static function getDaysNames($id_lang) {
		$iso = strtolower(Language::getIsoById($id_lang));
		$var='days'.ucfirst($iso);
		$def='daysEn';
		if (property_exists('myOwnLang', $var)) return myOwnLang::$$var;
		else if (property_exists('myOwnLang', $def)) return myOwnLang::$$def;
	}

	public static function getMonthsNames($id_lang) {
		$iso = strtolower(Language::getIsoById($id_lang));
		$var='months'.ucfirst($iso);
		$def='monthsEn';
		if (property_exists('myOwnLang', $var)) return myOwnLang::$$var;
		else if (property_exists('myOwnLang', $def)) return myOwnLang::$$def;
	}

	public static function getWeekString($obj, $i, $langue) {
		return $obj->l('W', 'pricesets').$i;
	}

}

?>
