<?php
/**
* 2010-2015 LaBulle
*
* NOTICE OF LICENSE
*
* This source file is subject to Terms and Conditions of Use 
* Available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade module to newer
* versions in the future. If you wish to customize module for your
* needs please refer to http://www.labulle.net for more information.
*
* @author    laBulle.net <info@labulle.net>
* @copyright 2010-2015 laBulle
* @license   http://www.labulle.net/download/CGV-en.pdf
*/

class MyOwnCalendar
	{
	public static $days_en = array(1 => 'Monday',2 => 'Tuesday',3 => 'Wednesday',4 => 'Thursday',5 => 'Friday',6 => 'Saturday',7 => 'Sunday');
	public static $days_fr = array(1 => 'Lundi',2 => 'Mardi',3 => 'Mercredi',4 => 'Jeudi',5 => 'Vendredi',6 => 'Samedi',7 => 'Dimanche');
	public static $days_es = array(1 => 'Lunes',2 => 'Martes',3 => 'Miércoles',4 => 'Jueves',5 => 'Viernes',6 => 'Sábado',7 => 'Domingo');
	public static $days_de = array(1 => 'Montag',2 => 'Dienstag',3 => 'Mittwoch',4 => 'Donnerstag',5 => 'Freitag',6 => 'Samstag',7 => 'Sonntag');
	public static $days_it = array(1 => 'Lunedì',2 => 'Martedì',3 => 'Mercoledì',4 => 'Giovedì',5 => 'Venerdì',6 => 'Sabato',7 => 'Domenica');
	public static $days_da = array(1 => 'Mandag',2 => 'Tirsdag',3 => 'Onsdag',4 => 'Torsdag',5 => 'Fredag',6 => 'Lørdag',7 => 'Søndag');
	public static $days_nl = array(1 => 'Maandag',2 => 'Dinsdag',3 => 'Woensdag',4 => 'Donderdag',5 => 'Vrijdag',6 => 'Zaterdag',7 => 'Zondag');
	public static $days_pt = array(1 => 'Segunda-feira',2 => 'Terça-feira',3 => 'Quarta-feira',4 => 'Quinta-feira',5 => 'Sexta-feira',6 => 'Sábado',7 => 'Domingo');
	public static $days_pl = array(1 => 'Poniedzialek',2 => 'Wtorek',3 => 'Sroda',4 => 'Czwartek',5 => 'Piatek',6 => 'Sobota',7 => 'Niedziela');
	public static $days_kp = array(1 => '월요일',2 => '화요일',3 => '수요일',4 => '목요일',5 => '금요일',6 => '토요일',7 => '일요일');
	public static $days_sv = array(1 => 'Måndag',2 => 'Tisdag',3 => 'Onsdag',4 => 'Torsdag',5 => 'Fredag',6 => 'Lördag',7 => 'Söndag');
	public static $days_et = array(1 => 'Esmaspäev',2 => 'Teisipäev',3 => 'Kolmapäev',4 => 'Neljapäev',5 => 'Reede',6 => 'Laupäev',7 => 'Pühapäev');
	public static $days_ru = array(1 => 'Понедельник',2 => 'Вторник',3 => 'Среда',4 => 'Четверг',5 => 'Пятница',6 => 'Суббота',7 => 'Воскресенье');
	public static $months_en = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
	public static $months_fr = array(1 => 'Janvier', 2 => 'Fevrier', 3 => 'Mars', 4 => 'Avril', 5 => 'Mai', 6 => 'Juin', 7 => 'Juillet', 8 => 'Aout', 9 => 'Septembre', 10 => 'Octobre', 11 => 'Novembre', 12 => 'Decembre');
	public static $months_es = array(1 => 'Enero', 2 => 'Febrero', 3 => 'Marzo', 4 => 'Abril', 5 => 'Mayo', 6 => 'Junio', 7 => 'Julio', 8 => 'Agosto', 9 => 'Septiembre', 10 => 'Octubre', 11 => 'Noviembre', 12 => 'Diciembre');
	public static $months_de = array(1 => 'Januar', 2 => 'Februar', 3 => 'März', 4 => 'April', 5 => 'Mai', 6 => 'Juni', 7 => 'Juli', 8 => 'August', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Dezember');
	public static $months_it = array(1 => 'Gennaio', 2 => 'Febbraio', 3 => 'Marzo', 4 => 'Aprile', 5 => 'Maggio', 6 => 'Giugno', 7 => 'Luglio', 8 => 'Agosto', 9 => 'Settembre', 10 => 'Ottobre', 11 => 'Novembre', 12 => 'Dicembre');
	public static $months_da = array(1 => 'Januar', 2 => 'Februar', 3 => 'Marts', 4 => 'April', 5 => 'Maj', 6 => 'Juni', 7 => 'Juli', 8 => 'August', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'December');
	public static $months_nl = array(1 => 'Januari', 2 => 'Februari', 3 => 'Maart', 4 => 'April', 5 => 'Mei', 6 => 'Juni', 7 => 'Juli', 8 => 'Augustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'December');
	public static $months_pt = array(1 => 'Janeiro', 2 => 'Fevereiro', 3 => 'Março', 4 => 'Abril', 5 => 'Maio', 6 => 'Junho', 7 => 'Julho', 8 => 'Agosto', 9 => 'Setembro', 10 => 'Outubro', 11 => 'Novembro', 12 => 'Dezembro');
	public static $months_kp = array(1 => '일월', 2 => '이월', 3 => '삼월', 4 => '사월', 5 => '오월', 6 => '유월', 7 => '칠월', 8 => '팔월', 9 => '구월', 10 => '시월', 11 => '십일월', 12 => '십이월');
	public static $months_pl = array(1 => 'Styczen', 2 => 'Luty', 3 => 'Marzec', 4 => 'Kwiecien', 5 => 'Maj', 6 => 'Czerwiec', 7 => 'Lipiec', 8 => 'Sierpien', 9 => 'Wrzesien', 10 => 'Pazdziernik', 11 => 'Listopad', 12 => 'Grudzien');
	public static $months_sv = array(1 => 'Januari', 2 => 'Februari', 3 => 'Mars', 4 => 'April', 5 => 'Maj', 6 => 'Juni', 7 => 'Juli', 8 => 'Augusti', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'December');
	public static $months_et = array(1 => 'Jaanuar', 2 => 'Veebruar', 3 => 'Märts', 4 => 'April', 5 => 'Mai', 6 => 'Juuni', 7 => 'Juuli', 8 => 'August', 9 => 'September', 10 => 'Oktoober', 11 => 'November', 12 => 'Detsember');
	public static $months_ru = array(1 => 'январь', 2 => 'февраль', 3 => 'март', 4 => 'апрель', 5 => 'может', 6 => 'июнь', 7 => 'июль', 8 => 'август', 9 => 'сентябрь', 10 => 'октябрь', 11 => 'ноябрь', 12 => 'декабрь');
	
	public static $months_short_en = array(1 => 'JAN', 2 => 'FEV', 3 => 'MAR', 4 => 'APR', 5 => 'MAY', 6 => 'JUN', 7 => 'JUL', 8 => 'AUG', 9 => 'SEP', 10 => 'OCT', 11 => 'NOV', 12 => 'DEC');
	public static $months_short_fr = array(1 => 'JAN', 2 => 'FÉV', 3 => 'MAR', 4 => 'AVR', 5 => 'MAI', 6 => 'JUN', 7 => 'JUL', 8 => 'AOÛ', 9 => 'SEP', 10 => 'OCT', 11 => 'NOV', 12 => 'DÉC');
	public static $months_short_es = array(1 => 'ENE', 2 => 'FEB', 3 => 'MAR', 4 => 'ABR', 5 => 'MAY', 6 => 'JUN', 7 => 'JUL', 8 => 'AGO', 9 => 'SEP', 10 => 'OCT', 11 => 'NOV', 12 => 'DIC');
	
	public static function getDayKey($date)
	{
		return date('Y-m-d', $date);
	}

	public static function getTimeListTime($day, $timestr)
    {
        return strtotime(date('Y-m-d', $day).' '.$timestr);
    }
        
	/*---------------------------------------------------------------------------------------------------------------
	Getting days dates from week
	-----------------------------------------------------------------------------------------------------------------*/
	public static function getDaysOfWeek($week, $year, $enable_week_end = true)
	{
		$day_list = array();
		if (Tools::strlen($week) == 1) $week = '0'.$week;
		$max = 5;
		if ($enable_week_end) $max = 7;
		$ref = strtotime($year.'W'.$week);
		if ((int)$week == 0)
			$ref = strtotime('-1 week', strtotime($year.'W01'));

		for ($i = 0; $i < $max; $i++)
			$day_list[$i] = strtotime('+'.$i.'day', $ref);

		return $day_list;
	}

	public static function getDaysOfWeekStr($yearweek, $enable_week_end = true)
	{
		$max = 5;
		$day_list = array();
		if ($enable_week_end) $max = 7;
		$ref = strtotime($yearweek);
		for ($i = 0; $i < $max; $i++)
			$day_list[$i] = strtotime('+'.$i.'day', $ref);

		return $day_list;
	}

	public static function getDaysOfWeekString($week, $year, $enable_week_end = true, $id_lang)
	{
		$days = self::getDaysOfWeek($week, $year, $enable_week_end);
		$days_str = array();
		foreach ($days as $day)
			$days_str[] = self::formatDateShort($day, $id_lang);
		return $days_str;
	}

	public static function sortbystarttime($a, $b)
	{
		if ($a->startTime == $b->startTime)
				return 0;
		return ($a->startTime < $b->startTime) ? - 1 : 1;
	}

	/*---------------------------------------------------------------------------------------------------------------
	Getting day from day number of week
	-----------------------------------------------------------------------------------------------------------------*/
	public static function getDayOfWeek($day_nb, $week, $year)
	{
		if (Tools::strlen($week) == 1)
			$week = '0'.$week;
		$ref = strtotime($year.'W'.$week);

		if ((int)$week == 0)
			$ref = strtotime('-1 week', strtotime($year.'W01'));
		return strtotime('+'.($day_nb - 1).'day', $ref);
	}

	public static function getDaysOfWeekTitles()
	{
		$ddays = array(1,2,3,4,5,6,7);
		return $ddays;
	}

	/*---------------------------------------------------------------------------------------------------------------
	Format week from timestamp
	-----------------------------------------------------------------------------------------------------------------*/
	public static function formatWeek($timestamp)
	{
		return date('W', $timestamp);
	}

	/*---------------------------------------------------------------------------------------------------------------
	Format time to hhHmm or hh:mm p/a.m
	-----------------------------------------------------------------------------------------------------------------*/
	public static function formatTime($time, $lang)
	{
		$iso = Tools::strtolower(Language::getIsoById($lang));
		if ($iso == 'fr')
			return (int)Tools::substr($time, 0, 2).'H'.Tools::substr($time, 3, 2);
		else
			return (int)Tools::substr($time, 0, 2).':'.Tools::substr($time, 3, 2);
	}

	public static function formatTimeFromDate($date, $lang)
	{
		return self::formatTime(date('H:i:s', $date), $lang);
	}

	public static function getHourPost($name)
	{
		$start_hour = Tools::getValue($name.'Hour');
		if (Tools::getValue($name.'Meridem') == 'PM')
		{
			if ((int)Tools::getValue($name.'Hour') == 12) $start_hour = 12;
			else $start_hour = (int)Tools::getValue($name.'Hour') + 12;
		}
		if (Tools::getValue($name.'Meridem') == 'AM')
		{
			if ((int)Tools::getValue($name.'Hour') == 12) $start_hour = '00';
			else $start_hour = Tools::getValue($name.'Hour');
		}
		return $start_hour.':'.Tools::getValue($name.'Minute').':00';
	}

	/*---------------------------------------------------------------------------------------------------------------
	Getting days names
	-----------------------------------------------------------------------------------------------------------------*/
	public static function getDaysString($langue)
	{
		$iso = Tools::strtolower(Language::getIsoById($langue));
		$var = 'days_'.$iso;
		if (property_exists('myOwnCalendar', $var))
			return self::$$var;
		else return self::$days_en;
	}

	/*---------------------------------------------------------------------------------------------------------------
	Getting days names
	-----------------------------------------------------------------------------------------------------------------*/
	public static function getDaysStringShort($langue)
	{
		$iso = Tools::strtolower(Language::getIsoById($langue));
		$var = 'days_'.$iso;
		if (property_exists('myOwnCalendar', $var))
			return Tools::substr(self::$$var, 0, 3);
		else return Tools::substr(self::$days_en, 0, 3);
	}

	/*---------------------------------------------------------------------------------------------------------------
	Getting day from timestamp
	-----------------------------------------------------------------------------------------------------------------*/
	public static function getDayString($date, $langue)
	{
		$day_id = date('N', $date);
		$days_names = self::getDaysString($langue);
		return $days_names[$day_id];
	}

	/*---------------------------------------------------------------------------------------------------------------
	Getting month name from month number
	-----------------------------------------------------------------------------------------------------------------*/
	public static function getMonthsString($langue)
	{
		$iso = Tools::strtolower(Language::getIsoById($langue));
		$var = 'months_'.$iso;
		if (property_exists('myOwnCalendar', $var))
			return self::$$var;
		else return self::$months_en;
	}

	/*---------------------------------------------------------------------------------------------------------------
	Getting day id
	-----------------------------------------------------------------------------------------------------------------*/
	public static function isDayAvailable($date, $days)
	{
		return ($days[date('N', $date)] == 1);
	}

	/*---------------------------------------------------------------------------------------------------------------
	Getting month name from month number
	-----------------------------------------------------------------------------------------------------------------*/
	public static function getMonthString($month_number, $langue)
	{
		$months = self::getMonthsString($langue);
		return $months[(int)$month_number];
	}

	/*---------------------------------------------------------------------------------------------------------------
	Format month year from timestamp
	-----------------------------------------------------------------------------------------------------------------*/
	public static function formatMonthAndYear($date, $langue)
	{
		$month_number = date('n', $date);
		$year_num = date('Y', $date);
		return self::getMonthString($month_number, $langue).' '.$year_num;
	}

	public static function isMonthStartInvisible($month, $year, $is_weekend_days)
	{
		if ($is_weekend_days)
			return false;
		else
		{
			$day = date('N', strtotime($year.'-'.$month.'-01'));
			if ($day == 6 || $day == 7)
					return true;
				return false;
		}
	}
	
	public static function getYear($date)
	{
		return date('Y', $date);
	}
	public static function getMonth($date)
	{
		return date('m', $date);
	}
	public static function getDay($date)
	{
		return date('j', $date);
	}
	/*---------------------------------------------------------------------------------------------------------------
	Format full date with day name from timestamp
	-----------------------------------------------------------------------------------------------------------------*/
	public static function formatDateWithDay($date, $langue)
	{
		$iso = Tools::strtolower(Language::getIsoById($langue));
		$day_id = date('N', $date);
		$day_number = date('j', $date);
		$month_number = date('n', $date);
		$year_num = date('Y', $date);
		if ($iso == 'en')
		{
			$ext = 'th';
			if ($day_number == 1 || $day_number == 21 || $day_number == 31) $ext = 'st';
			if ($day_number == 2 || $day_number == 22) $ext = 'nd';
			if ($day_number == 3 || $day_number == 23) $ext = 'rd';
			$day_str = self::$days_en[$day_id].' '.$day_number.$ext.' '.self::$months_en[$month_number].' '.$year_num;
		}
		elseif ($iso == 'de')
			$day_str = self::$days_de[$day_id].', '.$day_number.'. '.self::$months_de[$month_number].' '.$year_num;
		elseif ($iso == 'es')
		{
			$days_tab = self::getDaysString($langue);
			$day_str = self::$days_es[$day_id].' '.$day_number.' de '.self::$months_es[$month_number].' de '.$year_num;
		}
		else
		{
			$days_tab = self::getDaysString($langue);
			$day_str = $days_tab[$day_id].' '.$day_number.' '.self::getMonthString($month_number, $langue).' '.$year_num;
		}
		return $day_str;
	}

	/*---------------------------------------------------------------------------------------------------------------
	Format full date with day name from timestamp
	-----------------------------------------------------------------------------------------------------------------*/
	public static function formatDate($date, $langue)
	{
		$iso = Tools::strtolower(Language::getIsoById($langue));
		$day_number = date('j', $date);
		$month_number = date('n', $date);
		$year_num = date('Y', $date);
		if ($iso == 'en')
		{
			$ext = 'th';
			if ($day_number == 1 || $day_number == 21 || $day_number == 31) $ext = 'st';
			if ($day_number == 2 || $day_number == 22) $ext = 'nd';
			if ($day_number == 3 || $day_number == 23) $ext = 'rd';
			$day_str = $day_number.$ext.' '.self::$months_en[$month_number].' '.$year_num;
		}
		elseif ($iso == 'de')
			$day_str = $day_number.'. '.self::$months_de[$month_number].' '.$year_num;
		elseif ($iso == 'es')
			$day_str = $day_number.' de '.self::$months_es[$month_number].' de '.$year_num;
		else
			$day_str = $day_number.' '.self::getMonthString($month_number, $langue).' '.$year_num;

		return $day_str;
	}

	/*---------------------------------------------------------------------------------------------------------------
	Format short date from timestamp
	-----------------------------------------------------------------------------------------------------------------*/
	public static function formatDateShort($date, $langue)
	{
		$iso = Tools::strtolower(Language::getIsoById($langue));
		$sar = 'months_short_'.$iso;
		$day_number = date('j', $date);
		$month_number = date('n', $date);
		if ($iso == 'en')
		{
			$ext = 'th';
			if ($day_number == 1 || $day_number == 21 || $day_number == 31) $ext = 'st';
			if ($day_number == 2 || $day_number == 22) $ext = 'nd';
			if ($day_number == 3 || $day_number == 23) $ext = 'rd';
			/*if (property_exists('myOwnCalendar', $sar))
				$day_str = $day_number.$ext.' '.self::$$sar;
			else */
				$day_str = $day_number.$ext.' '.Tools::substr(self::getMonthString($month_number, $langue), 0, 3);
		} else {
			/*if (property_exists('myOwnCalendar', $sar))
				$day_str = $day_number.' '.self::$$sar;
			else */
				$day_str = $day_number.' '.Tools::substr(self::getMonthString($month_number, $langue), 0, 3);
		}
		return $day_str;
	}

	/*---------------------------------------------------------------------------------------------------------------
	Get weeks number from month
	-----------------------------------------------------------------------------------------------------------------*/
	public static function getWeeksOfMonth($month, $year)
	{
		$week_list = array();
		$wtemp = strtotime($year.'-'.$month.'-01');
		do
		{
			$week_list[date('o\WW', $wtemp)] = date('W', $wtemp);
			$wtemp = strtotime('+1week', $wtemp);

		} while ((int)date('m', $wtemp) == (int)$month);
		
		//we have to check if last day of month has been included
		$wtemp = strtotime($year.'-'.$month.'-01');
		$wtemp = strtotime('+1month', $wtemp);
		$wtemp = strtotime('-1day', $wtemp);

		if (!array_key_exists(date('o\WW', $wtemp), $week_list))
			$week_list[date('o\WW', $wtemp)] = date('W', $wtemp);
			
		return $week_list;
	}

	/*---------------------------------------------------------------------------------------------------------------
	count days in week before a date
	-----------------------------------------------------------------------------------------------------------------*/
	public static function countDaysInWeekBefore($limit)
	{
		$day_count = 0;
		$limit = strtotime(date('Y-m-d', $limit).' 00:00:00');
			$jours = self::getDaysOfWeek(date('W', $limit), date('Y', $limit));
		foreach ($jours as $jour)
			if ($jour < $limit)
				$day_count++;

		return $day_count;
	}

	/*---------------------------------------------------------------------------------------------------------------
	count days in week after a date
	-----------------------------------------------------------------------------------------------------------------*/
	public static function countDaysInWeekAfter($limit)
	{
		$day_count = 0;
		$limit = strtotime(date('Y-m-d', $limit).' 00:00:00');
			$jours = self::getDaysOfWeek(date('W', $limit), date('Y', $limit));
		foreach ($jours as $jour)
			if ($jour >= $limit)
				$day_count++;

		return $day_count;
	}

	/*---------------------------------------------------------------------------------------------------------------
	date utils
	-----------------------------------------------------------------------------------------------------------------*/
	public static function getDateFrom($date, $desireddelay, $time_slots)
	{
		$workdelay = 0;
		$delay = 0;
		while ($workdelay < $desireddelay)
		{
			$delay++;
			$testdate = strtotime('+'.$delay.' days', $date);
			$tsenabled = false;
			if (count($time_slots) > 0)
				foreach ($time_slots as $time_slot)
					if ($time_slot->isEnableOnDay($testdate)) $tsenabled = true;
			if (count($time_slots) == 0) $tsenabled = true;

			if ($tsenabled) $workdelay++;
		}

		return strtotime('+'.$delay.' days', $date);
	}

	public function getNextAvailableDate($date, $time_slots)
	{
		$delay = 0;
		do
		{
			$testdate = strtotime('+'.$delay.' days', $date);
			$delay++;
			$tsenabled = false;
			if (count($time_slots) > 0)
				foreach ($time_slots as $time_slot)
					if ($time_slot->isEnableOnDay($testdate)) $tsenabled = true;
			if (count($time_slots) == 0) $tsenabled = true;

		} while (!$tsenabled);

		return $testdate;
	}
}

?>