<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnPlanning {
	var $sqlId;
	var $name;
	var $email;
	var $id_shop=0;
	
	//todo 
	var $_lengthControl;
	
	var $ids_categories;
	var $ids_products;
	var $ids_notifs = array();
	var $ids_options = array();
	var $ids_users = array();
	var $assignment = 0;
	
	var $category;
	var $reservationStartType;
	var $reservationStartParams;
	var $reservationPeriod;
	var $reservationLengthParams;
	var $_reservationUnit;
	var $_reservationMinLength;
	var $_reservationMaxLength;
	var $_reservationLengthFreq=0;
	var $_reservationLengthStrict=false;
	var $_reservationLengthCanExceed=false;
	var $_reservationLengthQuantity=false;
	var $_reservationBetweenShift=0;
	var $_allowResOnHolidays=false;
	var $_allowResOnRestDays=false;
	var $_allowResBetweenDays=true;
	var $_reservationLengthControl;
	var $_lengthHolidays;
	var $_lengthDisabledTimeSlots;
	var $__reservationStart=0;
	
	var $reservationSelType;
	var $reservationSelPlanning;
	var $reservationSelParams;
	var $_reservationSelDays;
	var $_reservationSelWeekStart;
	var $_reservationSelWeekEnd;
	var $_reservationSelMonthStart;
	var $_reservationSelMonthEnd;
	var $_reservationSelMultiple;
	var $_reservationSelMode;
	var $_reservationShowTime=true;
	var $_reservationStartTime='00:00';
	var $_reservationEndTime='00:00';
	var $_reservationSelExt=null;
	var $_reservationCutTime=false;
	var $_reservationTimeByDay=true;
	var $_timeSliceLength=15;
	
	var $_reservationQty=0;
	var $_reservationQtyArg;
	
	var $priceParams;
	var $_priceUnit;
	var $_priceUnitSameAsLength;
	var $_priceEndTimeSlot;
	var $_priceDisabledTimeSlots=true;
	var $_priceStrict;
	var $_pricePreview;
	var $_priceHolidays=true;
	var $_reservationPriceType;
	var $_priceDoubleIfLength=false;
	var $_priceCombination=false;
	var $_coefPerProduct=false;
	
	var $depositAdvanceParams;
	var $_depositCalc=0;
	var $_depositAmount=0;
	var $_advanceRate=100;
	var $_advanceAmount=0;
	var $_depositByProduct=0;
	
	var $productOccupyParams;
	var $_occupyQty=0;
	var $_occupyCombinations=0;
	var $_occupyMaxOrder=0;
	var $_occupyObj=null;
	var $_occupyStockAssign=0;
	
	var $qtyParams;
	var $_qtyType;
	var $_qtyCapacity;
	var $_qtyDisplay;
	var $_qtySearch;
	var $_qtyFixed=1;
	
	var $productType=0;
	
	var $timeslots;
	var $_timeslotsTab;

	
	var $availability;
	var $options;
	var $_optionIgnoreAttrStock;
	var $_optionAlsoSell;
	var $_timeslotsObj;
	var $_optionValidationStatus;
	
	public $_mergedFamilies = array();
	public $_mergedNames = array();
	
	public function getUnitLabel($obj, $langue, $plur=false) {
		if ($this->_reservationLengthControl == reservation_length::NONE || $this->_reservationLengthControl == reservation_length::OPTIONAL || $this->_reservationPriceType==reservation_price::PRODUCT_DATE) return ' ';
		$unitLabel=Configuration::get('MYOWNRES_PRICELABEL_'.$this->sqlId, $langue);
		if ($this->_reservationSelExt!=null) return $this->_reservationSelExt->getUnitLabel($obj, $plur);
		if ($unitLabel=="") $unitLabel=myOwnLang::getUnit($this->_priceUnit, $plur);
		return $unitLabel;
	}
	
	public function getStartLabel($id_lang) {
		$iso = strtolower(Language::getIsoById($id_lang));
		$var = 'lbl_start_'.$iso;
		$label = stripslashes(Configuration::get('MYOWNRES_STARTLABEL_'.$this->sqlId, $id_lang));
		if ($label=='' && property_exists('myOwnLang', $var))
			return myOwnLang::$$var;
		else return $label;
	}
	public function getStopLabel($id_lang) {
		$iso = strtolower(Language::getIsoById($id_lang));
		$var = 'lbl_stop_'.$iso;
		$label = stripslashes(Configuration::get('MYOWNRES_STOPLABEL_'.$this->sqlId, $id_lang));
		if ($label=='' && property_exists('myOwnLang', $var))
			return myOwnLang::$$var;
		else return $label;
	}
	
	public function getMinDayLength() {
		$unit = 1;
		if ($this->_reservationUnit==reservation_unit::WEEK) $unit = 7;
		if ($this->_reservationUnit==reservation_unit::MONTH) $unit = 30;
		return round($this->_reservationMinLength*$unit);
	}
	
	public function isTimeSelect() {
		return ($this->reservationSelType==reservation_interval::TIMESLOT || $this->reservationSelType==reservation_interval::TIMESLICE);
	}
	
	public function getProductQuantity($id_product, $id_product_attribute=0, $stocks=null, $availabilities=null, $id_object=0) {
		$productQty=0;
		switch ($this->productType) {

			case product_type::MUTUEL:
				$productQty = $this->_occupyQty;
				break;
			case product_type::VIRTUAL:
				//attributes count
				if ($this->_occupyCombinations == occupy_combinations::IGNORE)
					$productQty = -1;
				else if ($this->_occupyCombinations==occupy_combinations::SUM) {
					$sattributes = MyOwnReservationsUtils::getProductAttributes($id_product);
					foreach($sattributes as $sattribute)
						$productQty += MyOwnReservationsUtils::getProductAttrQuantity($id_product, $sattribute);
							
				} else if ($this->_occupyCombinations==occupy_combinations::DEF) {
					$defAttr = MyOwnReservationsUtils::getDefaultAttribute($id_product);
					$productQty = MyOwnReservationsUtils::getProductAttrQuantity($id_product, $defAttr);
				}
				break;
			case product_type::AVAILABLE:
				$productQty=0;
				//we receive availabities from resa when checking
				if ($availabilities!=null) {
					foreach ($availabilities->list as $availability)
						if ($availability->quantity) 
							if ($availability->id_product==$id_product || $availability->id_product==0 || $id_product==0) {
								$productQty = $availability->quantity;
							}
				} else $productQty = -1;
				break;
			case product_type::PLACE:
				$productQty=0;
				if (Tools::getIsset('place'))
					$id_place = (int)Tools::getValue('place');
				else $id_place = $id_object;
				if ($stocks!=null) {
					$productQty = $stocks->countStockForProduct(-$id_place, 0);
					if ($productQty!=0) break;	
				}
				break;
			case product_type::STOCK:
				//check fisrt module stock
				if ($stocks!=null) {
					$productQty = $stocks->countVirtualForProduct($id_product);
					if ($productQty==0) $productQty = $stocks->countStockForProduct($id_product, ($this->_optionIgnoreAttrStock ? 0 : $id_product_attribute));
					if ($productQty>0) break;	
				}
				
			case product_type::SHARED:
				//same as product
			case product_type::PRODUCT:
				//same as default
			case product_type::RESOURCE:
				//same as default
				if ($this->_occupyObj!=null && $availabilities!=null) {
					$resourceQty=0;$resourcesavailables=array();
					$productQty = MyOwnReservationsUtils::getProductAttrQuantity($id_product, $id_product_attribute);
					$resources = $this->_occupyObj->getFamilyOptions($this);
					$occupyValue = $this->_occupyObj->getConfig($this->sqlId, resources::CONFIG_OCCUPATION, 0);
					foreach ($availabilities->list as $availability) {
						if ($resources!=array() && $availability->id_product<0 && in_array(-$availability->id_product, $resources) && !in_array(-$availability->id_product, $resourcesavailables)) {
							$resourcesavailables[]=-$availability->id_product;
							if ($occupyValue==0)
								$resourceQty += $productQty;
							else $resourceQty += 1;
						}
					}
					$productQty = $resourceQty;
					break;
				}
			default:
				//if no stock in PS conf
				if (Configuration::get('PS_ORDER_OUT_OF_STOCK')==1 or Configuration::get('PS_STOCK_MANAGEMENT')==0)
					$productQty = -1;
				else $productQty = MyOwnReservationsUtils::getProductAttrQuantity($id_product, $id_product_attribute);
				break;
		}
		return $productQty;
	}
	
	public function getTimeslots($full=false) {
  		$_timeSlots = new myOwnTimeSlots();
  		$ressourceTimeSlots = array();
  		$ressourceFullTimeSlots = array();
  		
		$defaultTimeSlot = myOwnTimeSlots::getDefault();
  			
		if (is_array($this->_timeslotsTab))
			if(count($_timeSlots->list)>0)
              foreach($_timeSlots->list AS $timeSlot) {
                  if (in_array($timeSlot->sqlId, $this->_timeslotsTab) or $timeSlot->id_family==$this->sqlId) 
                      $ressourceFullTimeSlots[$timeSlot->sqlId] =$timeSlot;
              }
        if ($this->sqlId==0 && $this->reservationSelType!=reservation_interval::TIMESLICE) $ressourceFullTimeSlots=$_timeSlots->list;
        if ($full && $this->reservationSelType!=reservation_interval::TIMESLICE) return $ressourceFullTimeSlots;
        
        //if ($this->_reservationSelExt!=null) return $this->_reservationSelExt->getTimeslots($this);

		if ($this->reservationSelType==reservation_interval::TIMESLOT) {
			if (count($ressourceFullTimeSlots)==0) {
				$defaultTimeSlot->days = array(1=>1,2=>1,3=>1,4=>1,5=>1,6=>1,7=>1);
				$defaultTimeSlot->_generated=true;
				$ressourceTimeSlots[0] = $defaultTimeSlot;
			} else $ressourceTimeSlots = $ressourceFullTimeSlots;
		}
		if ($this->reservationSelType==reservation_interval::TIMESLICE) {
			$defaultDbDays=explode(",",$this->_reservationSelDays);
			$defaultDays=array();
			foreach ($defaultDbDays as $key => $defaultDbDay)
				if ($key>0) $defaultDays[$key]=$defaultDbDay;
						
			$startTimeTab = explode(':', $this->_reservationStartTime);
			$sendTimeTab = explode(':', $this->_reservationEndTime);
			$startTimeMinutes = (int)$startTimeTab[0]*60+(int)$startTimeTab[1];
			$endTimeMinutes = (int)$sendTimeTab[0]*60+(int)$sendTimeTab[1];
			if ($endTimeMinutes == 0)
				$endTimeMinutes = 1440;
			if ($startTimeMinutes < $endTimeMinutes) {
				for ($minutes=$startTimeMinutes; $minutes < $endTimeMinutes; $minutes+=$this->_timeSliceLength) {
					$start_minute = $minutes % 60;
					$start_hour = ($minutes-$start_minute) / 60;
					$end_minute = ($minutes+$this->_timeSliceLength) % 60;
					$end_hour = ($minutes+$this->_timeSliceLength-$end_minute) / 60;
					
					$tmpTimeSlot = clone $defaultTimeSlot;
					$tmpTimeSlot->sqlId = -$minutes;
					$tmpTimeSlot->days = $defaultDays;
					$tmpTimeSlot->startTime = str_pad($start_hour, 2, "0", STR_PAD_LEFT).':'.str_pad($start_minute, 2, "0", STR_PAD_LEFT).":00";
					$tmpTimeSlot->name = $tmpTimeSlot->startTime;
					$tmpTimeSlot->endTime = str_pad($end_hour, 2, "0", STR_PAD_LEFT).':'.str_pad($end_minute, 2, "0", STR_PAD_LEFT).":00";
					$tmpTimeSlot->_generated=true;
					$ressourceTimeSlots[-$minutes] = $tmpTimeSlot;
				}
			}
		}
		if ($this->reservationSelType==reservation_interval::DAY || $this->reservationSelType==reservation_interval::NIGHT) {
			$defaultDbDays=explode(",",$this->_reservationSelDays);
			$defaultDays=array();
			foreach ($defaultDbDays as $key => $defaultDbDay)
				if ($key>0) $defaultDays[$key]=$defaultDbDay;
			$defaultTimeSlot->days = $defaultDays;
			$defaultTimeSlot->startTime = $this->_reservationStartTime.":00";
			$defaultTimeSlot->endTime = $this->_reservationEndTime.":00";
			$defaultTimeSlot->_generated=true;
			$ressourceTimeSlots[0] = $defaultTimeSlot;
		}
		if ($this->reservationSelType==reservation_interval::WEEK) {
			$defaultTimeSlot->days = array(1=>0,2=>0,3=>0,4=>0,5=>0,6=>0,7=>0);
			$defaultTimeSlot2 = clone $defaultTimeSlot;
			$defaultTimeSlot->days[$this->_reservationSelWeekStart]=1;
			$defaultTimeSlot->type=1;
			$defaultTimeSlot->_generated=true;
			$defaultTimeSlot2->days[$this->_reservationSelWeekEnd]=1;
			$defaultTimeSlot2->type=2;
			$defaultTimeSlot2->_generated=true;
			$ressourceTimeSlots[0] = $defaultTimeSlot;
			$ressourceTimeSlots[1] = $defaultTimeSlot2;
			if (MYOWNRES_WEEK_TS) {
				foreach ($ressourceFullTimeSlots as $ressourceFullTimeSlot)
					if (Tools::getValue('place', 0)==0 || $ressourceFullTimeSlot->id_object==0 || Tools::getValue('place', 0)==0 || $ressourceFullTimeSlot->id_object==Tools::getValue('place')) {
						$defaultTimeSlot = clone $ressourceFullTimeSlot;
						$defaultTimeSlot2 = clone $ressourceFullTimeSlot;
						$defaultTimeSlot->days = array(1=>0,2=>0,3=>0,4=>0,5=>0,6=>0,7=>0);
						$defaultTimeSlot->days[$this->_reservationSelWeekStart]=1;
						$defaultTimeSlot->type=1;
						$defaultTimeSlot->days[$this->_reservationSelWeekEnd]=1;
						//$defaultTimeSlot2->type=2;
						$ressourceTimeSlots[$ressourceFullTimeSlot->sqlId] = $defaultTimeSlot;
					//$ressourceTimeSlots[1] = $defaultTimeSlot2;
					}
			}
		}
		if ($this->reservationSelType==reservation_interval::MONTH) {
			$defaultTimeSlot->days = array(1=>1,2=>1,3=>1,4=>1,5=>1,6=>1,7=>1);
			$ressourceTimeSlots[0] = $defaultTimeSlot;
		}
              
        return $ressourceTimeSlots;
	}
		
	//Get reservation date
	//-----------------------------------------------------------------------------------------------------------------
	public function getNearesetTimeSlot($reservationStart, $current=false) {
			$currentTimeSlot = $this->_timeslotsObj->getTimeSlotOfTime($reservationStart);
			if ($currentTimeSlot!=null) { // && intval($currentTimeSlot->sqlId) >0
				if ($current)
					$reservationStart = $currentTimeSlot->getStartHourTime($reservationStart)-1;
				else {
					//echo date('Y-m-d H:i:s', $reservationStart);
					$reservationStart = $currentTimeSlot->getEndHourTime($reservationStart);
					/*$currentTimeSlot = $this->getNearesetTimeSlot($reservationStart, true);
					echo date('Y-m-d H:i:s', $reservationStart);*/
				}
			}
			return $reservationStart;
	}
	
	public function getReservationStart() {
		if ($this->__reservationStart>0)
			return $this->__reservationStart;
		$today_time = time();//strtotime("+0 day");
		$today_Week=date('W',$today_time);
		$today_Day=date('d',$today_time);
		$today_Year=date('Y',$today_time);
		$today_Month=date('m',$today_time);
		$reservationStartParamsTab = explode(';', $this->reservationStartParams);
		switch ($this->reservationStartType) { 
				case reservation_start::SAME_DAY: 
				$reservationStart = $this->getNearesetTimeSlot(time(), $reservationStartParamsTab[0]);
				break;
			case reservation_start::AFTER_MINUT: //After some days
				$reservationStart = $this->getNearesetTimeSlot(strtotime('+'.$reservationStartParamsTab[0].' minutes', time()), $reservationStartParamsTab[1]);
				break;
			case reservation_start::AFTER_HOURS: //After some days
				$reservationStart = $this->getNearesetTimeSlot(strtotime('+'.$reservationStartParamsTab[0].' hours', time()), $reservationStartParamsTab[1]);
				break;
			case reservation_start::AFTER_DAYS: //After some days
				$reservationStart = $this->getNearesetTimeSlot(strtotime('+'.$this->reservationStartParams.' days', time()));
				break;
			case reservation_start::NEXT_WEEK: //The next week
				//week begin management
				$resaParams = explode(';',$this->reservationStartParams);
				$resaStartDay = $resaParams[0];
				$resaStartTime = $resaParams[1];
				$startDay = myOwnCalendarWeek::getDayOfWeek($resaStartDay,$today_Week, $today_Year);
				$startTime = strtotime(date('Y-m-d',$startDay)." ".$resaStartTime);
				$afterStartTime=0;
				if ($today_time>$startTime) $afterStartTime=1;
				$reservationStart = strtotime("+".$afterStartTime." week",$startTime);
				break;
			case reservation_start::SOME_WEEKS: //After some weeks
				$reservationStartTemp = strtotime(date('Y-m-d').' 00:00:00');
				$reservationStart = strtotime('+'.$this->reservationStartParams.' week', $reservationStartTemp);
				break;
		}
		return $reservationStart;
	}
	
	public  function getPeriodUnits($cookie, $obj) {
		$periods = array();
		if ($this->_priceUnit!='') $priceUnit=$this->_priceUnit;
		else $priceUnit = $this->_reservationUnit;
		switch ($priceUnit) {
       		case reservation_interval::TIMESLOT: 
       			foreach ($this->_timeslotsObj->list as $timeslot)
       				$periods[$timeslot->sqlId] = $timeslot->name;
      			break;
      		case reservation_interval::TIMESLICE: 
       			foreach ($this->_timeslotsObj->list as $timeslot)
       				$periods[$timeslot->sqlId] = $timeslot->name;
      			break;
      		case reservation_interval::WEEK:
      			for ($i=1; $i<53; $i++)
      				$periods[$i] = myOwnLang::getWeekString($obj, $i, $cookie->id_lang);
      			break;
      		case reservation_interval::MONTH:
      			for ($i=1; $i<13; $i++)
      				$periods[$i] = MyOwnCalendarTool::getMonthString($i,$cookie->id_lang);
      			break;
      		case reservation_interval::DAY:
	  		default:
	  			if (MYOWNRES_PRICESET_BYWEEK)
      				$periods = myOwnLang::getDaysNames($cookie->id_lang);
      			else {
      				//myOwnLang::getUnit(reservation_unit::DAY, false).' '.$i.' ('.
	      			for ($i=1; $i<366; $i++)
      					$periods[($i-1)] = MyOwnCalendarTool::formatDateShort(strtotime('+'.($i-1).'days',strtotime(date('Y').'-01-01')), $cookie->id_lang);
      			}
      			break;
      	}
      	return $periods;
	}
	
	public function getTimeUnits($cookie, $obj, $id_product=0) {
		$lengths = array();
		if ($this->_reservationLengthControl == reservation_length::PRODUCT || $this->_reservationLengthControl == reservation_length::COMBINATION) {
			if ($this->_reservationLengthControl == reservation_length::PRODUCT || $id_product==0) $attributes=array(0);
			else $attributes = MyOwnReservationsUtils::getProductAttributes($id_product);
			$tmplengths=array();
			foreach($attributes as $attribute) {
				if ($attribute>0) $length = Configuration::get('MYOWNRES_RES_LENGTH_'.$id_product.'-'.$attribute);
				else $length = Configuration::get('MYOWNRES_RES_LENGTH_'.$id_product);
				$tmplengths[] = $length;
			}
			for ($i=1; $i<=max($tmplengths); $i++)
				$lengths[$i] = $i;
		} else {
			for ($i=1; $i<=$this->_reservationMaxLength; $i++)
				$lengths[$i] = $i;//." ".$this->getUnitLabel($obj,$cookie->id_lang, $i>0);
		}
		//$this->_reservationMinLength
		
      	return $lengths;
	}
		
	public function getProducts($id_lang) {
		$id_shop = myOwnUtils::getShop();
		$products=array();

	    $categories=array();
	    foreach(explode(',',$this->ids_categories) as $category)
	    	if (intval($category)>0)
	    		$categories += MyOwnReservationsUtils::getAllSubCategoriesId($category);
	    //if (count($categories)==0) return array(); removed because we can have products withoutc cat
	    
	    if (count($categories)>0) {
		    $categoriesStr=implode(',',$categories);
			$query = '
			SELECT *
			FROM `'._DB_PREFIX_.'category_product` cp
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON pl.`id_product` = cp.`id_product` '; //LEFT JOIN `'._DB_PREFIX_.'product` p ON p.`id_product` = cp.`id_product`
			if ($categoriesStr!='' && $this->ids_products!='') $query .= '
			WHERE (cp.`id_category` in ('.$categoriesStr.') OR pl.`id_product` in ('.$this->ids_products.')';
			else if ($this->ids_products!='') $query .= '
			WHERE (pl.`id_product` in ('.$this->ids_products.')';
			else $query .= '
			WHERE (cp.`id_category` in ('.$categoriesStr.')';
			$query .= '
				   ) AND pl.`id_lang` = '.$id_lang;

			$results = Db::getInstance()->ExecuteS($query);
			foreach($results as $result)
				$products[intval($result["id_product"])] = $result; 
				
		}
		
		//products selection
		$prods=explode(',',$this->ids_products);
		
		if (implode(',', array_filter($prods))!='') {
			$query = '
			SELECT *
			FROM `'._DB_PREFIX_.'product_lang` pl
			WHERE pl.`id_product` IN ('.implode(',', array_filter($prods)).') AND pl.`id_lang` = '.$id_lang;
			
			$results = Db::getInstance()->ExecuteS($query);
			foreach($results as $result)
				$products[intval($result["id_product"])] = $result; 
		}
		return $products;
	}
	
	public function getProductsId($catFilter=0)  {
		$productIds=array();
		//by categories
	   	$categories=array();

	    foreach(explode(',',$this->ids_categories) as $category)
	    	if (intval($category)>0 && ($catFilter==0 or $catFilter==$category))
	    		$categories += MyOwnReservationsUtils::getAllSubCategoriesId($category);
		
	    if (count($categories)!=0) {
		    $categoriesStr=implode(',',$categories);
			$query = '
				SELECT id_product 
				FROM `'._DB_PREFIX_.'category_product`
				WHERE `id_category` in ('.$categoriesStr.');';
			$products = Db::getInstance()->ExecuteS($query);
			foreach ($products as $product) 
				$productIds[(int)$product['id_product']]=(int)$product['id_product'];
		}

		//by products
		if (trim($this->ids_products)!='') {
			$query = '
				SELECT id_product 
				FROM `'._DB_PREFIX_.'product` pl
				WHERE pl.`id_product` in ('.$this->ids_products.')';
			$products = Db::getInstance()->ExecuteS($query);
			foreach ($products as $product) 
				$productIds[(int)$product['id_product']]=(int)$product['id_product'];
		}

		return $productIds;
	}
	
	public function getProductsQuantity() {
		$productQty=0;
		$products=$this->getProductsId();
		foreach($products as $productId) {
			$attributes = MyOwnReservationsUtils::getProductAttributes($productId);
			if (count($attributes))
				foreach($attributes as $attribute)
					$productQty += MyOwnReservationsUtils::getProductAttrQuantity($productId, $attribute);
			else $productQty += MyOwnReservationsUtils::getProductAttrQuantity($productId, 0);
		} 
		return $productQty;
	}
	
	public function getProductsCount() {
		$productCnt=0;
		$products=$this->getProductsId();
		foreach($products as $productId) {
			$productCnt++;
		} 
		return $productCnt;
	}
	
	public function searchProductQtyInAttribute($id_product, $id_product_attribute, $id_lang) {
		$attrs = MyOwnReservationsUtils::getAttributeCombinaisons($id_product, $id_product_attribute, $id_lang, false);

		foreach($attrs as $attr) 
			if ($attr["id_attribute_group"]==$this->_qtySearch) {
				$qtytmp=0;
				$attstabs = explode(' ', $attr["attribute_name"]);
				foreach ($attstabs as $attstab)
					$qtytmp+=intval($attstab);

				return $qtytmp;
			}
		return 1;
	}
	
	public function getDesiredShift($estimateCart) {
	
		$productLength=0;
		$desiredShift = $estimateCart->_mainProduct->_reservationMinLength;
		if ($estimateCart->_mainProduct->_reservationLengthControl == reservation_length::PRODUCT)
		     $productLength = intval(Configuration::get('MYOWNRES_RES_LENGTH_'.$estimateCart->id_product, 0));
		if ($estimateCart->_mainProduct->_reservationLengthControl == reservation_length::COMBINATION)
		     $productLength = intval(Configuration::get('MYOWNRES_RES_LENGTH_'.$estimateCart->id_product.'-'.$estimateCart->id_product_attribute, 0));
		if ($productLength) $desiredShift = $productLength;
		if ($estimateCart->_mainProduct->_reservationLengthQuantity)
			$desiredShift = $desiredShift * $estimateCart->quantity;
		return $desiredShift;
	}
	
	public function getShiftTimeSlot($startDate, $id_timeslot_start, &$desiredShift, $checkAvailable, $availabilities, &$endDate, &$id_timeslot_end, $id_product, $id_product_attribute, $reservationEnd=null) {
//echo '&'.$this->_reservationUnit.'='.($this->_reservationUnit==reservation_unit::TIMESLOT);
		$timeslots = $this->_timeslotsObj->list;
		$timeslot=null;
		if ($reservationEnd==null) $reservationEnd=strtotime("+999 days");
		//calcul date min
		$dayShift=0;
		$tsShift=0;
		$id_timeslot_end = -1;
		$afterStartTs=false;
		if ($desiredShift==1 && $this->isTimeSelect() && $this->_reservationUnit!=reservation_unit::DAY) {
			$endDate = $startDate;
			$id_timeslot_end = $id_timeslot_start;
			return 0;
		}

		do {
			$tempDate = strtotime("+".$dayShift."day",$startDate);

			foreach ($timeslots as $timeslot)
			if ($timeslot->id_object==0 || Tools::getValue('place', 0)==0 || $timeslot->id_object==Tools::getValue('place') ) {	

				if ($timeslot->sqlId == $id_timeslot_start or $timeslot->sqlId==0 or $id_timeslot_start==0) $afterStartTs=true; //!$isTsShift or 

				if ($afterStartTs) { 

					if ($id_product>0 && (
						   ($availabilities!=null && !$this->_allowResOnHolidays && !$availabilities->isAvailable($tempDate, $timeslot, $id_product, $id_product_attribute))
						or ($checkAvailable && !$this->_allowResOnRestDays && !$timeslot->isEnableOnDay($tempDate) ) ) )
						{
							
						$endDate = $tempDate;
						$id_timeslot_end = $timeslot->sqlId;
						$desiredShift=$dayShift;
						return -1;
					}
					$tsShift++;

					//echo 'getShiftTimeSlot:'.countWeeks(;
					//$this->_reservationUnit==reservation_unit::WEEK && $tempDate >= strtotime("+".$desiredShift."week",$startDate)
					
					if ( ( ($this->_reservationUnit==reservation_unit::MINUTE
								&& $tsShift >= $desiredShift) //greater only because of not resa min length enough
							or ($this->_reservationUnit==reservation_unit::TIMESLOT
								&& $tsShift >= $desiredShift)
																																		//removed $this->countTimeslots($startDate, $tempDate, $id_timeslot_start, $timeslot->sqlId) because double count take long time
							or ($this->_reservationUnit==reservation_unit::DAY
								&& $this->countDays($startDate, $tempDate, $id_timeslot_start, $timeslot->sqlId, $id_product, $id_product_attribute, $availabilities) >= $desiredShift)
							or ($this->_reservationUnit==reservation_unit::WEEK && $this->countWeeks($startDate, $tempDate) >= $desiredShift)
							or ($this->_reservationUnit==reservation_unit::MONTH && $tempDate >= strtotime("+".$desiredShift."month",$startDate)) )
						 && ($timeslot->isEnableOnDay($tempDate)) //removed or $this->_lengthDisabledTimeSlots because can't reserve a disabled ts
					    ) {
						$id_timeslot_end=$timeslot->sqlId;

						break 2;
					}
				}
			}
			if ($checkAvailable && (($this->reservationSelType==reservation_interval::TIMESLOT && !$this->_allowResBetweenDays))) { // OR $this->reservationSelType==reservation_interval::TIMESLICE
				break;
			}
			$dayShift++;
		} while ($id_timeslot_end==-1 && date('Ymd',$tempDate) < date('Ymd',$reservationEnd));

		if ($id_timeslot_end==-1) {
			$endDate = $tempDate;
			if ($timeslot!=null) $id_timeslot_end = $timeslot->sqlId;
			else $id_timeslot_end = 0;
			$desiredShift=$dayShift;
			return -2;
		}
		
		$endDate = $tempDate;
		return $dayShift;
	}
	
	public function getRevShiftTimeSlot(&$startDate, &$id_timeslot_start, $desiredShift, $reservationStart=null) {
		$timeslots = $this->_timeslotsObj->list;
		if ($desiredShift==0) return 1;
		$dayShift=0;
		$tsShift=0;
		$new_id_timeslot_start = -1;
		//$afterStartTs=false;
		$beforeStartTs=false;
		do {
			$tempDate = strtotime("-".$dayShift."day",$startDate);
			foreach ( array_reverse ($timeslots, true) as $timeslot) {
				//if ($timeslot->sqlId == $id_timeslot_start or $timeslot->sqlId==0 or $id_timeslot_start==0) $afterStartTs=true;
				//cho date('dm',$tempDate).' '.$timeslot->name.' '.intval($beforeStartTs).":";
				if ($timeslot->sqlId == $id_timeslot_start or $timeslot->sqlId==0 or $id_timeslot_start==0) $beforeStartTs=true;
				if ($beforeStartTs) {
					$tsShift++;

					if ( ( ($this->_reservationUnit==reservation_unit::TIMESLOT && $tsShift > $desiredShift)
						  or ($this->_reservationUnit==reservation_unit::DAY && ($dayShift) >= $desiredShift)
						  or ($this->_reservationUnit==reservation_unit::WEEK && $tempDate < strtotime("-".$desiredShift."week",$startDate))
						  or ($this->_reservationUnit==reservation_unit::MONTH && $tempDate <= strtotime("-".$desiredShift."month",$startDate)) )
						 && ($timeslot->isEnableOnDay($tempDate) or $this->_priceDisabledTimeSlots)
					    ) {

						$new_id_timeslot_start=$timeslot->sqlId;
						break 2;
					}
				}
				//cho $tsShift."<br/>";
				//$out.=date('Y-m-d',$tempDate).' '.$timeslot->isEnableOnDay($tempDate).'<br>';
			}
			$dayShift++;
		} while ($new_id_timeslot_start==-1 && (date('Ymd',$tempDate) >= date('Ymd',$reservationStart) or $reservationStart==null));
		if ($new_id_timeslot_start==-1) return -1;
		
		$startDate = $tempDate;
		$id_timeslot_start = $new_id_timeslot_start;
		return 1;
	}
	
	public function getLastTimeSlot(&$endDate, $startDate, $availabilities=null) {
		$timeslots = $this->_timeslotsObj->list;
		$dayShift=0;
		$tsShift=0;

		do {
			$tempDate = strtotime("-".$dayShift."day",$endDate);
			foreach ( array_reverse ($timeslots, true) as $timeslot) {
				if ($timeslot->type==0 or $timeslot->type==2)
					if (($availabilities!=null && !$this->_allowResOnHolidays && !$availabilities->isAvailable($tempDate, $timeslot))
						or (!$this->_allowResOnRestDays && !$timeslot->isEnableOnDay($tempDate)) ) {
							$endDate = $tempDate;
							return $timeslot;
					}
			}
			$dayShift++;
		} while ($tempDate >= $startDate);
		
		return null;
	}
	
	public function getFirstTimeSlot(&$startDate, $endDate, $availabilities=null) {
		$timeslots = $this->_timeslotsObj->list;
		$dayShift=0;
		$tsShift=0;
		
		if ($this->reservationSelType==reservation_interval::MONTH) {
			do {
				$tempDate = strtotime("+".$dayShift."month",$startDate);
				$day = myOwnCalendarMonth::getDay(date('m',$tempDate), date('Y',$tempDate), "start", $this->_reservationSelMonthStart);
				if ($day > $startDate && ($availabilities==null || $this->_allowResOnHolidays || $availabilities->isAvailable($day, $timeslots[0]))) {
					$startDate = $day;
					return $timeslots[0];
				}
				$dayShift++;
			} while ($tempDate <= $endDate);
		} else {
			do {
				$tempDate = strtotime("+".$dayShift."day",$startDate);
				foreach ($timeslots as $timeslot) {
					if ($timeslot->getStartHourTime($tempDate)>$startDate)
					if ($timeslot->type==0 or $timeslot->type==1)
						if (($availabilities==null || $this->_allowResOnHolidays || $availabilities->isAvailable($tempDate, $timeslot))
							&& ($this->_allowResOnRestDays || $timeslot->isEnableOnDay($tempDate)) ) {
								$startDate = $tempDate;
								return $timeslot;
						}
				}
				$dayShift++;
			} while ($tempDate <= $endDate);
		}
		
		return null;
	}
	
		
	//Count timeslots
	//-----------------------------------------------------------------------------------------------------------------
	
	public function countTimeslots($startday, $endday, $startts, $endts, $forPrice = false, $startLimit=0, $endLimit=0, $id_timeslot=-1, $days=array(), $id_product=0) {
		$timeslots = $this->_timeslotsObj->list;
		$daylength = -1;
		$tslength = 0;
		$afterstart=false;
		$dayorts = defined('MYOWNRES_PRICERULE_DAYORTS') && MYOWNRES_PRICERULE_DAYORTS;

		do {
			$daylength++;
			$checkLimit = strtotime("+".$daylength." day",$startday);
			if (is_array($timeslots))
			foreach($timeslots as $timeslot) 
			if (!$id_product || $timeslot->id_product==$id_product) {
				if ($timeslot->sqlId == $startts or $startts==0) $afterstart=true;
				if ($afterstart) {
					if ($this->_priceDisabledTimeSlots
						or $timeslot->isEnableOnDay($checkLimit)) {
						$isrightts = ($timeslot->sqlId == $id_timeslot or $id_timeslot==0);
						$isrightday = ($days==array() or $days[date("N",$checkLimit)]==1);
						
						if (	($id_timeslot==-1 or ($dayorts && $id_timeslot ? $isrightts || $isrightday : $isrightts && $isrightday))
							 && ($startLimit==0 or ($checkLimit >= $startday && $checkLimit <= $endday))
							) 
						$tslength++;
					}
				}
				if (date('Y-m-d',$checkLimit) == date('Y-m-d',$endday) && $timeslot->sqlId == $endts) break 2;
			}
		} while (date('Y-m-d',$checkLimit) != date('Y-m-d',$endday)
				 && date('Y-m-d',$checkLimit) < date('Y-m-d',$endday) && $daylength<1000 );
		
		if ($tslength>1 && $forPrice && $this->_priceEndTimeSlot) $tslength--;
		return $tslength;
	}
	
	public function countTimeslices($day, $startts, $endts, $forPrice = false, $startLimit=0, $endLimit=0, $timeslot=null, $days=array(), $id_product=0) {
		$start = -$startts;
		$end = -$endts;
		$dayorts = defined('MYOWNRES_PRICERULE_DAYORTS') && MYOWNRES_PRICERULE_DAYORTS;
		
		$isindaysofweek = $days[date("N",$day)]==1;
		if (!$dayorts && !$isindaysofweek) return 0;
		
		$weeks = array_filter(explode(',', $days[8]));
		$isinweeks = in_array(date("W",$day), $weeks || count($weeks)==0);
		if (!$dayorts && !$isinweeks) return 0;



		if ($end<$start) {
			$tmp = $start;
			$start = $end;
			$end = $tmp;
		}
		if ($timeslot!=null && (!$dayorts || (!$isinweeks && !$isindaysofweek))) {
			$stab=explode(':', $timeslot->startTime);
			$etab=explode(':', $timeslot->endTime);
			$minstart=(int)$stab[0]*60+(int)$stab[1];
			$maxend=(int)$etab[0]*60+(int)$etab[1];

			if ($start<$minstart) $start = $minstart;
			if (!$dayorts && $end<$minstart) return 0;
			if ($end>$maxend) $end = $maxend;
			if (!$dayorts && $start>$maxend) return 0;
		}
		$length=0;
		if (($end-$start) >= $this->_timeSliceLength)
			$length += (($end-$start) / $this->_timeSliceLength);
		
		return $length;
	}
	
	
	//Count Days
	//-----------------------------------------------------------------------------------------------------------------

	public function countDays($start, $end, $startTimeslot, $endTimeslot, $id_product=0, $id_product_attribute=0, $availabilities=null, $forPrice = false) {
		$timeslots = $this->_timeslotsObj->list;
		$countdisabled = (		($forPrice && $this->_priceDisabledTimeSlots)
							 or (!$forPrice && $this->_lengthDisabledTimeSlots)
							 or ($this->reservationSelType>=reservation_interval::WEEK));
		
		if (	($forPrice && !$this->_priceHolidays)
			or 	(!$forPrice && !$this->_lengthHolidays)
			or ($this->reservationSelType>=reservation_interval::WEEK)) {
			if ($availabilities != null) $_availabilities = $availabilities;
			else $_availabilities = null;//new myOwnAvailabilities($start, $end, $this, $id_product, $id_product_attribute);//myOwnAvailabilities::getInstance();
		} else $_availabilities = null;
// if ($countdisabled && $_availabilities == null) echo '@'.date('Y-m-d H:i:s ', $end).intval(($end-$start)/(60*60*24));
//  echo '#'.$forPrice.':'.intval ($countdisabled && $_availabilities == null) ;

		if ($countdisabled && $_availabilities == null) $count = intval(round(($end-$start)/(60*60*24)));
		else $count = $this->countEachDays($start, $end, $countdisabled, $_availabilities, $id_product, $id_product_attribute);
		//echo '<br>'.date('Y-m-d',$end).' f'.$forPrice.' c'.($countdisabled && $_availabilities == null).' :'.(($end-$start)).':'.(($end-$start)/(60*60*24)).':'.round(($end-$start)/(60*60*24)).':'.intval(round(($end-$start)/(60*60*24))).':'.$count;
		
		//when for price and availabilities need to count each day so we have to remove one when start>end because first day is next day start in this case
		if (!($countdisabled && $_availabilities == null) && $this->_reservationStartTime > $this->_reservationEndTime) $count--;
		
		//when for price and counting diff need to add a day when start and end time =00 or < 
		if (($countdisabled && $_availabilities == null) && $this->_reservationStartTime <= $this->_reservationEndTime) $count++;
		
		if ($this->_priceStrict && $forPrice) {
			if (MyOwnCalendarTool::isEndEarlierThanStart($timeslots, $startTimeslot, $endTimeslot)) $count--;
			else if ($count>1 && $forPrice && $this->_priceEndTimeSlot && $startTimeslot==$endTimeslot) $count--;
// 			else if ($forPrice && $startTimeslot==$endTimeslot && $startTimeslot==0) $count++; //not enough need to check time
		} else if ($count>1 && $forPrice && $this->_priceEndTimeSlot && MyOwnCalendarTool::getFirstTimeSlot($timeslots, $end)==$endTimeslot) $count--;

 //		echo '<br/>_'.$forPrice.'_'.$count.'_'.intval($_availabilities==null).'_'.date('Y-m-d H:i:s ', $start).'_'.date('Y-m-d H:i:s ', $end);
		return $count;
	}
	
	public function countEachDays($start, $end, $countdisabled=true, $availabilities = null, $id_product=0, $id_product_attribute=0) {
		$length = -1;
		$dayLength = 0;
// echo 'countEachDays';
		do {
			$length++;
			$checkLimit = strtotime("+".$length." day",$start);
// 			if ($checkLimit < $end or ($start==$end && $checkLimit <= $end) ) {
				if ($this->_timeslotsObj->areEnableOnDay($checkLimit)) {
					
					if ($availabilities == null or $availabilities->isAvailable($checkLimit, 0, $id_product, $id_product_attribute))
						$dayLength++;
				} elseif ($countdisabled)  $dayLength++;
// 				echo '|'.date('Y-m-d', $checkLimit).':'.$dayLength;
// 			}
		} while ($checkLimit < $end); //removed date('Y-m-d', for perf

		return $dayLength;
	}
	
	//Count Weeks
	//-----------------------------------------------------------------------------------------------------------------
	public function countWeeks($start, $end, $forPrice = false) {
		if (!$this->_priceStrict or !$forPrice) {
			
			$count = $this->countEachDays($start, $end, true);
			
			if ($this->reservationSelType==reservation_unit::WEEK && ($this->_reservationSelWeekEnd == $this->_reservationSelWeekStart OR MYOWNRES_WEEK_TS)) $count--;
			return ceil($count/7);
		} else return MyOwnCalendarTool::countEachWeeks($start, $end);
	}
}