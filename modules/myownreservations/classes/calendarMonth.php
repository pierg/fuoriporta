<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnCalendarMonth {
	public $year;
	public $month;
	public $_indexDays;
	public $_dayOfWeek;
	public $_dayOfMonth=null;
	public $_weeks;
	public $_filteredDays=array();
	
	public function __construct($month, $year, $dayofweek=null, $dayofmonth=null)
	{
		$this->month = $month;
		$this->year = $year;
		$this->_dayOfWeek = $dayofweek;
		$this->_dayOfMonth = $dayofmonth;
	}
	
	public function getStart() {
		return strtotime($this->year."-".$this->month."-01");
	}

	public function getSelection() {
		return $this->_dayOfMonth;
	}
	
	public function getDaySelection() {
		return $this->_filteredDays[date('Y-m-d', $this->getSelection())];
	}

	public function getEnd() {
		return strtotime("+1month -1day",$this->getStart());
	}
	
	//Get weeks number from month
	//-----------------------------------------------------------------------------------------------------------------
	public function getWeeks() {
		$this->_weeks = array();
		$weeks = MyOwnCalendarTool::getWeeksOfMonth($this->month, $this->year);

		foreach ($weeks as $weekKey => $weekVal) {
			$weekKeyTab = explode("W", $weekKey);
			$tempWeek = new myOwnCalendarWeek($weekVal, $weekKeyTab[0], $this->_dayOfWeek);
			if ($this->_dayOfWeek == null or $this->month==date("m",$tempWeek->getDay($this->_dayOfWeek)))
				$this->_weeks[$weekVal] = $tempWeek;
		}

		return $this->_weeks;
	}
	
	public function isStartInvisible($isWeekEndDays) {
		if ($isWeekEndDays) {
			return false;
		} else {
  			$day = date('N',strtotime($this->year.'-'.$this->month.'-01'));
  			if ($day==6 or $day==7)
        		return true;
        	return false;
		}
	}
	
	public function isDayIn($day) {
		return (date('Y',$day)==$this->year
				&& intval(date('m',$day))==intval($this->month));
	}
	
	public function toString($id_lang, $otherMonth=null) {
		if ($otherMonth!=null) return MyOwnCalendarTool::getMonthString($this->month,$id_lang).' - '.MyOwnCalendarTool::getMonthString($otherMonth->month,$id_lang).' '.$this->year;
		else return MyOwnCalendarTool::getMonthString($this->month,$id_lang).' '.$this->year;
	}
	
	public static function getDay($month, $year, $selType, $startDay) {
		if ($startDay=="0") {
			$refMin = strtotime($year."-".$month."-01");
			if ($selType=="end") return strtotime("+1month -1day",$refMin);
			else return $refMin;
		}
		if (stripos($startDay, "d")!==false) {
			$day = str_ireplace ("d", "", $startDay);
			if (strlen($day)==1) $day = "0".$day;
			return strtotime($year."-".$month."-".$day);
		}
		if (stripos($startDay, "w")!==false) {
			$dayofweek = str_ireplace ("w", "", $startDay);
			$refMin = strtotime($year."-".$month."-01");
			$refMax = strtotime("+1month -1day",$refMin);
			for($i=1; $i<8; $i++) {
				if ($selType=="end") $day=strtotime("-".($i-1)."day",$refMax);
				else $day=strtotime($year."-".$month."-0".$i);
				if (date('N',$day)==$dayofweek) return $day;
			}
		}
	}
	
	public function getDays() {
		$days=array();
		$cnt=0;
		for ($i = 1; $i <= 31; $i++) {
			$day = strtotime($this->year."-".$this->month."-".$i);
			if (date('Y',$day)==$this->year && intval(date('m',$day))==intval($this->month))
				$days[$i]=$day;
		}
		return $days;
	}
	
	public function getDaysObj() {
		$days=array();
		$cnt=0;
		for ($i = 1; $i <= 31; $i++) {
			$day = strtotime($this->year."-".$this->month."-".$i);
			if (date('Y',$day)==$this->year && intval(date('m',$day))==intval($this->month))
				$days[$i]=new myOwnCalendarDay($day);
		}
		return $days;
	}
	
			
	//Getting months
	//-----------------------------------------------------------------------------------------------------------------
	public static function getOnPeriod($start, $end, $dayofweek=null, $dayofmonth=null) {
		$monthList = array();
		$monthObj=null;
		$monthIndex = strtotime(date('Y-m',$start).'-10');
		do {
			if ($monthObj!=null) $monthList[] = $monthObj;
			$monthObj = myOwnCalendarMonth::getOnDate($monthIndex, $dayofweek, $dayofmonth);
			$monthIndex = strtotime("+1 month",$monthIndex);
		} while ($monthObj->getStart()<$end);
		return $monthList;
	}
	
	/*public function __get($property) {
		echo '__get:'.$property;
		if (property_exists($this, $property)) {
			if ($property=='_weeks' && !$this->_weeks)
				$this->_weeks = $this->getWeeks();
			
			echo 'test_weeks';
			return $this->$property;
		}
	}
	*/
	public function getTimeSlices($view, $type) {
		$slices = array();
		$timeslotsObj = $view->timeslotsObj;//$view->potentialResa->_mainProduct->_timeslotsObj;
		$enableWeekEnd = $timeslotsObj->isWeekEndDays();
		$tscount=count($timeslotsObj->list);
		$end = $this->getEnd();
		//need to add 1 day  because end of month is last day at 00:00
		$end = strtotime('+1 day',$end);
		if ($view->end<$end) $end = $view->end;
		$start = $this->getStart();
		if ($view->start>$start) $start = $view->start;
	
		if ($this->_dayOfMonth!=null) {
			$dayOfMonthStr = date('Y-m-d', $this->_dayOfMonth);
			
			$timeSlot = $timeslotsObj->list[0];
			$slice = new myOwnCalendarTimeSlice($this->_dayOfMonth, $timeSlot, $type, $view);
			
			$this->_filteredDays[$dayOfMonthStr] = new myOwnCalendarDay($this->_dayOfMonth);
			$this->_filteredDays[$dayOfMonthStr]->_slices[0] = $slice;
			
			return array($dayOfMonthStr.' '.$timeSlot->startTime => $slice);
		} else {
			foreach ($this->getWeeks() as $weekKey => $weekObj)
			{	
				foreach ($weekObj->getDaysObj($enableWeekEnd) as $dayKey => $dayObj)
				{
					foreach ($timeslotsObj->getTimeSlotsOfDay($dayObj->date, $type) AS $timeSlot)
					{
						//echo ' '.$timeSlot->sqlId.' '.date('Y-m-d', $dayObj->date).'>='.date('Y-m-d', $start);
						if ( $timeSlot->getEndHourTime($dayObj->date)<=$end
							&& $timeSlot->getStartHourTime($dayObj->date)>=$start)
							{
							
							$slice = new myOwnCalendarTimeSlice($dayObj->date, $timeSlot, $type, $view);
							$slices[date('Y-m-d', $dayObj->date).' '.$timeSlot->startTime] = $slice;
							$weekObj->_days[$dayKey]->_slices[$timeSlot->sqlId] = $slice;
						}
					}
					//adding day the day if enabled timeslots in it
					if (count($weekObj->_days[$dayKey]->_slices))
						 $this->_filteredDays[date('Y-m-d', $dayObj->date)] = $weekObj->_days[$dayKey];
						 
					//create index slice for mont view resume
					if ( $view->potentialResa->_mainProduct->isTimeSelect() 
						 && $dayObj->getStart()<=$end
						 && $dayObj->getEnd()>=$start)
					{
						$slice = new myOwnCalendarTimeSlice($dayObj->date);
						$slice->enable=count($weekObj->_days[$dayKey]->_slices)>0;
						$slices[date('Y-m-d', $dayObj->date)] = $slice;

						//adding index timeslice if timeslices in the day
						if (count($weekObj->_days[$dayKey]->_slices))
							$weekObj->_days[$dayKey]->_slices[-1] = $slice;
					}
				}
				$this->_weeks[$weekKey]=$weekObj;
			}
		}
		return $slices;
	}
	
	
	//Getting month
	//-----------------------------------------------------------------------------------------------------------------
	public static function getOnDate($monthIndex, $dayofweek=null, $dayofmonth=null) {
		return new myOwnCalendarMonth(date('m',$monthIndex), date('Y',$monthIndex), $dayofweek, $dayofmonth);
	}

}
	
?>