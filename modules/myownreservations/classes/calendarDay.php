<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnCalendarDay {
	public $date;
	public $key; 
	public $_slices=array();
	
	public function __construct($date)
	{
		if (is_string($date))
			$this->date = strtotime($date);
		else $this->date = $date;
		$this->key=date('Y-m-d', $this->date);
	}

	public function getStart() {
		return strtotime(date('Y-m-d', $this->date));
	}
	
	public function getEnd() {
		return strtotime('+1 day',$this->getStart());
	}
	
	public function getDayNumber()
	{
		return date('N', $this->date);
	}
	
	public function getSlice($timeSlotId) {
		if (array_key_exists($timeSlotId, $this->_slices))
			return $this->_slices[$timeSlotId];
		else return null;
	}
	
	//Getting days dates from week
	//-----------------------------------------------------------------------------------------------------------------
	public function getDays($enableWeekEnd = true) {
		if (strlen($this->week)==1) $this->week="0".$this->week;
		$max=5;
		if ($enableWeekEnd) $max=7;
		$ref = strtotime($this->year."W".$this->week);
		if (intval($this->week) == 0) {
			$ref = strtotime("-1 week",strtotime($this->year."W01"));
		}
		for($i=0; $i<$max; $i++) {
			$dayList[$i] = strtotime("+".$i."day",$ref);
		}
		return $dayList;
	}
	
	//Getting day from day number of week
	//-----------------------------------------------------------------------------------------------------------------
	public static function getDayOfWeek($dayNb, $week, $year) {
		if (strlen($week)==1) $week='0'.$week;
		$ref = strtotime($year."W".$week);
		
		if (intval($week) == 0) {
			$ref = strtotime("-1 week",strtotime($year."W01"));
		}
		return strtotime("+".($dayNb-1)."day",$ref);
	}
	
	public function toString($obj) {
		return date("Y-m-d", $this->getStart()).' '.$this->year;
	}
	
	//Getting week
	//-----------------------------------------------------------------------------------------------------------------
	public static function getOnDate($weekIndex) {
		return new myOwnCalendarWeek(date('W',$weekIndex), date('o',$weekIndex));
	}
	
	//Getting days
	//-----------------------------------------------------------------------------------------------------------------
	public static function getFilteredOnPeriod($start, $end, $_timeSlots, $type, $availabilities = null, $ext_object = null) {
		if(session_id() == '') session_start();
		$objectSelection = Tools::getValue('place', (int)$_SESSION['objectSelection']);
		$dayCount=0;
		$availableDays = array();
		if (count($_timeSlots->list) > 5) {
			$dstarts = date('Y-m-d',$start);
			$dstart = strtotime($dstarts.' 00:00:00');
			$dend = strtotime("+1 day", strtotime(date('Y-m-d',$end).' 00:00:00'));
		}
		$index=0;
		do {
 			$day=strtotime("+".$dayCount."day",$start);
			$dayCount++;
			$tsEnableOnDay=false;

			if (count($_timeSlots->list) > 5 && $dstarts!=date('Y-m-d',$day)) {
				if ($day<=$dend
					&& $day>=$dstart
					&& $availabilities->isAvailable($day)) {
					$tsEnableOnDay=true;
				}
			} else {
				
				foreach ($_timeSlots->getTimeSlotsOfDay($day, $type) AS $timeSlot) {
					$dayt=strtotime(date("Y-m-d",$day)." 00:00:00");
					if ($timeSlot->getEndHourTime($day)<=$end
						&& $timeSlot->getStartHourTime($day)>=$start
						&& ( $availabilities == null or $availabilities->isPeriodAvailable($timeSlot->getStartHourTime($day), $timeSlot->getEndHourTime($day), 0, 0, $objectSelection) ) ) {
						$tsEnableOnDay=true;
						break;
					}
				}
			}
  			if ($tsEnableOnDay) {
	  			$availableDays[$index]=new myOwnCalendarDay($day);
	  			$index++;
  			} 
		} while ($day <= $end);

		return $availableDays;
	}
	
	public function getTimeSlices($view, $type) {
		$slices = array();

		$end = $this->getEnd();
		if ($view->end<$end) $start = $view->end;
		$start = $this->getStart();
		if ($view->start>$start) $start = $view->start;

		foreach ($view->timeslotsObj->getTimeSlotsOfDay($this->date, $type) AS $timeSlot)
		{
			if ( $timeSlot->getEndHourTime($this->date)<=$end
				&& $timeSlot->getStartHourTime($this->date)>=$start)
				{
				$slice = new myOwnCalendarTimeSlice($this->date, $timeSlot, $type, $view);
				$slices[date('Y-m-d', $this->date).' '.$timeSlot->startTime] = $slice;
				$this->_slices[$timeSlot->sqlId] = $slice;
			}
		}
			
		return $slices;
	}

}
	
?>