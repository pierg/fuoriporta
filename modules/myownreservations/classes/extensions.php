<?php
/*
* 2010-2012 LaBulle All right reserved
*/
class extension_type {
	const OPTIONS = 0;
	const NOTIFICATION	= 1;
	const ADMIN = 2;
	const CONFIG = 3;
	const AJAX = 4;
	const MODULE = 5;
	const CART = 6;
	const GLOBFEE = 7;
	const RESADETAILS = 8;
	
	const PROD_UNIT = 9;
	const PROD_QTY = 16;
	const PROD_OCC = 26;
	
	const OBJECT = 10;
	const SHOW_OBJECTS = 11;
	
	const SEL_WIDGET = 12;
	const SEL_QTY = 13;
	
	const CARTINSERT = 14;
	const CARTUPDATE = 15;
	const CARTDETAILS = 17;
	
	const HEADER = 18;
	const ACCOUNT = 19;
	const ADM_PROD = 20;
	const UPD_PROD = 21;
	const FRONT = 23;
	const FEE = 24;
	const NOTIFOBJ = 25;
}

class myOwnExtensions {
	var $list=array();
	
	//Construct extensions item list
	//-----------------------------------------------------------------------------------------------------------------
	public function __construct() {
		$parents=array();
		foreach (glob(MYOWNRES_PATH . "extensions/*_.php") as $filename) { 
			 require_once($filename);
			 $name = str_ireplace("_.php", "", basename($filename));
			 $parents[$name] = 0;
	    }
		foreach (glob(MYOWNRES_PATH . "extensions/*.php") as $filename) 
			if (stripos($filename, 'admin')===false) { 
			 require_once($filename);
			 $name = str_ireplace(".php", "", basename($filename));
			 if (class_exists($name)) {
			 	$temp = new $name();
			 	$temp->name=$name;
			 	foreach ($parents as $parent => $val) {
				 	if (get_parent_class($temp) == $parent)
				 		$parents[$parent] = $temp->name;
			 	}

			 	$temp->enabled = !Configuration::get('MYOWNRES_EXT_DISABLED_'.$temp->id, 0);
			 	$this->list[$temp->id]=$temp;
			 }
		}
		foreach ($parents as $parent => $extloaded)
			if (!$extloaded) {
				$temp = new $parent();
			 	$temp->name=$parent;
			 	$temp->enabled = !Configuration::get('MYOWNRES_EXT_DISABLED_'.$temp->id, 0);
			 	$this->list[$temp->id]=$temp;
			}
		uasort($this->list, array('myOwnExtensions', 'sortById') );
	}
	
	public static function sortById($a,$b) {
		if ($a->id == $b->id) {
				//de la plus longue a la plus petite
			//if (($a->getEndTime(true)-$a->getStartTime()) == ($b->getEndTime(true)-$b->getStartTime())) {
			    return 0;
			//}
			return ($a->id < $b->id ? -1 : 1);
		}
		//de celle qui fini le plus tard a celle qui fini le plus tot
		return ($a->id < $b->id) ? -1 : 1;
	}
	
	public function setInfos($obj) {
		foreach ($this->list as $ext) $ext->setInfos($obj);
		$occ_ext = $obj->_extensions->getExtByType(extension_type::PROD_OCC);
			foreach ($obj->_products->list as $mainProduct) {	
				if (array_key_exists($mainProduct->productType, $occ_ext)) {
					$mainProduct->_occupyObj = $occ_ext[$mainProduct->productType];
				}
			}
				/*if ($mainProduct->reservationSelType>=reservation_interval::MONTH) {
					$mainProduct->_reservationSelExt = $this->getSelectionExtension($mainProduct->reservationSelType);
					if ($mainProduct->_reservationSelExt!=null) $mainProduct->_timeslotsObj->list = $mainProduct->_reservationSelExt->getTimeslots($mainProduct);
				}*/
	}
	
	public function displayConfigProduct($idProductFamilly) {
		$out='';
		foreach ($this->list as $extension) {
			if (method_exists ( $extension , "displayConfigProduct" ))
				$out .= $extension->displayConfigProduct($idProductFamilly);
		}
		return $out;
	}
	
	public function getConfigProduct($idProductFamilly) {
		$exts=array();
		foreach ($this->list as $extension) {
			if (method_exists ( $extension , "displayConfigProduct" ))
				$exts[get_class($extension)] = get_class($extension);//$extension;
		}
		return $exts;
	}
	
	public function getExtByName($extName) {
		foreach ($this->list as $extension) {
			if (strtolower(get_class($extension))==strtolower($extName))
				 return $extension;
		}
		return null;
	}
	
	public function getSelectionExtension($unit) {
		foreach ($this->list as $extension)
			if (method_exists($extension, "getUnitLabel") && $extension->unit==$unit) return $extension;
		return  null;
	}
	
	public function getExtByQty() {
		$exts = $this->getExtByType(extension_type::PROD_QTY);
		$qtyExts=array();
		foreach ($exts as $ext) 
			if (method_exists($ext, "getQties"))
				$qtyExts[$ext->id] = $ext;
		return $qtyExts;
	}
	
	public function getExtByType($extType) {
		$list=array();
		foreach ($this->list as $key => $extension) 
			if ($extension->enabled || $extType == extension_type::MODULE) {
			if ($extType == extension_type::NOTIFICATION && method_exists($extension, "displayConfigNotif")) $list[]=$extension;
			if ($extType == extension_type::NOTIFOBJ && method_exists($extension, "displayOrderConfResa")) $list[]=$extension;
			if ($extType == extension_type::OPTIONS && method_exists($extension, "displayConfigProduct")) $list[]=$extension;
			if ($extType == extension_type::MODULE && method_exists($extension, "displayConfigGlobal")) $list[]=$extension;
			if ($extType == extension_type::CART && method_exists($extension, "displayShoppingCart")) $list[]=$extension;
			if ($extType == extension_type::CARTDETAILS && method_exists($extension, "displayCartDetails")) $list[$key]=$extension;
			if ($extType == extension_type::RESADETAILS && method_exists($extension, "displayReservationDetails")) $list[]=$extension;
			if ($extType == extension_type::GLOBFEE && method_exists($extension, "getGlobalFee")) $list[]=$extension;
			if ($extType == extension_type::AJAX && method_exists($extension, "ajax")) $list[]=$extension;
			
			if ($extType == extension_type::PROD_UNIT && method_exists($extension, "getUnitLabel")) $list[]=$extension;
			if ($extType == extension_type::PROD_QTY && method_exists($extension, "getQties")) $list[]=$extension;
			if ($extType == extension_type::PROD_OCC && method_exists($extension, "displayConfigRuleOccupation")) $list[$key]=$extension;

			if ($extType == extension_type::OBJECT && method_exists($extension, "getContent")) $list[$key]=$extension;
			if ($extType == extension_type::SHOW_OBJECTS && method_exists($extension, "getTab")) $list[]=$extension;
			
			if ($extType == extension_type::SEL_QTY && method_exists($extension, "displayProductAction")) $list[]=$extension;
			if ($extType == extension_type::SEL_WIDGET && method_exists($extension, "widgetSelect")) $list[]=$extension;
			
			if ($extType == extension_type::CARTINSERT && method_exists($extension, "execCartInsert")) $list[]=$extension;
			if ($extType == extension_type::CARTUPDATE && method_exists($extension, "execCartUpdate")) $list[]=$extension;
			if ($extType == extension_type::HEADER && method_exists($extension, "displayHeader")) $list[]=$extension;
			if ($extType == extension_type::ACCOUNT && method_exists($extension, "execDisplayAccount")) $list[]=$extension;
			if ($extType == extension_type::ADM_PROD && method_exists($extension, "displayAdminProduct")) $list[]=$extension;
			if ($extType == extension_type::UPD_PROD && method_exists($extension, "actionProductUpdate")) $list[]=$extension;
			if ($extType == extension_type::FRONT && method_exists($extension, "displayFrontPage")) $list[]=$extension;
			if ($extType == extension_type::FEE && method_exists($extension, "displayConfigFee")) $list[]=$extension;
		}
		return $list;
	}
	
	public static function removeAllDetail($id_reservation=0) {
		$req = "DELETE FROM `"._DB_PREFIX_."myownreservations_detail` WHERE `id_reservation` = ".(int)$id_reservation;
 		return Db::getInstance()->Execute($req);
	}
	
	public static function getConfig($id_ext, $id_family=-2, $id_option=0, $id_internal=0, $default='') {
		$list=array();
		if ($id_family==-1)$id_family=0;
		$id_shop = myOwnUtils::getShop();
		if ($id_shop==1) $id_shop=0;
		$req="SELECT * FROM `"._DB_PREFIX_."myownreservations_option` WHERE `id_extension` = ".$id_ext." AND `id_option` = ".intval($id_option);
		if (defined('MYOWNRES_SEPARATE_SHOP') && MYOWNRES_SEPARATE_SHOP) $req.=" AND `id_shop`=".$id_shop;
		else  $req.=" AND `id_shop`=0";
		if ($id_family>-2) $req .= " AND `id_family` = ".intval($id_family);
		if ($id_internal>-1) $req .= " AND `id_internal` = ".intval($id_internal);
		$optionsSql = Db::getInstance()->ExecuteS($req);

		if ($id_internal>-1 && $id_family>-1) { //count($optionsSql)==1 && removed because case of multiple results per shop
			if ($id_internal==-1) {
				if (isset($optionsSql[0]))
					return $optionsSql[0]['value'];
				else return $default;
			}
			if ($id_option>0 && isset($optionsSql[0]['value'])) return $optionsSql[0]['value'];
			if ($id_option>0 && isset($optionsSql[0])) return $optionsSql[0];
		}
		if ($optionsSql==array()) return $default;
		
		return $optionsSql;
	}
}

?>