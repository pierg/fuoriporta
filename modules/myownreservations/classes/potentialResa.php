<?php
/*
* 2010-2012 LaBulle All right reserved
*/
	
class myOwnPotentialResa extends myOwnCart {
	var $_pricerules;
	var $_pricesets;
	var $_reservations;
	var $_productQty=-9;
	//var $_productprice=-1;
	var $_taxrate=-1;
	var $_reservationEnd=null;
	var $_selType;
	var $_selTypeNum;
	
	public function __construct() {
		$this->_reservations = new myOwnResas(0);
	}
	
	function getPreviewCart($day, $id_EndTimeslot, $availabilities = null) {
		$previewCart = clone $this;
		$previewCart->_fixedLength = 0;
		$previewCart->endDate = date('Y-m-d',$day);
		if ($id_EndTimeslot==0) {
			$ts = $previewCart->_mainProduct->_timeslotsObj->getDaily();

// 			if ($ts!=null && $ts->endTime<$ts->startTime) $previewCart->endDate = date('Y-m-d',strtotime('+1day', $day));
		}

		//removed since we get on the right day
		if ($previewCart->_mainProduct->reservationSelType==reservation_interval::WEEK && 
			$previewCart->_mainProduct->_reservationSelWeekStart >= $previewCart->_mainProduct->_reservationSelWeekEnd)
			$previewCart->endDate = date('Y-m-d',strtotime('+1week',$day));
			
		$previewCart->endTimeslot = $id_EndTimeslot;
		//if ($previewCart->id_product>0) $previewCart->setPrice($this->_pricerules, $this->_pricesets, $availabilities, $this->_productprice, $this->_taxrate);
		if ($previewCart->id_product>0) $previewCart->setPrice($this->_pricerules, $this->_pricesets, $availabilities);
		return $previewCart;
	}
	
	function getLength($forPrice = false, $availabilities = null) {
		if ($this->_fixedLength)
			return $this->_fixedLength;
		else return parent::getLength($forPrice, $availabilities); 
	}
	
	/*
	function getEndDateTime($display=false) {
		$time = parent::getEndDateTime($display);
		
		if ($potentialResa->_selType != "start")) {
			//display on calendar a resa with smae start day end end day
			if ($this->_mainProduct!=null && $this->_mainProduct->reservationSelType==reservation_interval::DAY)
				return strtotime("+1 day", $time);
		}
		return strtotime($this->endDate.' '.$endTime);
	}*/
	
	function getSelectCart($day, $reservationEndTimeslot) {
		$selectCart = clone $this;

		return $selectCart;
	}
	
	function getEstimateCart($day, $id_EndTimeslot, $availabilities=null) {
		$estimateCart = clone $this;
		$estimateCart->_fixedLength = 0;
		$estimateCart->startDate = date('Y-m-d',$day);
		$estimateCart->startTimeslot = $id_EndTimeslot;
		$productLength=0;

		$desiredShift = $estimateCart->_mainProduct->getDesiredShift($estimateCart);

		if ($estimateCart->_mainProduct->isTimeSelect()
			or $estimateCart->_mainProduct->reservationSelType==reservation_interval::DAY) {

			if (array_key_exists($estimateCart->startTimeslot, $estimateCart->_mainProduct->_timeslotsObj->list) && $estimateCart->_mainProduct->_timeslotsObj->list[$estimateCart->startTimeslot]->startTime > $estimateCart->_mainProduct->_timeslotsObj->list[$estimateCart->startTimeslot]->endTime) $desiredShift--;
		
			$realShif = $estimateCart->_mainProduct->getShiftTimeSlot($day, $estimateCart->startTimeslot, $desiredShift, true, $availabilities, $endDate, $estimateCart->endTimeslot, $estimateCart->id_product, $estimateCart->id_product_attribute, $estimateCart->_reservationEnd);
	//echo '='.$realShif;
			if ($realShif<0)
				return null;

		} else {
			$next = $estimateCart->getNextPeriod($day);
			if ($desiredShift>0) $desiredShift--;
			if ($desiredShift>0 || $estimateCart->_mainProduct->isTimeSelect()) {
				if ($estimateCart->_mainProduct->getShiftTimeSlot($next, 0, $desiredShift, true, $availabilities, $endDate, $estimateCart->endTimeslot, $estimateCart->id_product, $estimateCart->id_product_attribute, $estimateCart->_reservationEnd)<0) return null;
			} else $endDate = $next;
		}
		
		$estimateCart->endDate = date('Y-m-d',$endDate);
		if ($id_EndTimeslot==0 
			&& ($estimateCart->_mainProduct->isTimeSelect()
			or $estimateCart->_mainProduct->reservationSelType==reservation_interval::DAY)
			) {
			$ts = $estimateCart->_mainProduct->_timeslotsObj->getDaily();
			if ($ts!=null && $ts->endTime<$ts->startTime) $estimateCart->endDate = date('Y-m-d',strtotime('+1day', $endDate));
		}

		if (is_array($this->_pricerules) &&($desiredShift = $estimateCart->_mainProduct->_pricePreview 
			or $this->_mainProduct->_reservationLengthControl == reservation_length::FIXED
			or $this->_mainProduct->_reservationLengthControl == reservation_length::NONE
			or $this->_mainProduct->_reservationLengthControl == reservation_length::OPTIONAL
			or $this->_mainProduct->_reservationLengthControl == reservation_length::PRODUCT
			or $this->_mainProduct->_reservationLengthControl == reservation_length::COMBINATION))
			$estimateCart->setPrice($this->_pricerules, $this->_pricesets, $availabilities);

		return $estimateCart;
	}
	
	function getCart($day, $id_EndTimeslot, $availabilities=null) {
		if ($this->_selType == "end")
			return $this->getPreviewCart($day, $id_EndTimeslot, $availabilities);
		else return $this->getEstimateCart($day, $id_EndTimeslot, $availabilities);
	}
		
	public function getAvailableQuantity($cart=null, $availabilities=null, $stocks=null) {

		if ($this->_productQty==-9) return parent::getAvailableQuantity($cart, $availabilities, $stocks);
		if ($this->_productQty==-1) return -1;
		$unavailableQty = $this->getUnavailableQuantity($cart, $availabilities, $this->_reservations);

		if ($unavailableQty<0) return 0;

		return $this->_productQty-$unavailableQty;
	}
	
	public function isAvailable($cart=null, $availabilities=null, $stocks=null, $updating=false) {
		//check product qty (<0 means unlimited products)

		if ($this->_productQty==-9) return parent::isAvailable($cart, $availabilities, $stocks, $updating);
		if ($this->_productQty==-1) return true;
		
		//check reservations (<0 means holiday problem)
		$unavailableQty = $this->getUnavailableQuantity($cart, $availabilities, $this->_reservations);
		if ($unavailableQty<0) return false;
		
		$availableQty = $this->_productQty-$unavailableQty;
		$resaOccupation = $this->getOccupationQty();
		
		return ($resaOccupation <= $availableQty);
	}
	
	function showPrice() {
		return (($this->_mainProduct->_pricePreview) or ($this->_selType=="end" or $this->_selType==""));
	}
	
	function showLength() {
		$productLength=$this->_mainProduct->getDesiredShift($this);

		if ($this->_selType==""
			&& ($this->_mainProduct->reservationSelType==reservation_interval::TIMESLOT or $this->_mainProduct->reservationSelType==reservation_interval::TIMESLICE)
			&& (($this->_mainProduct->_reservationLengthControl == reservation_length::FIXED && $this->_mainProduct->_reservationMinLength>1)
				or $this->_mainProduct->_reservationLengthControl == reservation_length::PRODUCT
				or $this->_mainProduct->_reservationLengthControl == reservation_length::COMBINATION))
				return $productLength;
		else return false;
	}
	
	function showTime() {
		return $this->_mainProduct->_reservationShowTime;
	}
	
	function showQuantity() {
		return $this->_mainProduct->_qtyDisplay;
	}
	
	function showObject() {
		return count($this->_mainProduct->ids_options)==0;//$this->_mainProduct->productType != product_type::PLACE;
	}

	function getNextPeriod($day) {
	
		//if ($this->_mainProduct->_reservationSelExt!=null) return $this->_mainProduct->_reservationSelExt->getNextPeriod($day, $this->_mainProduct);
		
		if ($this->_mainProduct->reservationSelType==reservation_interval::DAY) {
			$day = strtotime(date('Y-m-d', $day));
// 			if () $this->_mainProduct->_timeslotsObj->list[]
// 			echo date('Y-m-d H:i:s', $day).'>>>'.date('Y-m-d H:i:s', strtotime('+1day', $day)-1);
			if ($this->_mainProduct->_reservationStartTime > $this->_mainProduct->_reservationEndTime) 
				return strtotime(date('Y-m-d', $day).' '.$this->_mainProduct->_reservationEndTime);
			else return strtotime('+1day', $day)-1;
		}
		
		if ($this->_mainProduct->reservationSelType==reservation_interval::WEEK) {
			$next = myOwnCalendarWeek::getDayOfWeek($this->_mainProduct->_reservationSelWeekEnd, date('W',$day), date('o',$day));
			// if next strictly less than day instead were adding one more week that should not
			if ($next<=$day) $next = strtotime('+1week', $next);
			return $next;
		}
		if ($this->_mainProduct->reservationSelType==reservation_interval::MONTH) {
			return myOwnCalendarMonth::getDay(date('m',$day), date('Y',$day), "end", $this->_mainProduct->_reservationSelMonthEnd);
		}

		return $day;
	}
	
	function getInputSelect($currentCart) {
		if ($this->_selType=="start")
			return $currentCart->startDate.'_'.$currentCart->startTimeslot;
		else return $currentCart->getSelectValue();
	}
	
	//Display period in resa cell
	//-----------------------------------------------------------------------------------------------------------------
	function showPeriod($obj, $day, $timeSlot=null, $id_lang, $short=false) {
		
		if ( ($this->_mainProduct->_reservationSelMode==reservation_mode::START_LENGTH && $this->_selType == "end")
			 ||	(Configuration::get('MYOWNRES_WIDGET_TYPE')==3 && $this->_selType == "end" && $short) ) {
			$length = $this->getLength();
           // die('000');
			return $length.' '.myOwnLang::getUnit($this->_mainProduct->_reservationUnit, $length>1);
		} else {

		   // die('1');
			$timedetail = $timeSlot!=null && $timeSlot->sqlId!=0 && count($this->_mainProduct->_timeslotsObj->list)>1;
			//get fro standard unit
			if ($this->_mainProduct->reservationSelType<=reservation_interval::MONTH || $this->_mainProduct->reservationSelType<=reservation_interval::TIMESLICE) {
				$str='';
				if ($timedetail) {
					if ($this->_mainProduct->_reservationShowTime)
						//in the case of end is optional we need to display end bound time for start
						if (MYOWNRES_WEEK_TS) $str .= MyOwnCalendarTool::formatDateShort($day,$id_lang);
						else if ($this->_selType=="end" || $this->_mainProduct->_reservationLengthControl == reservation_length::OPTIONAL)
							$str .= MyOwnCalendarTool::formatTimeFromDate($this->getEndStartDateTime(),$id_lang);
						else
							$str .= MyOwnCalendarTool::formatTimeFromDate($this->getStartDateTime(),$id_lang);
						
					else {
						$str .= $timeSlot->name;
						if (trim($timeSlot->name)=='' && $this->_mainProduct->reservationSelPlanning!=reservation_view::DAY && $this->_mainProduct->reservationSelPlanning!=reservation_view::MONTH)
							$str .= MyOwnCalendarTool::formatDateShort($day, $id_lang);
					}
				} else {
					if ($this->_selType=="end") $daytemp = $this->getNextPeriod($day);
					else $daytemp = $day;
					
					$str .= (($short) ?  date('j',$day) : MyOwnCalendarTool::formatDateShort($day, $id_lang));
				}
				//if single or multiple selection need to display end date in period.
				//if ($this->_mainProduct->_reservationLengthControl != reservation_length::NONE && ($this->_mainProduct->_reservationLengthControl != reservation_length::OPTIONAL || $this->_selType=="end")) {
				if ($this->_selType=="" && $this->_mainProduct->_reservationLengthControl != reservation_length::OPTIONAL && $this->_mainProduct->_reservationLengthControl != reservation_length::NONE) {
					if ($this->_selType=="") {
						if ($this->startDate==$this->endDate) {
							if ($this->_mainProduct->_reservationShowTime && $timedetail) $str .= ' - '.MyOwnCalendarTool::formatTimeFromDate($this->getEndDateTimeRaw(),$id_lang);
							 //'<span class="start">'.$str.'</span> - <span class="end">'..'</span>';
						} else
							$str .= ' - '.MyOwnCalendarTool::formatTimeFromDate($this->getEndDateTimeRaw(),$id_lang);
							//$str = ' - '.MyOwnCalendarTool::formatDateShort($this->getEndDateTimeRaw(),$id_lang);
					/*} elseif ($this->_selType!="end" && !$short) {
						if ( $this->_mainProduct->reservationSelPlanning==reservation_view::WEEK
							or 	$this->_mainProduct->reservationSelPlanning==reservation_view::MONTH) {
							$str .= " - ".MyOwnCalendarTool::formatDateShort($this->getNextPeriod($day),$id_lang);
						}*/
					}
				}
				return $str;
				
			//get from ext
			} else {
                //die('222');
				$exts = $obj->_extensions->getExtByType(extension_type::PROD_UNIT);
				foreach ($exts AS $ext)
					if ($this->_mainProduct->reservationSelType==$ext->unit)
						return $ext->showPeriod($obj, $day, $this, $timeSlot, $id_lang, $short);
			}
		}
	}
	
	public static function clearSessionPotentialResa() {
		if(session_id() == '') session_start();
		$_SESSION['startSelection'] = '';
		$_SESSION['endSelection'] = '';
		$_SESSION['lengthSelection'] = '';
		$_SESSION['categorySelection'] = '';
		$_SESSION['objectSelection'] ='';
		$_SESSION['pageStart'] = 0;
		$_SESSION['pageEnd'] = 0;
		$_SESSION['qtySelection'] = 0;

        $_SESSION['posti'] = 1;
	}
	
	public static function getSessionPotentialResa($obj, $id_product=-1, $id_product_attribute=-1) {
		global $cookie;
		global $cart;
		if(session_id() == '') session_start();

		$_SESSION['startSelection'] = Tools::getValue('selectedStart', (isset($_SESSION['startSelection']) ? $_SESSION['startSelection'] : '') );
		$_SESSION['endSelection'] = Tools::getValue('selectedEnd', (isset($_SESSION['endSelection']) ? $_SESSION['endSelection'] : '') );
		$_SESSION['lengthSelection'] = Tools::getValue('selectedLength', (isset($_SESSION['lengthSelection']) ? $_SESSION['lengthSelection'] : '') );
		$_SESSION['objectSelection'] = Tools::getValue('objectSelection', (isset($_SESSION['objectSelection']) ? $_SESSION['objectSelection'] : '') );
		$_SESSION['qtySelection'] = Tools::getValue('qtySelection', (isset($_SESSION['qtySelection']) ? $_SESSION['qtySelection'] : '') );
		$_SESSION['qtiesSelection'] = Tools::getValue('qtiesSelection', (isset($_SESSION['qtiesSelection']) ? $_SESSION['qtiesSelection'] : '') );
		$_SESSION['pageStart'] = Tools::getValue('pageStart', (isset($_SESSION['pageStart']) ? $_SESSION['pageStart'] : 0) );
		$_SESSION['pageEnd'] = Tools::getValue('pageEnd', (isset($_SESSION['pageEnd']) ? $_SESSION['pageEnd'] : 0) );

        if (isset($_POST['posti'])) {
            $_SESSION['posti'] = $_POST['posti'];
             //die('--'.$_SESSION['posti']);
        }

		if (isset($_POST['categorySelection'])) {
		    $_SESSION['categorySelection'] = $_POST['categorySelection'];
           // die('--'.$_SESSION['categorySelection']);
		}
		else if (isset($_POST['startSelection']) || isset($_GET['search_query'])) $_SESSION['categorySelection'] = -1;
		
		if (Tools::getValue('selectedStart')!='') $_SESSION['selectionTime'] = time();

		if ($_SESSION['startSelection'] !='' && (time()-$_SESSION['selectionTime'])>3600 ) {
			$_SESSION['startSelection'] = '';
			$_SESSION['endSelection'] = '';
			$_SESSION['objectSelection'] ='';
			$_SESSION['qtySelection'] = 0;
			$_SESSION['qtiesSelection'] = 0;
		}
      //  $_SESSION['startSelection']='2019-03-21_0';
        //$_SESSION['endSelection']='2019-03-21_0';

		$samePeriod = Configuration::get('MYOWNRES_SAME_PERIOD');
		if ($samePeriod && count($cart->getMyOwnReservationCart()->list)>0) {


			$_SESSION['categorySelection'] = -1;
			foreach($cart->getMyOwnReservationCart()->list AS $cartprod)
				$firstcart = $cartprod;
			$potentialResa = new myOwnPotentialResa();
			if ($id_product_attribute!=-1) $potentialResa->id_product_attribute = $id_product_attribute;
			if ($id_product!=-1) $potentialResa->id_product = $id_product;
			if ($_SESSION['qtySelection']>1) $potentialResa->quantity = $_SESSION['qtySelection'];
			else $potentialResa->quantity = 1;
			$potentialResa->_productQty = -1;
			$potentialResa->startDate = $firstcart->startDate;
			$potentialResa->startTimeslot = $firstcart->startTimeslot;
			$potentialResa->endDate = $firstcart->endDate;
			$potentialResa->endTimeslot = $firstcart->endTimeslot;
			$potentialResa->_mainProduct = $firstcart->_mainProduct;
			$potentialResa->ids_categories = explode(',', $potentialResa->_mainProduct->ids_categories);
			if ($potentialResa->isAvailable()>0)
				return $potentialResa;
			else return null;
			
		} elseif (array_key_exists('startSelection', $_SESSION) && $_SESSION['startSelection']!='' && $_SESSION['startSelection']!='0' && Configuration::get('MYOWNRES_WIDGET_TYPE')==1 && stripos($_SESSION['endSelection'], '_') && !$_SESSION['lengthSelection']) {

			$startSelectionTab=explode('_', $_SESSION['startSelection']);
			$endSelectionTab=explode('_', $_SESSION['endSelection']);
			$potentialResa = new myOwnPotentialResa();
			if ($id_product_attribute!=-1) $potentialResa->id_product_attribute = $id_product_attribute;
			if ($id_product!=-1) $potentialResa->id_product = $id_product;
			if ($_SESSION['qtySelection']>1) $potentialResa->quantity = $_SESSION['qtySelection'];
			else $potentialResa->quantity = 1;
			$potentialResa->_productQty = -1;
			$potentialResa->startDate = $startSelectionTab[0];
			$potentialResa->startTimeslot = intval($startSelectionTab[1]);
			$potentialResa->endDate = $endSelectionTab[0];
			$potentialResa->endTimeslot = intval($endSelectionTab[1]);
			$homeCat = Category::getRootCategory($cookie->id_lang);

			//standard
			if ((intval($_SESSION['categorySelection'])==0 or intval($_SESSION['categorySelection'])==$homeCat->id_category)
				&& array_key_exists(0, $obj->_products->list))
				$potentialResa->_mainProduct = $obj->_products->list[0];
			//pro
			else {
				if (intval($_SESSION['categorySelection'])==-1) $potentialResa->_mainProduct = $obj->_products->getGlobal("end");
				else $potentialResa->_mainProduct = $obj->_products->getResProductFromCategory(intval($_SESSION['categorySelection']));
				//case of rule without cat
				if (count($obj->_products->list)==1 && $potentialResa->_mainProduct == null)
					foreach($obj->_products->list as $mainProduct)
						$potentialResa->_mainProduct = $mainProduct;
			}
			$potentialResa->ids_categories = explode(',', $potentialResa->_mainProduct->ids_categories);
			if ($potentialResa->isAvailable()>0)
				return $potentialResa;
			else return null;
		} 
		else if (array_key_exists('startSelection', $_SESSION) && $_SESSION['startSelection']!='' && $_SESSION['startSelection']!='0' && $_SESSION['lengthSelection']!='0' && Configuration::get('MYOWNRES_WIDGET_TYPE')==3) {

			$startSelectionTab=explode("_", $_SESSION['startSelection']);
			$lengthSelection=intval($_SESSION['lengthSelection']);
			$potentialResa = new myOwnPotentialResa();
			if ($id_product_attribute!=-1) $potentialResa->id_product_attribute = $id_product_attribute;
			if ($id_product!=-1) $potentialResa->id_product = $id_product;
			$potentialResa->quantity = 1;
			$potentialResa->_productQty = -1;
			$potentialResa->startDate = $startSelectionTab[0];
			$potentialResa->startTimeslot = intval($startSelectionTab[1]);
			$desiredShift = $lengthSelection;
			$endDate = 0;
			$endTimeslot = 0;
			
			$homeCat = Category::getRootCategory($cookie->id_lang);
			//standard
			if ((intval($_SESSION['categorySelection'])==0 or intval($_SESSION['categorySelection'])==$homeCat->id_category)
				&& array_key_exists(0, $obj->_products->list))
				$potentialResa->_mainProduct = $obj->_products->list[0];
			//pro
			else {
				if (intval($_SESSION['categorySelection'])==-1) $potentialResa->_mainProduct = $obj->_products->getGlobal("end");
				else $potentialResa->_mainProduct = $obj->_products->getResProductFromCategory(intval($_SESSION['categorySelection']));
			}
			
			$potentialResa->_mainProduct->getShiftTimeSlot(strtotime($potentialResa->startDate), $potentialResa->startTimeslot, $desiredShift, false, null, $endDate, $endTimeslot, $potentialResa->id_product, $potentialResa->id_product_attribute);
			
			$potentialResa->endDate = date('Y-m-d',$endDate);
			$potentialResa->endTimeslot = $endTimeslot;
			$potentialResa->_fixedLength = $desiredShift;

			if ($potentialResa->isAvailable()>0)
				return $potentialResa;
			else return null;
		} else if(Tools::getIsset('searchStartDay')) {
			$potentialResa = new myOwnPotentialResa();
			$potentialResa->quantity = 1;
			$potentialResa->_productQty = -1;
			$potentialResa->startDate = Tools::getValue('searchStartDay');
			$potentialResa->startTimeslot = Tools::getValue('searchStartTimeslot', 0);
			$potentialResa->endDate = Tools::getValue('searchEndDay');
			$potentialResa->endTimeslot = Tools::getValue('searchEndTimeslot', 0);
			return $potentialResa;
		} else return null;
	}
	

}

?>