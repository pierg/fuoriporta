<?php
/**
* 2010-2014 LaBulle
*
* NOTICE OF LICENSE
*
* This source file is subject to Terms and Conditions of Use 
* Available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade module to newer
* versions in the future. If you wish to customize module for your
* needs please refer to http://www.labulle.net for more information.
*
* @author    laBulle.net <info@labulle.net>
* @copyright 2010-2014 laBulle
* @license   http://www.labulle.net/download/CGV-en.pdf
*/

class MyOwnUtils
{
	/*-----------------------------------------------------------------------------------------------------------------
	General utils
	-----------------------------------------------------------------------------------------------------------------*/

	public static function displayWarn($warn)
	{
		if (_PS_VERSION_ < '1.6.0.0') return '<div class="warn">'.$warn.'</div>';
		else return '<div class="alert alert-warning">'.$warn.'</div>';
	}

	public static function displayError($err)
	{
		if (_PS_VERSION_ >= '1.6.0.0') return '<div class="bootstrap"><div class="alert alert-danger">'.$err.'</div></div>';
		else return '<div class="error">'.$err.'</div>';
	}

	public static function displayConf($conf)
	{
		if (_PS_VERSION_ >= '1.6.0.0') return '<div class="bootstrap"><div class="alert alert-success">'.$conf.'</div></div>';
		return '<div class="conf">'.$conf.'</div>';
	}

	public static function displayInfo($hint, $display = true)
	{
		if (_PS_VERSION_ >= '1.6.0.0') return '<div class="bootstrap"><div style="'.($display ? 'display:block;' : 'display:none').'" class="alert alert-info">'.$hint.'</div></div>';
		else return '<div class="hint" style="'.($display ? 'display:block;' : '').'margin: 0 0 10px 0;">'.$hint.'</div>';
	}
	
	public static function displaySubInfo($hint)
	{
		return '<span style="display:none;"  class="subhint">'.$hint.'</span>';
	}

	/*-----------------------------------------------------------------------------------------------------------------
	PS16 utils
	-----------------------------------------------------------------------------------------------------------------*/

        public static function displayRow($obj, $actions, $id, $actions2=array(), $page=0, $page_total=0, $total_items=0)
        {
            //$pagination_opts = array(20, 50, 100, 200);
            $page_cnt = (int) Tools::getValue('pagination', 20);
            $out = '
    <div class="row">
        <div class="col-lg-6">';
            if (is_array($actions) && count($actions)) {
                $out .= '
            <div class="btn-group bulk-actions">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    '.$obj->l('Bulk actions', 'myownutils').' <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li>
                        <a href="#" onclick="javascript:checkDelBoxes($(this).closest(\'form\').get(0), \''.$id.'[]\', true);return false;">
                            <i class="icon-check-sign"></i>&nbsp;'.$obj->l('Select All', 'myownutils').'
                        </a>
                    </li>
                    <li>
                        <a href="#" onclick="javascript:checkDelBoxes($(this).closest(\'form\').get(0), \''.$id.'[]\', false);return false;">
                            <i class="icon-check-empty"></i>&nbsp;'.$obj->l('Unselect All', 'myownutils').'
                        </a>
                    </li>
                    <li class="divider"></li>
                    ';
                foreach ($actions as $actionKey => $actionValue) {
                    $out .= '
                    <li>
                            <a href="#" onclick="'.(stripos($actionKey, '$')!==false ? $actionKey : 'sendBulkAction($(this).closest(\'form\').get(0), \''.$actionKey.'\');').'">
                                '.$actionValue.'
                            </a>
                    </li>';
                }
                if ($actions2 != array()) {
                    $out .= '
                    <li class="divider"></li>
                    ';
                }
                foreach ($actions2 as $actionKey => $actionValue) {
                    $out .= '
                    <li>
                            <a href="#" onclick="'.$actionKey.';">
                                '.$actionValue.'
                            </a>
                    </li>';
                }
                $out .= '
                </ul>
            </div>';
            }
            $out .= '
            
        </div>';
        if ($page_total>0) {
        	$out .= '
        <div class="col-lg-6">
            
            <span class="pagination">
                '.$obj->l('Display:', 'myownutils').' 
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    '.$page_cnt.'
                    <i class="icon-caret-down"></i>
                </button>
                <input type="hidden" id="pagination" name="pagination" value="'.$page_cnt.'">
                <ul class="dropdown-menu">
                    <li>
                        <a href="javascript:$(\'#pagination\').val(20);$(\'#deliveries_form\').submit();" class="pagination-items-page" data-items="20" data-list-id="cart">20</a>
                    </li>
                    <li>
                        <a href="javascript:$(\'#pagination\').val(50);$(\'#deliveries_form\').submit();" class="pagination-items-page" data-items="50" data-list-id="cart">50</a>
                    </li>
                    <li>
                        <a href="javascript:$(\'#pagination\').val(100);$(\'#deliveries_form\').submit();" class="pagination-items-page" data-items="100" data-list-id="cart">100</a>
                    </li>
                    <li>
                        <a href="javascript:$(\'#pagination\').val(300);$(\'#deliveries_form\').submit();" class="pagination-items-page" data-items="300" data-list-id="cart">300</a>
                    </li>
                    <li>
                        <a href="javascript:$(\'#pagination\').val(1000);$(\'#deliveries_form\').submit();" class="pagination-items-page" data-items="1000" data-list-id="cart">1000</a>
                    </li>
                </ul>
                / '.$total_items.' '.$obj->l('result(s)', 'myownutils').'
                <input type="hidden" id="cart-pagination-items-page" name="cart_pagination" value="50">
            </span>
            <ul class="pagination pull-right">
                <li '.($page > 0 ? '' : 'class="disabled"').'>
                    <a href="'.($page > 0 ? 'javascript:$(\'#page\').val(0);$(\'#deliveries_form\').submit();' : '').'" class="pagination-link" data-page="1" data-list-id="cart">
                        <i class="icon-double-angle-left"></i>
                    </a>
                </li>
                <li '.($page > 0 ? '' : 'class="disabled"').'>
                    <a href="'.($page > 0 ? 'javascript:$(\'#page\').val('.intval($page--).');$(\'#deliveries_form\').submit();' : '').'" class="pagination-link" data-page="0" data-list-id="cart">
                        <i class="icon-angle-left"></i>
                    </a>
                </li>';
            for ($page_item = 1; $page_item < $page_total + 1; ++$page_item) {
                $out .= '
                <li '.($page == ($page_item - 1) ? 'class="active"' : '').'>
                    <a href="javascript:$(\'#page\').val('.($page_item - 1).');$(\'#deliveries_form\').submit();" class="pagination-link" data-page="'.$page_item.'" data-list-id="deliveries">'.$page_item.'</a>
                </li>';
            }
            $out .= '
                <li '.($page < $page_total - 1 ? '' : 'class="disabled"').'>
                    <a href="'.($page < $page_total - 1 ? 'javascript:$(\'#page\').val('.($page + 1).');$(\'#deliveries_form\').submit();' : '').'" class="pagination-link" data-page="2" data-list-id="cart">
                        <i class="icon-angle-right"></i>
                    </a>
                </li>
                <li '.($page < $page_total - 1 ? '' : 'class="disabled"').'>
                    <a href="'.($page < $page_total - 1 ? 'javascript:$(\'#page\').val('.($page_total - 1).');$(\'#deliveries_form\').submit();' : '').'" class="pagination-link" data-page="2" data-list-id="cart">
                        <i class="icon-double-angle-right"></i>
                    </a>
                </li>
            </ul>
        </div>';
        }
        $out .= '
    </div>';

            return $out;
        }


	public static function displaySaveButton($obj, $name, $last = false)
	{
		if (_PS_VERSION_ < '1.6.0.0')
		{
			if ($last) return '
				<div style="text-align:center;width:100%">
							<input type="submit" name="'.$name.'" value="'.ucfirst(myOwnLang::$actions[MYOWN_OPE::SAVE]).'" class="button" />
				</div>';
		}
		else
			return '<div class="panel-footer">
				<button type="submit" class="btn btn-default pull-right" name="'.$name.'"><i class="process-icon-save"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::SAVE]).'</button>
				</div>';
	}

	public static function getInputField($name, $value, $symbol = '', $style, $class = '', $with_taxes = true, $onchange = '', $on_keyup = '', $maxlength = 0, $prefix='')
	{
		$currency = false;
		if ($symbol == '')
		{
			$symbol = self::getCurrency();
			$currency = true;
			$icon = true;
		} else {
			$icon = (stripos($symbol, '<i')!==false);
		}
		if (_PS_VERSION_ >= '1.6.0.0') return '
		<div class="input-group">
			'.($icon ? '<span class="input-group-addon"> '.$symbol.'</span>' : ($prefix != '' ? '<span class="input-group-addon"> '.$prefix.'</span>' : '')).'
			'.($currency ? '<input id="priceType" name="priceType" type="hidden" value="TE">' : '').'
			<input name="'.$name.'" class="form-control" style="'.$style.'" id="'.$name.'" '.($maxlength > 0 ? 'maxlength="'.$maxlength.'" ' : '').' type="text" value="'.$value.'" onKeyup="'.$on_keyup.'" onchange="'.$onchange.';'.($currency ? 'noComma(\''.$name.'\');' : '').'" onkeyup="'.($currency ? '$(\'#priceType\').val(\'T'.($with_taxes ? 'I' : 'E').'\');if (isArrowKey(event)) return;':'').'">
			'.(!$icon ? '<span class="input-group-addon"> '.$symbol.'</span>' : '').'
		</div>';
		else return '<input type="text" class="'.$class.'" name="'.$name.'" value="'.$value.'" style="'.$style.'">
		<span style="float:left;margin-left:5px">'.$symbol.'</span>';
	}
	
	public static function getQtyField($obj, $name, $value, $ext='', $min=0)
	{
		return '<div style="display:inline-block">
					<div class="input-group">
						<div class="input-group-btn">
							<a onclick="$(this).parent().parent().find(\'.qty_edit\').val(parseInt(\'\'+$(this).parent().parent().find(\'.qty_edit\').val())+1)" style="width: 20px;padding: 5px 3px;" class="btn btn-default"><i class="icon-caret-up"></i></a>
							<a onclick="$(this).parent().parent().find(\'.qty_edit\').val(($(this).parent().parent().find(\'.qty_edit\').val() > '.$min.' ? $(this).parent().parent().find(\'.qty_edit\').val()-1 : 0))" style="width: 20px;padding: 5px 3px;" class="btn btn-default"><i class="icon-caret-down"></i></a>
						</div>
						<input class="qty_edit" style="width:40px;height:29px;padding: 6px 8px;border-bottom-left-radius:3px;border-top-left-radius:3px;border: 1px solid #C7D6DB;" name="'.$name.'" type="number" size="3" value="'.(int) $value.'" />
						<span class="input-group-addon">'.$ext.'</span>
					</div>
				</div>';
	}
	
	public static function getBoolField($obj, $name, $checked, $label = '', $style = '', $help='', $alert='', $disabled = false)
	{
		if (_PS_VERSION_ < '1.6.0.0') return '
		<label for="timeSlotEnable" class="li-label"></label>
		<input class="addTimeSlotsDay" name="'.$name.'" type="checkbox" value="1" '.($checked ? 'checked' : '').'/>&nbsp;'.$label;
		else
			return ($label != '' ? '
			<label for="timeSlotEnable" class="li-label" style="'.($style != '' ? $style : 'width:70%').';padding-top:7px;padding-left:0px">'.$label.'</label>
			' : '').'

			<span style="display:inline-flex" class="switch prestashop-switch fixed-width-lg">
				<input '.($disabled ? 'disabled' : '').' type="radio" name="'.$name.'" id="'.$name.'_on" value="1" '.($checked ? 'checked="checked"' : '').'>
				<label for="'.$name.'_on" '.($alert != '' ? 'onclick=""' : '').'>'.$obj->l('Yes', 'myownutils').'</label>
				<input '.($disabled ? 'disabled' : '').' type="radio" name="'.$name.'" id="'.$name.'_off" value="0" '.(!$checked ? 'checked="checked"' : '').'>
				<label for="'.$name.'_off">'.$obj->l('No', 'myownutils').'</label>
				<a class="slide-button btn"></a>
			</span>
			'.($help!='' ? '<div class="col-lg-9 col-lg-offset-3"><p class="help-block" style="margin-top: 2px;margin-bottom: 5px;">'.$help.'</p></div>' : '').'
		';
	}

	public static function getBoolItem($bool, $url)
	{
		if (_PS_VERSION_ < '1.6.0.0') return '<a href="'.$url.'" title="">'.($bool ? '<img src="../modules/myownreservations/img/enabled.gif" alt="enabled" />': '<img src="../modules/myownreservations/img/forbbiden.gif" alt="enabled" />').'</a>';
		else return '
		<a class="list-action-enable ajax_table_link '.($bool ? 'action-enabled' : 'action-disabled').'" href="'.$url.'" title="">
			<i class="icon-check '.(!$bool ? ' hidden' : '').'"></i>
			<i class="icon-remove '.($bool ? ' hidden' : '').'"></i>
		</a>';
	}

    public static function table16wrap($obj, $tab, $objet, $title, $count = -1, $actions = '')
    {
        $output = '
            <div class="panel tablepanel col-lg-12 panel'.$tab.'">
               '.self::table16panel($obj, $tab, $objet, $title, $count, $actions).'
            ';

        return $output;
    }

    public static function table16panel($obj, $tab, $objet, $title, $count = -1, $actions = '')
    {
        $output = '
                <div class="panel-heading">
                    '.$title.'                '.($count >= 0 ? '<span class="badge">'.$count.'</span>' : '').$actions.'
                    '.($tab != '' ?
                    '<span class="panel-heading-action">
                            '.($objet != '' ? '<a id="desc-panel-new" class="list-toolbar-btn" '.(stripos($objet, '$') === false ? 'href="'.$obj->getConfUrl($tab, '&edit'.$objet.'=0').'"' : 'onclick="'.$objet.'"').'>
                                <span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="'.ucfirst(myOwnLang::$actions[MYOWN_OPE::ADD]).'" data-html="true">
                                    <i class="process-icon-new "></i>
                                </span>
                            </a>' : '').'
                            <a id="desc-myown-export" class="list-toolbar-btn" href="'.$obj->getAjaxUrl('action=exportConfig&module_tab='.$tab).'" '.(stripos($objet, '$') === false ? 'href="'.$obj->getConfUrl($tab, '&edit'.$objet.'=0').'"' : 'onclick="'.$objet.'"').'>
                                <span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="'.ucfirst(myOwnLang::$actions[MYOWN_OPE::EXPORT]).'" data-html="true">
                                    <i class="process-icon-export "></i>
                                </span>
                            </a>                
                    </span>'
                    : '').'
                </div>
        ';

        return $output;
    }

	public static function getButtonsList($buttons)
	{
		$out = '';
		$isfront = Tools::getIsset('fc');
		$out .= '<ul class="'.(_PS_VERSION_ < '1.6.0.0' ? 'cc_button' : 'nav nav-pills pull-right').'">
		';
		foreach ($buttons as $key => $button)
			$out .= '<li>
						<a style="cursor: pointer;" id="desc-product-'.$key.'" class="toolbar_btn toolbar_btn'.($isfront ? '_front' : '').'_myown" '.(array_key_exists('href', $button) ? 'href="'.$button['href'].'"' : '').' title="'.$button['desc'].'" '.(array_key_exists('js', $button) ? 'onclick="'.$button['js'].'"' : '').'>
							<'.(_PS_VERSION_ < '1.6.0.0' ? 'span' : 'i').' class="'.$button['icon'].' "></'.(_PS_VERSION_ < '1.6.0.0' ? 'span' : 'i').'>
							<'.(_PS_VERSION_ < '1.6.0.0' ? 'div' : 'span').'>'.html_entity_decode($button['desc']).'</'.(_PS_VERSION_ < '1.6.0.0' ? 'div' : 'span').'>
						</a>
					</li>';
		$out .= '</ul>
		';
		return $out;
	}

	public static function getDeleteButton($obj, $id, $name = 'availabilities', $field = 'Availability', $args = '')
	{
		$out = '';
		$out .= '
		<div class="btn-group pull-right">
			<a href="'.$obj->getConfUrl($name, '&delete'.$field.'='.$id.$args).'" onclick="if (confirm(\''.$obj->l('Delete item ?', 'utils').'\')){ return true; }else{ event.stopPropagation(); event.preventDefault();};" title="'.ucfirst(myOwnLang::$actions[MYOWN_OPE::DELETE]).'" class="delete btn btn-default">
				<i class="icon-trash"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::DELETE]).'
			</a>
		</div>';
		return $out;
	}

	public static function getListButton($obj, $id, $name = 'place', $field = ' ', $args = '', $anchor = '')
	{
		$out = '';
		if ($field == ' ') $field = $name;
		$out .= '
		<div class="btn-group-action">
			<div class="btn-group pull-right" id="rescheduleActions'.$id.'" style="display:none">
				<input type="checkbox" name="rescheduleNotif"> <i class="icon-bullhorn"></i> '.$obj->l('Notify', 'utils').' 
				<button class="btn btn-default" name="submitReschedule" onclick="rescheduleExecute('.$id.');">
					<i class="icon-pencil"></i>
					'.ucfirst(myOwnLang::$actions[MYOWN_OPE::EDIT]).'
				</button>
				<a href="#" onclick="rescheduleCancel('.$id.');" class="cancel_shipping_number_link btn btn-default">
					<i class="icon-remove"></i>
					'.ucfirst(myOwnLang::$actions[MYOWN_OPE::CANCEL]).'
				</a>
			</div>
			<div class="btn-group pull-right baseActions" id="baseActions'.$id.'">
				<a '.($name == 'deliveries' ? 'onclick="rescheduleAction('.$id.');"' : 'href="'.$obj->getConfUrl($name, '&edit'.$field.'='.$id.$args, $anchor).'"').' title="'.ucfirst(myOwnLang::$actions[MYOWN_OPE::EDIT]).'" class="edit btn btn-default">
					<i class="icon-pencil"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::EDIT]).'
				</a>
				<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					<i class="icon-caret-down"></i>&nbsp;
				</button>
				<ul class="dropdown-menu">
					'.($name != 'deliveries' ? '
					<li>
						<a href="'.$obj->getConfUrl($name, '&edit'.$field.'='.$id.$args, $anchor).'" class="" title="'.$obj->l('Details', 'utils').'">
							<i class="icon-eye-open"></i> '.$obj->l('Details', 'utils').'
						</a>
					</li>
					<li>
						<a href="'.$obj->getConfUrl($name, '&delete'.$field.'='.$id.$args).'" onclick="if (confirm(\''.$obj->l('Delete item ?', 'utils').'\')){ return true; }else{ event.stopPropagation(); event.preventDefault();};" title="'.ucfirst(myOwnLang::$actions[MYOWN_OPE::DELETE]).'" class="delete">
							<i class="icon-trash"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::DELETE]).'
						</a>
					</li>
					' : '
					<li>
						<a href="#" onclick="rescheduleAction('.$id.');" title="'.ucfirst(myOwnLang::$actions[MYOWN_OPE::EDIT]).'" class="reschedule">
							<i class="icon-calendar-o"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::EDIT]).'
						</a>
					</li>
					<li>
						<a href="?tab=AdminOrders&id_order='.$id.'&vieworder&token='.Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$obj->context->cookie->id_employee).'" class="" title="'.$obj->l('Details', 'utils').'">
							<i class="icon-eye-open"></i> '.ucfirst(myOwnLang::$object[MYOWN_OBJ::ORDER]).'
						</a>
					</li>
					<li>
						<a href="'.myOwnReservationsController::getAdminUrl('validationStatus=1&deliveryId='.$id).'" title="'.$obj->l('Validate', 'utils').'" class="">
							<i class="icon-check-circle-o "></i> '.$obj->l('Validate', 'utils').'
						</a>
					</li>
					').'
					'.($name == 'place' ? '<li>
						<a href="'.$obj->getConfUrl('places', '&editTimeSlot=0&'.$name.'='.$id.'&'.$args, $anchor).'"  title="'.ucfirst(myOwnLang::$actions[MYOWN_OPE::NNEW]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::TIMESLOT]).'" class="delete">
							<i class="icon-clock-o"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::NNEW]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::TIMESLOT]).'
						</a>
					</li>' : '').'
				</ul>
			</div>
		</div>';
		return $out;
	}

	public static function dispayKpi($obj, $kpis = array())
	{
		$output = '';
		$output .= '
		<div class="panel kpi-container">
			<div class="row">';
			foreach ($kpis as $kpi)
			{
				$output .= '
				<div class="col-sm-6 col-lg-3">
					<div id="box-conversion-rate" class="box-stats '.$kpi['color'].'">
						<div class="kpi-content">
							<i class="'.$kpi['icon'].'"></i>
					
							<span class="title">'.$kpi['title'].'</span>
							<span class="subtitle">'.$kpi['subtitle'].'</span>
							<span class="value">'.$kpi['value'].'</span>
						</div>
					</div>
				</div>	';
			}
			$output .= '
			</div>			
		</div>';
		return $output;
	}

	public static function getLanguageFlags($obj, $func= 'changelanguageMyOwn', $name = 'ReservationMsg')
	{
		$default_language = (int)Configuration::get('PS_LANG_DEFAULT');
		$default_language = new Language((int)$default_language);
		$languages = Language::getLanguages();
		if (_PS_VERSION_ >= '1.6.0.0')
		{
			$out = '
			<div style="width:70px" class="col-lg-2">
				<button id="'.$name.'" type="button" class="languageMyOwn btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
					'.$default_language->iso_code.'
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu">';
				foreach ($languages as $language)
					$out .= '
					<li>
						<a href="javascript:'.$func.'(\''.$name.'\', \''.$name.'\', '.$language['id_lang'].', \''.$language['iso_code'].'\');">'.$language['name'].'</a>
					</li>';
				$out .= '
				</ul>
			</div>';
			return $out;
		}
		else
			return str_replace('changeLanguage', $func, $obj->displayFlags($languages, $default_language, $name, $name, true));
	}

	public static function insertDatepicker($obj, $id, $time = false, $args='', $ajax=false)
	{
		$out = '';
		$lang_iso = Language::getIsoById($obj->getContext()->cookie->id_lang);
		if (_PS_VERSION_ >= '1.5.0.0')
		{
			$out .= '<link href="'.__PS_BASE_URI__.'js/jquery/ui/themes/base/jquery.ui.datepicker.css" rel="stylesheet" type="text/css" media="all">';
			$out .= '<link href="'.__PS_BASE_URI__.'js/jquery/ui/themes/base/jquery.ui.theme.css" rel="stylesheet" type="text/css" media="all">';
			if (_PS_VERSION_ < '1.6.0.0')
			{
				$out .= '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/ui/jquery.ui.core.min.js"></script>';
				$out .= '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/ui/jquery.ui.datepicker.min.js"></script>';
				$out .= '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/ui/i18n/jquery.ui.datepicker-'.$lang_iso.'.js"></script>';
			}
			$out .= '<script type="text/javascript" defer>';
			if (is_array($id))
				foreach ($id as $id2)
					$out .= self::myBindDatepicker($id2, $time, $args, $ajax);
			else
				$out .= self::myBindDatepicker($id, $time, $args, $ajax);
			$out .= '</script>';
		}
		else
			$out .= str_ireplace('z-index: 3', 'z-index: 9000', includeDatepicker($id, $time = false));
		return $out;
	}

	public static function myBindDatepicker($id, $time, $args, $ajax=false)
	{
		$out = '';
		if ($time)
		$out .= '
			var dateObj = new Date();
			var hours = dateObj.getHours();
			var mins = dateObj.getMinutes();
			var secs = dateObj.getSeconds();
			if (hours < 10) { hours = "0" + hours; }
			if (mins < 10) { mins = "0" + mins; }
			if (secs < 10) { secs = "0" + secs; }
			var time = " "+hours+":"+mins+":"+secs;';

		if (!$ajax) $out .= '
		document.addEventListener("DOMContentLoaded", function(event) {';
		$out .= '
			$("#'.$id.'").datepicker({
				prevText:"",
				nextText:"",
				'.$args.'
				'.($args=='' ? 'dateFormat:"yy-mm-dd"'.($time ? '+time' : '') : '').'});';
		if (!$ajax) $out .= '
		});';
		return $out;
	}

	/*-----------------------------------------------------------------------------------------------------------------
	PS utils
	-----------------------------------------------------------------------------------------------------------------*/

	public static function getCurrency()
	{
		if (_PS_VERSION_ < '1.5.0.0')
			return Currency::getCurrent()->sign;
		else
			return Context::getContext()->currency->sign;
	}
	
	public static function getCurrencyIso()
	{
		if (_PS_VERSION_ < '1.5.0.0')
			return Currency::getCurrent()->iso_code;
		else
			return Context::getContext()->currency->iso_code;
	}

	public static function getCurrencyObj()
	{
		if (_PS_VERSION_ < '1.5.0.0')
			return Currency::getCurrent();
		else
			return Context::getContext()->currency;
	}

	public static function getShop()
	{
		if (_PS_VERSION_ >= '1.5.0.0' && Shop::getContext() == Shop::CONTEXT_SHOP)
		{
			$context = Context::getContext();
			$id_shop = $context->shop->id;
			if (defined('MYOWNRES_SEPARATE_SHOP') && MYOWNRES_SEPARATE_SHOP && $id_shop==1) return 0;
			else return $id_shop;
		}
		else
			return 0;
	}
	
	public static function getShopUrl()
	{
		if (_PS_VERSION_ >= '1.5.0.0' && Shop::getContext() == Shop::CONTEXT_SHOP)
		{
			$context = Context::getContext();
			return $context->shop->domain;
		}
		else
			return '';
	}

	public static function getPath($obj, $tpl)
    {
        if (Tools::file_exists_cache(_PS_THEME_DIR_.'modules/'.$obj->name.'/templates/'.$tpl)) {
            return _PS_THEME_DIR_.'modules/'.$obj->name.'/templates/'.$tpl;
        } else {
            return _PS_MODULE_DIR_.$obj->name.'/views/templates/hooks/'.$tpl;
        }
    }

	public static function checkImageUploadError($file)
	{
		if ($file['error'])
		{
			switch ($file['error'])
			{
				case 1:
					return Tools::displayError('The file is too large.');

				case 2:
					return Tools::displayError('The file is too large.');

				case 3:
					return Tools::displayError('The file was partialy uploaded');

				case 4:
					return Tools::displayError('The file is empty');
			}
		}
	}

	public static function gpsDistance($lat1, $lng1, $lat2, $lng2)
	{
		if ($lat1 == $lat2 && $lng1 == $lng2) return 0;
			$earth_radius = 6378137;   // Terre = sphère de 6378km de rayon
			$rlo1 = deg2rad($lng1);
			$rla1 = deg2rad($lat1);
			$rlo2 = deg2rad($lng2);
			$rla2 = deg2rad($lat2);
			$dlo = ($rlo2 - $rlo1) / 2;
			$dla = ($rla2 - $rla1) / 2;
			$a = (sin($dla) * sin($dla)) + cos($rla1) * cos($rla2) * (sin($dlo) * sin($dlo
		));
		$d = 2 * atan2(sqrt($a), sqrt(1 - $a));
		return ($earth_radius * $d);
	}

	public static function getEmployees($profile=0)
	{
		$employees_out=array();
		$employees = Db::getInstance()->ExecuteS('
		SELECT *
		FROM `'._DB_PREFIX_.'employee`
		WHERE `active` = 1
		'.(!$profile ? 'AND `id_profile` >1' : ($profile>1 ? 'AND `id_profile` ='.(int)$profile : '')).'
		ORDER BY `email`');
		foreach ($employees as $employee)
			$employees_out[$employee['id_employee']] = $employee;
		return $employees_out;
	}

	public static function recurseCategory($categories, $current, $id_category = 1, $id_selected = 1)
	{
		$output = '';
		$output .= '<option value="'.$id_category.'"'.(($id_selected == $id_category) ? ' selected="selected"' : '').'>'.
		str_repeat('&nbsp;', $current['infos']['level_depth'] * 5).self::hideCategoryPosition(Tools::stripslashes($current['infos']['name'])).'</option>';
		if (isset($categories[$id_category]))
			foreach (array_keys($categories[$id_category]) as $key)
				$output .= self::recurseCategory($categories, $categories[$id_category][$key], $key, $id_selected);
		return $output;
	}

	public static function hideCategoryPosition($name)
	{
		return preg_replace('/^[0-9]+\./', '', $name);
	}

	/*-----------------------------------------------------------------------------------------------------------------
	Carrier utils
	-----------------------------------------------------------------------------------------------------------------*/

	public static function isCarrierActive($id_carrier)
	{
		$carriers = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'carrier` WHERE `id_carrier`='.(int)$id_carrier);
		if (count($carriers) == 0)
			return false;
		else
		{
			$tempc = $carriers[0];
			if ($tempc['deleted'] == 1) return false;
			if ($tempc['active'] == 0) return false;
			return true;
		}
	}

	public static function getCarrierName($id_carrier)
	{
		$carriers = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'carrier` WHERE `id_carrier`='.(int)$id_carrier);
		if (count($carriers) == 0)
			return '';
		else
		{
			$tempc = $carriers[0];
			return $tempc['name'];
		}
	}

	public static function getCarrierDelay($id_carrier, $id_lang)
	{
		$carriers = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'carrier_lang` WHERE `id_carrier`='.(int)$id_carrier.' AND `id_lang`='.(int)$id_lang);
		if (count($carriers) == 0)
			return '';
		else
		{
			$tempc = $carriers[0];
			return $tempc['delay'];
		}
	}

	public static function listCarriers($full=false)
	{
		return Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'carrier`'.(!$full ? ' WHERE `deleted` = 0': ''));
	}

	public static function attachCarrier($id, $module)
	{
		if ((int)$id > 0)
			return Db::getInstance()->Execute('UPDATE `'._DB_PREFIX_.'carrier` SET `is_module` = 1, `shipping_external` = 1, `need_range` = 1, `external_module_name` = "'.$module.'" WHERE `id_carrier` = '.$id.' LIMIT 1;');
		return true;
	}

	public static function dettachCarrier($id)
	{
		if ((int)$id > 0)
			return Db::getInstance()->Execute('UPDATE `'._DB_PREFIX_.'carrier` SET `is_module` = 0, `shipping_external` = 0, `need_range` = 0, `external_module_name` = "" WHERE `id_carrier` = '.(int)$id.' LIMIT 1;');
		return true;
	}

	public static function isCarrierWeightRangeAvailable($id)
	{
		$range = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'range_weight` WHERE `id_carrier` = '.(int)$id);
		return (count($range) > 0);
	}

	public static function isCarrierPriceRangeAvailable($id)
	{
		$range = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'range_price` WHERE `id_carrier` = '.(int)$id);
		return (count($range) > 0);
	}

	public static function getCarrierFromOrder($order_id)
	{
		$s = Db::getInstance()->ExecuteS('SELECT `id_carrier` FROM `'._DB_PREFIX_.'orders` WHERE `id_order` ='.(int)$order_id.';');
		if (count($s) > 0) return $s[0]['id_carrier'];
		else return 0;
	}

	/*-----------------------------------------------------------------------------------------------------------------
	Orders utils
	-----------------------------------------------------------------------------------------------------------------*/

	public static function getOrdersList()
	{
		$orders = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'orders` ORDER BY id_order DESC LIMIT 1000');
		return $orders;
	}

	public static function getCustomersList()
	{
		$customers = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'customer` ORDER BY lastname');
		return $customers;
	}

	public static function getAddressesByCustomer($id_customer)
	{
		$orders = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'address` WHERE active=1 AND deleted=0 AND id_customer = '.$id_customer);
		return $orders;
	}

	public static function getStatusName($id_order_state, $lang)
	{
		if ((int)$id_order_state > 0)
		{
			$order_statuses = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'order_state_lang` WHERE `id_order_state` = '.(int)$id_order_state.' AND `id_lang` = '.$lang);
			$temp = $order_statuses[0];
			return $temp['name'];
		}
		else
			return '?';
	}

	public static function getStatusColor($id_order_state)
	{
		if ((int)$id_order_state > 0)
		{
			$order_statuses = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'order_state` WHERE `id_order_state` = '.(int)$id_order_state);
			if (count($order_statuses) == 0)
				return '';
			else
			{
				$temp = $order_statuses[0];
				return $temp['color'];
			}
		}
	}

	public static function getStatusList($langue)
	{
		return Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'order_state_lang` WHERE `id_lang` = '.(int)$langue);
	}

	public static function setOrderDeliveryAddress($id, $id_address)
	{
		$req = 'UPDATE `'._DB_PREFIX_.'orders` SET  `id_address_delivery` =  "'.$id_address.'" WHERE  `id_order` ="'.$id.'";';
		return Db::getInstance()->Execute($req);
	}

	/*-----------------------------------------------------------------------------------------------------------------
	Module utils
	-----------------------------------------------------------------------------------------------------------------*/

	public static function isCartSelection($id_cart)
	{
		for ($i = 0; $i < 4; $i++)
			if (trim(self::getCartSelection($id_cart, $i)) != '') return true;
		return false;
	}

	public static function getCartIdFromOrder($order_id)
	{
		$orders = Db::getInstance()->ExecuteS('SELECT `id_cart` FROM `'._DB_PREFIX_.'orders` WHERE `id_order` ='.(int)$order_id.';');

		if (count($orders) > 0)
			return $orders[0]['id_cart'];
		else
			return '';
	}

	public static function setOrderDeliveryDate($id, $date, $time)
	{
		$order = new Order((int)$id);
		$order->delivery_date = $date.' '.$time;
		$order->update();
	}

	public static function deleteUserGroup($id)
	{
		$req = 'DELETE FROM `'._DB_PREFIX_.'customer_group` WHERE `id_customer` = '.(int)$id;
		return Db::getInstance()->Execute($req);
	}

	public static function desactivateUser($id)
	{
		$req = 'UPDATE `'._DB_PREFIX_.'customer` SET  `active` = 0 WHERE  `id_customer`='.(int)$id;
		return Db::getInstance()->Execute($req);
	}

	public static function displayKpi($kpis = array())
	{
		$output = '';
		$output .= '
		<div class="panel kpi-container">
			<div class="row">';
			foreach ($kpis as $kpi)
			{
				$output .= '
				<div class="col-sm-6 col-lg-3">
					<div id="box-conversion-rate" class="box-stats '.$kpi['color'].'">
						<div class="kpi-content">
							<i class="'.$kpi['icon'].'"></i>
					
							<span class="title">'.$kpi['title'].'</span>
							<span class="subtitle">'.$kpi['subtitle'].'</span>
							<span class="value">'.$kpi['value'].'</span>
						</div>
					</div>
				</div>	';
			}
			$output .= '
			</div>			
		</div>';
		return $output;
	}

	public static function supprimerAccent($cc)
	{
		$cc = str_replace(	array(
								'à', 'â', 'ä', 'á', 'ã', 'å',
								'î', 'ï', 'ì', 'í',
								'ô', 'ö', 'ò', 'ó', 'õ', 'ø',
								'ù', 'û', 'ü', 'ú',
								'é', 'è', 'ê', 'ë',
								'ç', 'ÿ', 'ñ', 'ý',
							),
							array(
								'a', 'a', 'a', 'a', 'a', 'a',
								'i', 'i', 'i', 'i',
								'o', 'o', 'o', 'o', 'o', 'o',
								'u', 'u', 'u', 'u',
								'e', 'e', 'e', 'e',
								'c', 'y', 'n', 'y',
							),
							$cc
						);
		return $cc;
	}

	public static function _getFormatedAddress(Address $the_address, $line_sep, $fields_style = array())
	{
		return AddressFormat::generateAddress($the_address, array('avoid' => array()), $line_sep, ' ', $fields_style);
	}

	public static function nl2br($str)
	{
		return str_replace(array('\r\n', '\r', '\n'), '<br />', $str);
	}

	/*-----------------------------------------------------------------------------------------------------------------
	Get postalcode format from id
	-----------------------------------------------------------------------------------------------------------------*/
	public static function getPostCodeFormat($id, $langue)
	{
		if ($id > 0)
		{
			$req = 'SELECT * FROM `'._DB_PREFIX_.'myowndeliveries_postcodestype` LEFT JOIN  `'._DB_PREFIX_.'country_lang` ON ('._DB_PREFIX_.'myowndeliveries_postcodestype.id_postCodeType = '._DB_PREFIX_.'country_lang.id_country) WHERE id_lang = '.$langue.' AND `id_postCodeType` = '.$id.' LIMIT 1';
			$post_code_formats = Db::getInstance()->ExecuteS($req);
			return $post_code_formats[0];
		}
		else
		{
			if ($langue == 2) return array('id_postCodeType' => 0, 'name' => 'Pas de contrôle', 'format' => '1 à 10 caractères alphanumériques max', 'regexp' => '^([0-9A-Z \-]{1,10})$');
			else return array('id_postCodeType' => 0, 'name' => 'No control', 'format' => '1 to 10 alphanumeric chars max', 'regexp' => '^([0-9A-Z \-]{1,10})$');
		}
	}

	public static function isHookForModule($hook_name, $id_module)
	{
		if (version_compare(_PS_VERSION_, '1.6.0.11', '>=') && strtolower($hook_name) == 'displayheader')
			$hook_name = 'header';
		if (version_compare(_PS_VERSION_, '1.7.1.0', '>=') && strtolower($hook_name) == 'displayproductbuttons')
			$hook_name = 'displayproductadditionalinfo';

		if (version_compare(_PS_VERSION_, '1.7.0.0', '>=') && strtolower($hook_name) == 'actionajaxdiebefore')
			$hook_name = 'actionbeforeajaxdie';
		$result = Db::getInstance()->ExecuteS('
		SELECT *
		FROM `'._DB_PREFIX_.'hook` as h
		LEFT JOIN `'._DB_PREFIX_.'hook_module` as hm ON (hm.id_hook = h.id_hook)  
		WHERE `name` = "'.pSQL($hook_name).'" AND `id_module`='.$id_module);

		if (count($result) == 0) return false;
		else return $result[0];
	}
	
    public static function isFieldInTable($table, $field)
    {
        $fieldfound = false;
        $columns = Db::getInstance()->ExecuteS(' SHOW COLUMNS FROM '._DB_PREFIX_.$table);
        foreach ($columns as $column) {
            if ($column['Field'] == $field) {
                $fieldfound = true;
            }
        }

        return $fieldfound;
    }
}

?>