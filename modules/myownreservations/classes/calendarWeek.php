<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnCalendarWeek {
	public $year;
	public $week;
	public $_dayOfWeek;
	public $_days;
	public $_filteredDays;
	//public $days;
	
	public function __construct($week, $year, $dayofweek=null)
	{
		$this->week = $week;
		$this->year = $year;
		$this->_dayOfWeek = $dayofweek;
	}
	
	//Getting day from day number of week
	//-----------------------------------------------------------------------------------------------------------------
	public function getDay($dayNb) {
		return self::getDayOfWeek($dayNb, $this->week, $this->year);
	}
	
	public function getDayObj($dayNb) {
		return new myOwnCalendarDay(self::getDayOfWeek($dayNb, $this->week, $this->year));
	}
	
	public function getStart() {
		return $this->getDay(1);
	}
	
	public function getKey() {
		return $this->year.'-'.intval($this->week);
	}
	
	public function getSelection() {
		return $this->getDay($this->_dayOfWeek);
	}
	
	public function getDaySelection() {
		return $this->_days[date('Y-m-d', $this->getDay($this->_dayOfWeek))];
	}
	
	public function getEnd() {
		return strtotime('+1 day',$this->getDay(7));
	}
	
	//Getting days dates from week
	//-----------------------------------------------------------------------------------------------------------------
	public function getDays($enableWeekEnd = true) {
		if (strlen($this->week)==1) $this->week="0".$this->week;
		$max=5;
		if ($enableWeekEnd) $max=7;
		$ref = strtotime($this->year."W".$this->week);
		if (intval($this->week) == 0) {
			$ref = strtotime("-1 week",strtotime($this->year."W01"));
		}
		for($i=0; $i<$max; $i++) {
			$dayList[$i] = strtotime("+".$i."day",$ref);
		}
		return $dayList;
	}
	
	/*public function __get($property) {
		if (property_exists($this, $property)) {
			if ($property=='_days' && !$this->_days)
				$this->_days = $this->getDaysObj();
			
			echo 'test_days';
			return $this->$property;
		}
	}*/
	
	public function getDaysObj($enableWeekEnd = true) {
		$this->_days = array();
		if (strlen($this->week)==1) $this->week="0".$this->week;
		$max=5;
		if ($enableWeekEnd) $max=7;
		$ref = strtotime($this->year."W".$this->week);
		if (intval($this->week) == 0) {
			$ref = strtotime("-1 week",strtotime($this->year."W01"));
		}
		for($i=0; $i<$max; $i++) {
			$this->_days[date('Y-m-d', strtotime("+".$i."day",$ref))]=new myOwnCalendarDay(strtotime("+".$i."day",$ref));
		}
		
		return $this->_days;
	}
	
	public function getTimeSlices($view, $type) {
		$slices = array();
		$enableWeekEnd = $view->timeslotsObj->isWeekEndDays();
		$tscount=count($view->timeslotsObj->list);
		$end = $this->getEnd();
		if ($view->end<$end) $end = $view->end;
		$start = $this->getStart();
		if ($view->start>$start) $start = $view->start;

		foreach ($this->getDaysObj($enableWeekEnd) as $dayKey => $dayObj)
		{
			foreach ($view->timeslotsObj->getTimeSlotsOfDay($dayObj->date, $type) AS $timeSlot)
			{
				if ( $timeSlot->getEndHourTime($dayObj->date)<=$end
					&& $timeSlot->getStartHourTime($dayObj->date)>=$start)
					{
					$slice = new myOwnCalendarTimeSlice($dayObj->date, $timeSlot, $type, $view);
					//if more than one timeslice by starttime (case of home widget with mixed ts)
					$comp = '';
					while (array_key_exists(date('Y-m-d', $dayObj->date).' '.$timeSlot->startTime.$comp, $slices)) {
						$comp.='0';
					}
					$slices[date('Y-m-d', $dayObj->date).' '.$timeSlot->startTime.$comp] = $slice;
					$this->_days[$dayKey]->_slices[$timeSlot->sqlId] = $slice;
				}
			}
			//adding day the day if enabled timeslots in it

			if (count($this->_days[$dayKey]->_slices))
				 $this->_filteredDays[date('Y-m-d', $dayObj->date)] = $this->_days[$dayKey];

							 
			 //create index slice for month view resume
			if ( $view->potentialResa->_mainProduct->isTimeSelect() 
				 && $dayObj->getStart()<=$end
				 && $dayObj->getEnd()>=$start)
			{

				$slice = new myOwnCalendarTimeSlice($dayObj->date);
				$slice->enable=count($this->_days[$dayKey]->_slices)>0;
				$slices[date('Y-m-d', $dayObj->date)] = $slice;
				//adding index timeslice if timeslices in the day

				if (count($this->_days[$dayKey]->_slices))
					$this->_days[$dayKey]->_slices[-1] = $slice;
					
			}
		}
		//$this->_weeks[$weekKey]=$this;
		return $slices;
	}
	
	//Getting day from day number of week
	//-----------------------------------------------------------------------------------------------------------------
	public static function getDayOfWeek($dayNb, $week, $year) {
		if (strlen($week)==1) $week='0'.$week;
		$ref = strtotime($year."W".$week);
		
		if (intval($week) == 0) {
			$ref = strtotime("-1 week",strtotime($year."W01"));
		}
		return strtotime("+".($dayNb-1)."day",$ref);
	}
	
	public function toString($otherWeek=null, $lbl='') {
		global $cookie;
		if ($otherWeek!=null) return ucfirst(myOwnLang::getUnit(reservation_unit::WEEK, true)).' '.MyOwnCalendarTool::formatWeek($this->getStart()).' & '.$otherWeek->week.' '.$this->year;
		else if ($lbl=='') return ucfirst(myOwnLang::getUnit(reservation_unit::WEEK, false)).' '.MyOwnCalendarTool::formatWeek($this->getStart()).' '.$this->year;
		else return ucfirst($lbl).' '.MyOwnCalendarTool::formatDateShort($this->getStart(), $cookie->id_lang);
	}
	
	//Getting week
	//-----------------------------------------------------------------------------------------------------------------
	public static function getOnDate($weekIndex) {
		return new myOwnCalendarWeek(date('W',$weekIndex), date('o',$weekIndex), null);
	}

}
	
?>