<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnResas {
	var $list=array();
	var $id_order;

    protected $webserviceParameters = array(
        'fields' => array(
                'sqlId' => array(),
                'id_order' => array(),
                'id_shop' => array(),
                'id_product' => array(),
                'id_product_attribute' => array(),
                'id_customization' => array(),
                'id_object' => array(),
                'reference' => array(),
                'quantity' => array(),
                'quantity_refunded' => array(),
                'start_date' => array('setter' => 'setStartDate', 'getter' => 'getStartDate'),
                'end_date' => array('setter' => 'setEndDate'),
                'start_timeslot' => array('setter' => 'setStartTimeslot'),
                'end_timeslot' => array('setter' => 'setEndTimeslot'),
                'validated' => array(),
                'price' => array(),
                'tax_rate' => array(),
                'advance' => array(),
                'discounts' => array(),
                'stock' => array(),
                'external_fields' => array(),
                'comment' => array(),
            ),
        'hidden_fields' => array(
            'discounts',
            'stock',
            'comment',
        ),
    );
	
	public function __construct($id_order=0, $_products=null, $_pricerules=null, $_timeslots=null)
	{
		//$this->def=self::$definition;
		if (intval($id_order) > 0) {
			$this->id_order = $id_order;
			$this->list = myOwnResas::getProducts($id_order, $_products, $_pricerules, $_timeslots);
		}
	}
	
	public function getWebserviceParameters($ws_params_attribute_name = null)
    {
        $default_resource_parameters = array(
            'objectSqlId' => $this->def['primary'],
            'objectNodeName' => 'reservations',
            'objectsNodeName' => 'reservations',
            'retrieveData' => array(
                'className' => 'myOwnReservation',//get_class($this),
                'retrieveMethod' => 'getWebserviceObjectList',
                'params' => array(),
                'table' => $this->def['table'],
            ),
            'fields' => array(
                'id' => array('sqlId' => $this->def['primary'], 'i18n' => false),
            ),
        );
        
        if ($ws_params_attribute_name === null) {
            $ws_params_attribute_name = 'webserviceParameters';
        }

        $resource_parameters = array_merge_recursive($default_resource_parameters, $this->{$ws_params_attribute_name});

        return $resource_parameters;
	}
	
	public static function getFromXml($platform_key, $id_product, $inline) {
		$reservations = array();
		$id_shop = myOwnUtils::getShop();
		$xml = new SimpleXMLElement($inline);
		$platform_refs = array();
		if (is_array($id_product)) {
			foreach ($id_product as $id) {
				$syncref = Configuration::get('MYOWNRES_SYNCREF_'.$platform_key, $id);
				if($syncref!='') {
					$syncrefTab=explode(';', $syncref);
					$platform_refs[(int)$syncrefTab[0]] = $id;
				}
			}
		}
		
		if ($platform_key == sync_platforms::EXPEDIA) {
			$events = $xml['Bookings'];
			
			foreach ($xml->Bookings->Booking as $booking) {
				if ((int)$booking->Hotel['id'] == (int)Configuration::get('MYOWNRES_SYNC_EQC_HOTELID')) {
					if ($booking['status'] == 'retrieved' || $booking['status'] == 'pending') {
						//print_r($booking);
						if (isset($booking['confirmNumber']) && (string)$booking['type']!='Modify') {
							$issaved = self::isReservationByReference(strtoupper((string) $booking['confirmNumber']));
							if ($issaved) {
								$tempResa = new myOwnReservation($issaved);
								$tempResa->validated = ((string)$booking['type']!="Cancel" ? 1 : 0);
								$reservations[$tempResa->reference] = $tempResa;
							}
						} 
						else if (!is_array($id_product) || array_key_exists((int)$booking->RoomStay['roomTypeID'], $platform_refs)) {
							$issaved = false;
							if (isset($booking['confirmNumber']))
								$issaved = self::isReservationByReference(strtoupper((string) $booking['confirmNumber']));
							if ($issaved)
								$tempResa = new myOwnReservation($issaved);
							else $tempResa = new myOwnReservation();
							$tempResa->id_order = 0;
							$tempResa->id_shop = $id_shop;
							$tempResa->validated = ((string)$booking['type']!="Cancel" ? 1 : 0);
							$tempResa->id_product = (!is_array($id_product) ? $id_product : $platform_refs[(int)$booking->RoomStay['roomTypeID']]);
							$tempResa->id_source = $platform_key;
							$tempResa->startDate = (string)$booking->RoomStay->StayDate['arrival'];
							$tempResa->startTime = '00:00:00';
							$tempResa->startTimeslot = 0;
							$tempResa->endDate = (string)$booking->RoomStay->StayDate['departure'];
							$tempResa->endTime = '00:00:00';
							$tempResa->endTimeslot = 0;
							$tempResa->external_fields = $booking->PrimaryGuest->Name['givenName'].' '.$booking->PrimaryGuest->Name['surname'].' ('.$booking->PrimaryGuest->Phone['cityAreaCode'].$booking->PrimaryGuest->Phone['number'].$booking->PrimaryGuest->Phone['countryCode'].', '.(string)$booking->PrimaryGuest->Email.')';
							$tempResa->ref_order = (int)$booking['id'];
							$tempResa->reference = (isset($booking['confirmNumber']) ? $booking['confirmNumber'] : strtoupper(substr(md5((int)$booking['id']), -8)));
							$comment = '';
							foreach ($booking->SpecialRequest as $extra) {
								if ($tempResa->comment!='') $tempResa->comment.=', ';
								$tempResa->comment .= (string)$extra;
							}	
							$tempResa->advance = $booking->Total['amountAfterTaxes'];

							$reservations[$tempResa->reference] = $tempResa;
						}

					}
				}
			}
		}
		return $reservations;
		
	}
	
	public static function getFromICal($platform_key, $id_product, $inline) {
		$reservations = array();
		$inline = str_ireplace('DTSTART;VALUE=DATE', 'DTSTART,VALUE=DATE', str_ireplace('DTEND;VALUE=DATE', 'DTEND,VALUE=DATE', $inline));
		$events = explode('BEGIN:VEVENT', $inline);
		unset($events[0]);
		
		foreach ($events as $event) {
			if (stripos($event, 'DESCRIPTION:')!==false || $platform_key != 2) {
				$tempResa = new myOwnReservation();
				$tempResa->id_order = 0;
				$tempResa->validated = 1;
				$tempResa->id_product = $id_product;
				$tempResa->id_source = $platform_key;
				$eventsdetails=explode(';', $event);
				foreach ($eventsdetails as $eventsdetail) {
					$eventsdetailtab = explode(':', $eventsdetail);
					if ($eventsdetailtab[0]=='UID') {
						if (stripos($eventsdetailtab[1], '@')!==false) {
							$tmpref=explode('@', $eventsdetailtab[1]);
							//echo '_'.$tmpref[0].$tempResa->id_product.$tempResa->startDate.$tempResa->startTime.$tempResa->endDate.$tempResa->endTime.'_';
							$tempResa->reference = substr(md5($tmpref[1].$tempResa->id_product.$tempResa->startDate.$tempResa->startTime.$tempResa->endDate.$tempResa->endTime), -8);
							//echo md5($tmpref[0].$tempResa->id_product.$tempResa->startDate.$tempResa->startTime.$tempResa->endDate.$tempResa->endTime);
						} else 
						$tempResa->reference = substr($eventsdetailtab[1], -8);
					}
					if ($eventsdetailtab[0]=='SUMMARY') 
						$tempResa->comment = $eventsdetailtab[1];
					if ($eventsdetailtab[0]=='DTSART,VALUE=DATE') {
						$tempResa->startDate = date('Y-m-d', strtotime($eventsdetailtab[1]));
						$tempResa->startTime = '00:00:00';
						$tempResa->startTimeslot = 0;
					}
					if ($eventsdetailtab[0]=='DTSTART') {
						$datetime=explode('T', $eventsdetailtab[1]);
						$tempResa->startDate = date('Y-m-d', strtotime($datetime[0]));
						$tempResa->startTime = substr($datetime[1], 0, 2).':'.substr($datetime[1], 2, 2).':'.substr($datetime[1], 4, 2);
						$tempResa->startTimeslot = 0;
					}
					if ($eventsdetailtab[0]=='DTEND') {
						$datetime=explode('T', $eventsdetailtab[1]);
						$tempResa->endDate = date('Y-m-d', strtotime($datetime[0]));
						$tempResa->endTime = substr($datetime[1], 0, 2).':'.substr($datetime[1], 2, 2).':'.substr($datetime[1], 4, 2);
						$tempResa->endTimeslot = 0;
					}
					if ($eventsdetailtab[0]=='DTEND,VALUE=DATE') {
						$tempResa->endDate = date('Y-m-d', strtotime($eventsdetailtab[1]));
						$tempResa->endTime = '00:00:00';
						$tempResa->endTimeslot = 0;
					}
				}
				$reservations[$tempResa->reference] = $tempResa;
			}
		}
		
		return $reservations;
	}
	
	public static function sync($obj) {
		global $cookie;
		$default_lang = Configuration::get('PS_LANG_DEFAULT');
		$products = $obj->_products->getProducts($default_lang);
		echo 'sync';
		
		foreach (myOwnLang::$sync_platforms AS $platform_key => $platform_label)
			
			if (Configuration::get('MYOWNRES_SYNC_'.$platform_key)) {
				
				if ($platform_key == sync_platforms::ABRITEL) $url = 'https://www.abritel.fr/icalendar/';
				if ($platform_key == sync_platforms::AIRBNB) $url = 'https://www.airbnb.fr/calendar/ical/';
				
				//getting bookings
				if ($platform_key == sync_platforms::EXPEDIA) {
					$dom = new DOMDocument('1.0', 'utf-8');
					$bookingRetrievalRQ = myOwnReservationsProController::getExpediaElement($dom, 'BookingRetrievalRQ');
					$paramSet = $dom->createElement('ParamSet');
					$status = $dom->createElement('Status');
					$status->setAttribute('value', 'pending');
					$paramSet->appendChild($status);
					$status2 = $dom->createElement('Status');
					$status2->setAttribute('value', 'retrieved');
					$paramSet->appendChild($status2);
					$bookingRetrievalRQ->appendChild($paramSet);

					$dom->appendChild($bookingRetrievalRQ);
					//echo $dom->saveHTML();
					
					$ch = curl_init();
					$url = 'https://services.expediapartnercentral.com/eqc/br';
					curl_setopt( $ch, CURLOPT_URL, $url );
					curl_setopt( $ch, CURLOPT_POST, true );
					curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
					curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
					curl_setopt( $ch, CURLOPT_POSTFIELDS, $dom->saveHTML() );
					$result = curl_exec($ch);
					curl_close($ch);
					//echo $result;
					
					$resas = self::getFromXml(sync_platforms::EXPEDIA, array_keys($products), $result);
			
					$dom = new DOMDocument('1.0', 'utf-8');
					$bookingConfirmRQ = myOwnReservationsProController::getExpediaElement($dom, 'BookingConfirmRQ');
					$confirmNumbers = $dom->createElement('BookingConfirmNumbers');
					$isConfirm = false;
					foreach ($resas as $resa) {
						$issaved = self::isReservationByReference($resa->reference);
						if ($resa->validated) {
							echo '<br>+('.$resa->id_source.')'.$resa->reference.':'.$issaved;
							if (!$issaved)
								$resa->sqlInsert();
							$isConfirm=true;
							$confirmNumber = $dom->createElement('BookingConfirmNumber');
							$confirmNumber->setAttribute('bookingID', $resa->ref_order);
							$confirmNumber->setAttribute('bookingType', 'Book');
							$confirmNumber->setAttribute('confirmNumber', $resa->reference);
							$confirmNumber->setAttribute('confirmTime', date('c'));
							$confirmNumbers->appendChild($confirmNumber);
						} else {
							echo '<br>-('.$resa->id_source.')'.$resa->reference;
							if ($issaved) self::deleteReservation($issaved);
						}
					}
					$bookingConfirmRQ->appendChild($confirmNumbers);
					
					$dom->appendChild($bookingConfirmRQ);
					//echo $dom->saveHTML();

					if ($isConfirm) {
						$ch = curl_init();
						$url = 'https://services.expediapartnercentral.com/eqc/bc';
						curl_setopt( $ch, CURLOPT_URL, $url );
						curl_setopt( $ch, CURLOPT_POST, true );
						curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
						curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch, CURLOPT_POSTFIELDS, $dom->saveHTML() );
						$result = curl_exec($ch);
						curl_close($ch);
						//echo $result;
					}
				}

				foreach ($products as $id_product => $product) {
					$syncref = Configuration::get('MYOWNRES_SYNCREF_'.$platform_key, $id_product);

					if ($syncref!='') {
						$inline='';
						if ($platform_key == sync_platforms::EXPEDIA) {
							if (Configuration::get('MYOWNRES_SYNC_EQC_LOGIN')!='' && Configuration::get('MYOWNRES_SYNC_EQC_PASS')!='' && Configuration::get('MYOWNRES_SYNC_EQC_HOTELID')!='') {
								
								//check if there is a rate id
								$syncrefRateId=0;
								$syncrefTab=explode(';', $syncref);
								if (count($syncrefTab)>1 && trim($syncrefTab[1])!='')
									$syncrefRateId=trim($syncrefTab[1]);
								$syncref = trim($syncrefTab[0]);
																
								//publish bookings
								$dom = new DOMDocument('1.0', 'utf-8');
								$availRateUpdateRQ = myOwnReservationsProController::getExpediaElement($dom, 'AvailRateUpdateRQ');
																
								$mainProduct = $obj->_products->getResProductFromProduct($id_product);
								$existing_resas = self::getFutureReservationsForProductInsteadSource($obj, $id_product, sync_platforms::EXPEDIA);
								
								$flatResasLines = self::getFlatReservationsLines($obj, $existing_resas);
								
								$start = $mainProduct->getReservationStart(); 
								
								$end = strtotime("+".$mainProduct->reservationPeriod." day",$start);
								$days=array();
								$qty=$mainProduct->getProductQuantity($id_product);
								for ($i=1; $i<=$mainProduct->reservationPeriod;$i++) {
									$days[date('Y-m-d', strtotime("+".$i." day",$start))] = $qty;
								}
								//if ($id_product==3) print_r($days);
								foreach($flatResasLines as $line) {
									foreach($line as $flatResa) {
										$rstart = $flatResa->getStartDateTime();
										$rend = $flatResa->getEndDateTime();
										$rend = strtotime(date('Y-m-d 00:00:00', $rend));
										$cnt=0;
										do {
											
											$day = strtotime("+".$cnt." day",$rstart);
											$days[date('Y-m-d', $day)]--;
											//if ($id_product==3) echo '*'.date('Y-m-d', $day);
											$cnt++;
											//echo '<br/>'.date('Y-m-d H:i:s', $day).'<'.date('Y-m-d H:i:s',$rend);
										} while ($day < $rend);
									}
								}

								/*if ($productFamilly->_reservationPriceType==reservation_price::TABLE_DATE) {
									$periods = $productFamilly->getPeriodUnits($cookie, $obj);
								}*/
								//publish rates
								if ($syncrefRateId!=0 && $mainProduct->_reservationPriceType==reservation_price::TABLE_TIME || $mainProduct->_reservationPriceType==reservation_price::PRODUCT_TIME) {
									$availRateUpdate = $dom->createElement('AvailRateUpdate');
									$daterange = $dom->createElement('DateRange');
									$daterange->setAttribute('from', date('Y-m-d', $start));
									$daterange->setAttribute('to', date('Y-m-d', $end));
									$availRateUpdate->appendChild($daterange);
									$roomType = $dom->createElement('RoomType');
									$roomType->setAttribute('id', $syncref);
									$roomType->setAttribute('closed', 'false');
									
									if ($syncrefRateId!=0) {
										$ratePlan = $dom->createElement('RatePlan');
										$ratePlan->setAttribute('id', $syncrefRateId);
										if ($mainProduct->_reservationPriceType==reservation_price::TABLE_TIME) {
											$with_taxes = (Configuration::get('MYOWNRES_PRICE_TAXEXCL')!=1);
											$product = new Product($id_product);
											if ($with_taxes) $tax_rate=$product->tax_rate;
											else $tax_rate=0;
											$price  = MyOwnReservationsUtils::getProductPriceTaxes($id_product, 0, false);
											$periods = $mainProduct->getTimeUnits($cookie, $obj, $id_product);
											$setprices = myOwnPricesSets::getPricesSetForProduct($id_product, 13, $tax_rate, $with_taxes);
											
											foreach ($setprices as $length => $setprice) {
												if (round($price) != round($setprice) ) {
													$rate = $dom->createElement('Rate');
													$rate->setAttribute('currency', myOwnUtils::getCurrencyIso());
													$rate->setAttribute('lengthOfStay', $length);
													$perDay = $dom->createElement('PerDay');
													$perDay->setAttribute('rate', number_format($setprice, 2, '.', ''));
													$rate->appendChild($perDay);
													$ratePlan->appendChild($rate);
												}
												$roomType->appendChild($ratePlan);
												$availRateUpdate->appendChild($roomType);
												$availRateUpdateRQ->appendChild($availRateUpdate);
											}
										} else {
											$rate = $dom->createElement('Rate');
											$rate->setAttribute('currency', myOwnUtils::getCurrencyIso());
											$rate->setAttribute('rateChangeIndicator', 'false');
											$perDay = $dom->createElement('PerDay');
											$price  = MyOwnReservationsUtils::getProductPrice($id_product, 0);
											$perDay->setAttribute('rate', number_format($price, 2, '.', ''));
											$rate->appendChild($perDay);
											$ratePlan->appendChild($rate);
											$roomType->appendChild($ratePlan);
											$availRateUpdate->appendChild($roomType);
											$availRateUpdateRQ->appendChild($availRateUpdate);
										}
									}
								}
								foreach ($days as $daykey => $dayvalue) {
									$availRateUpdate = $dom->createElement('AvailRateUpdate');
									$daterange = $dom->createElement('DateRange');
									$daterange->setAttribute('from', $daykey);
									$daterange->setAttribute('to', $daykey);
									$availRateUpdate->appendChild($daterange);
									$roomType = $dom->createElement('RoomType');
									$roomType->setAttribute('id', $syncref);
									$roomType->setAttribute('closed', 'false');
									$inventory = $dom->createElement('Inventory');
									$inventory->setAttribute('totalInventoryAvailable', $dayvalue);
									$roomType->appendChild($inventory);
									$availRateUpdate->appendChild($roomType);
									$availRateUpdateRQ->appendChild($availRateUpdate);
									
								}
								$dom->appendChild($availRateUpdateRQ);
								//echo $dom->saveHTML();

								  
								$ch = curl_init();
								$url = 'https://services.expediapartnercentral.com/eqc/ar';
								curl_setopt( $ch, CURLOPT_URL, $url );
								curl_setopt( $ch, CURLOPT_POST, true );
								curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
								curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
								curl_setopt( $ch, CURLOPT_POSTFIELDS, $dom->saveHTML() );
								$result = curl_exec($ch);
								curl_close($ch);
								//echo $result;
								if (stripos($result, 'Success'))
									Configuration::updateValue('MYOWNRES_SYNC_T_'.$platform_key, array($id_product => date('Y-m-d H:i:s')));
								else
									PrestaShopLogger::addLog('laBulle myOwnReservations Sync : Cant get stream for product '.$id_product);

							}
						} else {
							try {
								if ($platform_key == sync_platforms::AIRBNB) {
									$ch = curl_init();
									curl_setopt($ch, CURLOPT_URL, $url.$syncref);
									curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
									curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
									$content = curl_exec($ch);
									curl_close($ch);
								} else 
									$content = file_get_contents($url.$syncref.($platform_key == 1 ? '.ics' : ''));
									
								$lines = explode("\n", $content);
								foreach ($lines as $line)
									if (trim($line)!='' && stripos($line, 'DTSTAMP')===false)
										$inline .= $line.';';
								$hash = strtolower(md5(strtolower($inline)));
								
								$resas = self::getFromICal($platform_key, $id_product, $inline);
								$existing_resas = self::getFutureReservationsForProductFromSource($obj, $id_product, $platform_key);
								//print_r($resas);
								foreach ($resas as $resa) {
									$issaved = self::isReservationByReference($resa->reference);
									echo '<br>+('.$resa->id_source.')'.$resa->reference.':'.$issaved;
									if (!$issaved)
										$resa->sqlInsert();
								}
								$todelete_resas=array();
								foreach ($existing_resas as $existing_resa)
									if (!array_key_exists($existing_resa->reference, $resas)) {
										$todelete_resas[] = $existing_resa;
										echo '<br>-('.$resa->id_source.')'.$resa->reference;
									}
								
								$last_hash = Configuration::get('MYOWNRES_SYNC_H_'.$platform_key, $id_product);
								if ($last_hash!=$hash) {
				
									Configuration::updateValue('MYOWNRES_SYNC_T_'.$platform_key, array($id_product => date('Y-m-d H:i:s')));
									Configuration::updateValue('MYOWNRES_SYNC_H_'.$platform_key, array($id_product => $hash));
								}
							}
							catch (Exception $e) {
							    PrestaShopLogger::addLog('laBulle myOwnReservations Sync : Cant get stream for product '.$id_product.' : '.$e->getMessage());
							}
						}
					}
				}
			}
	}
	
	public static function extractReservationItem($obj, $resa, $sel, $delete=false, $reScheduleStart='', $reScheduleStartTimeslot=0, $reScheduleEnd='', $reScheduleEndTimeslot=0) {
		$resa->_mainProduct = $obj->_products->getResProductFromProduct($resa->id_product);
		$resa = myOwnResas::populateReservation($resa);
		$oldResa = clone $resa;
		$addResa=null;
		
		$splited=false;
		$selected=false;
		for ($i=1; $i < $resa->quantity+1; $i++) {
			if (!$sel[$i]) $splited=true;
		}
		if (!count(array_filter($sel))) $splited=false;
		//PrestaShopLogger::addLog('extractReservationItem '.implode(',', $sel));
		//split cart and reservation
		if ($splited) {
			$totalQty=$resa->quantity;
			$resa->quantity=0;
			$oldStock = array();
			$newStock = array();
			$oldQtiyExt=array();
			$newQtiyExt=array();
			
			//get stock and extdetails
			$assignedStock = explode(";", $resa->stock);
			$qtiesExt = array();
			$resacart= myOwnCarts::getResCartFromResa($obj, $resa);
			foreach ($obj->_extensions->getExtByType(extension_type::PROD_QTY) as $ext) 
				if (in_array($ext->id, $resa->_mainProduct->ids_notifs) || $resa->_mainProduct->_qtyType==$ext->id)
					if (method_exists($ext, "getQties")) {
						$qties = $ext->getQties(-$resacart->sqlId);
						$j=1;
						foreach ($qties as $qtyKey => $qtyVal)
							for ($i = 1; $i < $qtyVal+1; $i++)
								$qtiesExt[$j++]=$qtyKey;//.' #'.$i;
					}	
			
			for ($i=1; $i < $totalQty+1; $i++) {
				if ($sel[$i]) {
					$resa->quantity++;
					$oldResa->quantity--;
					if (array_key_exists($i-1, $assignedStock))
						$newStock[] = $assignedStock[$i-1];
					if ($qtiesExt!=array()) {
						$newQtiyExt[] = $qtiesExt[$i];
					}
				} else {
					if (array_key_exists($i-1, $assignedStock))
						$oldStock[] = $assignedStock[$i-1];
					if ($qtiesExt!=array()) {
						$oldQtiyExt[] = $qtiesExt[$i];
					}
				}
				
			}
			
			//Saving extracted resa
			$resa->stock=implode(";", $newStock);
			if ($reScheduleEnd!='') $resa->endDate = $reScheduleEnd;
	 		if ($reScheduleEndTimeslot!=0) $resa->endTimeslot = $reScheduleEndTimeslot;
	 		if ($reScheduleStart!='') $resa->startDate = $reScheduleStart;
	 		if ($reScheduleStartTimeslot!=0) $resa->startTimeslot = $reScheduleStartTimeslot;
			//set sqlIsert from parent (cart)
			if (!$delete) { 
				$resa->id_cart = $resacart->id_cart;
				
				$addCart = $resa->sqlInsert(true);
				$addResa = $resa->sqlInsert();
				$resa->sqlId = $addResa;
			}
			//Saving old resa
			$oldResa->stock=implode(";", $oldStock);
			$oldResa->sqlUpdate();
			//saving old cart
			$resacart->quantity = $oldResa->quantity;
			$resacart->sqlUpdate();
			//update ext qty to remove item
			if ($delete) {
				$newQties = array();
				foreach($newQtiyExt as $newQty) {
					if (array_key_exists($newQty, $newQties)) $newQties[$newQty]++;
					else $newQties[$newQty] = 1;
				}
				if ($newQtyExt!=array())
					foreach ($obj->_extensions->getExtByType(extension_type::CARTUPDATE) as $ext) {
						if ($oldResa->_mainProduct->_qtyType==$ext->id)
							if (method_exists($ext, "execResaUpdate"))
								$ext->setQties($resacart->sqlId, $newQties);
					}
			}
			
			if ($qtiesExt!=array()) {
				foreach ($obj->_extensions->getExtByType(extension_type::PROD_QTY) as $ext) 
					if ($resa->_mainProduct->_qtyType==$ext->id)
						if (method_exists($ext, "updateQties")) {
							$ext->updateQties($resacart->sqlId, $addCart, $oldQtiyExt, $newQtiyExt, $delete);
						}
			}
			
			$resa = $oldResa;
			$oldResa = null;
		} elseif ($reScheduleEnd!='' || $reScheduleEndTimeslot!=0) {
	 		$resa->endDate = $reScheduleEnd;
	 		$resa->endTimeslot = $reScheduleEndTimeslot;
	 		$resa->sqlUpdate();
 		}
 		return $oldResa;
	}
	
	public function getResas($id_product, $id_product_attribute) {
		$resas = array();
		foreach($this->list AS $resa)
			if ($resa->id_product==$id_product && $resa->id_product_attribute == $id_product_attribute )
				$resas[] = $resa;

		return $resas;
	}
	
	public function getFamillyResas($mainProduct) {
		$resas = array();
		$products = $mainProduct->getProductsId();
		foreach($this->list AS $cart)
			if ( in_array($cart->id_product, $products) )
				$resas[] = $cart;

		return $resas;
	}
	
	public static function getProducts($id_order, $_products, $_pricerules, $_timeslots, $id_product=null, $id_product_attribute=null) {
		$resas = array();
		$sql = "SELECT * FROM " . _DB_PREFIX_ . "myownreservations_reservation WHERE id_order = ".$id_order;
		if ($id_product!=null) $sql .= " AND id_product = ".$id_product;
		if ($id_product_attribute!=null) $sql .= " AND id_product_attribute = ".$id_product_attribute;

		$resaSql = Db::getInstance()->ExecuteS($sql);
		foreach($resaSql AS $resa) {
			$tempResa = new myOwnReservation($resa['id_reservation'], $resa);
			$tempResa->_mainProduct = $_products->getResProductFromProduct($tempResa->id_product);
			
			$resas[] = $tempResa;
		}
		return $resas;
	}
	
	public static function populateReservation($tempReservation) {
	
		if ($tempReservation->id_order) {
			$order= new Order($tempReservation->id_order);
			$tempReservation->_order = $order;
			
			if (_PS_VERSION_ >= '1.5.0.0') $tempReservation->ref_order = $order->getUniqReference();
			
			$orderStatuss = Db::getInstance()->ExecuteS("SELECT * FROM  `" . _DB_PREFIX_ . "order_history` WHERE  `id_order` =".$tempReservation->id_order." ORDER BY date_add DESC LIMIT 1;");
			if (is_array($orderStatuss) && array_key_exists(0, $orderStatuss)) {
				$orderStatus = $orderStatuss[0];
				$tempReservation->_orderStatus = $orderStatus['id_order_state'];
			}
			
			$tempReservation->_customerId = $order->id_customer;
			$tempReservation->_customerAddressId = $order->id_address_delivery;
			$tempReservation->_carrier = $order->id_carrier;
			if ($order->module!='')	{
				$paymentModule=Module::getInstanceByName($order->module);
				if ($paymentModule!=null) $tempReservation->_payment = $paymentModule->id;
			}
			//customer
			$customers = Db::getInstance()->ExecuteS("SELECT * FROM `" . _DB_PREFIX_ . "customer` WHERE id_customer = " . intval($tempReservation->_customerId) . ";");
			if (array_key_exists(0, $customers)) {
				$customer = $customers[0];
				$tempReservation->_customerName = strtoupper($customer['lastname']).' '.$customer['firstname'];
				$tempReservation->_customerEmail = strtoupper($customer['email']);
			}
			
			//customer address
			$addresses = Db::getInstance()->ExecuteS("SELECT * FROM `" . _DB_PREFIX_ . "address` WHERE id_address = " . intval($tempReservation->_customerAddressId) . ";");
			if (array_key_exists(0, $addresses)) {
				$address = $addresses[0];
				$tempReservation->_customerPhone = $address['phone'];
				$tempReservation->_customerAddressStreet = $address['address1'].$address['address2'];
				$tempReservation->_customerAddressPostCode = $address['postcode'];
				$tempReservation->_customerAddressCity = $address['city'];
			}
		} else {
			$external_fields = explode(';', $tempReservation->external_fields);
			$tempReservation->_customerName = $external_fields[0];
			if (count($external_fields)>1) $tempReservation->_carrier = $external_fields[1];
			if (count($external_fields)>2) $tempReservation->_payment = $external_fields[2];
		}
		
		return $tempReservation;
	}
	
	public static function getReservation($id_resa) {
		$resas = array();
		$_timeslots = new myOwnTimeSlots();
		$sql = "SELECT * FROM " . _DB_PREFIX_ . "myownreservations_reservation WHERE id_reservation = ".$id_resa;

		$resaSql = Db::getInstance()->ExecuteS($sql);
		foreach($resaSql AS $resa) {
			$tempResa = new myOwnReservation($resa['id_reservation'], $resa);
			$tempResa = self::populateReservation($tempResa);
			return $tempResa;
		}
		return null;
	}
	
	public static function getReservations($id_order=null, $id_product=null, $id_product_attribute=null) {
		$resas = array();
		
		$id_shop = myOwnUtils::getShop();
		$_timeslots = new myOwnTimeSlots();
		if ($id_order!=null) $sql = "SELECT * FROM " . _DB_PREFIX_ . "myownreservations_reservation WHERE id_order = ".$id_order;
		else $sql = "SELECT * FROM " . _DB_PREFIX_ . "myownreservations_reservation WHERE id_reservation > 0";
		if ($id_product!=null) $sql .= " AND id_product = ".$id_product;
		if ($id_product_attribute!=null) $sql .= " AND id_product_attribute = ".$id_product_attribute;
		if (Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') && $id_shop>0) $sql .= " AND id_shop = ".$id_shop;
		$sql .= " ORDER BY start_date, start_time";

		$resaSql = Db::getInstance()->ExecuteS($sql);
		foreach($resaSql AS $resa) {
			$tempResa = new myOwnReservation($resa['id_reservation'], $resa);

			$resas[] = $tempResa;
		}
		return $resas;
	}
	
	public static function getReservationsForCustomer($obj, $id_customer=null, $myresafilter='') {
		$resas = array();
		$_timeslots = new myOwnTimeSlots();
// 		if ($id_order!=null)
		$sql = "SELECT * FROM " . _DB_PREFIX_ . "myownreservations_reservation LEFT JOIN " . _DB_PREFIX_ . "orders ON (" . _DB_PREFIX_ . "orders.id_order =  " . _DB_PREFIX_ . "myownreservations_reservation.id_order) WHERE " . _DB_PREFIX_ . "orders.id_customer = ".$id_customer;
		if ($myresafilter=='past')
			$sql .= " AND end_date < CURDATE()";
		if ($myresafilter=='curent')
			$sql .= " AND end_date < CURDATE() AND start_date > CURDATE()";
		if ($myresafilter=='incoming')
			$sql .= " AND start_date > CURDATE()";
		$sql .= " ORDER BY start_date DESC";
// 		else $sql = "SELECT * FROM " . _DB_PREFIX_ . "myownreservations_reservation WHERE id_reservation > 0";
// 		if ($id_product!=null) $sql .= " AND id_product = ".$id_product;
// 		if ($id_product_attribute!=null) $sql .= " AND id_product_attribute = ".$id_product_attribute;
// 		$sql .= " ORDER BY start_date, start_time";
// echo $sql;
		$resaSql = Db::getInstance()->ExecuteS($sql);
		foreach($resaSql AS $resa) {
			$tempResa = new myOwnReservation($resa['id_reservation'], $resa);
			$tempResa->_mainProduct = $obj->_products->getResProductFromProduct($tempResa->id_product);
			if ($tempResa->id_order>0) $tempResa = myOwnResas::populateReservation($tempResa);
			$resas[] = $tempResa;
		}
		return $resas;
	}
	
	public static function getFutureReservationsForProductFromSource($obj, $id_product, $id_source=0) {
		$resas = array();
		$sql = "SELECT * FROM " . _DB_PREFIX_ . "myownreservations_reservation WHERE id_product = ".(int)$id_product.($id_source ? " AND id_source = ".(int)$id_source : '')." AND start_date > CURDATE() ORDER BY start_date DESC";

		$resaSql = Db::getInstance()->ExecuteS($sql);
		foreach($resaSql AS $resa) {
			$tempResa = new myOwnReservation($resa['id_reservation'], $resa);
			/*$tempResa->_mainProduct = $obj->_products->getResProductFromProduct($tempResa->id_product);
			if ($tempResa->id_order>0) $tempResa = myOwnResas::populateReservation($tempResa);*/
			$resas[$tempResa->reference] = $tempResa;
		}
		return $resas;
	}
	
	public static function getFutureReservationsForProductInsteadSource($obj, $id_product, $id_source=0) {
		$resas = array();
		$sql = "SELECT * FROM " . _DB_PREFIX_ . "myownreservations_reservation WHERE id_product = ".(int)$id_product.($id_source ? " AND id_source != ".(int)$id_source : '')." AND start_date > CURDATE() ORDER BY start_date DESC";

		$resaSql = Db::getInstance()->ExecuteS($sql);
		foreach($resaSql AS $resa) {
			$tempResa = new myOwnReservation($resa['id_reservation'], $resa);
			/*$tempResa->_mainProduct = $obj->_products->getResProductFromProduct($tempResa->id_product);
			if ($tempResa->id_order>0) $tempResa = myOwnResas::populateReservation($tempResa);*/
			$resas[$tempResa->reference] = $tempResa;
		}
		return $resas;
	}
	
	public static function addFromCart($id_order, $cartProds, $id_shop=0) {
		$total = 0;
		if (Configuration::get('MYOWNRES_MIN_FOR_ADVANCE')) {
			foreach ($cartProds as $cartProd) 
				$total += $cartProd->getPriceTaxes(true);
		}
		foreach ($cartProds as $cartProd) {
			$newRes = new myOwnReservation();
			$newRes->id_order = $id_order;
			$newRes->id_shop = $id_shop;
			$newRes->id_cartproduct = $cartProd->sqlId;
			$newRes->id_product = $cartProd->id_product;
			$newRes->id_product_attribute = $cartProd->id_product_attribute;
			$newRes->id_customization = $cartProd->id_customization;
			$newRes->id_object = $cartProd->id_object;
			$newRes->quantity = $cartProd->quantity;
			/*$newRes->startDate = $cartProd->startDate;
			$newRes->startTimeslot = $cartProd->startTimeslot;
			$newRes->endDate = $cartProd->endDate;
			$newRes->endTimeslot = $cartProd->endTimeslot;*/
			$newRes->startTimeslot = $cartProd->startTimeslot;
			$newRes->endTimeslot = $cartProd->endTimeslot;
			$newRes->startDate = $cartProd->startDate;
	    	$newRes->endDate = $cartProd->endDate;
			if ($cartProd->_mainProduct!=null) {
	    		$newRes->startTime = $cartProd->_mainProduct->_timeslotsObj->list[$cartProd->startTimeslot]->startTime;
	    		$newRes->endTime = $cartProd->_mainProduct->_timeslotsObj->list[$cartProd->endTimeslot]->endTime;
	    		$advance = ($cartProd->_mainProduct->_advanceRate/100);
           		if (Configuration::get('PS_SHOP_ENABLE') && Configuration::get('MYOWNRES_NO_LOC_ADVANCE') && in_array(Tools::getRemoteAddr(), explode(',', Configuration::get('PS_MAINTENANCE_IP')))
           			|| (Configuration::get('MYOWNRES_MIN_FOR_ADVANCE') && $total<Configuration::get('MYOWNRES_MIN_FOR_ADVANCE')) ) //PS_SHOP_ENABLE
		   			$advance = 1;
	    		$newRes->advance = $cartProd->price*$advance+$cartProd->_mainProduct->_advanceAmount;
    		}
			$newRes->price = $cartProd->price;
			$newRes->tax_rate = $cartProd->tax_rate;
			$newRes->discounts = $cartProd->discounts;
			$newRes->validated = 0;
			$newRes->sqlInsert();
		}
	}
	
	public static function deleteReservation($id) {
		$query  = 'DELETE FROM '. _DB_PREFIX_ .'myownreservations_reservation WHERE id_reservation = '.$id.';';
		
		return Db::getInstance()->Execute($query);
	}
		
	public static function sortForGlouton($a,$b) {
		  if ($a->_end == $b->_end) {
		  		//de la plus longue a la plus petite
				if (($a->_end-$a->getStartDateTime()) == ($b->_end-$b->getStartDateTime())) {
				    return 0;
				}
				return ($a->_end-$a->getStartDateTime()) > ($b->_end-$b->getStartDateTime()) ? -1 : 1;
	      }
	      //de celle qui fini le plus tard a celle qui fini le plus tot
	      return ($a->_end > $b->_end) ? -1 : 1;
	}
	
	public static function getFlatResaLinesCount($cart, $availabilities = null, $mainProduct, $id_product, $id_product_attribute, $startDate, $endDate, $id_timeslot_start, $id_timeslot_end, $id_object=0, $serial="") {
		
		$res= self::getFlatResaLines($cart, $availabilities, $mainProduct, $id_product, $id_product_attribute, $startDate, $endDate, $id_timeslot_start, $id_timeslot_end, $id_object, $serial);
		if ($res<0) return $res;
		else return count($res);
	}
	
	public static function getReservationsOnPeriod($obj, $mainProduct, $id_product, $id_product_attribute, $startDate, $endDate) {
		global $cookie;
		$id_timeslot_end = $id_timeslot_start = 0;
		$id_shop = myOwnUtils::getShop();
		$startDay = date('Y-m-d',$startDate);
		$endDay = date('Y-m-d',$endDate);
		$timeslots = $mainProduct->_timeslotsObj->list;
		$resaObjects=array();

		//checking delay between resas
		$desiredShift = $mainProduct->_reservationBetweenShift;
		$mainProduct->getShiftTimeSlot($endDate, $id_timeslot_end, $desiredShift, false, null, $endDate, $id_timeslot_end, $id_product, $id_product_attribute);
		$endDay = date('Y-m-d',$endDate);
		$desiredShift = $mainProduct->_reservationBetweenShift;
		$mainProduct->getRevShiftTimeSlot($startDate, $id_timeslot_start, $desiredShift);
		$startDay = date('Y-m-d',$startDate);
		$isPack=false;
		$isPacked=false;
		if (Configuration::get('PS_PACK_STOCK_TYPE') == 1) {
			$isPack=Pack::isPack($id_product);
			$isPacked=Pack::isPacked($id_product);
		}
		$pack=array();
		$packsQty=array();
		$packs=array();
		$query  = "
		SELECT *
		FROM  `". _DB_PREFIX_ ."myownreservations_reservation`";
		//getting list if mutuel or if id_product=0 on home page..
		if ($mainProduct->productType==product_type::MUTUEL || $id_product==0 || $mainProduct->productType==product_type::RESOURCE || $mainProduct->productType==product_type::PLACE) {
			$productsIds=$mainProduct->getProductsId();
			if (count(array_filter($productsIds))) {
				$productsIdList = implode(',',array_filter($productsIds));
				$query .= "
			WHERE id_product in (".$productsIdList.") ";
			} else $query .= "
			WHERE id_product = 0 ";
		} else if ($isPack || $isPacked) {
			if ($isPack) {
				$pack[] = $id_product;
				$packitems = Pack::getItems($id_product, $cookie->id_lang);
				foreach ($packitems as $packitem) {
					$pack[] = $packitem->id;
					$packsQty[$packitem->id.'-'.$packitem->id_pack_product_attribute] = $packitem->pack_quantity;
					if (Pack::isPacked($packitem->id)) {
						$packstmp = Pack::getPacksTable($packitem->id, $cookie->id_lang, false);
						foreach ($packstmp as $packelem)
							$packs[] = $packelem['id_product'];
					}
				}
				
			}
			if ($isPacked) {
				$packs[] = $id_product;
				$packstmp = Pack::getPacksTable($id_product, $cookie->id_lang, false);
				foreach ($packstmp as $packelem)
					$packs[] = $packelem['id_product'];
	
			}
			$temparr = array_merge($pack, $packs);
			$query .= "
				WHERE id_product in (".implode(',', $temparr).") ";
		} else if ($mainProduct->productType!=product_type::PLACE) {
			$query  .= "
			WHERE id_product=".$id_product;
			if (!$mainProduct->_optionIgnoreAttrStock && $id_product!=0) $query .= "
			AND id_product_attribute = ".$id_product_attribute;
		} else
			$query  .= "
			WHERE id_product>0";
		$query  .= "
		AND start_date <= '".$endDay."'
		AND end_date >= '".$startDay."'
		AND validated = 1";
		if ($id_shop>0) $query .= " 
		AND (id_shop = ".$id_shop." OR id_shop = 0)";
		if (Tools::getValue('place')>0) $query .= " 
		AND (id_object = ".Tools::getValue('place')." OR id_object = 0)";

		$resas = Db::getInstance()->ExecuteS($query);

		foreach ($resas as $resa) {
			$resaObject = new myOwnReservation($resa['id_reservation'], $resa);
			$resaObject->sqlId = $resa['id_reservation'];
			$packQty=1;
			if (in_array($resa['id_product'], $packs)) {
				$packitems = Pack::getItems($resa['id_product'], $cookie->id_lang);
				foreach ($packitems as $packitem) if ($id_product==$packitem->id) $packQty=$packitem->pack_quantity;
			}
			if (in_array($resa['id_product'], $pack) && array_key_exists($resa['id_product'].'-'.$resa['id_product_attribute'], $packsQty)) {
				$packQty = 1 / ($packsQty[$resa['id_product'].'-'.$resa['id_product_attribute']]);
			}
			$resaObject->quantity = $resa['quantity']*$packQty;
			$resaObject->stock = $resa['stock'];
			if ($mainProduct->productType==product_type::PLACE)
				$resaObject->_mainProduct = $obj->_products->getResProductFromProduct($resa['id_product']);
			else $resaObject->_mainProduct = $mainProduct;
			if ( array_key_exists($resa['start_timeslot'], $timeslots) && array_key_exists($resa['end_timeslot'], $timeslots) ) {
				$desiredShift = $mainProduct->_reservationBetweenShift;
				if ($desiredShift>0) {
					$startDate = strtotime($resa['start_date']);
					$id_timeslot_start = $resa['start_timeslot'];
					$mainProduct->getRevShiftTimeSlot($startDate, $id_timeslot_start, $desiredShift);
					$resaObject->_start = $resaObject->getStartDateTime(date('Y-m-d',$startDate), $id_timeslot_start);
				} else 
					$resaObject->_start = $resaObject->getStartDateTime();
				$resaObject->_end = $resaObject->getEndDateTime();
			} else {
				$resaObject->_start = strtotime($resa['start_date'].' '.$resa['start_time']);
				$resaObject->_end = strtotime($resa['end_date'].' '.$resa['end_time']);
			}
			if ($mainProduct->sqlId != $resaObject->_mainProduct->sqlId && MYOWNRES_WEEK_TS && $resaObject->_mainProduct->_reservationUnit==reservation_unit::WEEK) {
				if ($resaObject->_mainProduct->_reservationSelWeekStart < $resaObject->_mainProduct->_reservationSelWeekEnd) {
					//echo '<br>@'.$resa->sqlId.'='.$resa->startDate.'_'.$resa->endDate;
					for($j = $resaObject->_mainProduct->_reservationSelWeekStart; $j <= $resaObject->_mainProduct->_reservationSelWeekEnd; $j++) {
						 
						 $ddate = date('Y-m-d', strtotime('+'.($j-1).' day', strtotime( date('o',strtotime($resa['start_date'])).'W'.date('W',strtotime($resa['start_date'])) ) ) );

						 $resaObject_copy = clone $resaObject;
						 $resaObject_copy->_start = strtotime($ddate.' '.$resa['start_time']);
						 $resaObject_copy->_end = strtotime($ddate.' '.$resa['end_time']);
						 $resaObjects[] = $resaObject_copy;
					}
				}
			} else {
				$resaObject->_mainProduct = null;
				$resaObjects[] = $resaObject;
			}
		}
		
		return $resaObjects;
	}
	
	public function getResaLines($cart, $availabilities = null, $mainProduct, $id_product, $id_product_attribute, $startDate, $endDate, $id_timeslot_start, $id_timeslot_end) {
		global $cookie;
		$debug=false;
		$timeslots = $mainProduct->_timeslotsObj->list;
		
		//$product->pack_stock_type==3
		$out="";
		$startDay = date('Y-m-d',$startDate);
		$endDay = date('Y-m-d',$endDate);

		//checking delay between resas
		$desiredShift = $mainProduct->_reservationBetweenShift;
		if ($desiredShift>0) {
			$mainProduct->getShiftTimeSlot($endDate, $id_timeslot_end, $desiredShift, false, null, $endDate, $id_timeslot_end, $id_product, $id_product_attribute);
			$endDay = date('Y-m-d',$endDate);
			//removed because reservations are enlarged before start date so we need to get reservations after view
			//$desiredShift = $mainProduct->_reservationBetweenShift;
			//$mainProduct->getRevShiftTimeSlot($startDate, $id_timeslot_start, $desiredShift);
			$startDay = date('Y-m-d',$startDate);
		}
		
		//sql req creation
		$notAfterTsReq="";$notBeforeTsReq="";
		$notAfterTsTest = false;
		$notBeforeTsTest = true;

		//if ($availabilities==null) $availabilities = new myOwnAvailabilities($startDate, $endDate, $mainProduct, $id_product, $id_product_attribute);
		//future evolution maybe we have to check only the start with _allowResOnHolidays
		if ($availabilities!=null && !$mainProduct->_allowResOnHolidays && !$availabilities->isPeriodTsAvailable($startDate, $endDate, $id_timeslot_start, $id_timeslot_end, $id_product, $id_product_attribute)) return reservation_error::PERIOD_IN_HOLIDAYS;

		if ($availabilities!=null && $mainProduct->_allowResOnHolidays) {
			if (!$availabilities->isAvailable($startDate, $timeslots[$id_timeslot_start], $id_product, $id_product_attribute)) return reservation_error::PERIOD_IN_HOLIDAYS;
			if (!$availabilities->isAvailable($endDate, $timeslots[$id_timeslot_end], $id_product, $id_product_attribute)) return reservation_error::PERIOD_IN_HOLIDAYS;
		}

		//calculate period to check
		$strTime="00:00:00";
		if ($mainProduct!=null && array_key_exists($id_timeslot_start, $timeslots)) //removed && $id_timeslot_end>0 due to nightsel that got a 00:00
			$strTime = $timeslots[$id_timeslot_start]->startTime;
		$startDateTime = strtotime($startDay.' '.$strTime);

		$strTime="00:00:00";
		if ($mainProduct!=null && array_key_exists($id_timeslot_end, $timeslots))  //removed && $id_timeslot_end>0 due to nightsel that got a 00:00
			$strTime = $timeslots[$id_timeslot_end]->endTime;

		if (($mainProduct!=null && $strTime=="00:00:00" && $mainProduct->reservationSelType!=reservation_interval::WEEK) 
			or ($mainProduct==null && $endDay==$startDay) )
			$endDateTime = strtotime("+1 day",strtotime($endDay.' '.$strTime));
		else $endDateTime = strtotime($endDay.' '.$strTime);
		
		//filter own cart resas to keep ones in the window
		$resaObjects = array();
		//echo count($this->list );

		foreach ($this->list as $resa) {
			//echo date('Y-m-d H:i',$resa->_start).'<'.date('Y-m-d H:i',$endDateTime).'<br>';
			//echo date('Y-m-d H:i',$resa->_end).'>'.date('Y-m-d H:i',$startDateTime).'<br>'.$resa->sqlId;
			if ( $resa->_start < $endDateTime && $resa->_end > $startDateTime) {
				$resa->_mainProduct = $mainProduct;

				if ($mainProduct->_reservationBetweenShift>0) {

						$startDate = strtotime($resa->startDate);
						$endDate = strtotime($resa->endDate);
						$id_timeslot_start = $resa->startTimeslot;
						$id_timeslot_end = $resa->endTimeslot;

						$mainProduct->getRevShiftTimeSlot($startDate, $id_timeslot_start, $mainProduct->_reservationBetweenShift);

						$resa->_start = $resa->getStartDateTime(date('Y-m-d',$startDate), $id_timeslot_start);
						
						$desiredShift=$mainProduct->_reservationBetweenShift+1;
						
						$mainProduct->getShiftTimeSlot(strtotime($resa->endDate), $resa->endTimeslot, $desiredShift, false, null, $endDate, $id_timeslot_end, $id_product, $id_product_attribute);
						//echo '<br>A '.$resa->endDate.'<br>';
						//echo '<br>B '.date('Y-m-d',$endDate).'<br>';
						$resa->_end = $resa->getEndDateTime(false, date('Y-m-d', $endDate), $id_timeslot_end);
						
					}/* else commented it already have been done efore
						$resa->_start = $resa->getStartDateTime();*/
				//echo '#'.$resa->_start;
				$resaObjects[] = $resa;
			}
		}
		
		//echo count($resaObjects);
		if ($cart != null && intval($cart->id)>0) {
			$resaCarts = $cart->getMyOwnReservationCart();
			//taking all products famillies resas on mutual
			if ($mainProduct->productType==product_type::MUTUEL || $mainProduct->productType==product_type::PLACE || $mainProduct->productType==product_type::RESOURCE) $resas = $resaCarts->getFamillyResas($mainProduct);
			//taking all combinations resas when ignoring combination
			elseif ($mainProduct->_optionIgnoreAttrStock) $resas = $resaCarts->getResas($id_product);
			//taking exact product instead
			else $resas = $resaCarts->getResas($id_product, $id_product_attribute);
			
			foreach ($resas as $resa) {
				if ( array_key_exists($resa->startTimeslot, $timeslots) && array_key_exists($resa->endTimeslot, $timeslots) ) {
					$resaObject = new myOwnReservation();
					$resaObject->sqlId = $cart->id;
					$resaObject->quantity = $resa->quantity;
					$resaObject->id_product = $id_product;
					$resaObject->id_product_attribute = $id_product_attribute;
					$resaObject->startTimeslot = $resa->startTimeslot;//needed for WEEK_TS because checking TS in this case
					$resaObject->_isCart=true;
					$resaObject->_mainProduct = $mainProduct;
					$desiredShift = $mainProduct->_reservationBetweenShift;
					if ($desiredShift>0) {
						$startDate = strtotime($resa->startDate);
						$endDate = strtotime($resa->endDate);
						$id_timeslot_start = $resa->startTimeslot;
						$id_timeslot_end = $resa->endTimeslot;
						$mainProduct->getRevShiftTimeSlot($startDate, $id_timeslot_start, $desiredShift);
						$resaObject->_start = $resa->getStartDateTime(date('Y-m-d',$startDate), $id_timeslot_start);
						$desiredShift++;
						
						$mainProduct->getShiftTimeSlot(strtotime($resa->endDate), $resa->endTimeslot, $desiredShift, false, null, $endDate, $id_timeslot_end, $id_product, $id_product_attribute);
						
						$resaObject->_end = $resa->getEndDateTime(false, date('Y-m-d', $endDate), $id_timeslot_end);

					} else {
						$resaObject->_start = $resa->getStartDateTime();
						$resaObject->_end = $resa->getEndDateTime(false);
					}
						
					if ( $resaObject->_start < $endDateTime && $resaObject->_end > $startDateTime)
						$resaObjects[] = $resaObject;
				}
			}
		}

		//creating lines for reservation with serial
		$flatResasLinesSerial = array();
		$flatResaObjects = array();
		foreach ($resaObjects as $resaObject) {
			$serials = explode(";",$resaObject->stock);
			//calculate product occupation
			$resaQty = $resaObject->getOccupationQty();
			//if ($resaObject->_mainProduct->_qtyType == reservation_qty::IGNORE) $resaQty=1;
			//else $resaQty=ceil(($resaObject->quantity-$resaObject->quantity_refunded)/$mainProduct->_qtyCapacity);
			
			for ($i = 0; $i < $resaQty; $i++) {
				if (count($serials)>$i && trim($serials[$i])!="")
					$flatResasLinesSerial[trim($serials[$i])][] = $resaObject;
				else $flatResaObjects[] = $resaObject;
			}
		}
		$resaObjects = $flatResaObjects;

		uasort($resaObjects, array('myOwnResas', 'sortForGlouton') );
		
		//creating lines for reservation without serial
		$flatResasLines = $flatResasLinesSerial;
		//$flatResasLinesCount=0;
		foreach ($resaObjects as $key => $resaObject) {
			//for ($i = 0; $i < $resaObject->quantity; $i++) {
				$flatResasLinesCount=0;
				$canbeplaced=false;
				//echo "resaObject:".$key."(".date('Y-m-d H:i:s',$resaObject->_end)." ".date('Y-m-d H:i:s',$resaObject->_start).")--------<br>";
				foreach($flatResasLines as $key2 => $flatResasLine) {
					$canbeplaced=true;
					$flatResasLinesCount++;
					//echo "flatResasLine:".$key2."--------<br>";
					foreach($flatResasLine as $key3 => $flatResa) {
						
						$canbeplaced = $canbeplaced && ($flatResa->_start >= $resaObject->_end or $flatResa->_end <= $resaObject->_start);
						//echo "flatResa:".$key3."(".date('Y-m-d H:i:s',$flatResa->_start).">=".date('Y-m-d H:i:s',$resaObject->_end)." or ".date('Y-m-d H:i:s',$flatResa->_end)." <= ".date('Y-m-d H:i:s',$resaObject->_start).")<br>";
					}
					//echo "key:".$key."=".intval($canbeplaced)."<br>";
					if ($canbeplaced) break;
				}
				if (!$canbeplaced) $flatResasLinesCount++;
				$flatResasLines[$flatResasLinesCount][] = $resaObject;
			//}
		}
		/*		//debug section-----------------------
		$flatResasLinesCount=0;
		foreach($flatResasLinesSerial as $key => $flatResasLine) {
			$out .= 'line'.$key.'<br>';
			foreach($flatResasLine as $flatResa) {
				$out .= $flatResa->sqlId.'='.date('Y-m-d H:i:s',$flatResa->_start).'-'.date('Y-m-d H:i:s',$flatResa->_end).'<br>';
			}
			$flatResasLinesCount++;
		}
		
		$out .= "-------------------------------<br>";
		foreach($flatResasLines as $flatResasLine) {
			$out .= 'line'.$flatResasLinesCount.'<br>';
			foreach($flatResasLine as $flatResa) {
				$out .= $flatResa->sqlId.'='.date('Y-m-d H:i:s',$flatResa->_start).'-'.date('Y-m-d H:i:s',$flatResa->_end).'<br>';
			}
			$flatResasLinesCount++;
		}
		$out .= "<br>C=".count($flatResasLines);*/
		//debug section-----------------------
		//echo $out;
		if ($debug) return $out;
		else return $flatResasLines;
	}
		  
	public static function getFlatResaLines($cart, $availabilities=null, $mainProduct, $id_product, $id_product_attribute, $startDate, $endDate, $id_timeslot_start, $id_timeslot_end, $id_object=0, $serial="", $alsoUnvalidated=false) {
		global $cookie;
		$debug=false;

		$timeslots = $mainProduct->_timeslotsObj->list;
		$id_shop = myOwnUtils::getShop();
		$out="";
		$startDay = date('Y-m-d',$startDate);
		$endDay = date('Y-m-d',$endDate);
		
		//checking delay between resas
		$desiredShift = $mainProduct->_reservationBetweenShift;
		$mainProduct->getShiftTimeSlot($endDate, $id_timeslot_end, $desiredShift, false, null, $endDate, $id_timeslot_end, $id_product, $id_product_attribute);
		$endDay = date('Y-m-d',$endDate);
		$desiredShift = $mainProduct->_reservationBetweenShift;
		$mainProduct->getRevShiftTimeSlot($startDate, $id_timeslot_start, $desiredShift);
		$startDay = date('Y-m-d',$startDate);
		
		//calculate period to check
		$strTime="00:00:00";
		if ($mainProduct!=null && $id_timeslot_start>0 && array_key_exists($id_timeslot_start, $timeslots)) 
			$strTime = $timeslots[$id_timeslot_start]->startTime;
		$startDateTime = strtotime($startDay.' '.$strTime);

		$strTime="00:00:00";
		if ($mainProduct!=null && $id_timeslot_end>0 && array_key_exists($id_timeslot_end, $timeslots)) 
			$strTime = $timeslots[$id_timeslot_end]->endTime;

		if (($mainProduct!=null && $strTime=="00:00:00" && $mainProduct->reservationSelType!=reservation_interval::WEEK) 
			or ($mainProduct==null && $endDay==$startDay) )
			$endDateTime = strtotime("+1 day",strtotime($endDay.' '.$strTime));
		else $endDateTime = strtotime($endDay.' '.$strTime);
		
		if ($mainProduct!=null && !$id_timeslot_end && array_key_exists($id_timeslot_start, $timeslots) && $timeslots[$id_timeslot_end]->endTime < $timeslots[$id_timeslot_start]->startTime) {
			$startDateTime = strtotime($startDay.' '.$timeslots[$id_timeslot_end]->startTime);
			$endDateTime = strtotime($endDay.' '.$timeslots[$id_timeslot_end]->endTime);
		}
		
		//sql req creation
		$notAfterTsReq="";$notBeforeTsReq="";
		$notAfterTsTest = false;
		$notBeforeTsTest = true;

		if ($availabilities==null) $availabilities = new myOwnAvailabilities($startDate, $endDate, $mainProduct, $id_product, $id_product_attribute);
		if (!$mainProduct->_allowResOnHolidays && !$availabilities->isPeriodTsAvailable($startDate, $endDate, $id_timeslot_start, $id_timeslot_end, $id_product, $id_product_attribute, $id_object)) return reservation_error::PERIOD_IN_HOLIDAYS;
		if ($mainProduct->_allowResOnHolidays) {
			if (!$availabilities->isAvailable($startDate, (array_key_exists($id_timeslot_start, $timeslots) ? $timeslots[$id_timeslot_start] : null), $id_product, $id_product_attribute, $id_object)) return reservation_error::PERIOD_IN_HOLIDAYS;
			//if (!$availabilities->isAvailable($endDateTime, (array_key_exists($id_timeslot_end, $timeslots) ? $timeslots[$id_timeslot_end] : null), $id_product, $id_product_attribute)) return -1;
			//we check last day but has next bound can +1 or not if on week, we ask with end -1 for start bound
			if ($startDate != $endDate && !$availabilities->isPeriodAvailable(strtotime("-1 day",$endDateTime), $endDateTime, $id_product, $id_product_attribute)) return reservation_error::PERIOD_IN_HOLIDAYS;
		}

		if (is_array($timeslots))
		foreach ($timeslots as $timeslot) {
			if ($notAfterTsTest) $notAfterTsReq .= "AND start_timeslot != ".$timeslot->sqlId." ";
			if ($timeslot->sqlId == $id_timeslot_end) $notAfterTsTest = true;
			if ($timeslot->sqlId == $id_timeslot_start) $notBeforeTsTest = false;
			if ($notBeforeTsTest) $notBeforeTsReq .= "AND end_timeslot != ".$timeslot->sqlId." ";
		}
		//patch for week same start and end day
		if ($mainProduct->_reservationUnit==reservation_unit::WEEK && $mainProduct->_reservationSelWeekStart == $mainProduct->_reservationSelWeekEnd) {
			$notBeforeTsReq = "AND end_timeslot != 0 ";
			$notAfterTsReq .= "AND start_timeslot != 0 ";
		}
		if ($mainProduct->_reservationUnit==reservation_unit::DAY && $mainProduct->_reservationStartTime > $mainProduct->_reservationEndTime) {
			$notBeforeTsReq = "AND end_timeslot != 0 ";
		}
		
		$isPack=false;
		$isPacked=false;
		if (Configuration::get('PS_PACK_STOCK_TYPE') == 1) {
			$isPack=Pack::isPack($id_product);
			$isPacked=Pack::isPacked($id_product);
		}
		$pack=array();
		$packsQty=array();
		$packs=array();
		
		$query  = "
			SELECT *
			FROM  `". _DB_PREFIX_ ."myownreservations_reservation` ";
			if ($mainProduct->productType==product_type::MUTUEL || $mainProduct->productType==product_type::PLACE || $mainProduct->productType==product_type::RESOURCE) {
				$productsIds=$mainProduct->getProductsId();
				$productsIdList = implode(',',array_filter($productsIds));
				$query .= "
			WHERE id_product in (".$productsIdList.") ";
			} else if ($isPack || $isPacked) {
			if ($isPack) {
				$pack[] = $id_product;
				$packitems = Pack::getItems($id_product, $cookie->id_lang);
				foreach ($packitems as $packitem) {
					$pack[] = $packitem->id;
					$packsQty[$packitem->id.'-'.$packitem->id_pack_product_attribute] = $packitem->pack_quantity;
					if (Pack::isPacked($packitem->id)) {
						$packstmp = Pack::getPacksTable($packitem->id, $cookie->id_lang, false);
						foreach ($packstmp as $packelem)
							$packs[] = $packelem['id_product'];
					}
				}
				
			}
			if ($isPacked) {
				$packs[] = $id_product;
				$packstmp = Pack::getPacksTable($id_product, $cookie->id_lang, false);
				foreach ($packstmp as $packelem)
					$packs[] = $packelem['id_product'];
	
			}
			$temparr = array_merge($pack, $packs);
			$query .= "
				WHERE id_product in (".implode(',', $temparr).") ";
		} else $query .= "
			WHERE id_product=".$id_product." ";
			if (!$mainProduct->_optionIgnoreAttrStock) $query .= "
			AND id_product_attribute = ".intval($id_product_attribute);
			$query  .= "
			AND (start_date < '".$endDay."' OR (start_date = '".$endDay."' ".$notAfterTsReq."))
			AND (end_date > '".$startDay."' OR (end_date = '".$startDay."' ".$notBeforeTsReq."))";
		if ($serial=="" && !$alsoUnvalidated) $query  .= " AND validated = 1";
		if ($serial!="") $query  .= " AND stock LIKE '%".trim($serial)."%'";
		if ($id_shop>0) $query .= " AND (id_shop = ".$id_shop." OR id_shop = 0)";
		if ( MYOWNRES_WEEK_TS && $mainProduct->_reservationUnit==reservation_unit::WEEK) $query .= " AND start_timeslot = ".(int)$id_timeslot_start;
		//echo $query;
		$resas = Db::getInstance()->ExecuteS($query);

		$resaObjects = array();
		if (is_array($resas) && count($resas)>0)
		foreach ($resas as $resa) {
			if ( array_key_exists($resa['start_timeslot'], $timeslots) && array_key_exists($resa['end_timeslot'], $timeslots) ) {
				$resaObject = new myOwnReservation($resa['id_reservation'], $resa);
				$resaObject->sqlId = $resa['id_reservation'];
				$packQty=1;
				if (in_array($resa['id_product'], $packs)) {
					$packitems = Pack::getItems($resa['id_product'], $cookie->id_lang);
					foreach ($packitems as $packitem) if ($id_product==$packitem->id) $packQty=$packitem->pack_quantity;
				}
				if (in_array($resa['id_product'], $pack) && array_key_exists($resa['id_product'].'-'.$resa['id_product_attribute'], $packsQty)) {
					$packQty = 1 / ($packsQty[$resa['id_product'].'-'.$resa['id_product_attribute']]);
				}
				$resaObject->quantity = $resa['quantity']*$packQty;
				$resaObject->stock = $resa['stock'];
				$resaObject->_mainProduct = $mainProduct;
				$desiredShift = $mainProduct->_reservationBetweenShift;
				if ($desiredShift>0) {
					$startDate = strtotime($resa['start_date']);
					$id_timeslot_start = $resa['start_timeslot'];
					$mainProduct->getRevShiftTimeSlot($startDate, $id_timeslot_start, $desiredShift);
					$resaObject->_start = $resaObject->getStartDateTime($startDate, $id_timeslot_start);
				} else 
					$resaObject->_start = $resaObject->getStartDateTime();
				$resaObject->_end = $resaObject->getEndDateTime();
				//echo '<br>S '.date('Y-m-d H:i:s', $startDateTime).' '.date('Y-m-d H:i:s', $resaObject->_start).'<br>';
				//echo '<br>E '.date('Y-m-d H:i:s', $endDateTime).' '.date('Y-m-d H:i:s', $resaObject->_end).'<br>';
				//do that because time slots on night sel are 0, and it's geting = start or = end
				if ( $resaObject->_start < $endDateTime && $resaObject->_end > $startDateTime)
					$resaObjects[] = $resaObject;
			}
		}

		if ($cart != null && intval($cart->id)>0) {
			$resaCarts = $cart->getMyOwnReservationCart();

			//taking all products famillies resas on mutual
			if ($mainProduct->productType==product_type::MUTUEL || $mainProduct->productType==product_type::PLACE || $mainProduct->productType==product_type::RESOURCE) $resas = $resaCarts->getFamillyResas($mainProduct);
			//taking all combinations resas when ignoring combination
			elseif ($mainProduct->_optionIgnoreAttrStock) $resas = $resaCarts->getResas($id_product);
			//taking exact product instead
			else $resas = $resaCarts->getResas($id_product, $id_product_attribute);

			foreach ($resas as $resa) {
				
				if ($resa->sqlId > 0) //only if resa saved because if not that means resa is during save (we're checking before saving resa)
				if ( array_key_exists($resa->startTimeslot, $timeslots) && array_key_exists($resa->endTimeslot, $timeslots) ) {
					$resaObject = new myOwnReservation();
					$resaObject->sqlId = $cart->id;
					$resaObject->quantity = $resa->quantity;
					$resaObject->_isCart=true;
					$resaObject->_mainProduct = $mainProduct;
					$desiredShift = $mainProduct->_reservationBetweenShift;

					if ($desiredShift>0) {
						$startDate = strtotime($resa->startDate);
						$id_timeslot_start = $resa->startTimeslot;
						$mainProduct->getRevShiftTimeSlot($startDate, $id_timeslot_start, $desiredShift);
						$resaObject->_start = $resaObject->getStartDateTime($startDate, $id_timeslot_start);
					} else 
						$resaObject->_start = $resa->getStartDateTime();
					$resaObject->_end = $resa->getEndDateTime();

					if ( $resaObject->_start < $endDateTime && $resaObject->_end > $startDateTime)
						$resaObjects[] = $resaObject;
				}
			}

			/*$query  = "
				SELECT *
				FROM  `". _DB_PREFIX_ ."myownreservations_cartproducts`
				WHERE id_product=".$id_product;
				if (!$mainProduct->_optionIgnoreAttrStock) $query .= "
				AND id_product_attribute = ".$id_product_attribute;
				$query  .= "
				AND (start_date < '".$endDay."' OR (start_date = '".$endDay."' ".$notAfterTsReq."))
				AND (end_date > '".$startDay."' OR (end_date = '".$startDay."' ".$notBeforeTsReq."))
				AND id_cart = ".$cart->id;
	
			$resas = Db::getInstance()->ExecuteS($query);
	
			if (is_array($resas) && count($resas)>0)
			foreach ($resas as $resa) {
				if ( array_key_exists($resa['start_timeslot'], $timeslots) && array_key_exists($resa['end_timeslot'], $timeslots) ) {
					$resaObject = new myOwnReservation();
					$resaObject->sqlId = $resa['id_cart'];
					$resaObject->quantity = $resa['quantity'];
					$resaObject->_mainProduct = $mainProduct;
					$desiredShift = $mainProduct->_reservationBetweenShift;
					if ($desiredShift>0) {
						$startDate = strtotime($resa['start_date']);
						$id_timeslot_start = $resa['start_timeslot'];
						$mainProduct->getRevShiftTimeSlot($startDate, $id_timeslot_start, $desiredShift);
						$resaObject->_start = $resaObject->getStartDateTime($startDate, $id_timeslot_start);
					} else 
						$resaObject->_start = $resaObject->getStartDateTime();
					$resaObject->_end = $resaObject->getEndDateTime();

					$resaObjects[] = $resaObject;
				}
			}*/
		}
		
		//creating lines for reservation with serial
		$flatResasLinesSerial = array();
		$flatResaObjects = array();
		foreach ($resaObjects as $resaObject) {
			$serials = explode(";",$resaObject->stock);
			//calculate product occupation
			$resaQty = $resaObject->getOccupationQty();
			//if ($resaObject->_mainProduct->_qtyType == reservation_qty::IGNORE) $resaQty=1;
			//else $resaQty=ceil(($resaObject->quantity-$resaObject->quantity_refunded)/$mainProduct->_qtyCapacity);
			
			for ($i = 0; $i < $resaQty; $i++) {
				if (count($serials)>$i && trim($serials[$i])!="")
					$flatResasLinesSerial[trim($serials[$i])][] = $resaObject;
				else $flatResaObjects[] = $resaObject;
			}
		}
		$resaObjects = $flatResaObjects;

		uasort($resaObjects, array('myOwnResas', 'sortForGlouton') );
		
		//creating lines for reservation without serial
		$flatResasLines = $flatResasLinesSerial;
		//$flatResasLinesCount=0;
		foreach ($resaObjects as $key => $resaObject) {
			//for ($i = 0; $i < $resaObject->quantity; $i++) {
				$flatResasLinesCount=0;
				$canbeplaced=false;
				//echo "resaObject:".$key."(".date('Y-m-d H:i:s',$resaObject->_end)." ".date('Y-m-d H:i:s',$resaObject->_start).")--------<br>";
				foreach($flatResasLines as $key2 => $flatResasLine) {
					$canbeplaced=true;
					$flatResasLinesCount++;
					//echo "flatResasLine:".$key2."--------<br>";
					foreach($flatResasLine as $key3 => $flatResa) {
						
						$canbeplaced = $canbeplaced && ($flatResa->_start >= $resaObject->_end or $flatResa->_end <= $resaObject->_start);
						//echo "flatResa:".$key3."(".date('Y-m-d H:i:s',$flatResa->_start).">=".date('Y-m-d H:i:s',$resaObject->_end)." or ".date('Y-m-d H:i:s',$flatResa->_end)." <= ".date('Y-m-d H:i:s',$resaObject->_start).")<br>";
					}
					//echo "key:".$key."=".intval($canbeplaced)."<br>";
					if ($canbeplaced) break;
				}
				if (!$canbeplaced) $flatResasLinesCount++;
				$flatResasLines[$flatResasLinesCount][] = $resaObject;
			//}
		}
		
		//debug section-----------------------
		$flatResasLinesCount=0;
		foreach($flatResasLinesSerial as $key => $flatResasLine) {
			$out .= 'line'.$key.'<br>';
			foreach($flatResasLine as $flatResa) {
				$out .= $flatResa->sqlId.'='.date('Y-m-d H:i:s',$flatResa->_start).'-'.date('Y-m-d H:i:s',$flatResa->_end).'<br>';
			}
			$flatResasLinesCount++;
		}
		$out .= "-------------------------------<br>";
		foreach($flatResasLines as $flatResasLine) {
			$out .= 'line'.$flatResasLinesCount.'<br>';
			foreach($flatResasLine as $flatResa) {
				$out .= $flatResa->sqlId.'='.date('Y-m-d H:i:s',$flatResa->_start).'-'.date('Y-m-d H:i:s',$flatResa->_end).'<br>';
				//syslog(LOG_WARNING, "line:".date('Y-m-d H:i:s',$flatResa->_start).'-'.date('Y-m-d H:i:s',$flatResa->_end));
			}
			$flatResasLinesCount++;
		}

		$out .= "<br>C=".count($flatResasLines);
		//debug section-----------------------
		//echo $out;
		if ($debug) return $out;
		else return $flatResasLines;
	}
	
	public static function getReservationsForProductFamilly($mainProduct, $startDate, $endDate, $debug=false) {
		if ($mainProduct!=null) {
			$_timeslots = $mainProduct->_timeslotsObj;
			$productsIds=$mainProduct->getProductsId();
			$productsIdList = implode(',',$productsIds);
		} else $_timeslots=new myOwnTimeSlots();
		$id_shop = myOwnUtils::getShop();
		$startDay = date('Y-m-d',$startDate);
		$endDay = date('Y-m-d',$endDate);
		$query  = "
			SELECT *
			FROM  `". _DB_PREFIX_ ."myownreservations_reservation`
			WHERE (start_date <= '".$endDay."')
			AND (end_date >= '".$startDay."')";
			if ($mainProduct!=null && $productsIdList!='') $query .= " AND id_product IN (".$productsIdList.")";
			if ($id_shop>0) $query .= " AND (id_shop = ".$id_shop." OR id_shop = 0)";
		$out = $query;	

		$resas = Db::getInstance()->ExecuteS($query);
		if ($debug) return $query;
		$resaObjects = array();
		foreach ($resas as $resa) {

			if ( (array_key_exists($resa['start_timeslot'], $_timeslots->list) && array_key_exists($resa['end_timeslot'], $_timeslots->list))
				or ($resa['start_timeslot']==0 && $resa['end_timeslot']==0) ) {
				$resaObject = new myOwnReservation($resa['id_reservation'], $resa);
				$resaObject->_mainProduct = $mainProduct;
				$resaObjects[] = $resaObject;
			}
		}
		return $resaObjects;
	}
	
	public static function getAllReservationsForProductFamilly($mainProduct) {
		if ($mainProduct!=null) {
			$_timeslots = $mainProduct->_timeslotsObj;
			$productsIds=$mainProduct->getProductsId();
			$productsIdList = implode(',',$productsIds);
		} else $_timeslots=new myOwnTimeSlots();
		$id_shop = myOwnUtils::getShop();
		$query  = "
			SELECT *
			FROM  `". _DB_PREFIX_ ."myownreservations_reservation` WHERE id_reservation > 0";
			if ($mainProduct!=null) $query .= " AND id_product IN (".$productsIdList.")";
			if ($id_shop>0) $query .= " AND (id_shop = ".$id_shop." OR id_shop = 0)";

		$resas = Db::getInstance()->ExecuteS($query);

		$resaObjects = array();
		foreach ($resas as $resa) {

			if ( (array_key_exists($resa['start_timeslot'], $_timeslots->list) && array_key_exists($resa['end_timeslot'], $_timeslots->list))
				or ($resa['start_timeslot']==0 && $resa['end_timeslot']==0) ) {
				$resaObject = new myOwnReservation($resa['id_reservation'], $resa);
				$resaObject->_mainProduct = $mainProduct;
				$resaObjects[] = self::populateReservation($resaObject);
				//$resaObjects[] = $resaObject;
			}
		}
		return $resaObjects;
	}
	
	public static function getReservationsForProductOnPeriod($id_product, $id_product_attribute, $startDate, $endDate) {
		$id_shop = myOwnUtils::getShop();
		$startDay = date('Y-m-d',$startDate);
		$endDay = date('Y-m-d',$endDate);
		$query  = "
			SELECT *
			FROM  `". _DB_PREFIX_ ."myownreservations_reservation`
			WHERE (start_date <= '".$endDay."')
			AND (end_date >= '".$startDay."')";
			if ($id_product>0) $query .= " AND id_product = ".$id_product;
			if ($id_product_attribute>0) $query .= " AND id_product_attribute = ".$id_product_attribute;
			if ($id_shop>0) $query .= " AND (id_shop = ".$id_shop." OR id_shop = 0)";

		$resas = Db::getInstance()->ExecuteS($query);
		return $resas;
	}
	
	
	public static function getReservationsStartingOrEnding($date=0) {
		$id_shop = myOwnUtils::getShop();
		if ($date!=0) $day = date('Y-m-d',$date);
		else $day = date('Y-m-d');
		$query  = "
			SELECT *
			FROM  `". _DB_PREFIX_ ."myownreservations_reservation`
			WHERE (start_date = '".$day."' OR end_date = '".$day."') AND validated = 1 ";
			if ($id_shop>0) $query .= " AND (id_shop = ".$id_shop." OR id_shop = 0)";

		$resas = Db::getInstance()->ExecuteS($query);
		
		$resaObjects = array();
		foreach ($resas as $resa) {

				$resaObject = new myOwnReservation($resa['id_reservation'], $resa);
				$resaObjects[] = self::populateReservation($resaObject);
				//$resaObjects[] = $resaObject;
		}
		return $resaObjects;
	}
	
	public static function getFlatReservationsLines($obj, $resaObjects) {
		uasort($resaObjects, array('myOwnResas', 'sortForGlouton') );
		$out='';
		$flatResasLines = array();
		$flatResasLinesCount=0;
		foreach ($resaObjects as $resaObject) {
			//calculate product occupation
			if ($resaObject->_mainProduct==null) $resaObject->_mainProduct = $obj->_products->getResProductFromProduct($resaObject->id_product);
			$resaQty = $resaObject->getOccupationQty();
			//$_qtyCapacity=1;
			//if($resaObject->_mainProduct!=null) $_qtyCapacity= $resaObject->_mainProduct->_qtyCapacity;
			//if ($resaObject->_mainProduct->_qtyType == reservation_qty::IGNORE) $resaQty=1;
			//else $resaQty=ceil(($resaObject->quantity-$resaObject->quantity_refunded)/$_qtyCapacity);
			
			for ($i = 0; $i < $resaQty; $i++) {
				$flatResasLinesCount=0;
				$canbeplaced=false;
				foreach($flatResasLines as $flatResasLine) {
					$canbeplaced=true;
					$flatResasLinesCount++;
					foreach($flatResasLine as $flatResa)
						$canbeplaced = $canbeplaced && ($flatResa->getStartDateTime() >= $resaObject->getEndDateTime() or $flatResa->getEndDateTime() <= $resaObject->getStartDateTime());
					
					if ($canbeplaced) break;
				}
				if (!$canbeplaced) $flatResasLinesCount++;
				$flatResasLines[$flatResasLinesCount-1][] = $resaObject;
			}
		}
		$flatResasLinesCount=0;
		/*$out .= '-------------------<br>';
		foreach($flatResasLines as $flatResasLinesCount => $flatResasLine) {
			$out .= 'line'.$flatResasLinesCount.'<br>';
			foreach($flatResasLine as $flatResa) {
				$out .= $flatResa->sqlId.'='.date('Y-m-d H:i:s',$flatResa->getStartDateTime()).'-'.date('Y-m-d H:i:s',$flatResa->getEndDateTime()).'<br>';
			}
		}
		echo $out;*/
		return $flatResasLines;
	}

	public static function getReservationsForProduct($obj, $timeslots, $id_product, $id_product_attribute, $date, $reservationFilter, $timeslotFilter) {
		$seldDay = date('Y-m-d',$date);
		$id_shop = myOwnUtils::getShop();
		$notAfterTsReq="";$notBeforeTsReq="";
		$notAfterTsTest = false;
		$notBeforeTsTest = true;
		
		if (is_array($timeslots) && $timeslotFilter!='')
		foreach ($timeslots as $timeslot) {
			if ($notAfterTsTest) $notAfterTsReq .= "AND start_timeslot != ".$timeslot->sqlId." ";
			if ($timeslot->sqlId == $timeslotFilter) $notAfterTsTest = true;
			if ($timeslot->sqlId == $timeslotFilter) $notBeforeTsTest = false;
			if ($notBeforeTsTest) $notBeforeTsReq .= "AND end_timeslot != ".$timeslot->sqlId." ";
		}	
		$query  = "
			SELECT *
			FROM  `". _DB_PREFIX_ ."myownreservations_reservation`
			WHERE (start_date < '".$seldDay."' OR (start_date = '".$seldDay."' ".$notAfterTsReq."))
			AND (end_date > '".$seldDay."' OR (end_date = '".$seldDay."' ".$notBeforeTsReq."))";
			if ($id_product>-1) $query .= " AND id_product=".$id_product;
			if ($id_product_attribute>-1) $query .= " AND id_product_attribute = ".$id_product_attribute;
			if ($reservationFilter=='start') $query .= " AND start_date = '".$seldDay."'";
			if ($reservationFilter=='end') $query .= " AND end_date = '".$seldDay."'";
			if ($timeslotFilter!='') {
				if ($reservationFilter=='start') $query .= " AND start_timeslot = '".$timeslotFilter."'";
				if ($reservationFilter=='end') $query .= " AND end_timeslot = '".$timeslotFilter."'";
			}
			if ($id_shop>0) $query .= " AND (id_shop = ".$id_shop." OR id_shop = 0)";
		$out = $query;	

		$resas = Db::getInstance()->ExecuteS($query);
		$resaObjects = array();
		foreach ($resas as $resa) {

			if ( ($resa['start_timeslot']==0 && $resa['end_timeslot']==0) 
					or (array_key_exists($resa['start_timeslot'], $obj->_timeSlots->list) && array_key_exists($resa['end_timeslot'], $obj->_timeSlots->list))
				  ) {
				$resaObject = new myOwnReservation($resa['id_reservation'], $resa);
				$resaObject->_mainProduct = $obj->_products->getResProductFromProduct($resaObject->id_product);
				$resaObjects[] = self::populateReservation($resaObject);
			}
		}
		$flatResasLines = self::getFlatReservationsLines($obj, $resaObjects);
		return $flatResasLines;
	}
	
	public static function getReservationsForSerial($serial, $mainProduct=null) {
		 $_timeslots = new myOwnTimeSlots();
		 
			$query  = "
			SELECT * 
			FROM `" . _DB_PREFIX_ . "myownreservations_reservation` 
			LEFT JOIN `" . _DB_PREFIX_ . "orders` ON (" . _DB_PREFIX_ . "myownreservations_reservation.id_order = " . _DB_PREFIX_ . "orders.id_order) 
			WHERE " . _DB_PREFIX_ . "myownreservations_reservation.stock LIKE '%".trim($serial)."%' ";

			 $resas = Db::getInstance()->ExecuteS($query);

			 $resaObjects = array();
			 foreach ($resas as $resa) {
			 	//if ( array_key_exists($resa['start_timeslot'], $_timeslots->list) && array_key_exists($resa['end_timeslot'], $_timeslots->list) ) {
			 		$resaObject = new myOwnReservation($resa['id_reservation'], $resa);
			 		$resaObject->_deliveryId = $resa['delivery_number'];
			 		$resaObject->customerId = $resa['id_customer'];
			 		$resaObject->_customerAddressId = $resa['id_address_delivery'];
			 		if ($mainProduct!=null) $resaObject->_mainProduct = $mainProduct;
			 		
			 		if ($resaObject->id_order>0) $resaObject = myOwnResas::populateReservation($resaObject);
			 		//echo ' '.$resaObject->id_product.' ';
			 		$resaObjects[] = $resaObject;
			 	//}
			 }
			 
		return $resaObjects;
	}
	
	public static function isReservationByReference($reference) {
		$query  = "
			SELECT id_reservation
			FROM `" . _DB_PREFIX_ . "myownreservations_reservation` 
			WHERE " . _DB_PREFIX_ . "myownreservations_reservation.reference LIKE '%".trim($reference)."%' ";

		$resas = Db::getInstance()->ExecuteS($query);

		if (count($resas))
			return (int)$resas[0]['id_reservation'];
		else return false;
	}
	
	public static function getReservationsOfDayAndTimeSlot($day, $timeSlotId, $productFilter, $status, $reservationFilter, $reservationId, $exept) {
		$_timeslots = new myOwnTimeSlots();
		$id_shop = myOwnUtils::getShop();
		 //if (intval($timeSlotId)>0) $timeslot = $_timeslots->list[$timeSlotId];
		 if ($day!="") $dayStr = date("Y-m-d",$day);
		 
		 if ($reservationId=="") {
			$notAfterTsReq="";$notBeforeTsReq="";
			if ($timeSlotId>0) {
				$notAfterTsTest = false;
				$notBeforeTsTest = true;
				foreach ($_timeslots->list as $timeslot) {
					if ($notAfterTsTest) $notAfterTsReq .= "AND start_timeslot != ".$timeslot->sqlId." ";
					if ($timeslot->sqlId == $timeSlotId) {
						$notAfterTsTest = true;
						$notBeforeTsTest = false;
					}
					if ($notBeforeTsTest) $notBeforeTsReq .= "AND end_timeslot != ".$timeslot->sqlId." ";
				}
			}
			$query  = "
			SELECT * 
			FROM `" . _DB_PREFIX_ . "myownreservations_reservation` as resas
			LEFT JOIN `" . _DB_PREFIX_ . "orders` ON (resas.id_order = " . _DB_PREFIX_ . "orders.id_order) 
			WHERE  (start_date < '".$dayStr."' OR (start_date = '".$dayStr."' ".$notAfterTsReq."))
			AND (end_date > '".$dayStr."' OR (end_date = '".$dayStr."' ".$notBeforeTsReq."))";
			if ($productFilter!="") $query .= " AND id_product = ".$productFilter;
			if ($reservationFilter=="1" && $timeSlotId>0 && array_key_exists($timeSlotId, $_timeslots->list)) $query .= " AND (start_timeslot = ".$timeSlotId." OR start_timeslot = 0)";
			if ($reservationFilter=="1") $query .= " AND start_date = '".$dayStr."'";
			if ($reservationFilter=="2" && $timeSlotId>0 && array_key_exists($timeSlotId, $_timeslots->list)) $query .= " AND (end_timeslot = ".$timeSlotId." OR end_timeslot = 0)";
			if ($reservationFilter=="2") $query .= " AND end_date = '".$dayStr."'";
			if ($id_shop>0) $query .= " AND (resas.id_shop = '".$id_shop."' OR resas.id_shop = 0)";
			$query .= " ORDER BY id_product, id_product_attribute";

		} else {
			$query  = "
			SELECT * 
			FROM `" . _DB_PREFIX_ . "myownreservations_reservation` 
			LEFT JOIN `" . _DB_PREFIX_ . "orders` ON (" . _DB_PREFIX_ . "myownreservations_reservation.id_order = " . _DB_PREFIX_ . "orders.id_order) 
			WHERE " . _DB_PREFIX_ . "myownreservations_reservation.id_reservation = ".$reservationId;
		}
			 
			 $resas = Db::getInstance()->ExecuteS($query);

			 $resaObjects = array();
			 foreach ($resas as $resa) {
			 	if ( $reservationId!="" or ($resa['start_timeslot']==0 && $resa['end_timeslot']==0) or (array_key_exists($resa['start_timeslot'], $_timeslots->list) && array_key_exists($resa['end_timeslot'], $_timeslots->list)) ) {
			 		$resaObject = new myOwnReservation($resa['id_reservation'], $resa);
			 		$resaObject->_deliveryId = $resa['delivery_number'];
			 		$resaObject->customerId = $resa['id_customer'];
			 		$resaObject->_customerAddressId = $resa['id_address_delivery'];

			 		if ($resaObject->id_order>0) $resaObject = myOwnResas::populateReservation($resaObject);
			 		//echo ' '.$resaObject->id_product.' ';
			 		if ( ($status == "" or $status==substr($resaObject->_orderStatus,0,1))
			 			AND ($exept == "" or $resaObject->sqlId!=$exept))
			 			$resaObjects[] = $resaObject;
			 	}
			 }
			 
		return $resaObjects;
	}
	
	public static function getOtherReservationsOfDay($day, $exept) {
		 $_timeslots = new myOwnTimeSlots();
		 if ($day!="") $dayStr = date("Y-m-d",$day);

			$query  = "
			SELECT * 
			FROM `" . _DB_PREFIX_ . "myownreservations_reservation` res
			LEFT JOIN `" . _DB_PREFIX_ . "orders` o ON (res.id_order = o.id_order) 
			LEFT JOIN `" . _DB_PREFIX_ . "myownreservations_timeslot` tsstart ON (tsstart.id_timeSlot = res.start_timeslot ) 
			LEFT JOIN `" . _DB_PREFIX_ . "myownreservations_timeslot` tsend ON (tsend.id_timeSlot = res.end_timeslot ) 
			WHERE (start_date = '".$dayStr."' AND (tsstart.day". date("N",$day) ." = 0 OR (tsstart.id_timeSlot IS NULL AND res.start_timeslot!=0)))
				OR (end_date = '".$dayStr."' AND (tsend.day". date("N",$day) ." = 0 OR (tsend.id_timeSlot IS NULL AND res.end_timeslot!=0)))";
			
			
			$query .= " ORDER BY res.id_product";
			 $resas = Db::getInstance()->ExecuteS($query);
			
			 $resaObjects = array();
			 if (count($resas)>0)
			 foreach ($resas as $resa) {
			 		$resaObject = new myOwnReservation($resa['id_reservation'], $resa);
			 		$resaObject->_deliveryId = $resa['delivery_number'];
			 		$resaObject->customerId = $resa['id_customer'];
			 		$resaObject->_customerAddressId = $resa['id_address_delivery'];
			 		
			 		if ($resaObject->id_order>0) $resaObject = myOwnResas::populateReservation($resaObject);
			 		
			 		if ( $exept == "" or $resaObject->sqlId!=$exept)
			 			$resaObjects[] = $resaObject;
			 }
			 
		return $resaObjects;
	}
	
	public static function getReservationsOfDayCount($day, $mainProduct=null) {
		if ($day!="") $dayStr = date("Y-m-d",$day);
		$id_shop = myOwnUtils::getShop();
		if ($mainProduct!=null) {
			$productsIds=$mainProduct->getProductsId();
			$productsIdList = implode(',',$productsIds);
		}
		$query  = "
		SELECT * 
		FROM `" . _DB_PREFIX_ . "myownreservations_reservation` res
		LEFT JOIN `" . _DB_PREFIX_ . "orders` ON (res.id_order = " . _DB_PREFIX_ . "orders.id_order) 
		WHERE  start_date <= '".$dayStr."' AND end_date >= '".$dayStr."' AND validated=1";
		if ($mainProduct!=null)  $query .= " AND id_product IN (".$productsIdList.")";
		if ($id_shop>0) $query .= " AND (res.id_shop = ".$id_shop." OR res.id_shop = 0)";

		$resas = Db::getInstance()->ExecuteS($query);

		return count($resas);
	}
	
	public static function getUnvalidatedReservationsOfDayCount($day, $mainProduct=null) {
		if ($day!="") $dayStr = date("Y-m-d",$day);
		$id_shop = myOwnUtils::getShop();
		if ($mainProduct!=null) {
			$productsIds=$mainProduct->getProductsId();
			$productsIdList = implode(',',$productsIds);
		}
		$query  = "
		SELECT * 
		FROM `" . _DB_PREFIX_ . "myownreservations_reservation` res
		LEFT JOIN `" . _DB_PREFIX_ . "orders` ON (res.id_order = " . _DB_PREFIX_ . "orders.id_order) 
		WHERE  start_date <= '".$dayStr."' AND end_date >= '".$dayStr."' AND validated=0";
		if ($mainProduct!=null)  $query .= " AND id_product IN (".$productsIdList.")";
		if ($id_shop>0) $query .= " AND (res.id_shop = ".$id_shop." OR res.id_shop = 0)";
		
		$resas = Db::getInstance()->ExecuteS($query);

		return count($resas);
	}
	
	public static function getOtherReservationsOfDayCount($day, $mainProduct=null) {
		if ($day!="") $dayStr = date("Y-m-d",$day);
		$id_shop = myOwnUtils::getShop();
		if ($mainProduct!=null) {
			$productsIds=$mainProduct->getProductsId();
			$productsIdList = implode(',',$productsIds);
		}
		$query  = "
		SELECT * 
		FROM `" . _DB_PREFIX_ . "myownreservations_reservation` res
		LEFT JOIN `" . _DB_PREFIX_ . "orders` o ON (res.id_order = o.id_order) 
		LEFT JOIN `" . _DB_PREFIX_ . "myownreservations_timeslot` tsstart ON (tsstart.id_timeSlot = res.start_timeslot ) 
		LEFT JOIN `" . _DB_PREFIX_ . "myownreservations_timeslot` tsend ON (tsend.id_timeSlot = res.end_timeslot ) 
		WHERE (start_date = '".$dayStr."' AND (tsstart.day". date("N",$day) ." = 0 OR tsstart.id_timeSlot IS NULL AND res.start_timeslot!=0))
			OR (end_date = '".$dayStr."' AND (tsend.day". date("N",$day) ." = 0 OR tsend.id_timeSlot IS NULL AND res.end_timeslot!=0))";
		if ($mainProduct!=null)  $query .= " AND res.id_product IN (".$productsIdList.")";
		if ($id_shop>0) $query .= " AND (res.id_shop = ".$id_shop." OR res.id_shop = 0)";
				
		$resas = Db::getInstance()->ExecuteS($query);
			 
		return count($resas);
	}
	
	public static function rescheduleReservation($id, $startDate, $startTimeslot, $endDate, $endTimeslot) {
		//43;10;0;2011-07-17;3;2011-07-25;1
		$query  = 'UPDATE '. _DB_PREFIX_ .'myownreservations_reservation SET start_date = "'.$startDate.'", start_timeslot = '.$startTimeslot.', end_date = "'.$endDate.'", end_timeslot = '.$endTimeslot.' WHERE id_reservation = '.$id.';';
		return Db::getInstance()->Execute($query);
	}
	
	public static function changeValdiationReservation($id, $status) {
		$query  = 'UPDATE '. _DB_PREFIX_ .'myownreservations_reservation SET validated = '.intval($status).' WHERE id_reservation = '.$id.';';
		return Db::getInstance()->Execute($query);
	}
	
	public static function setReservationStock($id, $stock) {
		$query  = 'UPDATE '. _DB_PREFIX_ .'myownreservations_reservation SET stock = "'.$stock.'" WHERE id_reservation = '.$id.';';

		return Db::getInstance()->Execute($query);
	}
	
}
	
?>