<?php
/*
* 2010-2012 LaBulle All right reserved
*/
	
class myOwnCart {
	var $sqlId=0;
	var $id_cart=0;
	var $id_product=0;
	var $id_product_attribute=0;
	var $id_customization;
	var $id_object=0;
	var $quantity=1;
	var $priceQuantity=1;
	var $startDate;
	var $startTimeslot;
	var $endDate;
	var $endTimeslot;
	var $price;
	var $oldPrice;
	var $tax_rate;
	var $discounts;
	var $discountIds;
	var $_productPrice=-1;
	var $_oldProductPrice=-1;
	var $_mainProduct;
	//var $_pricerules;
	var $_isReducVisible=false;
	var $_fixedLength=0;
	var $_highPrice;
	
	//Create a timeslot in database
	//-----------------------------------------------------------------------------------------------------------------
	public function sqlInsert() {
		$query  = 'INSERT INTO '. _DB_PREFIX_ .'myownreservations_cartproducts (id_cart, id_product, id_product_attribute, id_customization, id_object, quantity, start_date, start_timeslot, end_date, end_timeslot) VALUES ('.$this->id_cart.', '.$this->id_product.', '.$this->id_product_attribute.', '.intval($this->id_customization).', '.intval($this->id_object).', '.$this->quantity.', "'.$this->startDate.'", '.(int)$this->startTimeslot.', "'.$this->endDate.'", '.(int)$this->endTimeslot.');';

		$res = Db::getInstance()->Execute($query);
		$inserted = Db::getInstance()->ExecuteS("SELECT LAST_INSERT_ID() AS ID;");
		if ($res) return $inserted[0]['ID'];
		else return false;
	}

	//Delete a timeslot in database
	//-----------------------------------------------------------------------------------------------------------------
	public function sqlDeleteAll() {
		$query  = 'DELETE FROM '. _DB_PREFIX_ .'myownreservations_cartproducts WHERE id_cart = '.$this->id_cart.' AND id_product = '.$this->id_product.' AND id_product_attribute = '.$this->id_product_attribute.' AND id_customization = '.intval($this->id_customization).';';
		return Db::getInstance()->Execute($query);
	}
	public function sqlDelete() {
		$query  = 'DELETE FROM '. _DB_PREFIX_ .'myownreservations_cartproducts WHERE id_cartproduct = '.$this->sqlId.';';
		
		myOwnExtensions::removeAllDetail(-$this->sqlId);
		return Db::getInstance()->Execute($query);
	}
	
	//Update a timeslot in database
	//-----------------------------------------------------------------------------------------------------------------
	function sqlUpdate() {
		$query  = 'UPDATE '. _DB_PREFIX_ .'myownreservations_cartproducts SET quantity = "'.$this->quantity.'", start_date = "'.$this->startDate.'", start_timeslot = '.$this->startTimeslot.', end_date = "'.$this->endDate.'", end_timeslot = '.$this->endTimeslot.' WHERE id_cartproduct = '.$this->sqlId.' AND id_cart = '.$this->id_cart.' AND id_product = '.$this->id_product.' AND id_product_attribute = '.$this->id_product_attribute.' AND id_customization = '.intval($this->id_customization).' AND id_object = '.intval($this->id_object).';';
	
		return Db::getInstance()->Execute($query);
	}
	function sqlUpdateQty() {
		$query  = 'UPDATE '. _DB_PREFIX_ .'myownreservations_cartproducts SET quantity = "'.$this->quantity.'" WHERE id_cartproduct = '.$this->sqlId.';';
		return Db::getInstance()->Execute($query);
	}

	//-----------------------------------------------------------------------------------------------------------------
	//SETERS
	//-----------------------------------------------------------------------------------------------------------------
	
	function updateQuantity($qty) {
		$this->quantity=$qty;
		$this->sqlUpdate();
	}
	
	function checkAndUpdateQty($operator, $cartQuantity, $cart, $availabilities, $stocks) {
		global $cookie;
		if ($operator == 'up') {
			$this->quantity+=$cartQuantity;
			if ($this->isAvailable($cart, $availabilities, $stocks, true)>0)
				return $this->sqlUpdateQty();
			else return false;
		} elseif ($operator == 'down') {
			$this->quantity-=$cartQuantity;
			if (!$this->quantity)
				return $this->sqlDelete();
			else return $this->sqlUpdateQty();
		}
	}
	
	function checkQty($operator, $cartQuantity, $cart, $availabilities, $stocks) {
		$isAvailable=false;
		if ($operator == 'up') {
			$way=1;
		} elseif ($operator == 'down') {
			$way=-1;
		}
		//updating qty for availability test
		$this->quantity+=$way * $cartQuantity;
		
		if ($this->quantity == 0 or $this->isAvailable($cart, $availabilities, $stocks, true)>0)
			$isAvailable = true;
		
		//restoring after availability test
		$this->quantity-=$way * $cartQuantity;
		
		return $isAvailable;
		
		
	}

	function calculateDayPeriod($setprices=null, $isratio=false) {
		$id_currency = myOwnUtils::getCurrencyObj()->id;
		if ($this->startDate=="") $start = strtotime($this->endDate);
		else $start = strtotime($this->startDate);
		$end = strtotime($this->endDate);
		$length = -1;
		$price=0;
		$cnt=0;
		$ratiosum=0;
		do {
			$length++;
			$checkLimit = strtotime("+".$length." day",$start);
			if (  ($this->_mainProduct->_priceDisabledTimeSlots
				    or $this->_mainProduct->_timeslotsObj->areEnableOnDay($checkLimit))
				and (date('Y-m-d',$checkLimit) < date('Y-m-d',$end)
					or !$this->_mainProduct->_priceEndTimeSlot
					or $length==0) ) {
						if ($setprices!=null) {
							if (MYOWNRES_PRICESET_BYWEEK) $key=date("N",$checkLimit);
							else $key=date("z",$checkLimit);
							if ($isratio) {
								$cnt++;
								if (array_key_exists($key, $setprices)) {
									$ratiosum += $setprices[$key];
								} $ratiosum += 1;
							} else {
								if (array_key_exists($key, $setprices)) {
									$price+= Tools::convertPrice($setprices[$key], $id_currency);
								} else $price+= $this->_productPrice;
							}
						}
			}
		} while ( date('Y-m-d',$checkLimit) < date('Y-m-d',$end) && $length<10000 );
		
		if ($isratio) return ($cnt ? $ratiosum/$cnt : 1);
		elseif ($setprices!=null) return $price;
		else return $length;
	}
	
	function calculateWeekPeriod($setprices=null, $isratio=false) {
//echo 'calculateWeekPeriod';
		$id_currency = myOwnUtils::getCurrencyObj()->id;
		if ($this->startDate=="") $start = strtotime($this->endDate);
		else $start = strtotime($this->startDate);
		$end = strtotime($this->endDate);
		$length=-1;
		$price=0;
		$cnt=0;
		$ratiosum=0;
		$weeklength=0;
		do {
			$length++;
			$checkLimit = strtotime("+".$length." day",$start);

			if (date("N",$checkLimit) == $this->_mainProduct->_reservationSelWeekStart && (date('Y-m-d',$checkLimit) < date('Y-m-d',$end) or $this->_mainProduct->_priceStrict)) {
//echo date('Y-m-d',$checkLimit).'<'.date('Y-m-d',$end).'  ';
				if ($setprices!=null) {
					$key = date("W",$checkLimit);
					if ($isratio) {
						$cnt++;
						if (array_key_exists($key, $setprices)) {
							$ratiosum += $setprices[$key];
						} $ratiosum += 1;
					} else {
						if (array_key_exists($key, $setprices)) {
							$price+= Tools::convertPrice($setprices[$key], $id_currency);
						} else $price+= $this->_productPrice;
					}
				}
				$weeklength++;
//echo 'wl'.$weeklength;
			}
		} while ( date('Y-m-d',$checkLimit) < date('Y-m-d',$end) && $length<10000 );
		
		if ($isratio) return ($cnt ? $ratiosum/$cnt : 1);
		elseif ($setprices!=null) return $price;
		else return $weeklength;
	}
	
	function calculateMonthPeriod($setprices=null, $isratio=false) {
		$id_currency = myOwnUtils::getCurrencyObj()->id;
		if ($this->startDate=="") $start = strtotime($this->endDate);
		else $start = strtotime($this->startDate);
		$end = strtotime($this->endDate);
		$length=-1;
		$price=0;
		$cnt=0;
		$ratiosum=0;
		$monthlength=0;
		do {
			$length++;
			$checkLimit = strtotime("+".$length." day",$start);

			if ($checkLimit== myOwnCalendarMonth::getDay(date('m',$checkLimit), date('Y',$checkLimit),"start",$this->_mainProduct->_reservationSelMonthStart)
				) {
				// && (date('Y-m-d',$checkLimit) < date('Y-m-d',$end) or $this->_mainProduct->_priceStrict)
				if ($setprices!=null) {
					$key = date("n",$checkLimit);
					if ($isratio) {
						$cnt++;
						if (array_key_exists($key, $setprices)) {
							$ratiosum += $setprices[$key];
						} $ratiosum += 1;
					} else {
						if (array_key_exists($key, $setprices)) {
							$price+= Tools::convertPrice($setprices[$key], $id_currency);
						} else $price+= $this->_productPrice;
					}
				}
				$monthlength++;
			}
		} while ( date('Y-m-d',$checkLimit) < date('Y-m-d',$end) && $length<10000 );
		
		if ($isratio) return ($cnt ? $ratiosum/$cnt : 1);
		elseif ($setprices!=null) return $price;
		else return $monthlength;
	}

	public function calculateTimeslotPeriod($setprices, $isratio=false) {
		$timeslots = $this->_mainProduct->_timeslotsObj->list;
		$id_currency = myOwnUtils::getCurrencyObj()->id;
		if ($this->startDate=="") $start = strtotime($this->endDate);
		else $start = strtotime($this->startDate);
		$end = strtotime($this->endDate);
		$price=0;
		$cnt=0;
		$ratiosum=0;
		$daylength = -1;
		$tslength = 0;
		$afterstart=false;
		do {
			$daylength++;
			$checkLimit = strtotime("+".$daylength." day",$start);
			if (is_array($timeslots))
			foreach($timeslots as $timeslot) {
				if ($timeslot->sqlId == $this->startTimeslot) $afterstart=true;
				if ($afterstart) {
					//calc if day enabled or not ignoring last timeslot
					if (  ($this->_mainProduct->_priceDisabledTimeSlots
						    or $timeslot->isEnableOnDay($checkLimit)
						and (date('Y-m-d',$checkLimit) < date('Y-m-d',$end)
							or $timeslot->sqlId != $this->endTimeslot
							or !$this->_mainProduct->_priceEndTimeSlot
							or $tslength==0) ) ) {
								if ($setprices!=null) {
									if ($isratio) {
										$cnt++;
										if (array_key_exists($timeslot->sqlId, $setprices)) {
											$ratiosum += $setprices[$timeslot->sqlId];
										} $ratiosum += 1;
									} else {
										if (array_key_exists($timeslot->sqlId, $setprices)) {
											$tsprice=$setprices[$timeslot->sqlId];
											//if ($tsprice) 
											//echo '+'.$tsprice;
											$price+= Tools::convertPrice($tsprice, $id_currency);
											//else $price+= $this->_productPrice;
										} else {
											$price+= $this->_productPrice;
										}
									}
								}
								$tslength++;
					}
				}
				if (date('Y-m-d',$checkLimit) == date('Y-m-d',$end) && $timeslot->sqlId == $this->endTimeslot) break 2;
			}
		} while ( date('Y-m-d',$checkLimit) < date('Y-m-d',$end) && $daylength<10000 );
		
		if ($isratio) return ($cnt ? $ratiosum/$cnt : 1);
		elseif ($setprices!=null) return $price;
		else return $tslength;
	}
	
	function setPrice($pricerules, $setprices=null, $availabilities = null, $productprice=-1, $taxrate=-1, $cartresas=null) { //need $cartresas to check price rule qty
		
		//get price without taxes
		if ($productprice==-1) {
			if ($this->_productPrice==-1) {
				$this->_productPrice = MyOwnReservationsUtils::getProductPriceTaxes($this->id_product, $this->id_product_attribute, false, $this->_mainProduct->_optionAlsoSell, $this->quantity);
				$this->_oldProductPrice = MyOwnReservationsUtils::getOldProductPriceTaxes($this->id_product, $this->id_product_attribute, false, $this->_mainProduct->_optionAlsoSell, $this->quantity);
				if ($this->_productPrice == $this->_oldProductPrice) $this->_oldProductPrice=-1;
			}
		} else $this->_productPrice = $productprice;
		
		if ($taxrate==-1) {
			//productprice may be specific price so we can't get this for tax rate
			if ($productprice == $this->_productPrice) $productPrice_without_taxes = MyOwnReservationsUtils::getProductPriceTaxes($this->id_product, $this->id_product_attribute, false, $this->_mainProduct->_optionAlsoSell, $this->quantity);
			else $productPrice_without_taxes = $this->_productPrice;
			$productPrice_with_taxes = MyOwnReservationsUtils::getProductPriceTaxes($this->id_product, $this->id_product_attribute, true, $this->_mainProduct->_optionAlsoSell, $this->quantity);
			if ($this->_productPrice > 0) $this->tax_rate = (($productPrice_with_taxes-$productPrice_without_taxes)/$productPrice_without_taxes)*100;
			else $this->tax_rate = 0;
		} else $this->tax_rate = $taxrate;
		
		if ($this->_fixedLength==0)
			$length = $this->getLength(true, $availabilities);
		//to calculate min price with best perf on category page
		else $length = $this->_fixedLength;
// echo '_setPrice'.$this->endDate.':'.(int)$length.get_class($length);
		if ($setprices===null) {
			if ($this->_mainProduct->_reservationPriceType==reservation_price::TABLE_RATE) {
				$isPricesSet = myOwnPricesSets::isPricesSetForProduct($this->id_product);
				$setprices = array(
					reservation_price::TABLE_TIME => ($isPricesSet ? myOwnPricesSets::getPricesSetForProduct($this->id_product, $this->id_product_attribute) : myOwnPricesSets::getPricesSetForFamily($this->_mainProduct, reservation_price::TABLE_TIME)),
					reservation_price::TABLE_DATE => myOwnPricesSets::getPricesSetForFamily($this->_mainProduct, reservation_price::TABLE_DATE)
				);
				
			} else $setprices = myOwnPricesSets::getPricesSetForProduct($this->id_product, $this->id_product_attribute);
			
		}
		//calc with distinct unit price
		$combPrice=0;
		if ($this->_mainProduct->_priceCombination && $this->id_product_attribute) 
			$combPrice = Combination::getPrice($this->id_product_attribute);

		if (count($setprices) && $this->_mainProduct->_reservationPriceType==reservation_price::TABLE_DATE) {
				switch ($this->_mainProduct->_priceUnit) { 
					case reservation_unit::TIMESLOT: 
						$this->price = $this->calculateTimeslotPeriod($setprices);
						break;
					case reservation_unit::WEEK: 
						$this->price = $this->calculateWeekPeriod($setprices);
						break;
					case reservation_unit::MONTH: 
						$this->price = $this->calculateMonthPeriod($setprices);
						break;
					case reservation_unit::DAY: 
					default:
						$this->price = $this->calculateDayPeriod($setprices);
						break;
				}
		} else if (count($setprices) && $this->_mainProduct->_reservationPriceType==reservation_price::TABLE_TIME) {
			$id_currency = myOwnUtils::getCurrencyObj()->id;

			if (array_key_exists((int)$length, $setprices) && $setprices[$length]>0) {
				$this->price= Tools::convertPrice($setprices[$length], $id_currency);
			} else {
				$this->price= $this->_productPrice*$length;
				if ($this->_oldProductPrice>-1) $this->oldPrice= $this->_oldProductPrice*$length;
			}
		} else if ($this->_mainProduct->_reservationPriceType==reservation_price::TABLE_RATE) {
			$id_currency = myOwnUtils::getCurrencyObj()->id;
			$period_ratio=1;
			switch ($this->_mainProduct->_priceUnit) { 
				case reservation_unit::TIMESLOT: 
					$period_ratio = $this->calculateTimeslotPeriod($setprices[reservation_price::TABLE_DATE], true);
					break;
				case reservation_unit::WEEK: 
					$period_ratio = $this->calculateWeekPeriod($setprices[reservation_price::TABLE_DATE], true);
					break;
				case reservation_unit::MONTH: 
					$period_ratio = $this->calculateMonthPeriod($setprices[reservation_price::TABLE_DATE], true);
					break;
				case reservation_unit::DAY: 
				default:
					$period_ratio = $this->calculateDayPeriod($setprices[reservation_price::TABLE_DATE], true);
					break;
			}

			if (array_key_exists((int)$length, $setprices[reservation_price::TABLE_TIME]) && $setprices[reservation_price::TABLE_TIME][$length]>0) {
				$this->price= Tools::convertPrice($setprices[reservation_price::TABLE_TIME][$length]*$this->_productPrice, $id_currency);
			} else {
				$this->price= $this->_productPrice*$length;
				if ($this->_oldProductPrice>-1) $this->oldPrice= $this->_oldProductPrice*$length;
			}
			$this->price= $this->price*$period_ratio;
		
		//classic length price
		} else if ($this->_mainProduct->_reservationPriceType==reservation_price::PRODUCT_TIME) {
			$this->price = $this->_productPrice * $length;
			if ($this->_oldProductPrice>-1) $this->oldPrice = $this->_oldProductPrice * $length;
		} else if ($this->_mainProduct->_priceDoubleIfLength) {
			$this->price = $this->_productPrice * 2;
			if ($this->_oldProductPrice>-1) $this->oldPrice = $this->_oldProductPrice * 2;
		}
		else {
			$this->price = $this->_productPrice;
			if ($this->_oldProductPrice>-1) $this->oldPrice = $this->_oldProductPrice;
		}
		$this->_highPrice = $this->price;
		if ($combPrice>0 && $length>1) {
			$this->price -= ($combPrice*($length-1));
			if ($this->_oldProductPrice>-1) $this->oldPrice -= ($combPrice*($length-1));
		}
		//Keep only the price impact with price set with the greater bound
		$setImpact=null;
		$originalPrice=$this->price;
		$appliedDiscounts=array();
		$appliedDiscountIds=array();

		//on prend le prix fixe pour la plus grande durée qui s'applique
		foreach($pricerules as $pricerule)
			if ($pricerule->impact==pricesrules_impact::SET && $pricerule->doesApplyToResa($this, $cartresas))
				if ($setImpact==null or $setImpact->getMaxBound()<$pricerule->getMaxBound()) $setImpact=$pricerule;
		
		if ($setImpact!=null) {
			$fixedAmount=Tools::convertPrice($setImpact->amount/(1+($this->tax_rate/100)));
			//dans le cas ou le prix fixe continue a s'appliquer on fait le calcul sur la partie ou s'applique le prix total
			if ($setImpact->getMaxBound() > $length) $originalPartPrice = $originalPrice;
			else $originalPartPrice = $this->_productPrice*$setImpact->getMaxBound();
			$this->price += $fixedAmount-$originalPartPrice;

			if ($setImpact->visible) {
				$this->_isReducVisible = true;
				$appliedDiscounts[] = $setImpact->name.':'.($fixedAmount).'/'.myOwnLang::getUnit($this->_mainProduct->_priceUnit, false);
				$appliedDiscountIds[] = $setImpact->sqlId;
			}
		}
		
		foreach($pricerules as $pricerule)
			if ($pricerule->impact!=pricesrules_impact::SET && $pricerule->doesApplyToResa($this, $cartresas) && !$pricerule->overall) {
				if ($pricerule->visible) $this->_isReducVisible = true;
				//calculate resa
				$reducAmount=Tools::convertPrice($pricerule->amount/(1+($this->tax_rate/100)));
			
				$reduc_length = $pricerule->getResaReduc($this, $availabilities);
				if ($pricerule->impact==pricesrules_impact::PERIOD) {
					$reduc = ($reduc_length*$reducAmount)-($reduc_length*$this->_productPrice);
				} else {
					$reduc = $pricerule->ratio*($this->_productPrice*$reduc_length)*($pricerule->impact==pricesrules_impact::DECREASE ? -1 : 1)+$reducAmount*($pricerule->impact==pricesrules_impact::DECREASE ? -1 : 1);
				}
				
				$this->price += $reduc;
				if ($pricerule->visible) {
					$appliedDiscounts[] = $pricerule->name.':'.$reduc;
					$appliedDiscountIds[] = $pricerule->sqlId;
				}
			}
		$best_percent_rule=0;$best_percent=100;
		if (Configuration::get('MYOWNRES_PRICERULE_BEST')) {
			foreach($pricerules as $pricerule)
				if ($pricerule->impact!=pricesrules_impact::SET && $pricerule->doesApplyToResa($this, $cartresas) && $pricerule->overall) {
					$percent = ($pricerule->impact==pricesrules_impact::DECREASE ? -1 : 1)*$pricerule->ratio;
					if ( $percent < $best_percent || $best_percent  == 100) {
						$best_percent = $percent;
						$best_percent_rule = $pricerule->sqlId;
					}
				}
		}
		foreach($pricerules as $pricerule)
			if (($best_percent_rule==0 || $pricerule->sqlId==$best_percent_rule) && $pricerule->impact!=pricesrules_impact::SET && $pricerule->doesApplyToResa($this, $cartresas) && $pricerule->overall) {
				if ($pricerule->visible) $this->_isReducVisible = true;
				//calculate resa
				$reducAmount=Tools::convertPrice($pricerule->amount/(1+($this->tax_rate/100)));
				
				if ($pricerule->type==pricesrules_type::HISTORY) {
					$lengthdividedbyqty = $pricerule->getResaReduc($this, $availabilities);
					$reduc = $pricerule->ratio*($this->_productPrice)*$lengthdividedbyqty*($pricerule->impact==pricesrules_impact::DECREASE ? -1 : 1)+$reducAmount*($lengthdividedbyqty/$length)*($pricerule->impact==pricesrules_impact::DECREASE ? -1 : 1);
				} else if ($pricerule->impact==pricesrules_impact::PERIOD) {
					$reduc = ($length*$reducAmount)-($length*$this->_productPrice);
				} else {
					$reduc = $pricerule->ratio*($this->price)*($pricerule->impact==pricesrules_impact::DECREASE ? -1 : 1)+$reducAmount*($pricerule->impact==pricesrules_impact::DECREASE ? -1 : 1);
				}
				if (!$pricerule->quantity && $this->quantity>1 && !$this->sqlId)
					$reduc = $reduc / $this->quantity;
				if (!$pricerule->quantity && $this->getOccupationQty(true)>1 && $this->sqlId)
					$reduc = $reduc / $this->getOccupationQty(true);
				$this->price += $reduc;
				if ($pricerule->visible) {
					if ($pricerule->type==pricesrules_type::HISTORY)
						$appliedDiscounts[] = $pricerule->name.':'.$reduc*$this->quantity;
					else $appliedDiscounts[] = $pricerule->name.':'.$reduc;
					$appliedDiscountIds[] = $pricerule->sqlId;
				}
			}
		
		//when a length is quantity is saved id>0 to 1 so we need to set price with real qty
		if ($this->sqlId && $this->_mainProduct->_reservationLengthQuantity)
			$this->price = $this->price * ($this->getOccupationQty(true));
			
		if (!$this->sqlId && $this->_mainProduct->_reservationLengthQuantity)
			$this->price = $this->price * ($this->quantity);
			
		$ignoreQty = false;
		if ($this->_mainProduct->_qtyType == reservation_qty::ATTENDEES)
			$ignoreQty = myOwnExtensions::getConfig($this->_mainProduct->_qtyType, $this->_mainProduct->sqlId, guests::CONFIG_QTY, 0);
	
		if ($ignoreQty) $this->priceQuantity = 1;
		else $this->priceQuantity = $this->quantity;
			
		$this->discounts = implode(';', $appliedDiscounts);
		$this->discountIds = implode(';', $appliedDiscountIds);
	}
	
	function getFullKey() {
		return $this->startDate.'_'.$this->startTimeslot.'@'.$this->endDate.'_'.$this->endTimeslot.($this->id_object ? '@'.(int)$this->id_object : '');
	}
	
	function getCss($pricerules) {
		$class='';
		foreach(explode(';', $this->discountIds) as $discountId)
			if (intval($discountId)>0 && $pricerules[$discountId]->style!='')
				$class.='priceRule'.$discountId.' ';
		return $class;
	}
	
	//-----------------------------------------------------------------------------------------------------------------
	//GETERS
	//-----------------------------------------------------------------------------------------------------------------
	function getStartHour($obj=null) {
		$strTime="00:00:00";
		if ($this->_mainProduct!=null && array_key_exists($this->startTimeslot, $this->_mainProduct->_timeslotsObj->list))
			$strTime=$this->_mainProduct->_timeslotsObj->list[$this->startTimeslot]->startTime;
		else if ($this->startTimeslot && $obj!=null && array_key_exists($this->startTimeslot, $obj->_timeSlots->list))
			$strTime=$obj->_timeSlots->list[$this->startTimeslot]->startTime;
		return $strTime;
	}
	function getStartEndHour($obj=null) {
		$strTime="00:00:00";
		if ($this->_mainProduct!=null && array_key_exists($this->startTimeslot, $this->_mainProduct->_timeslotsObj->list))
			$strTime=$this->_mainProduct->_timeslotsObj->list[$this->startTimeslot]->endTime;
		else if ($this->startTimeslot && $obj!=null && array_key_exists($this->startTimeslot, $obj->_timeSlots->list))
			$strTime=$obj->_timeSlots->list[$this->startTimeslot]->endTime;
		return $strTime;
	}
	function getStartTimeSlot() {
		if ($this->_mainProduct!=null && array_key_exists($this->startTimeslot, $this->_mainProduct->_timeslotsObj->list))
			return $this->_mainProduct->_timeslotsObj->list[$this->startTimeslot];
		return null;
	}
	
	function getEndHour($obj=null) {
		$strTime="00:00:00";
		if ($this->_mainProduct!=null && array_key_exists($this->endTimeslot, $this->_mainProduct->_timeslotsObj->list))
			$strTime=$this->_mainProduct->_timeslotsObj->list[$this->endTimeslot]->endTime;
		else if ($this->endTimeslot && $obj!=null && array_key_exists($this->endTimeslot, $obj->_timeSlots->list))
			$strTime=$obj->_timeSlots->list[$this->endTimeslot]->endTime;
		return $strTime;
	}
	
	function getEndTimeSlot() {
		if ($this->_mainProduct!=null && array_key_exists($this->endTimeslot, $this->_mainProduct->_timeslotsObj->list))
			return $this->_mainProduct->_timeslotsObj->list[$this->endTimeslot];

		return null;
	}
	
	function getSelectValue() {
		//warning need both start and end even with one selection because of week and month
		if ($this->startTimeslot == $this->endTimeslot && $this->startDate == $this->endDate)
			return $this->startDate.'_'.$this->startTimeslot;
		else 
			return $this->startDate.'_'.$this->startTimeslot.'@'.$this->endDate.'_'.$this->endTimeslot;
	}
	
	function getStartDateTime($startDate=-1, $startTimeslot=-1) {
		$strTime="00:00:00";
		if ($startTimeslot==-1) $startTimeslot = $this->startTimeslot;
		if ($startDate==-1) $startDate = $this->startDate;
		if ($startDate=='0000-00-00') $startDate = $this->endDate;
		
		if ($this->_mainProduct!=null && array_key_exists($startTimeslot, $this->_mainProduct->_timeslotsObj->list)) 
			$strTime=$this->_mainProduct->_timeslotsObj->list[$startTimeslot]->startTime;
		return strtotime($startDate.' '.$strTime);
	}
	
	//yes were getting the start of end timeslot for cases of double reservation without length
	function getEndStartDateTime() {
		$strTime="00:00:00";
		
		if ($this->_mainProduct!=null && array_key_exists($this->endTimeslot, $this->_mainProduct->_timeslotsObj->list)) 
			$strTime=$this->_mainProduct->_timeslotsObj->list[$this->endTimeslot]->startTime;
		return strtotime($this->startDate.' '.$strTime);
	}
	
	function getEndDateTime($display=false, $endDate=-1, $endTimeslot=-1) {
		$strTime="00:00:00";
		$endTime="00:00:00";
		if ($endTimeslot==-1) $endTimeslot = $this->endTimeslot;
		if ($endDate==-1) $endDate = $this->endDate;
		
		if (MYOWNRES_WEEK_TS && $this->_mainProduct!=null && $this->_mainProduct->_reservationUnit==reservation_unit::WEEK)
			$endTimeslot = $this->startTimeslot;
		
		if ($this->_mainProduct!=null && array_key_exists($endTimeslot, $this->_mainProduct->_timeslotsObj->list)) 
			$endTime=$this->_mainProduct->_timeslotsObj->list[$endTimeslot]->endTime;
		
		//check timeslot end before start
		if ($this->_mainProduct!=null && $this->startTimeslot==$endTimeslot && !$this->_mainProduct->isTimeSelect()) {
			if ($this->_mainProduct!=null && array_key_exists($this->startTimeslot, $this->_mainProduct->_timeslotsObj->list)) 
				$strTime=$this->_mainProduct->_timeslotsObj->list[$this->startTimeslot]->startTime;
			//if ($endTime<=$strTime) return strtotime("+1 day",strtotime($endDate.' '.$endTime));
		}
		//7_july removed  && $display to condition because was only occupy one day instead of 2 when start time = end time =00:00 with day sel
		if ($endTime==$strTime && $strTime=="00:00:00") { // && $this->startDate == $endDate 
			//display on calendar a resa with smae start day end end day
				if ($this->_mainProduct!=null && $this->_mainProduct->reservationSelType==reservation_interval::DAY)
					return strtotime("+1 day",strtotime($endDate.' '.$endTime));
		}
		return strtotime($endDate.' '.$endTime);
			
		//what is this fucking condition for ??
		/*if ($this->_mainProduct!=null && $endTime=="00:00:00" && $this->_mainProduct->reservationSelType!=reservation_interval::WEEK) return strtotime("+1 day",strtotime($this->endDate.' '.$endTime));
		else if ($this->_mainProduct==null && $this->endDate==$this->startDate) return strtotime("+1 day",strtotime($this->endDate.' '.$endTime));
		else return strtotime($this->endDate.' '.$endTime);*/
	}
	
		
	public function done() {
		if ($this->getEndDateTime()<time()) return true;
		return false;
	}
	
	//to get exact end date time because of fucking condition in previous method
	function getEndDateTimeRaw() {
		$strTime="00:00:00";
		if ($this->_mainProduct!=null && $this->endTimeslot != 0 && array_key_exists($this->endTimeslot, $this->_mainProduct->_timeslotsObj->list)) 
			$strTime=$this->_mainProduct->_timeslotsObj->list[$this->endTimeslot]->endTime;
			
		//what is this fucking confition for ??
		if ($this->_mainProduct==null && $this->endDate==$this->startDate) return strtotime("+1 day",strtotime($this->endDate.' '.$strTime));
		else return strtotime($this->endDate.' '.$strTime);
	}
	
	function getLength($forPrice=false, $availabilities=null) {
		$resStartDate = strtotime($this->startDate);
		$resEndDate = strtotime($this->endDate);
		if ($this->startTimeslot === '') return 1;
		if ($this->startDate == '') return 1;
		if ($this->startDate == '0000-00-00') return 1;

		if ($this->_mainProduct==null) return $this->countDays($forPrice, $availabilities);

		if ($forPrice) $lengthUnit = $this->_mainProduct->_priceUnit;
		else $lengthUnit = $this->_mainProduct->_reservationUnit;
		
		$length = $this->countDays($forPrice, $availabilities);

		if ($lengthUnit==reservation_unit::TIMESLOT && !($this->_mainProduct->_timeslotsObj->isProductAssigned() && !$this->id_product))
			$length = $this->_mainProduct->countTimeslots($resStartDate, $resEndDate, $this->startTimeslot, $this->endTimeslot, $forPrice, 0, 0, -1, array(), ($this->_mainProduct->_timeslotsObj->isProductAssigned() ? $this->id_product : 0) );
			
		if ($lengthUnit==reservation_unit::WEEK)
			$length = $this->countWeeks($forPrice);

		if ($lengthUnit==reservation_unit::MONTH)
			$length = $this->CountMonths($forPrice);

		if ($this->_mainProduct->_reservationSelExt!=null) $length+=$this->_mainProduct->_reservationSelExt->length;
// 		echo '%'.$this->endDate.' '.$length;
		return $length;
	}
	

	function getPrice() {
		return $this->getPriceTaxes((Product::getTaxCalculationMethod() != PS_TAX_EXC));
	}
	
	function getOldPrice() {
		return $this->getOldUnitPriceWithReduc((Product::getTaxCalculationMethod() != PS_TAX_EXC));
	}
	
	function getUnitPrice() {
		return $this->getUnitPriceWithReduc((Product::getTaxCalculationMethod() != PS_TAX_EXC));
	}
	
	function getUnitHighPrice() {
		return $this->getUnitHighPriceTaxes((Product::getTaxCalculationMethod() != PS_TAX_EXC));
	}
	
	function getHighPrice() {
		return $this->getHighPriceTaxes((Product::getTaxCalculationMethod() != PS_TAX_EXC));
	}
	
	function getPriceTaxes($taxes=false) {
		$qty = ($this->_mainProduct->_reservationLengthQuantity ? 1 : $this->priceQuantity);
		
		if ($taxes) return Tools::ps_round($this->price* (1+($this->tax_rate/100))* $qty,_PS_PRICE_DISPLAY_PRECISION_);
		return Tools::ps_round($this->price* $qty,_PS_PRICE_DISPLAY_PRECISION_);
	}
	
	function getUnitHighPriceTaxes($taxes=false) {
		if ($taxes) return Tools::ps_round($this->_highPrice* (1+($this->tax_rate/100)),_PS_PRICE_DISPLAY_PRECISION_);
		return Tools::ps_round($this->_highPrice,_PS_PRICE_DISPLAY_PRECISION_);
	}
	
	function getHighPriceTaxes($taxes=false) {
		$qty = ($this->_mainProduct->_reservationLengthQuantity ? 1 : $this->priceQuantity);
		
		if ($taxes) return Tools::ps_round($this->_highPrice* (1+($this->tax_rate/100))* $qty,_PS_PRICE_DISPLAY_PRECISION_);
		return Tools::ps_round($this->_highPrice* $qty,_PS_PRICE_DISPLAY_PRECISION_);
	}
	
	function getUnitPriceWithReduc($taxes=false) {
		if ($taxes) return Tools::ps_round($this->price* (1+($this->tax_rate/100)),_PS_PRICE_DISPLAY_PRECISION_);
		return Tools::ps_round($this->price,_PS_PRICE_DISPLAY_PRECISION_);
	}
	function getOldUnitPriceWithReduc($taxes=false) {
		if ($taxes) return Tools::ps_round($this->oldPrice* (1+($this->tax_rate/100)),_PS_PRICE_DISPLAY_PRECISION_);
		return Tools::ps_round($this->oldPrice,_PS_PRICE_DISPLAY_PRECISION_);
	}	
	protected function getUnavailableQuantity($cart=null, $availabilities = null, $reservations=null) {
		
		if ($reservations==null)
			return myOwnResas::getFlatResaLinesCount($cart, $availabilities, $this->_mainProduct, $this->id_product, $this->id_product_attribute, strtotime($this->startDate), strtotime($this->endDate), $this->startTimeslot, $this->endTimeslot, $this->id_object);
		else {
			$lines = $reservations->getResaLines($cart, $availabilities, $this->_mainProduct, $this->id_product, $this->id_product_attribute, strtotime($this->startDate), strtotime($this->endDate), $this->startTimeslot, $this->endTimeslot);
			if ($lines!=-1) return count($lines);
			else return $lines;
		}
	}
	
	public function getAvailableQuantity($cart=null, $availabilities = null, $stocks=null) {
		$productQty = 0;
		if ($this->_mainProduct!=null) $productQty = $this->_mainProduct->getProductQuantity($this->id_product, $this->id_product_attribute, $stocks, $availabilities, $this->id_object);
		if ($productQty<0) return $productQty;
		$unavailableQty = $this->getUnavailableQuantity($cart, $availabilities);

		//need to return 0 if available qty<0 and not negative because negative is ok 
		if (($productQty-$unavailableQty)<0) return 0;
		
		return $productQty-$unavailableQty;
	}


    public function getAvailableQuantityToStringMod($obj) {
        $countavailabilitiestoedit=0;
        $editmenu='';
        if ($this->_mainProduct == null) return '';
        $availabilities = new myOwnAvailabilities($this->getStartDateTime(), $this->getEndDateTime(), $this->_mainProduct, intval($this->id_product), intval($this->id_product_attribute), false, false);
        $editfoot='';
        $resources = array();
        $resources_used = array();
        if ($this->_mainProduct->productType == product_type::RESOURCE && $this->_mainProduct->_occupyObj!=null)
            $resources = $this->_mainProduct->_occupyObj->getFamilyOptions($this->_mainProduct);
        $availabilitiestoedit = array();
        $edit = '';
        foreach ($availabilities->list as $availability)
            if ($this->id_object==0 or $availability->id_object==0 or $availability->id_object==$this->id_object)
                if ($this->id_product==0 or $availability->id_product==0 or $this->id_product==$availability->id_product or ($resources!=array() && $availability->id_product<0 && in_array(-$availability->id_product, $resources) )) {
                    $availabilitiestoedit[] = $availability;
                    $resources_used[]=-$availability->id_product;
                }
        $countavailabilitiestoedit++;



        if ($this->_mainProduct!=null && $this->id_product)
            $productQty = $this->_mainProduct->getProductQuantity($this->id_product, $this->id_product_attribute, $obj->_stocks, $availabilities, $this->id_object);

        if ($this->id_product) $unavailableQty = $this->getUnavailableQuantity();

        if ($unavailableQty<0) return '<div style="border:1px solid #cccccc;border-radius:5px;padding:5px;font-weight:bold;color:#B80000;background-image:url(\''.__PS_BASE_URI__.'modules/myownreservations/img/holiday.gif\');">'.$edithead.$obj->l('Product unavailable on this period', 'cart').$edit.'</div>'.$editfoot;

        $leftQty = $productQty-$unavailableQty;
        if ($productQty>0)
            $occPercent = round($leftQty/$productQty*100);
        else $occPercent = 100;


                 if ($productQty==0) return 0;
        else if ($productQty!=-1) { if ( ($edithead.($leftQty)) == 0 ) return 0;  return 1; }
        else return 1;



        if ($productQty==0) return '<div id="displayAvailability" style="border:1px solid #cccccc;border-radius:5px;padding:5px;background-color:'.MyOwnReservationsUtils::getOccupationColor(100).'" >1'.$edithead.$obj->l('Not available on this period', 'cart').($edithead!='' ? '</a>' :'').$edit.'</div>'.$editfoot;
        else if ($productQty!=-1) return '<div style="border:1px solid #cccccc;border-radius:5px;padding:5px;background-color:'.MyOwnReservationsUtils::getOccupationColor(100-$occPercent).'" >2'.$edithead.($leftQty).'/'.$productQty.' '.$obj->l('available on this period', 'cart').($edithead!='' ? '</a>' :'').$edit.'</div>'.$editfoot;
        else return '<div style="border-radius:5px;padding:5px;background-color:'.MyOwnReservationsUtils::getOccupationColor(0).'" >3'.$edithead.$obj->l('Available on this period', 'cart').($edithead!='' ? '</a>' :'').$edit.'</div>'.$editfoot;
    }





    public function getAvailableQuantityToString($obj) {


	    //die($this->getStartDateTime().'-'.$this->getEndDateTime().'-'.$this->_mainProduct.'-'.intval($this->id_product).'-'.intval($this->id_product_attribute));

         // die('--'.$this->getStartDateTime().'-'.$this->getEndDateTime().'-'.var_dump($this->_mainProduct).'-'.intval($this->id_product).'-'.intval($this->id_product_attribute));

        $countavailabilitiestoedit=0;
		$editmenu='';
		if ($this->_mainProduct == null) return '';
		$availabilities = new myOwnAvailabilities($this->getStartDateTime(), $this->getEndDateTime(), $this->_mainProduct, intval($this->id_product), intval($this->id_product_attribute), false, false);
		$editfoot='';
		$resources = array();
		$resources_used = array();
		if ($this->_mainProduct->productType == product_type::RESOURCE && $this->_mainProduct->_occupyObj!=null)
			$resources = $this->_mainProduct->_occupyObj->getFamilyOptions($this->_mainProduct);
		$availabilitiestoedit = array();
		$edit = '';
		foreach ($availabilities->list as $availability)
			if ($this->id_object==0 or $availability->id_object==0 or $availability->id_object==$this->id_object)
				if ($this->id_product==0 or $availability->id_product==0 or $this->id_product==$availability->id_product or ($resources!=array() && $availability->id_product<0 && in_array(-$availability->id_product, $resources) )) {
				$availabilitiestoedit[] = $availability;
				$resources_used[]=-$availability->id_product;
		}
		$countavailabilitiestoedit++;
		$editfoot.='
			<div id="editAvailability" style="display:none;margin: 0px -10px; padding-left: 5px;" class="well bootstrap">';
		foreach ($availabilitiestoedit as $availability)
			if (MYOWNRES_WEEK_TS && ($availability->startTimeslot==$this->startTimeslot or $availability->startTimeslot==0)) {
			
			//$availabilitiestoedit[$availability->sqlId]=$availability;
			/*$edit .= '<span style="float:right" class="badge badge-'.($availability->type ? 'success' : 'error').'"><i class="icon-'.($availability->id_product >= 0 ? ($availability->type ? 'check-circle' : 'times-circle') : $this->_mainProduct->_occupyObj->icon).'"></i> '.(!$availability->type ? '#'.$availability->sqlId.':' : '').($availability->id_product >= 0 ? ''.$availability->quantity : ($this->_mainProduct->_occupyObj!=null ? ' '.substr($this->_mainProduct->_occupyObj->objects->list[-$availability->id_product]->name, 0, 3) : '')).'</span>';*/
			if ($availability->id_product >= 0) {
				$countavailabilitiestoedit++;
				$editfoot.='
				
				<span style="left: -10px;position: relative;"><i class="icon-'.($availability->type ? 'check-circle' : 'times-circle').'"></i> '.'#'.$availability->sqlId.'</span>
				<input type="text" size="2" id="availability_'.$availability->sqlId.'" name="availability_'.$availability->sqlId.'" value="'.$availability->quantity.'" style="height:27px;width:35px;display: inline-block;">
				<a class="edit btn btn-default" name="" onclick="$(\'#editAvailability\').hide();$(this).parent().prev().show();" value="" style="float:right;margin-left:5px" class="button btn btn-default">
					<i class="icon-close"></i> ' . ucfirst(myOwnLang::$actions[MYOWN_OPE::CANCEL]) . '
				</a>';
				$editmenu='
				<div class="btn-group pull-right baseActions" id="baseActions1" style="float:right" >
					<a onclick="myOwnAvailabilityUpdate('.$availability->sqlId.', $(\'#availability_'.$availability->sqlId.'\').val())" title="' . ucfirst(myOwnLang::$actions[MYOWN_OPE::SAVE]) . '" class="edit btn btn-default">
						<i class="icon-save"></i> ' . ucfirst(myOwnLang::$actions[MYOWN_OPE::SAVE]) . '
					</a>
					<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
						<i class="icon-caret-down"></i>&nbsp;
					</button>
					<ul class="dropdown-menu">
						<li>
							<a onclick="if (confirm(\'' . ucfirst(myOwnLang::$actions[MYOWN_OPE::DELETE]) . ' ?\')){ return myOwnAvailabilityDelete('.$availability->sqlId.'); }else{ event.stopPropagation(); event.preventDefault();};" title="' . ucfirst(myOwnLang::$actions[MYOWN_OPE::DELETE]) . '" class="delete">
								<i class="icon-trash"></i> ' . ucfirst(myOwnLang::$actions[MYOWN_OPE::DELETE]) . '
							</a>
						</li>
					</ul>
				</div>';
			} else {
				$editfoot.='<a class="edit btn btn-default" name="" onclick="if (confirm(\'' . ucfirst(myOwnLang::$actions[MYOWN_OPE::DELETE]) . ' ?\')){ return myOwnAvailabilityDelete('.$availability->sqlId.'); }else{ event.stopPropagation(); event.preventDefault();};" value="" style="float:left;margin-left:5px">'.($this->_mainProduct->_occupyObj!=null ? ' '.substr($this->_mainProduct->_occupyObj->objects->list[-$availability->id_product]->name, 0, 3) : '').' <i class="icon-trash"></i></a>';
				$editmenu='
				<a class="edit btn btn-default" name="" onclick="$(\'#editAvailability\').hide();$(this).parent().prev().show();" value="" style="float:right;margin-left:5px" class="button btn btn-default">
					<i class="icon-close"></i> ' . ucfirst(myOwnLang::$actions[MYOWN_OPE::CANCEL]) . '
				</a>
				<div class="btn-group pull-right baseActions" id="baseActions1" style="float:right" >
					<a onclick="" title="' . ucfirst(myOwnLang::$actions[MYOWN_OPE::ADD]) . '" class="edit btn btn-default">
						<i class="icon-plus-square"></i> ' . ucfirst(myOwnLang::$actions[MYOWN_OPE::ADD]) . '
					</a>
					<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
						<i class="icon-caret-down"></i>&nbsp;
					</button>
					<ul class="dropdown-menu">';
					foreach ($resources as $key => $resource_id) {
						$resource = $this->_mainProduct->_occupyObj->objects->list[$resource_id];
						if (!in_array($resource_id, $resources_used)) $editmenu.='
						<li>
							<a onclick="myOwnAvailabilityCreate(-'.$resource->sqlId.', 0, '.(int)$this->_mainProduct->sqlId.', \''.Tools::getValue('period').'\', '.(int)Tools::getValue('id_object').', 0)" title="' . $resource->name . '">
								<i class="icon-'.$this->_mainProduct->_occupyObj->icon.'"></i> ' . $resource->name . '
							</a>
						</li>';
					}
					$editmenu.='
					</ul>
				</div>
				<div style="clear:both"></div>';
			}
		}
		$editfoot.=$editmenu.'
			</div>';
		if ($countavailabilitiestoedit==0) $editfoot='<div id="editAvailability" style="display:none;margin: 0px -10px; padding-left: 5px;" class="well bootstrap">
					<label style="width: 35%;float:left;padding-top: 7px;font-weight:bold;">'.ucfirst(myOwnLang::$actions[MYOWN_OPE::FNEW]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::AVAILABILITY]).'</label>
					<input type="text" size="2" id="new_availability_qty" name="new_availability_qty" value="0" style="height:27px;width:30px;display: inline-block;">
					<a class="edit btn btn-default" name="" onclick="$(\'#editAvailability\').hide();$(this).parent().prev().show();" value="" style="float:right;margin-left:5px" class="button btn btn-default">
						<i class="icon-close"></i> ' . ucfirst(myOwnLang::$actions[MYOWN_OPE::CANCEL]) . '
					</a>
					<a class="edit btn btn-default" name="" onclick="myOwnAvailabilityCreate('.(int)Tools::getValue('id_product', $this->id_product).', '.(int)Tools::getValue('id_product_attribute', $this->id_product_attribute).', '.(int)$this->_mainProduct->sqlId.', \''.Tools::getValue('period', $this->getFullKey()).'\', '.(int)Tools::getValue('id_object', $this->id_object).', $(\'#new_availability_qty\').val())" value="" style="float:right" class="button btn btn-default"><i class="icon-save"></i> ' . ucfirst(myOwnLang::$actions[MYOWN_OPE::CREATE]) . '</a>
				</div>';
		$edithead='';
		if (count($availabilitiestoedit)) {
			$edithead='<a onclick="$(\'#editAvailability\').show();$(this).parent().hide();" style="display:block;cursor:pointer;width: 100%;height: 100%;top: 0;left: 0;color:rgb(102, 102, 102);"><i class="icon-pencil" style="float:left"></i>';
		} else {
			$edithead='<a onclick="$(\'#editAvailability\').show();$(this).parent().hide();" style="display:block;cursor:pointer;width: 100%;height: 100%;top: 0;left: 0;color:rgb(102, 102, 102);"><i class="icon-plus-circle" style="float:left"></i>';
		}
	
		if ($this->_mainProduct!=null && $this->id_product) $productQty = $this->_mainProduct->getProductQuantity($this->id_product, $this->id_product_attribute, $obj->_stocks, $availabilities, $this->id_object);

		if ($this->id_product) $unavailableQty = $this->getUnavailableQuantity();
		if ($unavailableQty<0) return '<div style="border:1px solid #cccccc;border-radius:5px;padding:5px;font-weight:bold;color:#B80000;background-image:url(\''.__PS_BASE_URI__.'modules/myownreservations/img/holiday.gif\');">'.$edithead.$obj->l('Product unavailable on this period', 'cart').$edit.'</div>'.$editfoot;
		
		$leftQty = $productQty-$unavailableQty;
		if ($productQty>0)
			$occPercent = round($leftQty/$productQty*100);
		else $occPercent = 100;
		if ($productQty==0) return '<div id="displayAvailability" style="border:1px solid #cccccc;border-radius:5px;padding:5px;background-color:'.MyOwnReservationsUtils::getOccupationColor(100).'" >'.$edithead.$obj->l('Not available on this period', 'cart').($edithead!='' ? '</a>' :'').$edit.'</div>'.$editfoot;
		else if ($productQty!=-1) return '<div style="border:1px solid #cccccc;border-radius:5px;padding:5px;background-color:'.MyOwnReservationsUtils::getOccupationColor(100-$occPercent).'" >'.$edithead.($leftQty).'/'.$productQty.' '.$obj->l('available on this period', 'cart').($edithead!='' ? '</a>' :'').$edit.'</div>'.$editfoot;
		else return '<div style="border-radius:5px;padding:5px;background-color:'.MyOwnReservationsUtils::getOccupationColor(0).'" >'.$edithead.$obj->l('Available on this period', 'cart').($edithead!='' ? '</a>' :'').$edit.'</div>'.$editfoot;
	}
	
	public function isAvailable($cart=null, $availabilities = null, $stocks=null, $updating=false) {
		//Cart is null when checking reservation after added  it to cart
		//$availableQty = $this->getAvailableQuantity($cart, $availabilities, $stocks);
		//if ($availableQty<0) return true;
//syslog(LOG_WARNING, "availablities?".intval($availabilities != null));
		//check product qty (<0 means unlimited products)
				//need to get avaialbilities on reservation 
		if ($availabilities == null && $this->_mainProduct!=null && $this->_mainProduct->productType==product_type::AVAILABLE) {
			$availabilities = new myOwnAvailabilities($this->getStartDateTime(), $this->getEndDateTime(), $this->_mainProduct, (int)$this->id_product, (int)$this->id_product_attribute);	
		}
		if ( $this->_mainProduct==null) return reservation_error::PRODUCT_ISNT_RENTED;
		$productQty = $this->_mainProduct->getProductQuantity($this->id_product, $this->id_product_attribute, $stocks, $availabilities, $this->id_object);
//syslog(LOG_WARNING, "isAvailable productQty:".$productQty);	
		if ($productQty<0) return true;

		//check reservations (<0 means holiday problem)
		$unavailableQty = $this->getUnavailableQuantity($cart, $availabilities);

//syslog(LOG_WARNING, "getUnavailableQuantity unavailableQty:".$unavailableQty);	
		if ($unavailableQty<0) return $unavailableQty;
//syslog(LOG_WARNING, "-unavailableQty:$unavailableQty");	

		$availableQty = $productQty-$unavailableQty;
		
		//if updating that's mean resa is in the cart with updated qty so we just ahve to check that there is left qties
		if ($updating && $availableQty>=0) return true;
//syslog(LOG_WARNING, "test:".intval(strtolower(get_class($this))!='myownreservation' or $this->validated));
		//if resa saved in cart or reservation validated qty has already been counted so we just check is there is qty left
		if ($this->sqlId>0 && !$updating && $this->_mainProduct->productType!=product_type::AVAILABLE) {
			if (strtolower(get_class($this))!='myownreservation' or $this->validated) {
//syslog(LOG_WARNING, "availableQty>0 ?".intval(($availableQty>=0)));
				if ($availableQty>=0) return true;
				else return reservation_error::ALL_PRODUCTS_BOOOKED;
			}
		}
//syslog(LOG_WARNING, "availableQty:$availableQty");
//syslog(LOG_WARNING, "qty:".$this->quantity);
		$resaOccupation = $this->getOccupationQty();
//syslog(LOG_WARNING, "resaOccupation:$resaOccupation");

		if ($resaOccupation <= $availableQty) return true;
		else return reservation_error::NOT_ENOUGH_AVAILABLE;
	}
	
	public function getOccupationQty($forPrice=false) {
		global $cookie;
		$resaOccupation = 1;
		$type=$this->_mainProduct->productType;
		$qty=$this->quantity;
		
		switch ($this->_mainProduct->productType) {
   			case product_type::VIRTUAL: 
       			if ($this->_mainProduct->_occupyCombinations != occupy_combinations::IGNORE)
       				$resaOccupation = $qty;
  				break;
			case product_type::SHARED:
				$qtyCapacity=$this->_mainProduct->_qtyCapacity;
				if ($qtyCapacity>0)
					$resaOccupation = ceil($this->quantity/$qtyCapacity);
				break;
			case product_type::PRODUCT:
	   		case product_type::MUTUEL:	
			case product_type::AVAILABLE: 
			default:
				$resaOccupation = $qty;
   		}
   		if ($this->_mainProduct->_qtyType == reservation_qty::SEARCH)
			$resaOccupation = $resaOccupation * $this->_mainProduct->searchProductQtyInAttribute($this->id_product, $this->id_product_attribute, $cookie->id_lang);
		if ($this->_mainProduct->_qtyType == reservation_qty::ATTENDEES) {
			$ignoreQty = myOwnExtensions::getConfig($this->_mainProduct->_qtyType, $this->_mainProduct->sqlId, guests::CONFIG_QTY, 0);
			if ($ignoreQty) return 1;
			else return $resaOccupation;
		}
			
		if ($this->_mainProduct->_reservationLengthQuantity && !$forPrice)
			return 1;
		else return $resaOccupation ;
	}
	
	public function isValidLength($availabilities = null) {
		$length = $this->getLength(false, $availabilities);
		$freq = $this->_mainProduct->_reservationLengthFreq;
		if ($this->_mainProduct->_reservationLengthControl == reservation_length::LIMIT
			|| $this->_mainProduct->_reservationLengthControl == reservation_length::FIXED
			|| $this->_mainProduct->_reservationLengthControl == reservation_length::PRODUCT) {
			if ($length < $this->_mainProduct->_reservationMinLength)  return false;
			if ($this->_mainProduct->_reservationMaxLength>0 && !$this->_mainProduct->_reservationSelMultiple && !$this->_mainProduct->_reservationLengthQuantity && $length > $this->_mainProduct->_reservationMaxLength) return false;
		}
		if ($freq>1 && $length%$freq!=0) return false;
		return true;
	}
	
	function statusToString($obj, $span=false) {
		return ($span ? '<span class="badge badge-success">' : '').($this->validated ? $obj->l('Validated', 'cart') : $obj->l('Validation pending', 'cart') ).($span ? '</span>' : '');
	}
	
	function toString($obj, $langue, $small) {
		//one ts
		if ($this->_mainProduct!= null)
			$_timeSlots = $this->_mainProduct->_timeslotsObj;
		else $_timeSlots = $obj->_timeSlots;
		$reScheduleEnd = '';
		$reschedulesel = false;
		if (Tools::getIsset('submitReScheduleEnd')) {
			if ($this->quantity==1) $reschedulesel = Tools::getIsset('resa_'.$this->sqlId, '');
			else 
				for ($i=1; $i<($this->quantity+1); $i++)
					if (Tools::getIsset('resa_'.$this->sqlId.'_'.$i, ''))
						$reschedulesel = true;
			
			if ($reschedulesel) {
				$reScheduleEnd = Tools::getValue('reScheduleEnd', '');
				$reScheduleEndTimeslot = Tools::getValue('reScheduleEndTimeslot', 0);
			}
		}
		
		
		if ($this->_mainProduct!= null && $this->_mainProduct->_reservationSelExt!=null) return $this->_mainProduct->_reservationSelExt->toString($obj, $this, $langue, $small);

		$resaStr='';
		if ($reScheduleEnd=='' && (
			($this->_mainProduct!=null && ($this->_mainProduct->_reservationLengthControl == reservation_length::NONE))
			or ($this->startDate==$this->endDate && $this->startTimeslot==$this->endTimeslot)
			)) {
			$resaStr .= $obj->l('On(date)', 'cart').' '.MyOwnCalendarTool::formatDateCompact(strtotime($this->startDate), $langue);
			if ($this->getStartTimeSlot()!=null)
				if ($this->_mainProduct->_reservationShowTime) 
					$resaStr .= ' '.MyOwnCalendarTool::formatTime($this->getStartHour(), $langue).($this->_mainProduct->_reservationLengthControl != reservation_length::NONE ? ' '.$obj->l('to(time)', 'cart').' '.MyOwnCalendarTool::formatTime($this->getEndHour(), $langue) : '');
				else if ($this->_mainProduct->reservationSelType==reservation_interval::TIMESLOT)
					$resaStr .= ' '.$this->getStartTimeSlot()->name;
			
			//same day different ts
		} else if ($reScheduleEnd=='' && $this->startDate==$this->endDate && $this->_mainProduct->_reservationLengthControl != reservation_length::OPTIONAL) {
			$resaStr .= $obj->l('On(date)', 'cart').' '.MyOwnCalendarTool::formatDate(strtotime($this->startDate), $langue);
			//if ($this->startTimeslot != 0 && $this->getStartHour()!=$this->getEndHour()) 
				$resaStr .= ', '.$obj->l('from(time)', 'cart').' '.MyOwnCalendarTool::formatTime($this->getStartHour(), $langue).' '.$obj->l('to(time)', 'cart').' '.MyOwnCalendarTool::formatTime($this->getEndHour(), $langue);
			
		//different days
		} else {
			$displayMonth=true;$displayYear=true;
			if ($this->_mainProduct!=null && !$this->_mainProduct->_reservationShowTime) {
				$displayMonth=(date('Y-m', strtotime($this->startDate)) != date('Y-m', strtotime($this->endDate)));
				$displayYear=(date('Y', strtotime($this->startDate)) != date('Y', strtotime($this->endDate)));
			}
			if ($this->startDate!='0000-00-00') {
				if ($this->_mainProduct==null || $this->_mainProduct->_reservationLengthControl != reservation_length::OPTIONAL)
					$resaStr .= $obj->l('From(date)', 'cart').' ';
				else $resaStr .= $this->_mainProduct->getStartLabel($langue).' ';
				$resaStr .= MyOwnCalendarTool::formatDate(strtotime($this->startDate), $langue, $displayMonth, $displayYear).' ';
				if ($this->_mainProduct!=null && $this->startTimeslot != 0 && $this->_mainProduct->_reservationShowTime && !MYOWNRES_WEEK_TS) 
					$resaStr .= MyOwnCalendarTool::formatTime($this->getStartHour(), $langue).($small ? '<br>' : ', ');
			}
			if ($this->_mainProduct==null || $this->_mainProduct->_reservationLengthControl != reservation_length::OPTIONAL)
				$resaStr .= $obj->l('To(date)', 'cart').' ';
			else $resaStr .= $this->_mainProduct->getStopLabel($langue).' ';
			if ($reScheduleEnd!='' && $reScheduleEnd!=$this->endDate) $resaStr .= '<s>';
			$resaStr .= MyOwnCalendarTool::formatDate(strtotime($this->endDate), $langue).' ';
			if ($this->_mainProduct!=null && $this->endTimeslot && $this->_mainProduct->_reservationShowTime) 
				$resaStr .= MyOwnCalendarTool::formatTime($this->getEndHour(), $langue);
			if (MYOWNRES_WEEK_TS) {
				$strTime="00:00:00";
				if ($this->_mainProduct!=null && array_key_exists($this->startTimeslot, $this->_mainProduct->_timeslotsObj->list))
					$strTime=$this->_mainProduct->_timeslotsObj->list[$this->startTimeslot]->startTime;
				$endTime="00:00:00";
				if ($this->_mainProduct!=null && array_key_exists($this->startTimeslot, $this->_mainProduct->_timeslotsObj->list))
					$endTime=$this->_mainProduct->_timeslotsObj->list[$this->startTimeslot]->endTime;
				$resaStr .= ', '.$obj->l('from(time)', 'cart').' '.MyOwnCalendarTool::formatTime($strTime, $langue).' '.$obj->l('to(time)', 'cart').' '.MyOwnCalendarTool::formatTime($endTime, $langue);
			}
			if ($reScheduleEnd!='' && $reScheduleEnd!=$this->endDate) {
				$resaStr .= '</s>';
				$resaStr .= MyOwnCalendarTool::formatDate(strtotime($reScheduleEnd), $langue).' ';
				if ($reScheduleEndTimeslot && $this->_mainProduct->_reservationShowTime) {
					$strTime="00:00:00";
					if ($this->_mainProduct!=null && array_key_exists($reScheduleEndTimeslot, $this->_mainProduct->_timeslotsObj->list))
						$strTime=$this->_mainProduct->_timeslotsObj->list[$reScheduleEndTimeslot]->endTime;
					$resaStr .= MyOwnCalendarTool::formatTime($strTime, $langue);
				}
			}
		}
		return $resaStr;
	}

	function endToString($obj, $langue) {
		return $obj->l('Until(date)', 'cart').' '.MyOwnCalendarTool::formatDate(strtotime($this->endDate), $langue).' '.MyOwnCalendarTool::formatTime($obj->_timeSlots->list[$this->endTimeslot]->endTime, $langue);
	}
	
	public function getCustomArray($obj, $cookie, $resv_title, $taxes = false, $orderStatus=0) {		
		if ($orderStatus>0) {
			//warning # is used to add actions on admin orders
			if (_PS_VERSION_ < "1.7.0.0")
				$resv_title.='&nbsp;<span id="resa_'.$this->sqlId.'">#'.$this->sqlId.'</span>';
			else $resv_title.=' #'.$this->sqlId;
			if ($this->validated) {
				if (_PS_VERSION_ < "1.7.0.0")
					$resv_title.=' <span style="color:#4F8A10">('.$obj->l('Validated', 'cart').')</span>';
				else $resv_title.=' ('.$obj->l('Validated', 'cart').')';
			} else if ($this->_mainProduct!=null) {
				if (in_array((string)$orderStatus, explode(',',$this->_mainProduct->_optionValidationStatus))) {
					if (_PS_VERSION_ < "1.7.0.0")
						$resv_title.=' <span style="color:#D8000C">('.$obj->l('Validation failed', 'cart').')</span>';
					else $resv_title.=' ('.$obj->l('Validation failed', 'cart').')';
				} else $resv_title.=' ('.$obj->l('Validation pending', 'cart').')';
			} else $resv_title.=' ('.$obj->l('Validation pending', 'cart').')';
		}
		$opt_head='';
		$ctrl = Tools::getValue('controller');
		$method = Tools::getValue('method');
		if (strtolower($ctrl)=='adminorders' && !Tools::getIsset('submitState') && _PS_VERSION_ < "1.7.0.0") {
			if (Tools::getValue('reScheduleEnd', '')!='') $checked = Tools::getIsset('resa_'.$this->sqlId, ''); 
			else $checked=true;
			if ($this->quantity > 1) {
				for ($i=1; $i<($this->quantity+1); $i++) {
					$opt_head .= '<input type="checkbox" name="resa_'.$this->sqlId.'_'.$i.'" value="1" class="check-out-checkbox" '.(Tools::getValue('resa_'.$this->sqlId.'_'.$i, !Tools::getIsset("reScheduleEnd")) ? 'checked="checked' : '').'" product="'.$this->id_product.'" style="display:none"><span>#'.$i.' </span>'.($i!=$this->quantity ? '<br/>' : '').'';
				}
			} else
				$resv_title = '<input type="checkbox" name="resa_'.$this->sqlId.'" value="1" class="check-out-checkbox" '.($checked ? 'checked="checked' : '').'" product="'.$this->id_product.'" style="display:none"> '.$resv_title;
		}
		$ajax = Tools::getValue('ajax');
		if ($ctrl=='order' || $ctrl=='orderopc')
			$discount=$this->reductionsToHtml($taxes);
		else $discount=$this->reductionsToString($taxes);
		/*$ext_object = null;

		foreach ($obj->_extensions->getExtByType(extension_type::OBJECT) as $ext) 
			if (method_exists($ext, "objectSelect"))
				if ($this->_mainProduct!=null && in_array($ext->id, $this->_mainProduct->ids_options))
					$ext_object = $ext;*/

		$ext_text = '';
		if ($ctrl!='adminmyownreservations' || $method=='contract')
		foreach ($obj->_extensions->getExtByType(extension_type::CARTDETAILS) as $ext) 
			if (method_exists($ext, "displayCartDetails")) {
				$ext_content = $ext->displayCartDetails($obj, $this, -1, $ctrl);
				if ($ext_content!='') $ext_text.='<br/>'.$ext_content;
			}
		/*$resaDescription='';
		foreach ($obj->_extensions->list as $ext) {
  			if (method_exists($ext, "displayReservation"))
				$resaDescription.=$ext->displayReservation($obj, $this->reservation);
  		}*/
				
		//need to add extra qty here for ajax update
		foreach ($obj->_extensions->getExtByType(extension_type::SEL_QTY) as $ext)
			if (method_exists($ext, "displayQtyField")) {
				$ext_qty = $ext->displayQtyField($obj,$this->_mainProduct, $this);
				if (trim($ext_qty)!='') $ext_text .= '<div style="display:none" id="qty_field_'.$this->sqlId.'">'.$ext_qty.'</div>';
			}
		if ($method=='contract' || $method=='quotations') {
			$ext_content='';$ext_qty='';$ext_text='';
		}
		$prod_amount=0;	
		if ($ajax && $this->_mainProduct!=null && $this->_mainProduct->_depositByProduct)
			$prod_amount = Configuration::get('MYOWNRES_RES_DEPOSIT_'.$this->id_product, 0);
		
		$id_currency = myOwnUtils::getCurrencyObj()->id;
		$currency = new Currency($id_currency);

		return array(
			'id_customization' => -$this->sqlId, 
			'id_product' => $this->id_product, 
			'id_customization_field' => 0, 
			'id_product_attribute' => $this->id_product_attribute, 
			'type' => 1, 'index' => 1,
			'id_module'	=> $obj->id,
			'name' => $resv_title, 
			'value' => ($opt_head != '' ? '<div class="panel-checkbox" style="display:none">'.$opt_head.'</br></div>' : '').$this->toString($obj, $cookie->id_lang, true).(Configuration::get('MYOWNRES_PRICE_RAW') && !isset($this->_saved) && Tools::getValue('controller')!='AdminCarts' && Tools::getValue('controller')!='adminmyownreservations' ? ' : '.Tools::displayPrice($this->getUnitHighPrice(), $currency, false) : '').($discount!='' ? '<br/>'.(stripos(',', $discount)!==false ? $obj->l('Discounts', 'cart') : $obj->l('Discount', 'cart')).' : '.$discount : '').($prod_amount >0 ? '<br/>'.$obj->l('Deposit', 'cart').' : '.Tools::displayPrice($prod_amount, $currency, false).' ' : '').$ext_text
			);
	}
	
	function lengthToString($obj, $id_lang, $availabilities = null) {
		if ($this->_mainProduct!=null) {
			if ($this->_mainProduct->_reservationLengthControl == reservation_length::NONE) return $obj->l('None','cart');
			else $length = $this->getLength(false, $availabilities);
			if ($this->_mainProduct->_reservationSelExt!=null) $unit = $this->_mainProduct->_reservationSelExt->getUnitLabel($obj, $length>1);
			else $unit = myOwnLang::getUnit($this->_mainProduct->_reservationUnit, $length>1);
			return $length.' '.$unit;
		} else {
			$length = $this->getLength(false, $availabilities);
			return $length.' '.myOwnLang::getUnit(reservation_unit::DAY, $length>1);
		}
	}
	
	function unitToString($obj, $id_lang) {
		if ($this->_mainProduct!=null) return $this->_mainProduct->getUnitLabel($obj, $id_lang);
		else return myOwnLang::getUnit(reservation_unit::DAY);
	}
	
	function reductionsToString($taxes = false) {
		$list="";
		$discounts = explode(';', $this->discounts);
		foreach($discounts as $discount) {
			$discountTab = explode(':', $discount);
			if (count($discountTab)>1) {
				$reduc=$discountTab[1];
				if ($taxes) $reduc = $reduc*(1+($this->tax_rate/100));
				if ($list!="") $list .= ", ";
				$list .= ucfirst($discountTab[0]).' : '.($reduc<0 ? '' :'+').str_replace(",",".",Tools::displayPrice($reduc));
			}
		}
		return $list;
	}
	
	function reductionsToHtml($taxes = false) {
		$list="";
		$discounts = explode(';', $this->discounts);
		foreach($discounts as $discount) {
			$discountTab = explode(':', $discount);
			if (count($discountTab)>1) {
				$reduc=$discountTab[1];
				if ($taxes) $reduc = $reduc*(1+($this->tax_rate/100));
				if ($list!="") $list .= " &nbsp; ";
				$list .= ucfirst($discountTab[0]).' <span class="price-percent-reduction small">'.($reduc<0 ? '' :'+').str_replace(",",".",Tools::displayPrice($reduc)).'</span>';
			}
		}
		return $list;
	}
	
	//-----------------------------------------------------------------------------------------------------------------
	//PERIOD UTILS
	//-----------------------------------------------------------------------------------------------------------------
	
	//Count timeslots on period
	//-----------------------------------------------------------------------------------------------------------------
	public function countTimeslotsBetween($startLimit, $endLimit) {
		if ($this->startDate=="") $startday = strtotime($this->endDate);
		else $startday = strtotime($this->startDate);
		$endday = strtotime($this->endDate);
		return $this->_mainProduct->countTimeslots($startday, $endday, $this->startTimeslot, $this->endTimeslot, true, $startLimit, $endLimit);
	}
	
	//Count Timeslots and days
	//-----------------------------------------------------------------------------------------------------------------
	public function countTimeslotId($id_timeslot=0, $daysandweeks) {
		if ($this->startDate=="") $startday = strtotime($this->endDate);
		else $startday = strtotime($this->startDate);
		$endday = strtotime($this->endDate);
		return $this->_mainProduct->countTimeslots($startday, $endday, $this->startTimeslot, $this->endTimeslot, true, 0, 0, $id_timeslot, $daysandweeks);
	}
	
	public function countTimeslicesId($id_timeslot=0, $daysandweeks) {
		$timeslot = null;
		if ($id_timeslot>0) {
			$obj = Module::getInstanceByName('myownreservations');
			if (array_key_exists($id_timeslot, $obj->_timeSlots->list))
				$timeslot = $obj->_timeSlots->list[$id_timeslot];
		}
		if ($this->startDate=="") $startday = strtotime($this->endDate);
		else $startday = strtotime($this->startDate);
		return $this->_mainProduct->countTimeslices($startday, $this->startTimeslot, $this->endTimeslot, true, 0, 0, $timeslot, $daysandweeks);
	}
	
	//Count Days on period
	//-----------------------------------------------------------------------------------------------------------------
	public function countDaysBetween($startDate, $endDate, $length=null) {
		$timeslots = $this->_mainProduct->_timeslotsObj->list;
		if ($this->startDate=="") $start = strtotime($this->endDate);
		else $start = strtotime($this->startDate);
		$end = strtotime($this->endDate);
		
		if ($length!=null && $this->startDate!="" && $start>$startDate && $end<$endDate) return $length;

		$length = -1;
		$daylength = 0;
		$endBetweenDates=false;
		do {
			$length++;
			$checkLimit = strtotime("+".$length." day",$start);
			if (date('Y-m-d',$end)==date('Y-m-d',$checkLimit)) $endBetweenDates=true;
			
			if ($checkLimit >= $startDate && $checkLimit <= $endDate)
				if ($this->_mainProduct->_priceDisabledTimeSlots
					or $this->_mainProduct->_timeslotsObj->areEnableOnDay($checkLimit))
					$daylength++;
		} while (date('Y-m-d',$checkLimit) != date('Y-m-d',$end)
				 && date('Y-m-d',$checkLimit) < date('Y-m-d',$end) && $length<10000 );

		if ($daylength>0 && $endBetweenDates && $this->_mainProduct->_priceEndTimeSlot && MyOwnCalendarTool::getFirstTimeSlot($timeslots, $end)==$this->endTimeslot) $daylength--;
		if ($this->_mainProduct->reservationSelType==reservation_interval::DAY && $this->_mainProduct->_reservationStartTime > $this->_mainProduct->_reservationEndTime) $daylength--;
		if ($this->_mainProduct->reservationSelType==reservation_interval::DAY && $this->_mainProduct->_reservationStartTime <= $this->_mainProduct->_reservationEndTime) $daylength++;
		return $daylength;
	}
	
	//Count Days
	//-----------------------------------------------------------------------------------------------------------------
	public function countDays($forPrice = false, $availabilities = null) {
		if ($this->startDate=="") $start = strtotime($this->endDate);
		else $start = strtotime($this->startDate);
		$end = strtotime($this->endDate);
		if ($this->_mainProduct==null) $count = MyOwnCalendarTool::countDaysDefault($start, $end);
		else $count = $this->_mainProduct->countDays($start, $end, $this->startTimeslot, $this->endTimeslot, $this->id_product, $this->id_product_attribute, $availabilities, $forPrice);
		return $count;
	}
		
	//Count Weeks
	//-----------------------------------------------------------------------------------------------------------------
	public function countWeeks($forPrice = false) {

		if ($this->startDate=="") $start = strtotime($this->endDate);
		else $start = strtotime($this->startDate);
		$end = strtotime($this->endDate);

		return $this->_mainProduct->countWeeks($start, $end, $forPrice);
	}
	
	//Count Weeks
	//-----------------------------------------------------------------------------------------------------------------
	public function countWeeksBetween($startDate, $endDate) {
		if ($this->startDate=="") $start = strtotime($this->endDate);
		else $start = strtotime($this->startDate);
		$end = strtotime($this->endDate);

		$length = -1;
		$weekLength = 0;
		$currentWeek="";
		do {
			$length++;
			$checkLimit = strtotime("+".$length." day",$start);
			//if ($currentWeek!=date('o-W',$checkLimit)) {
			if ($checkLimit >= $startDate && $checkLimit <= $endDate)
				if ($this->_mainProduct->_timeslotsObj->areEnableOnDay($checkLimit)
					&& date('Y-m-d',$checkLimit) != date('Y-m-d',$end)) {
					$weekLength++;
				}
		} while (date('Y-m-d',$checkLimit) != date('Y-m-d',$end)
				 && date('Y-m-d',$checkLimit) < date('Y-m-d',$end) && $length<10000 );
				 
		return $weekLength;
	}
	
	//Count months
	//-----------------------------------------------------------------------------------------------------------------
	public function countMonths($forPrice = false) {
		if ($this->startDate=="") $start = strtotime($this->endDate);
		else $start = strtotime($this->startDate);
		$end = strtotime($this->endDate);
		if (!$this->_mainProduct->_priceStrict or !$forPrice) {
			$length=0;
			do {
				$length++;
				$checkLimit = strtotime("+".$length." month",$start);
			} while (date('Y-m-d',$checkLimit) < date('Y-m-d',$end) && $length<1000 );
			return $length;
		} else return MyOwnCalendarTool::countEachMonths($start, $end);
	}


}

?>