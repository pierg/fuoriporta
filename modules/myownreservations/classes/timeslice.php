<?php

class myOwnTimeSlice {
	var $ressources;
	var $startTime;
	var $endTime;
	var $days;
	var $selectable;
	var $resEndTime;

	//is this timeslot enable on a day
	//-----------------------------------------------------------------------------------------------------------------
	function isEnableOnDay($day) {
		return $this->days[date("N",$day)];
	}
	
	//Check if a timeslot is available for a new delivery
	//-----------------------------------------------------------------------------------------------------------------
	function isUnavailable($day, $availabilities) {
		//$startsel=strtotime('-2 hours',$this->getStartDate($day));
		$ressources = explode(',',$this->ressources[date("N",$day)]);
		$available=false;
		foreach ($ressources as $ressourceId) {
			//if (!myOwnServicesCalendar::isDayInThisHoliday($day, $placeHolidays[$ressourceId])) {
			if ($availabilities->isPeriodAvailable($this->getStartDate($day), $this->getEndDate($day), $ressourceId)) 
				$available=true;
		}
		return !$available;
	}

	//Get start date of this timeslot
	//-----------------------------------------------------------------------------------------------------------------
	function getStartDate($day) {
		return strtotime(date('Y-m-d',$day).' '.$this->startTime);
	}
	function getStartDateStr($day) {
		return date('Y-m-d H:i:s',$this->getStartDate($day));
	}

	//Get end date of this timeslot
	//-----------------------------------------------------------------------------------------------------------------
	function getEndDate($day) {
		return strtotime(date('Y-m-d',$day).' '.$this->endTime);
	}
	
	//Check if a timeslot is available for a new delivery
	//-----------------------------------------------------------------------------------------------------------------
	function getAvailableRessource($day, $ressourcesQties, $serviceQty, $availabilities) {
		//$startsel=strtotime('-2 hours',$this->getStartDate($day));
		$ressources = explode(',',$this->ressources[date("N",$day)]);
		foreach ($ressources as $ressourceId) {
			//if (!myOwnServicesCalendar::isDayInThisHoliday($day, $placeHolidays[$ressourceId])) {
			if ($availabilities->isPeriodAvailable($this->getStartDate($day), $this->getReservationEndTime($day), $ressourceId)) {
				$resqty = myOwnServicesReservations::getQtyOnInterval($this->getStartDate($day),$this->getReservationEndTime($day), $ressourceId);
				if ($resqty+$serviceQty<=$ressourcesQties[$ressourceId]) return $ressourceId;
			}//else return -2;
		}
		return -1;
	}
	
	function getTotalQty($day, $ressourcesQties) {
		$cnt=0;
		$ressources = explode(',',$this->ressources[date("N",$day)]);
		foreach ($ressources as $ressourceId) {
			$cnt+=($ressourcesQties[$ressourceId]);
		}
		return $cnt;
	}
	function getLength() {
		if ($this->startTime==$this->endTime) return 24*60;
		return (strtotime($this->endTime)-strtotime($this->startTime))/60;
	}
	
	function isSelectableOnDay($day, $availabilities) {
		if ($this->selectable[date("N",$day)]) {
			$available=false;
			$ressources = explode(',',$this->ressources[date("N",$day)]);
			//if (date('Y-m-d',$this->getStartDate($day)) == "2013-05-20") echo '#'.date('H:i:s',$this->getStartDate($day)).' '.$this->ressources[date("N",$day)];
			foreach ($ressources as $ressourceId) {
				//echo "isPeriodAvailable ".$ressourceId." ".date('Y-m-d H:i:s',$this->getStartDate($day)).' '.date('Y-m-d H:i:s',$this->getReservationEndTime($day));
				
				if ($availabilities->isPeriodAvailable($this->getStartDate($day), $this->getReservationEndTime($day), $ressourceId)) $available=true;
				//if (date('Y-m-d',$this->getStartDate($day)) == "2013-05-20") echo "isPeriodAvailable".$ressourceId.":".intval($available);
			}
			//if (date('Y-m-d',$this->getStartDate($day)) == "2013-05-20") echo '#'.intval($available);
			return $available;
		}
		return false;
	}
	function getRessourceOnDay($day) {
		if (array_key_exists(date("N",$day), $this->ressources))
			return $this->ressources[date("N",$day)];
		else return "";
	}
	
	function getReservationEndTime($day) {
		return strtotime(date('Y-m-d',$day).' '.$this->resEndTime);
	}
	function getReservationEndTimeStr($day) {
		return date('Y-m-d H:i:s',$this->getReservationEndTime($day));
	}
	

	
}
	
?>