<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnReservation extends myOwnCart {
	var $id_order;
	var $id_cartproduct=0;
	var $reference;
	var $ref_order;
	var $id_source;
	var $id_shop;
	var $id_assigned;
	var $startTime="00:00:00";
	var $endTime="00:00:00";
	var $validated;
	var $stock;
	var $quantity_refunded;
	var $advance;
	var $comment;
	var $external_fields;
	
	var $_start;
	var $_end;
	var $_orderStatus;
	var $_deliveryId;
	var $_customerId;
	var $_customerName="";
	var $_customerEmail="";
	var $_customerPhone;
	var $_customerAddressId;
	var $_customerAddressStreet;
	var $_customerAddressPostCode;
	var $_customerAddressCity;
	var $_carrier = 0;
	var $_payment = 0;
	var $_saved=false;
	var $_isCart=false;
	
	public static $definition = array(
        'table' => 'reservations',
        'primary' => 'id_reservation',
        'fields' => array(
            'id' =>            array( 'sqlId' => 'sqlId', 'type' => ObjectModel::TYPE_INT, 'validate' => 'isUnsignedId', 'required'=>true),
            'id_order' =>            array('type' => ObjectModel::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_shop' =>            array('type' => ObjectModel::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_product' =>            array('type' => ObjectModel::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_product_attribute' =>            array('type' => ObjectModel::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_customization' =>            array('type' => ObjectModel::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_object' =>            array('type' => ObjectModel::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_assigned' =>            array('type' => ObjectModel::TYPE_INT, 'validate' => 'isUnsignedId'),
            
            'reference'  =>                array('type' => ObjectModel::TYPE_STRING, 'validate' => 'isGenericName'),
            'external_fields' =>                array('type' => ObjectModel::TYPE_STRING),
            
            'quantity_refunded' =>            array('type' => ObjectModel::TYPE_INT, 'validate' => 'isUnsignedId'),
            'quantity' =>            array('type' => ObjectModel::TYPE_INT, 'validate' => 'isUnsignedId'),
            
            'start_date' =>            array('type' => ObjectModel::TYPE_DATE, 'validate' => 'isDate', 'required' => true, 'getter'=>'getStartDate'),
            'end_date' =>            array('type' => ObjectModel::TYPE_DATE, 'validate' => 'isDate', 'required' => true,  'getter'=>'getEndDate'),
            
            'start_time' =>            array('type' => ObjectModel::TYPE_INT, 'validate' => 'isUnsignedId', 'getter'=>'getStartTime'),
            'end_time' =>            array('type' => ObjectModel::TYPE_INT, 'validate' => 'isUnsignedId', 'getter'=>'getEndTime'),
            
            'start_timeslot' =>            array('type' => ObjectModel::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true, 'getter'=>'getStartTimeSlotId'),
            'end_timeslot' =>            array('type' => ObjectModel::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true, 'getter'=>'getEndTimeSlotId'),
            
            'validated' =>            array('type' => ObjectModel::TYPE_INT, 'validate' => 'isUnsignedId'),
            
            'price' =>                array('type' => ObjectModel::TYPE_FLOAT, 'validate' => 'isPrice'),
            'tax_rate' =>                array('type' => ObjectModel::TYPE_FLOAT, 'validate' => 'isPrice'),
            'advance' =>                array('type' => ObjectModel::TYPE_FLOAT, 'validate' => 'isPrice'),
            
            'discounts' =>                array('type' => ObjectModel::TYPE_STRING),
            'stock' =>                array('type' => ObjectModel::TYPE_STRING),
            'comment' =>                array('type' => ObjectModel::TYPE_STRING),
        ),
    );
    
	public function __construct($id_reservation=0, $queryRes=null)
	{
		//if () {
			$this->sqlId=$id_reservation;
			if ($queryRes==null && intval($id_reservation) > 0) {
				$sql = "SELECT * FROM " . _DB_PREFIX_ . "myownreservations_reservation WHERE id_reservation = ".$id_reservation;
				$resaSql = Db::getInstance()->ExecuteS($sql);
				if (count($resaSql)) $queryRes= $resaSql[0];
			}
			if ($queryRes!=null) {
				$this->_saved = true;
				$this->sqlId = intval($queryRes['id_reservation']);
				$this->reference = $queryRes['reference'];
				$this->id_order = intval($queryRes['id_order']);
				$this->id_shop = intval($queryRes['id_shop']);
				if (array_key_exists('id_source', $queryRes)) $this->id_source = intval($queryRes['id_source']);
				if (array_key_exists('id_cartproduct', $queryRes)) $this->id_cartproduct = intval($queryRes['id_cartproduct']);
				$this->id_product = intval($queryRes['id_product']);
				$this->id_product_attribute = intval($queryRes['id_product_attribute']);
				$this->id_customization = intval($queryRes['id_customization']);
				if (array_key_exists('id_object', $queryRes)) $this->id_object = intval($queryRes['id_object']);
				if (array_key_exists('id_assigned', $queryRes)) $this->id_assigned = intval($queryRes['id_assigned']);
				$this->quantity = $queryRes['quantity'];
				if (array_key_exists('startDate', $queryRes)) 
					$this->startDate = $queryRes['startDate'];
				else $this->startDate = $queryRes['start_date'];
				if (array_key_exists('start_time', $queryRes)) 
					$this->startTime = $queryRes['start_time'];
				$this->startTimeslot = intval($queryRes['start_timeslot']);
				//patch for creating from POST (cannot rename end_date that is DB field and dhtmlx use it and add time to it)
				if (array_key_exists('endDate', $queryRes)) 
					$this->endDate = $queryRes['endDate'];
				else $this->endDate = $queryRes['end_date'];
				if (array_key_exists('end_time', $queryRes)) 
					$this->endTime = $queryRes['end_time'];
				$this->endTimeslot = intval($queryRes['end_timeslot']);
				$this->validated = $queryRes['validated'];
				$this->price = $queryRes['price'];
				$this->tax_rate = $queryRes['tax_rate'];
				$this->discounts = $queryRes['discounts'];
				$this->stock = $queryRes['stock'];
			 	$this->comment = stripslashes($queryRes['comment']);
				$this->quantity_refunded = $queryRes['quantity_refunded'];
				$this->advance = $queryRes['advance'];
				$this->external_fields = stripslashes($queryRes['external_fields']);
			} else $this->_saved = false;
		//}
	}
	
	public function validateFields() {

    	if ($this->startDate=='' || $this->endDate=='') return 'Please indicate start date and End date';
    	if ($this->startTimeslot==='' || $this->endTimeslot==='') return 'Please indicate start time slot and End time slot';
    	if ($this->id_product=='') return 'Please indicate product';
    	$obj = Module::getInstanceByName('myownreservations');
    	$this->_mainProduct = $obj->_products->getResProductFromProduct($tempResa->id_product);
    	if ($this->_mainProduct==null) return 'Product is not reservable';
    	if (!array_key_exists($this->startTimeslot, $this->_mainProduct->_timeslotsObj->list)) return 'Invalid start time slot';
    	if (!array_key_exists($this->endTimeslot, $this->_mainProduct->_timeslotsObj->list)) return 'Invalid end time slot';
    	if (!$this->_mainProduct->_timeslotsObj->list[$this->startTimeslot]->isEnableOnDay(strtotime($this->startDate))) return 'Start time slot disabled on start date';
    	if (!$this->_mainProduct->_timeslotsObj->list[$this->endTimeslot]->isEnableOnDay(strtotime($this->endDate))) return 'End time slot disabled on end date';
    	$this->startTime=$this->getStartHour();
    	$this->endTime=$this->getEndHour();
    	$this->setPrice($obj->_pricerules->list, null);
    	return true;
    }
    
    public function getWebserviceParameters($ws_params_attribute_name = null)
    {
    	$fields = self::$definition['fields'];
     	foreach($fields as $key => &$field)
     		if (!array_key_exists('sqlId', $field))
     			$field['sqlId']=$key;
        $default_resource_parameters = array(
            'objectSqlId' => $this->def['primary'],
            'objectNodeName' => 'reservations',
            'objectsNodeName' => 'reservations',
            'retrieveData' => array(
                'className' => 'myOwnReservation',//get_class($this),
                'retrieveMethod' => 'getWebserviceObjectList',
                'params' => array(''),
                'table' => $this->def['table'],
            ),
            'fields' => $fields
        );

        return $default_resource_parameters;
    }
    
    public function getWebserviceObjectList($sql_join, $sql_filter, $sql_sort, $sql_limit)
    {
        $assoc = Shop::getAssoTable($this->def['table']);
        $class_name = WebserviceRequest::$ws_current_classname;
        $vars = get_class_vars($class_name);
        if ($assoc !== false) {
            if ($assoc['type'] !== 'fk_shop') {
                $multi_shop_join = ' LEFT JOIN `'._DB_PREFIX_.bqSQL($this->def['table']).'_'.bqSQL($assoc['type']).'`
										AS `multi_shop_'.bqSQL($this->def['table']).'`
										ON (main.`'.bqSQL($this->def['primary']).'` = `multi_shop_'.bqSQL($this->def['table']).'`.`'.bqSQL($this->def['primary']).'`)';
                $sql_filter = 'AND `multi_shop_'.bqSQL($this->def['table']).'`.id_shop = '.Context::getContext()->shop->id.' '.$sql_filter;
                $sql_join = $multi_shop_join.' '.$sql_join;
            } else {
                $vars = get_class_vars($class_name);
                foreach ($vars['shopIDs'] as $id_shop) {
                    $or[] = '(main.id_shop = '.(int)$id_shop.(isset($this->def['fields']['id_shop_group']) ? ' OR (id_shop = 0 AND id_shop_group='.(int)Shop::getGroupFromShop((int)$id_shop).')' : '').')';
                }

                $prepend = '';
                if (count($or)) {
                    $prepend = 'AND ('.implode('OR', $or).')';
                }
                $sql_filter = $prepend.' '.$sql_filter;
            }
        }
        $query = '
		SELECT DISTINCT main.`'.bqSQL(self::$definition['primary']).'` FROM `'._DB_PREFIX_.bqSQL('myownreservations_reservation').'` AS main
		'.$sql_join.'
		WHERE 1 '.$sql_filter.'
		'.($sql_sort != '' ? $sql_sort : '').'
		'.($sql_limit != '' ? $sql_limit : '');

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);
    }
	
	function getEndDateTime($display=true, $endDate=-1, $endTimeslot=-1) {
		if ($display && $endDate!=-1)
			return strtotime($this->endDate.' '.$this->endTime);
		else return parent::getEndDateTime($display, $endDate, $endTimeslot);
	}
	
	function getStartDateTime($startDate = -1, $startTimeslot = -1) {
		//args used when using getRevShift
		if ($startDate!=-1 && $startTimeslot!=-1) {
			if ($this->_mainProduct!=null && array_key_exists($startTimeslot, $this->_mainProduct->_timeslotsObj->list)) 
				$startTime = $this->_mainProduct->_timeslotsObj->list[$startTimeslot]->startTime;
			return strtotime($startDate.' '.$startTime);
		} else if ($this->_mainProduct!=null && $this->_mainProduct->reservationSelType==reservation_interval::DAY && $this->_mainProduct->_reservationStartTime != $this->_mainProduct->_reservationEndTime) {
			return strtotime($this->startDate.' '.$this->_mainProduct->_reservationStartTime);
		} else 
			return strtotime($this->startDate.' '.$this->startTime);
	}
	
	function getSerialItem($qty) {
		$serial = explode(';', $this->stock);
		if (array_key_exists($qty, $serial)) return $serial[$qty];
		return '';
	}
	
	//Create a resa in database
	//-----------------------------------------------------------------------------------------------------------------
	function sqlInsert($parent=false) {
		if ($parent) return parent::sqlInsert();
		$query  = 'INSERT INTO '. _DB_PREFIX_ .'myownreservations_reservation (reference, id_order, id_shop, id_source, id_cartproduct, id_product, id_product_attribute, id_customization, id_object, id_assigned, quantity, quantity_refunded, start_date, start_timeslot, start_time, end_date, end_timeslot, end_time, validated, price, tax_rate, advance, discounts, stock, comment, external_fields) VALUES ("'.($this->id_source && $this->reference!='' ? $this->reference : strtoupper(Tools::passwdGen(8, 'NO_NUMERIC'))).'", '.$this->id_order.', '.(int)$this->id_shop.', '.(int)$this->id_source.', '.(int)$this->id_cartproduct.', '.$this->id_product.', '.$this->id_product_attribute.', '.intval($this->id_customization).', '.intval($this->id_object).', 0,  '.$this->quantity.', 0, "'.$this->startDate.'", '.$this->startTimeslot.', "'.$this->startTime.'", "'.$this->endDate.'", '.$this->endTimeslot.', "'.$this->endTime.'", '.(int)$this->validated.', "'.$this->price.'", "'.$this->tax_rate.'", "'.floatval($this->advance).'", "'.$this->discounts.'", "'.$this->stock.'", "'.$this->comment.'", "'.$this->external_fields.'");';
		$res = Db::getInstance()->Execute($query);
		$inserted = Db::getInstance()->ExecuteS("SELECT LAST_INSERT_ID() AS ID;");
		if ($res) return $inserted[0]['ID'];
		else return false;
	}
	
	//Update a resa in database
	//-----------------------------------------------------------------------------------------------------------------
	function sqlUpdate() {
		$query  = 'UPDATE '. _DB_PREFIX_ .'myownreservations_reservation SET id_cartproduct = "'.(int)$this->id_cartproduct.'", quantity = "'.$this->quantity.'", start_date = "'.$this->startDate.'", start_timeslot = '.$this->startTimeslot.', start_time = "'.$this->startTime.'", end_date = "'.$this->endDate.'", end_timeslot = '.$this->endTimeslot.', end_time = "'.$this->endTime.'", validated = '.(int)$this->validated.', price = "'.$this->price.'", tax_rate = "'.$this->tax_rate.'", advance = "'.floatval($this->advance).'", discounts = "'.$this->discounts.'", comment = "'.$this->comment.'", stock = "'.$this->stock.'", external_fields = "'.addslashes ($this->external_fields).'" WHERE id_reservation = '.$this->sqlId.';';
	
		return Db::getInstance()->Execute($query);
	}
	function sqlUpdateQty() {
		$query  = 'UPDATE '. _DB_PREFIX_ .'myownreservations_reservation SET quantity = "'.$this->quantity.'", quantity_refunded = "'.intval($this->quantity_refunded).'" WHERE id_reservation = '.$this->sqlId.';';
		//return $query;
		return Db::getInstance()->Execute($query);
	}
	
	function validate($obj, $val) {
		$this->validated=($val==1);
		$query  = 'UPDATE '. _DB_PREFIX_ .'myownreservations_reservation SET validated = '.(int)$val.' WHERE id_reservation = '.$this->sqlId.';';
		$res = Db::getInstance()->Execute($query);

		//Add extenssion attachements
		foreach ($obj->_extensions->getExtByType(extension_type::NOTIFICATION) as $ext) 
			if ($this->_mainProduct!=null && in_array($ext->id, $this->_mainProduct->ids_notifs))
				if (method_exists($ext, "validate"))
					$ext->validate($obj, $this, ($val==1));

		return $res;
	}
	
	function assign($obj, $val) {
		$this->id_assigned=$val;
		$query  = 'UPDATE '. _DB_PREFIX_ .'myownreservations_reservation SET id_assigned = '.$val.' WHERE id_reservation = '.$this->sqlId.';';
		$res = Db::getInstance()->Execute($query);
		return $res;
	}
	
	function park($obj, $val) {
		$this->id_object=$val;
		$query  = 'UPDATE '. _DB_PREFIX_ .'myownreservations_reservation SET id_object = '.$val.' WHERE id_reservation = '.$this->sqlId.';';
		$res = Db::getInstance()->Execute($query);
		return $res;
	}
	
	public function isAvailable($cart=null, $availabilities = null, $stocks=null, $updating=false) {
		if (!$this->_saved) return parent::isAvailable($cart, $availabilities, $stocks);
		else {
			$availableQty = $this->getAvailableQuantity($cart, $availabilities);
			
			$noStock=false;
			if (Configuration::get('PS_ORDER_OUT_OF_STOCK')==1 or Configuration::get('PS_STOCK_MANAGEMENT')==0 or $availableQty==-1) $noStock=true;
			
			$qtyCapacity=$this->_mainProduct->_qtyCapacity;
			if ($qtyCapacity>0) $resaOccupation = ceil($this->quantity/$qtyCapacity);
			else  $resaOccupation = $this->quantity;
	
			if (!$this->sqlId) return ($noStock or $resaOccupation <= $availableQty);
			else return ($noStock or $availableQty>=0);
		}
	}
	
	function getDiscountsString($labels) {
		$out = "";
		$discountsstr = $this->discounts;
		$discounts = explode(";",$discountsstr);
		foreach($discounts as $discount) {
			$discountdetails = explode(":",$discount);
			if (count($discountdetails)>1) {
				if ($out!="") $out .= "<br/>";
				if ($labels) $out .= $discountdetails[0];
				else $out .= Tools::ps_round(-$discountdetails[1],_PS_PRICE_DISPLAY_PRECISION_).' '.myOwnUtils::getCurrency();
			}
		}
		return $out;
	}
	
	function getUnitPriceSaved($with_taxes) {
		if ($with_taxes) return $this->price*(1+($this->tax_rate/100));
		else return $this->price;
	}
	
	function getTotalPriceSaved($with_taxes) {
		return Tools::ps_round($this->getUnitPriceSaved($with_taxes)*$this->quantity,_PS_PRICE_DISPLAY_PRECISION_);
	}
	
	function getAdvancePriceSaved($with_taxes) {
		if ($with_taxes) return $this->advance*$this->quantity*(1+($this->tax_rate/100));
		else return $this->advance*$this->quantity;
	}
	
	function toArray($id_lang=0, $link=null, $productsImages=null, $index=0, $factorize=0) {
		if ($productsImages!=null)
			$mediumSize=new ImageType(1);

		$scheduleTemp=array();
		$scheduleTemp['id'] = intval($this->sqlId).($index > 0 ? '#'.$index : '');
		$scheduleTemp['ref'] = $this->reference;
		$scheduleTemp['repeat_index'] = $index;
		$scheduleTemp['id_reservation'] = intval($this->sqlId);
		$scheduleTemp['id_order'] = intval($this->id_order);
		$scheduleTemp['id_source'] = intval($this->id_source);
		$scheduleTemp['ref_order'] = $this->ref_order;
		$scheduleTemp['id_shop'] = intval($this->id_shop);
		$scheduleTemp['id_product'] = $this->id_product;
		$scheduleTemp['id_product_attribute'] = $this->id_product_attribute;
		$scheduleTemp['id_customization'] = $this->id_customization;
		$scheduleTemp['quantity'] = $this->quantity;
		$scheduleTemp['startDate'] = $this->startDate;
		$scheduleTemp['start_timeslot'] = $this->startTimeslot;
		$scheduleTemp['endDate'] = $this->endDate;
		$scheduleTemp['end_timeslot'] = $this->endTimeslot;
		$scheduleTemp['periodKey'] = $this->startDate.'_'.$this->startTimeslot.'#'.$this->endDate.'_'.$this->endTimeslot;
		$scheduleTemp['validated'] = $this->validated;
		$scheduleTemp['comment'] = addslashes($this->comment);
		$scheduleTemp['object'] = addslashes($this->id_object);
		$scheduleTemp['assigned'] = addslashes($this->id_assigned);
		$scheduleTemp['stock'] = addslashes($this->stock);
		$scheduleTemp['price'] = $this->price;
		$scheduleTemp['amount'] = Tools::displayPrice($this->getTotalPriceSaved(true));
		$scheduleTemp['tax_rate'] = $this->tax_rate;
		$scheduleTemp['discounts'] = $this->discounts;
		$scheduleTemp['quantity_refunded'] = $this->quantity_refunded;
		$scheduleTemp['advance'] = $this->advance;
		$scheduleTemp['external_fields'] = $this->external_fields;

		$scheduleTemp['details'] = $this->_customerName;
		$scheduleTemp['customer'] = $this->_customerName.($this->_customerPhone != '' ? ' ('.$this->_customerPhone.')' : '');
		if ($this->_mainProduct!=null) {
			if ($this->_mainProduct->productType!=product_type::VIRTUAL)
				$scheduleTemp['ykey'] = $this->id_product.'-'.$this->id_product_attribute;
			else $scheduleTemp['ykey'] = $this->id_product.'-0';
		} else $scheduleTemp['ykey'] = "0";
		
		if ($this->_mainProduct!=null)
			$scheduleTemp['productFamilly'] = $this->_mainProduct->sqlId;

		$scheduleTemp['start_date'] = date('Y-m-d H:i', $this->getStartDateTime());
		$scheduleTemp['end_date'] = date('Y-m-d H:i', $this->getEndDateTime(true));
		$scheduleTemp['start_timestamp'] = ($this->getStartDateTime()*1000);
		$scheduleTemp['end_timestamp'] = ($this->getEndDateTime(false)*1000);
		
		if ($id_lang) {
			$attribute_text=MyOwnReservationsUtils::getAttributeCombinaisons($this->id_product, $this->id_product_attribute, $id_lang);
			$scheduleTemp['product'] = MyOwnReservationsUtils::getProductName($this->id_product, $id_lang);
			$scheduleTemp['attribute'] = trim($attribute_text)!='' ? '<br />'.$attribute_text : '';
			$scheduleTemp['status_text'] = strtoupper(substr(myOwnUtils::getStatusName($this->_orderStatus,$id_lang),0,22));
			$scheduleTemp['status_color'] = myOwnUtils::getStatusColor($this->_orderStatus);
		}
		if ($productsImages!=null && array_key_exists($this->id_product, $productsImages)) {
			$viewedProduct=$productsImages[$this->id_product];
			$scheduleTemp['image'] = $link->getImageLink($viewedProduct->link_rewrite, $viewedProduct->cover, $mediumSize->name);
		}
		return $scheduleTemp;
	}

}



?>