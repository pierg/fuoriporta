SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Structure de la table `ps_myownreservations_cartproducts`
--

CREATE TABLE IF NOT EXISTS `ps_myownreservations_cartproducts` (
  `id_cartproduct` int(11) NOT NULL AUTO_INCREMENT,
  `id_cart` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_product_attribute` int(11) NOT NULL,
  `id_customization` int(11) NOT NULL,
  `id_object` int(11) NOT NULL,
  `quantity` int(10) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `start_timeslot` int(11) NOT NULL,
  `end_timeslot` int(11) NOT NULL,
  PRIMARY KEY (`id_cartproduct`)
) DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_myownreservations_reservation`
--

CREATE TABLE IF NOT EXISTS `ps_myownreservations_reservation` (
  `id_reservation` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) NOT NULL,
  `id_cartproduct` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_product_attribute` int(11) NOT NULL,
  `id_customization` int(11) NOT NULL,
  `id_object` int(11) NOT NULL,
  `reference` varchar(8) NOT NULL,
  `quantity` int(10) NOT NULL,
  `quantity_refunded` int(10) NOT NULL,
  `start_date` DATE NOT NULL,
  `end_date` DATE NOT NULL,
  `start_time` TIME NOT NULL,
  `end_time` TIME NOT NULL,
  `start_timeslot` int(11) NOT NULL,
  `end_timeslot` int(11) NOT NULL,
  `validated` tinyint(1) NOT NULL,
  `price` decimal(20,6) NOT NULL,
  `tax_rate` decimal(10,3) NOT NULL,
  `advance` decimal(20,6) NOT NULL,
  `discounts` text NOT NULL,
  `stock` text NOT NULL,
  `external_fields` VARCHAR(128) NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`id_reservation`)
) DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Structure de la table `ps_myownreservations_timeslot`
--

CREATE TABLE IF NOT EXISTS `ps_myownreservations_timeslot` (
  `id_timeslot` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_family` int(10) NOT NULL,
  `id_product` int(10) NOT NULL,
  `id_object` int(10) NOT NULL,
  `name` varchar(64) NOT NULL,
  `day1` tinyint(1) NOT NULL,
  `day2` tinyint(1) NOT NULL,
  `day3` tinyint(1) NOT NULL,
  `day4` tinyint(1) NOT NULL,
  `day5` tinyint(1) NOT NULL,
  `day6` tinyint(1) NOT NULL,
  `day7` tinyint(1) NOT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `type` tinyint(1) NOT NULL,
  `quota` int(11) NOT NULL,
  PRIMARY KEY (`id_timeSlot`)
) DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_myownreservations_pricesrule`
--

CREATE TABLE IF NOT EXISTS `ps_myownreservations_pricesrule` (
  `id_pricerule` int(11) NOT NULL AUTO_INCREMENT,
  `id_family` int(11) NOT NULL,
  `id_object` int(10) NOT NULL,
  `name` varchar(64) NOT NULL,
  `ids_products` TEXT NOT NULL,
  `ids_attributes` TEXT NOT NULL,
  `type` tinyint(1) NOT NULL,
  `ratio` decimal(10,4) NOT NULL,
  `amount` decimal(10,3) NOT NULL,
  `params` varchar(255) NOT NULL,
  `overall` tinyint(1) NOT NULL,
  `quantity` tinyint(3) NOT NULL DEFAULT '1',
  `enable` tinyint(1) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `impact` tinyint(2) NOT NULL,
  `style` varchar(255) NOT NULL,
  PRIMARY KEY (`id_pricerule`)
) DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Structure de la table `ps_myownreservations_availability`
--

CREATE TABLE IF NOT EXISTS `ps_myownreservations_availability` (
  `id_availability` int(10) NOT NULL AUTO_INCREMENT,
  `id_family` int(11) NOT NULL,
  `id_shop` int(10) NOT NULL,
  `id_product` int(10) NOT NULL,
  `id_product_attribute` int(10) NOT NULL,
  `id_object` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `start_timeslot` int(11) NOT NULL,
  `end_timeslot` int(11) NOT NULL,
  `quantity` int(10) NOT NULL,
  PRIMARY KEY (`id_availability`)
) DEFAULT CHARSET=utf8 ;

-- --------------------------------------------------------

--
-- Structure de la table `ps_myownreservations_pricesset`
--

CREATE TABLE IF NOT EXISTS `ps_myownreservations_pricesset` (
  `id_product` int(10) NOT NULL,
  `id_product_attribute` int(10) NOT NULL,
  `period` int(2) NOT NULL,
  `amount` decimal(10,3) NOT NULL
) DEFAULT CHARSET=utf8 ;

-- --------------------------------------------------------

--
-- Structure de la table `ps_myownreservations_option`
--

CREATE TABLE IF NOT EXISTS `ps_myownreservations_option` (
  `id_family` int(10) NOT NULL,
  `id_shop` int(10) NOT NULL,
  `id_extension` int(10) NOT NULL,
  `id_option` int(10) NOT NULL,
  `id_internal` int(10) NOT NULL,
  `type` tinyint(3) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id_family`, `id_extension`, `id_option`, `id_internal`, `id_shop`)
) DEFAULT CHARSET=utf8 ;

-- --------------------------------------------------------

--
-- Structure de la table `ps_myownreservations_detail`
--

CREATE TABLE IF NOT EXISTS `ps_myownreservations_detail` (
  `id_reservation` int(10) NOT NULL,
  `id_extension` int(10) NOT NULL,
  `id_option` int(10) NOT NULL,
  `id_internal` int(10) NOT NULL,
  `type` tinyint(3) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id_reservation`, `id_extension`, `id_option`, `id_internal`)
) DEFAULT CHARSET=utf8 ;
