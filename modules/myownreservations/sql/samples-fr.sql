SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Contenu de la table `ps_myownreservations_timeslot`
--

INSERT INTO `ps_myownreservations_timeslot` (`id_timeSlot`, `name`, `day1`, `day2`, `day3`, `day4`, `day5`, `day6`, `day7`, `startTime`, `endTime`, `type`, `quota`) VALUES
(1, 'midi', 1, 1, 1, 1, 1, 0, 0, '12:00:00', '15:00:00', 0, 10),
(2, 'soir', 1, 1, 1, 1, 1, 0, 0, '15:00:00', '18:00:00', 0, 10),
(3, 'matin', 1, 1, 1, 1, 1, 0, 0, '08:00:00', '12:00:00', 0, 20);

