<?php
/*
* 2010-2012 LaBulle All right reserved
*/
if (file_exists(_PS_MODULE_DIR_ . "myownreservations/myownreservations.php"))
	require_once(_PS_MODULE_DIR_ . "myownreservations/myownreservations.php");

/**
* Validate an order in database
* Function called from a payment module
*
* @param integer $id_cart Value
* @param integer $id_order_state Value
* @param float $amountPaid Amount really paid by customer (in the default currency)
* @param string $paymentMethod Payment method (eg. 'Credit card')
* @param string $message Message to attach to order
*/
if (_PS_VERSION_ >= "1.5.0.0") {
	abstract class PaymentModule extends PaymentModuleCore
	{
		public function validateOrder($id_cart, $id_order_state, $amountPaid, $paymentMethod = 'Unknown', $message = NULL, $extraVars = array(), $currency_special = NULL, $dont_touch_amount = false, $secure_key = false, Shop $shop = null)
		{
			global $cart;
			if ( $cart!=null && $cart->myownreservations!=null) {
				$extraVars = MyOwnReservationsUtils::validateOrderExtraVars($id_cart, $extraVars);
				
				/*$theme_template_path = _PS_THEME_DIR_.'mails'.DIRECTORY_SEPARATOR.$this->context->language->iso_code.DIRECTORY_SEPARATOR.'order_conf_product_list.tpl';
				$default_mail_template_path = _PS_MAIL_DIR_.$this->context->language->iso_code.DIRECTORY_SEPARATOR.'order_conf_product_list.tpl';

		        if (Tools::file_exists_cache($theme_template_path) || Tools::file_exists_cache($default_mail_template_path))
		        	unset($extraVars['{products}']);*/
		        
			}
			$res = parent::validateOrder($id_cart, $id_order_state, $amountPaid, $paymentMethod, $message, $extraVars, $currency_special, $dont_touch_amount, $secure_key, $shop);
			//still null = module diabled
			if ( $cart==null or $cart->myownreservations==null) return $res;
					
 			//update order total to add balance (couldn't do that before because of payment error if total_paid != total_paid_real )
 			$summary = $cart->getSummaryDetails();
 			$balance_reservations = $summary['balance_reservations'];
 			$balance_reservations_wt = $summary['balance_reservations_wt'];
 			
 			$advance_reservations = $summary['advance_reservations'];
 			$advance_reservations_wt = $summary['advance_reservations_wt'];
 			
 			$total_reservations_wt=$summary['total_reservations_wt'];
 			$total_reservations=$summary['total_reservations'];
 			$only_products_wt = $summary['total_products_wt']-$total_reservations_wt;
 			$only_products = $summary['total_products']-$total_reservations;

 			$total_price_wt = $summary['total_price'];
 			$total_price = $summary['total_price_without_tax'];
 			if ($balance_reservations>0) {
 				$order=new Order($this->currentOrder);
 				//Need to set total paid real even in PS 1.5. because if not we will not know the advance 
 				$order->total_paid_real = 0;//$order->total_paid;
 				$order->total_paid += $balance_reservations_wt;
 				$order->total_paid_tax_incl += $balance_reservations_wt;
 				$order->total_paid_tax_excl += $balance_reservations;
 				$order->update();
  			}
			return $res;
		}
	}
} else {
	abstract class PaymentModule extends PaymentModuleCore
	{
		public function validateOrder($id_cart, $id_order_state, $amountPaid, $paymentMethod = 'Unknown', $message = NULL, $extraVars = array(), $currency_special = NULL, $dont_touch_amount = false, $secure_key = false)
		{
			global $cart;
			if ( $cart!=null && $cart->myownreservations!=null)
				$extraVars = MyOwnReservationsUtils::validateOrderExtraVars($id_cart, $extraVars);
			$res = parent::validateOrder($id_cart, $id_order_state, $amountPaid, $paymentMethod, $message, $extraVars, $currency_special, $dont_touch_amount, $secure_key);
			//still null = module diabled
			if ( $cart==null or $cart->myownreservations==null) return $res;

			//update order total to add balance (couldn't do that before because of payment error if total_paid != total_paid_real )
 			$summary = $cart->getSummaryDetails();
 			$balance_reservations=$summary['balance_reservations_wt'];
 			if ($balance_reservations>0) {
 				$order=new Order($this->currentOrder);
 				$order->total_paid += $balance_reservations;
 				$order->update();
  			}
			return $res;
		}
	}
}
	
