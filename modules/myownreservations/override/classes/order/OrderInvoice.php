<?php
/*
* 2010-2012 LaBulle All right reserved
*/
if (file_exists(_PS_MODULE_DIR_ . "myownreservations/myownreservations.php"))
	require_once(_PS_MODULE_DIR_ . "myownreservations/myownreservations.php");

class OrderInvoiceTemp extends OrderInvoiceCore
{
	public $rates=array();
	public $taxes_excl=array();
	/**
	 * Get order products
	 *
	 * @return array Products with price, quantity (with taxe and without)
	 */
	public function getProducts($products = false, $selectedProducts = false, $selectedQty = false)
	{
		global $cookie;
		$myownreservations = Module::getInstanceByName('myownreservations');
		myOwnReservationsController::_construire($myownreservations);
		$depositMsg = Configuration::get('MYOWNRES_MSG_DEPOSIT', $cookie->id_lang);
		if (version_compare(_PS_VERSION_, '1.7.0.0', '>=')) $myownreservations->getContext()->getTranslator()->getCatalogue(null)->set('Wrapping Costs', $depositMsg, 'ShopPDF');
		$hidePrice = (Configuration::get('MYOWNRES_PRICE_TYPE')==reservation_price_display::HIDE);
		$products = parent::getProducts($products, $selectedProducts, $selectedQty);
		$newProducts=array();
		$new_total_tax_excl=0;
		$new_total_tax_incl=0;
		$advancePrice_wt=0;
		$advancePrice=0;
		$resaPrice=0;
		foreach ($products as $order_detail_id => $product) {
			$name=$product['product_name'];
			$customizedDatas=$product['customizedDatas'];
			
			$resas = myOwnResas::getReservations($this->id_order, $product['product_id'], $product['product_attribute_id']);
			if (count($resas)>0) {
				foreach($resas as $resa) {
					//$resa->_mainProduct = $myownreservations->_products->getResProductFromProduct($resa->id_product);
					//if (stripos($product['product_name'], "<br>")===false) $product['product_name']=$name."<br>".$resa->toString($myownreservations, $cookie->id_lang, false);
					$product['total_price_tax_excl'] = $resa->getTotalPriceSaved(false)*intval(!$hidePrice);
					$product['total_price_tax_incl'] = $resa->getTotalPriceSaved(true)*intval(!$hidePrice);
					$product['unit_price_tax_excl'] = $resa->getUnitPriceWithReduc(false);
					$product['unit_price_tax_incl'] = $resa->getUnitPriceWithReduc(true);
					$advancePrice_wt+=$resa->getAdvancePriceSaved(true);
					$advancePrice+=$resa->getAdvancePriceSaved(false);
					$product['unit_price_tax_excl_including_ecotax'] = $product['unit_price_tax_excl'];
		            $product['unit_price_tax_incl_including_ecotax'] = $product['unit_price_tax_incl'];
		            $product['total_price_tax_excl_including_ecotax'] = $product['total_price_tax_excl'];
		            $product['total_price_tax_incl_including_ecotax'] = $product['total_price_tax_incl'];
					$product['product_quantity'] = $resa->quantity;
					$product['customizedDatas']=array();
					foreach ($customizedDatas as $id_address => $customizedDataAddr)
						foreach ($customizedDataAddr as $key => $customizedDataItem)
							if ($key > 0 or $key==-$resa->sqlId or $key==-$resa->id_cartproduct) {
								if (!array_key_exists($id_address, $product['customizedDatas']))
									$product['customizedDatas'][$id_address]=array();
								$product['customizedDatas'][$id_address][$key]=$customizedDataItem;
							}
					
					$newProducts[$order_detail_id] = $product;
					$new_total_tax_excl+=$product['total_price_tax_excl'];
					$new_total_tax_incl+=$product['total_price_tax_incl'];
					$resaPrice+=$product['total_price_tax_incl'];
				}
			} else {
				$newProducts[$order_detail_id] = $product;
				$new_total_tax_excl+=$product['total_price_tax_excl'];
				$new_total_tax_incl+=$product['total_price_tax_incl'];
			}
			if ($product['total_price_tax_excl']>0) {
				$tax_amount=$product['total_price_tax_incl']-$product['total_price_tax_excl'];
				$rate = number_format( (($product['total_price_tax_incl']/$product['total_price_tax_excl'])-1)*100 ,0);
				if (array_key_exists($rate, $this->rates)) {
					$this->rates[$rate]+=$tax_amount;
					$this->taxes_excl[$rate]+=$product['total_price_tax_excl'];
				} else {
					$this->rates[$rate]=$tax_amount;
					$this->taxes_excl[$rate]=$product['total_price_tax_excl'];
				}
			}
		}
		if ($resaPrice==0) return $newProducts;
		$advance_rate = $advancePrice/$resaPrice;
		$this->total_balance_discounts_wt = ($this->total_discount_tax_incl*(1-$advance_rate));
		$this->total_balance_discounts = ($this->total_discount_tax_excl*(1-$advance_rate));
		$this->total_advance = $advancePrice;
		$this->total_advance_wt = $advancePrice_wt;
		$this->total_products=$new_total_tax_excl;
		$this->total_products_wt=$new_total_tax_incl;
		$this->total_resas_wt=$resaPrice;
		return $newProducts;
	}
	
	public function getProductTaxesBreakdown($order=null) {
		$taxes = parent::getProductTaxesBreakdown($order);
		foreach($taxes as &$taxe)
			foreach($this->rates as $rateKey => $rateValue) {
				if (array_key_exists('name', $taxe) && number_format(intval($taxe['name']),0)==number_format(intval($rateKey),0)) {
					$taxe['total_amount']=$rateValue;
					$taxe['total_price_tax_excl']=$this->taxes_excl[$rateKey];
				}
			}
		return $taxes;
	}
	
	// or !Module::isEnabled('myownreservations') 
	public function add($autodate = true, $null_values = false) {
		if (!Module::isInstalled('myownreservations') or !file_exists(_PS_MODULE_DIR_ . "myownreservations/myownreservations.php")) return parent::add($autodate, $null_values);
		$order = new Order($this->id_order);
		if ($order->total_paid_real>0 && $order->total_paid_real < $order->total_paid_tax_incl && !$order->hasInvoice()) {
			$balance_wt = $order->total_paid_tax_incl - $order->total_paid_real;
			$total_resas_wt = MyOwnReservationsUtils::getOrderTotalReservations($order->id, true);
			$total_resas = MyOwnReservationsUtils::getOrderTotalReservations($order->id, false);
			$resas_tax_rate = $total_resas / $total_resas_wt;
			$advance_resas_wt = $total_resas_wt - $balance_wt;
			
			$only_prod_wt = $this->total_products_wt - $total_resas_wt;
			$only_prod = $this->total_products - $total_resas_wt;
			$only_prod_taxes = $only_prod_wt - $only_prod;
			
			$advance_resas = $advance_resas_wt * $resas_tax_rate;
			$advance_resas_taxes = $advance_resas_wt - $advance_resas;
			
			//taxes
			$taxes = $this->total_paid_tax_incl - $this->total_paid_tax_excl;
			$this->total_paid_tax_incl = $order->total_paid_real;
			//workaround to display a correect tax amount
			$this->total_paid_tax_excl = $this->total_paid_tax_incl - $taxes;
			//$this->total_paid_tax_excl = Tools::ps_round(($this->total_paid_tax_incl - $advance_resas_taxes - $only_prod_taxes),_PS_PRICE_DISPLAY_PRECISION_);
			
			$order->total_paid_real = 0;
			$order->save();
		}
		return parent::add($autodate, $null_values);
	}

}

if (_PS_VERSION_ >= "1.6.0.11") {

	class OrderInvoice extends OrderInvoiceTemp
	{
		public function getProductTaxesBreakdown($order=null) {
			$taxes = parent::getProductTaxesBreakdown($order);
			foreach($taxes as &$taxe)
				foreach($this->rates as $rateKey => $rateValue) {
					if (number_format(intval($taxe['name']),0)==number_format(intval($rateKey),0)) {
						$taxe['total_amount']=$rateValue;
						$taxe['total_price_tax_excl']=$this->taxes_excl[$rateKey];
					}
				}
			return $taxes;
		}
	}

} else {

	class OrderInvoice extends OrderInvoiceTemp
	{
		public function getProductTaxesBreakdown() {
			$taxes = parent::getProductTaxesBreakdown();
			foreach($taxes as &$taxe)
				foreach($this->rates as $rateKey => $rateValue) {
					$rate=(array_key_exists('name', $taxe) ? intval($taxe['name']) : intval($taxe['rate']));
					if (number_format($rate,0)==number_format(intval($rateKey),0)) {
						$taxe['total_amount']=$rateValue;
						$taxe['total_price_tax_excl']=$this->taxes_excl[$rateKey];
					}
				}
			return $taxes;
		}
	}

}

