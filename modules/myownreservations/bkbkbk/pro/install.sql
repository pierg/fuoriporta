SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Structure de la table `ps_myownreservations_products`
--

CREATE TABLE IF NOT EXISTS `ps_myownreservations_products` (
  `id_product` int(11) NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) NOT NULL,
  `name` varchar(64) NOT NULL,
  `ids_categories` text NOT NULL,
  `ids_products` text NOT NULL,
  `ids_users` varchar(64) NOT NULL,
  `ids_notifs` varchar(64) NOT NULL,
  `productType` tinyint(1) NOT NULL,
  `reservationStartType` tinyint(1) NOT NULL,
  `reservationStartParams` varchar(32) NOT NULL,
  `reservationPeriod` int(3) NOT NULL,
  `reservationLengthParams` varchar(32) NOT NULL,
  `reservationSelType` tinyint(1) NOT NULL,
  `reservationSelPlanning` tinyint(1) NOT NULL,
  `reservationSelParams` varchar(64) NOT NULL,
  `productOccupyParams` varchar(32) NOT NULL,
  `qtyParams` varchar(32) NOT NULL DEFAULT '1',
  `timeslots` varchar(128) NOT NULL,
  `priceParams` varchar(32) NOT NULL,
  `depositAdvanceParams` varchar(32) NOT NULL,
  `availability` text NOT NULL,
  `options` varchar(32) NOT NULL,
  `email` varchar(128) NOT NULL,
  PRIMARY KEY (`id_product`)
) DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_myownreservationsprostock`
--

CREATE TABLE IF NOT EXISTS `ps_myownreservations_stock` (
  `id_stock` int(11) NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `serial` varchar(12) NOT NULL,
  `comment` text NOT NULL,
  `options` varchar(32) NOT NULL,
  PRIMARY KEY (`id_stock`)
) DEFAULT CHARSET=utf8;