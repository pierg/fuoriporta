<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnStocks {
	var $list=array();
	
	//Construct stock item list
	//-----------------------------------------------------------------------------------------------------------------
	public function __construct() {
		$stockSql = Db::getInstance()->ExecuteS("SELECT * FROM " . _DB_PREFIX_ . "myownreservations_stock");
		foreach($stockSql AS $stock) {
			$tempStock = new myOwnStock();
			$tempStock->sqlId = $stock['id_stock'];
			if (array_key_exists('enabled', $tempStock))
				$tempStock->enabled = $stock['enabled'];
			$tempStock->id_shop = $stock['id_shop'];
			$tempStock->id_product = $stock['product_id'];
			$tempStock->id_product_attribute = $stock['attribute_id'];
			$tempStock->name = $stock['name'];
			if (preg_match( "@^\d*$@i" , trim($stock['serial']))!= 0) 
				$tempStock->serial = str_pad($stock['serial'], 12, "0", STR_PAD_LEFT);
			else $tempStock->serial = $stock['serial'];
			if (array_key_exists('quantity', $tempStock))
				$tempStock->quantity = $stock['quantity'];
			$tempStock->comment = $stock['comment'];
			$this->list[$tempStock->sqlId] = $tempStock;
		}
	}
	
	public static function deleteStock($id) {
		$query='DELETE FROM ' . _DB_PREFIX_ . 'myownreservations_stock WHERE id_stock = "' . $id . '"';
		return Db::getInstance()->Execute($query);
	}
	
	public function getStockForSerial($serial) {
		$stock=null;
		if (count($this->list))
		foreach($this->list AS $stock) {
			if ($stock->serial==$serial)
				return $stock;
		}
		return null;
	}
	
	public function getStockForProduct($product_id, $product_attribute_id=0) {
		$list=array();
		if (count($this->list))
		foreach($this->list AS $stock) {
			if ($stock->id_product==$product_id && ($stock->id_product_attribute==$product_attribute_id or $product_attribute_id==0))
				$list[]=$stock;
		}
		asort($list);
		return $list;
	}
	
	public function countVirtualForProduct($product_id) {
		$id_shop = myOwnUtils::getShop();
		$listCnt=0;
		if (count($this->list)>0)
		foreach($this->list AS $stock) {
			if ($stock->id_product==$product_id 
				&& ($id_shop==0 or $stock->id_shop==$id_shop or $stock->id_shop==0))
				if ($stock->id_product_attribute==0) $listCnt++;
		}
		return $listCnt;
	}
	
	public function countStockForProduct($product_id, $product_attribute_id=0, $ignoreAttribute=false) {
		$id_shop = myOwnUtils::getShop();
		$listCnt=0;
		if (count($this->list)>0)
		foreach($this->list AS $stock) {
			if ($stock->id_product==$product_id 
				&& ($product_attribute_id==0 or $stock->id_product_attribute==$product_attribute_id or $ignoreAttribute)
				&& ($id_shop==0 or $stock->id_shop==$id_shop or $stock->id_shop==0))
				if ($stock->quantity) $listCnt+=$stock->quantity;
				else $listCnt++;
		}
		return $listCnt;
	}
	
	public function countStockForCategory($category) {
		if (intval($category)>0)
			$query = '
			SELECT *
			FROM `'._DB_PREFIX_.'category_product` cp
			WHERE cp.`id_category` = '.intval($category);
		else	
		    $query = '
		    SELECT *
		    FROM  `'._DB_PREFIX_.'product` pl';
		
		$results = Db::getInstance()->ExecuteS($query);
		
		$listCnt=0;
		foreach($results as $result) {
			if (count($this->list)>0)
			foreach($this->list AS $stock) {
				if ($stock->id_product==intval($result["id_product"]))
					$listCnt++;
			}
		}

		return $listCnt;
	}
	
	public function getAllFiltered($obj, $products, $timeslotslist, $date='', $id_timeslot='', $idFilter='', $productFilter='', $productAttributeFilter='', $nameFilter='', $serialFilter='', $reservationFilter='', $timeslotFilter='', $customerFilter='', $carrierFilter='', $paymentFilter='') {
		global $cookie;
		$_products = $obj->_products;
		$list=array();
		//$productsList = $_products->getProducts($cookie->id_lang, false);	

		foreach($products AS $product) {
			$baseStock = new myOwnStock();
			$baseStock->sqlId = 0;
			$baseStock->id_product = $product['id_product'];

			$mainProduct = $_products->getResProductFromProduct($baseStock->id_product);
			
			if ($obj->_stocks->countVirtualForProduct($baseStock->id_product)>0) 
				$attributes=array(0 => 0);
			else 
				$attributes = MyOwnReservationsUtils::getProductAttributes($baseStock->id_product);

			foreach($attributes as $attribute) {
				$baseStock->id_product_attribute = $attribute;
				
				//filter product
				if (
					($productFilter=="" or $baseStock->id_product==$productFilter) &&
					($productAttributeFilter=="" or $baseStock->id_product_attribute==$productAttributeFilter)
					) {

					$stockitems=$this->getStockForProduct($baseStock->id_product, $baseStock->id_product_attribute);
					
					//if no stock items we generate a line for each qty
					if (count($stockitems)==0) {
					
						$productQty = $mainProduct->getProductQuantity($baseStock->id_product, $baseStock->id_product_attribute);//without stock param because we wan't them
						if ($date!='') $resaLines = myOwnResas::getReservationsForProduct($obj, $timeslotslist, $baseStock->id_product, ($mainProduct->_optionIgnoreAttrStock ? -1 : $baseStock->id_product_attribute), $date, $reservationFilter, $timeslotFilter);

						//if ($date!='') $resaLines = myOwnResas::getFlatResaLines(null, $mainProduct, $baseStock->id_product, $baseStock->id_product_attribute, $date, $date, 0, 0, "", true);
						//if ($date!='') echo '#'.$baseStock->id_product.' '.count($resaLines).'<br>';
						//if filter stock empty
						if ($idFilter=="" && $nameFilter=="" && $serialFilter=="") 
							for($elem = 0; $elem<$productQty; $elem++) {
								//if ($date!='') echo '@'.$elem.':'.array_key_exists($elem, $resaLines).'<br>';
								$tempStock = clone $baseStock;
								
								if ($date!='' && array_key_exists($elem, $resaLines))
									foreach ($resaLines[$elem] as $resa)
										if (($customerFilter =='' or stripos($resa->_customerName, $customerFilter)!==false)
											&& ($carrierFilter =='' or $resa->_carrier==$carrierFilter) 
											&& ($paymentFilter =='' or $resa->_payment==$paymentFilter))
											$tempStock->_reservations[]=$resa;
								
								if ($reservationFilter==''
									or ($reservationFilter!='free' && count($tempStock->_reservations)>0) 
									or ($reservationFilter=='free' && count($tempStock->_reservations)==0) )
									$list[] = $tempStock;
							}
					//if stock with serial geting them
					} else {
						if ($date!='') $resaLines = myOwnResas::getReservationsForProduct($obj, $timeslotslist, $baseStock->id_product, ($mainProduct->_optionIgnoreAttrStock ? -1 : $baseStock->id_product_attribute), $date, $reservationFilter, $timeslotFilter);
						foreach($stockitems as $stockitem) {
							//filter stock
							if (
								($idFilter=="" or $stockitem->sqlId==$idFilter) &&
								($nameFilter=="" or stripos($stockitem->name, $nameFilter)!==false) &&
								($serialFilter=="" or stripos($stockitem->serial, $serialFilter)!==false) 
							) {

								if ($date!='')
									foreach($resaLines as $resaLine)
										foreach($resaLine as $resa) {
											$serials=explode(';', $resa->stock);
											foreach ($serials as $serial)
												if (($serial==$stockitem->serial)
													&& ($customerFilter =='' or stripos($resa->_customerName, $customerFilter)!==false)
													&& ($carrierFilter =='' or $resa->_carrier==$carrierFilter) 
													&& ($paymentFilter =='' or $resa->_payment==$paymentFilter))
													$stockitem->_reservations[$resa->sqlId]=$resa;
										}	
								if ($reservationFilter==''
									or ($reservationFilter!='free' && count($stockitem->_reservations)>0) 
									or ($reservationFilter=='free' && count($stockitem->_reservations)==0) )
									$list[] = $stockitem;
							}
						}
					}
				}
			}

		}
		return $list;
	}
	
	public static function getFiltered($_products, $products, $timeslotslist, $date, $id_timeslot, $idFilter, $productFilter, $productAttributeFilter, $nameFilter, $serialFilter) {
		$id_shop = myOwnUtils::getShop();
		$day = date('Y-m-d',$date);
		$notAfterTsReq="";$notBeforeTsReq="";
		if ($id_timeslot) {
			$notAfterTsTest = false;
			$notBeforeTsTest = true;
			foreach ($timeslotslist as $timeslot) {
				if ($notAfterTsTest) $notAfterTsReq .= "AND start_timeslot != ".$timeslot->sqlId." ";
				if ($timeslot->sqlId == $id_timeslot) {
					$notAfterTsTest = true;
					$notBeforeTsTest = false;
				}
				if ($notBeforeTsTest) $notBeforeTsReq .= "AND end_timeslot != ".$timeslot->sqlId." ";
			}
		}

		$req = "SELECT * FROM "._DB_PREFIX_."myownreservations_stock as stock
		LEFT JOIN  "._DB_PREFIX_."myownreservations_reservation ON (
		"._DB_PREFIX_."myownreservations_reservation.stock LIKE CONCAT('%', stock.serial ,'%')
		AND (start_date < '".$day."' OR (start_date = '".$day."' ".$notAfterTsReq."))
		AND (end_date > '".$day."' OR (end_date = '".$day."' ".$notBeforeTsReq.")) ";
		if ($id_shop>0) $req .= " AND ("._DB_PREFIX_."myownreservations_reservation.id_shop = ".$id_shop." OR "._DB_PREFIX_."myownreservations_reservation.id_shop = 0)";
		$req .= ") 
		WHERE stock.id_stock > 0 AND stock.product_id >= 0";
		if ($id_shop>0) $req .= " AND (stock.id_shop = ".$id_shop." OR stock.id_shop = 0)";
		
		$list=array();
		if ($idFilter!="") $req .= " AND id_stock = ".$idFilter;
		if ($productFilter!="") $req .= " AND product_id = ".$productFilter;
		if ($productAttributeFilter!="") $req .= " AND attribute_id = ".$productAttributeFilter;
		if ($nameFilter!="") $req .= " AND name LIKE '%".$nameFilter."%'";
		if ($serialFilter!="") $req .= " AND serial LIKE '%".$serialFilter."%'";

		$stockSql = Db::getInstance()->ExecuteS($req);

		foreach($stockSql AS $stock) {
			if (array_key_exists($stock['product_id'], $products)) {
				$tempStock = new myOwnStock();
				$tempStock->sqlId = $stock['id_stock'];
				$tempStock->id_product = $stock['product_id'];
				$tempStock->id_product_attribute = $stock['attribute_id'];
				$tempStock->name = $stock['name'];
				if (preg_match( "@^\d*$@i" , trim($stock['serial']))!= 0) 
					$tempStock->serial = str_pad($stock['serial'], 12, "0", STR_PAD_LEFT);
				else $tempStock->serial = $stock['serial'];
				$tempStock->comment = $stock['comment'];
				$tempStock->_id_reservation = $stock['id_reservation'];
				$tempStock->_validated = $stock['validated'];
				if (isset($stock['start_date'])) {
					$newCart = new myOwnReservation();
					$newCart->sqlId = $stock['id_reservation'];
					$newCart->id_product = $stock['product_id'];
					$newCart->id_product_attribute =  $stock['attribute_id'];
					$newCart->quantity = 1;
					$newCart->startDate = $stock['start_date'];
					$newCart->startTimeslot = $stock['start_timeslot'];
					$newCart->endDate = $stock['end_date'];
					$newCart->endTimeslot = $stock['end_timeslot'];
						
					$tempStock->_reservations[] = $newCart;
				}
				$list[$tempStock->sqlId] = $tempStock;
			}
		}
		return $list;
	}
}

?>