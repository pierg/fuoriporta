SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Contenu de la table `ps_myownreservations_timeslot`
--

INSERT INTO `ps_myownreservations_timeslot` (`id_timeSlot`, `id_family`, `name`, `day1`, `day2`, `day3`, `day4`, `day5`, `day6`, `day7`, `startTime`, `endTime`, `type`, `quota`) VALUES
(1, 2, 'midday', 1, 1, 1, 1, 1, 0, 0, '12:00:00', '15:00:00', 0, 10),
(2, 2, 'evenning', 1, 1, 1, 1, 1, 0, 0, '15:00:00', '18:00:00', 0, 10),
(3, 2, 'morning', 1, 1, 1, 1, 1, 0, 0, '08:00:00', '12:00:00', 0, 20);


INSERT INTO `ps_myownreservations_products` (`id_product`, `name`, `category`, `reservationStartType`, `reservationStartParams`, `reservationPeriod`, `reservationLengthParams`, `timeslots`, `priceParams`, `availability`, `options`, `email`, `reservationSelType`, `reservationSelPlanning`, `reservationSelParams`) VALUES
(1, 'iPods', 2, 0, '', 70, '1;1;30;0;1;1', '3;1;2;', '0;0;1;0;0', '', '0;0', '', 1, 2, ',1,1,1,1,1,0,0;1;5;d2;d28'),
(2, 'Laptops', 4, 1, '3', 10, '0;1;1;0;0;0', '3;1;2;', '0;0;0;0;0', '', '0;0', '', 0, 1, ',0,0,0,0,0,0,0;1;1;0;w1');