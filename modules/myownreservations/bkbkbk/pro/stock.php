<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnStock {
	var $sqlId;
	var $enable=1;
	var $id_product;
	var $id_product_attribute;
	var $name;
	var $serial;
	var $quantity;
	var $comment;
	var $id_shop;
	var $_reservations=array();
	//Create a stock item in database
	//-----------------------------------------------------------------------------------------------------------------
	function sqlInsert() {
		$query  = 'INSERT INTO '. _DB_PREFIX_ .'myownreservations_stock (id_shop, name, product_id, attribute_id, serial, comment, quantity, options, enabled) VALUES ('.(int)$this->id_shop.', "'.$this->name.'", '.(int)$this->id_product.', '.(int)$this->id_product_attribute.', "'.$this->serial.'", "'.$this->comment.'", "'.(int)$this->quantity.'", "'.$this->options.'", "'.(int)$this->enabled.'");';
		return Db::getInstance()->Execute($query);
	}
	
	//Update a stock item in database
	//-----------------------------------------------------------------------------------------------------------------
	function sqlUpdate() {
		$query  = 'UPDATE '. _DB_PREFIX_ .'myownreservations_stock SET name = "'.$this->name.'", enabled = "'.(int)$this->enabled.'", quantity = "'.(int)$this->quantity.'", product_id = "'.$this->id_product.'", attribute_id = "'.$this->id_product_attribute.'", serial = "'.$this->serial.'", quantity = "'.(int)$this->quantity.'",comment = "'.$this->comment.'", options = "'.$this->options.'" WHERE `id_stock` = ' . $this->sqlId . ';';
		
		return Db::getInstance()->Execute($query);
	}
	
	//Delete a stock item in database
	//-----------------------------------------------------------------------------------------------------------------
	function sqlDelete() {
		$query='DELETE FROM ' . _DB_PREFIX_ . 'myownreservations_stock WHERE id_stock = "' . $this->sqlId . '"';
		return Db::getInstance()->Execute($query);
	}
	
}

?>