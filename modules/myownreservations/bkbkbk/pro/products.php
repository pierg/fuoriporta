<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnProductsRules extends myOwnProducts {
	//Construct timeslot list
	//-----------------------------------------------------------------------------------------------------------------
	public function __construct() {
		$id_shop = myOwnUtils::getShop();
		$this->list=array();
		$sql="SELECT * FROM " . _DB_PREFIX_ . "myownreservations_products";
		if (defined('MYOWNRES_SEPARATE_SHOP') && MYOWNRES_SEPARATE_SHOP)
			$sql.=" WHERE id_shop = ".$id_shop;
		$ressourceSql = Db::getInstance()->ExecuteS($sql);
		foreach($ressourceSql AS $ressource) {
			$tempProduct = new myOwnProductsRule();
			$tempProduct->sqlId = $ressource['id_product'];
			if (array_key_exists('id_shop', $ressource))
				$tempProduct->id_shop = $ressource['id_shop'];
			$tempProduct->name = $ressource['name'];
			$tempProduct->email = $ressource['email'];
			if (trim($ressource['ids_users'])!='') $tempProduct->ids_users =  explode(',', $ressource['ids_users']);
			else $tempProduct->ids_users = array();
			if (array_key_exists('assignment', $ressource))
				$tempProduct->assignment = $ressource['assignment'];
			$tempProduct->ids_categories = implode(',', array_filter(explode(',', $ressource['ids_categories'])));
			if (isset($ressource['ids_products']))
				$tempProduct->ids_products = implode(',', array_filter(explode(',', $ressource['ids_products'])));
			if (isset($ressource['ids_notifs']))
				$tempProduct->ids_notifs = array_filter(explode(',', $ressource['ids_notifs']));
			else $tempProduct->ids_notifs = array();
			if (isset($ressource['ids_options']))
				$tempProduct->ids_options = array_filter(explode(',', $ressource['ids_options']));
			else $tempProduct->ids_options = array();

			//Planning					
			//-----------------------------------------------------------------------------------------------------------------
			$tempProduct->reservationStartType = $ressource['reservationStartType'];
			$tempProduct->reservationStartParams = $ressource['reservationStartParams'];
			$tempProduct->reservationPeriod = $ressource['reservationPeriod'];
			
			//Prodcut occupation					
			//-----------------------------------------------------------------------------------------------------------------
			if (isset($ressource['productOccupyParams']))
				$tempProduct->productOccupyParams = $ressource['productOccupyParams'];
			$productOccupyParamsTab = explode(";",$tempProduct->productOccupyParams);
			if (count($productOccupyParamsTab)>2) {
				$tempProduct->_occupyType = $productOccupyParamsTab[0];
				$tempProduct->_occupyQty = $productOccupyParamsTab[1];
				$tempProduct->_occupyCombinations = $productOccupyParamsTab[2];
			}
			if (count($productOccupyParamsTab)>3)
				$tempProduct->_occupyMaxOrder = $productOccupyParamsTab[3];
			if (count($productOccupyParamsTab)>4)
				$tempProduct->_occupyStockAssign = $productOccupyParamsTab[4];
		
			if (isset($ressource['productType']))
				$tempProduct->productType = $ressource['productType'];
			
			$tempProduct->options = $ressource['options'];
			$optionsTab = explode(";",$tempProduct->options);

			$tempProduct->_optionAlsoSell=($optionsTab[1]==1);

			if (isset($_POST['qtyCapacity']))
				$tempProduct->_qtyCapacity = intval($_POST['qtyCapacity']);

			//Reservation selection						
			//-----------------------------------------------------------------------------------------------------------------
			$tempProduct->reservationSelType = $ressource['reservationSelType'];
			$tempProduct->reservationSelPlanning = $ressource['reservationSelPlanning'];
			$tempProduct->reservationSelParams = $ressource['reservationSelParams'];
			$reservationSelParamsTab = explode(";",$tempProduct->reservationSelParams);
			if (count($reservationSelParamsTab)>3) {
				$tempProduct->_reservationSelDays=$reservationSelParamsTab[0];
				$tempProduct->_reservationSelWeekStart=intval($reservationSelParamsTab[1]);
				$tempProduct->_reservationSelWeekEnd=intval($reservationSelParamsTab[2]);
				$tempProduct->_reservationSelMonthStart=$reservationSelParamsTab[3];
				$tempProduct->_reservationSelMonthEnd=$reservationSelParamsTab[4];
			} else $tempProduct->_reservationSelDays=',1,1,1,1,1,1,1';
			$tempProduct->_reservationSelMultiple=0;
			if (count($reservationSelParamsTab)>5) $tempProduct->_reservationSelMode = intval($reservationSelParamsTab[5]);
			if (count($reservationSelParamsTab)>6) $tempProduct->_reservationShowTime=intval($reservationSelParamsTab[6]);
			else $tempProduct->_reservationShowTime=1;
			if (count($reservationSelParamsTab)>7) $tempProduct->_reservationStartTime=$reservationSelParamsTab[7];
			if (count($reservationSelParamsTab)>8) $tempProduct->_reservationEndTime=$reservationSelParamsTab[8];
			if (count($reservationSelParamsTab)>9) $tempProduct->_reservationCutTime=$reservationSelParamsTab[9];
			if (count($reservationSelParamsTab)>10) $tempProduct->_reservationTimeByDay=$reservationSelParamsTab[10];
			if (count($reservationSelParamsTab)>11) $tempProduct->_timeSliceLength=$reservationSelParamsTab[11];

			//Reservation length				
			//-----------------------------------------------------------------------------------------------------------------
			$tempProduct->reservationLengthParams = $ressource['reservationLengthParams'];
			$reservationLengthParamsTab = explode(";",$tempProduct->reservationLengthParams);
			
			if (count($reservationLengthParamsTab)>5) {
				$tempProduct->_reservationUnit=intval($reservationLengthParamsTab[0]);
				$tempProduct->_reservationMinLength=intval($reservationLengthParamsTab[1]);
				$tempProduct->_reservationMaxLength=intval($reservationLengthParamsTab[2]);
				$tempProduct->_reservationBetweenShift=intval($reservationLengthParamsTab[3]);
				$tempProduct->_allowResOnHolidays=($reservationLengthParamsTab[4]==1);
				$tempProduct->_allowResOnRestDays=($tempProduct->reservationSelType != reservation_interval::TIMESLICE
													&& ($reservationLengthParamsTab[5]==1	
														|| ($tempProduct->_reservationUnit!=reservation_unit::DAY
															&& $tempProduct->_reservationUnit!=reservation_unit::TIMESLOT) ) );
			}

			if (count($reservationLengthParamsTab)>6) $tempProduct->_reservationLengthControl=intval($reservationLengthParamsTab[6]);
			//compatibility with version without this option
			else {
				if ($tempProduct->_reservationMinLength==$tempProduct->_reservationMaxLength) {
					if ($tempProduct->_reservationMinLength == 0) $tempProduct->_reservationLengthControl = reservation_length::FREE;
					else $tempProduct->_reservationLengthControl = reservation_length::FIXED;
				} else $tempProduct->_reservationLengthControl = reservation_length::LIMIT;
			}

			if (count($reservationLengthParamsTab)>8) {
				$tempProduct->_lengthHolidays=($reservationLengthParamsTab[7]==1);
				$tempProduct->_lengthDisabledTimeSlots=($reservationLengthParamsTab[8]==1);
			} else {
				$tempProduct->_lengthHolidays=$tempProduct->_priceHolidays;
				$tempProduct->_lengthDisabledTimeSlots=$tempProduct->_priceDisabledTimeSlots;
			}
			if (count($reservationLengthParamsTab)>9) $tempProduct->_reservationLengthFreq=intval($reservationLengthParamsTab[9]);
			if (count($reservationLengthParamsTab)>10)  $tempProduct->_reservationLengthStrict=($reservationLengthParamsTab[10]==1);
			
			if (count($reservationLengthParamsTab)>11)  $tempProduct->_reservationLengthCanExceed=($reservationLengthParamsTab[11]==1);
			
			if (count($reservationLengthParamsTab)>12)  $tempProduct->_reservationLengthQuantity=($reservationLengthParamsTab[12]==1);
			
			if (count($reservationLengthParamsTab)>13)  $tempProduct->_allowResBetweenDays=($tempProduct->reservationSelType != reservation_interval::TIMESLICE && $reservationLengthParamsTab[13]==1);
			
			//Commented because now sel mode is more important than length kind
			/*if  ($tempProduct->_reservationLengthControl == reservation_length::FREE  
				 or $tempProduct->_reservationLengthControl == reservation_length::LIMIT) 
				 $tempProduct->_reservationSelMultiple=false;*/
			//Compatibility with older version paramas (old value 1 mean multiple instead there is length so became 4 otherwise)
			if ($tempProduct->_reservationSelMode===1) {
				if ($tempProduct->_reservationLengthControl == reservation_length::FREE || $tempProduct->_reservationLengthControl == reservation_length::LIMIT)
					$tempProduct->_reservationSelMode = reservation_mode::START_END;
				else $tempProduct->_reservationSelMode = reservation_mode::MULTIPLE;
			}
			$tempProduct->_reservationSelMultiple = ($tempProduct->_reservationSelMode == reservation_mode::MULTIPLE);
				 
			if ( ($tempProduct->_reservationSelMode == reservation_mode::MULTIPLE || $tempProduct->_reservationSelMode == reservation_mode::UNIQUE)
				&& ($tempProduct->_reservationLengthControl == reservation_length::FREE || $tempProduct->_reservationLengthControl == reservation_length::LIMIT)) {
				$tempProduct->_reservationLengthControl = reservation_length::FIXED;
				$tempProduct->_reservationMinLength = 1;
				$tempProduct->_reservationMaxLength = $tempProduct->_reservationMinLength;
			}
				 
			if  ($tempProduct->_reservationLengthControl == reservation_length::FREE  
				 or $tempProduct->_reservationLengthControl == reservation_length::NONE) 
				 	/*if ($tempProduct->reservationSelType<=reservation_interval::MONTH)
				 		$tempProduct->_reservationUnit=$tempProduct->reservationSelType;
				 	else */if ($tempProduct->_reservationSelExt!=null)
				 		$tempProduct->_reservationUnit=$tempProduct->_reservationSelExt->parentUnit;

			//Reservation price						
			//-----------------------------------------------------------------------------------------------------------------
			$tempProduct->priceParams = $ressource['priceParams'];
			$priceParamsTab = explode(";",$tempProduct->priceParams);
			$priceUnit=$priceParamsTab[0];

			if ($priceUnit!='') $tempProduct->_priceUnit=$priceUnit;
			else $tempProduct->_priceUnit = $tempProduct->_reservationUnit;

			$tempProduct->_priceUnitSameAsLength = ($priceUnit=='' || $tempProduct->_priceUnit == $tempProduct->_reservationUnit);
			if ($tempProduct->_reservationUnit<$tempProduct->reservationSelType && $tempProduct->reservationSelType!=5)
				$tempProduct->_reservationUnit=$tempProduct->reservationSelType;

			if (array_key_exists(1, $priceParamsTab)) $tempProduct->_priceEndTimeSlot=($priceParamsTab[1]==1);
			else $tempProduct->_priceEndTimeSlot=true;
			if (array_key_exists(2, $priceParamsTab)) $tempProduct->_priceDisabledTimeSlots=($priceParamsTab[2]==1);
			else $tempProduct->_priceDisabledTimeSlots=false;
			if (array_key_exists(3, $priceParamsTab)) $tempProduct->_priceStrict=($priceParamsTab[3]==1);
			else $tempProduct->_priceStrict=false;
			if (array_key_exists(4, $priceParamsTab)) $tempProduct->_pricePreview=($priceParamsTab[4]==1);
			else $tempProduct->_pricePreview=false;
			if (array_key_exists(5, $priceParamsTab)) $tempProduct->_priceHolidays=($priceParamsTab[5]==1);
			else $tempProduct->_priceHolidays=false;
			if (array_key_exists(6, $priceParamsTab)) $tempProduct->_reservationPriceType=(int)$priceParamsTab[6];
			else $tempProduct->_reservationPriceType=0;
			if (array_key_exists(7, $priceParamsTab)) $tempProduct->_priceDoubleIfLength=($priceParamsTab[7]==1);
			else $tempProduct->_priceDoubleIfLength=false;
			if (array_key_exists(8, $priceParamsTab)) $tempProduct->_priceCombination=($priceParamsTab[8]==1);
			else $tempProduct->_priceCombination=false;
			if (array_key_exists(9, $priceParamsTab)) $tempProduct->_coefPerProduct=($priceParamsTab[9]==1);
			else $tempProduct->_coefPerProduct=false;

			//Reservation payment				
			//-----------------------------------------------------------------------------------------------------------------
			if (isset($ressource['depositAdvanceParams']))
				$tempProduct->depositAdvanceParams = $ressource['depositAdvanceParams'];
			$depositAdvanceParamsTab = explode(";",$tempProduct->depositAdvanceParams);
			if (count($depositAdvanceParamsTab)>0) $tempProduct->_depositCalc=$depositAdvanceParamsTab[0];
			if (count($depositAdvanceParamsTab)>1) $tempProduct->_depositAmount=$depositAdvanceParamsTab[1];
			if (count($depositAdvanceParamsTab)>2) $tempProduct->_advanceRate=$depositAdvanceParamsTab[2];
			else $tempProduct->_advanceRate=100;
			if (count($depositAdvanceParamsTab)>3) $tempProduct->_advanceAmount=$depositAdvanceParamsTab[3];
			if (count($depositAdvanceParamsTab)>4) $tempProduct->_depositByProduct=intval($depositAdvanceParamsTab[4]);
			
			//Product Qty			
			//-----------------------------------------------------------------------------------------------------------------
			$tempProduct->qtyParams = $ressource['qtyParams'];
			$tempProduct->_qtyCapacity=1;
			$tempProduct->_qtyDisplay=0;
			$tempProduct->_qtyType=0;
			$tempProduct->_qtyFixed=1;
			
			$quantityTab = explode(";",$tempProduct->qtyParams);
			if (count($quantityTab)>0) $tempProduct->_qtyCapacity=intval($quantityTab[0]);
			if (count($quantityTab)>1) $tempProduct->_qtyDisplay=intval($quantityTab[1]);
			if (count($quantityTab)>2) $tempProduct->_qtyType=intval($quantityTab[2]);
			if (count($quantityTab)>3) $tempProduct->_qtyFixed=intval($quantityTab[3]);
			if (count($quantityTab)>4) $tempProduct->_qtySearch=intval($quantityTab[4]);
			if (!$tempProduct->_qtyFixed) $tempProduct->_qtyFixed=1;
			
			$tempProduct->_optionIgnoreAttrStock=( $tempProduct->_qtyType == reservation_qty::SEARCH || $tempProduct->productType==product_type::MUTUEL or ($tempProduct->productType==product_type::VIRTUAL 
												&& ($tempProduct->_occupyCombinations == occupy_combinations::SUM || $tempProduct->_occupyCombinations==occupy_combinations::DEF)) or ($tempProduct->productType==product_type::STOCK 
												&& $tempProduct->_occupyCombinations == occupy_combinations::SUM));
												
			
			//others				
			//-----------------------------------------------------------------------------------------------------------------
			$tempProduct->options = $ressource['options'];
			$optionsTab = explode(";",$tempProduct->options);
			if (count($optionsTab)>1) {
				$tempProduct->_optionAlsoSell=($optionsTab[1]==1);
			}
			if (count($optionsTab)>2 && $optionsTab[2]!='') $tempProduct->_optionValidationStatus = $optionsTab[2];
			else $tempProduct->_optionValidationStatus = Configuration::get('MYOWNRES_RES_STATUS');
			
			$tempProduct->timeslots = $ressource['timeslots'];
			$tempProduct->_timeslotsTab = array();
			$timeslots = explode(";", $tempProduct->timeslots);
  				foreach($timeslots AS $timeslot) {
      				if (intval($timeslot)>0)
          				$tempProduct->_timeslotsTab[] = intval($timeslot);
  				}
			$tempProduct->availability = $ressource['availability'];
			
			$tempProduct->_timeslotsObj = new myOwnTimeSlots();
			$tempProduct->_timeslotsObj->availabilities = $tempProduct->availability;
			$tempProduct->_timeslotsObj->list = $tempProduct->getTimeslots();

			$this->list[$tempProduct->sqlId] = $tempProduct;
		}
	}
	
	public function getGlobal($type=true, $ishome=false) {
		if (count($this->list)==1) {
			foreach($this->list as $mainProduct)
				return $mainProduct;
		}
		$ids_categories=array();
		$ids_products=array();
		$mergeDays = array(1=>0,2=>0,3=>0,4=>0,5=>0,6=>0,7=>0);
		$interval = array (
			reservation_interval::TIMESLICE => 0,	
			reservation_interval::TIMESLOT => 0,
			reservation_interval::DAY => 0,
			reservation_interval::WEEK => 0,
			reservation_interval::MONTH => 0);
		$allowResOnHolidays=true;
		$allowResOnRestDays=true;
		$start=-1;
		$reservationPeriod=-1;
		$defaultTimeSlot = myOwnTimeSlots::getDefault(0);
		
		//merging params
		foreach ($this->list as $prodFamilly) {
			//merge products and famillies
			if ($ishome) {
				$ids_categories = array_merge ($ids_categories, array_filter(explode(',', $prodFamilly->ids_categories)));
				$ids_products = array_merge ($ids_products, array_filter(explode(',', $prodFamilly->ids_products)));
			}
			//merge resa window params
			$tempStart=$prodFamilly->getReservationStart();
			if ($start==-1 or $start>$tempStart) 
				$start = $tempStart;
			if ($reservationPeriod==-1 or $reservationPeriod<$prodFamilly->reservationPeriod)
				$reservationPeriod = $prodFamilly->reservationPeriod;
			//count selTypes
			if ($prodFamilly->_reservationSelExt!=null) {
				if (!array_key_exists((int)$prodFamilly->_reservationSelExt->unit, $interval))
					$interval[(int)$prodFamilly->_reservationSelExt->unit]=1;
				else $interval[(int)$prodFamilly->_reservationSelExt->unit]++;
			} else 
				$interval[$prodFamilly->reservationSelType]++;
			
			$allowResOnHolidays &= $prodFamilly->_allowResOnHolidays;
			$allowResOnRestDays &= $prodFamilly->_allowResOnRestDays;
			//merge days available

			foreach ($mergeDays as $dayKey => $dayValue)
				if ($prodFamilly->_timeslotsObj->areTimeSlotsEnableOnDayOfWeek($dayKey, $type))
					$mergeDays[$dayKey]=1;
		}
		$mergedProdFamilly = new myOwnPlanning();

		//slice if all slice
		if ($interval[reservation_interval::TIMESLICE] == count($this->list))
			$mergedProdFamilly->reservationSelType = reservation_interval::TIMESLICE;
		else if ($interval[reservation_interval::TIMESLOT] > 0) { //== count($this->list)
			$mergedProdFamilly->reservationSelType = reservation_interval::TIMESLOT;
			//merge timeslots
			$mergedts = array();
			$ids_categories=array();
			$ids_products=array();
			foreach ($this->list as $prodFamilly)
				if ($prodFamilly->_reservationSelExt==null && $prodFamilly->reservationSelType == reservation_interval::TIMESLOT) {
					//$mergedts = array_merge ($mergedts, $prodFamilly->_timeslotsObj->list);
					$mergedts += $prodFamilly->_timeslotsObj->list;
					$mergedProdFamilly->_mergedFamilies[] = $prodFamilly->sqlId;
					$mergedProdFamilly->_mergedNames[$prodFamilly->sqlId] = $prodFamilly->name;
					$ids_categories = array_merge ($ids_categories, array_filter(explode(',', $prodFamilly->ids_categories)));
					$ids_products = array_merge ($ids_products, array_filter(explode(',', $prodFamilly->ids_products)));
				}
		} else {
			$mergedProdFamilly->reservationSelType = reservation_interval::DAY;
			
			$mergedProdFamilly->_reservationUnit = $mergedProdFamilly->reservationSelType;
			$defaultTimeSlot->days = $mergeDays;
			foreach ($this->list as $prodFamilly)
				if ($prodFamilly->_reservationSelExt==null && $prodFamilly->reservationSelType == reservation_interval::DAY) {
					//$mergedts = array_merge ($mergedts, $prodFamilly->_timeslotsObj->list);
					$mergedProdFamilly->_mergedFamilies[] = $prodFamilly->sqlId;
					$mergedProdFamilly->_mergedNames[$prodFamilly->sqlId] = $prodFamilly->name;
					$ids_categories = array_merge ($ids_categories, array_filter(explode(',', $prodFamilly->ids_categories)));
					$ids_products = array_merge ($ids_products, array_filter(explode(',', $prodFamilly->ids_products)));
				}
		}

		//$prodFamilly->_reservationUnit=intval($reservationLengthParamsTab[0]);
		$mergedProdFamilly->_reservationSelDays = implode(",", $mergeDays);
		$mergedProdFamilly->ids_categories = implode(",", $ids_categories);
		$mergedProdFamilly->ids_products = implode(",", $ids_products);
		$mergedProdFamilly->sqlId = -1;
		$mergedProdFamilly->__reservationStart = $start;
		$mergedProdFamilly->reservationPeriod = $reservationPeriod;
		
		$mergedProdFamilly->_reservationShowTime = false;
		$mergedProdFamilly->_reservationBetweenShift = 0;
		$mergedProdFamilly->_reservationMinLength = 0;
		$mergedProdFamilly->_reservationMaxLength = 0;
		$mergedProdFamilly->_reservationLengthControl = reservation_length::FREE;
		$mergedProdFamilly->_timeslotsObj = new myOwnTimeSlots();
		if ($mergedProdFamilly->reservationSelType == reservation_interval::TIMESLOT) {
			$mergedProdFamilly->reservationSelPlanning = reservation_view::WEEK;
			$mergedProdFamilly->_timeslotsObj->list = $mergedts; 
		} else if ($mergedProdFamilly->reservationSelType==reservation_interval::DAY) {
			$mergedProdFamilly->reservationSelPlanning = reservation_view::MONTH;
			$mergedProdFamilly->_timeslotsObj->list = array(0=>$defaultTimeSlot);
		} else {
			$mergedProdFamilly->reservationSelPlanning = reservation_view::WEEK;
		}

		return $mergedProdFamilly;
	}

}

?>