{*
* 2010-2017 LaBulle
*
* NOTICE OF LICENSE
*
* This source file is subject to Terms and Conditions of Use 
* Available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade module to newer
* versions in the future. If you wish to customize module for your
* needs please refer to http://www.labulle.net for more information.
*
* @author    laBulle.net <info@labulle.net>
* @copyright 2010-2017 laBulle
* @license   http://www.labulle.net/download/CGV-en.pdf
*}
{if !$myOwnPriceDisplay}
<style type="text/css">
span.price,
span.ajax_block_products_total,
span.ajax_cart_shipping_cost,
span.ajax_block_cart_total,
#our_price_display,
div.cart-prices,
span.product-price,
tr.cart_total_price {
	display:none !important;
}
</style>
{/if}
<script type="text/javascript">
var myOwnCartLink='{$cartLink}';
var myOwnPriceFrom='{l s='From' mod='myownreservations'}';
{literal}
	//checkout steps mgmt
	//--------------------------------------------------------------------------------------
	function hideOpcStep1() {
	    $("#opc_account").remove();
		$("h2").each(function(index) {
			if (isLogged && $(this).html().indexOf("1") != -1) $(this).hide();
		});
	}
	function hideOpcStep2() {
		$("#carrierTable").hide();
		
		$(".carrier_title").remove();
		$(".delivery_options_address").remove();
		var html = $("#opc_delivery_methods").html();
		$("#carrier_area").after('<div><h2><span>2</span>&nbsp;{/literal}{l s='Order Informations' mod='myownreservations'}{literal}</h2><div id="opc_informations" class="opc-main-block">'+html+'</div></div>');
		$("#carrier_area").remove();
		$('#cgv').on('click', function(e){
			updatePaymentMethodsDisplay();
		});
				
		$("h2").each(function(index) {
			if ($(this).html().indexOf("2.") != -1) {
				$(this).html("{/literal}{l s='2. ORDER INFORMATIONS' mod='myownreservations'}{literal}");
				updateCarrierSelectionAndGift();
			}
		});
		$(".carrier_title").remove();
	}
	function prepareHideOpcStep1() {
		if ($("#button_order_cart")!=null) {
    		var htmlStr = $("#button_order_cart").parent().html();
    		{/literal}
	        htmlStr += '<form name="myOwnReservationSkipStepA" action="{$orderLink}" method="post"><input type="hidden" name="cgv" value="1" ><input type="hidden" name="id_carrier" value="{$defaultCarrier}"><input type="hidden" name="processCarrier" value="1"></form>';
	        {literal}
	        $("#button_order_cart").parent().html( htmlStr );
			$("#button_order_cart").attr("href", "javascript:document.forms.myOwnReservationSkipStepA.submit();");
	    }
	}
	function hideStdStep1() {
		if ($('input[name="processAddress"]')!=null) {
    		var cont = $('input[name="processAddress"]').parent();
    		cont.children("input").remove();
    		var htmlStr = cont.html();
    		{/literal}
    		htmlStr += '</form><form name="myOwnReservationSkipStepB" action="{$orderLink}&step=3" method="post"><input type="hidden" name="step" value="3" ><input type="hidden" name="cgv" value="1" ><input type="hidden" name="id_carrier" value="{$defaultCarrier}"><input type="hidden" name="processCarrier" value="1"><a href="javascript:document.forms.myOwnReservationSkipStepB.submit();" class="exclusive">{l s='Next' mod='myownreservations'} »</a>';
    		{literal}
    		cont.html(htmlStr);
    	}
	}
	function hideStdStep() {
		if ($(".cart_navigation")!=null) {
    		var htmlStr = '';//$(".cart_navigation").html();
    		{/literal}
	        htmlStr += '<form name="myOwnReservationSkipStepB" action="{$orderLink}&step={$nextStep}" method="post"><input type="hidden" name="step" value="{$nextStep}" ><input type="hidden" name="cgv" value="1" ><input type="hidden" name="id_carrier" value="{$defaultCarrier}"><input type="hidden" name="processCarrier" value="1"></form>';
	        {literal}
	        $(".cart_navigation").after( htmlStr );
	        if ($(".standard-checkout").length ) $(".standard-checkout").attr("href","javascript:document.forms.myOwnReservationSkipStepB.submit();");
	        else $(".cart_navigation a").eq(1).attr("href","javascript:document.forms.myOwnReservationSkipStepB.submit();");
	    }
	}
	function changePreviousStep() {
		if ($(".cart_navigation")!=null) {
    		var htmlStr = '';//$(".cart_navigation").html();
    		{/literal}
	        htmlStr += '<form name="myOwnReservationSkipPrevious" action="{$orderLink}&step={$previousStep}" method="post"><input type="hidden" name="step" value="{$previousStep}" ></form>';
	        {literal}
	        $(".cart_navigation").after( htmlStr );
	        $(".cart_navigation a").eq(0).attr("href","javascript:document.forms.myOwnReservationSkipPrevious.submit();");
	    }
	}
	function loginStep() {
		{/literal}
		$('#login_form input[name="back"]').remove();
		$("#login_form").append('<input type="hidden" name="back" value="{$orderLink}&step=3&cgv=1&id_carrier={$defaultCarrier}&processCarrier=1">');
		{literal}
	}
	function addressCreationStep(step) {
		{/literal}
		$('#add_address input[name="back"]').val("order.php?step="+step);
		{literal}
	}
	
	function setOrderSkipDeliveryBtn() {
		var id_address=$('input[name="id_address_delivery"]').val();
		var id_carrier={/literal}{$defaultCarrier}{literal};
		alert(id_address+' '+id_carrier);
		var btn_label = $('button[name="confirm-addresses"]').html();
		$('button[name="confirm-addresses"]').remove();
		var form = '<form class="clearfix" id="js-delivery" data-url-update="commande?ajax=1&action=selectDeliveryOption" method="post"><input type="hidden" name="id_address_delivery" value="'+id_address+'"><input type="hidden" name="delivery_option['+id_address+']" id="delivery_option_'+id_carrier+'" value="'+id_carrier+',"/><button type="submit" class="continue btn btn-primary pull-xs-right" name="confirmDeliveryOption" value="1">'+btn_label+'</button></form>';
		$('#checkout-addresses-step .content').append(form);
	}
	
	function orderSkipAddressStep () {
		$("#checkout-addresses-step").hide();
		$(".step-number").html('&nbsp;');
	}
	
	//price and button display
	//--------------------------------------------------------------------------------------
	function hideCartButtons() {
		$(".right_block > .exclusive").css("display","none");
		$("#product_list > li > div > .ui-block-b").css("display","none");
	}
	function hidePriceDisplay() {
    	$(".right_block .price, .center_block .price").each(function(index) {
    		$(this).html("");
    	});
	}
	{/literal}
	{if $_PS_VERSION_<"1.6"}
	{literal}
	function setSessionPrices() {
		$(".exclusive").each(function(index) {
			if (typeof(productResaPrice)!='undefined') {
				for (id_product in productResaPrice) {
					if ($(this).attr("rel")=="ajax_id_product_"+id_product) {
						if (productResaPrice[id_product]=='-1') {
							$(this).parent().parent().contents().find(".availability").html("{/literal}{l s='Period not valid for this product' mod='myownreservations'}{literal}");
							$(this).after('<span class="exclusive">{/literal}{l s='Add to cart' mod='myownreservations'}{literal}</span>');
						} else if (productResaPrice[id_product]=='') {
							$(this).parent().parent().contents().find(".availability").html("{/literal}{l s='Not available for selected period' mod='myownreservations'}{literal}");
							$(this).after('<span class="exclusive">{/literal}{l s='Add to cart' mod='myownreservations'}{literal}</span>');
						} else {
							{/literal}
							$(this).parent().parent().contents().find(".availability").html("{l s='Available for selected period' mod='myownreservations'}");
							{if $addToCartOnList && $isResaSession}
							$(this).after('<a class="exclusive ajax_add_resa_to_cart_button" onclick="myownreservationCallCart('+id_product+', '+productDefaultAttr[id_product]+', 1, \'{$potentialResa->startDate}_{$potentialResa->startTimeslot}@{$potentialResa->endDate}_{$potentialResa->endTimeslot}\', this);">{l s='Add to cart' mod='myownreservations'}</a>');
							{/if}
							{literal}
						}
						{/literal}
						{if $myOwnPriceDisplay}
							if (productResaPrice[id_product]!='-1') $(this).parent().contents().find(".price").html(productResaPrice[id_product]);
						{else}
							$(this).parent().contents().find(".price").remove();
						{/if}
						$(this).remove();
						{literal}
					}
				}
			}
		});
	}
	{/literal}
	{/if}
	{literal}
	function myOwnReservationDislay() {
	{/literal}
		{if $myOwnPriceType==0}
			setGlobalUnits{if $_PS_VERSION_>="1.6"}16{/if}();
		{else}
			setMinPrices{if $_PS_VERSION_>="1.6"}16{/if}();
		{/if}
		
	{if $isResaSession && ($myownpage!='category' or $isCatOfSession or $_PS_VERSION_>="1.6") && $myownpage!='product'}
		{if $_PS_VERSION_<"1.6"}
		setSessionPrices();
		{else}
		setSessionPrices16();
		{/if}
	{/if}
	{if !$myOwnPriceDisplay}
		hidePriceDisplay();
	{/if}
		{literal}
	}
	
	//--------------------------------------------------------------------------------------
	document.addEventListener("DOMContentLoaded", function(event) {
		{/literal}
		//alert('{$myownpage}');
		{if $myownpage=='order-opc' && !$stdCheckout}
			{if $hideStep1}hideOpcStep1();{/if}
			{if $hideStep2}hideOpcStep2();{/if}
		{/if}
		{if $myownpage=='address' && $hideStep2}
			addressCreationStep(3);
		{/if}
		{if $myownpage=='order' && $stdCheckout}
			{if $_PS_VERSION_<"1.7"}
				{if $hideStep1}
					$("#order_step li").eq(2).css("display","none");
					prepareHideOpcStep1();
					{if $currentStep==1}hideStdStep1();{/if}
					/*{if $currentStep==0}$(".order_delivery").hide();{/if}*/
				{/if}
				{if $hideStep2}
					$("#order_step li").eq(3).css("display","none");
				{/if}
				{if $hideStep1 or $hideStep2}
					{if $currentStep>0}changePreviousStep();{/if}
					{if $currentStep==0 && $islogged}hideStdStep();{/if}
				{/if}
			{else}
				//setOrderSkipDeliveryBtn();
				{if $hideStep1 && $cust_id_address_delivery}orderSkipAddressStep();{/if}
			{/if}
		{/if}
		{if $hideStep1 && $myownpage=='authentication' && $stdCheckout}
		loginStep();
		{/if}
		myOwnReservationDislay();
		{literal}
	});
	document.addEventListener("DOMContentLoaded", function(event) {
		$(document).ajaxComplete(function() {
			myOwnReservationDislay();
		});
	});
{/literal}
</script>
