{*
* 2010-2017 LaBulle
*
* NOTICE OF LICENSE
*
* This source file is subject to Terms and Conditions of Use 
* Available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade module to newer
* versions in the future. If you wish to customize module for your
* needs please refer to http://www.labulle.net for more information.
*
* @author    laBulle.net <info@labulle.net>
* @copyright 2010-2017 laBulle
* @license   http://www.labulle.net/download/CGV-en.pdf
*}
{if !$ourtheme}
<section id="dashresass" class="panel widget">
	<header class="panel-heading">
		<i class="icon-bar-chart"></i> {l s='Check-In/Out' mod='myownreservations'}
	</header>

	<div class="tab-content">
			<div class="table-responsive">
				<table class="table data_table" id="table_checkinout">
{else}
<section class="box-typical box-typical-dashboard panel-default scrollable lobipanel lobipanel-sortable" data-inner-id="TgHPFYq8uc" data-index="0">
	                    <header class="box-typical-header panel-heading ui-sortable-handle">
	                        <span class="panel-title">{l s='Check-In/Out' mod='myownreservations'}</span>
							<div class="dropdown"><ul class="dropdown-menu dropdown-menu-right"><li><a data-func="editTitle" data-tooltip="Edit title" data-original-title="" title=""><i class="panel-control-icon glyphicon glyphicon-pencil"></i><span class="control-title">Edit title</span></a></li><li><a data-func="unpin" data-tooltip="Unpin"><i class="panel-control-icon glyphicon glyphicon-move"></i><span class="control-title">Unpin</span></a></li><li><a data-func="reload" data-tooltip="Reload"><i class="panel-control-icon glyphicon glyphicon-refresh"></i><span class="control-title">Reload</span></a></li><li><a data-func="minimize" data-tooltip="Minimize"><i class="panel-control-icon glyphicon glyphicon-minus"></i><span class="control-title">Minimize</span></a></li><li><a data-func="expand" data-tooltip="Fullscreen"><i class="panel-control-icon glyphicon glyphicon-resize-full"></i><span class="control-title">Fullscreen</span></a></li><li><a data-func="close" data-tooltip="Close"><i class="panel-control-icon glyphicon glyphicon-remove"></i><span class="control-title">Close</span></a></li></ul><div class="dropdown-toggle" data-toggle="dropdown"><span class="panel-control-icon glyphicon glyphicon-cog"></span></div>
	                    	</div>
	                    </header>
	                    <div class="box-typical-body">
	                    	<table class="tbl-typical">
{/if}	


					<thead>
						<tr>
							<th class="text-left"><div>{l s='Customer' mod='myownreservations'}</div></th>
							<th class="text-center"><div>{l s='Way' mod='myownreservations'}</div></th>
							<th class="text-center"><div>{l s='Time' mod='myownreservations'}</div></th>
							<th class="text-center"><div>{l s='Products' mod='myownreservations'}</div></th>
							<th class="text-center"><div>{l s='Order' mod='myownreservations'}</div></th>
							<th class="text-right"></th>
						</tr>
					</thead>
					<tbody>
						{foreach $reservations as $reservation}
						<tr>
							<td class="text-left"><a href="">{$reservation->_customerName}</a></td>
							<td class="{if $day==$reservation->startDate && $day==$reservation->endDate}text-center{else}{if $day==$reservation->startDate}text-left{else}text-right{/if}{/if}">{if $day==$reservation->startDate && $day==$reservation->endDate}<i class="icon-arrow-circle-down"></i> {l s='In & Out' mod='myownreservations'} <i class="icon-arrow-circle-up"></i>{else}{if $day==$reservation->startDate}<i class="icon-arrow-circle-down"></i> {l s='In' mod='myownreservations'}{else}{l s='Out' mod='myownreservations'} <i class="icon-arrow-circle-up"></i>{/if}{/if}</td>
							<td class="text-center">{if $day==$reservation->startDate && $day==$reservation->endDate}{$reservation->startTime|truncate:5:"":true} - {$reservation->endTime|truncate:5:"":true}{else}{if $day==$reservation->startDate}{$reservation->startTime|truncate:5:"":true}{else}{$reservation->endTime|truncate:5:"":true}{/if}{/if}</td>
							<td class="text-center">{$reservation->quantity}</td>
							<td class="text-center">{$reservation->_order->reference}</td>
							<td class="text-right"> <a class="btn btn-default" onclick="showResaBox({$reservation->sqlId});showResaDetails({$reservation->sqlId});" title="Details"><i class="icon-search"></i> </a></td>
						</tr>
						{/foreach}
					</tbody>
{if !$ourtheme}
				</table>
			</div>
		</div>

</section>
{else}
</table>
	                    	
	                    </div>
</section>
{/if}

