{*
* 2010-2017 LaBulle
*
* NOTICE OF LICENSE
*
* This source file is subject to Terms and Conditions of Use 
* Available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade module to newer
* versions in the future. If you wish to customize module for your
* needs please refer to http://www.labulle.net for more information.
*
* @author    laBulle.net <info@labulle.net>
* @copyright 2010-2017 laBulle
* @license   http://www.labulle.net/download/CGV-en.pdf
*}
{if $_PS_VERSION_>="1.6" && $_PS_VERSION_<"1.7"}<div class="{$planningType}">{/if}
	<div id="idTabResa">
		<div id="primary_block">
			<div id="myOwnReservationproductLoading" style="display:none" class="myOwnReservationLoading"><img src="{$my_img_dir}/loader.gif"></div>
			<div id="myOwnReservationproductContent" class="myOwnReservationContent" type="{$planningType}">{$planning}</div>
		</div>
	</div>
{if $_PS_VERSION_>="1.6" && $_PS_VERSION_<"1.7"}</div>{/if}
<link href="{$module_dir}views/css/planning.css" rel="stylesheet" type="text/css" media="all" />