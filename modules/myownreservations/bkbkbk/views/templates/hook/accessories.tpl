{if isset($accessories) && $accessories}
	<!--Accessories -->
		<h2 class="page-product-heading">{l s='Get this accessories for same period' mod='myownreservations'}</h2>
		<div class="block products_block accessories-block clearfix">
			<div class="block_content">
				<ul id="bxslider" class="bxslider clearfix">
					{foreach from=$accessories item=accessory name=accessories_list}
						{if ($accessory.allow_oosp || $accessory.quantity_all_versions > 0 || $accessory.quantity > 0) && $accessory.available_for_order && !isset($restricted_country_mode)}
							{assign var='accessoryLink' value=$myownlink->getProductLink($accessory.id_product, $accessory.link_rewrite, $accessory.category)}
							<li class="item product-box ajax_block_product{if $smarty.foreach.accessories_list.first} first_item{elseif $smarty.foreach.accessories_list.last} last_item{else} item{/if} product_accessories_description" style="margin-right: 20px;">
								<div class="product_desc">
									<a href="{$accessoryLink|escape:'html':'UTF-8'}" title="{$accessory.legend|escape:'html':'UTF-8'}" class="product-image product_image">
										<img class="lazyOwl" src="{$myownlink->getImageLink($accessory.link_rewrite, $accessory.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{$accessory.legend|escape:'html':'UTF-8'}" {if isset($homeSize)}width="{$homeSize.width}" height="{$homeSize.height}"{/if}/>
									</a>
									<div class="block_description">
										<a href="{$accessoryLink|escape:'html':'UTF-8'}" title="{l s='More' mod='myownreservations'}" class="product_description">
											{$accessory.description_short|strip_tags|truncate:25:'...'}
										</a>
									</div>
								</div>
								<div class="s_title_block">
									<h5 class="product-name">
										<a href="{$accessoryLink|escape:'html':'UTF-8'}">
											{$accessory.name|truncate:20:'...':true|escape:'html':'UTF-8'}
										</a>
									</h5>
									{if $accessory.show_price && !isset($restricted_country_mode) && !$PS_CATALOG_MODE}
									<span class="price">
										{if $accessories_potentialresas[$accessory.id_product]!=null}
											{assign var='resa' value=$accessories_potentialresas[$accessory.id_product]}
											{displayWtPrice p=$resa->getPrice()}
										{/if}
									</span>
									{/if}
								</div>
								<div class="clearfix" style="margin-top:5px">
									{if !$PS_CATALOG_MODE && $accessories_potentialresas[$accessory.id_product]!=null}
										<div class="no-print">
											<a class="exclusive button" onclick="myownreservationCallCart({$resa->id_product}, {$resa->id_product_attribute}, {$resa->quantity}, '{$resa->startDate}_{$resa->startTimeslot}@{$resa->endDate}_{$resa->endTimeslot}', $('.crossseling'));" data-id-product="{$accessory.id_product|intval}" title="{l s='Book' mod='myownreservations'}">
												<span>{l s='Book' mod='myownreservations'}</span>
											</a>
										</div>
									{/if}
								</div>
							</li>
						{/if}
					{/foreach}
				</ul>
			</div>
		</div>
	<!--end Accessories -->
{/if}
