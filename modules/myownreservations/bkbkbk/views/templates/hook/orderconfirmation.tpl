{*
* 2010-2017 LaBulle
*
* NOTICE OF LICENSE
*
* This source file is subject to Terms and Conditions of Use 
* Available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade module to newer
* versions in the future. If you wish to customize module for your
* needs please refer to http://www.labulle.net for more information.
*
* @author    laBulle.net <info@labulle.net>
* @copyright 2010-2017 laBulle
* @license   http://www.labulle.net/download/CGV-en.pdf
*}
{if $showExt}
<link href="{$module_dir}/views/css/orderConf.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript">
	{literal}
 	    document.addEventListener("DOMContentLoaded", function(event) {
 	{/literal}
	 	    {if $order_total_paid_real_num < $order_total_paid_num}
	 		{literal}
	 	    	$(".price, .order-confirmation-table td, #content-hook_payment_return dd").each(function( index ) { //
			{/literal}
						if ($(this).html().indexOf("{$order_total_paid}")>=0) $(this).html("{$order_total_paid_real}");
						if ($(this).html().indexOf("{$order_total_paid_num}")>=0) $(this).html("{$order_total_paid_real}");
			{literal}
				});
			{/literal}
				{if $_PS_VERSION_>"1.7"}
					$(".order-confirmation-table table:last").before("<table><tbody><tr><td>{l s='Reservations' mod='myownreservations'} {$advance_title}</td><td>{$resas_advance}</td></tr><tr><td>{l s='Total purchases' mod='myownreservations'}</td><td>{$order_total_purchases}</td></tr></tbody></table><hr/>");
				
					$(".myOwnUnit").parents(".order-line").remove();
				{/if}
			{/if}
	{literal}
 		});
 	{/literal}
</script>
{else}
<style>
{$myownstyle}
</style>
{/if}
<div id="viewed-resa" class="block products_block resas_conf{if $_PS_VERSION_>="1.7"}_new{/if}">
	{if $_PS_VERSION_<"1.7"}
	<p class="title_block" style="font: 600 18px/22px "Open Sans", sans-serif;color: #555454;background: #f6f6f6;border-top: 5px solid #333;text-transform: uppercase;padding: 14px 5px 17px 20px;margin-bottom: 20px;">{l s='Your reservations' mod='myownreservations'} {if !$showExt} {l s='for order' mod='myownreservations'} {$order_name}{/if}</p>
	{else}
		<h3 class="card-title h3">{l s='Your reservations' mod='myownreservations'} {if !$showExt} {l s='for order' mod='myownreservations'} {$order_name}{/if}</h3>
	{/if}
	<div class="block_content">
		<ul class="products clearfix" style="list-style: none;">
{foreach from=$reservations key=resaKey item=reservation name=resaLoop}
{if isset($productsImages[$reservation->id_product])}
	{assign var='viewedProduct' value=$productsImages[$reservation->id_product]}
{else}
	{assign var='viewedProduct' value=null}
{/if}
				<li class="clearfix first_item" style="display: list-item;padding:10px 0;border-bottom:1px dotted #ccc;">
					<a style="float: left;" {if $viewedProduct!=null}href="{$myownlink->getProductLink($viewedProduct->id, $viewedProduct->link_rewrite, $viewedProduct->category_rewrite)}"{/if} class="content_img" title="{$viewedProduct->name|escape:html:'UTF-8'}"><img src="{$myownlink->getImageLink($viewedProduct->link_rewrite, $viewedProduct->cover, {$imageTypeR->name})}" height="{$imageTypeR->height}" width="{$imageTypeR->width}" alt="{$viewedProduct->legend|escape:html:'UTF-8'}" /></a>
					<div class="text_desc" style="float: left;margin-left: 10px;">
						<p class="s_title_block" style="{if $_PS_VERSION_<"1.7"}font-size: 12px;color: #333;font-weight: bold;{/if}"><a {if $viewedProduct!=null}href="{$myownlink->getProductLink($viewedProduct->id, $viewedProduct->link_rewrite, $viewedProduct->category_rewrite)}"{/if} title="{$viewedProduct->name|escape:html:'UTF-8'}">{$viewedProduct->name|escape:html:'UTF-8'}</a>{if $resas_attributes[$viewedProduct->id]!=''} - {$resas_attributes[$viewedProduct->id]}{/if}</p>
						<p class="resa_period" style="line-height: 14px;color: #666;font-weight: bold;font-size: 14px;">{$reservation->toString($myownreservations, $id_lang, true)}
						</p>
						{$resaDescription[$reservation->sqlId] nofilter}
					</div>
					{if $showExt}<div class="text_desc">{$resaExtention[$reservation->sqlId] nofilter}</div>{/if}
					<div style="clear:both"></div>
				</li>
{/foreach}
		</ul>
	</div>
	{if $showExt}
	<div style="margin-top:15px">{foreach from=$globalExtentions item=globalExtention}{$globalExtention nofilter}{/foreach}</div>
	{/if}
</div>
