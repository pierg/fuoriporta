{*
* 2010-2017 LaBulle
*
* NOTICE OF LICENSE
*
* This source file is subject to Terms and Conditions of Use 
* Available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade module to newer
* versions in the future. If you wish to customize module for your
* needs please refer to http://www.labulle.net for more information.
*
* @author    laBulle.net <info@labulle.net>
* @copyright 2010-2017 laBulle
* @license   http://www.labulle.net/download/CGV-en.pdf
*}
{if $myPlanningBox==1}
<link href="{$module_dir}views/css/planning.css" rel="stylesheet" type="text/css" media="all" />
{/if}
<script type="text/javascript">
var myOwnResAlsoSell={$myOwnResAlsoSell};
var myOwnPriceDisplay={$myOwnPriceDisplay};
var myOwnPriceFrom='{l s='From' mod='myownreservations'}';
var myHomeSelection='{$myHomeSelection}';
var myHomePage='{$myHomePage}';
var imgDir='{$imgDir}';
var quantity = 1;
{if $myPlanningBox==1}
var myPlanningBox = true;
{else}
var myPlanningBox = false;
{/if}
{literal}
function myownreservationAttrChange() {
	myownreservationActions();
	//set -1 to last arg to indicate a first load
	myOwnGetSelector('product').showView(-1, 'Product');
	//myProductSelector.showView(-1);
}

function initLocationChange(time)
{
	if(!time) time = 500;
	setInterval(myOwnCheckUrl, time);
}
function myOwnCheckUrl() {
	var my_original_url=original_url;
	checkUrl();
	if (my_original_url != window.location) {
		myownreservationActions();
		my_original_url=window.location;
	}
}
function myOwnGetSelector(target) {
	var temp = window["my"+target+"Selector"];
	if (typeof(temp)!='undefined') return temp;
	else return myOwnSelector;
}

function myownreservationActions () {
{/literal}
		var idCombination = {$defaultcombinaison}
		{if $_PS_VERSION_<"1.7"}
			idCombination = $('#idCombination').val();
		{else}
			if (history.state!=null) idCombination = history.state.id_product_attribute;
		{/if}
		var myowntab = "idTab1";
		{if $_PS_VERSION_>="1.7"}myowntab = $("#idTabResa").parent().attr('id');{/if}
		var adchtml = '';
		if (idCombination=='') idCombination=0;
		if (typeof productHasAttributes != "undefined" && productHasAttributes && selectedCombination['unavailable']) idCombination=-1;
		$("#add_to_cart").html('');
		var label = "{l s='Purchase' mod='myownreservations'}";
		{if $_PS_VERSION_<"1.6"}
		if (myOwnResAlsoSell) $("#add_to_cart").append( '<input type="submit" name="Submit" value="'+label+'" class="exclusive">');
		{else}
		if (myOwnResAlsoSell) $("#add_to_cart").append( '<button type="submit" name="Submit" class="exclusive"><span>'+label+'</span></button>');
		{/if}
		
		{if $myOwnResWay}
		if ($("#myOwnResWay").length==0)
			$(".product_attributes").append('<fieldset class="attribute_fieldset" id="myOwnResWay"><div class="attribute_list"><ul><li><div class="radio"><span class="checked"><input type="radio" class="myownresway_radio" name="myownresway" value="start" checked="checked"></span></div><span><i class="icon-long-arrow-right"></i> {$myOwnResLblStart}</span></li><li><div class="radio"><span><input type="radio" class="myownresway_radio" name="myownresway" value="stop"></span></div><span><i class="icon-long-arrow-left"></i> {$myOwnResLblEnd}</span></li><li><div class="radio"><span><input type="radio" class="myownresway_radio" name="myownresway" value="both"></span></div><span><i class="icon-exchange"></i> {$myOwnResLblStart}/{$myOwnResLblEnd}</span></li></ul></div></fieldset>');
			myOwnGetSelector('product').resWay='start';
		{/if}
		{if $myOwnResIgnoreProdQty}
		$("#quantity_wanted_p").empty();
		$("#quantityAvailable").hide();
		$("#quantityAvailableTxt").hide();
		$("#quantityAvailableTxtMultiple").hide();
		{/if}
		
		{if $myPlanningBox==0}
		if ($("#idTabResa").length) {
			$("#add_to_cart, .product-add-to-cart .add-to-cart").append('<a href="javascript:showTab(\'idTabResa\');goToResaTabScroll(\'{$productLink}\');" class="button">{l s='Select a period' mod='myownreservations'}</a>');
			$(".product-add-to-cart .add-to-cart").attr("data-button-action", "").attr("onclick", "javascript:showTab('"+myowntab+"');goToResaTabScroll('{$productLink}');return false;").html('<i class="material-icons grid_on">event</i>{l s='Select a period' mod='myownreservations'}');
		}
		{/if}
		{if $myPlanningBox==1}
			{if $_PS_VERSION_<"1.7"}
			$("#add_to_cart, .product-add-to-cart .add-to-cart").append('<a onclick="showPlanningBox();myOwnGetSelector(\'product\').showView(-1, \'Product\', -1);" class="button">{l s='Select a period' mod='myownreservations'}</a>');
			{else}
			$(".product-add-to-cart .add-to-cart").attr("data-button-action", "").attr("onclick", "showPlanningBox();myOwnGetSelector('product').showView(-1, 'Product', -1);return false;").html('<i class="material-icons grid_on">event</i>{l s='Select a period' mod='myownreservations'}');
			{/if}
		{/if}
		var priceItem = $("#our_price_display, .current-price span");
		{if $myOwnPriceType==0}
		if (priceItem.length && priceItem.html().indexOf("/{$myOwnResPriceUnit}") == -1) {
			if (!myOwnResAlsoSell && "{$myOwnResPriceUnit}"!=" ") priceItem.append("/{$myOwnResPriceUnit}");
		}
		var oldPriceItem = $("#old_price_display");
		if (oldPriceItem.length && oldPriceItem.html().indexOf("/{$myOwnResPriceUnit}") == -1) {
			if (!myOwnResAlsoSell && "{$myOwnResPriceUnit}"!=" ") oldPriceItem.append("/{$myOwnResPriceUnit}");
		}
		{else}
		if (typeof(productAttrMinPrice[idCombination])!='undefined') {
			if (priceItem.find("span.myownresPrice").length==0)
				priceItem.html('<span class="myownresPrice" style="font-size:10px;">'+myOwnPriceFrom+' </span>'+productAttrMinPrice[idCombination]);
		}
		{/if}
{if $isResaSession}
{literal}
		//actions if resas dates in session 
		if (productAttrAvailable[idCombination]==0) {
			$("#pQuantityAvailable").css("display","none");
			$("#availability_value, #product-availability").html('{/literal}{if $_PS_VERSION_>="1.7"}<i class="material-icons product-unavailable">&#xE14B;</i>{/if}{l s='Not available' mod='myownreservations'} {literal}'+productAttrLabel[idCombination]);
			$("#availability_value").removeClass("label-success");
			$("#availability_value").addClass("label-danger");
			adchtml = '<i class="material-icons grid_on">event</i>{/literal}{l s='Change period' mod='myownreservations'}{literal}';
		} else {
			{/literal}
			var label='';
			if (myOwnResAlsoSell) label="{l s='Reserve product' mod='myownreservations'}";
			else label='{l s='Add to cart' mod='myownreservations'}';
			{if $_PS_VERSION_>="1.7"}
			$(".product-add-to-cart .add-to-cart").attr("data-button-action", "").attr("onclick", "myownreservationCallCart({$id_product}, $('#idCombination').val(), $('#quantity_wanted').val(), '{$potentialResa->startDate}_{$potentialResa->startTimeslot}@{$potentialResa->endDate}_{$potentialResa->endTimeslot}', this);return false;");//.append('<i class="material-icons grid_on">event</i>{l s='Select a period' mod='myownreservations'}');
			adchtml = '';//<a id="reserve" class="exclusive button ajax_add_resa_to_cart_button" onclick="">'+label+'</a>';
			{else if $_PS_VERSION_<"1.6"}
			adchtml += '<a id="reserve" class="exclusive button ajax_add_resa_to_cart_button" onclick="myownreservationCallCart({$id_product}, $(\'#idCombination\').val(), $(\'#quantity_wanted\').val(), \'{$potentialResa->startDate}_{$potentialResa->startTimeslot}@{$potentialResa->endDate}_{$potentialResa->endTimeslot}\', this);\">'+label+'</a>';
			{else}
			adchtml += '<button id="reserve" class="exclusive ajax_add_resa_to_cart_button" onclick="myownreservationCallCart({$id_product}, $(\'#idCombination\').val(), $(\'#quantity_wanted\').val(), \'{$potentialResa->startDate}_{$potentialResa->startTimeslot}@{$potentialResa->endDate}_{$potentialResa->endTimeslot}\', this);event.stopPropagation(); event.preventDefault();\"><span>'+label+'</span></button>';
			{/if}
			$("#pQuantityAvailable").css("display","block");
			$("#availability_value, #product-availability").html('{if $_PS_VERSION_>="1.7"}<i class="material-icons product-available">&#xE5CA;</i>{/if}{l s='Available' mod='myownreservations'}'+productAttrLabel[idCombination]);
			$("#last_quantities").css("display","block");
			$(".myOwnResQuantityAvailable").html(productAttrQty[idCombination]);
			{literal}
		}
{/literal}
		var selLabel ="{l s='Purchase' mod='myownreservations'}";
		{if $_PS_VERSION_<"1.6"}
		if (myOwnResAlsoSell) adchtml += '<input type="submit" name="Submit" value="'+selLabel+'" class="exclusive">';
		{else}
		if (myOwnResAlsoSell) adchtml += '<button type="submit" name="Submit" class="exclusive"><span>'+selLabel+'</span></button>';
		{/if}
		else {
			$("#our_price_display, .current-price span").html(productAttrPrice[idCombination]);
			$("#old_price_display, span.regular-price").html(productAttrOldPrice[idCombination]);
		}
		showTab(myowntab);
		if (adchtml!='') $("#add_to_cart, .product-add-to-cart .add-to-cart").html(adchtml);
		//if (adchtml=='') $(".product-add-to-cart .add-to-cart").remove();
		$("#availability_statut").css("display","block");
		$("#last_quantities").css("display","none");
{else}
		//actions if no resas dates in session 
		$("#pQuantityAvailable").css("display","none");
		$('.modal-body .product-add-to-cart').remove();
{/if}
		if (!myOwnPriceDisplay) $("p.price").remove();
		
		$(".accessories-block a.exclusive, .accessories-block a.ajax_add_to_cart_button").each(function() {
			var id_accessoire = $(this).attr("data-id-product");
			if (typeof(productLinkedAvailable)!='undefined' && typeof(productLinkedAvailable[id_accessoire])!='undefined') {
				if (productLinkedAvailable[id_accessoire]) {
					$(this).html("<span>{l s='Reserve product' mod='myownreservations'}</span>");
					$(this).attr("onclick", productLinkedAction[id_accessoire]);
					$(this).attr("href", "");
				} else {
					$(this).remove();
				}
				$(this).parent().parent().parent().find("span.price").html(productLinkedPrice[id_accessoire]);
				
			} else {
				$(this).remove();
			}
		});
{literal}
}
{/literal}{if !$isquickview}{literal}document.addEventListener("DOMContentLoaded", function(event) {{/literal}{/if}{literal}
	$(window).trigger('planning'); 
	keepAddToCart();
	{/literal}
	{if $myOwnResIgnoreProdQty}
	{literal}
	quantityAvailable=999999;
	if (typeof(combinations)!='undefined')
		$.each(combinations, function(key, combination)
		{
			combination['quantity']=999999;
		});
	{/literal}
	{/if}
	{literal}
	$('#buy_block').bind("keyup keypress", function(e) {
	  var code = e.keyCode || e.which; 
	  if (code  == 13) {               
	    e.preventDefault();
	    return false;
	  }
	});
{/literal}
{if $isResaSession}
	$("#quantityAvailable").addClass("myOwnResQuantityAvailable");
	$("#pb-left-column .price, .our_price_display").before("<span class='myOwnSelPeriod'>{$resaSessionString}</span>");
	/*$("#pQuantityAvailable").append("<span class='myOwnSelPeriod'>{$resaSessionString}</span>");*/
	if ($("#idTabResa").length) {
	//
		{if $_PS_VERSION_<"1.6"}
		$("#add_to_cart").after('<p class="buttons_bottom_block"><a href="javascript:showTab(\'idTabResa\');goToResaTabScroll();" class="button">{l s='Change period' mod='myownreservations'}</a></p>');
		{else}
		$("#add_to_cart").after('<p class="buttons_bottom_block"><a href="javascript:showTab(\'idTabResa\');goToResaTabScroll();" class="button lnk_view btn btn-default"><span>{l s='Change period' mod='myownreservations'}</span></a></p>');
		{/if}
	}
{/if}
{if $_PS_VERSION_<"1.7"}
{literal}
	//attributes change
	if (typeof(updateDisplay)=='undefined') {
		$("#attributes :input").change(function () {
			findCombination();
			myownreservationActions();
			myOwnGetSelector('product').showView(-1, 'Product', -1);
		});
	
		$("#attributes :input, .color_pick").click(function () {
			myownreservationActions();
			myOwnGetSelector('product').showView(-1, 'Product', -1);
		});
	}
{/literal}
{/if}
{literal}
	//quantity change
	$("#quantity_wanted").keyup(function () {
		if (typeof(myOwnResQty)!='undefined' && $(this).val() != "" && myOwnResQty != $(this).val()) {
			myOwnSelList.length=0;
			myOwnResQty = $(this).val();
	      	myOwnGetSelector('product').showView(-1, 'Product', -1);
	  	}
	});
	
	$("#quantity_wanted").change(function () {
		if (typeof(myOwnResQty)!='undefined' && $(this).val() != "" && myOwnResQty != $(this).val()) {
			myOwnSelList.length=0;
			myOwnResQty = $(this).val();
	      	myOwnGetSelector('product').showView(-1, 'Product', -1);
	  	}
	});
	
	$("#myownr_object").change(function () {
		myownreservationActions();
		myOwnGetSelector('product').showView(-1, 'Product', -1);
	});

{/literal}
	{if $_PS_VERSION_>="1.7"}
		
	prestashop.on("updatedProduct", function() { 
	    myownreservationActions();
	    myOwnGetSelector('product').showView(-1, 'Product', -1);
	});
	prestashop.on("updatedCart", function() { 
	    myownreservationActions();
	    myOwnGetSelector('product').showView(-1, 'Product', -1);
	});
	{/if}
{literal}
	
	myownreservationActions();
	
	$(".myownresway_radio").click(function () {
		var resWay = $("input[name=myownresway]:checked").val();
		myOwnGetSelector('product').resWay=resWay;
		myOwnGetSelector('product').showView(-1, 'Product', -1);
	});
	myOwnGetSelector('product').showView(-1, 'Product', -1);
{/literal}{if !$isquickview}{literal}});{/literal}{/if}

</script>
