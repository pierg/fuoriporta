{foreach from=$quartersList key=quarter item=monthsList name=quartersLoop}
<center>
<table width="100%" id="myOwn{$planningStyle}CalendarTop" class="myOwnCalendarTop">
	<tr>
	{if $reservationView->homeProducts!=array()}
	    <td class="myOwnCalendarProduct myOwnCalendarTopItem"></td>
	{/if}
	{foreach from=$monthsList key=monthkey item=month name=monthsLoop}
        <td class="myOwnCalendarTopItem">
        {$month->toString($id_lang)}
        </td>    
	{/foreach}
	{math equation="x - y" x=$nbMonths y=$monthsList|@count assign='leftMonths'}
    {if $leftMonths>0}{section name=waistsizes start=0 loop=$leftMonths step=1}<td></td>{/section}{/if}
	<tr/>
</table>
<table width="100%" id="myOwn{$planningStyle}MonthsCalendarLine" class="myOwnCalendarLine">
        <tr>
        	{include file=$planning_dayline cell_days=array() linetype="sel" isDaySel=true}
            {foreach from=$monthsList key=monthkey item=month name=monthsLoop}
            {assign var='day' value=$month->getDaySelection()}
            <td class="myOwnCalendarLineItem">
	            {include file=$planning_cell}
            </td>
            {/foreach}
            {math equation="x - y" x=$nbMonths y=$monthsList|@count assign='leftMonths'}
            {if $leftMonths>0}{section name=waistsizes start=0 loop=$leftMonths step=1}<td class="myOwnCalendarLineItem"></td>{/section}{/if}
        <tr/>
</table>
</center>
{/foreach}