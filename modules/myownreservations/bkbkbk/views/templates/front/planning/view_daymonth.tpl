{foreach from=$reservationMonth->_weeks item=week name=weeksLoop}
	{assign var='lastEndTime' value=""}
      {if $smarty.foreach.weeksLoop.first}
        <table width="100%" id="myOwn{$planningStyle}CalendarTop" class="myOwnCalendarTop" planning="{$planningType}">
    		<tr>
    			 {if ((isset($weeksList) && $weeksList|@count>1) or isset($reservationMonth))}
		         {include file=$planning_dayline cell_class="myOwnCalendarTopItem" cell_content="getDayString" cell_days=$week->_days linetype="top"}
		         {else}
		         {include file=$planning_dayline cell_class="myOwnCalendarTopItem" cell_content="formatDateShort" cell_days=$week->_days linetype="top"}
		         {/if}
    		</tr>
        </table>
       {/if} 
       {if !($smarty.foreach.weeksLoop.first && $reservationMonth->isStartInvisible($isWeekEndDays)) && $reservationView->start < $week->getEnd()}
       {if !$isDaySel or $widget}
    	<table width="100%" id="myOwn{$planningStyle}CalendarHeadLine" class="myOwnCalendarHeadLine">
    		<tr>
    			{include file=$planning_dayline cell_class="myOwnCalendarHeadLineItem" cell_content="getDayOfMonth" cell_days=$week->_days linetype=""}
    		</tr>
        </table>
        {/if}
        {if !$isDaySel}
        <table width="100%" id="myOwn{$planningStyle}MonthCalendarLine" class="myOwnCalendarLine" planning="{$planningType}">
            {foreach from=$timeList key=timeListTime item=timeListStr name=timeListLoop}
                {*{include file=$planning_timeline last_line=false cell_days=$week->_days}*}
            	<tr>
            		{include file=$planning_dayline cell_class="myOwnCalendarLineItem" cell_content="" cell_days=$week->_days linetype="sel"}
                </tr>
                {*{if $smarty.foreach.timeSlotsLoop.last}
                	{include file=$planning_timeline last_line=true cell_days=$week->_days}
                {/if}*}
            {/foreach}
    	</table>
    	{else}
    	<table width="100%" id="myOwn{$planningStyle}MonthCalendarLine" class="myOwnCalendarLine" planning="{$planningType}">
            {foreach from=$timeSlotList item=timeSlot name=timeSlotsLoop}
                {include file=$planning_timeline last_line=false cell_days=$week->_days}
            	<tr>
            		{include file=$planning_dayline cell_class="myOwnCalendarLineItem" cell_content="" cell_days=$week->_days linetype="sel"}
                </tr>
                {if $smarty.foreach.timeSlotsLoop.last}
                	{include file=$planning_timeline last_line=true cell_days=$week->_days}
                {/if}
            {/foreach}
    	</table>
    	{/if}
    	{/if}
{/foreach}