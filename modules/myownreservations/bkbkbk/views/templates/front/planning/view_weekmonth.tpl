{foreach from=$reservationMonths key=month item=reservationMonth name=monthsLoop}
<center>
<table width="100%" id="myOwn{$planningStyle}CalendarTop" class="myOwnCalendarTop">
	<tr>
		{if $reservationView->homeProducts!=array()}
		    <td class="myOwnCalendarProduct myOwnCalendarTopItem"></td>
		{/if}
        <td class="myOwnCalendarTopItem">
        {$reservationMonth->toString($id_lang)}
        </td>
	<tr/>
</table>
{assign var='lastEndTime' value=""}
{foreach from=$timeSlots key=timeSlotKey item=timeSlot name=timeSlotsLoop}
<table width="100%" id="myOwn{$planningStyle}WeekMonthCalendarLine" class="myOwnCalendarLine">
        <tr>
        	{if $timeSlotKey>0}
        		<td class="myOwnCalendarHours">{$calendar->formatTime($timeSlot->startTime,$id_lang)}</td>
        	{/if}
        	
        	{include file=$planning_dayline cell_days=array() linetype="sel" isDaySel=true}
        	{if $isMultipleMonth && $reservationView->weeks|@count<5}<td class="myOwnCalendarLineItem"></td>{/if}
            {foreach from=$reservationMonth->_weeks key=week item=reservationWeek name=weeksLoop}
            	{assign var='day' value=$reservationWeek->getDaySelection()}
                <td class="myOwnCalendarLineItem" {if $isMultipleMonth && $reservationView->weeks|@count<5}colspan="2"{/if}>
                    {include file=$planning_cell}
                </td>
            {/foreach}
            {if $isMultipleMonth && $reservationView->weeks|@count<5}<td class="myOwnCalendarLineItem"></td>{/if}
        <tr/>
        
        {if $timeSlotKey>0}
        	<td class="myOwnCalendarHours">{$calendar->formatTime($timeSlot->endTime,$id_lang)}</td>
        	{foreach from=$reservationMonth->_weeks key=week item=reservationWeek name=weeksLoop}
        	<td></td>
        	{/foreach}
        {/if}
</table>
{/foreach}
</center>
{/foreach}