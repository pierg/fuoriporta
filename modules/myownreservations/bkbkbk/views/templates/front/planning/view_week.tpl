{if $isWeekEndDays}{assign var='daycount' value=7}{else}{assign var='daycount' value=5}{/if}
{if !$isDaySel}{math assign='width' equation="round((490-(10*x))/x)" x=$daycount}{else}{math assign='width' equation="round((530-(10*x))/x)" x=$daycount}{/if}
{if !$isDaySel}{math assign='cellwidth' equation="round(490/x)" x=$daycount}{else}{math assign='cellwidth' equation="round(530/x)" x=$daycount}{/if}
{foreach from=$weeksList key=weekKey item=week name=weeksLoop}
{assign var='lastEndTime' value=""}
<table width="100%" id="myOwn{$planningStyle}CalendarTop" class="myOwnCalendarTop" planning="{$planningType}">
	<tr>
		{if ($isDaySel OR $timeSlotList|@count == 1) && $planningType!='home'}
		{include file=$planning_dayline cell_class="myOwnCalendarTopItem" linetype="top" cell_content="getDayString" cell_days=$week->_days}
		{else}
        {include file=$planning_dayline cell_class="myOwnCalendarTopItem" linetype="top" cell_content="formatDateShort" cell_days=$week->_days}
        {/if}
	<tr/>
</table>
<table width="100%" id="myOwn{$planningStyle}WeekCalendarLine" class="myOwnCalendarLine" planning="{$planningType}">
	<tbody>
    {if !$isDaySel}
	    {foreach from=$timeList key=timeListTime item=timeListStr name=timeListLoop}
	        {*{include file=$planning_timeline last_line=false cell_days=$week->_days}*}
	        <tr>
	        	{include file=$planning_dayline cell_class="myOwnCalendarLineItem" linetype="line" cell_days=$week->_days  linetype="sel"}
	        </tr>
	      
	        {*{if $smarty.foreach.timeListLoop.last}
				{include file=$planning_timeline last_line=true cell_days=$week->_days}
			{/if}*}
			{*{assign var='lastEndTime' value=$timeSlot->endTime}*}
	    {/foreach}
	{else}
	    {foreach from=$timeSlotList item=timeSlot name=timeSlotsLoop}
	        {include file=$planning_timeline last_line=false cell_days=$week->_days}
	        <tr>
	        	{include file=$planning_dayline cell_class="myOwnCalendarLineItem" linetype="line" cell_days=$week->_days  linetype="sel"}
	        </tr>
	    {/foreach}
    {/if}
	</tbody>
</table>
{/foreach}


