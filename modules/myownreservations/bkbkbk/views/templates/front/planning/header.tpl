<style type="text/css">
.myOwnUnvalidate { display:none };
</style>

<table width="100%" id="myOwn{$planningStyle}CalendarHeader" class="myOwnCalendarHeader">
	{if ($potentialResa->_selType=="end")}
	  {assign var='resStartTimeSlot' value=$potentialResa->getStartTimeSlot()}
	  <tr {if $planningType=="column" || $planningStyle=="topproduct"}style="border-bottom:none"{/if}>
	    <td colspan="{if $planningType=="column" || $planningStyle=="topproduct"}2{else}2{/if}">
	    	{if $planningType=="column" || $planningStyle=="topproduct"}
	    	 <h5 class="myOwnH5" style="font-weight:normal;height:16px;" label="{l s='From' mod='myownreservations'}"><label style="margin-right:10px">{l s='From' mod='myownreservations'}</label> {$calendar->formatDateWithMonthShort($potentialResa->getStartDateTime(),$id_lang)}{if $resStartTimeSlot!=null && $resStartTimeSlot->sqlId>0 && $potentialResa->showTime()} {$calendar->formatTime($resStartTimeSlot->startTime,$id_lang)}{/if}</h5>
	    	{/if}
	    	{if $planningType=="product" && $planningStyle!="topproduct"}
			<h3 class="myOwnH3" style="font-weight:normal;"><b>{$myOwnResLblStart}</b> : {$calendar->formatDateWithDay($potentialResa->getStartDateTime(),$id_lang)}{if $resStartTimeSlot->sqlId>0  && $potentialResa->showTime()} {$calendar->formatTime($resStartTimeSlot->startTime,$id_lang)}{/if}</h3>
	      	{/if}
	    </td> 
	    {if $planningType!="home"}
	    <td style="text-align:right" align="right" style="vertical-align:top" {if $planningType=="column"}width="40"{/if}>
	      <a  tabindex="3" id="myOwnColumnReset" class="button {if $_PS_VERSION_>="1.6"}{if $_PS_VERSION_>="1.7"}btn-tertiary{else}lnk_view btn btn-default btn{/if}{/if}" onclick="my{$planningSelector}Selector.change();return false;" title="Change"><span  style="font-size: 14px;padding:3px;{if $_PS_VERSION_<"1.7"}padding-left:7px;padding-right:7px{/if}">{if $planningType=="column"}{if $_PS_VERSION_>="1.7"}<i class="material-icons" style="display:inline">event</i> {/if}{l s='Edit' mod='myownreservations'}{else}{l s='Change' mod='myownreservations'}{/if}</span></a>
	    </td> 
	    {/if}
	  </tr>
	{/if}
	
	  <tr>
	  	<td colspan="2"> {*{if $planningType=="column"}3{else}2{/if}*}
	  	    {if $planningType=="product"}
	  	    	{capture name='my_module_tempvar'}{l s='Select #way# of #reservationLabel#' mod='myownreservations'}{/capture}
				{assign var='selectionLabel' value=$smarty.capture.my_module_tempvar}
				{assign var='wayLabel' value=""}
				{if $myOwnResWay && $potentialResa->_selType==""}
					{if $potentialResa->_selTypeNum=="1"}
						{assign var='wayLabel' value=$myOwnResLblStart}
					{/if}
					{if $potentialResa->_selTypeNum=="2"}
						{assign var='wayLabel' value=$myOwnResLblEnd}
					{/if}
				{else}
					{if $potentialResa->_selType=="start"}
						{assign var='wayLabel' value=$myOwnResLblStart}
					{/if}
					{if $potentialResa->_selType=="end"}
						{assign var='wayLabel' value=$myOwnResLblEnd}
					{/if}
				{/if}
				<h3 class="myOwnH3" 
				style="font-weight:normal;" 
				id="{$potentialResa->_selType}productSel" 
				label = "{$wayLabel}" 
				defLabel = "{$selectionLabel|replace:'#reservationLabel#':$reservationLabel|replace:'#way#':$wayLabel}" >
					
					{if $potentialResa->_selType=="" && !$myOwnResWay}
						{if $reservationView->multiSel}<b>{l s='Select periods of your' mod='myownreservations'}{else}{l s='Select period of your' mod='myownreservations'}{/if} {$reservationLabel}</b>
					{else}
						<b>{$selectionLabel|replace:'#reservationLabel#':$reservationLabel|replace:'#way#':$wayLabel}</b>
					{/if}
	
				</h3>
				{if $reservationView->ext_object != null && $potentialResa->showObject()}
				<h3 class="myOwnH3" 
				style="font-weight:bold;display:none;padding-bottom:0px" id="{$potentialResa->_selType}productObj">
				{$reservationView->ext_object->label} :
				</h3>
				{/if}
			 {/if}
			 {if $planningType=="column"}
				<h5 class="myOwnH5" 
				style = "font-weight:normal;height:16px;" id = "{$potentialResa->_selType}{$planningSelector}Sel" 
				{if $potentialResa->_selType=="start"}
					label = "{l s='From' mod='myownreservations'}" defLabel = "{l s='From' mod='myownreservations'}" 
				{/if}
				{if $potentialResa->_selType=="end" && !$lengthWidget}
					label = "{l s='To' mod='myownreservations'}" defLabel = "{l s='To' mod='myownreservations'}" 
				{/if}
				{if $potentialResa->_selType=="end" && $lengthWidget}
					label = "{l s='During' mod='myownreservations'}" defLabel = "{l s='During' mod='myownreservations'}" 
				{/if}>
				<label style="margin-right:10px">
				{if $potentialResa->_selType=="start"}{l s='From' mod='myownreservations'}{/if}
				{if $potentialResa->_selType=="end" && !$lengthWidget}{l s='To' mod='myownreservations'}{/if}
				{if $potentialResa->_selType=="end" && $lengthWidget}{l s='During' mod='myownreservations'}{/if}
				{if $potentialResa->_selType==""}{l s='At' mod='myownreservations'}{/if}
				</label>
				</h5>
			 {/if}
	  	</td>
	  	{if $planningType!="column"}
	  	<td align="right" style="text-align:right;" valign="top">
	  		{if !$widget} 
		        {if $potentialResa->_selType=="start"}
		        {if $_PS_VERSION_>="1.7"}
		        	<button disabled id="myOwnUnvalidate" class="btn btn-primary myOwnUnvalidate">{l s='Validate' mod='myownreservations'} {$myOwnResLblStart}</button>
		        {else}
		        	<span id="myOwnUnvalidate" class="exclusive myOwnUnvalidate">{l s='Validate' mod='myownreservations'} {$myOwnResLblStart}</span>
		        {/if}
		        <a tabindex="4"  id="myOwnValidate" class="myOwnValidate {if $_PS_VERSION_>="1.6"}button btn btn-default btn-primary{/if}" style="display:none" onclick="my{$planningSelector}Selector.validateProductStart();goToResaTabScroll();"><span>{l s='Validate' mod='myownreservations'} {$myOwnResLblStart}</span></a>
		        {/if}
		        {if $potentialResa->_selType=="end" or $potentialResa->_selType==""}
		        	{capture assign="add_label"}{if $extra_step!=''}{l s='Validate' mod='myownreservations'}{else}{l s='Add to cart' mod='myownreservations'}{/if}{/capture}
			        {if $_PS_VERSION_>="1.7"}
			        	<button disabled id="myOwnUnvalidate" class="exclusive myOwnUnvalidate btn btn-primary">{$add_label}</button>
			        {else}
			        	<span id="myOwnUnvalidate" class="exclusive myOwnUnvalidate btn btn-primary">{$add_label}</span>
			        {/if}
			        <a id="myOwnValidate" class="myOwnValidate {if $_PS_VERSION_>="1.6"}button btn btn-default btn-primary{/if}" style="display:none" onclick="{if $extra_step!=''}{$extra_step}{else}myownreservationAddToCart(this);{/if}"><span>{$add_label}</span></a>
		        {/if}
	        {/if}
		</td>
		{else}
		<td style="text-align:right" align="right" width="50%">
			<a tabindex="0" id="myOwnColumnEdit" class="button {if $_PS_VERSION_>="1.6"}{if $_PS_VERSION_>="1.7"}btn-tertiary{else}lnk_view btn btn-default{/if}{/if}" onclick="my{$planningSelector}Selector.change();$(this).hide();return false;" title="Change" style="float:left;display:none;width:100%;cursor:pointer"><span style="font-size: 14px;padding:3px;padding-left:7px;padding-right:7px;text-align:center">{if $_PS_VERSION_>="1.7"}<i class="material-icons" style="display:inline">event</i> {/if}{l s='Edit' mod='myownreservations'}</span></a>
		</td>
		{/if}
	  </tr>
	  
	  {if $reservationView->multiSel && $planningType!="column"}
	  <tr>
	  	<td colspan="3">
	  		<h4><div class="selectDelete" style="display:none" onclick="my{$planningType}Selector.empty();"></div><div class="selectedlegend"></div></h4>
	  	</td>
	  </tr>
	  {/if}
</table>