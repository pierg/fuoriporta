<div style="overflow:scroll;{if $planningSelector=="column"}max-height:200px{/if}" id="myOwnCalendarLengths">
<table width="100%" id="myOwn{$planningStyle}ListCalendarLine" class="myOwnCalendarLine">
{foreach from=$lengthSlices item=slice key=LengthVal name=daysLoop}
        <tr>
            <td class="myOwnCalendarLineItem">
            {if !$slice->isAvailable}
				<div class="holidaySlot lengthSlot {$planningType}Slot"><span class="unslotLabel"><span class="timeLabel">{$slice->label}</span>{if $planningType!='topcolumn' && $planningType!='column'}{if !$hidePrice && $potentialResa->showPrice()}<br/>{/if}{if $potentialResa->showQuantity()}<br/>{/if}{/if}</span></div>
			{else}
	            {if !$slice->isEnable}
					<div class="unavailableSlot lengthSlot {$planningType}Slot"><span class="unslotLabel">{$slice->label}</span></div>
				{else}
					{assign var='timeSlotAvailable' value=($slice->availableQuantity >= $potentialResa->getOccupationQty() or $slice->availableQuantity <0)}
	            	<div {if $timeSlotAvailable}
	            		length="{$LengthVal}"
	            		label="{$slice->label}"
	            		key="{$potentialResa->startDate}_{$potentialResa->startTimeslot}!{$LengthVal}"
	            		onclik="my{$planningSelector}Selector.lengthSelection($(this));"
	            		onclick="my{$planningSelector}Selector.slotSelection(this);"
						onmouseover="my{$planningSelector}Selector.hoverlight(this, true);"
						onmouseout="my{$planningSelector}Selector.hoverlight(this, false);"
						class="{if $slice->css!=""}{$slice->css}{/if}lengthSlot availableSlot {$planningType}Slot"
						name="{if $slice->css!=""}{$slice->css}{else}availableSlot{/if}"
			    	 {else}
				    	class="unavailableSlot {$planningType}Slot"
				    	name="unavailableSlot"
			    	{/if}
			    	{if $slice->object!=null}
			    		objectValue="{$slice->object->sqlId}"
			    		objectLabel="{$slice->object->name}"
				     {/if}
					><span class="timeLabel">{$slice->label}</span>
						{if $planningType!='topcolumn' && $planningType!='column'}
							{if $slice->label!=''}<br/>{/if}
				    		{if !$hidePrice && $potentialResa->showPrice()}
				        		<span class="{if $slice->isReduc}reductionLabel{/if}">
				        			 {if ($potentialResa->_selType=="start")}
				        				<span class="estimateLabel">{$previewPriceLabel}</span>
				        			 {/if}
				        			 {convertPrice price=$slice->price}</span>
				        	{/if}
				        	{if $potentialResa->showQuantity()}
				        		<br/><span class="quantityLabel">{if $slice->availableQuantity == 0}{l s='not' mod='myownreservations'} {$previewQuantityLabel}{else}{if $slice->availableQuantity >0}{$slice->availableQuantity}{/if} {if $slice->availableQuantity >1}{$previewQuantityLabels}{else}{$previewQuantityLabel}{/if}{/if}</span>
				        	{/if}
				        	{if $slice->object!=null}
				        	<br/><span class="objectLabel">{$reservationView->ext_object->label} : {$slice->object->name}</span>
				        	{/if}
				        {/if}
			    </div>
			    {/if}
			{/if}
            <td>
        </tr>
{/foreach}
</table>
</div>
