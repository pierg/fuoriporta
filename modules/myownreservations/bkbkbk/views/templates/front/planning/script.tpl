
<script type="text/javascript" name="planning" defer>
var my{$planningSelector}Selector;
{if !$isajaxloaded}{literal}document.addEventListener("DOMContentLoaded", function(event) {{/literal}{/if}
	if (typeof(my{$planningSelector}Selector)=='undefined') 
		my{$planningSelector}Selector = Object.create(myOwnSelector);;
	my{$planningSelector}Selector.target='{$planningSelector}';
	my{$planningSelector}Selector.selType='{$potentialResa->_selType}';

	{if $planningSelector=="column" || $planningSelector=="topcolumn"}
		my{$planningSelector}Selector.setFamily({$potentialResa->_mainProduct->sqlId});
		my{$planningSelector}Selector.uniqueSel = {($potentialResa->_mainProduct->_reservationSelMode == reservation_mode::MULTIPLE || $potentialResa->_mainProduct->_reservationSelMode == reservation_mode::UNIQUE)|intval};
	{else}
		my{$planningSelector}Selector.multiSel={$multiSel};
	{/if}
{if $potentialResa->endDate==""}
	my{$planningSelector}Selector.setStartSelection();
{/if}
{if $potentialResa->_selType=="end"}
	my{$planningSelector}Selector.submitedStart = '{$potentialResa->startDate}_{$potentialResa->startTimeslot}';
{/if}

	{if $potentialResa->endDate!=""}
		{if $widgetTypeVal==3}
			my{$planningSelector}Selector.restoreSession('{$potentialResa->startDate}_{$potentialResa->startTimeslot}','{$potentialResa->getLength()}');
			{if $potentialResa->startTimeslot && $planningSelector!="column" && $planningSelector!="topcolumn"}my{$planningSelector}Selector.showDay('{$potentialResa->endDate}', '{$planningType}');{/if}
		{else}
			my{$planningSelector}Selector.restoreSession('{$potentialResa->startDate}_{$potentialResa->startTimeslot}','{$potentialResa->endDate}_{$potentialResa->endTimeslot}');
			{if $potentialResa->startTimeslot && $planningSelector!="column" && $planningSelector!="topcolumn"}my{$planningSelector}Selector.showDay('{$potentialResa->endDate}', '{$planningType}');{/if}
		{/if}
	{/if}
	{if $myPlanningBox==1 && $planningSelector!="column"}
	if ($("#myOwnReservation{$planningSelector}Content").innerHeight()>0)
		$("#myOwnReservation{$planningSelector}Content").parent().innerHeight($("#myOwnReservation{$planningSelector}Content").innerHeight());
	{/if}
{if !$isajaxloaded}{literal}});{/literal}{/if}

</script>
