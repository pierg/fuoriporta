
	{assign var='slice' value=$day->getSlice($timeSlot->sqlId)}
	{if $slice==null}
	    <div class="emptySlot {$planningType}Slot">&nbsp;</div>
	{else}
		{if !$slice->isAvailable}
			{if $potentialResa->_mainProduct->productType==product_type::AVAILABLE}
			<div class="emptySlot {$planningType}Slot">
				<div class="unslotLabel">
					{if $planningStyle!="pproduct"}<span class="timeLabel">{/if}
						{$calendar->formatDateShort($slice->start, $id_lang)}
					{if $planningStyle!="pproduct"}</span>{/if}
				</div>
			</div>
			{else}
			<div class="holidaySlot {$planningType}Slot">
				<div class="unslotLabel">
					{if $planningStyle!="pproduct"}<span class="timeLabel">{/if}
						{$slice->label}
					{if $planningStyle!="pproduct"}</span>{/if}
					{if !$hidePrice && $potentialResa->showPrice()}
						<br/>{/if}{if $potentialResa->showQuantity()}<br/>{/if}
				</div>
			</div>
			{/if}
		{else}
			{if !$slice->isEnable}
				<div class="unavailableSlot {$planningType}Slot">
					<div class="unslotLabel">{$slice->label}</div>
				</div>
			{else}
			    {assign var='timeSlotAvailable' value=($slice->availableQuantity >= $potentialResa->getOccupationQty() or $slice->availableQuantity <0)}
			    <div {if $timeSlotAvailable}
				    	onmouseover="my{$planningSelector}Selector.hover(this, true, '{$planningType}');" 
				    	onmouseout="my{$planningSelector}Selector.hover(this, false, '{$planningType}');" 
				    	time="{$slice->start}" 
				    	key="{$slice->getKey()}" 
				    	{if $potentialResa->showLength()}length start="{$slice->getStartMinutes()}" endSlice="{$slice->getSliceEndMinutes()}" endSlot="{$slice->getSlotEndMinutes()}" endSlotTime="{$slice->slotEnd}"  endTime="{$slice->end}" {/if}
				    	id="{$planningType}{$slice->getKey()}" 
				    	{if ($potentialResa->_selType=="start")}
				    		label="{if $planningStyle=="topproduct"}{$calendar->formatDateShort($slice->start, $id_lang)}{else}{$calendar->formatDateWithDay($slice->start, $id_lang)}{/if} {if $slice->idTimeSlot && $potentialResa->showTime()}{$calendar->formatTime($slice->getStartTime(), $id_lang)}{/if}" 
				    	{/if}
				    	{if ($potentialResa->_selType=="end")}
				    		label="{if $planningStyle=="topproduct"}{$calendar->formatDateShort($slice->end, $id_lang)}{else}{$calendar->formatDateWithDay($slice->end, $id_lang)}{/if} {if $slice->idTimeSlot && $potentialResa->showTime()}{$calendar->formatTime($slice->getEndTime(), $id_lang)}{/if}" 
				    	{/if}
				    	{if $potentialResa->_selType==""}
				    		label="{$slice->label}"
				    	{/if}
				    	onclick="{if !$hidePrice && $potentialResa->showPrice() && $planningStyle=="topproduct"}showResaPrice('{convertPrice price=$slice->price}');{/if}my{$planningSelector}Selector.{if $timeSlot->sqlId!=-1}slotSelection(this){else}showDay('{$day->key}'){/if};" 
				    	class="slot {if $slice->css!=""}{$slice->css}{/if}availableSlot {$planningType}Slot" 
						name="{if $slice->css!=""}{$slice->css}{else}availableSlot{/if}"
				    {else}
				    	class="slot unavailableSlot {$planningType}Slot"
				    	name="unavailableSlot"
			    	{/if}
			    	{if $slice->object!=null}
			    		objectValue="{$slice->object->sqlId}" 
			    		objectLabel="{$slice->object->name}" 
				     {/if} 
			    >
				    <div class="slotInput">
				    	{if !$widget}
				            <input name="myOwnDeliveriesRes" type="{if $reservationView->multiSel}checkbox{else}radio{/if}" {if !$timeSlotAvailable}disabled{/if} value="{$slice->getKey()}" />
				        {/if}
				    </div>
			        <div class="slotLabel">
				        {if $planningStyle!="pproduct"}<span class="timeLabel">{/if}
				        	{$slice->label}
				        {if $planningStyle!="pproduct"}</span>{/if}
				        {if $slice->label!=''}<br/>{/if}
			        	{if $timeSlot->sqlId!=-1}
				        	{if !$hidePrice && $potentialResa->showPrice() && ($planningStyle=="product" || $planningStyle=="topproduct")}
				        		<span class="priceLabel {if $slice->isReduc}reductionLabel{/if}">
				        			 {if ($potentialResa->_selType=="start")}
				        				<span class="estimateLabel">{$previewPriceLabel}</span>
				        			 {/if}
				        			 {convertPrice price=$slice->price}</span>
				        	{/if}
				        	{if $potentialResa->showQuantity()}
				        		<br/><span class="quantityLabel">{if $slice->availableQuantity == 0}{l s='not' mod='myownreservations'} {$previewQuantityLabel}{else}{if $slice->availableQuantity >0}{$slice->availableQuantity}{/if} {if $slice->availableQuantity >1}{$previewQuantityLabels}{else}{$previewQuantityLabel}{/if}{/if}</span>
				        	{/if}
				        	{if $slice->object!=null && $potentialResa->showObject()}
				        	<br/><span class="objectLabel">{$reservationView->ext_object->label} : {$slice->object->name}</span>
				        	{/if}
			        	{/if}
			        </div>
			    </div>
			{/if}
		{/if}
	{/if}
