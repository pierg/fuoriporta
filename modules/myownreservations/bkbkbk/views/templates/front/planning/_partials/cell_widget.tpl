{foreach from=$reservationView->getAvailableProducts($id_lang, $Day, $timeSlot) key=product item=productName name=productsLoop}
	{assign var='viewedProduct' value=$productsImages[$product]}
<div class="block_content" style="padding-top:1px">
<center>
	{if $imageSize>0}<a href="{$myownlink->getProductLink($viewedProduct->id, $viewedProduct->link_rewrite, $viewedProduct->category_rewrite)}" title="{$viewedProduct->name|escape:html:'UTF-8'}"><img src="{$myownlink->getImageLink($viewedProduct->link_rewrite, $viewedProduct->cover, {$mediumSize->name})}" height="{$mediumSize->height}" width="{$mediumSize->width}" alt="{$viewedProduct->legend|escape:html:'UTF-8'}" /></a>{/if}
	<h5 class="widget_title"><a href="{$myownlink->getProductLink($viewedProduct->id, $viewedProduct->link_rewrite, $viewedProduct->category_rewrite)}" title="{$viewedProduct->name|escape:html:'UTF-8'}">{$viewedProduct->name|truncate:14:'...'|escape:html:'UTF-8'}</a></h5>
</center>
</div>
{/foreach}