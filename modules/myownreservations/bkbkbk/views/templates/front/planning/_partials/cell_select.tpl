{assign var='slice' value=$day->getSlice($timeSlot->sqlId)}
{if $slice!=null}
	{if !$slice->isAvailable}
		<div class="holidaySlot selSlot"><div class="unslotLabel">{$slice->label}</div></div>
	{else}
		{if !$slice->isEnable}
			<div class="unavailableSlot selSlot"><div class="unslotLabel">{$slice->label}_</div></div>
		{else}
			{assign var='timeSlotAvailable' value=($slice->availableQuantity >0 or $slice->availableQuantity <0)}
		    <div {if $timeSlotAvailable}
		    	onmouseover="my{$planningSelector}Selector.hover(this, true);" 
		    	onmouseout="my{$planningSelector}Selector.hover(this, false);" 
		    	{if ($potentialResa->_selType=="start")}
		    		label="{$calendar->formatDateWithMonthShort($slice->start, $id_lang)} {if $slice->idTimeSlot && $potentialResa->showTime()}{$calendar->formatTime($slice->getStartTime(), $id_lang)}{/if}" 
		    	{/if}
		    	{if ($potentialResa->_selType=="end")}
		    		label="{$calendar->formatDateWithMonthShort($slice->end, $id_lang)} {if $slice->idTimeSlot && $potentialResa->showTime()}{$calendar->formatTime($slice->getEndTime(), $id_lang)}{/if}" 
		    	{/if}
		    	time="{$slice->start}" 
		    	key="{$slice->getKey()}" 
		    	id="{$planningType}{$slice->getKey()}" 
		    	onclick="my{$planningSelector}Selector.{if $timeSlot->sqlId!=-1}slotSelection(this){else}showDay('{$day->key}'){/if};"
		    	{/if} 
		    	class="{if !$timeSlotAvailable}un{/if}availableSlot selSlot" name="{if !$timeSlotAvailable}un{/if}availableSlot">
		    	<div class="slotInput">
		            <input name="myOwnDeliveriesRes" type="{if $reservationView->multiSel}checkbox{else}radio{/if}" {if !$timeSlotAvailable}disabled{/if} value="{$slice->getKey()}" />
		        </div>
		        <div class="slotLabel">
			        {$slice->label}
		        </div>
		    </div>
		{/if}
	{/if}
{else}
<div class="emptySlot selSlot">&nbsp;</div>
{/if}