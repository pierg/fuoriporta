{*
* 2010-2017 LaBulle
*
* NOTICE OF LICENSE
*
* This source file is subject to Terms and Conditions of Use 
* Available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade module to newer
* versions in the future. If you wish to customize module for your
* needs please refer to http://www.labulle.net for more information.
*
* @author    laBulle.net <info@labulle.net>
* @copyright 2010-2017 laBulle
* @license   http://www.labulle.net/download/CGV-en.pdf
*}
<link href="{$module_dir}views/css/planning.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" defer>
var myOwnSelectCatLink = {$selectCatLink nofilter};
var myOwnSelectCatFammily = {$selectCatFammily nofilter};
{literal}document.addEventListener("DOMContentLoaded", function(event) {{/literal}
if (typeof(my{$planningSelector}Selector)=='undefined')
	var my{$planningSelector}Selector = Object.create(myOwnSelector);
my{$planningSelector}Selector.setFamily({$potentialResa->_mainProduct->sqlId});
{literal}});{/literal}
</script>
<div id="featured-products_block_center" class="block">
	{if $_PS_VERSION_<"1.6"}<h4>{l s='Availabilities' mod='myownreservations'}</h4>{/if}
	{if $_PS_VERSION_>="1.7"}<h1 style="text-align:center" class="text-uppercase h1">{l s='Availabilities' mod='myownreservations'}</h1>{/if}
		{if count($selectCats)>0}

		{else}
			<input id="myOwnSelectHomeCat" type="hidden" value="{$defaultCategoryLink}"><input id="myOwnSelectHomeCatId" type="hidden" value="{$defaultCategoryId}">
		{/if}
		<div class="block_content">
			<div id="myOwnReservationhomeLoading" style="width:auto;text-align:center;padding-top:30px;display:none"><img src="{$my_img_dir}/loader.gif"></div>
			<div id="myOwnReservationhomeContent">{$planning nofilter}</div>
		</div>
</div>
