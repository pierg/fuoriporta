{*
* 2010-2017 LaBulle
*
* NOTICE OF LICENSE
*
* This source file is subject to Terms and Conditions of Use 
* Available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade module to newer
* versions in the future. If you wish to customize module for your
* needs please refer to http://www.labulle.net for more information.
*
* @author    laBulle.net <info@labulle.net>
* @copyright 2010-2017 laBulle
* @license   http://www.labulle.net/download/CGV-en.pdf
*}
<link href="{$module_dir}views/css/planning.css" rel="stylesheet" type="text/css" media="all" />
<link href="{$module_dir}views/css/widget-top.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript">
var myOwnSearchLink = '{$searchLink}';
var myOwnSelectCatLink = {$selectCatLink nofilter};
var myOwnSelectEarliers = {$earlierSelections nofilter};
var myOwnSelectEtras = {$extraSelections nofilter};
var myOwnSelectQty = {$extraQty nofilter};
var myOwnSelectUniques = {$uniqueSelections nofilter};
var myOwnSelectEarlierLabels = {$earlierSelectionLabels nofilter};
var myOwnSessionSelections =  {$sessionSelections nofilter};
var myOwnSelectCatFammily = {$selectCatFammily nofilter};
var my{$planningSelector}Selector;
{literal}
document.addEventListener("DOMContentLoaded", function(event) {
	{/literal}
	if (typeof(my{$planningSelector}Selector)=='undefined') 
		my{$planningSelector}Selector = Object.create(myOwnSelector);
	my{$planningSelector}Selector.target='{$planningSelector}';
	{literal}
    $('.homeslider-description').click(function () {
        window.location.href = $(this).prev('a').prop('href');
    });

    if ($('#htmlcontent_top').length > 0)
    	$('#widget-top').addClass('col-xs-8');
    else
    	$('#widget-top').addClass('col-xs-12');
    attachQtyBtn();
});
function attachQtyBtn() {
    $(".myownr_extra_field .attendee_quantity_up").unbind("click");
	$(".myownr_extra_field .attendee_quantity_up").on("click", function(e) {
		e.preventDefault();
	    fieldName = $(this).data("field-qty");
	    var field = $("input[name="+fieldName+"]");
	    var currentVal = parseInt(field.val());
	    var max = field.attr("max");
	
	    if (!isNaN(currentVal) && (currentVal <= max || max==0))
	        $("input[name="+fieldName+"]").val(currentVal + 1).trigger("keyup");
	    //else
	    //    $("input[name="+fieldName+"]").val(quantityAvailableT);

		var sign = "";
	
		var cat_qty="";
		//alert($(this).parents("td").html());
		$(this).parents("div").find("input.qty_field").each(function(index) {
			if ($(this).val()!="") {
				if (cat_qty != "")
					cat_qty += ",";
				cat_qty += $(this).val()+" "+ $(this).attr("key");
			}
		});
		mytopcolumnSelector.qties=cat_qty;
	});
	$(".qty_field").unbind("change");
	$(".qty_field").on("change", function(e) {
		var cat_qty="";
		//alert($(this).parents("td").html());
		$(this).parents("div").find("input.qty_field").each(function(index) {
			if ($(this).val()!="") {
				if (cat_qty != "")
					cat_qty += ",";
				cat_qty += $(this).val()+" "+ $(this).attr("key");
			}
		});
		mytopcolumnSelector.qties=cat_qty;
	});
	$(".myownr_extra_field .attendee_quantity_down").unbind("click");
	$(".myownr_extra_field .attendee_quantity_down").on("click", function(e) {
		e.preventDefault();
		fieldName = $(this).data("field-qty");
	    var field = $("input[name="+fieldName+"]");
	    var currentVal = parseInt(field.val());
	    var min = field.attr("min");
	    if (!isNaN(currentVal) && currentVal > min)
	        $("input[name="+fieldName+"]").val(currentVal - 1).trigger("keyup");
	    else
	        $("input[name="+fieldName+"]").val(min);
	
		sign = "-";
	
		var cat_qty="";

		$(this).parents("div").find("input.qty_field").each(function(index) {
			if ($(this).val()!="") {
				if (cat_qty != "")
					cat_qty += ",";
				cat_qty += $(this).val()+" "+ $(this).attr("key");
			}
		});
		mytopcolumnSelector.qties=cat_qty;
	});
}
{/literal}
</script>
{if $_PS_VERSION_>="1.7"}
<div class="container" id="widget-top-new">
{else}
<div id="widget-top" class="col-xs-8" style="">
{/if}
	
	<div style="" {if $_PS_VERSION_<"1.7"}id="bx-wrapper"{/if} type="{$planningSelector}">
		<h2>{l s='Reservation' mod='myownreservations'}</h2>
		{if count($selectCats)>1}
		<div class="myownr_form">
			<div class="myownr_field myownr_field_category">

					{if $_PS_VERSION_>="1.6" && $_PS_VERSION_<"1.7"}
		    		<div class="selector" id="" style="display:inline-block;text-align:center">
		    			<span style="font-weight:bold;font-size:13px;width: 250px; -webkit-user-select: none;">{if $resa_category>0}{$selectCats[$resa_category]}{/if}</span>
		    		{/if}
		    		<select style="padding:2px" class="myOwnSelectCat button_large" type="{$planningSelector}" onChange="my{$planningSelector}Selector.checkColumnCat($(this));">
			    		{foreach from=$selectCats key=selectCatId item=selectCatName name=selectCatLoop}
			    			<option value="{$selectCatId}" {if $resa_category==$selectCatId}selected{/if}>{$selectCatName}</option>
			    		{/foreach}
			    	</select>
					{if $_PS_VERSION_>="1.6" && $_PS_VERSION_<"1.7"}</div>{/if}
					
			</div>
		{if $_PS_VERSION_<"1.7"}</div>{/if}
		{else}
			<input id="myOwnSelectHomeCat" type="hidden" value="{$defaultCategoryLink}"><input id="myOwnSelectHomeCatId" type="hidden" value="{$defaultCategoryId}">
		{/if}
		{if $_PS_VERSION_<"1.7"}<div class="myownr_form">{/if}
			<div class="myownr_field myownr_field_period">
				<a class="myownr_buttons" id="myownr_btn_start" onclick="my{$planningSelector}Selector.displayStartPopup($(this));" onblur="my{$planningSelector}Selector.hidePopup($(this));">
					{if $_PS_VERSION_>="1.7"}
						<i class="material-icons">event</i>
					{else}
						<i class="icon-calendar"></i>
					{/if}
					<div class="content">
						<span class="label" default="{$myOwnResLblStart}" unique="{l s='Period' mod='myownreservations'}" multiple="{l s='Periods' mod='myownreservations'}">{$myOwnResLblStart}</span>
						<span class="sel">&nbsp;</span>
					</div>
				</a>
			</div>
			<div class="myownr_field myownr_field_period">
				<a class="myownr_buttons" id="myownr_btn_stop" onclick="my{$planningSelector}Selector.displayStopPopup($(this));"  onblur="my{$planningSelector}Selector.hidePopup($(this));">
					{if $_PS_VERSION_>="1.7"}
						{if $lengthWidget}<i class="icon-time"></i>{else}<i class="material-icons">event</i>{/if}
					{else}
						{if $lengthWidget}<i class="icon-time"></i>{else}<i class="icon-calendar"></i>{/if}
					{/if}
					<div class="content">
						<span class="label">{if $lengthWidget}{l s='Length' mod='myownreservations'}{else}{$myOwnResLblEnd}{/if}</span>
						<span class="sel">&nbsp;</span>
					</div>
				</a>
			</div>
			{if $extraSelections!='[]'}
			<div id="extraSel" class="">
			</div>
			{/if}
			{if $extraQty!='[]'}
			<div id="extraQty" class="">
			</div>
			{/if}
			<div class="myownr_field myownr_field_validate" style="width:140px;padding:5px;">
				<a class="myownr_buttons myownr_disabledbtn" id="myownr_btn_validate" onclick="my{$planningSelector}Selector.validate($(this));" >
					<i class="icon-search"></i>
					<span class="action">
						{l s='Search' mod='myownreservations'}
					</span>
				</a>
			</div>
		</div>
	</div>
	{if $_PS_VERSION_>="1.7"}
		<div style="clear:both"></div>
	{else}
		<img style="max-width:100%;height:auto;" src="{$base_uri}modules/homeslider/images/sample-3.jpg" />
	{/if}
</div>

<div id="myownr_overlay">
	<div class="block">
		<p class="title_block"></p>
		<div class="block_content">
			<div id="myOwnReservationtopcolumnLoading" style="width:auto;text-align:center;padding-top:30px;display:none"><img src="{$my_img_dir}/loader.gif"></div>
			<div id="myOwnReservationtopcolumnContent" class="myOwnReservationContent" type="topcolumn">{$planning nofilter}</div>
		</div>
	</div>
</div>