{*
* 2010-2017 LaBulle
*
* NOTICE OF LICENSE
*
* This source file is subject to Terms and Conditions of Use 
* Available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade module to newer
* versions in the future. If you wish to customize module for your
* needs please refer to http://www.labulle.net for more information.
*
* @author    laBulle.net <info@labulle.net>
* @copyright 2010-2017 laBulle
* @license   http://www.labulle.net/download/CGV-en.pdf
*}
<link href="{$module_dir}views/css/planning.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript">
var myOwnSearchLink = '{$searchLink}';
var myOwnSelectCatLink = {$selectCatLink nofilter};
var myOwnSelectCatFammily = {$selectCatFammily nofilter};

</script>
<div id="featured-products_block_center" class="{if $_PS_VERSION_>="1.7"}card card-block{else}block{/if}">
	{if $_PS_VERSION_>="1.6" && $_PS_VERSION_<"1.7"}<p class="title_block">{l s='Reservation' mod='myownreservations'}</p>{else}<h4 class="text-uppercase h6">{l s='Reservation' mod='myownreservations'}</h4>{/if}
		<div class="block_content">
			{if $widgetType=="select"}
				{if count($selectCats)>1}
				<table width="100%" class="myOwnColumnCategory" id="myOwnColumnCategory" class="">
					<tr>
						<td colspan="2">
							<h5 class="myOwnH3" style="font-weight:normal;height:16px;width:100%;{if $_PS_VERSION_>="1.7"}display:inline-block;width:50%{/if}"><b>{l s='Category' mod='myownreservations'}</b> : </h5>

							{if $_PS_VERSION_>="1.6" && $_PS_VERSION_<"1.7"}
				    		<div class="selector" id="" style="display:inline-block;text-align:center">
				    			<span style="width: 150px; -webkit-user-select: none;">{if $resa_category>0}{$selectCats[$resa_category]}{/if}</span>
				    		{/if}
				    		<select style="padding:2px" class="button_large myOwnSelectCat" type="{$planningSelector}" onChange="my{$planningSelector}Selector.checkColumnCat($(this));">
					    		{foreach from=$selectCats key=selectCatId item=selectCatName name=selectCatLoop}
					    			<option value="{$selectCatId}" {if $resa_category==$selectCatId}selected{/if}>{$selectCatName}</option>
					    		{/foreach}
					    	</select>
					    	{if $_PS_VERSION_>="1.6" && $_PS_VERSION_<"1.7"}</div>{/if}
						</td>
					</tr>
				</table>
				{else}
					<input id="myOwnSelectHomeCat" type="hidden" value="{$defaultCategoryLink}"><input id="myOwnSelectHomeCatId" type="hidden" value="{$defaultCategoryId}">
				{/if}
			{else}
				<table width="100%" id="myOwnColumnCategory" class="">
					<tr>
						<td>
							<h5 class="myOwnH3" style="font-weight:normal;height:16px;width:100%"><b>{l s='Search' mod='myownreservations'}</b> : </h5>
						</td>
						<td>
							<input class="search_query ac_input" type="text" id="myOwnSelectSearch" value="{$search_query}" autocomplete="off">
						</td>
					</tr>
				</table>
			{/if}
    		
			<div id="myOwnReservation{$planningSelector}Loading" style="width:auto;text-align:center;padding-top:30px;display:none"><img src="{$my_img_dir}/loader.gif"></div>
			<div id="myOwnReservation{$planningSelector}Content" class="myOwnReservationContent" type="column">{$planning nofilter}</div>
			<p id="cart-buttons" style="margin-top:5px;text-align:center;min-height:20px">
				
				<span id="myOwnColumnUnSelect" class="{if !$isSel}exclusive{if $_PS_VERSION_>="1.7"} btn btn-primary disabled{/if}{/if}" style="float:right;font-size:14px;{if $isSel}display:none;{/if}width:100%">{l s='Show products' mod='myownreservations'}</span>
				<a tabindex onclick="my{$planningSelector}Selector.{if $widgetTypeVal==3}validate(){else}submitColumn('{$widgetType}'){/if};" id="myOwnColumnSelect" class="{if !$isSel}exclusive{/if} {if $_PS_VERSION_>="1.6"}button btn btn-default {/if}{if $_PS_VERSION_>="1.7"}btn-primary{/if}" style="width:100%;text-align:center;{if !$isSel}display: none;{/if}" title="{l s='Submit' mod='myownreservations'}"><span style="display:block;font-size: 14px;text-align:center;width:100%">{if $widgetType=="select"}{l s='Show products' mod='myownreservations'}{else}{l s='Search products' mod='myownreservations'}{/if}</span></a>
			</p>
		</div>
</div>
