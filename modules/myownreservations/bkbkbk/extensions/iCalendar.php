<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class iCalendar extends myOwnExtension {
	const OPTION_EMAIL = 1;
	const OPTION_FEED = 2;
	
	public function __construct() {
		$this->id=3;
	}
	
	public function ajax($obj, $cookie) {
		//if (Tools::getValue('notif')=='icalendar') {
			$id_lang = Tools::getValue('id_lang', $cookie->id_lang);
			$id_resa = Tools::getValue('id_resa');
			$id_rule = Tools::getValue('id_rule');
			$sel = Tools::getValue('sel');
			if ((int)$id_resa>0) {
				if (!$cookie->id_customer && !$cookie->id_employee) return '';
				$reservation = myOwnResas::getReservation($id_resa);
				$file = $this->attachFile($obj, $reservation);
				$order = new Order($reservation->id_order);
				if ($order->id_customer==$cookie->id_customer || $cookie->id_employee) {
					header('Content-type: text/calendar; charset=utf-8');
					header('Content-Disposition: inline; filename='.$id_resa.'.ics');
					echo $file["content"];
				}
			}
			if ((int)$id_rule>0) {
				$mainProduct=$obj->_products->list[$id_rule];
				echo self::getAdminFeed($obj, $id_lang, $mainProduct, $sel);
			}
		//}
	}

	public function setInfos($obj) {
		$this->title=$obj->l('iCalendar object', 'iCalendar');
		$this->description=$obj->l('Attachment an iCalendar object on confirmation email and page', 'iCalendar');
	}
	
	public function execConfigNotif($obj, $mainProduct) {
		$mainProductId = $mainProduct->sqlId;
		if ($mainProductId<0) $mainProductId=0;
		if (isset($_POST['iCalendar_email'])) $this->setConfig($mainProductId, iCalendar::OPTION_EMAIL, 0, intval($_POST['iCalendar_email']));
		if (isset($_POST['iCalendar_feed'])) $this->setConfig($mainProductId, iCalendar::OPTION_FEED, 0, intval($_POST['iCalendar_feed']));
	}
	
	public function displayConfigNotif($obj, $idProductFamilly) {
		global $cookie;
		$output='';
		$sendEmail=$this->getConfig($idProductFamilly, iCalendar::OPTION_EMAIL);
		$rssFeed_ical=$this->getConfig($idProductFamilly, iCalendar::OPTION_FEED);
		$output.= '
		<ol style="padding:0px;margin-bottom:0px;">
			<li class="myowndlist">
				<input type="checkbox" id="iCalendar_feed" value="1" name="iCalendar_feed" '.($rssFeed_ical ? 'checked' : '').'> ' . $obj->l('Publish product family reservations as an', 'iCalendar') . ' <a href="'.__PS_BASE_URI__.'index.php?method=iCalendar&fc=module&module=myownreservations&controller=actions&id_lang=1&id_rule='.$idProductFamilly.'">'.$obj->l('iCal Feed', 'iCalendar').'</a> <span class="mylabel-tooltip" title="'.myOwnHelp::$sections['editNotifTab']['icalendar'].'">'.$obj->l('Change selection', 'iCalendar').'</span>
			</li>
		</ol>';
		return $output;
	}
	
	public function getAttachements($obj, $order, $reservations) {
		$attachements=array();
		foreach ($reservations as $reservation) {
			if (in_array($this->id, $reservation->_mainProduct->ids_notifs)  )
				$attachements[]=$this->attachFile($obj, $reservation);
		}
		return $attachements;
	}
	
	public function displayOrderConfResa($obj, $resa) {
		$link = new Link();
		return '<div class="resa_link">	
					<a href="'.MyOwnReservationsUtils::getModuleLink($link, $this->name).'&notif=icalendar&id_resa='.$resa->sqlId.'">
					<b>'.$obj->l('iCalendar', 'iCalendar').'</b><br/>
					<img width="24" height="24" class="imgm" alt="" src="'.__PS_BASE_URI__.'modules/myownreservations/extensions/iCalendar.png" style="border:none"><br/>
					'.$obj->l('Download', 'iCalendar').'
					</a>
				</div>';
	}
	
	public function displayReservationDetails($obj, $resa, $line=-1) {
		global $cookie;
// 		$order = new Order($resa->id_order);
// 		if ($eventid)
		if ($line>-1) return '';
		$link = new Link();
		//'.MyOwnReservationsUtils::getModuleLink($link, $this->name).'&notif=icalendar&id_resa='.$resa->sqlId.'
		return  '
				<tr class="invoice_line" id="invoice_2">
					<td align="left">'.$obj->l('iCalendar', 'iCalendar').'</td>
					<td align="right"><a href="ajax-tab.php?tab=adminmyownreservations'.'&token='.Tools::getAdminToken('adminmyownreservations'.intval(Tab::getIdFromClassName('adminmyownreservations')).intval($cookie->id_employee)).'&action=extension&method=icalendar&id_resa='.$resa->sqlId.'">'.$obj->l('Download', 'iCalendar').'</a></td>
				</tr>';
	}
	
	public function attachFile($obj, $reservation) {
		//$fileAttachments=array();
              		
		//foreach ($reservation as $reservation) {
			if (intval($reservation->id_order)>0) {
				$order = new Order($reservation->id_order);
				$order_name=Configuration::get('PS_INVOICE_PREFIX', (int)($order->id_lang)).sprintf('%06d', $order->invoice_number);
				if (_PS_VERSION_ >= "1.5.0.0") $order_name=$order->getUniqReference();
				$id_lang= $order->id_lang;
			} else {
				$order_name='EXT';
				$id_lang= $obj->context->cookie->id_lang;
			}
			syslog(LOG_WARNING, 'id_lang:'.$id_lang);
			/*$language = Language::getLanguage($id_lang);
			Context::getContext()->language = $language;
			syslog(LOG_WARNING, 'iso:'.Context::getContext()->language['iso_code']);
			syslog(LOG_WARNING, 'liso:'.$language['iso_code']);*/
			$lang = new Language($id_lang);
			Context::getContext()->language = $lang;
			Context::getContext()->cookie->id_lang = (int)$lang->id;
			
			//$msg = htmlentities(myOwnLang::getMessage('NOTIFY_PROD',$id_lang));
			$plbla="";
			if ($reservation->_mainProduct==null)
				$reservation->_mainProduct = $obj->_products->getResProductFromProduct($reservation->id_product);
			$start_timselot=$reservation->getStartTimeSlot();
			$end_timselot=$reservation->getEndTimeSlot();
			
 			$plbl = MyOwnReservationsUtils::getProductName($reservation->id_product, $id_lang);
 			$plbla = MyOwnReservationsUtils::getAttributeCombinaisons($reservation->id_product, $reservation->id_product_attribute, $id_lang);
 			$plbla = str_ireplace("<br />", ", ", $plbla);
 			//if ($plbla!="") $plbl .= ' '.$plbla;
 			
 			/*$msg = str_replace('%product%', $plbl, $msg);
 			$msg = str_replace('%re%', '', $msg);
 			if ($start_timselot!=null && $start_timselot->sqlId>0) $startts=' '.MyOwnCalendarTool::formatTime($start_timselot->startTime,$id_lang); else $startts='';
 			$msg = str_replace('%start%', MyOwnCalendarTool::formatDateWithDay($reservation->getStartDateTime(),$id_lang).$startts, $msg);
 			if ($end_timselot!=null && $end_timselot->sqlId>0) $endts=' '.MyOwnCalendarTool::formatTime($end_timselot->endTime,$id_lang); else $endts='';
 			$msg = str_replace('%end%', MyOwnCalendarTool::formatDateWithDay($reservation->getEndDateTime(),$id_lang).$endts, $msg);*/
        
			$ics=new iCal(md5($order_name).'@'.Configuration::get('PS_SHOP_DOMAIN'), Configuration::get('PS_TIMEZONE'));
	  		$ics->from_name=Configuration::get('PS_SHOP_NAME');
	  		$ics->from_email=Configuration::get('PS_SHOP_EMAIL');
	  		$ics->name=$obj->l('Reservation for', 'icalendar').' '.$plbl; 
	  		$ics->start=$reservation->getStartDateTime();
	  		$ics->end=$reservation->getEndDateTime();
	  		$ics->description=$obj->l('Reservation for', 'icalendar').' '.$plbl.' ('.$plbla.') '.$obj->l('on order', 'iCalendar').' '.$order_name; 
	  		//$ics->location=$place->name.' ('.$place->address->address1.' '.$place->address->postcode.' '.$place->address->city.')';
	  		//$ics->latlng=str_ireplace(",", ";", $place->location);
	  		$fileAttachment['content'] = $ics->getData();
	  		$fileAttachment['name'] = $reservation->reference.'.ics';
			$fileAttachment['mime'] = 'text/calendar';
			return $fileAttachment;
			//$fileAttachments[]=$fileAttachment;
  		//}
  		//return $fileAttachments;
	}
	
	public function getAdminFeed($obj, $id_lang, $mainProduct, $sel='-3 months') {
		$reservations=array();

		if ($sel!='all') {
			$selDate = strtotime($sel);
			$today = strtotime(date('Y-m-d'));
			if ($selDate < $today) {
				$startDate = $selDate;
				$endDate = $today;
			} else {
				$startDate = $today;
				$endDate = $selDate;
			}
			$resas=myOwnResas::getReservationsForProductFamilly($mainProduct, $startDate, $endDate); 
		} else {
			$resas=myOwnResas::getAllReservationsForProductFamilly($mainProduct);
		}
		
		foreach ($resas as $resa) {
				$reservations[$resa->getStartDateTime()]=$resa;
		}

		header('Content-type: text/calendar; charset=utf-8');
		header('Content-Disposition: inline; filename='.$mainProduct->name.' '.$sel.'.ics');

		echo "BEGIN:VCALENDAR\nVERSION:2.0\n";

	ksort ( $reservations);
		foreach ($reservations AS $reservation)
		{
			if (intval($reservation->id_order)>0) {
				$order = new Order($reservation->id_order);
				$order_name=Configuration::get('PS_INVOICE_PREFIX', (int)($order->id_lang)).sprintf('%06d', $order->invoice_number);
				if (_PS_VERSION_ >= "1.5.0.0") $order_name=$order->getUniqReference();
			} else $order_name='EXT';
			
			$msg = htmlentities(myOwnLang::getMessage('NOTIFY_PROD',$order->id_lang));
			$plbla="";
			$start_timselot=$reservation->getStartTimeSlot();
			$end_timselot=$reservation->getEndTimeSlot();
			
 			$plbl = MyOwnReservationsUtils::getProductName($reservation->id_product, $order->id_lang);
 			$plbla = MyOwnReservationsUtils::getAttributeCombinaisons($reservation->id_product, $reservation->id_product_attribute, $order->id_lang);
 			$plbla = str_ireplace("<br />", ", ", $plbla);
 			if ($plbla!="") $plbl .= ' '.$plbla;
 			
 			/*$msg = str_replace('%product%', $plbl, $msg);
 			$msg = str_replace('%re%', '', $msg);
 			if ($start_timselot->sqlId>0) $startts=' '.MyOwnCalendarTool::formatTime($start_timselot->startTime,$order->id_lang); else $startts='';
 			$msg = str_replace('%start%', MyOwnCalendarTool::formatDateWithDay($reservation->getStartDateTime(),$order->id_lang).$startts, $msg);
 			if ($end_timselot->sqlId>0) $endts=' '.MyOwnCalendarTool::formatTime($end_timselot->endTime,$order->id_lang); else $endts='';
 			$msg = str_replace('%end%', MyOwnCalendarTool::formatDateWithDay($reservation->getEndDateTime(),$order->id_lang).$endts, $msg);
 			$msg = html_entity_decode($msg);*/
 			
			$ics=new iCal(md5($order_name).'@'.Configuration::get('PS_SHOP_DOMAIN'), Configuration::get('PS_TIMEZONE'));
	  		$ics->from_name = Configuration::get('PS_SHOP_NAME');
	  		$ics->from_email = Configuration::get('PS_SHOP_EMAIL');
	  		$ics->to_name = $reservation->_customerName;
	  		$ics->to_email = $reservation->_customerEmail;
	  		$ics->name=$obj->l('Reservation for', 'iCalendar').' '.$plbl;
	  		$ics->start=$reservation->getStartDateTime();
	  		$ics->end=$reservation->getEndDateTime();
	  		$ics->description=$obj->l('Reservation for', 'icalendar').' '.$plbl.' ('.$plbla.') '.$obj->l('on order', 'iCalendar').' '.$order_name; 
	  		
	  		echo $ics->getData(false);
		}
		
		echo "END:VCALENDAR\n";
	}
	
	public function execNotification($cookie,$obj,$value) {
		return 'test';
	}
}
if (!class_exists('ICal')) {
class iCal {
    var $_data;
    var $uid;
    var $timezone;
    var $from_name;
    var $from_email;
    var $to_name;
    var $to_email;
    var $name;
    var $start;
    var $end;
    var $description;
    var $location;
    var $latlng;
    
    public function __construct($uid, $timezone) {
        $this->uid = $uid;
        $this->timezone = $timezone;
    }
    
    public function getData($full=true) {
    	//LOCATION:".$this->location."\nGEO:".$this->latlng."\n
    	$this->_data = ($full ? "BEGIN:VCALENDAR\nVERSION:2.0\n" : "")."BEGIN:VEVENT\nUID:".$this->uid."\nDTSTAMP:".date("Ymd\THis\Z")."\nORGANIZER;CN=".$this->from_name.":MAILTO:".$this->from_email.($this->to_name != '' ? "\nATTENDEE;CUTYPE=".$this->to_name.":MAILTO:".$this->to_email : "")."\nDTSTART;TZID=".$this->timezone.":".date("Ymd\THis",$this->start)."\nDTEND;TZID=".$this->timezone.":".date("Ymd\THis",$this->end)."\nDESCRIPTION:".$this->description."\nSUMMARY:".$this->name."\nEND:VEVENT\n".($full ? "END:VCALENDAR\n" : "");
        return $this->_data;
    }
    
    public function save() {
        file_put_contents($this->name.".ics",$this->_data);
    }
    
    public function show() {
        header("Content-type:text/calendar");
        header('Content-Disposition: attachment; filename="'.$this->name.'.ics"');
        Header('Content-Length: '.strlen($this->_data));
        Header('Connection: close');
        echo $this->data;
    }
}
}
?>