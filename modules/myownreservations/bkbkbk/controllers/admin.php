<?php
/*
* 2010-2012 LaBulle All right reserved
*/
 


class myOwnReservationsAdminController
{
	public static $obj;
	public static $module;
	public static $admin;
	public static $context;

	//==============================================================================================================================================
	//     SHOW MENU
	//==============================================================================================================================================
		
	public static function showPlanningMenu($obj, $view, $style='') {
		global $rights;
		$out="";
		$img='../modules/'.$obj->name.'/img/';
		$out .= '<script type="text/javascript"> 

		$(document).ready(function() {
			if (typeof(scheduler)=="undefined") currentViewMode = "month";
			planningDisplayed = '.(($view=="month" or $view=="week" or $view=="day") ? 'true' : 'false' ).';

			if (planningDisplayed) {
				$("#"+currentViewMode+"_button").addClass(\'header_button_hover\');
				$("#header_img").attr("src", "'.$img.'calendar-"+currentViewMode+".png");
			}
			
			$("#planning_menu").mouseenter(function(){
				//$(this).css("background","ivory");
				$("#"+currentViewMode+"_button").addClass("header_button_hover");
				$("#header_img").attr("src", "'.$img.'calendar-"+currentViewMode+".png");
			})
			 .mouseleave(function(){
			 	$(this).css("background","");
			 	$(".header_button").removeClass("header_button_hover");
			 	if (planningDisplayed) $("#"+currentViewMode+"_button").addClass("header_button_hover");
			})
			
			$(".header_button").mouseenter(function(){
				$(".header_button").removeClass("header_button_hover");
				$(this).addClass("header_button_hover");
				$("#header_img").attr("src", "'.$img.'calendar-"+$(this).attr("view")+".png");
			})
			 .mouseleave(function(){
			 	$(this).removeClass("header_button_hover");
			 	$("#"+currentViewMode+"_button").addClass("header_button_hover");
			 	$("#header_img").attr("src", "'.$img.'calendar-"+currentViewMode+".png");
			})
			.click(function(){
				var planningSel="";
				var btn_view = $(this).attr("view");
				if (planningDisplayed) scheduler.setCurrentView(currentViewDate, btn_view);
			 	else document.location.href="'.myOwnReservationsController::getAdminUrl('').'#date='.date("Y-m-d").',mode="+btn_view;
//btn_view=="grid" || 
			 	if (btn_view=="timeline") $("#dhx_cal_print").hide();
			 	else $("#dhx_cal_print").show();
			});
		});
		</script>';
		if ($view=="month" or $view=="week" or $view=="day") $planningIcon=$view;
		else if ($view=="stock") $planningIcon="stock";
		else $planningIcon="month";
		
		if (_PS_VERSION_ < '1.6.0.0') {
			$out .= '<div id="planning_menu" class="" style="width:620px;'.$style.'">';
			if (_PS_VERSION_ < "1.6.0.0") $out .= '
							<div style="float:left;padding-right:20px;" onclick="location.href=\''.myOwnReservationsController::getAdminUrl('&view=month').'\'">
								<h3>
									<span id="current_obj" style="font-weight: normal;">
											<span class="breadcrumb item-0 ">'.$obj->l('Catalog', 'admin').'<img alt="&gt;" style="margin-right:5px" src="../img/admin/separator_breadcrumb.png"></span>
											<img id="header_img" src="'.$img.'calendar-'.$planningIcon.'.png" style="margin-right:5px"><span class="breadcrumb item-1 ">'.$obj->l('Planning', 'admin').'</span>
									</span>
								</h3>
							</div>';
			$out .= '
							<div style="float:left;position:relative">
								<input style="width:55px;border-top-right-radius: 0px;border-bottom-right-radius: 0px;border-right: none;" id="month_button" class="header_button button" view="month" type="button" value="'.$obj->l('Month', 'admin').'">
							</div>
							<div style="float:left;position:relative">
								<input style="width:85px;border-radius: 0px;border-right: none;" id="timeline_button" class="header_button button" view="timeline" type="button" value="'.$obj->l('Timeline', 'admin').'">
							</div>
							<div style="float:left;position:relative">
								<input style="width:70px;border-radius: 0px;border-right: none;" id="week_button" class="header_button button" view="week" type="button" value="'.$obj->l('Week', 'admin').'">
							</div>
							<div style="float:left;position:relative">
								<input style="width:70px;border-top-left-radius: 0px;border-bottom-left-radius: 0px;" id="grid_button" class="header_button button  header_button_last" view="grid" type="button" value="'.$obj->l('Grid', 'admin').'">
							</div>
							';
					$out .= '
							<div style="clear:both"></div>
						</div>';
			}
		return $out;
	}
	
	public static function getButtonsList($obj)
	{
		global $cookie;
		$employee = new Employee($cookie->id_employee);
		$rights = Profile::getProfileAccess($employee->id_profile, Tab::getCurrentTabId());

		$buttons=array();
		$view=Tools::getValue('myOwnResTab', 'planning');
		$isproversion=is_file(_PS_MODULE_DIR_ ."myownreservations/pro/products.php");
				
		$output = '';
		
		
		if ($rights['add']==1) 
			$buttons['create'] = array(
				'desc' => ucfirst(myOwnLang::$actions[MYOWN_OPE::CREATE]),
				'js' => ($view=='planning' ? "showNewResa();" : "$('#addStock').toggle('slow')"),
				'icon' => 'process-icon-new'
			);
		if ($view!='planning' && $rights['delete']==1)
			$buttons['delete'] = array(
				'desc' => ucfirst(myOwnLang::$actions[MYOWN_OPE::DELETE]),
				'js' => 'if (confirm(\''.$obj->l('Delete selected stock items ?', 'admin').'\')) {$(\'#stockformaction\').val(\'deleteStocks\');$(\'#stockform\').submit();}',
				'icon' => 'process-icon-delete'
			);
		
		$buttons['print'] = array(
			'desc' => ucfirst(myOwnLang::$actions[MYOWN_OPE::PPRINT]),
			'js' => ($view=='planning' ? "print();" : "$('#globalactions').submit();"),
			'icon' => 'process-icon-delete',
			'style' => 'background-image: url(../modules/myownreservations/img/print.png);'
		);
		$buttons['export'] = array(
			'desc' => ucfirst(myOwnLang::$actions[MYOWN_OPE::EXPORT]),
			'js' => ($view=='planning' ? "exportPeriod();" : 'document.location.href=\''.myOwnReservationsController::getAdminUrl('stock&action=exportConfig&myOwnResTab=stock').'\'' ),
			'icon' => 'process-icon-export'
		);
		$buttons['help'] = array(
			'desc' => ucfirst(myOwnLang::$actions[MYOWN_OPE::HELP]),
			'js' => ($view=='planning' ? 'document.location.href=\''.myOwnReservationsController::getAjaxUrl('action=printHelp').'\'' : 'toogleHelp();$(\'.hint\').toggle();' ),
			'icon' => 'process-icon-help'
		);
		
		return $buttons;
	}
	
	public static function getMenuHeader($obj, $cookie, $views, $buttons) {
		$content = '';
		$content .= '
		<div class="toolbar-placeholder">
			<div class="toolbarBox toolbarHead" style="padding: 5px 0;">
				'.$buttons.'
				<div class="pageTitle">
					
					'.$views.'
				</div>
				
			</div>
		</div>';
		return $content;
	}
	
	public static function displayPageHeader($obj, $timesel, $type, $productFamillyFilter) {
		global $cookie;
		global $mycarrier;
		global $rights;

		$_timeSlots = $obj->_timeSlots;
		$_products = $obj->_products;
		
		$title = ucfirst(myOwnLang::$objects[MYOWN_OBJ::RESV]);
		if ($type=="stock") $title = $obj->l('Stock availability', 'admin');
		if ($type=="roundtrip" or $type=="day" or $type=="stock") {
			$timeselStr=date("Y-m-d", $timesel);
			$print='&day='.$timeselStr;
		}
		if ($type=="week") {
			$print='&week='.$timesel;
			$tabReq = explode("-",$timesel);
			$currentYear = $tabReq[0];
			$currentWeek = $tabReq[1];
		}
		if ($type=="month") {
			$tabDate = explode("-",$timesel);
			$currentMonth = $tabDate[1];
			$currentYear = $tabDate[0];
			$print='&month='.$timesel;
		}
		
		$timeSlotFilter = Tools::getValue('timeSlotFilter');
		$reservationFilter = Tools::getValue('reservationFilter');
		$productFilter = Tools::getValue('productFilter');
		$statusFilter = Tools::getValue('statusFilter');
		$output='';
		$output .= myOwnUtils::insertDatepicker($obj, array('datepickerDate'), false, 'dateFormat: "yy-mm-dd", onSelect: submitDatePickerStock');
		$views='';
		if (_PS_VERSION_ >= "1.6.0.0" && $type!="stock")
			$views = myOwnReservationsAdminController::showPlanningMenu($obj, 'month', 'float:left;margin:-6px;width:300px;margin-left:20px');
	
		$langue=$cookie->id_lang;
		$output = '';
		
		$output .= '
		<table class="myHeader nobootstrap" width="100%" border="0" cellspacing="0">
		 	 <tr>
			    <td width="'.(_PS_VERSION_ < "1.6.0.0" ? '350px' : '650px').'">
			    	<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
			    	<input type="hidden" name="view" value="'.$type.'" />';
			    	if ($type=="roundtrip" or $type=="day" or $type=="stock") {
			    		$output .= '
				    	<input style="width:25px;background:#FFF6D3 3px 3px no-repeat url(\'../img/admin/previous.gif\');display:none" type="submit" name="submitDateDayPrev" id="submitDateDayPrev" class="button" value="">
				    	<input style="float:left;margin:0px;height:18px;padding:1px;width:90px;-webkit-box-sizing:initial;-moz-box-sizing:initial;
box-sizing: initial;" size="10" type="text" name="datepickerFrom" id="datepickerDate" class="datepicker admin_extra_button" value="'.$timeselStr.'">
				    	<input style="text-align:left;padding-left:22px;background:#FFF6D3 3px 3px no-repeat url(\'../img/admin/date.png\');display:none" type="submit" name="submitDatePicker" id="submitDatePicker" class="button" value="'.$obj->l('Show', 'admin').'" />
				    	<input style="width:25px;background:#FFF6D3 3px 3px no-repeat url(\'../img/admin/next.gif\');display:none" type="submit" name="submitDateDayNext" id="submitDateDayNext" class="button" value="">
				    	<div class="admin_prev_button" style="float:left"  onclick="$(\'#submitDateDayPrev\').click()">&nbsp;</div>
				    	<div class="admin_next_button" style="float:left;" onclick="$(\'#submitDateDayNext\').click()">&nbsp;</div>
				    	<div style="float:left;width:100px;margin-left:25px">'. MyOwnReservationsUtils::insertProductFamillyPicker($obj, $langue, "productFamillyFilter", "dhx_cal_extra", "height:24px;width:100px;font-size:12px;background-color:#FFF;", $productFamillyFilter, "submitDatePickerStock();", false).'</div>';
				    	$output .= $views;
			    	}

			    	$output.= '
			    	</form>
			    </td>
			    <td align="center" class="headerTitle">'.$title.' ';
			    
			    if ($type=="roundtrip" or $type=="day" or $type=="stock") $output .= $obj->l('for', 'admin').' ' . MyOwnCalendarTool::formatDateWithDay($timesel,$langue);
			    if ($type=="week") $output .= $obj->l('for the week', 'admin').' ' . intval($currentWeek) .' '.$obj->l('on year', 'admin').' '.$currentYear;
			    if ($type=="month") $output .= $obj->l('for', 'admin').' ' . MyOwnCalendarTool::getMonthString($currentMonth,$langue) .' '. $currentYear;
			    
			    if ($timeSlotFilter != '') {
			    	$timeSlotFilterObj=$_timeSlots->list[$timeSlotFilter];
			    	$output .= ' '.$obj->l('between', 'admin').' '.MyOwnCalendarTool::formatTime($timeSlotFilterObj->startTime,$langue).' '.$obj->l('and', 'admin').' '.MyOwnCalendarTool::formatTime($timeSlotFilterObj->endTime,$langue).($timeSlotFilterObj->id_family ? ' '.$obj->l('for', 'admin').' '.$_products->list[$timeSlotFilterObj->id_family]->name : '');
			    }
			    if ($productFamillyFilter != '' && intval($productFamillyFilter)>0)
			    	$output .= ' '. $obj->l('for product familly', 'admin') .' "'.  $_products->list[$productFamillyFilter]->name .'"';
			    if ($statusFilter != '')
			    	$output .= ' '. $obj->l('for orders with status', 'admin') .' "'. substr(myOwnUtils::getStatusName($statusFilter,$langue),0,22) .'"';
			    $output .= '
			    </td>
			    <td align="right">
			    	<form name="globalactions" id="globalactions" action="ajax-tab.php?tab=admin'.$obj->name.'&token='.Tools::getAdminToken('admin'.$obj->name.intval(Tab::getIdFromClassName('admin'.$obj->name)).intval($cookie->id_employee)).'&action=printStock" method="post" target="_blank">
			    		<input type="hidden" name="id_lang" value="'.$cookie->id_lang.'" />';
			    		if ($type=="day") $output .= '<input type="hidden" name="exportFrom" value="'.date('Y-m-d',$timesel).'" /><input type="hidden" name="exportTo" value="'.date('Y-m-d',$timesel).'" />';
			    		if ($type=="week") {
			    			$tabReq = explode("-",$timesel);
			    			$tabReqYear = $tabReq[0];
			    			$tabReqWeek = $tabReq[1];
			    			$output .= '<input type="hidden" name="exportFrom" value="'.date("Y-m-d",myOwnCalendarWeek::getDayOfWeek(1, $tabReqWeek, $tabReqYear)).'" />';
			    			$output .= '<input type="hidden" name="exportTo" value="'.date("Y-m-d",myOwnCalendarWeek::getDayOfWeek(7, $tabReqWeek, $tabReqYear)).'" />';
			    			
			    		}
			    		if ($type=="month") {
			    			$start=strtotime($currentYear."-".$currentMonth."-01");
			    			$end=strtotime("+1month -1day",$start);
			    			$output .= '<input type="hidden" name="exportFrom" value="'.date("Y-m-d",$start).'" />';
			    			$output .= '<input type="hidden" name="exportTo" value="'.date("Y-m-d",$end).'" />';
			    		}
			    		$output .= '
			    		<input type="hidden" name="reservationFilter" value="'.$reservationFilter.'" />
			    		<input type="hidden" name="productFamillyFilter" value="'.$productFamillyFilter.'" />
			    		<input type="hidden" name="timeSlotFilter" value="'.$timeSlotFilter.'" />
			    		<input type="hidden" name="statusFilter" value="'.$statusFilter.'" />
			    		<input type="hidden" name="exportFormat" value="standard.csv" />';
			    	if ($type=="stock") {
			    		/*
				    	$label=$obj->l('Labels', 'admin');
						$printFormats=array(
							'5160'=>'Avery 5160 (3x10 '.$label.')',
							'5161'=>'Avery 5161 (2x10 '.$label.')',
							'5162'=>'Avery 5162 (2x7 '.$label.')',
							'5163'=>'Avery 5163 (2x5 '.$label.')',
							'8600'=>'Avery 8600 (3x10 '.$label.')',
							'L7163'=>'Avery L7163 (2x7 '.$label.')',
							'3422'=>'Avery 3422 (3x8 '.$label.')'
							);
						$output .= '
				    	<script type="text/javascript">
				    	$("#globalactions").submit(function() {
						  return getBarCodesv1();
						});
					    function getBarCodesv1() {
					    	var barcodes="";
					    	$(\'input[name="stockBox[]"]\').each(function( index ) {
								if ($(this).attr("checked")) barcodes += $(this).val()+";";
							});
							if (barcodes=="" && $("#printFormat").val()!="") {
								alert("'.$obj->l('Please select stock items', 'admin').'");
								return false;
							}
							$("#exportBarCodesList2").val(barcodes);
							return true;
					    }
					    </script>
						<input type="hidden" name="date" value="'.$timeselStr.'">
						<i>'.$obj->l('Print format', 'admin').' : </i><select id="printFormat" name="printFormat" style="width:95px;height:24px">
							<option value="">'.$obj->l('Table', 'admin').'</option>';
						foreach($printFormats as $key => $printFormat) {
							$output .='
							<option value="'.$key.'">'.$printFormat.'</option>';
						}
						$output .='
						</select>&nbsp;
						<input type="hidden" name="barCodesList" id="exportBarCodesList2" value="">
						';	
						*/
			    	}
			    	$output .= '
			    	</form>
			    </td>
			</tr>
		</table>';
		return $output;
	}


	//-----------------------------------------------------------------------
	//     MAIN
	//-----------------------------------------------------------------------
	public static function display($adminmyownreservation)
	{
		global $obj;
		global $rights;
		global $cookie;
		global $admin;
		$admin = $adminmyownreservation;
		$obj = Module::getInstanceByName('myownreservations');
		
		//undefinded function to check license
		$obj->gcheck();
		$out='';
		
		$_products = $obj->_products;
		
		//get allowed product family
		$employee = new Employee($cookie->id_employee);
		$rights = Profile::getProfileAccess($employee->id_profile, Tab::getCurrentTabId());
		if ($employee->id_profile>1) {
			$_products->list = $_products->getListForEmployee($employee->id);
		}
		$obj->_products = $_products;

		if (session_id() == '') session_start();
		$productFamillyFilter = "";
		if (Tools::getValue('products','')!='') $_SESSION["myown_familyFilter"] = Tools::getValue('products','');
		if (isset($_POST['productFamillyFilter'])) $_SESSION["myown_familyFilter"] = Tools::getValue('productFamillyFilter','');
		if (!isset($_SESSION["myown_familyFilter"])) $_SESSION["myown_familyFilter"] = "";
		$productFamillyFilter = intval($_SESSION["myown_familyFilter"]);

		if (!array_key_exists($productFamillyFilter, $_products->list) && intval($productFamillyFilter)>0) {
			if (array_key_exists(0, $_products->list)) $productFamillyFilter=0;
			else $productFamillyFilter=array_shift(array_values($_products->list))->sqlId;
		}

		//output
		//-----------------------------------------------------------------------------------------------------------------

		
		$out .= MyOwnReservationsUtils::displayIncludes();
		if (_PS_VERSION_ < "1.6.0.0") {
			$admin->initPageHeaderToolbar();
			$buttons = MyOwnReservationsUtils::getButtonsList($admin->page_header_toolbar_btn);
			$views = myOwnReservationsAdminController::showPlanningMenu($obj, 'month');
			$out .= self::getMenuHeader($obj, $cookie, $views, $buttons);
		}
		$out .= myOwnReservationsPlanningController::display($obj, $admin, $productFamillyFilter);

		return $out;
  }
  
}
?>