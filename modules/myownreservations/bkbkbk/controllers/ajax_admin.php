<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnReservationsAjaxController
{
	public static function create($obj) {
		global $cookie, $rights;
		$male=false;$plur=false;
		$objet = myOwnLang::$object[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::RESV]);
		$resultheadok = $objet.' '.myOwnLang::getResult($obj, true, !$plur);
		$resultheadnok = $objet.' '.myOwnLang::getResult($obj, false, !$plur);

		$result=array();
		if (_PS_VERSION_ >= "1.5.0.0")
			$link = $obj->getContext()->link;
		else $link = new Link();

		$label = myOwnLang::getOperation($obj, MYOWN_OPE::CREATE, false);
		$tempId = $_POST['id'];
		$resa = new myOwnReservation($tempId, $_POST);
		$resa->_mainProduct = $obj->_products->getResProductFromProduct($resa->id_product);
		$resa->startTime = $resa->getStartHour();
		$resa->endTime = $resa->getEndHour();
		$resa->id_shop = myOwnUtils::getShop();
		$resa = myOwnResas::populateReservation($resa);
		$cart_id=0;
		if ($resa->id_order)
			$resa->id_cart = MyOwnUtils::getCartIdFromOrder($resa->id_order);
	
	 	$result['resultBool'] = ($resa->id_cart ? $resa->id_cartproduct = MyOwnReservationsUtils::updateCartDetail($obj, $resa, $resa->quantity, $resa->startDate, $resa->startTimeslot, $resa->endDate, $resa->endTimeslot) : 1) && $resa->sqlInsert();
	 	
	 	if ($result['resultBool']) {
	 		 $result['resultLabel'] = myOwnUtils::displayConf($resultheadok.' '.$label);
	 		 $inserted = Db::getInstance()->ExecuteS("SELECT LAST_INSERT_ID() AS ID;");
	 		 $resa->sqlId = $inserted[0]['ID'];
	 		 $resa->validate($obj, $resa->validated);
	 		 $result['resultId'] = $resa->sqlId;
	 	} else $result['resultLabel'] = myOwnUtils::displayError($resultheadnok.' '.$label);
	 	
	 	//update order
		if ($resa->id_order>0 && isset($_GET['refreshOrder'])) {
			$diff = MyOwnReservationsUtils::updateOrderDetail($obj, $resa, $cookie->id_lang, Tools::getIsset('refreshOrder'), Tools::getValue('updateInvoice'));
		}
		
	 	if (!MyOwnReservationsUtils::sendProdMsg($obj, $resa, strtotime($resa->startDate), $resa->getStartTimeSlot(), strtotime($resa->endDate), $resa->getEndTimeSlot(), intval($cookie->id_employee), $resa->_mainProduct, true))
			$result['resultLabel'] .= myOwnUtils::displayError($obj->l('but an error occurred while sending e-mail to', 'ajax_admin').' '.$obj->l('the product family owner', 'ajax_admin'));
		
		if (isset($_POST["notify"]) && $_POST["notify"]) {
			if (!MyOwnReservationsUtils::sendMsg($obj, $resa, strtotime($resa->startDate), $resa->getStartTimeSlot(), strtotime($resa->endDate), $resa->getEndTimeSlot(), intval($cookie->id_employee)))
				$result['resultLabel'] .= myOwnUtils::displayError($obj->l('but an error occurred while sending e-mail to', 'ajax_admin').' '.$obj->l('the customer', 'ajax_admin'));
		}
	 		
		//update object
		$productsImages=MyOwnReservationsUtils::getProductsImages(array($resa->id_product), $cookie->id_lang);
		$result['resultObj'] = $resa->toArray($cookie->id_lang, $link, $productsImages);
		return Tools::jsonEncode($result);
	}
	
	
	public static function delete($obj) {
		global $cookie, $rights;
		$male=false;$plur=false;
		$objet = myOwnLang::$object[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::RESV]);
		$resultheadok = $objet.' '.myOwnLang::getResult($obj, true, !$plur);
		$resultheadnok = $objet.' '.myOwnLang::getResult($obj, false, !$plur);
		$result=array();
		$label = myOwnLang::getOperation($obj, MYOWN_OPE::DELETE, false);
		$resa = new myOwnReservation($_GET['resa_id']);
		
		if (isset($_GET["updateStatus"]) && $resa->id_order>0) {
			$history = new OrderHistory();
			$history->id_order = (int)$resa->id_order;
			$history->id_employee = (int)($cookie->id_employee);
			$history->changeIdOrderState($_GET["updateStatus"], (int)$resa->id_order);
			$templateVars = array();
			$history->addWithemail(true, $templateVars);
		}
		$selectedItems=$_GET["selectedItems"];
		$selectedItemsTab=explode(',', $selectedItems);
		
		//take care deleting resa after status change
		if (!count(array_filter($selectedItemsTab))) $result['resultBool'] = myOwnResas::deleteReservation($_GET['resa_id']);
		else {
			myOwnResas::extractReservationItem($obj, $resa, $selectedItemsTab, true);
			$result['resultBool'] = true;
		}
			
	 	//update order
		if ($resa->id_order>0 && isset($_GET['refreshOrder'])) {
			$diff = MyOwnReservationsUtils::updateOrderDetail($obj, $resa, $cookie->id_lang, isset($_GET['refreshOrder']), intval($_GET['updateInvoice']), null);
		}
			
		if ($result['resultBool']) {
	 		 $result['resultLabel'] = myOwnUtils::displayConf($resultheadok.' '.$label);
	 	} else $result['resultLabel'] = myOwnUtils::displayError($resultheadnok.' '.$label);
	 	return Tools::jsonEncode($result);
	}
	//-----------------------------------------------------------------------
	//    DISPALY
	//-----------------------------------------------------------------------
	public static function display($productsFilter=array())
	{
		global $cookie, $rights;
		
		$obj = Module::getInstanceByName('myownreservations');
		//get rights
		$employee = new Employee($cookie->id_employee);
		$idTab = Tab::getIdFromClassName('admin'.$obj->name);
		$rights = Profile::getProfileAccess($employee->id_profile, $idTab);
		$_products = $obj->_products;

		/*if (Tools::getIsset('fc')) {
			foreach ($obj->_extensions->list as $ext) {
				if (get_class($ext)=='marketplace') {
					echo get_class($ext).'-';
					if (method_exists($ext, "getProductsCustomer")) {
						$_products->list = $ext->getProductsCustomer($cookie->id_customer);
					}
				}
			}
		} else*/
		if ($employee->id_profile>1) {
			$_products->list = $_products->getListForEmployee($employee->id);
		}
		
		$male=false;$plur=false;
		$objet = myOwnLang::$object[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::RESV]);
		$resultheadok = $objet.' '.myOwnLang::getResult($obj, true, !$plur);
		$resultheadnok = $objet.' '.myOwnLang::getResult($obj, false, !$plur);
		$result=array();
		
		if (isset($_POST['mycoolxmlbody'])) {
			//---------------------------------------- print reservations ---------------------------------------- 
			require_once MYOWNRES_PATH.'library/pdfGenerator.php';
			require_once MYOWNRES_PATH.'library/pdfWrapper.php';
			require_once MYOWNRES_PATH.'library/tcpdf_ext.php';
			$debug = false;
			function PDFErrorHandler ($errno, $errstr, $errfile, $errline) {
				global $xmlString;
				if ($errno < 1024) {
					echo $errstr."<br>";
					error_log($xmlString, 3, 'error_report_'.date("Y_m_d__H_i_s").'.xml');
					echo $xmlString;
					exit(1);
				}
			}
			
			$error_handler = set_error_handler("PDFErrorHandler");
			
			if (get_magic_quotes_gpc()) {
				$xmlString = stripslashes($_POST['mycoolxmlbody']);
			} else {
				$xmlString = $_POST['mycoolxmlbody'];
			}
			$xmlString =  urldecode ($xmlString);
			
			if ($debug == true) {
				error_log($xmlString, 3, 'debug_'.date("Y_m_d__H_i_s").'.xml');
			}
			
			$xml = new SimpleXMLElement($xmlString, LIBXML_NOCDATA);
			$scPDF = new schedulerPDF();
			$scPDF->printScheduler($xml);
		
		} else if (isset($_POST['action'])) {
			//---------------------------------------- reschedule reservation ---------------------------------------- 
			if ($_POST['action']=="reschedule") {
				$label = $obj->l('rescheduled', 'ajax_admin');
				
				$resa = new myOwnReservation($_POST['resa_id']);
				$oldResa = clone $resa;
				$resa->_mainProduct = $obj->_products->getResProductFromProduct($resa->id_product);
				$resa = myOwnResas::populateReservation($resa);
				$originalresa = clone $resa;
				$selectedItems=$_POST["selectedItems"];
				
				$reScheduleQty = intval($_POST["quantity"]);
				$reScheduleStart = $_POST["startDate"];
				$reScheduleStartTimeslot = $_POST["startTimeslot"];
				$reScheduleEnd = $_POST["endDate"];
				$reScheduleEndTimeslot = $_POST["endTimeslot"];
				$rescheduled = ($resa->startDate != $reScheduleStart
		 						|| $resa->startTimeslot != $reScheduleStartTimeslot
		 						|| $resa->endDate != $reScheduleEnd
		 						|| $resa->endTimeslot != $reScheduleEndTimeslot);
		 						
		 		if ($rescheduled && $selectedItems!='') {
					myOwnResas::extractReservationItem($obj, $resa, explode(',', $selectedItems), false, $reScheduleStart, $reScheduleStartTimeslot, $reScheduleEnd, $reScheduleEndTimeslot);
				} if ($selectedItems=='') {
					$resa->startDate = $reScheduleStart;
			 		$resa->startTimeslot = $reScheduleStartTimeslot;
			 		$resa->endDate = $reScheduleEnd;
			 		$resa->endTimeslot = $reScheduleEndTimeslot;
		 		}
		 		if ($resa->_mainProduct!=null) {
		    		$resa->startTime = $resa->_mainProduct->_timeslotsObj->list[$resa->startTimeslot]->startTime;
		    		$resa->endTime = $resa->_mainProduct->_timeslotsObj->list[$resa->endTimeslot]->endTime;
	    		}

		 		if ($selectedItems=='' && trim($resa->stock) != trim($_POST["stock"])) {
		 			$label = ucfirst(myOwnLang::$operations[MYOWN_OPE::UPDATE]);
		 			$result = myOwnResas::setReservationStock($resa->sqlId, $_POST["stock"]);
		 			$resa->stock=$_POST["stock"];
		 		}
		 		
		 		//check qty change
		 		$desiredQty = $_POST["quantity"];
		 		$action="";
		 		if (stripos('-', $desiredQty)!==null) {
		 			$action="remove";
		 		}
		 		if (abs(intval($desiredQty))>0) {
		 			if ($action=="") $action="add";
		 			$label = myOwnLang::$operations[MYOWN_OPE::UPDATE];
		 			$resa->quantity += $reScheduleQty;
		 			if (array_key_exists('qty_field', $_POST)) {
			 			//$_POST["qty_field"]
			 			foreach ($obj->_extensions->getExtByType(extension_type::CARTUPDATE) as $ext) {
							if (method_exists($ext, "execResaUpdate"))
								$ext->execResaUpdate($obj, $resa);
						}
		 			}
		 		}
		 		
			 	//update order
				if ($resa->id_order>0 && isset($_POST['refreshOrder'])) {
					$diff = MyOwnReservationsUtils::updateOrderDetail($obj, $resa, $cookie->id_lang, Tools::getValue('refreshOrder'), intval(Tools::getValue('updateInvoice')), $oldResa);
				}
		 		
		 		if (isset($_POST["updateStatus"]) && $resa->id_order>0) {
					$history = new OrderHistory();
					$history->id_order = (int)$resa->id_order;
					$history->id_employee = (int)($cookie->id_employee);
					$history->changeIdOrderState($_POST["updateStatus"], (int)$resa->id_order);
					$templateVars = array();
					$history->addWithemail(true, $templateVars);
				}
		 		
		 		$result=array();
		 		$result['resultBool'] = $resa->sqlUpdate();
		 		$result['resultStart'] = $resa->getStartDateTime();
		 		$result['resultEnd'] = $resa->getEndDateTime();
		 		
		 		if ($result['resultBool']) $result['resultLabel'] = myOwnUtils::displayConf($resultheadok.' '.$label);
		 		else $result['resultLabel'] = myOwnUtils::displayError($resultheadnok.' '.$label);
				
				if ($rescheduled)
		 			if (!MyOwnReservationsUtils::sendProdMsg($obj, $resa, strtotime($resa->startDate), $resa->getStartTimeSlot(), strtotime($resa->endDate), $resa->getEndTimeSlot(), intval($cookie->id_employee), $resa->_mainProduct, true))
		 				$result['resultLabel'] .= myOwnUtils::displayError($obj->l('but an error occurred while sending e-mail to', 'ajax_admin').' '.$obj->l('the product family owner', 'ajax_admin'));
		 		
		 		if (isset($_POST["notify"])) {
		 			if (!MyOwnReservationsUtils::sendMsg($obj, $resa, strtotime($resa->startDate), $resa->getStartTimeSlot(), strtotime($resa->endDate), $resa->getEndTimeSlot(), intval($cookie->id_employee)))
		 				$result['resultLabel'] .= myOwnUtils::displayError($obj->l('but an error occurred while sending e-mail to', 'ajax_admin').' '.$obj->l('the customer', 'ajax_admin'));
		 		}
		 		if ($selectedItems=='' && $resa->id_order>0) {
		 			MyOwnReservationsUtils::updateCartDetail($obj, $originalresa, $reScheduleQty, $reScheduleStart, $reScheduleStartTimeslot, $reScheduleEnd, $reScheduleEndTimeslot);
		 		}
		 		
		 		echo Tools::jsonEncode($result);
			}
		} else  if (isset($_POST['resa_id']) or isset($_GET['resa_id'])) {
		
			//---------------------------------------- get reservation ---------------------------------------- 
			
			
			if (stripos($_POST['resa_id'], ',')) {
				$resa=array();
					foreach(explode(',', $_POST['resa_id']) as $resa_id)
						if (intval($resa_id)>0) {
							$resat=new myOwnReservation($resa_id);
							$resat->_mainProduct = $obj->_products->getResProductFromProduct($resat->id_product);
							$resat = myOwnResas::populateReservation($resat);
							$resa[]=$resat;
						}
			} else {
				$resa = new myOwnReservation($_POST['resa_id']);
				//when drad new resa
				if (!$resa->_saved && isset($_POST['product']) && trim($_POST['product'])!='') {
					$prod = explode('-', $_POST['product']);
					$resa->id_product = $prod[0];
					$resa->id_product_attribute = $prod[1];
				} else if (!$resa->_saved && isset($_POST['productFamilyFilter'])) {
					$resa->_mainProduct = $obj->_products->list[(int)$_POST['productFamilyFilter']];
				}
				if ($resa->id_product) $resa->_mainProduct = $obj->_products->getResProductFromProduct($resa->id_product);
				$resa = myOwnResas::populateReservation($resa);
			}
			myOwnReservationsReservationsController::show($obj, $cookie, $resa);
		} else {
			//---------------------------------------- reservations JSON list ---------------------------------------- 
			if (_PS_VERSION_ >= "1.5.0.0")
				$link = $obj->getContext()->link;
			else $link = new Link();
			$link->protocol_content  = (Tools::usingSecureMode() && Configuration::get('PS_SSL_ENABLED'));	
		
			//get reservations data
			$resas = myOwnResas::getReservations();
			$resasScheduler=array();
			$productsIds=array();
			foreach ($resas as $resa)
				$productsIds[$resa->id_product]=$resa->id_product;
			$productsImages=MyOwnReservationsUtils::getProductsImages($productsIds, $cookie->id_lang);
		
			//build content
			foreach ($resas as $resa) 
				if ($productsFilter==array() || in_array($resa->id_product, $productsFilter)) {
					$resa = myOwnResas::populateReservation($resa);
					$resa->_mainProduct = $_products->getResProductFromProduct($resa->id_product);
					//dismiss reservation if mainProduct null if not admin
					if ($employee->id_profile<2 or $resa->_mainProduct!=null) {
						if (MYOWNRES_WEEK_TS && $resa->_mainProduct->_reservationUnit==reservation_unit::WEEK) {
							if ($resa->_mainProduct->_reservationSelWeekStart < $resa->_mainProduct->_reservationSelWeekEnd) {
								//echo '<br>@'.$resa->sqlId.'='.$resa->startDate.'_'.$resa->endDate;
								for($j = $resa->_mainProduct->_reservationSelWeekStart; $j <= $resa->_mainProduct->_reservationSelWeekEnd; $j++) {
									 $resa->startDate = $resa->endDate = date('Y-m-d', strtotime('+'.($j-1).' day', strtotime( date('o',strtotime($resa->startDate)).'W'.date('W',strtotime($resa->startDate)) ) ) );
									 $resa->endTimeslot = $resa->startTimeslot;
									 $resasScheduler[]=$resa->toArray($cookie->id_lang, $link, $productsImages, $j);
								}
							}
						} else {
							$resasScheduler[]=$resa->toArray($cookie->id_lang, $link, $productsImages);
						}
					}
				}
		
			if (Tools::getValue('goupedView')) {
				$resasSchedulerTemp=array();
				foreach ($resasScheduler as $resasSchedulerItem) {
					if (!is_array($resasSchedulerTemp[$resasSchedulerItem['periodKey'].'_'.$resasSchedulerItem['object']]))
						$resasSchedulerTemp[$resasSchedulerItem['periodKey'].'_'.$resasSchedulerItem['object']]=array();
					$resasSchedulerTemp[$resasSchedulerItem['periodKey'].'_'.$resasSchedulerItem['object']][]=$resasSchedulerItem;
				}
				$resasScheduler=array();
				foreach ($resasSchedulerTemp as $resasSchedulerTempItemArray) {
					$qty=0;
					$resas=array();
					$validated=true;
					foreach ($resasSchedulerTempItemArray as $resasSchedulerTempItem) {
						$qty += $resasSchedulerTempItem['quantity'];
						if (!$resasSchedulerTempItem['validated'])
							$validated=false;
						$resas[]=$resasSchedulerTempItem['id_reservation'];
					}
					$resasSchedulerTempItemArray[0]['id'] = implode(',', $resas);
					if (stripos($resasSchedulerTempItemArray[0]['id'], ',')===false)
						$resasSchedulerTempItemArray[0]['id'].=',';
					if ($resasSchedulerTempItemArray[0]['repeat_index']>0)
						$resasSchedulerTempItemArray[0]['id'] .= '#'.$resasSchedulerTempItemArray[0]['repeat_index'];
					$resasSchedulerTempItemArray[0]['quantity'] = $qty;
					$resasSchedulerTempItemArray[0]['validated'] = $validated;
					$resasSchedulerTempItemArray[0]['id_order'] = $resasSchedulerTempItemArray[0]['ref_order'] = count($resasSchedulerTempItemArray);
					$resasScheduler[]=$resasSchedulerTempItemArray[0];
				}
			}
							
			die(Tools::jsonEncode($resasScheduler));
		}
	}
	
	//-----------------------------------------------------------------------
	//    EXPORT
	//-----------------------------------------------------------------------
	public static function exportConfig($tab) {
		$_products = new myOwnProductsRules();
		$id_shop = myOwnUtils::getShop();
		header('Content-type: text/csv');
		header('Content-Disposition: attachment; filename="'.$tab.'_'.date('Y-m-d').'.csv"');

		$productFamillyFilter= Tools::getValue('productFamillyFilter', 0);
		$in='';
		if (intval($productFamillyFilter)>0) {
			$prodcutFamilly= $_products->list[$productFamillyFilter];
			$products=$prodcutFamilly->getProductsId();
			$in=implode(',', $products);
			if ($tab=='pricesrules')
				$in=' WHERE product in ('.$in.') OR product = 0';
			else if ($tab=='availability')
				$in=' WHERE id_product in ('.$in.') OR id_product = 0';
			else if ($tab=='products')
				$in=' WHERE id_product = '.$productFamillyFilter;
			else if ($tab=='stock')
				$in=' WHERE product_id in ('.$in.')';
			else if ($tab=='timeslot')
				$in=' WHERE id_product in ('.$in.') OR id_product = 0';
		}
		$dataTable="";
		if ($tab=='pricesrules')
			$dataTable='myownreservations_pricesrule';
		else if ($tab=='availability')
			$dataTable='myownreservations_availability';
		else if ($tab=='products')
			$dataTable='myownreservations_products';
		else if ($tab=='pricessets')
			$dataTable='myownreservations_pricesset';
		else if ($tab=='stock')
			$dataTable='myownreservations_stock';
		else if ($tab=='timeslots' or $tab=='timeslot')
			$dataTable='myownreservations_timeslot';
						
		$req = "SELECT * FROM `" . _DB_PREFIX_.$dataTable."`".$in;
	    $orders = Db::getInstance()->ExecuteS($req);
	
	    $cnt=0;$firstvalue="";
	    if (is_array($orders))
		foreach($orders AS $order) {
			if ($cnt==0) {
				$outline="";
				foreach($order AS $key => $value) {
					if ($outline!="") $outline.=";";
					$outline.=$key;
				}
				echo $outline."\r\n";
			}
			$outline="";
			if (is_array($order))
			foreach($order AS $key => $value) {
				if ($outline!="") $outline.=";";
				else $firstvalue=$value;
				$outline.=str_replace(";", "|", $value);
			}
			if (intval($firstvalue)>0) echo $outline."\r\n";
			$cnt++;
		}
		
		
	}
}
?>