<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnReservationsAvailabilitiesController {
	
	//==============================================================================================================================================
	//     SHOW AVAILABILITIES
	//==============================================================================================================================================
	
	public static function sortIdAsc($a, $b)
	{
		if ($a->sqlId == $b->sqlId) return 0;
			return ($a->sqlId < $b->sqlId) ? - 1 : 1;
	}
	public static function sortIdDesc($a, $b)
	{
		if ($a->sqlId == $b->sqlId) return 0;
			return ($a->sqlId > $b->sqlId) ? - 1 : 1;
	}

	public static function sortNameAsc($a, $b)
	{
		if ($a->id_product == $b->id_product) return 0;
		return ($a->id_product < $b->id_product) ? - 1 : 1;
	}
	public static function sortNameDesc($a, $b)
	{
		if ($a->id_product == $b->id_product) return 0;
		return ($a->id_product > $b->id_product) ? - 1 : 1;
	}
	public static function sortStartDateAsc($a, $b)
	{
		if ($a->startDate == $b->startDate) return 0;
		return (strtotime($a->startDate) < strtotime($b->startDate)) ? - 1 : 1;
	}
	public static function sortStartDateDesc($a, $b)
	{
		if ($a->startDate == $b->startDate) return 0;
		return (strtotime($a->startDate) > strtotime($b->startDate)) ? - 1 : 1;
	}
	public static function sortEndDateAsc($a, $b)
	{
		if ($a->endDate == $b->endDate) return 0;
		return (strtotime($a->endDate) < strtotime($b->endDate)) ? - 1 : 1;
	}
	public static function sortEndDateDesc($a, $b)
	{
		if ($a->endDate == $b->endDate) return 0;
		return (strtotime($a->endDate) > strtotime($b->endDate)) ? - 1 : 1;
	}
	public static function show($from, $obj, $id_product=null, $productsFilter=array()) {
		global $cookie;
		$employee = new Employee($cookie->id_employee);
		$idTab = Tab::getIdFromClassName('adminmyownreservations');
		$rights = Profile::getProfileAccess($employee->id_profile, $idTab);
		$isfront=($productsFilter!=array());
		if ($isfront) {
			$rights['delete']=1;
		}
		$formurl = ($isfront ? $_SERVER['REQUEST_URI'] : myOwnReservationsController::getConfUrl('availability'));
		$langue=$cookie->id_lang;
		$obj->checkIsProVersion();
		$odd=false;
		$output='';
		$days = myOwnLang::getDaysNames($langue);
		$ext_object=null;
		$res_object=null;
		foreach ($obj->_extensions->getExtByType(extension_type::OBJECT) as $ext) {
			if (method_exists($ext, "objectSelect"))
				if ($id_product>0 || $ext->id!=11)
					$ext_object = $ext;
			if ($id_product==0) {
				if (method_exists($ext, "objectSelect"))
					if ($ext->id==11)
						$res_object = $ext;
			}
		}
		$is_qty=false;
		foreach($obj->_products->list as $mProd) 
			if($mProd->productType==product_type::AVAILABLE)
				$is_qty=true;
		
		$mainProduct = null;
		$manageQty=false;
		if ($id_product>0) $mainProduct = $obj->_products->getResProductFromProduct($id_product);
		if ($mainProduct !=null && $mainProduct->productType==product_type::AVAILABLE) $manageQty=true;
		
		if (Tools::getIsset('submitReset', ''))
		{
			unset($obj->getContext()->cookie->myown_filter_product);
			unset($obj->getContext()->cookie->myown_filter_startdate);
			unset($obj->getContext()->cookie->myown_filter_enddate);

			$product_filter = '';
			$startdate_filter = 0;
			$enddate_filter = 0;
			$isfiltered = false;
		}
		else
		{
			$filter_product = Tools::getValue('filter_name', '');
			if ($filter_product != '') $obj->getContext()->cookie->myown_filter_product = $filter_product;
			if (isset($obj->getContext()->cookie->myown_filter_product)) $product_filter = $obj->getContext()->cookie->myown_filter_product;
			else $product_filter = '';

			if (Tools::getIsset('filter_startdate')) $obj->getContext()->cookie->myown_filter_startdate = Tools::getValue('filter_startdate');
			if (Tools::getIsset('filter_enddate')) $obj->getContext()->cookie->myown_filter_enddate = Tools::getValue('filter_enddate');
			if (isset($obj->getContext()->cookie->myown_filter_startdate)) $startdate_filter = strtotime($obj->getContext()->cookie->myown_filter_startdate);
			else $startdate_filter = 0;
			if (isset($obj->getContext()->cookie->myown_filter_enddate)) $enddate_filter = strtotime($obj->getContext()->cookie->myown_filter_enddate);
			else $enddate_filter = 0;

			$isfiltered = ($product_filter != '' || $startdate_filter > 0 || $enddate_filter > 0);
		}
		
		if (!$id_product && $from=='object')
			$sorted_availabilities_list = array();
		else
			$sorted_availabilities_list = myOwnAvailabilities::getAvailabilitiesFiltered($startdate_filter, $enddate_filter, ($productsFilter==array() ? $id_product : $productsFilter), $product_filter, $langue);
		
		$orderby = Tools::getValue('orderby', 'id');
		$orderway = Tools::getValue('orderway', 'asc');

		$orderway = Tools::getValue('orderway', 'asc');

		if ($orderby == 'id' && $orderway == 'asc') uasort($sorted_availabilities_list, array('myOwnReservationsAvailabilitiesController', 'sortIdAsc') );
		if ($orderby == 'id' && $orderway == 'desc') uasort($sorted_availabilities_list, array('myOwnReservationsAvailabilitiesController', 'sortIdDesc') );
		if ($orderby == 'product' && $orderway == 'asc') uasort($sorted_availabilities_list, array('myOwnReservationsAvailabilitiesController', 'sortNameAsc') );
		if ($orderby == 'product' && $orderway == 'desc') uasort($sorted_availabilities_list, array('myOwnReservationsAvailabilitiesController', 'sortNameDesc') );
		if ($orderby == 'startdate' && $orderway == 'asc') uasort($sorted_availabilities_list, array('myOwnReservationsAvailabilitiesController', 'sortStartDateAsc') );
		if ($orderby == 'startdate' && $orderway == 'desc') uasort($sorted_availabilities_list, array('myOwnReservationsAvailabilitiesController', 'sortStartDateDesc') );
		if ($orderby == 'enddate' && $orderway == 'asc') uasort($sorted_availabilities_list, array('myOwnReservationsAvailabilitiesController', 'sortEndDateAsc') );
		if ($orderby == 'enddate' && $orderway == 'desc') uasort($sorted_availabilities_list, array('myOwnReservationsAvailabilitiesController', 'sortEndDateDesc') );

		$output .= '
		<script type="text/javascript">
		function editAvailability(id) {
			
			for(var index in jsholidays) {
				if (jsholidays[index].sqlId==id) {
					var jsholiday = jsholidays[index];
// 					
					$(".availabilityType").each(function() {
						$(this).attr("checked", ($(this).val()==jsholiday.type) );
					});
					
					$("#holidayFamily option").each(function() {
						$(this).attr("selected", ($(this).val()==jsholiday.id_family || ($(this).val()=="" && jsholiday.id_family==0) ) );
					});
					populateProducts(jsholiday.id_family);

					if (jsholiday.id_product!=null) $("#holidayProduct").val(jsholiday.id_product);
					else $("#holidayProduct").val(0);
					$("#holidayProduct").trigger("chosen:updated");
					populateAttribute();
					populateUnits();
					
					$("#holidayProductAttribute option").each(function() {
						$(this).attr("selected", ($(this).val()==jsholiday.id_product_attribute) );
					});
					$("#holidayProductAttribute").val(jsholiday.id_product_attribute);
					
					$("#holidayStart").val(jsholiday.startDate);
					$("#holidayEnd").val(jsholiday.endDate);
					populateTimeslot("start");
					$("#holidayStartTimeslot option").each(function() {
						$(this).attr("selected", ($(this).val()==jsholiday.startTimeslot) );
					});
					populateTimeslot("end");
					$("#holidayEndTimeslot option").each(function() {
						$(this).attr("selected", ($(this).val()==jsholiday.endTimeslot) );
					});
// 					$("#holidayStartTimeslot").val(jsholiday.startTimeslot);
// 					$("#holidayEndTimeslot").val(jsholiday.endTimeslot);
					
					$("#holidayQuantity").val(jsholiday.quantity);
					checkHolidayQty();
					
					$("#holidayObject option").each(function() {
						$(this).attr("selected", ($(this).val()==jsholiday.id_object) );
					});
					
					
					editAvailability_show(id);
				}
			}
		}
		function addAvailability_show() {
			$("#editAvailability_id").val(0);
						
			$("#editAvailability_legend").hide();
			$("#editAvailability_submit").hide();
			$("#addAvailability_legend").show();
			$("#addAvailability_submit").show();
			
			$("#addAvailability").show();
		}
		function editAvailability_show(id) {
			$("#editAvailability_legend span.badge").html("#"+id);
			$("#editAvailability_id").val(id);
			
			$("#editAvailability_legend").show();
			$("#editAvailability_submit").show();
			$("#addAvailability_legend").hide();
			$("#addAvailability_submit").hide();
			
			$("#addAvailability").show();
		}
		var jsholidays = JSON.parse(\''.json_encode($sorted_availabilities_list).'\');

		</script>';
		
		if ($id_product==null)
			$output .= '
		<form action="'.$formurl.'" id="availabilities_frm" method="post">
			<input type="hidden" id="orderway" name="orderway" value="'.$orderway.'">
			<input type="hidden" id="orderby" name="orderby" value="'.$orderby.'">
			';
		if (_PS_VERSION_ < "1.5.0.0")
			$output .= '
			<fieldset class="settingsList">
				<legend><img src="../modules/'.myOwnReservationsController::$name.'/img/calendar_available.png" />'.ucfirst(myOwnLang::$objects[MYOWN_OBJ::AVAILABILITY]).'</legend>
				 
				<span class="myowndeliveriesSettingsPreTable" style="width:850px">'.ucfirst(myOwnLang::$actions[MYOWN_OPE::EDIT]).' '.ucfirst(myOwnLang::$objects[MYOWN_OBJ::AVAILABILITY]).' :</span>
				<br /><br />';
		else if ($id_product==null) $output .= myOwnUtils::displayInfo(myOwnHelp::$objects['availability']['help'], false);
		
		$btns='';
		if ($id_product!=null) {
			$btns = '
			<span class="panel-heading-action">
			<a id="desc-customer-export" class="list-toolbar-btn" onclick="$(\'#addAvailability\').toggle();">
				<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="'.ucfirst(myOwnLang::$actions[MYOWN_OPE::ADD]).'" data-html="true">
					<i class="process-icon-new "></i>
				</span>
			</a>
			</span>';
		}
						
		if (_PS_VERSION_ >= "1.6.0.0" && $id_product>=0 && $from!='object') {
			$output .= MyOwnReservationsUtils::table16wrap($obj, ($id_product==null ? 'availability' : ''), ($id_product==null ? 'addAvailability_show();' : ''), ($productsFilter==array() ? ucfirst(myOwnLang::$objects[MYOWN_OBJ::AVAILABILITY]) : ''), ($productsFilter==array() ? count($sorted_availabilities_list) : -1), $btns);
		}
		$nocolspan=6;
		if ($obj->isProVersion && ($id_product==null || $id_product<0)) $nocolspan++;
		if ($is_qty) $nocolspan++;
		if ($ext_object!=null) $nocolspan++;
		if ($id_product == null) $nocolspan++;
		$output .= '
				<table style="width:100%;background-color:#FFFFFF;table-layout:fixed" class="table">
					<thead>
						<tr>
							<th style="width:55px;padding-right:0px;padding-left:0px;" align="left">'.$obj->l('ID', 'availabilities').'
								'.($id_product==0 ? MyOwnReservationsUtils::getOrderLink('', 'id'): '').'</th>
							<th style="text-align:center;">'.$obj->l('Type', 'availabilities').'</th>
							'.($obj->isProVersion && ($id_product==null || $id_product<0) ? '<th style="text-align:center;">'.ucfirst(myOwnLang::$object[MYOWN_OBJ::FAMILY]).'</th>' : '').'
							<th style="text-align:left;">'.($id_product == null ? ucfirst(myOwnLang::$object[MYOWN_OBJ::PRODUCT]).($res_object!=null ? ' / '.$res_object->label : '').'
								'.($id_product==0 ? MyOwnReservationsUtils::getOrderLink('', 'product'): '') : ($manageQty ? $obj->l('Quantity', 'availabilities') : '')).'</th>
							<th style="text-align:left;">'.$obj->l('Attribute', 'availabilities').'</th>
							<th style="text-align:left;width:25%">'.$obj->l('Start date', 'availabilities').'
								'.($id_product==0 ? MyOwnReservationsUtils::getOrderLink('', 'startdate'): '').'</th>
							<th style="text-align:left;width:25%">'.$obj->l('End date', 'availabilities').'
								'.($id_product==0 ? MyOwnReservationsUtils::getOrderLink('', 'enddate'): '').'</th>
							'.($is_qty ? '<th style="text-align:center;">'.$obj->l('Quantity', 'availabilities').'</th>' : '').'
							'.($ext_object!=null ? '<th style="text-align:center;">'.$ext_object->title.'</th>' : '').'
							'.($id_product == null ? '<th style="width:100px;text-align:center;">'.$obj->l('Action', 'availabilities').'</th>' : '').'
						</tr>';
						if ($id_product==null) {
							$output .= '
							<tr class="nodrag nodrop filter" style="height: 35px;">
								<th></th>
								<th  class="center"></th>
								'.($obj->isProVersion && $id_product==null ? '<th></th>' : '').'
								<th>'.($id_product==null ? '<input type="text" name="filter_name" value="'.$product_filter.'" style="width:90%;" onkeypress="formSubmit(event, \'submitFilterButton\');">' : '').'</th>
								<th></th>
								<th class="center">
									<div class="input-group fixed-width-md row-margin-bottom">
										<input type="text" class="filter datepicker date-input form-control" name="filter_startdate" id="filter_startdate" value="'.($startdate_filter > 0 ? date('Y-m-d', $startdate_filter) : '').'" placeholder="'.$obj->l('From', 'deliveries').'">
										<span class="input-group-addon">
											<i class="icon-calendar"></i>
										</span>
									</div>
								</th>
								<th class="center">
									<div class="input-group fixed-width-md">
										<input type="text" class="filter datepicker date-input form-control" name="filter_enddate" id="filter_enddate" value="'.($enddate_filter > 0 ? date('Y-m-d', $enddate_filter) : '').'" placeholder="'.$obj->l('To', 'deliveries').'">
										<span class="input-group-addon">
											<i class="icon-calendar"></i>
										</span>
									</div>
								</th>
							'.($is_qty ? '<th style="text-align:center;">--</th>' : '').'
							'.($ext_object!=null ? '<th style="text-align:center;">--</th>' : '').'
								<th class="center">'.(_PS_VERSION_ < '1.6.0.0' ? '' : '<span class="pull-right">
									<button type="submit" id="" name="submitFilter" class="btn btn-default" onclick="$(\'#availabilities_frm\').submit()">
										<i class="icon-search"></i> '.$obj->l('Filter', 'deliveries').'
									</button>
									'.($isfiltered ? '<button type="submit" name="submitReset" class="btn btn-warning">
											<i class="icon-eraser"></i> '.$obj->l('reset', 'deliveries').'
										</button>' : '').'</span>').'
								</th>
						</tr>';
						}
					$output .= '
					</thead>
					<tbody>';
							
				if (count($sorted_availabilities_list)==0) {
					if (_PS_VERSION_ < "1.6.0.0") $output .= '
					<tr>
						<td colspan="'.$nocolspan.'">'.ucfirst(myOwnLang::$operations[MYOWN_OPE::NORES]).' '.ucfirst(myOwnLang::$objects[MYOWN_OBJ::AVAILABILITY]).'</td>
					</tr>';
					else $output .= '
					<tr>
						<td class="list-empty" colspan="'.$nocolspan.'">
							<div class="list-empty-msg">
								<i class="icon-warning-sign list-empty-icon"></i>
								'.ucfirst(myOwnLang::$operations[MYOWN_OPE::NORES]).' '.ucfirst(myOwnLang::$objects[MYOWN_OBJ::AVAILABILITY]).'
							</div>
						</td>
					</tr>';
				} else
					foreach($sorted_availabilities_list AS $holiday) {
						$odd=!$odd;
						$product='';
						if ($id_product==null) {
							$product = $obj->l('All', 'availabilities');
							if ($holiday->id_product >0)
								$product = MyOwnReservationsUtils::getProductName($holiday->id_product, $langue);
							else if ($holiday->id_product <0 && $res_object!=null) {
								$product = (array_key_exists(-$holiday->id_product, $res_object->objects->list) ? '<span class="badge"><i class="icon-'.$res_object->icon.'"></i> <a style="color:#FFF" href="'.myOwnReservationsController::getConfUrl($res_object->name).'&edit'.strtolower($res_object->field).'='.-$holiday->id_product.'">'.$res_object->objects->list[-$holiday->id_product]->name.'</span>' : '');
							}
						}
						$productattribute = $obj->l('All', 'availabilities');
						$startTimeslot="";$endTimeslot="";
						if ($holiday->startTimeslot) {
							if (array_key_exists($holiday->startTimeslot, $obj->_timeSlots->list)) $startTimeslot=$obj->_timeSlots->list[$holiday->startTimeslot]->name;
							else $startTimeslot=$obj->l('unknown time', 'availabilities').' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::TIMESLOT]);
						}
						if ($holiday->endTimeslot) {
							if (array_key_exists($holiday->endTimeslot, $obj->_timeSlots->list)) $endTimeslot=$obj->_timeSlots->list[$holiday->endTimeslot]->name;
							else $endTimeslot=$obj->l('unknown time', 'availabilities').' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::TIMESLOT]);
						}
						if ($holiday->id_product_attribute >0) $productattribute = MyOwnReservationsUtils::getAttributeCombinaisons($holiday->id_product,$holiday->id_product_attribute,$langue);
						$output .= '
					<tr class="row_hover'.($odd==0 ? ' alt_row odd' : '').'">
						<td>'.($rights['delete'] ? '<div class="'.(file_exists(_PS_ROOT_DIR_."/override/classes/Tab.php") ? 'checkbox checkbox-only' : '').'"><input type="checkbox" name="deleteAvailabilities[]" id="availability'.$holiday->sqlId.'" value="'.$holiday->sqlId.'" '.($id_product!=null && $id_product>0 && $holiday->id_product!=$id_product ? 'disabled' : '').'> ' : '').'<label for="availability'.$holiday->sqlId.'">'.$holiday->sqlId.'</label></div></td>
						<td style="text-align:center;padding:7px">'.self::printAvailabilityType($obj, $holiday->type).'</td>
						'.($obj->isProVersion && ($id_product==null || $id_product<0) ? '<td style="text-align:center;">'.($holiday->id_family >0 ? (array_key_exists($holiday->id_family, $obj->_products->list) ? trim($obj->_products->list[$holiday->id_family]->name)!='' ? $obj->_products->list[$holiday->id_family]->name :  '#'.$holiday->id_family : '#'.$holiday->id_family) : $obj->l('All', 'availabilities')).'</td>' : '').'
						<td>'.$product.($manageQty && $holiday->quantity && $holiday->type==1 ? ' <span class="badge badge-success"> x'.$holiday->quantity.'</span>' : '').'</td>
						<td>'.$productattribute.'</td>
						<td>'.MyOwnCalendarTool::formatDateWithDay(strtotime($holiday->startDate),$langue).' '.($startTimeslot!='' ? $obj->l('at', 'availabilities').' '.$startTimeslot : '').'</td>
						<td>'.MyOwnCalendarTool::formatDateWithDay(strtotime($holiday->endDate),$langue).' '.($endTimeslot!='' ? $obj->l('at', 'availabilities').' '.$endTimeslot : '').'</td>
						'.($is_qty ? '<td style="text-align:center">'.($holiday->id_family && array_key_exists($holiday->id_family, $obj->_products->list) && $obj->_products->list[$holiday->id_family]->productType==product_type::AVAILABLE ? $holiday->quantity : '--').'</td>' : '').'
						'.($ext_object!=null ? '<td>'.(array_key_exists($holiday->id_object, $ext_object->objects->list) ? '<span class="badge badge-success"><i class="icon-'.$ext_object->icon.'"></i> <a style="color:#FFF" href="'.myOwnReservationsController::getConfUrl($ext_object->name).'&edit'.strtolower($ext_object->field).'='.$holiday->id_object.'">'.$ext_object->objects->list[$holiday->id_object]->name.'</span>' : '').'</th>' : '').'';
						if ($id_product==null) $output .= '
						<td style="text-align:center;">
							'.($isfront ? '' :'<a class="edit btn btn-default" name="" onclick="editAvailability('.$holiday->sqlId.');" value="" class="button btn btn-default" style="width:100%;" /><i class="icon-pencil"></i> ' . ucfirst(myOwnLang::$actions[MYOWN_OPE::EDIT]) . '</a>').'
							
						</td>';
						else $output .= '';
						$output .= '
					</tr>';
				}
				
			  						
		$output .= '
					</tbody>
				</table>';
				
		if (_PS_VERSION_ >= "1.6.0.0") {
			if (count($sorted_availabilities_list) && $rights['delete']) {
				if (!$isfront) {
					$actions = array('submitAddproductAndStay' => '<i class="icon-trash"></i>&nbsp;'.ucfirst(myOwnLang::$actions[MYOWN_OPE::DELETE]));
					$output .= MyOwnReservationsUtils::displayRow($obj, $actions, 'deleteAvailabilities').'<div style="clear:both"></div>';
				} else $output .= '
					<div style="float:left;">
						<input type="submit" name="submitDeleteAvailabilities" value="'.ucfirst(myOwnLang::$actions[MYOWN_OPE::DELETE]).'" class="button" style="width:100%;margin-top:10px">
					</div>';
			}
			if ($id_product>=0 && $from!='object') $output .= '</div>';
			
		}
		if (_PS_VERSION_ < "1.6.0.0") {
			if (count($sorted_availabilities_list) && $rights['delete'])
				$output .= '
					<div style="float:left;">
						<input type="submit" name="submitDeleteAvailabilities" value="'.ucfirst(myOwnLang::$actions[MYOWN_OPE::DELETE]).'" class="button" style="width:100%;margin-top:10px">
					</div>';
			$output .= '
				</fieldset>';
		}

		if ($id_product==null)
			$output .= '
		</form>';
		return $output;
				
	}
	
	public static function printAvailabilityType($obj, $type)
	{
		if ($type)
			return '<span class="label label-success" style="padding: 4px 6px;">
						<i class="icon-check"></i>
						'.ucfirst(myOwnLang::$object[MYOWN_OBJ::AVAILABILITY]).'
					</span>';
		else 
			return '<span class="label label-danger" style="padding: 4px 6px;">
						<i class="icon-remove"></i>
						'.$obj->l('Unavailability', 'availabilities').'
					</span>';
	}
	
	public static function checkEdit($obj) {
		$errors=array();
		if (!isset($_POST["holidayStart"]) || strlen($_POST["holidayStart"]) == 0) return false;
		
		if (strlen($_POST["holidayStart"]) > 0 OR strlen($_POST["holidayEnd"])>0) {
				if (preg_match( "@^([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})$@i" , $_POST["holidayStart"])== 0 ) {
					$errors[] = $obj->l('A holiday bound must be formated like YYYY-MM-DD', 'availabilities'); 
				} elseif (preg_match( "@^([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})$@i" , $_POST["holidayEnd"])== 0 ) {
					$errors[] = $obj->l('A holiday bound must be formated like YYYY-MM-DD', 'availabilities');
				} elseif (strtotime($_POST["holidayStart"]) > strtotime($_POST["holidayEnd"])) {
					$errors[] = $obj->l('The end date date must be later or equal than the start date', 'availabilities');
				}
		}

		return $errors;
	}
	
	// ADD AVAILABILITY
	//-----------------------------------------------------------------------------------------------------------------
	public static function addAction($obj, &$msg) {
		if (isset($_POST["holidayStart"]) && strlen($_POST["holidayStart"]) > 0 && strlen($_POST["holidayEnd"])>0) {
			$tempAvailability = new myOwnAvailability();
			//if (isset( $_POST['holidayProduct'])) $tempAvailability->id_product = $_POST['holidayProduct'];
			if (isset($_POST['idAvailability']))
				$tempAvailability->sqlId=$_POST['idAvailability'];
			if (isset($_POST['holidayProduct'])) {
				if (is_array($_POST['holidayProduct']))
					$tempAvailability->id_product = implode('', $_POST['holidayProduct']);
				else {
					$tempAvailability->id_product = $_POST['holidayProduct'];
					$mainProduct = $obj->_products->getResProductFromProduct($tempAvailability->id_product);
					if ($mainProduct!=null) $tempAvailability->id_family = $mainProduct->sqlId;
				}
			}
			$objet = myOwnLang::$object[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::AVAILABILITY]);
			$tempAvailability->id_family = Tools::getValue('holidayFamily', $tempAvailability->id_family);
			$tempAvailability->id_product_attribute = Tools::getValue('holidayProductAttribute', 0);
			$tempAvailability->type = $_POST['holidayType'];
			$tempAvailability->startDate = $_POST['holidayStart'];
			$tempAvailability->endDate = $_POST['holidayEnd'];
			if (isset( $_POST['holidayObject'])) $tempAvailability->id_object  = $_POST['holidayObject'];
			$tempAvailability->startTimeslot = Tools::getValue('holidayStartTimeslot',0);
			$tempAvailability->endTimeslot = Tools::getValue('holidayEndTimeslot',0);
			$tempAvailability->quantity = Tools::getValue('holidayQty',0);
			if ($tempAvailability->sqlId==0)
				$result = $tempAvailability->sqlInsert();
			else $result = $tempAvailability->sqlUpdate();
			$operation = myOwnLang::getOperation($obj, MYOWN_OPE::CREATE);
			if ($result) $msg = $objet.' '.myOwnLang::getResult($obj, true, true).' '.$operation;
			else $msg = $objet.' '.myOwnLang::getResult($obj, true, true).' '.$operation.' : '.Db::getInstance()->getMsgError();
			return $result;
		}
		return false;
	}
	
	// DELETE AVAILABILITIES
	//-----------------------------------------------------------------------------------------------------------------
	public static function deleteAction($obj, &$msg) {
		$result = false;
		if (isset($_POST["deleteAvailabilities"])) {
			$result = true;
			foreach ($_POST["deleteAvailabilities"] as $availabilityToDelete) {
				$tempAvailability = new myOwnAvailability();
				$tempAvailability->sqlId = $availabilityToDelete;
				$result &= $tempAvailability->sqlDelete();
			}
			$operation = myOwnLang::getOperation($obj, MYOWN_OPE::DELETE, false);
			if (count($_POST["deleteAvailabilities"])==1) $objet = myOwnLang::$object[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::AVAILABILITY]);
			else $objet = myOwnLang::$objects[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$objects[MYOWN_OBJ::AVAILABILITY]);
			if ($result) $msg = $objet.' '.myOwnLang::getResult($obj, true, count($_POST["deleteAvailabilities"])==1).' '.$operation;
			else $msg = $objet.' '.myOwnLang::getResult($obj, true, true).' '.$operation.' : '.Db::getInstance()->getMsgError();
		}
		return $result;
	}
		
	//==============================================================================================================================================
	//     ADD AVAILABILITIES
	//==============================================================================================================================================
	
	public static function add($from, $obj, $id_product=0, $productsFilter=array()) {
		global $cookie;
		$langue=$cookie->id_lang;
		$output='';
		$products=$obj->_products->getProducts($langue);
		$isfront = ($productsFilter!=array());
		
		myOwnLang::translate($obj);
	if ($id_product>0) {
		$mainProduct = $obj->_products->getResProductFromProduct($id_product);
		if ($mainProduct!=null && $mainProduct->_reservationSelExt!=null) {
			$args=$mainProduct->_reservationSelExt->getWidgetArgs($mainProduct->_reservationSelDays);
			
		} elseif ($mainProduct!=null && ($mainProduct->reservationSelType == reservation_unit::DAY)) { 
			$args = 'showWeek: true,
					dateFormat:"yy-mm-dd",

					beforeShowDay: function(date) {
							var seldays = '.Tools::jsonEncode(explode(',',$mainProduct->_reservationSelDays)).';
							var dayofweek = date.getDay();
							if (dayofweek==0) dayofweek=7;
							if (seldays[dayofweek]==1) return [true, "holidayStart"];
							else return [false, "holidayStart"];
					},
					onSelect : function(date) {
						if ($(this).attr("id")=="holidayEnd") {
							var maxi = $("#holidayEnd").datepicker("getDate");
							maxi.setDate(maxi.getDate()-1);
							$("#holidayStart").datepicker("option", {maxDate:maxi});
						}
						if ($(this).attr("id")=="holidayStart") {
							var mini = $("#holidayStart").datepicker("getDate");
							mini.setDate(mini.getDate()+1);
							$("#holidayEnd").datepicker("option", {minDate:mini});
						}
					}';
		} else if ($mainProduct!=null && $mainProduct->reservationSelType == reservation_unit::WEEK) {
			$args = 'showWeek: true,
					dateFormat:"yy-mm-dd",

					beforeShowDay: function(date) {
						if ($(this).attr("id")=="holidayStart") {
							if (date.getDay()=='.intval($mainProduct->_reservationSelWeekStart).($mainProduct->_reservationSelWeekStart==7 ? ' || date.getDay()==0' : '').') return [true, "holidayStart"];
							else return [false, "holidayStart"];
						}
						if ($(this).attr("id")=="holidayEnd") {
							if (date.getDay()=='.intval($mainProduct->_reservationSelWeekEnd).($mainProduct->_reservationSelWeekEnd==7 ? ' || date.getDay()==0' : '').') return [true, "holidayEnd"];
							else return [false, "holidayEnd"];
						}
					},
					onSelect : function(date) {
						if ($(this).attr("id")=="holidayEnd") {
							var maxi = $("#holidayEnd").datepicker("getDate");
							maxi.setDate(maxi.getDate()-1);
							$("#holidayStart").datepicker("option", {maxDate:maxi});
						}
						if ($(this).attr("id")=="holidayStart") {
							var mini = $("#holidayStart").datepicker("getDate");
							mini.setDate(mini.getDate()+1);
							$("#holidayEnd").datepicker("option", {minDate:mini});
						}
					}';
			$output .= '
			<script type="text/javascript">
					$(".ui-datepicker-calendar td a").live("mousemove", function() {
						var day = parseInt($(this).html());
						var holidayEnd=$(this).parent().hasClass("holidayEnd");
						$(this).parent().parent().parent().find("td").each(function() {
							var sday = parseInt($(this).find("span").html());
							if (!holidayEnd && sday>day && sday<(day+8)) $(this).find("span").addClass("ui-state-hover");
							if (holidayEnd && sday<day && sday>(day-8)) $(this).find("span").addClass("ui-state-hover");
						});
						$(this).addClass("ui-state-hover");
					});
					$(".ui-datepicker-calendar td a").live("mouseleave", function() { $(this).parent().parent().parent().find("td").find("span").removeClass("ui-state-hover"); });
			</script>';
		} else if ($mainProduct!=null && $mainProduct->reservationSelType == reservation_unit::MONTH) {
			$args = '
					dateFormat:"yy-mm-dd",
					onChangeMonthYear: function(year, month) {
						if ($(this).attr("id")=="holidayStart") {
							startSelectedMonth = month-1;
							startSelectedYear = year;
						}
						if ($(this).attr("id")=="holidayEnd") {
							endSelectedMonth = month-1;
							endSelectedYear = year;
						}
					},
					onSelect : function(date) {
						if ($(this).attr("id")=="holidayEnd") {
							var maxi = $("#holidayEnd").datepicker("getDate");
							maxi.setDate(maxi.getDate()-1);
							$("#holidayStart").datepicker("option", {maxDate:maxi});
						}
						if ($(this).attr("id")=="holidayStart") {
							var mini = $("#holidayStart").datepicker("getDate");
							mini.setDate(mini.getDate()+1);
							$("#holidayEnd").datepicker("option", {minDate:mini});
						}
					},
					beforeShowDay: function(date) {
						if ($(this).attr("id")=="holidayStart") {
							if (date.getDate()==getMonthStart("'.$mainProduct->_reservationSelMonthStart.'", date) ) return [true, "holidayStart"];
							else return [false, "holidayStart"];
						}
						if ($(this).attr("id")=="holidayEnd") {
							if (date.getDate()==getMonthEnd("'.$mainProduct->_reservationSelMonthEnd.'", date) ) return [true, "holidayEnd"];
							else return [false, "holidayEnd"];
						}
					}';
			$output .= '
			<script type="text/javascript">
				var dnow = new Date();
				var startSelectedMonth = endSelectedMonth = dnow.getMonth();
				var startSelectedYear = endSelectedYear = dnow.getFullYear();

				function getMonthEnd(monthEnd, date) {
					var tempdate = new Date(date.getFullYear(), date.getMonth()+1, 0);
					var lastday=tempdate.getDate();
					if (monthEnd=="0") return tempdate.getDate();
					if (monthEnd.indexOf("d")==0) return monthEnd.replace("d","");
					if (monthEnd.indexOf("w")==0) {
						for (i = 0; i < 7; i++) {
							var tempdate = new Date(date.getFullYear(), date.getMonth(), lastday-i);
							if (tempdate.getDay()==monthEnd.replace("w","")) var monthend=tempdate;
						}
						return monthend.getDate();
					}
				}
				
				function getMonthStart(monthStart, date) {
					if (monthStart=="0") return 1;
					if (monthStart.indexOf("d")==0) return monthStart.replace("d","");
					if (monthStart.indexOf("w")==0) {
						for (i = 1; i < 8; i++) {
							var tempdate = new Date(date.getFullYear(), date.getMonth(), i);
							if (tempdate.getDay()==monthStart.replace("w","")) var monthstart=tempdate;
						}
						return monthstart.getDate();
					}
				}
					
				$(".ui-datepicker-calendar td a").live("mousemove", function() {
					var holidayEnd=$(this).parent().hasClass("holidayEnd");
					if (holidayEnd) 
						var day = new Date(endSelectedYear, endSelectedMonth, 1);
					else var day = new Date(startSelectedYear, startSelectedMonth, 1);
					var monthStart = getMonthStart("'.$mainProduct->_reservationSelMonthStart.'", day);
					var monthEnd = getMonthEnd("'.$mainProduct->_reservationSelMonthEnd.'", day);
					$(this).parent().parent().parent().find("td").each(function() {
						var sday = parseInt($(this).find("span").html());
						if (sday>=monthStart && sday<=monthEnd) $(this).find("span").addClass("ui-state-hover");
					});
					$(this).addClass("ui-state-hover");
				});
				$(".ui-datepicker-calendar td a").live("mouseleave", function() { $(this).parent().parent().parent().find("td").find("span").removeClass("ui-state-hover"); });
					
			</script>';
		} else $args='';
	} else $args='';
		$output .= '
			<script type="text/javascript">
				var varQty=new Array();
';
					foreach($obj->_products->list as $mProd) 
						$output .= '
				varQty['.$mProd->sqlId.']='.intval($mProd->productType==product_type::AVAILABLE).';';
						
					$output .= '
					
				$( document ).ready(function() {
					checkHolidayQty();
					$( "#holidayProduct").change(function() {
						checkHolidayQty();
					});
					$( "#holidayFamily").change(function() {
						checkHolidayQty();
					});
				});
				$( document ).ajaxComplete(function( event, xhr, settings ) {
					checkHolidayQty();
				});
				function checkHolidayQty() {
					if (typeof(productFamilly)!=\'undefined\') {
						var selFamily = $("#holidayFamily").val();
						var selProduct = $("#holidayProduct").val();
						if (selProduct>0)
							selFamily=productFamilly[selProduct];
						else if(0 in varQty)
							selFamily=0;

						if (varQty[selFamily]) $("#holidayQty").show();
						else  $("#holidayQty").hide();
					}
				}
				function forceEnd() {
					if (!$("#holidayEnd").attr("changed")) {
						d = $("#holidayStart").datepicker("getDate");
						$("#holidayEnd").datepicker("setDate", new Date(d.getFullYear(),d.getMonth(),d.getDate()+1));
					}
				}
			</script>';
			
			
		if ($isfront) $output .= '<div class="box">';
		$formurl = ($isfront ? $_SERVER['REQUEST_URI'] : myOwnReservationsController::getConfUrl('availability'));
		if ($id_product==0)
			$output .= '
			<form action="'.$formurl.'" method="post">';
		$output .= MyOwnReservationsUtils::insertTimeslotPickerScript($obj, 'holiday', 0, $obj->l('All', 'availabilities'));

		if (_PS_VERSION_ < "1.6.0.0")
			$output .= '
			<fieldset class="settingsList" id="addAvailability" style="margin-bottom:15px;'.(!$isfront ? 'display:none' : '').'">
				<legend id="addAvailability_legend"><img src="../img/admin/edit.gif" />'.ucfirst(myOwnLang::$actions[MYOWN_OPE::ADD]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::AVAILABILITY]).'</legend>
				'.($isfront ? '' :'<legend id="editAvailability_legend" style="display:none"><img src="../img/admin/add.gif" />'.ucfirst(myOwnLang::$actions[MYOWN_OPE::EDIT]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::AVAILABILITY]).'</legend>');
		else $output .= '
			<div class="panel" id="addAvailability" style="'.(!$isfront ? 'display:none' : '').'">
				<h3 id="addAvailability_legend"><i class="icon-calendar-o"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::FNEW]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::AVAILABILITY]).'</h3>
				<h3 id="editAvailability_legend" style="display:none"><i class="icon-time"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::EDIT]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::AVAILABILITY]).' <span class="badge"></span></h3>';

				$output .=myOwnUtils::insertDatepicker($obj, array('holidayStart', 'holidayEnd'), false, $args);
				$output .=myOwnUtils::insertDatepicker($obj, array('filter_startdate', 'filter_enddate'), false);
				$output .='
				<div style="float:left;width:150px;style="width:100%;display:none"">
					<span class="mylabel-tooltip" style="opacity:100" title="'.myOwnHelp::$objects['availability']['type'].'" >'.$obj->l('Type', 'availabilities').'</span><br />';
					foreach (myOwnLang::$availabilityTypes AS $availabilityTypeKey => $availabilityTypeValue){
						$output .= '<input type="radio" class="availabilityType" name="holidayType" value="'.$availabilityTypeKey.'" '.($availabilityTypeKey==0 ? 'checked' : '').'>&nbsp;';
									if ($availabilityTypeKey==1) $output .= '<img src="'.__PS_BASE_URI__.'modules/myownreservations/img/enabled.gif" alt="enabled" />';
									if ($availabilityTypeKey==0) $output .= '<img src="'.__PS_BASE_URI__.'modules/myownreservations/img/forbbiden.gif" alt="enabled" />';
									$output .= '&nbsp;'.$availabilityTypeValue.'<br>';
					}
					$output .= '
				</div>';
				$attributes=array();
				if ($id_product>0) $attributes = MyOwnReservationsUtils::getProductAttributes($id_product);
				if ($obj->isProVersion && $productsFilter==array() && $id_product==0) {
					$output .= '
					<div style="float:left;width:120px;padding-right:15px"><span>'.ucfirst(myOwnLang::$object[MYOWN_OBJ::FAMILY]).'</span><br />
						<div style="display:inline-block">'.MyOwnReservationsUtils::insertProductFamillyPicker($obj, $cookie->id_lang, "holidayFamily", "", "", Tools::getValue('holidayFamily'), "", false, "holidayProduct" , $id_product, '.reservationUnit', false).'</div>
					</div>';
				}
				$output .= '
				<div style="float:left;width:220px;padding-right:15px">
					'.($id_product !=0 ? (count($attributes)>0 ? '<span id="holidayProductAttributeSpan" class="">'.$obj->l('Attribute', 'availabilities') : '') : '<span id="holidayProductSpan" class="">'.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRODUCT])).'</span><br />'.'
					'.MyOwnReservationsUtils::insertProductPicker($obj, $cookie->id_lang, "holidayProduct", $id_product, "holidayProductAttribute", 0, "width:100%", 'populateTimeslot(\'start\');populateTimeslot(\'end\');$(\'#holidayProduct\').trigger(\'chosen:updated\');', ($productsFilter!=array() ? $productsFilter : $id_product), '', $productsFilter==array()).'
				</div>';
				$output .= '
				<div style="float:left;width:150px;padding-right:15px">
					<span class="mylabel-tooltip" style="opacity:100" title="'.myOwnHelp::$objects['availability']['date'].'">'.$obj->l('Start', 'availabilities').'</span><br />
					'.MyOwnReservationsUtils::getDateField('holidayStart', ($id_product==0 ? date('Y-m-d') : ''), '', '', 'populateTimeslot(\'start\');forceEnd();').'<br />
					<select class="upsInput" id="holidayStartTimeslot" name="holidayStartTimeslot" style="width:100%;display:none"></select>
				</div>
				<div style="float:left;width:150px;padding-right:15px">
					<span class="mylabel-tooltip" style="opacity:100" title="'.myOwnHelp::$objects['availability']['date'].'">'.$obj->l('End', 'availabilities').'</span><br />
					'.MyOwnReservationsUtils::getDateField('holidayEnd', ($id_product==0 ? date('Y-m-d') : ''), '', '', 'populateTimeslot(\'end\');$(this).attr(\'changed\',true)').'<br />
					<select class="upsInput" id="holidayEndTimeslot" name="holidayEndTimeslot" style="width:100%;display:none"></select>
				</div>
				<div id="holidayQty" style="float:left;width:150px;padding-right:15px;display:none;opacity:100">
					<span class="mylabel-tooltip" title="'.myOwnHelp::$objects['availability']['qty'].'">'.$obj->l('Quantity', 'availabilities').'</span><br />
					<input id="holidayQuantity" class="upsInput" style="width:50px;text-align:center" type="number" name="holidayQty" value="0" /><br />
				</div>
				';
				foreach ($obj->_extensions->getExtByType(extension_type::OBJECT) as $ext) 
					if (method_exists($ext, "objectSelect") && ($id_product>=0 || $ext->id!=11))
						$output .= '<div id="holidayObject" style="float:left;width:150px;padding-right:15px">
					'.$ext->objectSelect($obj, $cookie->id_lang, 'holidayObject').'
				</div>';
				if ($id_product==0)
					$output .= '
				<div style="float:right;width:80px;">
					<input type="submit" name="submitAvailability" id="addAvailability_submit" value="' . ucfirst(myOwnLang::$actions[MYOWN_OPE::ADD]) . '" class="button btn btn-default" style="width:100%;margin-top:30px" />
					'.($isfront ? '' :'<input type="submit" name="submitAvailability" id="editAvailability_submit" value="' . ucfirst(myOwnLang::$actions[MYOWN_OPE::EDIT]) . '" class="button btn btn-default" style="width:100%;margin-top:30px" />').'
					<input type="hidden" name="idAvailability" id="editAvailability_id" value="" />
				</div>
				<div style="float:right;width:80px;padding-right:15px;">
					<input type="button" name="submitReservationSettings" onclick="$(\'#addAvailability\').hide();" value="' . ucfirst(myOwnLang::$actions[MYOWN_OPE::CANCEL]) . '" class="button btn btn-default" style="width:100%;margin-top:30px;" />
				</div>';
				$output .= '
				<div style="clear:both"></div>';
				
				if (_PS_VERSION_ >= "1.6.0.0" && $id_product!=0)
					$output .= '
				<div class="panel-footer">
					<a onclick="$(\'#holidayStart\').val(\'\');$(\'#addAvailability\').hide()" class="btn btn-default"><i class="process-icon-cancel"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::CANCEL]).'</a>
					<button type="submit" name="submitAddproduct"  onclick="$(\'#priceSetBtn\').attr(\'checked\',\'checked\')" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::SAVE]).'</button>
					<button type="submit" name="submitAddproductAndStay"  onclick="$(\'#priceSetBtn\').attr(\'checked\',\'checked\')" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::SAVESTAY]).'</button>
				</div>';
				
			if (_PS_VERSION_ < "1.6.0.0")
				$output .= '
			</fieldset>';
			else $output .= '
			</div>';
		if ($id_product==0)
			$output .= '
		</form>';
		if ($isfront) $output .= '</div>';
		return $output;
				
	}

}

?>