<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnReservationsCalendarController {
	
	//==============================================================================================================================================
	//     SHOW PLANNING
	//==============================================================================================================================================

	public static function display($obj, $cookie, $smarty, $mainProduct, $potentialResa, $type="product", $ajax=false)
	{
     	global $cart;
     	$debug=false;
     	myOwnReservationsController::_construire($obj);		
		
		$mainProduct = $potentialResa->_mainProduct;
		if ($mainProduct==null) return "";
	
		$output='';
		$protocol_content = $protocol_link = (Configuration::get('PS_SSL_ENABLED') || Tools::usingSecureMode()) ? 'https://' : 'http://';
		$link=new Link($protocol_link, $protocol_content);
		$smarty->assign('myownlink',$link);
		$smarty->assign('id_lang', $cookie->id_lang);
		$smarty->assign('ishomeselector', MYOWNRES_HOME_ALL==false);
		$smarty->assign('myOwnResWay', $mainProduct->_reservationLengthControl == reservation_length::OPTIONAL);
			$smarty->assign('myOwnResLblStart', $mainProduct->getStartLabel($cookie->id_lang));
			$smarty->assign('myOwnResLblEnd', $mainProduct->getStopLabel($cookie->id_lang));
		$smarty->assign('prestaCart',$cart);
		$smarty->assign('stocks',$obj->_stocks);
		$widgetHome = Configuration::get('MYOWNRES_WIDGET_HOME');
		if ($type=="product") 
			$smarty->assign('planning_cell', MyOwnReservationsUtils::getPath($obj, 'views/templates/front/planning/_partials/cell_product.tpl'));
		else if ($type=="widget")
			$smarty->assign('planning_cell', MyOwnReservationsUtils::getPath($obj, 'views/templates/front/planning/_partials/cell_widget.tpl'));
		else if ($type=="home") {
			if ($widgetHome>1) {
				if (_PS_VERSION_ >= "1.7.0.0")
					$smarty->assign('planning_cell', MyOwnReservationsUtils::getPath($obj, 'views/templates/front/planning/_partials/cell_homeproduct_ps17.tpl'));
				else $smarty->assign('planning_cell', MyOwnReservationsUtils::getPath($obj, 'views/templates/front/planning/_partials/cell_homeproduct.tpl'));
			} else $smarty->assign('planning_cell', MyOwnReservationsUtils::getPath($obj, 'views/templates/front/planning/_partials/cell_home.tpl'));
		} else
			$smarty->assign('planning_cell', MyOwnReservationsUtils::getPath($obj, 'views/templates/front/planning/_partials/cell_select.tpl'));

		$smarty->assign('planning_timeline', MyOwnReservationsUtils::getPath($obj, 'views/templates/front/planning/_partials/line_time.tpl'));
		$smarty->assign('planning_dayline', MyOwnReservationsUtils::getPath($obj, 'views/templates/front/planning/_partials/line_day.tpl'));
		$smarty->assign('widget',($type=="home"));
		$smarty->assign('widgetHome',$widgetHome);
		
		$myOwnCalendar = new MyOwnCalendarTool();
		$smarty->assign('calendar',$myOwnCalendar);
		$smarty->assign('my_img_dir',_PS_IMG_);
		$smarty->assign('module_dir',_MODULE_DIR_);
		$smarty->assign('base_uri',__PS_BASE_URI__);
		if (_PS_VERSION_ >= "1.7.0.0")
			smartyRegisterFunction($smarty, 'function', 'convertPrice', array('Product', 'convertPrice'));
		
    	//Manage user selection
		$selection='';$origSelection=-1;
		$res_start = Tools::getValue('res_start', 0);
		if (Tools::getIsSet('sel'))
			$origSelection = Tools::getValue('sel');
		$selection = $origSelection;
		if ($selection<0 or $selection=="") {
			if ($selection==-2) myOwnPotentialResa::clearSessionPotentialResa();
			if (($res_start==0 or $res_start==-1) && isset($_SESSION[$type.'prod'.$mainProduct->sqlId.'_sel']))
				$selection = $_SESSION[$type.'prod'.$mainProduct->sqlId.'_sel'];
			else $selection=0;
		} else {
			$_SESSION[$type.'prod'.$mainProduct->sqlId.'_sel'] = $selection;
		}
		
		$sessionProductPotentialResa = myOwnPotentialResa::getSessionPotentialResa($obj, $potentialResa->id_product, $potentialResa->id_product_attribute);
		$sessionApplicable = ($sessionProductPotentialResa != null
								&& $sessionProductPotentialResa->_mainProduct!=null
								&& $potentialResa->_mainProduct->sqlId == $sessionProductPotentialResa->_mainProduct->sqlId );
		
		$isEndSelection = ($res_start!=-1 && $res_start!=0);

		if ($potentialResa->_selType=="") {
			if (($type!="column" && $type!="topcolumn")
				&& ($mainProduct->_reservationLengthControl == reservation_length::FIXED
					or $mainProduct->_reservationLengthControl == reservation_length::NONE
					or $mainProduct->_reservationLengthControl == reservation_length::PRODUCT
					or $mainProduct->_reservationLengthControl == reservation_length::COMBINATION)
				)
				$potentialResa->_selType="";
			else 
				$potentialResa->_selType="start";
		}

		//Manage reservation step
		if ($isEndSelection) {
			//fill start potential resa
			$potentialResa->_selType = "end";
			$res_start_tab_tmp = explode("@", $res_start);
		    $res_start_tab = explode("_", $res_start_tab_tmp[0]);
 			$potentialResa->startDate =$res_start_tab[0];
 			$potentialResa->startTimeslot = $res_start_tab[1];
 			if ($origSelection<0 && $sessionApplicable && $type == "topcolumn") {
 				$selection=$_SESSION['pageEnd'];
 			}
 		}
 		else if ($sessionApplicable && ($origSelection<0 || $origSelection=='')) {

			//if ($sessionProductPotentialResa != null && $sessionProductPotentialResa->_mainProduct!=null && ($potentialResa->_mainProduct->sqlId == $sessionProductPotentialResa->_mainProduct->sqlId )) {
				//checking avaialability to reset calendar if this product not available (when on product planning only)
				if ($sessionProductPotentialResa->id_product) {
					$sessionProductPotentialResa->_productQty = $mainProduct->getProductQuantity($potentialResa->id_product, $potentialResa->id_product_attribute, $obj->_stocks);
					$sessionProductPotentialResa->_reservations=null;
					
					$isAvailable = $sessionProductPotentialResa->isAvailable($cart, null, $obj->_stocks) > 0;
				} else $isAvailable=true;
				//need to reset selection if session potential resa because we will display start sleection with start session date
				$selection=0;

				if ($isAvailable) {
					//fill potential resa with session
					$potentialResa->_fixedLength = $sessionProductPotentialResa->_fixedLength;
		 			$potentialResa->startDate =$sessionProductPotentialResa->startDate;
		 			$potentialResa->startTimeslot = $sessionProductPotentialResa->startTimeslot;
		 			$potentialResa->endDate =$sessionProductPotentialResa->endDate;
		 			$potentialResa->endTimeslot = $sessionProductPotentialResa->endTimeslot;
			 		//echo 'test '.$type.' ['.$potentialResa->_selType.'] '.intval($type != "topcolumn" && $potentialResa->_selType=="start");
					if ($type != "topcolumn" && $potentialResa->_selType=="start") {
			 			$potentialResa->_selType="end";
			 			$selection=$_SESSION['pageEnd'];
		 			} else {
			 			$selection=$_SESSION['pageStart'];
		 			}
				}
			//}
		}


//echo '@'.$potentialResa->_selType;
		if ($debug) $rustart = getrusage();

		$calendarView = new myOwnCalendarView($obj, $potentialResa, $type);
		foreach ($obj->_extensions->getExtByType(extension_type::OBJECT) as $ext) 
			if (method_exists($ext, "objectSelect"))
				if ($ext->id!=11)
					$calendarView->ext_object = $ext;

		if ($type=="home") {

			if ($mainProduct->_mergedFamilies!=array()) {//$widgetHome>1) 
				foreach ($mainProduct->_mergedFamilies as $mergedProduct )
					$calendarView->homeProducts[$mergedProduct] = $obj->_products->list[$mergedProduct]->getProducts($cookie->id_lang);
			} else {
				$calendarView->homeProducts[$mainProduct->sqlId] = $mainProduct->getProducts($cookie->id_lang);
			}
			$with_taxes = (Configuration::get('MYOWNRES_PRICE_TAXEXCL')!=1);

			foreach ($calendarView->homeProducts as $homeProductKey => &$homeProduct)
				foreach ($homeProduct as &$product)
					$product['price'] = Tools::ps_round(MyOwnReservationsUtils::getProductPriceTaxes($product["id_product"], 0, $with_taxes, $mainProduct->_optionAlsoSell),_PS_PRICE_DISPLAY_PRECISION_);

		}
		//debug time
		if ($debug) {
			$ruend = getrusage();
			echo "new myOwnCalendarView " . myOwnCalendarView::rutime($ruend, $rustart, "utime") .  "<br>";
		}
		$err='';
		if (!$calendarView->reservationEnabled) $err = '<span>'.htmlentities(myOwnLang::getMessage('DESACTIVATE_RES', $cookie->id_lang)).'</span>';

		if ($debug) $rustart = getrusage();

		/*if (1 && $potentialResa->_selType=="end")
			$tpl = 'lengths';
		else */$tpl = $calendarView->getTemplate($obj, $selection, $type, $ajax);

		//debug time
		if ($debug) {
			$ruend = getrusage();
			echo "get Template " . myOwnCalendarView::rutime($ruend, $rustart, "utime") .  "<br>";
		}

		return $err.$tpl;
	}

	//==============================================================================================================================================
	//     PRODUCT PLANNING
	//==============================================================================================================================================
	
	public static function displayProduct($obj, $cookie, $smarty)
	{
     	global $cart;
     	$out='';
     	myOwnReservationsController::_construire($obj);
		
 		//get product params 
 		$productId = Tools::getValue('id_product');
		if (Tools::getIsSet('ipa')) $productAttributeId = intval(Tools::getValue('ipa'));
		else $productAttributeId=MyOwnReservationsUtils::getDefaultAttribute($productId);
		
		$mainProduct = $obj->_products->getResProductFromProduct($productId);

		if ($mainProduct==null) return "";
		
		$desiredQty = 1;
		if (Tools::getIsSet('qty_field')) {
			$desiredQty=0;
			$attendee=Tools::getValue('qty_field');
			$attendees=explode(",", $attendee);
			foreach ($attendees as $attendee) {
				$attendeetab=explode(" ", $attendee);
				$desiredQty+=$attendeetab[0];
			}
		} else {
			if ($mainProduct->_qtyType == reservation_qty::SEARCH)
				$desiredQty = $mainProduct->searchProductQtyInAttribute($productId, $productAttributeId, $cookie->id_lang);
			else if (Tools::getIsSet('qty')) $desiredQty = Tools::getValue('qty');
		}
		//echo 'desiredQty:'.$desiredQty;
		if ($mainProduct->_qtyType == reservation_qty::FIXED)
			$desiredQty = $mainProduct->_qtyFixed;
			
		$productQty = $mainProduct->getProductQuantity($productId, $productAttributeId, $obj->_stocks);

		//get price without taxes
		$productprice = MyOwnReservationsUtils::getProductPriceTaxes($productId, $productAttributeId, false, $mainProduct->_optionAlsoSell, $desiredQty);
		$productPrice_with_taxes = MyOwnReservationsUtils::getProductPriceTaxes($productId, $productAttributeId, true, $mainProduct->_optionAlsoSell, $desiredQty);
		$oldProductPrice = MyOwnReservationsUtils::getOldProductPriceTaxes($productId, $productAttributeId, false, $mainProduct->_optionAlsoSell, $desiredQty);
		if ($oldProductPrice==$productprice) $oldProductPrice=-1;
		if ($productprice > 0) $taxrate = (($productPrice_with_taxes-$productprice)/$productprice)*100;
		else $taxrate = 0;
 	
		//fill reservation parameters
		$potentialResa = new myOwnPotentialResa();
		$potentialResa->id_product = $productId;
		$potentialResa->id_product_attribute = $productAttributeId;
		$potentialResa->id_object = (int)Tools::getValue('place', 0);
		$potentialResa->quantity = $desiredQty;
		$potentialResa->_mainProduct = $mainProduct;
		$potentialResa->_pricerules = $obj->_pricerules->getForProduct($mainProduct->sqlId, $productId, $productAttributeId);
		if ($potentialResa->_mainProduct->_reservationPriceType==reservation_price::TABLE_RATE) {
			$isPricesSet = myOwnPricesSets::isPricesSetForProduct($productId);
			$setprices = array(
				reservation_price::TABLE_TIME => ($isPricesSet ? myOwnPricesSets::getPricesSetForProduct($productId, $productAttributeId) : myOwnPricesSets::getPricesSetForFamily($potentialResa->_mainProduct, reservation_price::TABLE_TIME)),
				reservation_price::TABLE_DATE => myOwnPricesSets::getPricesSetForFamily($potentialResa->_mainProduct, reservation_price::TABLE_DATE)
			);

		} else $potentialResa->_pricesets = myOwnPricesSets::getPricesSetForProduct($productId, $productAttributeId);
		$potentialResa->_productQty = $productQty;
		//$potentialResa->_productprice = $productprice;
		$potentialResa->_productPrice = $productprice;
		$potentialResa->_oldProductPrice = $oldProductPrice;
		$potentialResa->_taxrate = $taxrate;
		
		if ($mainProduct->_reservationSelMode != reservation_mode::MULTIPLE && $mainProduct->_reservationSelMode != reservation_mode::UNIQUE)
			$potentialResa->_selType="start";
		
		//calculate occupation Qty
		//if ($mainProduct->_qtyType == reservation_qty::IGNORE) $occupationQty=1;
		//else $occupationQty=ceil($desiredQty/$mainProduct->_qtyCapacity);
		$occupationQty = $potentialResa->getOccupationQty();

		if (($productQty<$occupationQty && $productQty>=0) or $productAttributeId<0) {
			$out = '<div>';
			$out .= $obj->l('This product is not available for this quantity', 'calendar');
			if ($productAttributeId!=0) $out .= $obj->l(', you may check other attributes.', 'calendar');
			$out .= '</div>';
			$out .= '<script type="text/javascript" name="planning">
						if (typeof(myproductSelector)=="undefined")
							var myproductSelector = Object.create(myOwnSelector);
						myproductSelector.target="product";
					</script>';
			return $out;
		}
    	return $out.myOwnReservationsCalendarController::display($obj, $cookie, $smarty, $mainProduct, $potentialResa, "product", true);
	}

	//==============================================================================================================================================
	//     HOME PLANNING
	//==============================================================================================================================================
	
	public static function displayHome($obj, $ajax=false) {
     	global $isWeekEndDays;
     	global $cart;
     	global $smarty;
     	global $cookie;
     	global $link;
	 	$homeProducts = array();
	 	
	 	$default = $obj->_products->getDefaultCategory();
	 	$myOwnSelectCat = Tools::getValue('myOwnSelectCat', $default);
	 	$id_mainProduct = Tools::getValue("res_familly",-1);
	 	if (array_key_exists(0, $obj->_products->list))
			$mainProduct = $obj->_products->list[0];
		elseif (array_key_exists($id_mainProduct, $obj->_products->list))
			$mainProduct = $obj->_products->list[$id_mainProduct];
		else if ($myOwnSelectCat>0) {
			$mainProduct = $obj->_products->getResProductFromCategory($myOwnSelectCat);
			//foreach($obj->_products->list as $mainProduct);
		} else {
			$mainProduct = $obj->_products->getGlobal("", true);
		}
		
		$widgetHome = Configuration::get('MYOWNRES_WIDGET_HOME');
		//if ($widgetHome==1) $default=$mainProduct->sqlId;

		$mainProducts=array($mainProduct);
		
		$planning='';
		foreach ($mainProducts as $mainProduct) {
			//fill reservation parameters
			$potentialResa = new myOwnPotentialResa();
			$potentialResa->quantity = 1;
			$potentialResa->_mainProduct = $mainProduct;
			$potentialResa->_pricerules = $obj->_pricerules;
			$potentialResa->_productQty = -1;
			$smarty->assign('potentialResa',$potentialResa);
			
			
	        //Manage user selection
			$selection = Tools::getValue('sel', 0);
			if ($selection<0) $selection=0;
			$smarty->assign('sel',$selection);
	
			//products and cats
			$cats = explode(',',$mainProduct->ids_categories);
			$myOwnSelectCat = $obj->_products->getCategoriesLink($cats, ($widgetHome>1 ? $obj->l('All', 'calendarView') : ''));

		    $productsIds=$mainProduct->getProductsId($myOwnSelectCat);
		    $productsImages=MyOwnReservationsUtils::getProductsImages($productsIds, $cookie->id_lang);
		    $imageSize = Configuration::get('MYOWNRES_WIDGET_IMG');
		    $imageType=  new ImageType($imageSize);
		    if ($imageSize > 0 && !$imageType->id) {
		    	$imageType=  new ImageType(2);
		    	$imageSize=2;
		    }
		    $imageTypes=ImageType::getImagesTypes();
			foreach($imageTypes as $imageType)
				if ($imageType['name']=='small_default') $mediumSize=new ImageType($imageType['id_image_type']);
		    $smarty->assign(array(
					'productsImages' => $productsImages,
					'mediumSize' => $mediumSize));
			if (_PS_VERSION_ >= "1.7.0.0") smartyRegisterFunction($smarty, 'function', 'displayPrice', array('Tools', 'displayPriceSmarty'));
			
			$smarty->assign(array(
				'planningSelector'	=> 'home',
				'PS_CATALOG_MODE' 	=> Configuration::get('PS_CATALOG_MODE'),
				'imageSize' 		=> $imageSize,
				'widget' 			=> true,
				'next_label' 		=> myOwnLang::$reservSwitchesNext[$mainProduct->reservationSelPlanning],
				'previous_label' 	=> myOwnLang::$reservSwitchesPrevious[$mainProduct->reservationSelPlanning],
				'planning_cell_widget' =>  MyOwnReservationsUtils::getPath($obj, 'views/templates/front/planning/_partials/cell_widget.tpl')
			));
					
			$styles=$obj->_pricerules->getStyles($potentialResa->id_product);
			
			$potentialResa = new myOwnPotentialResa();
			$potentialResa->_mainProduct = $mainProduct;
			$potentialResa->_pricerules = $obj->_pricerules->getForFamily($mainProduct->sqlId);
			$potentialResa->_productQty = -1;

	    	$planning .= myOwnReservationsCalendarController::display($obj, $cookie, $smarty, $mainProduct, $potentialResa, "home", $ajax);
    	}
		$smarty->assign('planning',$planning);
		
		
		
		if ($ajax) return $planning;
		else return $styles.$obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/front/widget/home.tpl');
		//}
	}

	//==============================================================================================================================================
	//     SELECT PLANNING
	//==============================================================================================================================================
	
	public static function displayWidget($obj, $ajax=false, $type='')
	{
     	global $cart;
     	global $smarty;
     	global $cookie;
     	$id_shop = MyOwnReservationsUtils::getShop();
     	$mainProduct=null;
     	//$ajax = (Tools::getValue('ajax', 'false')=='true');
	 	
	 	if ($type=='') {
	     	if (Configuration::get('MYOWNRES_WIDGET_TYPE')==1 || Configuration::get('MYOWNRES_WIDGET_TYPE')==3) $type="select";
	     	else $type="search";
	 	}
     	if(session_id() == '') session_start();
     	myOwnReservationsController::_construire($obj);

		if ($type=="select") {
			$id_mainProduct = Tools::getValue("res_familly",-1);
			//std vers
			if (array_key_exists(0, $obj->_products->list)) {
				$mainProduct = $obj->_products->list[0];
				if (!in_array($_GET['id_category'], explode(',',$mainProduct->ids_categories)))
					$mainProduct = null;
			} elseif (array_key_exists($id_mainProduct, $obj->_products->list))
				$mainProduct = $obj->_products->list[$id_mainProduct];
			else if (isset($_GET['id_category']))
				$mainProduct = $obj->_products->getResProductFromCategory($_GET['id_category']);
			else if (isset($_GET['id_product']))
				$mainProduct = $obj->_products->getResProductFromProduct($_GET['id_product']);

			if (Configuration::get('MYOWNRES_WIDGET_COLSTRICT')==1 && $mainProduct==null) return "";
			if ($mainProduct==null)
				foreach ($obj->_products->list as $mainProd)
					if ($id_shop==0 or $mainProd->id_shop==0 or $mainProd->id_shop==$id_shop) {
						$id_mainProduct = $mainProd->sqlId;
						$mainProduct = $mainProd;
						break;
					}
			if ($mainProduct==null) return "";
		} else {
			$mainProduct = $obj->_products->getGlobal("start");
			$args='dateFormat:"dd-M-yy",';
			$datepicker = myOwnUtils::insertDatepicker($obj, array('searchStart', 'searchEnd'), false, $args);
			$smarty->assign('datepicker',$datepicker);
			$smarty->assign('tspicker','');
			$minDate = $mainProduct->getReservationStart();
			$maxDate = strtotime("+".$mainProduct->reservationPeriod." day",$minDate);
			$smarty->assign('minDate',$minDate);
			$smarty->assign('maxDate',$maxDate);

		}
		$link=new Link();
		if (empty($link->protocol_content))
			$link->protocol_content = Tools::getCurrentUrlProtocolPrefix();
		$smarty->assign('myownlink',$link);
			
		//fill reservation parameters
		$potentialResa = new myOwnPotentialResa();
		$potentialResa->_mainProduct = $mainProduct;
		$smarty->assign('potentialResa',$potentialResa);
		$smarty->assign('planningSelector','column');
		
		$startDay = strtotime(Tools::getValue('searchStartDay'));
		$smarty->assign('searchStartDay', $startDay);
		$endDay = strtotime(Tools::getValue('searchEndDay'));
		$smarty->assign('searchEndDay', $endDay);

		$planning = myOwnReservationsCalendarController::display($obj, $cookie, $smarty, $mainProduct, $potentialResa, "column", $ajax);
		if ($ajax) return $planning;
		else $planning = '<div id="primary_block" test="column" style="margin-top:0px">'.$planning.'</div>';

		$cats = explode(',',$mainProduct->ids_categories);
		$obj->_products->getCategoriesLink($cats);

		$smarty->assign('widgetType',$type);
		$smarty->assign('planning',$planning);
		$smarty->assign('isSel', false);
		if ($type=="select") 
			return $obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/front/widget/columns.tpl');
		else
			return $obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/front/widget/search.tpl');
 	}
 	
 	public static function displayTopWidget($obj, $ajax=false, $type='')
	{
     	global $cart;
     	global $smarty;
     	global $cookie;
     	$id_shop = MyOwnReservationsUtils::getShop();
     	$mainProduct=null;
     	$qtyExts = $obj->_extensions->getExtByQty();
     	//$ajax = (Tools::getValue('ajax', 'false')=='true');
	 	
	 	if ($type=='') {
	     	if (Configuration::get('MYOWNRES_WIDGET_TYPE')==1 || Configuration::get('MYOWNRES_WIDGET_TYPE')==3) $type="select";
	     	else $type="search";
	 	}
     	if(session_id() == '') session_start();
     	$objectSelection = (int)$_SESSION['objectSelection'];
     	$qtySelection = 1;$qtiesSelection='';
     	if (array_key_exists('qtySelection', $_SESSION)) $qtySelection = (int)$_SESSION['qtySelection'];
     	if (array_key_exists('qtiesSelection', $_SESSION)) $qtiesSelection = $_SESSION['qtiesSelection'];
     	myOwnReservationsController::_construire($obj);

		if ($type=="select") {
			$id_mainProduct = Tools::getValue("res_familly",-1);
			//std vers
			if (array_key_exists(0, $obj->_products->list))
				$mainProduct = $obj->_products->list[0];
			elseif (array_key_exists($id_mainProduct, $obj->_products->list))
				$mainProduct = $obj->_products->list[$id_mainProduct];
			else if (isset($_GET['id_category']))
				$mainProduct = $obj->_products->getResProductFromCategory($_GET['id_category']);
			else if (isset($_GET['id_product']))
				$mainProduct = $obj->_products->getResProductFromProduct($_GET['id_product']);

			if ($mainProduct==null)
				foreach ($obj->_products->list as $mainProd)
					if ($id_shop==0 or $mainProd->id_shop==0 or $mainProd->id_shop==$id_shop) {
						$id_mainProduct = $mainProd->sqlId;
						$mainProduct = $mainProd;
						break;
					}
			if ($mainProduct==null) return "";
		} else
			$mainProduct = $obj->_products->getGlobal("start");
			
		
		$earlierSelections=array();
		$uniqueSelections=array();
		$earlierSelectionLabels=array();
		$extraSelections=array();
		$extraQty=array();
		$sessionSelections=array();
		$sessionPotentialResa = myOwnPotentialResa::getSessionPotentialResa($obj);
		foreach ($obj->_products->list as $mainProd) {
			$startParamsTab = explode(';', $mainProd->reservationStartParams);
			$start = $mainProd->getReservationStart();
			$end = strtotime("+".$mainProd->reservationPeriod." day", $start);
			$availabilities = new myOwnAvailabilities($start, $end, $mainProd, 0, 0, false, false);
			$startTimeslot = $mainProd->getFirstTimeSlot($start, $end, $availabilities);
			//echo $mainProd->name.':'.date('Y-m-d H:i:s', $start).($startTimeslot != null ? '_'.$startTimeslot->sqlId : '').' '.date('Y-m-d', $end).'#';
			if ($startTimeslot!=null) $sel = date('Y-m-d', $start).'_'.$startTimeslot->sqlId;
			else $sel = date('Y-m-d', $start).'_0';
			
			$txt = MyOwnCalendarTool::formatDateWithMonthShort($start, $cookie->id_lang);
			if ($startTimeslot!=null && $startTimeslot->sqlId>0 && $mainProd->_reservationShowTime)
				$txt .= ' '.MyOwnCalendarTool::formatTimeFromDate($startTimeslot->getStartHourTime($start), $cookie->id_lang);
			
			foreach ($obj->_extensions->getExtByType(extension_type::SEL_WIDGET) as $ext) {
 				if (in_array($ext->id, $mainProd->ids_options)) {
					if (method_exists($ext, "widgetSelect"))
							$extraSelections[$mainProd->sqlId]='<i class="icon-'.$ext->icon.'"></i><div class="content">'.$ext->widgetSelect($obj, $mainProd, $cookie->id_lang, "topcolumn", $objectSelection);
				}	
			}

			if (array_key_exists($mainProd->_qtyType, $qtyExts))
				$extraQty[$mainProd->sqlId]='<i class="icon-'.$qtyExts[$mainProd->_qtyType]->icon.'"></i><div class="content">'.$qtyExts[$mainProd->_qtyType]->widgetSelect($obj, $mainProd, $cookie->id_lang, "topcolumn", $qtiesSelection).'</div>';
			
			$uniqueSelections[$mainProd->sqlId] = intval($mainProd->_reservationSelMode == reservation_mode::MULTIPLE || $mainProd->_reservationSelMode == reservation_mode::UNIQUE);
			$earlierSelections[$mainProd->sqlId] = $sel;
			$earlierSelectionLabels[$mainProd->sqlId] = $txt;
			if ($sessionPotentialResa!=null && $sessionPotentialResa->_mainProduct!= null && $sessionPotentialResa->_mainProduct->sqlId==$mainProd->sqlId) {
				$sessionSelections[$mainProd->sqlId] = $sessionPotentialResa->startDate.'_'.$sessionPotentialResa->startTimeslot.'@'.$sessionPotentialResa->endDate.'_'.$sessionPotentialResa->endTimeslot;
			} else $sessionSelections[$mainProd->sqlId] = '';
		}
		$smarty->assign('uniqueSelections',Tools::jsonEncode($uniqueSelections));
		$smarty->assign('earlierSelections',Tools::jsonEncode($earlierSelections));
		$smarty->assign('extraSelections',Tools::jsonEncode($extraSelections));
		$smarty->assign('extraQty',Tools::jsonEncode($extraQty));
		$smarty->assign('earlierSelectionLabels',Tools::jsonEncode($earlierSelectionLabels));
		$smarty->assign('sessionSelections',Tools::jsonEncode($sessionSelections));
		
		//fill reservation parameters
		$potentialResa = new myOwnPotentialResa();
		$potentialResa->_mainProduct = $mainProduct;
		$potentialResa->_selType="start";
		$smarty->assign('potentialResa',$potentialResa);
		$smarty->assign('planningSelector','topcolumn');
		$smarty->assign('planning','topcolumn');	
		
		$planning = myOwnReservationsCalendarController::display($obj, $cookie, $smarty, $mainProduct, $potentialResa, "topcolumn", $ajax);

		if ($ajax) return $planning;
		else $planning = '<div id="primary_block" test="column" style="margin-top:0px">'.$planning.'</div>';

		$cats = explode(',',$mainProduct->ids_categories);
		$obj->_products->getCategoriesLink($cats);
		$lengthWidget = (Configuration::get('MYOWNRES_WIDGET_TYPE')==3);

		$smarty->assign(array (
			'myOwnResLblStart' => $mainProduct->getStartLabel($cookie->id_lang),
			'myOwnResLblEnd' => $mainProduct->getStopLabel($cookie->id_lang),
			'widgetType' => $type,
			'lengthWidget' => $lengthWidget,
			'planning' => $planning,
			'isSel' =>  false
		));

		return $obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/front/widget/top.tpl');

 	}
	
}

?>