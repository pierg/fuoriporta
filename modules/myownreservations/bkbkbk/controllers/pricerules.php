<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnReservationsPricerulesController {

	//==============================================================================================================================================
	//     PRICE RULES LIST
	//==============================================================================================================================================

	public static function show($cookie,$obj) {
		$langue=$cookie->id_lang;
		$output='';
		$isproversion=is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");
		$odd=false;
		if (_PS_VERSION_ < "1.5.0.0") {
			$output .= '
			<div style="padding:20px 10px 0px 10px;">
				<fieldset class="settingsList">
					<legend>
						<img src="../modules/'.$obj->name.'/img/AdminDiscounts.gif" />'.$obj->l('Prices rules management', 'pricerules').'
					</legend>

					<span class="myowndeliveriesSettingsPreTable">
						<a href="#" class="tooltip" title="'.myOwnHelp::$objects['pricerules']['help'].'">'.$obj->l('Edit prices rules', 'pricerules').'</a>
					</span>
					<br />
					<br />';
		} else $output .= myOwnUtils::displayInfo(myOwnHelp::$objects['pricerules']['help'], false);
		
		if (_PS_VERSION_ >= "1.6.0.0") {
			$output .= MyOwnReservationsUtils::table16wrap($obj, 'pricesrules', "Pricesrule", ucfirst(myOwnLang::$objects[MYOWN_OBJ::PRICERULE]), count($obj->_pricerules->list));
		}
		
    	$output .= '
    				<table style="width:100%;background-color:#FFFFFF" class="table">
									<thead>
										<tr>
											<th width="10px">'.$obj->l('ID', 'pricerules').'</th>
											<th>'.$obj->l('Name', 'pricerules').'</th>
											<th width="100px" >'.$obj->l('Type', 'pricerules').'</th>';
											if ($isproversion) 
												$output .= '
											<th >'.ucfirst(myOwnLang::$object[MYOWN_OBJ::FAMILY]).'</th>';
											$output .= '
											<th style="text-align:center;">'.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRODUCT]).'</th>
											<th width="20%"  style="text-align:center;">'.$obj->l('Price impact', 'pricerules').'</th>
											<th width="20%"  style="text-align:center;">'.$obj->l('Options', 'pricerules').'</th>
											<th width="50px" style="text-align:center;">'.$obj->l('Enabled', 'pricerules').'</th>
											<th width="50px" style="text-align:center;">'.$obj->l('Visible', 'pricerules').'</th>
											<th width="70px">'.$obj->l('Actions', 'pricerules').'</th>
										</tr>
									</thead>';
									
								if (count($obj->_pricerules->list)==0) {
									if (_PS_VERSION_ < "1.6.0.0") $output .= '
									<tr>
										<td colspan="9">'.ucfirst(myOwnLang::$operations[MYOWN_OPE::NORES]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRICERULE]).'</td>
									</tr>';
									else $output .= '
									<tr>
										<td class="list-empty" colspan="'.($isproversion ? '10' : '9').'">
											<div class="list-empty-msg">
												<i class="icon-warning-sign list-empty-icon"></i>
												'.ucfirst(myOwnLang::$operations[MYOWN_OPE::NORES]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRICERULE]).'
											</div>
										</td>
									</tr>';
								} else
									foreach($obj->_pricerules->list AS $key => $value) {
										$odd=!$odd;
										$product = 	'';
										$produnitStr=""; $mainProduct=null;
										if (is_array($value->ids_products) && count($value->ids_products)) {
											foreach($value->ids_products as $prod) {
												if ($product!='') $product.=', ';
												$product .= MyOwnReservationsUtils::getProductName($prod, $langue);
												
											}
											$mainProduct = $obj->_products->getResProductFromProduct($prod);
										} else $product = $obj->l('All', 'pricerules');
										/* $impact = '';$calc='';
										if ($value->impact == 0) $impact = '-';
										if ($value->impact == 1) $impact = '=';
										if ($value->impact == 2) $impact = '+';
										if ($value->amount > 0) $calc .= $impact.Tools::ps_round($value->amount, 2).myOwnUtils::getCurrency();
										if ($value->ratio > 0 && $value->impact != 1) $calc .= ' '.$impact.($value->impact!=1 ? number_format((100*$value->ratio),_PS_PRICE_DISPLAY_PRECISION_,'.','').'%' : '');
										if ($calc == '') $calc .= $obj->l('None', 'pricerules');
										if ($mainProduct != null && $value->impact == 3) $calc .= ' /'.myOwnLang::getUnit($mainProduct->_priceUnit); */
										$output .= '
									<tr class="row_hover'.($odd==0 ? ' alt_row odd' : '').'">
										<td>'.$key.'</td>
										<td  style="text-align:center;">'.$value->name .'</td>
										<td style="font-weight:bold;">'.myOwnLang::$priceruleTypes[$value->type].'</td>';
											if ($isproversion) {
												if ($value->id_family>0) {
													if (!array_key_exists($value->id_family, $obj->_products->list)) $prodname = myOwnLang::getOperation($obj, MYOWN_OPE::DELETE);
													else $prodname = $obj->_products->list[$value->id_family]->name;
												} else  $prodname = $obj->l('All', 'timeslots');

												$output .= '
											<td >'.$prodname.'</td>';
											}
											$output .= '
										<td  style="text-align:center;">'.$product .'</td>
										<td  style="text-align:center;"> '.$value->getInpactToString($mainProduct) .'</td>
										<td>';
/* 										$params = $params=explode(";",$value->params);
										if ($value->type == pricesrules_type::DATES) 
											$output .= $obj->l('From(date)', 'pricerules').' '.MyOwnCalendarTool::formatDate(strtotime($params[0]),$langue).(array_key_exists(1, $params) ? ' '.$obj->l('to(date)', 'pricerules').' '.MyOwnCalendarTool::formatDate(strtotime($params[1]),$langue) : '');
										if ($value->type == pricesrules_type::LENGTH or $value->type == pricesrules_type::DELAY or $value->type == pricesrules_type::QTY) 
											if ($params[0]<=1 && $params[1]==0) $output .= $obj->l('For any length', 'pricerules');
											else {
												if (array_key_exists(1, $params) && $params[0]==$params[1]) $output .= $obj->l('For', 'pricerules').' '.$params[0].' ';
												else $output .= $obj->l('From', 'pricerules').' '.$params[0].(array_key_exists(1, $params) ? ' '.$obj->l('to(qty)', 'pricerules').' '.$params[1].' ' : '');
												if ($mainProduct!=null && $value->type != pricesrules_type::QTY) $output .= $produnitStr=myOwnLang::getUnit($mainProduct->_reservationUnit, ($params[1]>1));
											}
										if ($value->type == pricesrules_type::RECURRING) { 
											if (intval($params[0])==0) {
												$output .= $obj->l('For all time slots', 'pricerules').' ';
											} else {
												$output .= $obj->l('For time slot', 'pricerules').' : ';
												if (array_key_exists($params[0],$obj->_timeSlots->list)) $output .= $obj->_timeSlots->list[$params[0]]->name;
												else  $output .= $obj->l('unknown', 'pricerules');
											}
										} */
										$output .= $value->getConditionToString($obj, $mainProduct).'</td>
										<td style="text-align:center;">
											<div>'.myOwnUtils::getBoolItem($value->enable, myOwnReservationsController::getConfUrl('pricesrules&qeditPricesrule='.$key.'&qenable='.intval(!$value->enable))).'
											</div></td>
										<td style="text-align:center;">
											<div>'.myOwnUtils::getBoolItem($value->visible, myOwnReservationsController::getConfUrl('pricesrules&qeditPricesrule='.$key.'&qvisible='.intval(!$value->visible))).'
											</div></td>
										<td style="text-align:center;">
											'.(_PS_VERSION_ < "1.6.0.0" ? '
											<a href="'.myOwnReservationsController::getConfUrl('pricesrules&editPricesrule='.$key).'"><img src="../img/admin/edit.gif" alt="" title="" /></a>
											<a href="'.myOwnReservationsController::getConfUrl('pricesrules','&copyPricesrule='.$key).'"><img src="../img/admin/duplicate.png" alt="'.$obj->l('Copy', 'products').'"></a>
											<a href="'.myOwnReservationsController::getConfUrl('pricesrules&deletePricesrule='.$key).'" onclick="return confirm(\''.ucfirst(myOwnLang::$actions[MYOWN_OPE::DELETE]).' '.$obj->l('prices rule', 'pricerules').' '.$key.' ?\');"><img src="../img/admin/delete.gif" alt="" title="" /></a>
											' : MyOwnReservationsUtils::getListButton($obj, $key, 'pricesrules', 'Pricesrule')).'
										</td>
									</tr>';
									}
				  						
		$output .= '</table>';
		
		if (_PS_VERSION_ >= "1.6.0.0") $output .= '</div>';
		
		if (_PS_VERSION_ < "1.5.0.0") {
      		$output .= '
			<br /><span class="myowndeliveriesSettingsForm"><a href="'.myOwnReservationsController::getConfUrl('pricesrules&editPricesrule=0').'"><img src="../img/admin/add.gif" border="0">&nbsp;'.$obj->l('Add a new prices rule', 'pricerules').'</a>&nbsp;</span>
			<br />
			</fieldset></div>';
		}
		return $output;
	}
	
	//==============================================================================================================================================
	//     ADD/EDIT PRICES RULE
	//==============================================================================================================================================

	public static function edit($cookie,$obj,$id) {
		$langue=$cookie->id_lang;
		$days = myOwnLang::getDaysNames($langue);
		$output='';
		$types = array(2=> $obj->l('Length', 'pricerules'), 3=> $obj->l('Dates', 'pricerules'),4=> $obj->l('Recurring', 'pricerules') );
		$products=$obj->_products->getProducts($langue);

		if (intval($id)>0 ) {
      		$value = $obj->_pricerules->list[$id];
        } else $value=new myOwnPricesRule();
        $params = $params=explode(";",$value->params);
        $output .=myOwnUtils::insertDatepicker($obj, array('pricesruleParamStart', 'pricesruleParamEnd'));
        if ($value->type == pricesrules_type::LENGTH or $value->type==pricesrules_type::DATES or $value->type==pricesrules_type::QTY) $paramImpact = $params[2];
        else $paramImpact=0;
        
        $styles=explode(';', $value->style);
        $textColor='';$backColor='';
        foreach($styles as $styles) {
	        $keyval=explode(':', $styles);
	        if (array_key_exists(1, $keyval) && $keyval[0]=='background-color') $backColor=strtoupper(trim($keyval[1]));
	        if (array_key_exists(1, $keyval) && $keyval[0]=='color') $textColor=strtoupper(trim($keyval[1]));
        }
        
        //object selection
		$obj_sel=null;
		$obj_exts = $obj->_extensions->getExtByType(extension_type::OBJECT);
		foreach ($obj_exts as $obj_ext)
			if ($obj_ext->id==6)
				$obj_sel = $obj_ext;
        
		$output .= '<div style="padding:0px 10px 0px 10px;">';
		//$output .= '<h2 style="margin-top:10px">'.(intval($id)>0 ? ucfirst(myOwnLang::$actions[MYOWN_OPE::EDIT]) : ucfirst(myOwnLang::$actions[MYOWN_OPE::ADD])).' '.$obj->l('prices rule', 'pricerules').'</h2>';
		$output .= '
		<script type="text/javascript" src="'._PS_JS_DIR_.'/jquery/plugins/jquery.colorpicker.js"></script>
		<script type="text/javascript" src="'._MODULE_DIR_.$obj->name.'/views/js/bootstrap-tagsinput.min.js"></script>
		<link href="'._MODULE_DIR_.$obj->name.'/views/css/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" media="all" />
		<script language=\'javascript\'> 
			function increase_brightness(hex, percent){
			    hex = hex.replace(/^\s*#|\s*$/g, "");
			
			    // convert 3 char codes --> 6, e.g. `E0F` --> `EE00FF`
			    if(hex.length == 3){
			        hex = hex.replace(/(.)/g, "$1$1");
			    }
			
			    var r = parseInt(hex.substr(0, 2), 16),
			        g = parseInt(hex.substr(2, 2), 16),
			        b = parseInt(hex.substr(4, 2), 16);
					
				if (percent>0)
			   	 return "#" +
			       ((0|(1<<8) + r + (256 - r) * percent / 100).toString(16)).substr(1) +
			       ((0|(1<<8) + g + (256 - g) * percent / 100).toString(16)).substr(1) +
			       ((0|(1<<8) + b + (256 - b) * percent / 100).toString(16)).substr(1);
			    if (percent<0) 
			   	 return "#" +
			       ((0|(1<<8) + r + (r) * percent / 100).toString(16)).substr(1) +
			       ((0|(1<<8) + g + (g) * percent / 100).toString(16)).substr(1) +
			       ((0|(1<<8) + b + (b) * percent / 100).toString(16)).substr(1);
			}
			function checkQtyOpt() {
				var prods = $("#pricesruleProduct");
				if(prods.val()==null || prods.val()==0) $(".pricesruleQtyParamSameProd").show();
				else {
					$(".pricesruleQtyParamSameProd").hide();
					var comb = $("#pricesruleProductAttribute").val();
					if (comb==0) $(".pricesruleQtyParamSameComb").show();
					else $(".pricesruleQtyParamSameComb").hide();
				}
			}
			function impactChange(impact) {
				if (impact.value=="1" || impact.value=="3")
					$("#pricesruleRatio").hide();
				else
					$("#pricesruleRatio").show();
				
				if (impact.value=="1")
					$("#pricesruleOverall").hide();
				else
					$("#pricesruleOverall").show();
				
				if (impact.value=="3")
					$("#amountUnitPrice span.reservationUnit").show();
				else
					$("#amountUnitPrice span.reservationUnit").hide();
			}';
			$output .= '
			function showTypeOption(type) {
				$("#rule_ts").toggle(type=='.pricesrules_type::RECURRING.');
				$("#rule_length").toggle(type=='.pricesrules_type::LENGTH.');
				$("#rule_dates").toggle(type=='.pricesrules_type::DATES.');
				$("#rule_delay").toggle(type=='.pricesrules_type::DELAY.');
			}

			function setDemoStyle() {
				var backColor = $("#pricesruleBackColor").val();
				var textColor = $("#pricesruleTextColor").val();
				
				if (textColor=="")
					textColor="#ffffff";
				
				if (backColor=="" || backColor=="#ffffff")
					backColor="#31A900";

				var start = increase_brightness(backColor, 30);
				var end = increase_brightness(backColor, -30);

				bgWebKit="-webkit-gradient(linear, left top, left bottom, from("+start+"), to("+end+"))";
				bgMoz="-moz-linear-gradient(top, "+start+", "+end+")";
				$("#demoCell").css({ background: bgMoz }).css({ background: bgWebKit });
				$("#demoCell").css("filter", "progid:DXImageTransform.Microsoft.gradient(startColorstr=\'"+start+"\', endColorstr=\'"+end+"\');");
				$("#demoCell").css("color", textColor);
				$("#demoCell").css("border","solid 1px");
				$("#demoCell").css("border-top-color",start);
				$("#demoCell").css("border-left-color",start);
				$("#demoCell").css("border-bottom-color",end);
				$("#demoCell").css("border-right-color",end);
			}
			$(document).ready(function() {
				showTypeOption('.$value->type.');
				listElem("panelPriceRule");
				$.fn.mColorPicker.init.allowTransparency = false;
				$.fn.mColorPicker.init.replace = false;
				$("#pricesruleBackColor, #pricesruleTextColor").mColorPicker();
				$("#pricesruleBackColor, #pricesruleTextColor").change(function( index ) {
					setDemoStyle();
					if ($(this).val()!="") $(this).val($(this).val().toUpperCase());
				});
				$("div#mColorPickerFooter").css("display","none");

				$(".mColorPickerTrigger").css("position","relative");
				$(".mColorPickerTrigger").css("top","-3px");
				setDemoStyle();
				var weeks=new Array();
				for (i = 1; i < 53; i++) { 
				    weeks.push(i);
				}
			    $("#pricesruleParamWeeks").tagsinput({
			        maxChars: 8,
			        allowDuplicates: false,
			        trimValue: true,
			        typeahead: {
					    source: weeks
					},
				    tagClass: function(item) {
				        return \'label label-week\';
				    }
				});
    		});
		</script> 

		<form id="editPricesruleForm" name="editPricesruleForm" action="'.myOwnReservationsController::getConfUrl('pricesrules').'" method="post">';
		if ($id!=0 ) $output .= '<input type="hidden" name="idPricesrule" value="'.intval($id).'" />';
		$output .= myOwnUtils::displayInfo(myOwnHelp::$objects['pricerules']['help'], false);
		$output .= '
		<table width="100%"><tr><td width="50%" style="vertical-align:top;padding-right:20px">';
		if (_PS_VERSION_ < "1.5.0.0") $output .= '
			<fieldset style="width:95%;margin-bottom: 20px;">
				<legend><img src="../modules/'.$obj->name.'/img/AdminDiscounts.gif" />'.$obj->l('Properties', 'pricerules').'</legend>';
		else $output .= '
			<div class="panel">
				<h3><i class="icon-tags"></i> '.$obj->l('Properties', 'pricerules').'</h3>';
		$output .= '			
				<ol style="padding-left:0px;margin:0px">
					<li class="myowndlist">
						<label for="name" class="li-label">
							<span class="mylabel-tooltip" title="'.myOwnHelp::$objects['pricerules']['name'].'">' .$obj->l('Name', 'pricerules').'</span>
						</label>
						<input style="margin-top:2px;width:40%" type="text" size="40" id="name" name="pricesruleName" value="'.Tools::getValue('pricesruleName', $value->name).'" />
					</li>';
					$obj->checkIsProVersion();
					if ($obj->isProVersion) {
						$output .= '
						<li class="myowndlist">
							<label for="family" class="li-label">'.ucfirst(myOwnLang::$object[MYOWN_OBJ::FAMILY]).'</label>
							<div style="display:inline-block;">'.MyOwnReservationsUtils::insertProductFamillyPicker($obj, $langue, "pricesruleFamily", "", "", Tools::getValue('pricesruleFamily',$value->id_family), "", false, "pricesruleProduct" , $value->ids_products, '.reservationUnit').'</div>
						</li>';
					}
					$output .= '
					<li class="myowndlist">
						<label for="product" class="li-label">
							<span class="mylabel-tooltip" class="tooltip" title="'.myOwnHelp::$objects['pricerules']['product'].'">'.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRODUCT]).'</span>
						</label>
						<div style="display:inline-block;min-width:70%;">'.MyOwnReservationsUtils::insertProductPicker($obj, $cookie->id_lang, "pricesruleProduct", (Tools::getIsset('pricesruleProduct') ? array_filter($_POST['pricesruleProduct']) : $value->ids_products) , "pricesruleProductAttribute", (Tools::getIsset('pricesruleProductAttribute') ? array_filter($_POST['pricesruleProductAttribute']) : $value->ids_attributes), "margin-top:2px;width:100%", "checkQtyOpt();", 0, '.reservationUnit').'</div>
					</li>';
					
		if ($obj_sel!=null) $output .= '
					<li class="myowndlist">
						<label for="product" class="li-label">
							<span class="mylabel">'.$obj_sel->label.'</span>
						</label>
						'.$obj_sel->productSelectInput($obj, 0, $value->id_object, 'width:30%;height:32px;', '', true).'
					</li>';
					
		$output .= '
					<li class="myowndlist">
						'.myOwnUtils::getBoolField($obj, 'pricesruleEnable', Tools::getValue('pricesruleEnable', $value->enable), '<span class="mylabel-tooltip" title="'.myOwnHelp::$objects['pricerules']['enable'].'">'.$obj->l('Enabled', 'pricerules').'</span>', 'width: 160px').'
					</li>
				</ol>
			'.(_PS_VERSION_ < "1.6.0.0" ? '</fieldset>' : '</div>').'
		</td><td width="50%">';
		if (_PS_VERSION_ < "1.6.0.0") $output .= '
			<fieldset style="width:95%;margin-bottom: 20px">
				<legend><img src="../img/admin/appearance.gif" /> '.$obj->l('Appearance', 'pricerules').'</legend>';
		else $output .= '
			<div class="panel">
				<h3><i class="icon-eye"></i> '.$obj->l('Appearance', 'pricerules').'</h3>';
		$output .= '
				<ol style="float:left;padding-left:0px;margin:0px;width:65%">
					<li class="myowndlist">
						<label class="li-label">'.$obj->l('Cell color', 'pricerules').'</label>
						<div style="display:inline-block;width:45%"><div class="input-group">
							<input style="position: relative; top: -3px;" id="pricesruleBackColor" data-mcolorpicker=false" data-hex="true" type="text" size="10" name="pricesruleBackColor" value="'.$backColor.'" />
						</div></div>
					</li>
					<li class="myowndlist">
						<label class="li-label">'.$obj->l('Price color', 'pricerules').'</label>
						<div style="display:inline-block;width:45%"><div class="input-group">
							<input style="position: relative; top: -3px;" id="pricesruleTextColor" data-hex="true" type="text" size="10" name="pricesruleTextColor" value="'.$textColor.'" />
						</div></div>
					</li>
					<li class="myowndlist">
						'.myOwnUtils::getBoolField($obj, 'pricesruleVisible', Tools::getValue('pricesruleVisible', $value->visible)==1, $obj->l('Display as discount', 'pricerules'), 'width:160px').'
					</li>
				</ol>
				<div class="well" style="float:right;width:30%">
					<p style="color: #7F7F7F;font-size: 0.85em;margin-top:-15px">'.$obj->l('Preview', 'pricerules').'</p>
					<div id="demoCell" style="float:right;width:90px;height:40px;padding-top:10px;white-space: nowrap;text-align:center;font-size:11px;font-style: normal;font-variant: normal;font-weight: normal;line-height: 14px;"><b>Demo cell</b><br /><i>from 19,99'.myOwnUtils::getCurrency().'</i></div>
				</div>
				<div style="clear:both"></div>
			'.(_PS_VERSION_ < "1.6.0.0" ? '</fieldset>' : '</div>').'
		</td></tr></table>';

		if (_PS_VERSION_ < "1.6.0.0") $output .= '
			<fieldset style="width:95%;margin-bottom: 20px">
				<legend> '.$obj->l('Conditions', 'pricerules').'</legend>';
		else $output .= '
			<div class="panel">
				<h3><i class="icon-random"></i> '.$obj->l('Conditions', 'pricerules').'</h3>';
		// type
		//-----------------------------------------------------------------------------------------------------------------
		$output .= '
		
		<table cellspacing="0" cellpadding="0" style="width: 100%; margin-bottom:10px;" class="table" id="panelPriceRule" type="single">
			<thead>
				<tr class="nodrag nodrop">
					<th class="center" width="20">
					<th colspan="2"> '.$obj->l('Rule type', 'pricerules').'</th>
					<th width="60%">'.$obj->l('Rule options', 'pricerules').'</th>
				</tr>			
			</thead><tbody>
			';
				foreach (myOwnLang::$priceruleTypes AS $priceruleTypeKey => $priceruleTypesValue) {
						$output .= '
			<tr>
				<td width="20">
					<input style="" class="listElem" title="'.myOwnHelp::$objects['pricerules']['help'].'" type="radio" onChange="listElem(\'panelPriceRule\');" id="pricesruleType" name="pricesruleType" value="' . $priceruleTypeKey. '" '.(Tools::getValue('pricesruleType', $value->type)==$priceruleTypeKey ? ' checked' : '').'>
				</td>
				<td width="30px"><img src="../modules/'.$obj->name.'/img/pricerule'.$priceruleTypeKey.'.png"></td>
				<td>
					<div class="moduleDesc" id="anchorAutoupgrade">
						' . MyOwnReservationsUtils::getsubtitle($priceruleTypesValue) . '
						<p class="desc">'.myOwnLang::$priceruleTypDescription[$priceruleTypeKey].'</p>
						'.myOwnUtils::displaySubInfo(myOwnHelp::$options['pricerules'][$priceruleTypeKey]).' 
					</div>
				</td>
				<td>
					';
					if ($priceruleTypeKey==pricesrules_type::LENGTH) {
						$output .= '				
						<ol idd="rule_length" style="float:left;padding-left:0px;margin:0px;width:100%">
							<li class="myowndlist" style="padding:2px;">
								<label class="sub-li-label">
									<span class="mylabel" title="'.myOwnHelp::$options['pricerules'][pricesrules_type::LENGTH].'">'.$obj->l('Length', 'pricerules').'</span>
								</label>
								<div style="float: left;">
									'.myOwnUtils::getInputField('pricesruleParamMin', Tools::getValue('pricesruleParamMin', ($value->type ==pricesrules_type::LENGTH ? $params[0] : '')), '<span class="reservationUnit" id="reservationUnitForLength"></span>', 'width:50px', '', false, '', '', 0, $obj->l('From', 'pricerules')).'
								</div>
								<div style="float: left;padding-left:5px;">
									'.myOwnUtils::getInputField('pricesruleParamMax', Tools::getValue('pricesruleParamMax', ($value->type ==pricesrules_type::LENGTH ? $params[1] : '')), '<span class="reservationUnit" id="reservationUnitForLength"></span>', 'width:50px;display:inline-block', '', false, '', '', 0, $obj->l('To', 'pricerules')).'
								</div>
								<div style="float: left;padding-left:5px;padding-top:8px;">
									<input class="addTimeSlotsDay" name="pricesruleLengthParamImpact" type="checkbox" value="1" '.(Tools::getValue('pricesruleLengthParamImpact',$paramImpact)==1 ? 'checked' : '').'/> <span class="mylabel" style="font-weight:normal">'.$obj->l('or more', 'pricerules').'</span>
								</div>
								<div style="clear:both;"></div>
							</li>
							<li class="myowndlist" style="padding:2px;">
								<label class="sub-li-label">
									<span class="mylabel" title="">'.$obj->l('Start constraint', 'pricerules').'</span>
								</label>
								<select style="width:30%;display:inline-block;height:22px" name="pricesruleParamLengthTimeslot">';
									$output .='<option value="0" ';
									if (Tools::getValue('pricesruleParamLengthTimeslot', ($value->type ==pricesrules_type::LENGTH  ? (isset($params[3]) ? intval($params[3]) : '') : '')) == 0) $output .='selected ';
									$output .='>'.$obj->l('None', 'pricerules').'</option>';
									if (count($obj->_timeSlots->list))
									foreach($obj->_timeSlots->list as $key => $ts) {
										$output .='<option value="'.$key.'" ';
										if (Tools::getValue('pricesruleParamLengthTimeslot', ($value->type ==pricesrules_type::LENGTH  ? (isset($params[3]) ? intval($params[3]) : '') : '')) == $key) $output .='selected ';
										$output .='>'.$ts->name.'</option>';
									}
								$output .='</select>
								<select style="width:30%;display:inline-block;height:22px" name="pricesruleParamOneDay">';
									$output .='<option value="0" ';
									if (Tools::getValue('pricesruleParamOneDay', ($value->type ==pricesrules_type::LENGTH  ? (isset($params[4]) ? intval($params[4]) : '') : '')) == 0) $output .='selected ';
									$output .='>'.$obj->l('None', 'pricerules').'</option>';
									foreach(myOwnLang::getDaysNames($langue) as $dayId=>$dayName) {
										$output .='<option value="'.$dayId.'" ';
										if (Tools::getValue('pricesruleParamOneDay', ($value->type ==pricesrules_type::LENGTH  ? (isset($params[4]) ? intval($params[4]) : '') : '')) == $dayId) $output .='selected ';
										$output .='>'.$dayName.'</option>';
									}
								$output .='</select> 
							</li>

						</ol>';
					}
					if ($priceruleTypeKey==pricesrules_type::DELAY) {
						$output .= '
						<ol idd="rule_delay" style="float:left;padding-left:0px;margin:0px;width:100%">
							<li class="myowndlist" style="padding:2px;">
								<label class="sub-li-label">
									<span class="mylabel">'.$obj->l('Delay', 'pricerules').'</span>
								</label>
								
								<div style="float: left;padding-left:5px;">
									'.myOwnUtils::getInputField('pricesruleParamMinDelay', Tools::getValue('pricesruleParamMinDelay', ($value->type ==pricesrules_type::DELAY ? $params[0] : '')), '<span class="reservationUnit" id="reservationUnitForLength"></span>', 'width:50px;display:inline-block', '', false, '', '', 0, $obj->l('From', 'pricerules')).'
								</div>
								<div style="float: left;padding-left:5px;">
									'.myOwnUtils::getInputField('pricesruleParamMaxDelay', Tools::getValue('pricesruleParamMaxDelay', ($value->type ==pricesrules_type::DELAY ? $params[1] : '')), '<span class="reservationUnit" id="reservationUnitForLength"></span>', 'width:50px;display:inline-block', '', false, '', '', 0, $obj->l('To', 'pricerules')).'
								</div>
							</li>
						</ol>';
					}
					if ($priceruleTypeKey==pricesrules_type::QTY) {
						$output .= '
						<ol idd="rule_delay" style="float:left;padding-left:0px;margin:0px;width:100%">
							<li class="myowndlist" style="padding:2px;">
								<label class="sub-li-label">
									<span class="mylabel">'.$obj->l('Quantity', 'pricerules').'</span>
								</label>
								
								<div style="float: left;padding-left:5px;">
									'.myOwnUtils::getInputField('pricesruleParamMinQty', Tools::getValue('pricesruleParamMinQty', ($value->type ==pricesrules_type::QTY ? $params[0] : '')), '<span class="reservationUnit" id="reservationUnitForLength"></span>', 'width:50px;display:inline-block', '', false, '', '', 0, $obj->l('From', 'pricerules')).'
								</div>
								<div style="float: left;padding-left:5px;">
									'.myOwnUtils::getInputField('pricesruleParamMaxQty', Tools::getValue('pricesruleParamMaxQty', ($value->type ==pricesrules_type::QTY ? $params[1] : '')), '<span class="reservationUnit" id="reservationUnitForLength"></span>', 'width:50px;display:inline-block', '', false, '', '', 0, $obj->l('To', 'pricerules')).'
								</div>
								<div style="float: left;padding-left:5px;padding-top:8px;">
									<input class="addTimeSlotsDay" name="pricesruleQtyParamImpact" type="checkbox" value="1" '.(Tools::getValue('pricesruleQtyParamImpact',$paramImpact)==1 ? 'checked' : '').'/> <span class="mylabel" style="font-weight:normal;display:inline-block">'.$obj->l('or more', 'pricerules').'</span>
								</div>
								<div style="clear:both;"></div>
							</li>
							<li class="myowndlist" style="padding:2px;">
								<label class="sub-li-label">
									<span class="mylabel">'.$obj->l('Restriction', 'pricerules').'</span>
								</label>
								<div style="display:inline-block">
									<input class="addTimeSlotsDay pricesruleQtyParamSameProd" name="pricesruleQtyParamSameProd" type="checkbox" value="1" '.(Tools::getValue('pricesruleQtyParamSameProd',isset($params[3]) ? $params[3] : 0)==1 ? 'checked' : '').'/> <span class="mylabel pricesruleQtyParamSameProd" style="font-weight:normal;display:inline-block">'.$obj->l('For same product', 'pricerules').'&nbsp;&nbsp;</span>
									<input class="addTimeSlotsDay pricesruleQtyParamSameComb" name="pricesruleQtyParamSameComb" type="checkbox" value="1" '.(Tools::getValue('pricesruleQtyParamSameComb',isset($params[4]) ? $params[4] : 0)==1 ? 'checked' : '').'/> <span class="mylabel pricesruleQtyParamSameComb" style="font-weight:normal;display:inline-block">'.$obj->l('For same combination', 'pricerules').'&nbsp;&nbsp;</span>
									
									<input class="addTimeSlotsDay" name="pricesruleQtyParamSamePeriod" type="checkbox" value="1" '.(Tools::getValue('pricesruleQtyParamSamePeriod',isset($params[5]) ? $params[5] : 0)==1 ? 'checked' : '').'/> <span class="mylabel" style="font-weight:normal;display:inline-block">'.$obj->l('For same period', 'pricerules').'</span>
								</div>
							</li>
						</ol>';
					}
					if ($priceruleTypeKey==pricesrules_type::HISTORY) {
						$output .= '
						<ol idd="rule_delay" style="float:left;padding-left:0px;margin:0px;width:100%">
							<li class="myowndlist" style="padding:2px;">
								<label class="sub-li-label">
									<span class="mylabel">'.$obj->l('Total length', 'pricerules').'</span>
								</label>
								
								<div style="float: left;padding-left:5px;">
									'.myOwnUtils::getInputField('pricesruleParamMinTotal', Tools::getValue('pricesruleParamMinTotal', ($value->type ==pricesrules_type::HISTORY ? $params[0] : '')), '<span class="reservationUnit" id="reservationUnitForLength"></span>', 'width:50px;display:inline-block', '', false, '', '', 0, $obj->l('From', 'pricerules')).'
								</div>
								<div style="float: left;padding-left:5px;">
									'.myOwnUtils::getInputField('pricesruleParamMaxTotal', Tools::getValue('pricesruleParamMaxTotal', ($value->type ==pricesrules_type::HISTORY ? $params[1] : '')), '<span class="reservationUnit" id="reservationUnitForLength"></span>', 'width:50px;display:inline-block', '', false, '', '', 0, $obj->l('To', 'pricerules')).'
								</div>
								<div style="float: left;padding-left:5px;padding-top:8px;">
									<input class="addTimeSlotsDay" name="pricesruleQtyParamImpact" type="checkbox" value="1" '.(Tools::getValue('pricesruleQtyParamImpact',$paramImpact)==1 ? 'checked' : '').'/> '.$obj->l('or more', 'pricerules').'</span>
								</div>
								<div style="clear:both;"></div>
							</li>
							<li class="myowndlist" style="padding:2px;">
								<label class="sub-li-label">
									<span class="mylabel">'.$obj->l('Restriction', 'pricerules').'</span>
								</label>
								<div style="display:inline-block">
									<input class="addTimeSlotsDay pricesruleQtyParamSameProd" name="pricesruleQtyParamSameProd" type="checkbox" value="1" '.(Tools::getValue('pricesruleQtyParamSameProd',isset($params[3]) ? $params[3] : 0)==1 ? 'checked' : '').'/> <span class="mylabel pricesruleQtyParamSameProd" style="font-weight:normal;display:inline-block">'.$obj->l('For same product', 'pricerules').'&nbsp;&nbsp;</span>
									<input class="addTimeSlotsDay pricesruleQtyParamSameComb" name="pricesruleQtyParamSameComb" type="checkbox" value="1" '.(Tools::getValue('pricesruleQtyParamSameComb',isset($params[4]) ? $params[4] : 0)==1 ? 'checked' : '').'/> <span class="mylabel pricesruleQtyParamSameComb" style="font-weight:normal;display:inline-block">'.$obj->l('For same combination', 'pricerules').'&nbsp;&nbsp;</span>
									
									'.($obj->_extensions->getExtByName('attendees')!=null ? '<input class="addTimeSlotsDay" name="pricesruleQtyParamSameAttendee" type="checkbox" value="1" '.(Tools::getValue('pricesruleQtyParamSameAttendee',isset($params[5]) ? $params[5] : 0)==1 ? 'checked' : '').'/> <span class="mylabel" style="font-weight:normal;display:inline-block">'.$obj->l('For same attendee', 'pricerules').'</span>' : '').'
								</div>
							</li>
						</ol>';
					}
					if ($priceruleTypeKey==pricesrules_type::DATES) {
						$output .= '
						<ol idd="rule_dates" style="float:left;padding-left:0px;margin:0px">
							<li class="myowndlist" style="padding:2px;">
								<label class="sub-li-label">
									<span class="mylabel">'.$obj->l('Period', 'pricerules').' </span>
								</label>
								<div style="display:inline-block" >
									'.MyOwnReservationsUtils::getDateField('pricesruleParamStart', Tools::getValue('pricesruleParamStart', ($value->type ==pricesrules_type::DATES ? $params[0] : '')), $obj->l('From(date)', 'pricerules'), '', '', '').'
								</div>
								<div style="display:inline-block" >
									'.MyOwnReservationsUtils::getDateField('pricesruleParamEnd', Tools::getValue('pricesruleParamEnd', ($value->type ==pricesrules_type::DATES ? $params[1] : '')), $obj->l('to(date)', 'pricerules'), '', '', '').'
								</div>
								<div style="display:inline-block;vertical-align:top;padding-left:5px;padding-top:8px;" >
									<input class="addTimeSlotsDay" name="pricesruleDateParamImpact" type="checkbox" value="1" '.(Tools::getValue('pricesruleDateParamImpact',$paramImpact)==1 ? 'checked' : '').'/> <span class="mylabel" style="font-weight:normal">'.$obj->l('or later', 'pricerules').'</span>
								</div>
							</li>
						</ol>';
					}
					if ($priceruleTypeKey==pricesrules_type::RECURRING) {
						$output .= '
						<ol idd="rule_ts" style="float:left;padding-left:0px;margin:0px;width:100%">
							<li class="myowndlist" style="padding:2px;">
								<label class="sub-li-label">
									<span class="mylabel">'.ucfirst(myOwnLang::$object[MYOWN_OBJ::TIMESLOT]).'</span>
								</label>
								<select style="width:25%"  name="pricesruleParamTimeslot">';
									$output .='<option value="0" ';
									if (Tools::getValue('pricesruleParamTimeslot', ($value->type ==pricesrules_type::RECURRING ? intval($params[0]) : '')) == 0) $output .='selected ';
									$output .='>'.$obj->l('All', 'pricerules').'</option>';
									if (count($obj->_timeSlots->list))
									foreach($obj->_timeSlots->list as $key => $ts) {
										$output .='<option value="'.$key.'" ';
										if (Tools::getValue('pricesruleParamTimeslot', ($value->type ==pricesrules_type::RECURRING ? intval($params[0]) : '')) == $key) $output .='selected ';
										$output .='>'.$ts->name.'</option>';
									}
								$output .='</select>
							</li>
							<li class="myowndlist" style="padding:2px;">
								<label class="sub-li-label">'.$obj->l('Days', 'pricerules').'</label><div style="clear:both"></div>
								<div style="display:inline-block">';
									foreach(myOwnLang::getDaysNames($langue) as $dayId=>$dayName) {
										$output .= myOwnUtils::getBoolField($obj, 'pricesruleParamDay'.$dayId, Tools::getValue('pricesruleParamDay'.$dayId, ($value->type == pricesrules_type::RECURRING ? $params[$dayId] : ''))==1, $dayName, 'width:140px').'<div style="clear:both"></div>';
										/*$output .= '
										<input class="addTimeSlotsDay" name="pricesruleParamDay'. $dayId .'" type="checkbox" value="1" ';
											if (Tools::getValue('pricesruleParamDay'.$dayId, ($value->type ==pricesrules_type::RECURRING ? $params[$dayId] : ''))==1) $output .= 'checked';
											$output .= '/> '.$dayName.'&nbsp;&nbsp;&nbsp;';*/
									}	
									$output .= '
								</div>
							</li>
							
							<li class="myowndlist">
								<label class="sub-li-label">'.$obj->l('Weeks', 'pricerules').'</label>
								
								<input placeholder="'.$obj->l('Type week number', 'pricerules').'" style="width:50%" type="text" name="pricesruleParamWeeks" id="pricesruleParamWeeks" value="';
								$weeks = explode(';', $params[8]);
								foreach ($weeks as $week)
									if (Tools::strlen(trim($week)) > 0) $output .= trim($week).', ';
								$output .= '">
								
							</li>
						</ol>';
					}
					$output .= '
				</td>
			</tr>
			';
				}
				$output .= '
		</table>
		<div style="clear:both"></div>
		'.(_PS_VERSION_ < "1.6.0.0" ? '</fieldset>' : '</div>');

		if (_PS_VERSION_ < "1.6.0.0") $output .= '
			<fieldset style="margin-bottom: 30px;">
				<legend><img src="../img/admin/invoice.gif" />'.$obj->l('Price calculation', 'pricerules').'</legend>';
		else $output .= '
			<div class="panel">
				<h3><i class="icon-money"></i> '.$obj->l('Price calculation', 'pricerules').'</h3>';
				
		 $output .= '	
				<ol style="padding-left:0px;margin:0px;width: 50%;">
					<li class="myowndlist">
						<label for="name" class="li-label">
							<span class="mylabel-tooltip" title="'.myOwnHelp::$objects['pricerules']['impact'].'">'.$obj->l('Impact', 'pricerules').'</span>
						</label>
						<select style="width:30%" name="pricesruleImpact" id="impact" onchange="impactChange(this);">';
					foreach (myOwnLang::$priceruleImpacts as $key => $impact) {
						$output .='<option value="'.$key.'"'.(Tools::getValue('pricesruleImpact', $value->impact)==$key ? ' selected' : '').' >'.$impact.'</option>';
					}
					$output .='</select>
					</li>
					<li class="myowndlist" id="pricesruleRatio" '.($value->impact==1 ? 'style="display:none"' : '').'>
						<label for="name" class="li-label">
							<span class="mylabel-tooltip" title="'.myOwnHelp::$objects['pricerules']['ratio'].'">'.$obj->l('Ratio', 'pricerules').'</span>
						</label>
						'.myOwnUtils::getInputField('pricesruleRatio', Tools::getValue('pricesruleRatio', number_format((100*$value->ratio),_PS_PRICE_DISPLAY_PRECISION_,'.','')), '%', 'width:50px;display:inline-block', '').'
					</li>
					<li class="myowndlist" id="amountUnitPrice" >
						<label for="name" class="li-label">
							<span class="mylabel-tooltip" title="'.myOwnHelp::$objects['pricerules']['amount'].'">'.$obj->l('Amount', 'pricerules').'</span>
						</label>
						'.myOwnUtils::getInputField('pricesruleAmount', Tools::getValue('pricesruleAmount',number_format($value->amount,_PS_PRICE_DISPLAY_PRECISION_,'.','')), myOwnUtils::getCurrency().' '.$obj->l('(taxes incl.)', 'pricerules').'&nbsp;<span class="reservationUnit" '.($value->impact!=3 ? 'style="display:none"' : '').'></span>', 'width:80px;display:inline-block', '').'
						
					</li>
					<li class="myowndlist" id="pricesruleOverall" '.($value->impact==1 ? 'style="display:none"' : '').'>
						'.myOwnUtils::getBoolField($obj, 'pricesruleOverallImpact', Tools::getValue('pricesruleOverallImpact', $value->overall)==1, $obj->l('Scope', 'pricerules'), 'width:160px', myOwnHelp::$objects['pricerules']['over']).'
					</li>
					<li class="myowndlist">
						'.myOwnUtils::getBoolField($obj, 'pricesruleQuantityImpact', Tools::getValue('pricesruleQuantityImpact', $value->quantity), $obj->l('Apply for each quantity', 'pricerules'), 'width:160px', myOwnHelp::$objects['pricerules']['quantity']).'
					</li>
					<div style="clear:both"></div>
				</ol>
			'.(_PS_VERSION_ < "1.6.0.0" ? '</fieldset>' : '</div>').'
			
		</form></div>';
		
		return $output;
	}
}



?>