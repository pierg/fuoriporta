<?php
/*
* 2010-2012 LaBulle All right reserved
*/
 


class myOwnReservationsStockController
{
	
	public static function sortIdAsc($a,$b) {
		  if ($a->sqlId == $b->sqlId) return 0;
	      return ($a->sqlId < $b->sqlId) ? -1 : 1;
	}
	public static function sortIdDesc($a,$b) {
		  if ($a->sqlId == $b->sqlId) return 0;
	      return ($a->sqlId > $b->sqlId) ? -1 : 1;
	}
	public static function sortNameAsc($a,$b) {
		  if ($a->name == $b->name) return 0;
	      return ($a->name < $b->name) ? -1 : 1;
	}
	public static function sortNameDesc($a,$b) {
		  if ($a->name == $b->name) return 0;
	      return ($a->name > $b->name) ? -1 : 1;
	}
	public static function sortSerialAsc($a,$b) {
		  if ($a->serial == $b->serial) return 0;
	      return ($a->serial < $b->serial) ? -1 : 1;
	}
	public static function sortSerialDesc($a,$b) {
		  if ($a->serial == $b->serial) return 0;
	      return ($a->serial > $b->serial) ? -1 : 1;
	}
	public static function sortProductAsc($a,$b) {
		  if ($a->id_product == $b->id_product) return 0;
	      return ($a->id_product < $b->id_product) ? -1 : 1;
	}
	public static function sortProductDesc($a,$b) {
		  if ($a->id_product == $b->id_product) return 0;
	      return ($a->id_product > $b->id_product) ? -1 : 1;
	}
	public static function sortAttributeAsc($a,$b) {
		  if ($a->id_product_attribute == $b->id_product_attribute) return 0;
	      return ($a->id_product_attribute < $b->id_product_attribute) ? -1 : 1;
	}
	public static function sortAttributeDesc($a,$b) {
		  if ($a->id_product_attribute == $b->id_product_attribute) return 0;
	      return ($a->id_product_attribute > $b->id_product_attribute) ? -1 : 1;
	}
	
	public static function printStock($obj, $dayFilter, $productFamillyFilter) {
    	global $cookie;
    	//header('Content-type: application/pdf');
		//header('Content-Disposition: attachment; filename="stock'.($dayFilter != '' ? '_'.$dayFilter : '').'.pdf"');
		if (!class_exists('TCPDF'))
    		require_once(MYOWNRES_PATH.'library/tcpdf/tcpdf.php');
    	$start = $end = strtotime($dayFilter);
	
		// create new PDF document
		$pdf = new TCPDF('l', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('laBulle');
		$pdf->SetTitle(MyOwnCalendarTool::formatDate($start, $cookie->id_lang));
		
		// set default header data
		$pdf->SetHeaderData('', '',$obj->l('Product stock', 'stocks').($dayFilter != '' ? ' '.MyOwnCalendarTool::formatDate($start, $cookie->id_lang) : ''), '');
		
		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		
		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(10);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		
		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		
		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		    require_once(dirname(__FILE__).'/lang/eng.php');
		    $pdf->setLanguageArray($l);
		}
		
		// set font
		$pdf->SetFont('helvetica', '', 8);
		
		// add a page
		$pdf->AddPage();

		// data loading
		$obj = Module::getInstanceByName('myownreservations');
		
		$tbl=self::displayStock($obj, $dayFilter, $productFamillyFilter, true);
		
		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output();
		//$pdf->Output('stock'.($dayFilter != '' ? '_'.$dayFilter : '').'.pdf', 'I');
	}
	
	public static function printSerial() {
		if (Tools::getValue('barCodesList', '')!='') {
			header('Content-type: application/pdf');
			header('Content-Disposition: attachment; filename="barcodes.pdf"');
			
			include_once(MYOWNRES_PATH.'pro/pdf-barcode.php');
			include_once(MYOWNRES_PATH.'pro/php-barcode.php');
			
			$_stocks = new myOwnStocks();
			$barCodesTab = explode(";",$_POST['barCodesList']);

			$format = Tools::getValue('printFormat', '5160');
			$pdf = new PDF_barcodes($format, 'mm', 1, 1);
			
			$pdf->Open();
			$pdf->AddPage();
			
			// Print labels
			if (count($_stocks->list)>0)
			foreach ($_stocks->list as $stock) {
				if (in_array($stock->sqlId, $barCodesTab)) {
					$img = barcode_print($stock->serial,"ANY",3,false);
					if ($img!=null) $pdf->Add_Label($img, $stock->name);
				}
			}
			
			$pdf->Output();
		
		}
	}
	
	public static function printSerialv2() {
		if (Tools::getValue('barCodesList', '')!='') {
			//header('Content-type: application/pdf');
			//header('Content-Disposition: attachment; filename="barcodes.pdf"');

			include_once(MYOWNRES_PATH.'pro/php-barcode.php');
			
			require_once(_PS_TOOL_DIR_.'tcpdf/config/lang/eng.php');
			if(class_exists('TCPDF') != true)
				include_once(_PS_TOOL_DIR_.'tcpdf/tcpdf.php');
			
			require_once(MYOWNRES_PATH.'pro/class.label.php');
			require_once(MYOWNRES_PATH.'pro/labelTemplate.php');
			
			$format = Tools::getValue('printFormat', '1');
			
			$_stocks = new myOwnStocks();
			$barCodesTab = explode(";",$_POST['barCodesList']);


			$data = array();

			if (count($_stocks->list)>0)
			foreach ($_stocks->list as $stock) {
				if (in_array($stock->sqlId, $barCodesTab)) {
					$img = barcode_print($stock->serial,"ANY",3,false);
					if ($img!=null) {
						$info= array (
							'barcode'=> $img,
							'serial' => $stock->serial
						);
					}
					array_push($data,$info);
				}
			}

			$pdf = new labelTemplate( $format, $data , MYOWNRES_PATH.'pro/', "labels.xml", true);
			//$pdf = new label( $label_id, $data , CLASS_PATH."label/", "labels.xml", true);
			
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->SetAuthor("laBulle");
			$pdf->SetTitle("Etiquettes par myOwnReservations");
			$pdf->SetSubject("Création d'étiquettes Code Barre");
			$pdf->SetKeywords("TCPDF, PDF, example, test, guide, kiwi");
			
			// remove default header/footer
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);
			
			// remove default margin
			$pdf->SetHeaderMargin(0);
			$pdf->SetFooterMargin(0);
			
			$pdf->SetAutoPageBreak( true, 0);
			
			//set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
			
			$pdf->Addlabel();
			
			// Affichage du document dans le navigateur
			$pdf->Output("barcodes.pdf", "I");
		}
	}
		
	public static function displayList($obj, $dayFilter, $productFamillyFilter, $print=false) {
		global $cookie;
		global $rights;
		if(!isset($_SESSION)) session_start();
		$output='';
		$_timeSlots = $obj->_timeSlots;
		$_products = $obj->_products;
		$_pricerules = $obj->_pricerules;
$odd=false;
		$reservationFilterLabels = array("start" => $obj->l('Starting', 'stocks'), "run" => $obj->l('Running', 'stocks'), "end" => $obj->l('Ending', 'stocks'), "free" => $obj->l('Free', 'stocks') );
		//get products list
		$products=array();
		$timeslots=array();
		
		$carriers = MyOwnReservationsUtils::listCarriers(); $carrierName="";
		$payment_modules = array();
		foreach (PaymentModule::getInstalledPaymentModules() as $p_module)
			$payment_modules[] = Module::getInstanceById((int)$p_module['id_module']);
			
		$paymentName="";
		foreach($obj->_products->list as $employeeProductFamily)
			if ($productFamillyFilter==0 or $productFamillyFilter==$employeeProductFamily->sqlId) {
				if ($employeeProductFamily->productType==product_type::STOCK) { 
					foreach($employeeProductFamily->getProducts($cookie->id_lang) as $product) 
						$products[$product['id_product']]=$product;
					if ($employeeProductFamily->reservationSelType==reservation_unit::TIMESLOT)
						$timeslots += $employeeProductFamily->_timeslotsObj->list;
				}
			}

		//fill filters
		$stockproduct=array();
		$stockproductattribute=array();


		if (isset($_POST["submitReset"]) && $_POST["submitReset"]!="") {
			unset($_SESSION["filter_id_stock"]);
			unset($_SESSION["filter_id_product"]);
			unset($_SESSION["filter_id_product_attribute"]);
			unset($_SESSION["filter_name"]);
			unset($_SESSION["filter_customer"]);
			unset($_SESSION["filter_carrier"]);
			unset($_SESSION["filter_payment"]);
			unset($_SESSION["filter_serial"]);
			unset($_SESSION["filter_timeslot"]);

			$idFilter="";
			$productFilter="";
			$productAttributeFilter="";
			$nameFilter="";
			$serialFilter="";
			$customerFilter="";
			$carrierFilter="";
			$paymentFilter="";
			$timeslotFilter="";
			$reservationFilter="";
		} else { 			
			if (isset($_POST["filter_id_stock"])) $_SESSION["filter_id_stock"] = $_POST["filter_id_stock"];
			if (isset($_SESSION["filter_id_stock"])) $idFilter=$_SESSION["filter_id_stock"];
			else $idFilter="";
			
			if (isset($_POST["filter_id_product"])) $_SESSION["filter_id_product"] = $_POST["filter_id_product"];
			if (isset($_SESSION["filter_id_product"])) $productFilter=$_SESSION["filter_id_product"];
			else $productFilter="";
			if (!array_key_exists($productFilter, $products))
				$productFilter = $_SESSION["filter_id_product"] = "";
			
			if (isset($_POST["filter_id_product_attribute"])) $_SESSION["filter_id_product_attribute"] = $_POST["filter_id_product_attribute"];
			if (isset($_SESSION["filter_id_product_attribute"])) $productAttributeFilter=$_SESSION["filter_id_product_attribute"];
			else $productAttributeFilter="";
			
			if (isset($_POST["filter_name"])) $_SESSION["filter_name"] = $_POST["filter_name"];
			if (isset($_SESSION["filter_name"])) $nameFilter=$_SESSION["filter_name"];
			else $nameFilter="";
			
			if (isset($_POST["filter_customer"])) $_SESSION["filter_customer"] = $_POST["filter_customer"];
			if (isset($_SESSION["filter_customer"])) $customerFilter=$_SESSION["filter_customer"];
			else $customerFilter="";
			
			if (isset($_POST["filter_carrier"])) $_SESSION["filter_carrier"] = $_POST["filter_carrier"];
			if (isset($_SESSION["filter_carrier"])) $carrierFilter=$_SESSION["filter_carrier"];
			else $carrierFilter="";
				
			if (isset($_POST["filter_payment"])) $_SESSION["filter_payment"] = $_POST["filter_payment"];
			if (isset($_SESSION["filter_payment"])) $paymentFilter=$_SESSION["filter_payment"];
			else $paymentFilter="";
			
			if (isset($_POST["filter_serial"])) $_SESSION["filter_serial"] = $_POST["filter_serial"];
			if (isset($_SESSION["filter_serial"])) $serialFilter=$_SESSION["filter_serial"];
			else $serialFilter="";
			
			if (isset($_POST["filter_timeslot"])) $_SESSION["filter_timeslot"] = $_POST["filter_timeslot"];
			if (isset($_SESSION["filter_timeslot"])) $timeslotFilter=$_SESSION["filter_timeslot"];
			else $timeslotFilter="";
			if (!array_key_exists($timeslotFilter, $timeslots))
				$timeslotFilter = $_SESSION["filter_timeslot"] = "";
				
			if (isset($_POST["filter_reservation"])) $_SESSION["filter_reservation"] = $_POST["filter_reservation"];
			if (isset($_SESSION["filter_reservation"])) $reservationFilter=$_SESSION["filter_reservation"];
			else $reservationFilter="";
		}
		
		$isfiltered = ($reservationFilter!="" || $timeslotFilter!="" || $serialFilter!="" || $paymentFilter!="" || $carrierFilter!="" || $customerFilter!="" || $nameFilter!="" || $productAttributeFilter!="" || $productFilter!="" || $idFilter!="");
		
		foreach ($obj->_stocks->getAllFiltered($obj, $products, $timeslots) as $stock) {
			if (!array_key_exists($stock->id_product, $stockproduct)) 
				$stockproduct[$stock->id_product] = MyOwnReservationsUtils::getProductName($stock->id_product, $cookie->id_lang);
			$attrtoadd = str_replace("<br />",", ",MyOwnReservationsUtils::getAttributeCombinaisons($stock->id_product, $stock->id_product_attribute, $cookie->id_lang));
			if (($productFilter=='' or $stock->id_product==$productFilter) && trim($attrtoadd)!="" && !array_key_exists($stock->id_product.'-'.$stock->id_product_attribute, $stockproductattribute))
				$stockproductattribute[$stock->id_product.'-'.$stock->id_product_attribute] = $attrtoadd;
		}
		
		if ($productAttributeFilter!="") {
			$prodAttrFilterTab = explode("-",$productAttributeFilter);
			$productFilter = $prodAttrFilterTab[0];
			$productAttrFilter = $prodAttrFilterTab[1];
		} else $productAttrFilter="";

		//$sortedStockList= myOwnStocks::getFiltered($_products, $products, $_timeSlots->list, strtotime($dayFilter), "", $idFilter, $productFilter, $productAttrFilter, $nameFilter, $serialFilter);
		$sortedStockList= $obj->_stocks->getAllFiltered($obj, $products, $timeslots, strtotime($dayFilter), "", $idFilter, $productFilter, $productAttrFilter, $nameFilter, $serialFilter, $reservationFilter, $timeslotFilter, $customerFilter, $carrierFilter, $paymentFilter);

		$orderby="product";
		if (isset($_POST['orderby'])) $orderby=$_POST['orderby'];
		$orderway="asc";
		if (isset($_POST['orderway'])) $orderway=$_POST['orderway'];

		if ($orderby=='id' && $orderway=='asc') uasort($sortedStockList, array('myOwnReservationsStockController', 'sortIdAsc') );
		if ($orderby=='id' && $orderway=='desc') uasort($sortedStockList, array('myOwnReservationsStockController', 'sortIdDesc') );
		if ($orderby=='name' && $orderway=='asc') uasort($sortedStockList, array('myOwnReservationsStockController', 'sortNameAsc') );
		if ($orderby=='name' && $orderway=='desc') uasort($sortedStockList, array('myOwnReservationsStockController', 'sortNameDesc') );
		if ($orderby=='serial' && $orderway=='asc') uasort($sortedStockList, array('myOwnReservationsStockController', 'sortSerialAsc') );
		if ($orderby=='serial' && $orderway=='desc') uasort($sortedStockList, array('myOwnReservationsStockController', 'sortSerialDesc') );
		if ($orderby=='product' && $orderway=='asc') uasort($sortedStockList, array('myOwnReservationsStockController', 'sortProductAsc') );
		if ($orderby=='product' && $orderway=='desc') uasort($sortedStockList, array('myOwnReservationsStockController', 'sortProductDesc') );
		if ($orderby=='attribute' && $orderway=='asc') uasort($sortedStockList, array('myOwnReservationsStockController', 'sortAttributeAsc') );
		if ($orderby=='attribute' && $orderway=='desc') uasort($sortedStockList, array('myOwnReservationsStockController', 'sortAttributeDesc') );
		
		$filterStockList=array();
		
		$paginationOpts=array(20,50,100,200);
		$label=$obj->l('Labels', 'stocks');

		$cnt=0;
		$pagCnt = (int)Tools::getValue('pagination', 20);
		$page=0;
		if (isset($_POST["page"])) $page=intval($_POST["page"]);
		foreach ($sortedStockList as $sortedStockItem) {
			if ($print or ($cnt>=($page*$pagCnt) && $cnt< (($page+1)*$pagCnt) ) ) {
				$filterStockList[] = $sortedStockItem;
			}
			$cnt++;
		}
		
		$pageTotal=ceil(count($sortedStockList)/$pagCnt);
		
		if (!$print) {
			$output .= myOwnReservationsReservationsController::getScript($obj, $print).'
		<form method="post" id="stockform" action="'.myOwnReservationsController::getConfUrl('stock', '&products='.$productFamillyFilter).'" class="form">
				<input type="hidden" id="submitFiltercustomer" name="submitFiltercustomer" value="0">
				<input type="hidden" id="productFamillyFilterStock" name="productFamillyFilter" value="'.$productFamillyFilter.'">
				<input type="hidden" id="page" name="page" value="'.$page.'">
				<input type="hidden" id="orderway" name="orderway" value="'.$orderway.'">
				<input type="hidden" name="datepickerFrom" value="'.$dayFilter.'">
				<input type="hidden" id="stockformaction" name="action" value="filterStock">
				<input type="hidden" id="orderby" name="orderby" value="'.$orderby.'">';
			/*$output .=' 
				<table width="100%" style="border-collapse:collapse;">
				<tbody>
					<tr>
						<td style="vertical-align:bottom;;">
							<span style="float: left;">';
							
								if ($page>1) $output .=' <input type="image" src="../img/admin/list-prev2.gif" onclick="$(\'#page\').val(0)">';
								if ($page>0) $output .='&nbsp; <input type="image" src="../img/admin/list-prev.gif" onclick="$(\'#page\').val('.($page-1).')">';
								$output .=' '.$obj->l('Page', 'stocks').' <b>'.($page+1).'</b> / '.$pageTotal.' ';
								if ($page<($pageTotal-1)) $output .=' <input type="image" src="../img/admin/list-next.gif" onclick="$(\'#page\').val('.($page+1).')">';
								if ($page<($pageTotal-2)) $output .=' &nbsp;<input type="image" src="../img/admin/list-next2.gif" onclick="$(\'#page\').val('.($pageTotal-1).')">';
								$output .=' | '.$obj->l('Show', 'stocks').'
								<select style="display:inline-block;width:60px" name="pagination" onchange="$(\'#submitFilterButton\').focus();$(\'#submitFilterButton\').click();">';
								foreach($paginationOpts as $paginationOpt) {
									$output .='<option value="'.$paginationOpt.'"';
									if ($paginationOpt == $pagCnt) $output .=' selected';
									$output .='>'.$paginationOpt.'</option>';
								}
								$output .='</select>
								/ '.count($sortedStockList).' '.$obj->l('result(s)', 'stocks').'
							</span>
							<span style="float: right;">
								<input type="submit" name="submitReset" value="'.$obj->l('Reset', 'stocks').'" class="button">
								<input type="submit" id="submitFilterButton" name="submitFilter" value="'.$obj->l('Filter', 'stocks').'" class="button">
							</span>
							<span class="clear"></span>
						</td>
					</tr>
					<tr>
						<td>';
			}*/
			$output .=' 
				<table class="table" cellpadding="0" cellspacing="0" width="100%" style="Table-Layout:fixed;border-radius:0px;border-width:1px;">
					<thead>
						<tr class="nodrag nodrop" '.($print ? ' style="background-color:#C2D5FC;color:#000000;"' : '').'>
							'.(!$print ? '
							<th style="width:14px">
								<input type="checkbox" name="checkme" class="noborder" onclick="checkDelBoxes(this.form, \'stockBox[]\', this.checked)">
							</th>
							' : '').'
							<th width="55px">'.$obj->l('ID', 'stocks').(!$print ? '
								'.MyOwnReservationsUtils::getOrderLink($productFamillyFilter, 'id') : '').'
							</th>
							<th width="8%">'.$obj->l('Serial number', 'stocks').(!$print ? '
								'.MyOwnReservationsUtils::getOrderLink($productFamillyFilter, 'serial') : '').'</th>
							<th width="8%">'.$obj->l('Name', 'stocks').(!$print ? '
								'.MyOwnReservationsUtils::getOrderLink($productFamillyFilter, 'name') : '').'</th>
							<th width="16%">'.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRODUCT]).(!$print ? '
								'.MyOwnReservationsUtils::getOrderLink($productFamillyFilter, 'product') : '').'</th>
							<th width="16%">'.$obj->l('Product attribute', 'stocks').(!$print ? '
								'.MyOwnReservationsUtils::getOrderLink($productFamillyFilter, 'attribute') : '').'</th>
							<th width="16%">'.$obj->l('On reservation', 'stocks').'</th>
							<th width="10%">'.$obj->l('Customer', 'stocks').'</th>
							<th width="8%">'.$obj->l('Carrier', 'stocks').'</th>
							<th width="8%">'.$obj->l('Advance', 'stocks').'</th>
							<th width="8%">'.$obj->l('Payment', 'stocks').'</th>
						</tr>
					';
				if (!$print) {
					$output .=' 
						
						<tr class="nodrag nodrop filter row_hover" style="height: 35px;">
							'.(!$print ? '<th class="center"></th>' : '').'
							<th><input type="text" name="filter_id_stock" value="'.$idFilter.'" style="width: 90%;" onkeypress="formSubmit(event, \'submitFilterButton\');"></th>
							<th><input type="text" name="filter_serial" value="'.$serialFilter.'" style="width:90%;" onkeypress="formSubmit(event, \'submitFilterButton\');"></th>
							<th><input type="text" name="filter_name" value="'.$nameFilter.'" style="width:90%;" onkeypress="formSubmit(event, \'submitFilterButton\');"></th>
							<th><select style="width:90%" name="filter_id_product" onchange="$(\'#submitFilterButton\').focus();$(\'#submitFilterButton\').click();">';
							$output .= '<option value="">--</option>';
							foreach($stockproduct AS $key => $value) {
								$output .= '<option value="' . $key. '" ';
								if ($productFilter==$key)
									$output .= ' selected';
								$output .= '>' . $value . '</option>';
							}
							$output .= '
							</select>
							</th>
							<th><select style="width:90%" name="filter_id_product_attribute" onchange="$(\'#submitFilterButton\').focus();$(\'#submitFilterButton\').click();">';
							$output .= '<option value="">--</option>';
							foreach($stockproductattribute AS $key => $value) {
								$output .= '<option value="' . $key. '" ';
								if ($productAttributeFilter==$key)
									$output .= ' selected';
								$output .= '>' . $value . '</option>';
							}
							$output .= '
							</select>
							</th>
							<th align="left">
								<select style="width:45%" name="filter_reservation" onchange="$(\'#submitFilterButton\').focus();$(\'#submitFilterButton\').click();">';
								$output .= '<option value="">--</option>';
								foreach($reservationFilterLabels AS $key => $label) {
									$output .= '<option value="' . $key. '" ';
									if ($reservationFilter==$key)
										$output .= ' selected';
									$output .= '>' . $label . '</option>';
								}
								$output .= '
								</select>';
								if (count($timeslots))
								$output .= '
								<select style="width:45%" name="filter_timeslot" onchange="$(\'#submitFilterButton\').focus();$(\'#submitFilterButton\').click();">';
								$output .= '<option value="">--</option>';
								foreach($timeslots AS $timeslot) {
									$output .= '<option value="' . $timeslot->sqlId. '" ';
									if ($timeslotFilter==$timeslot->sqlId)
										$output .= ' selected';
									$output .= '>' . $timeslot->name . '</option>';
								}
								$output .= '
								</select>';
								$output .= '
							</th>
							<th class="center"><input type="text" name="filter_customer" value="'.$customerFilter.'" style="width:90%;" onkeypress="formSubmit(event, \'submitFilterButton\');"></th>
							<th class="center">
							<select style="width:90%" name="filter_carrier" onchange="$(\'#submitFilterButton\').focus();$(\'#submitFilterButton\').click();">';
							$output .= '<option value="">--</option>';
							$output .= '<option value="0" ';
								if ($carrierFilter==0 && $carrierFilter!='')
									$output .= ' selected';
								$output .= '>' . $obj->l('Other', 'stocks') . '</option>';
							foreach($carriers AS $carrier) {
								$output .= '<option value="' . $carrier['id_carrier']. '" ';
								if ($carrierFilter==$carrier['id_carrier'])
									$output .= ' selected';
								$output .= '>' . $carrier['name'] . '</option>';
							}
							$output .= '
							</select>
							</th>
							<th class="center"> -- </th>
							<th class="center"><select style="width:90%" name="filter_payment" onchange="$(\'#submitFilterButton\').focus();$(\'#submitFilterButton\').click();">';
							$output .= '<option value="">--</option>';
								$output .= '<option value="0" ';
								if ($paymentFilter==0 && $paymentFilter!='')
									$output .= ' selected';
								$output .= '>' . $obj->l('Other', 'stocks') . '</option>';
							foreach($payment_modules AS $payment) {
								$output .= '<option value="' . $payment->id. '" ';
								if ($paymentFilter==$payment->id)
									$output .= ' selected';
								$output .= '>' . $payment->displayName . '</option>';
							}
							$output .= '
							</select></th>
							<th class="center">'.(_PS_VERSION_ < '1.6.0.0' ? '' : '<span class="pull-right">
								<button type="submit" id="" name="submitFilter" class="btn btn-default" onclick="$(\'#stockform\').submit()">
									<i class="icon-search"></i> '.$obj->l('Filter', 'deliveries').'
								</button>
								'.($isfiltered ? '<input type="submit" name="submitReset" value="'.$obj->l('Reset', 'stocks').'" class="btn btn-warning">' : '').'</span>').'</th>
						</tr>
						
					';
				}
				$output .=' 
					</thead>
					<tbody>';
					
				if (count($filterStockList)>0)
					foreach ($filterStockList as $stock) {
						$link='\''.myOwnReservationsController::getConfUrl('stock', '&editStock='.$stock->sqlId).'\'';
						$odd=!$odd;
						$resaList = '';$customerList = ''; $carrierList = ''; $advanceList = ''; $paymentList = '';
						foreach($stock->_reservations as $reservation) {
							if ($resaList!='') $resaList .= '<br />';
							$resaList .= '<a class="pointer" onclick="showResaBox('.$reservation->sqlId.');showResaDetails('.$reservation->sqlId.',\'\');">'.str_ireplace("<br>", "<br />", $reservation->toString($obj, $cookie->id_lang, true)).'</a>';
							if ($customerList!='') $customerList .= '<br />';
							if ($carrierList!='') $carrierList .= '<br />';
							if ($paymentList!='') $paymentList .= '<br />';
							if ($advanceList!='') $advanceList .= '<br />';
							if ($reservation->id_order==0) {
								$customerList .= '<i>'.$reservation->_customerName.'</i>';
							} else {
								$customerList .= '<a href="?tab=AdminCustomers&id_customer='.$reservation->_customerId.'&viewcustomer&token='.Tools::getAdminToken('AdminCustomers'.intval(Tab::getIdFromClassName('AdminCustomers')).intval($cookie->id_employee)).'" target="_blank">'.$reservation->_customerName.'</a>';
							}
							$carrierName = $paymentName = $obj->l('Other', 'stocks');
							foreach ($carriers as $carrier) if ($carrier['id_carrier']==$reservation->_carrier) $carrierName=$carrier['name'];
							$carrierList .= $carrierName;
							$advanceList .= Tools::ps_round($reservation->advance,_PS_PRICE_DISPLAY_PRECISION_).' '.myOwnUtils::getCurrency();
							foreach ($payment_modules as $payment) 
								if ($payment->id==$reservation->_payment) $paymentName=$payment->displayName;
							$paymentList .= $paymentName;
						}
						//
						$style="";
						if (count($stock->_reservations)>0) $style="background-color:#ddd;border-top:1px solid white;border-bottom:1px solid white";
						$output .= '
						<tr style="'.(count($stock->_reservations)>0 ? '' : '').'" class="'.($odd==0 ? ' alt_row odd' : '').'">
							'.(!$print ? '<td style="'.$style.'" class="center">'.($stock->sqlId> 0 ? '<input type="checkbox" name="stockBox[]" value="'.$stock->sqlId.'" class="noborder">': '').'</td>':'').'
							<td style="'.$style.'" width="2%" '.($stock->sqlId> 0 ? 'class="pointer" onclick="document.location='.$link.'"' : '').'>'.($stock->sqlId> 0 ? $stock->sqlId : '').'</td>
							<td style="'.$style.'" width="8%" '.($stock->sqlId> 0 ? 'class="pointer" onclick="document.location='.$link.'"' : '').'>'.$stock->serial.'</td>
							<td style="'.$style.'" width="8%" '.($stock->sqlId> 0 ? 'class="pointer" onclick="document.location='.$link.'"' : '').'>'.$stock->name.'</td>
							<td style="'.$style.'" width="16%" '.($stock->sqlId> 0 ? 'class="pointer" onclick="document.location='.$link.'"' : '').'>'.MyOwnReservationsUtils::getProductName($stock->id_product, $cookie->id_lang).'</td>
							<td style="'.$style.'" width="16%" '.($stock->sqlId> 0 ? 'class="pointer" onclick="document.location='.$link.'"' : '').'>'.MyOwnReservationsUtils::getAttributeCombinaisons($stock->id_product, $stock->id_product_attribute, $cookie->id_lang).'</td>
							<td style="'.$style.'" width="16%" >'.$resaList.'</td>
							<td style="'.$style.'" width="10%">'.$customerList.'</td>
							<td style="'.$style.'" width="8%">'.$carrierList.'</td>
							<td style="'.$style.'" width="8%">'.$advanceList.'</td>
							<td style="'.$style.'" width="8%">'.$paymentList.'</td>
							<td></td>';
							$output .= '
						</tr>';
					} else {
						if (_PS_VERSION_ < "1.6.0.0") $output .= '
					<tr>
						<td colspan="12">'.ucfirst(myOwnLang::$operations[MYOWN_OPE::NORES]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::STOCKITEM]).'</td>
					</tr>';
					else $output .= '
					<tr>
						<td class="list-empty" colspan="12">
							<div class="list-empty-msg">
								<i class="icon-warning-sign list-empty-icon"></i>
								'.ucfirst(myOwnLang::$operations[MYOWN_OPE::NORES]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::STOCKITEM]).'
							</div>
						</td>
					</tr>';
					}
					$output .= '
					</tbody>
					</table>';
		/*if (!$print) {
					$output .= '
						</div>
						</td>
					</tr>
				</tbody>
				</table>';*/
				
		$actions = array('$(\'#PrintSelection\').toggle();' => '<i class="icon-print"></i>&nbsp;'.ucfirst(myOwnLang::$actions[MYOWN_OPE::PPRINT]));
		$actions2 = array('if (confirm(\''.$obj->l('Delete selected items ?', 'stocks').'\'));sendBulkAction($(this).closest(\'form\').get(0), \'submitBulkRemoveSelection\');' => '<i class="icon-trash"></i>&nbsp;'.ucfirst(myOwnLang::$actions[MYOWN_OPE::DELETE]));
		/*if (MyOwnDeliveriesCarriers::$multiple) 
			$actions2 = array('submitBulkChangeOrderStatusSelection' => '<i class="icon-refresh"></i>&nbsp;'.$obj->l('Change Order Status', 'deliveries'), 'submitBulkExportSelection' => '<i class="icon-cloud-download"></i>&nbsp;'.$obj->l('Export data', 'deliveries'));*/
		$output .= MyOwnReservationsUtils::displayRow($obj, $actions, 'stockBox', $actions2, $page, $pageTotal, count($sortedStockList));	//'stockform', 	
			
		$output .= '	
				</form>
				';
		}
		return $output;
	}


	
	//-----------------------------------------------------------------------------------------------------------------
	//Edit stock
	//-----------------------------------------------------------------------------------------------------------------
	public static function displayEdit($cookie, $obj, $id, $dayStr, $productFamillyFilter) {
		global $rights;
		//if (_PS_VERSION_ < "1.5.0.0" )	{
		//include_once(MYOWNRES_PATH.'pro/pdf-barcode.php');
		include_once(MYOWNRES_PATH.'pro/php-barcode.php');
		//	}
		$_products = $obj->_products;
		$link=new Link();
		$image='';
		$mainProduct=null;
		$stock=$obj->_stocks->list[$id];
		/*if (_PS_VERSION_ < "1.5.0.0" ) */$img = barcode_print($stock->serial,"ANY",3,false);
		//else $img=null;
		if ($stock->id_product>0) {
			$mainProduct = $mainProduct = $obj->_products->getResProductFromProduct($stock->id_product);
			$productsImages=MyOwnReservationsUtils::getProductsImages(array($stock->id_product), $cookie->id_lang);
			$imageTypes=ImageType::getImagesTypes();
			foreach($imageTypes as $imageType)
				if ($imageType['name']=='medium_default') $mediumSize=new ImageType($imageType['id_image_type']);
			$viewedProduct=$productsImages[$stock->id_product];
			$image = 'http://'.$link->getImageLink($viewedProduct->link_rewrite, $viewedProduct->cover, $mediumSize->name);
		} else {
			if (file_exists(_PS_IMG_DIR_.'places/'.intval(-$stock->id_product).'.png')) 
				$image = _PS_IMG_.'places/'.intval(-$stock->id_product).'.png';
		}
		if ($img!=null)  imagepng($img, _PS_TMP_IMG_DIR_.'myownr_stock_'.$stock->sqlId.'.png');

		$output = MyOwnReservationsUtils::displayIncludes();
		$output .= (_PS_VERSION_ < "1.5.0.0" ? '<script src="'._PS_JS_DIR_.'jquery/jquery.fancybox-1.3.4.js" type="text/javascript" charset="utf-8"></script><link rel="stylesheet" href="'._PS_CSS_DIR_.'jquery.fancybox-1.3.4.css" type="text/css" charset="utf-8">' :
		'<script src="'._PS_JS_DIR_.'jquery/plugins/fancybox/jquery.fancybox.js" type="text/javascript" charset="utf-8"></script><link rel="stylesheet" href="'._PS_JS_DIR_.'jquery/plugins/fancybox/jquery.fancybox.css" type="text/css" charset="utf-8">');
		$output .= myOwnReservationsReservationsController::getScript($obj);
		
		$output .= '
		<table style="float:left;width:45%;background-color:#FFFFFF" cellpadding="0" cellspacing="0" class="table">
			<thead>
				<tr>
					<th style="font-size:14px">'.$stock->serial.'</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>'.($image!='' ? '<img src="'.$image.'"/> <b>'.MyOwnReservationsUtils::getProductName($stock->id_product, $cookie->id_lang).'</b>'.($stock->id_product_attribute ? '<br>'.MyOwnReservationsUtils::getAttributeCombinaisons($stock->id_product, $stock->id_product_attribute, $cookie->id_lang) : '') : '').'</td>
					<td align="right">'.($img!=null ? '<img src="'._PS_TMP_IMG_.'myownr_stock_'.$stock->sqlId.'.png" height="60px"/>' : '').'</td>
				</tr>
			</tbody>
		</table>';
		
		$output .= '
		<form id="displayEditStock" action="'.myOwnReservationsController::getConfUrl((Tools::getIsset('editplace') ? 'productPlaces&editplace='.Tools::getValue('editplace') : 'stock').'stock&day='.$dayStr.'&products='.$productFamillyFilter.'&orderby=name&orderway=desc').'" method="post">
		<input type="hidden" name="id_stock" value="'.$id.'" />';
		
		if (_PS_VERSION_ < "1.6.0.0") $output .= '
			<fieldset style="float:right;width:45%;margin-bottom:10px">
				<legend><img src="../img/t/AdminCMS.gif" />'.$obj->l('Details', 'stocks').'</legend>';
		else $output .= '
			<div class="panel" style="float:right;width:45%;margin-bottom:10px">
				<h3><i class="icon-tags"></i> '.$obj->l('Details', 'stocks').'</h3>';
				
		$output .= '
					<ol style="padding-left:0px;">
					';
					if ($rights['edit']==1) {
					$output .= '
					<li class="myowndlist">
						<label for="name" class="shipLabel" style="width:130px">'.$obj->l('Name', 'stocks').' :&nbsp;</label>
						<input style="width:25%" type="text" name="stockName" value="'.$stock->name.'" />
					</li>
					<li class="myowndlist">
						<label for="type" class="shipLabel" style="width:130px">'.$obj->l('Comment', 'stocks').' :&nbsp;</label>
						<textarea style="width:300px" name="stockComment" cols="50" rows="3">'.$stock->comment.'</textarea> 
					</li>
					';
					} else {
						$output .= '
					<li class="myowndlist">
						<label for="name" class="shipLabel" style="width:130px">'.$obj->l('Name', 'stocks').' :&nbsp;</label>
						'.$stock->name.'&nbsp;
					</li>
					<li class="myowndlist">
						<label for="type" class="shipLabel" style="width:130px">'.$obj->l('Comment', 'stocks').' :&nbsp;</label>
						'.$stock->comment.'
					</li>';
					}
					$output .= '
				</ol>
			'.(_PS_VERSION_ < "1.6.0.0" ? '</fieldset>' : '</div>').'
		</form><br/>
		<div style="clear:both"></div>';

		$_resasList = myOwnResas::getReservationsForSerial($stock->serial, $mainProduct);

			
		$output .= '
		<table style="width:100%;background-color:#FFFFFF" cellpadding="0" cellspacing="0" class="table">
			<thead>
			<tr>
				<th width="10px">ID</th>
				<th width="22px">'.ucfirst(myOwnLang::$object[MYOWN_OBJ::RESV]).'</th>
			</tr>
			</thead>
			<tbody>';
		if (count($_resasList))
			foreach($_resasList as $resa) $output .= '
			<tr class="row_hover">
				<td class="pointer" onclick="showResaBox('.$resa->sqlId.');showResaDetails('.$resa->sqlId.',\'\');">'.$resa->sqlId.'</td>
				<td class="pointer" onclick="showResaBox('.$resa->sqlId.');showResaDetails('.$resa->sqlId.',\'\');">'.$resa->toString($obj, $cookie->id_lang, false).'</td>
			</tr>';
		else {
			if (_PS_VERSION_ < "1.6.0.0") $output .= '
			<tr><td colspan="2">'.$obj->l('No reservation assigned', 'stocks').'</td></tr>';
			else $output .= '
				<tr>
					<td class="list-empty" colspan="9">
						<div class="list-empty-msg">
							<i class="icon-warning-sign list-empty-icon"></i>
							'.$obj->l('No reservation assigned', 'stocks').'
						</div>
					</td>
				</tr>';
		}
		$output .= '
		</tbody></table>';

		
		$output .= '</div>
			<div class="clear"></div>';
			
		return $output;
	}
	
	/*---------------------------------------------------------------------------------------------------------------
	bulk Actions Display
	-----------------------------------------------------------------------------------------------------------------*/
	public static function bulkActionsDisplay($obj, $day, $cookie)
	{
		$output = '
		<script type="text/javascript">
		function submitBulkPrintSelection() {
			$("#PrintSelection").show();
		}
		function getBarCodes() {
	    	var barcodes="";
	    	$(\'input[name="stockBox[]"]\').each(function( index ) {
				if ($(this).attr("checked")) barcodes += $(this).val()+";";
			});
			if (barcodes=="" && $("#printFormat").val()!="") {
				alert("'.$obj->l('Please select stock items', 'stocks').'");
				return false;
			}
			$("#exportBarCodesList").val(barcodes);
			return true;
	    }
		</script>
		
		<form name="globalactions" id="printSelectionFrom" action="ajax-tab.php?tab=admin'.$obj->name.'&token='.Tools::getAdminToken('admin'.$obj->name.intval(Tab::getIdFromClassName('admin'.$obj->name)).intval($cookie->id_employee)).'&action=printStock" method="post" target="_blank">
		
		<div class="panel" id="PrintSelection" style="display:none">
			<div class="panel-heading">
				'.$obj->l('Choose a print format', 'stocks').'
			</div>
		';
		$label=$obj->l('Labels', 'stocks');
		$timeselStr=date("Y-m-d", $day);
		
		if (_PS_VERSION_ < "1.6.0.0") {
		$printFormats=array(
			''=>$obj->l('Table', 'stocks'),
			'5160'=>'Avery 5160 (3x10 '.$label.')',
			'5161'=>'Avery 5161 (2x10 '.$label.')',
			'5162'=>'Avery 5162 (2x7 '.$label.')',
			'5163'=>'Avery 5163 (2x5 '.$label.')',
			'8600'=>'Avery 8600 (3x10 '.$label.')',
			'L7163'=>'Avery L7163 (2x7 '.$label.')',
			'3422'=>'Avery 3422 (3x8 '.$label.')'
		);
		} else if (file_exists(MYOWNRES_PATH.'pro/labels.xml')) {
			$labelXml = simplexml_load_file(MYOWNRES_PATH.'pro/labels.xml');
			foreach ($labelXml->label as $labelName=>$label)
				$printFormats[intval($label['id'])] =  strval($label->name);
		}
		
		$output .= $obj->l('Format', 'stocks').' :
		<input type="hidden" name="date" value="'.$timeselStr.'">
		<input type="hidden" name="barCodesList" id="exportBarCodesList" value="">
		<select id="printFormat" name="printFormat"  style="width:100%">';
		foreach($printFormats as $key => $printFormat) {
			$output .='
			<option value="'.$key.'">'.$printFormat.'</option>';
		}
		$output .='
		</select>
						
			<div class="panel-footer">
					<button type="submit" name="cancel" class="btn btn-default" onclick="$(\'#PrintSelection\').hide();">
						<i class="icon-remove"></i>
						'.ucfirst(myOwnLang::$actions[MYOWN_OPE::CANCEL]).'
					</button>
					<button type="button" class="btn btn-default" name="submitBulkPrintSelection" onclick="getBarCodes();$(\'#printSelectionFrom\').submit();">
						<i class="icon-print"></i>
						'.ucfirst(myOwnLang::$actions[MYOWN_OPE::PPRINT]).'
					</button>
			</div>
		</div>
		
		</form>
		';
		return $output;
	}
	
	//-----------------------------------------------------------------------------------------------------------------
	//Execute actions
	//-----------------------------------------------------------------------------------------------------------------
	public static function executeActions($obj) {
     	global $cookie;

     	$operationLabel="";
     	$htmlRes="";
     	$plur=false;
     	$result = true;
     	$operationObject=myOwnLang::$object[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::RESV]);

     	if (isset($_POST["submitAddStock"])) {
     		$id_shop = myOwnUtils::getShop();
     		if (intval($_POST["stock_id_product"])>0) {
	     		$serials=explode("\r",$_POST["serials"]);
	     		$serialcount=0;
	     		//$stockitems = $_stocks->getStockForProduct($_POST["id_product"], $_POST["id_product_attribute"]);
	 			foreach($serials as $key => $serial) {
	 				if (strlen(trim($serial))>0) {
	 					if (strlen(trim($serial)) < 13 ) {
	 						$serialExist=false;
	 						if (count($obj->_stocks->list)>0)
	 						foreach($obj->_stocks->list as $stockitem)
	 							if (trim($serial)==trim($stockitem->serial)) $serialExist=true;
	 						if (!$serialExist) {
			 					$serialcount++;
			     				$tempStock = new myOwnStock();
			     				$tempStock->id_shop = $id_shop;
			     				$tempStock->id_product = $_POST["stock_id_product"];
			     				if (isset($_POST["stock_id_product_attribute"])) $tempStock->id_product_attribute = $_POST["stock_id_product_attribute"];
			     				else $tempStock->id_product_attribute = 0;
			     				$tempStock->serial = strtoupper(trim($serial));
			     				$tempStock->sqlInsert();
			     				if (!preg_match( "@^\d*$@i" , trim($serial))!= 0 && trim($serial) < 1000000000000 ) 
			     					$htmlRes .= myOwnUtils::displayWarn($obj->l('Warning on line', 'stocks').' '.($key+1).' '.$obj->l('Serial number with letters cannot be printed as barcode', 'stocks'));
		     				} else {
		     					$htmlRes .= myOwnUtils::displayError($obj->l('Error on line', 'stocks').' '.($key+1).' '.$obj->l('Serial number', 'stocks').' '.$serial.' '.$obj->l('already exists', 'stocks'));
		     				}
	     				} else {
	     					$htmlRes .= myOwnUtils::displayError($obj->l('Error on line', 'stocks').' '.($key+1).' '.$obj->l('Serial number must have a length of 12 characters maximum', 'stocks'));
	     				}
	 				}
	 			}
		     	if ($serialcount>0) {
		     		$htmlRes .= myOwnUtils::displayConf($serialcount.' '.$obj->l('serials numbers added for product', 'stocks'));
		     		$obj->_stocks = new myOwnStocks();
	     		} else {
	     			$htmlRes .= myOwnUtils::displayError($obj->l('No serial added to product stock', 'stocks'));
	     		}
     		} else {
     			$htmlRes .= myOwnUtils::displayError($obj->l('Please select a product', 'stocks'));
     		}
     	}
     	
     	if (Tools::getIsSet('submitBulkRemoveSelection') && Tools::getIsSet('stockBox'))
		{
			$stocks = Tools::getValue('stockBox');
			if (is_array($stocks))
				foreach ($stocks as $stock)
				{

					$operationLabel=myOwnLang::getOperation($obj, MYOWN_OPE::DELETE);
					$result = $result && myOwnStocks::deleteStock($stock);
					
					
				}
			$operationObject = ucfirst(myOwnLang::$object[MYOWN_OBJ::STOCKITEM]);
			if (count($stocks)>1) $operationObject = ucfirst(myOwnLang::$object[MYOWN_OBJ::STOCKITEM]);
			if ($result) $htmlRes .= myOwnUtils::displayConf($operationObject.' '.myOwnLang::getResult($obj, true, count($stocks)==1).' '.$operationLabel);
			else $htmlRes .= myOwnUtils::displayError($operationObject.' '.myOwnLang::getResult($obj, false, count($stocks)==1).' '.$operationLabel.' : '.Db::getInstance()->getMsgError());
					
			$obj->_stocks=new myOwnStocks();
			$operationLabel = '';
		}
     	

     	$action = Tools::getValue("action");
		if ($action=='deleteStocks') {
			$deleteCnt=0;
			$result=true;
			$operationObject=ucfirst(myOwnLang::$objects[MYOWN_OBJ::STOCKITEM]);
			$operationLabel=myOwnLang::getOperation($obj, MYOWN_OPE::DELETE);
			if (isset($_POST["stockBox"]))
			foreach($_POST["stockBox"] as $stock) {
				$deleteCnt++;
				$result = $result && myOwnStocks::deleteStock($stock);
			}
			$obj->_stocks=new myOwnStocks();
		}
		if ($action=='deleteStock' && intval($_GET["deleteStock"])>0) {
			$operationObject=ucfirst(myOwnLang::$object[MYOWN_OBJ::STOCKITEM]);
			$operationLabel=myOwnLang::getOperation($obj, MYOWN_OPE::DELETE);
			$result = myOwnStocks::deleteStock(intval($_GET["deleteStock"]));
			$obj->_stocks=new myOwnStocks();
		}
		if (isset($_POST["id_stock"]) && $_POST["id_stock"]!="" && intval($_POST["id_stock"])>0) {
			$operationObject=ucfirst(myOwnLang::$object[MYOWN_OBJ::STOCKITEM]);
			$operationLabel=myOwnLang::getOperation($obj, MYOWN_OPE::UPDATE);
			$stockitem = $obj->_stocks->list[$_POST["id_stock"]];
			$stockitem->name=$_POST["stockName"];
			$stockitem->comment=$_POST["stockComment"];
			$result = $stockitem->sqlUpdate();
		}

		if ($operationLabel!="") {
			if ($result) $htmlRes .= myOwnUtils::displayConf($operationObject.' '.myOwnLang::getResult($obj, true, !$plur).' '.$operationLabel);
			else $htmlRes .= myOwnUtils::displayError($operationObject.' '.myOwnLang::getResult($obj, false, !$plur).' '.$operationLabel.' : '.Db::getInstance()->getMsgError());
		}
		return $htmlRes;
	}
  
}
?>