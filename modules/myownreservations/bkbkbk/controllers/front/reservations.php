<?php
/**
* 2010-2015 LaBulle.
*
* this front ctrl manage tracking page
*
* NOTICE OF LICENSE
*
* This source file is subject to Terms and Conditions of Use 
* Available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade module to newer
* versions in the future. If you wish to customize module for your
* needs please refer to http://www.labulle.net for more information.
*
* @author    laBulle.net <info@labulle.net>
* @copyright 2010-2014 laBulle
* @license   http://www.labulle.net/download/CGV-en.pdf
*/

class myownreservationsreservationsModuleFrontController extends ModuleFrontController
{
    public $ssl = true;
    public $php_self = 'module-myownreservations-reservations';
/* http://192.168.0.39/mycollectionplaces/1-6-0?fc=module&module=mycollectionplaces&controller=shiptrack */
    public function init()
    {
        parent::init();
        $this->display_column_left = false;
        $this->obj = new myOwnReservations();
        $obj = $this->obj;
        
        if (_PS_VERSION_ >= '1.5.0.0') {
            $obj->getContext()->controller->addCSS(_MODULE_DIR_.$obj->name.'/views/css/front.css', 'screen');
            $obj->getContext()->controller->addCSS(_MODULE_DIR_.$obj->name.'/views/css/back.css', 'screen');
            $obj->getContext()->controller->addCSS(_MODULE_DIR_.$obj->name.'/views/css/planning.css', 'screen');
            $obj->getContext()->controller->addJquery();
            $obj->getContext()->controller->addJS(_MODULE_DIR_.$obj->name.'/views/js/jquery.tooltipster.min.js');
            $obj->getContext()->controller->addJS(_THEME_JS_DIR_.'history.js');
        } else {
//             Tools::addCSS(_MODULE_DIR_.$obj->name.'/views/css/front.css', 'screen');
//             Tools::addCSS(_MODULE_DIR_.$obj->name.'/views/css/planning.css', 'screen');
//             Tools::addJS(_MODULE_DIR_.$obj->name.'/views/js/mycollectionplaces.js');
        }
    }
    
    public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();

        $breadcrumb['links'][] = $this->addMyAccountToBreadcrumb();

        return $breadcrumb;
    }
    
    public function setMedia()
	{
		parent::setMedia();
		$this->addJquery();
	    $this->addJqueryUI('ui.slider');
	    $this->addJqueryUI('ui.datepicker');
	    $this->addJqueryPlugin(array('fancybox'));
	    $this->addJs(_MODULE_DIR_.$this->obj->name.'/views/js/jquery.tooltipster.min.js', 'all');
	}

    public function initContent()
    {
        parent::initContent();
        $cookie = $this->context->cookie;
        $link = new Link();
        if (empty($link->protocol_content))
			$link->protocol_content = Tools::getCurrentUrlProtocolPrefix();
        $obj = Module::getInstanceByName('myownreservations');
        $front='';
        $exttitle='';
		//ext
		//-----------------------------------------------------------------------------------------------------------------
    	foreach ($this->obj->_extensions->getExtByType(extension_type::FRONT) as $ext) 
			if (method_exists($ext, "displayFrontPage")) {
				$tempfront =$ext->displayFrontPage($this->obj, array($this));
				if ($tempfront!="") $exttitle = $ext->title;
				$front .= $tempfront;
			}

        $admin = Tools::getValue('admin');
        if ($admin=='reservations') {
        	$front.='<link href="'._MODULE_DIR_.$this->obj->name.'/views/css/orderConf.css" rel="stylesheet" type="text/css" media="all" />';
    		$front.=myOwnReservationsReservationsController::display($obj);
//     		$out .= MyOwnReservationsUtils::displayIncludes(true);
    	}
    	
    	$this->context->smarty->assign(array(
    		'myownlink' => $link,
			'adminpage' => $admin,
			'front' => $front,
			'exttitle' => $exttitle,
			'base_dir' => _PS_BASE_URL_.__PS_BASE_URI__
		));
		if (_PS_VERSION_ >= "1.7.0.0")
			$this->setTemplate('module:myownreservations/views/templates/front/account/reservations_form.tpl');
		else  $this->setTemplate('account/reservations.tpl');
    }
}
