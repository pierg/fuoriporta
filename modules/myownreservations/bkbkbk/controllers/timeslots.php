<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnReservationsTimeslotsController {
	
	public static function check($obj) {
		$start_conflict=array();
		$end_conflict=array();
		$contain_conflict=array();
		$errors=array();
		$langue=$obj->getContext()->cookie->id_lang;
		
		if (!isset($_POST['timeSlotName']) || $_POST['timeSlotName']=='') return false;
		
		if (Tools::getIsset('timeSlotName') AND (strlen(Tools::getValue('timeSlotName'))>16 or strlen(Tools::getValue('timeSlotName'))==0)) {
			$errors[] = $obj->l('A time slot name cannot be empty and must be smaller than 16 chars', 'timeslots');
		}
		if (Tools::getIsset('startTimeHour')) {
            $start_time = MyOwnCalendarTool::getHourPost('startTime');
            $end_time = MyOwnCalendarTool::getHourPost('endTime');
            $days = MyOwnCalendarTool::getDaysString($langue);
            //test date de début comprise dans un créneau 
            $id_time_slot = Tools::getValue('idTimeSlot', 0);
            $id_family = Tools::getValue('timeSlotFamily');
            $id_object = Tools::getValue('object', 0);
            $id_product = Tools::getValue('timeSlotProduct', 0);
            $week_type = Tools::getValue('timeSlotWeekType', 0);
            $mainProduct = null;
            if (array_key_exists($id_family, $obj->_products->list))
            	$mainProduct = $obj->_products->list[$id_family];
            if ($mainProduct!=null && $mainProduct->productType!=product_type::AVAILABLE)
            foreach ($days as $dayKey => $dayStr) {
                if (count($obj->_timeSlots->list) > 0) {
                    foreach ($obj->_timeSlots->list as $time_slot) {
                        if (Tools::getValue('day'.$dayKey, 0) && $time_slot->days[$dayKey] && $time_slot->sqlId != $id_time_slot
                        	 && ($time_slot->id_family==$id_family or $time_slot->id_family==0 or $id_family==0)
                        	 && ($time_slot->id_product==$id_product or $time_slot->id_product==0 or $id_product==0)
                        	 && ($time_slot->week_type==$week_type or $time_slot->week_type==0 or $week_type==0)
                        	 && ($time_slot->id_object==$id_object or $time_slot->id_object==0 or $id_object==0)) {
                            if (strtotime($start_time) >= strtotime($time_slot->startTime) && strtotime($start_time) < strtotime($time_slot->endTime)) {
                            	if (array_key_exists($time_slot->sqlId, $start_conflict))
                            		$start_conflict[$time_slot->sqlId][]=$dayKey;
                            	else $start_conflict[$time_slot->sqlId] = array($dayKey);
//                                 $errors[] = $obj->l('The start time', 'timeslots').' ('.MyOwnCalendar::formatTime($start_time, $langue).') '.$obj->l('of the time slot cannot be included between the start time (included) and the end time (excluded)  of time slot', 'timeslots').' '.$time_slot->sqlId.' ('.MyOwnCalendar::formatTime($time_slot->startTime, $langue).' - '.MyOwnCalendar::formatTime($time_slot->endTime, $langue).') '.$obj->l('on').' '.$dayStr;
                                break;
                            }
                        	//   test que le creneau n'englobe pas un autre 
                        	if ($time_slot->endTime == '00:00:00') {
                            	$tsend = '23:59:00';
                        	} else {
                            	$tsend = $time_slot->endTime;
                        	}
                            if ($end_time == '00:00:00') {
                                $end = '23:59:00';
                            } else {
                                $end = $end_time;
                            }
                            if (strtotime($start_time) < strtotime($time_slot->startTime) && strtotime($end) > strtotime($tsend)) {
                                if (array_key_exists($time_slot->sqlId, $contain_conflict))
                            		$contain_conflict[$time_slot->sqlId][]=$dayKey;
                            	else $contain_conflict[$time_slot->sqlId] = array($dayKey);
//                             	$errors[] = $obj->l('The time slot cannot contain the time slot', 'timeslots').' '.$time_slot->sqlId.' ('.MyOwnCalendar::formatTime($time_slot->startTime, $langue).' - '.MyOwnCalendar::formatTime($time_slot->endTime, $langue).') '.$obj->l('on').' '.$dayStr;
                                break;
                            }
                        }
                    }
                }
            }
        }

        if (Tools::getIsset('endTimeHour')) {
            $end_time = MyOwnCalendarTool::getHourPost('endTime');
            //   test date de fin comprise dans un créneau   
            $id_time_slot = Tools::getValue('idTimeSlot', 0);
            $id_family = Tools::getValue('timeSlotFamily');
            $id_object = Tools::getValue('object', 0);
            $id_product = Tools::getValue('timeSlotProduct', 0);
            $week_type = Tools::getValue('timeSlotWeekType', 0);
            $mainProduct = null;
            if (array_key_exists($id_family, $obj->_products->list))
            	$mainProduct = $obj->_products->list[$id_family];
            if ($mainProduct!=null && $mainProduct->productType!=product_type::AVAILABLE)
            foreach ($days as $dayKey => $dayStr) {
                if (count($obj->_timeSlots->list) > 0) {
                    foreach ($obj->_timeSlots->list as $time_slot) {
                        if (Tools::getValue('day'.$dayKey, 0) && $time_slot->days[$dayKey] && $time_slot->sqlId != $id_time_slot 
                        	&& ($time_slot->id_family==$id_family or $time_slot->id_family==0 or $id_family==0)
                        	&& ($time_slot->id_product==$id_product or $time_slot->id_product==0 or $id_product==0)
                        	&& ($time_slot->week_type==$week_type or $time_slot->week_type==0 or $week_type==0)
                        	&& ($time_slot->id_object==$id_object or $time_slot->id_object==0 or $id_object==0)) {
                            if (strtotime($end_time) > strtotime($time_slot->startTime) && strtotime($end_time) <= strtotime($time_slot->endTime)) {
                            	if (array_key_exists($time_slot->sqlId, $end_conflict))
                            		$end_conflict[$time_slot->sqlId][]=$dayKey;
                            	else $end_conflict[$time_slot->sqlId] = array($dayKey);
//                                 $errors[] = $obj->l('The end time', 'timeslots').' ('.MyOwnCalendar::formatTime($end_time, $langue).') '.$obj->l('of the time slot cannot be included between the start time (excluded) and the end time (included)  of time slot', 'timeslots').' '.$time_slot->sqlId.' ('.MyOwnCalendar::formatTime($time_slot->startTime, $langue).' - '.MyOwnCalendar::formatTime($time_slot->endTime, $langue).') '.$obj->l('on').' '.$dayStr;
                                break;
                            }
                        }
                    }
                }
            }
            $start_time = MyOwnCalendar::getHourPost('startTime');
            if ($start_time == $end_time) {
                $errors[] = $obj->l('The end time', 'timeslots').' ('.MyOwnCalendar::formatTime($end_time, $langue).') '.$obj->l('of the time slot cannot be equal than the start time', 'timeslots').' ('.MyOwnCalendar::formatTime($start_time, $langue).')';
            }

            if (strtotime($end_time) < strtotime($start_time) && $end_time != '00:00:00') {
                $errors[] = $obj->l('The end time', 'timeslots').' ('.MyOwnCalendar::formatTime($end_time, $langue).') '.$obj->l('of the time slot cannot be earlier than the start time', 'timeslots').' ('.MyOwnCalendar::formatTime($start_time, $langue).')';
            }
        }
        $dayStr = MyOwnCalendar::getDaysString($langue);
        foreach ($start_conflict as $start_conflict_ts => $days) {
        	$day_list_str='';
        	$time_slot = $obj->_timeSlots->list[$start_conflict_ts];
        	foreach ($days as $day) {
        		if ($day_list_str!='') $day_list_str .= ', ';
        		$day_list_str .= $dayStr[$day];
        	}
        	$errors[] = $obj->l('The start time', 'timeslots').' ('.MyOwnCalendar::formatTime($start_time, $langue).') '.$obj->l('of the time slot cannot be included between the start time (included) and the end time (excluded)  of time slot', 'timeslots').' '.$time_slot->sqlId.' ('.MyOwnCalendar::formatTime($time_slot->startTime, $langue).' - '.MyOwnCalendar::formatTime($time_slot->endTime, $langue).') '.$obj->l('on').' '.$day_list_str;
        	
        }
        foreach ($end_conflict as $end_conflict_ts => $days) {
        	$day_list_str='';
        	$time_slot = $obj->_timeSlots->list[$end_conflict_ts];
        	foreach ($days as $day) {
        		if ($day_list_str!='') $day_list_str .= ', ';
        		$day_list_str .= $dayStr[$day];
        	}
        	$errors[] = $obj->l('The end time', 'timeslots').' ('.MyOwnCalendar::formatTime($end_time, $langue).') '.$obj->l('of the time slot cannot be included between the start time (excluded) and the end time (included)  of time slot', 'timeslots').' '.$time_slot->sqlId.' ('.MyOwnCalendar::formatTime($time_slot->startTime, $langue).' - '.MyOwnCalendar::formatTime($time_slot->endTime, $langue).') '.$obj->l('on').' '.$day_list_str;
        }
        foreach ($contain_conflict as $contain_conflict_ts => $days) {
        	$day_list_str='';
        	$time_slot = $obj->_timeSlots->list[$contain_conflict_ts];
        	foreach ($days as $day) {
        		if ($day_list_str!='') $day_list_str .= ', ';
        		$day_list_str .= $dayStr[$day];
        	}
        	$errors[] = $obj->l('The end time', 'timeslots').' ('.MyOwnCalendar::formatTime($end_time, $langue).') '.$obj->l('of the time slot cannot be included between the start time (excluded) and the end time (included)  of time slot', 'timeslots').' '.$time_slot->sqlId.' ('.MyOwnCalendar::formatTime($time_slot->startTime, $langue).' - '.MyOwnCalendar::formatTime($time_slot->endTime, $langue).') '.$obj->l('on').' '.$day_list_str;
        }
        return $errors;
	}
	
	public static function editAction($obj, &$msg) {
		$result=false;
		//if(isset($_POST['idTimeSlot'])){
			$tempTimeSlot = new myOwnTimeSlot();
			$startTime = MyOwnCalendarTool::getHourPost('startTime');
			$endTime = MyOwnCalendarTool::getHourPost('endTime');
			
			$tempTimeSlot->startTime = $startTime;
			$tempTimeSlot->endTime = $endTime;
			$tempTimeSlot->name = $_POST['timeSlotName'];
			//$tempTimeSlot->quota = $_POST['timeslotQuota'];
			$tempTimeSlot->type = $_POST['timeSlotType'];
			$idTimeSlot=0;
			if (isset($_POST["idTimeSlot"])) $idTimeSlot = intval($_POST["idTimeSlot"]);
			$tempTimeSlot->sqlId = $idTimeSlot;
			$tempTimeSlot->id_family = Tools::getValue('timeSlotFamily', 0);
			$tempTimeSlot->id_product = Tools::getValue('timeSlotProduct', 0);
			$tempTimeSlot->id_object = Tools::getValue('object', 0);
			
			for($i=1; $i<8; $i++) {
				$tempTimeSlot->days[$i] =  intval(isset($_POST['day'.$i]) && $_POST['day'.$i]==1);
			}
			$tempTimeSlot->week_type = Tools::getValue('timeSlotWeekType', 0);
			
			$objet = myOwnLang::$object[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::TIMESLOT]);
			if ($tempTimeSlot->sqlId==0) {
				$operation = myOwnLang::getOperation($obj, MYOWN_OPE::CREATE);
				$result = $tempTimeSlot->sqlInsert();
				$inserted = Db::getInstance()->ExecuteS("SELECT LAST_INSERT_ID() AS ID;");
				$insertedId = $inserted[0]['ID'];
			} else {
				$operation = myOwnLang::getOperation($obj, MYOWN_OPE::UPDATE);
				$result = $tempTimeSlot->sqlUpdate();
			}
			$obj->_timeSlots = new myOwnTimeSlots();
			if ($result) $msg = $objet.' '.myOwnLang::getResult($obj, false, true).' '.$operation;
			else $msg = $objet.' '.myOwnLang::getResult($obj, false, true).' '.$operation.' : '.Db::getInstance()->getMsgError();
			return $result;
		//}
		//return false;
	}
	
	// DELETE TIME SLOT
	//-----------------------------------------------------------------------------------------------------------------
	public static function deleteAction($obj, &$msg) {
		$result = true;
		if (isset($_POST["deleteTimeslots"])) {
			foreach ($_POST["deleteTimeslots"] as $timeslotToDelete) {
				$tempTimeSlot = new myOwnTimeSlot();
				$tempTimeSlot->sqlId = $timeslotToDelete;
				$result &= $tempTimeSlot->sqlDelete();
			}
		}
		$operation = myOwnLang::getOperation($obj, MYOWN_OPE::DELETE, false);
		if (count($_POST["deleteTimeslots"])==1) $objet = myOwnLang::$object[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::TIMESLOT]);
		else $objet = myOwnLang::$objects[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$objects[MYOWN_OBJ::TIMESLOT]);
		if ($result) $msg = $objet.' '.myOwnLang::getResult($obj, false, count($_POST["deleteTimeslots"])==1).' '.$operation;
		else $msg = $objet.' '.myOwnLang::getResult($obj, false, true).' '.$operation.' : '.Db::getInstance()->getMsgError();
			
		return $result;
	}
	
	
	//==============================================================================================================================================
	//     TIME SLOT LIST
	//==============================================================================================================================================

	public static function show($from, $obj, $id_family=null, $id_product=null, $sel = array()) {
		global $cookie;
		$langue=$cookie->id_lang;
		$output='';
		$odd=false;
		$days = myOwnLang::getDaysNames($langue);
		$inProductRule=false;
		if (Tools::getValue('myOwnResTab')!='timeslots') $inProductRule=true;
		$isproversion=is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");
		
		$list=array();
		$isprodts=false;
		if ($id_family || $from!='object')
		foreach($obj->_timeSlots->list AS $key => $value) {
			if ((($value->id_family==0 && $id_family >=0) || $id_family==null || $value->id_family==$id_family)
			      && ( $id_product==null || $value->id_product==0 || $value->id_product==$id_product) )
				$list[$key]=$value;
			if ($value->id_product) $isprodts=true;
		}
		
		if ($id_family==null && _PS_VERSION_ < "1.5.0.0")
			$output .= '<div style="padding:20px 10px 0px 10px;">
			<fieldset class="settingsList">
				<legend>
					<img src="../img/admin/time.gif" />'.ucfirst(myOwnLang::$objects[MYOWN_OBJ::TIMESLOT]).'
				</legend>';
		if (_PS_VERSION_ < "1.5.0.0") 
			$output .= '
				<span class="myowndeliveriesSettingsPreTable">
					<a href="#" class="tooltip" title="'.myOwnHelp::$objects['timeslot']['help'].'">'.$obj->l('Edit time slots', 'timeslots').'</a>
				</span>
				<br />
				<br />';
		else $output .= myOwnUtils::displayInfo(myOwnHelp::$objects['timeslot']['help'], false);
		
		$btns='';
		if ($from=='product') {
			$btns = '
			<span class="panel-heading-action">
			<a id="desc-customer-export" class="list-toolbar-btn" onclick="$(\'#newTimeSlot\').toggle();">
				<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="'.ucfirst(myOwnLang::$actions[MYOWN_OPE::ADD]).'" data-html="true">
					<i class="process-icon-new "></i>
				</span>
			</a>
			</span>';
		} else {
			$btns = '
			<span class="panel-heading-action">
			<a id="desc-customer-export" class="list-toolbar-btn" href="'.$obj->getConfUrl('timeslots', '&editTimeSlot=0&fromProduct='.$id_family).'">
				<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="'.ucfirst(myOwnLang::$actions[MYOWN_OPE::ADD]).'" data-html="true">
					<i class="process-icon-new "></i>
				</span>
			</a>
			</span>';
		}


		if (_PS_VERSION_ >= "1.6.0.0"  && $from!='object') {
			$output .= MyOwnReservationsUtils::table16wrap($obj, ($id_product==null ? 'timeslots' : ''), 'TimeSlot', ucfirst(myOwnLang::$objects[MYOWN_OBJ::TIMESLOT]), count($list), $btns);
		}
		
		
		$thcount=11;
		if ($from!='product' && $from!='family') $output .= '<form action="'.$obj->getConfUrl('timeslots').'&#"  method="post">';
		$output .= '
				<table style="width:100%;background-color:#FFFFFF" '.($id_family!=null ? 'cellpadding="0" cellspacing="0"' : '').' class="table">
					<thead>
						<tr height="">';
						if ($inProductRule && $isproversion && $from!='product' && $from!='object') {
							$thcount++;
							$output .= '
							<th width="20px" >'.($inProductRule && $isproversion ? '<span class="mylabel-tooltip" title="'.myOwnHelp::$sections['editTimeslotsTab']['timeslot'].'">?</span>' : '').'</th>';
						}
						$output .= '
						<th width="50px" >'.$obj->l('ID', 'timeslots').'</th>
						<th>'.$obj->l('Name', 'timeslots').'</th>';
						if ($isproversion && $id_family>=0 && $from!='product') {
							$thcount++;
							$output .= '
							<th>'.ucfirst(myOwnLang::$object[MYOWN_OBJ::FAMILY]).'</th>';
						}
						if ($isprodts && $from!='product') {
							$thcount++;
							$output .= '
							<th style="text-align:center;">'.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRODUCT]).'</th>';
						}
						$output .= '
						<th width="15%"  style="text-align:center;">'.$obj->l('Start', 'timeslots').'</th>
						<th width="15%"  style="text-align:center;">'.$obj->l('End', 'timeslots').'</th>';
						foreach(myOwnLang::getDaysNames($langue) as $dayId=>$dayName) {
							$output .= '<th width="5%" style="text-align:center;">'.substr($dayName, 0,3).'</th>';
						}
						if ($from!='product') {
							$thcount++;
							$output .= '
							<th width="70px">'.$obj->l('Actions', 'timeslots').'</th>';
						}
					$output .= '
					</tr>
					</thead>';
				
				if (count($list)==0) {
					if (_PS_VERSION_ < "1.6.0.0") $output .= '
					<tr>
						<td colspan="'.$thcount.'">'.ucfirst(myOwnLang::$operations[MYOWN_OPE::NORES]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::TIMESLOT]).'</td>
					</tr>';
					else $output .= '
					<tr>
						<td class="list-empty" colspan="'.$thcount.'">
							<div class="list-empty-msg">
								<i class="icon-warning-sign list-empty-icon"></i>
								'.ucfirst(myOwnLang::$operations[MYOWN_OPE::NORES]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::TIMESLOT]).'
							</div>
						</td>
					</tr>';
				} else
					foreach($list AS $key => $value) {
							$odd=!$odd;
							if ($value->id_family>0) {
								if (!array_key_exists($value->id_family, $obj->_products->list)) $familyname = myOwnLang::getOperation($obj, MYOWN_OPE::DELETE);
								else $familyname = $obj->_products->list[$value->id_family]->name;
								if ($familyname=='') $familyname='#'.$value->id_family;
							} else  $familyname = $obj->l('All', 'timeslots');
							if ($value->id_product>0) {
								$prodname = MyOwnReservationsUtils::getProductName($value->id_product, $langue);
							} else  $prodname = $obj->l('All', 'timeslots');
							$output .= '
						<tr class="row_hover'.($odd==0 ? ' alt_row odd' : '').'">';
							if ($from!='product' && $from!='global' && $from!='object' && ($from!='family' || $id_family)) {
								$output .= '<td><input class="addTimeSlotsDay" name="productTimeslot'. $key .'" type="checkbox" value="1" ';
								if ($value->id_family==$id_family && $id_family>0) $output .= 'checked disabled';
								elseif ($value->id_family>0 && $value->id_family!=$id_family) $output .= 'disabled';
								elseif (is_array($sel) && in_array($key, $sel)) $output .= 'checked';
								$output .= '/></td>';
							}
								$output .= '
							<td>'.($from=='product' || $from=='global' ? '<input type="checkbox" name="deleteTimeslots[]" value="'.$key.'"> ' : '').$key.'</td>
							<td style="font-weight:bold;text-align:center;">'.$value->name.'</td>
							';
							if ($isproversion && $id_family>=0 && $from!='product') 
								$output .= '
								<td style="text-align:center;">'.$familyname.'</td>';
							if ($isprodts && $from!='product') 
								$output .= '
								<td style="text-align:center;">'.$prodname.'</td>';
							$output .= '
							<td  style="text-align:center;">'.MyOwnCalendarTool::formatTime($value->startTime,$langue) .'</td>
							<td style="text-align:center;">'.MyOwnCalendarTool::formatTime($value->endTime,$langue) .'</td>';
							foreach(myOwnLang::getDaysNames($langue) as $dayId=>$dayName) {
								$output .= '
							<td  width="60px" style="text-align:center;">
								<div>'.myOwnUtils::getBoolItem(($value->days[$dayId]==1), myOwnReservationsController::getConfUrl(($id_family>0 ? 'products' : 'timeslots'), ($id_family>0 ? '&editProduct='.$id_family : '').'&'.($value->days[$dayId]==1 ? 'disable' : 'enable').'TimeSlot='.$key.'&day='.$dayId),'#key_tab=editTimeslotsTab').'</div>
							</td>';
							}
							if ($from!='product')
								$output .= '<td style="text-align:center;">
								'.(_PS_VERSION_ < "1.6.0.0" ? '<a href="'.myOwnReservationsController::getConfUrl(Tools::getValue('myOwnResTab').'&fromProduct='.$id_family.'&editTimeSlot='.$key).'"><img src="../img/admin/edit.gif" alt="" title="" /></a><a href="'.myOwnReservationsController::getConfUrl(($id_family==null ? 'timeslots' : 'products'),($id_family==null ? '' : '&fromProduct='.$id_family ).'&deleteTimeSlot='.$key).'" onclick="return confirm(\''.ucfirst(myOwnLang::$actions[MYOWN_OPE::DELETE]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::TIMESLOT]).' '.$key.' ?\');"><img src="../img/admin/delete.gif" alt="" title="" /></a>' : MyOwnReservationsUtils::getListButton($obj, $key, 'timeslots', 'TimeSlot', ((!$inProductRule or $id_family!=null)? '&fromProduct='.$id_family.'&editProduct='.$id_family : ''), ((!$inProductRule or $id_family!=null)? '#key_tab=editTimeslotsTab' : ''))).'
							</td>';
								$output .= '
						</tr>';
				}
			  						
	$output .= '
			</table>';
			
		if ($from!='family' && count($list)) {
			$actions = array('submitAddproductAndStay' => '<i class="icon-trash"></i>&nbsp;'.ucfirst(myOwnLang::$actions[MYOWN_OPE::DELETE]));
			$output .= MyOwnReservationsUtils::displayRow($obj, $actions, 'deleteTimeslots').'<div style="clear:both"></div>';
			$output .= '<div style="clear:both"></div>';
		}
		if ($from!='product' && $from!='family' && $from!='object') $output .= '</form>';
		if (_PS_VERSION_ >= "1.6.0.0" && $from!='object') $output .= '</div>';
		
		if ($id_family==null && _PS_VERSION_ < "1.5.0.0") {
	      	if (array_key_exists($id_family, $obj->_products->list) or $id_family==null) 
	      		$output .= '
			<a href="'.myOwnReservationsController::getConfUrl('products&editTimeSlot=0&fromProduct='.$id_family).'"><img src="../img/admin/add.gif" border="0">&nbsp;'.$obj->l('Add a new', 'timeslots').' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::TIMESLOT]).'</a><br />';
			else $output .= '
			'.$obj->l('Save the product family to add a new timeslot', 'timeslots').'<br />';
			$output .= '</fieldset></div>';		
		}
		return $output;
	}
	

	//TIme selection
	//-----------------------------------------------------------------------------------------------------------------
	public static function timeSelect($name, $value, $langue, $help="", $class="listElemHour") {
           	if ( isset($_POST[$name.'Hour']) ) $value = MyOwnCalendarTool::getHourPost($name);
           	$output='';
			$output .='<select onchange="if(\''.$value.'\'==\'\' && $(\'#'.str_ireplace("start", "end", $name).'Hour\').val()==\'00\') { $(\'#'.str_ireplace("start", "end", $name).'Hour\').val($(\'#'.$name.'Hour\').val())}" '.($help!="" ? 'class="tooltip '.$class.'" title="'.$help.'"' : 'class="'.$class.'"').' name="'.$name.'Hour" id="'.$name.'Hour">';
			//$maxHour=13;
			//$minHour=1;
			//if ($langue==2) 
			$maxHour=24;
			$minHour=0;
			
			if (intval(substr($value,0,2))<13) $selectedTime = intval(substr($value,0,2));
			else $selectedTime = intval(substr($value,0,2))-12;
			if (intval(substr($value,0,2))==0) $selectedTime = 12;
			
			$selectedTime = intval(substr($value,0,2));
			for($i=$minHour; $i<($maxHour); $i++) {
				$hour = $i;
				if ($hour < 10) $hour='0'.$i;
				$output .='<option value="'.$hour.'" ';
				if ($selectedTime == $i) $output .='selected ';
				$output .='>'.$hour.'</option>';
			}
			$output .='</select>';
			$output .='&nbsp;:&nbsp;';
			$output .='<select name="'.$name.'Minute" id="'.$name.'Minute" class="'.$class.'">';
			for($i=0; $i<12; $i++) {
				if (($i*5)<10) $output .='<option value="0'.($i*5).'" ';
				else $output .='<option value="'.($i*5).'" ';
				if (intval(substr($value,3,2)) == ($i*5)) $output .='selected ';
				if (($i*5)<10) $output .='>0'.($i*5).'</option>';
				else $output .='>'.($i*5).'</option>';
			}
			$output .='</select>';
			return $output;
	}

	//==============================================================================================================================================
	//     ADD/EDIT TIME SLOT
	//==============================================================================================================================================

	public static function edit($from, $obj, $id, $id_product=0) {
		global $cookie;
		$days = myOwnLang::getDaysNames($cookie->id_lang);
		$langue=$cookie->id_lang;
		$output='';
		$fromProduct=intval(Tools::getValue('fromProduct'));
		if (intval($id)>0 && array_key_exists($id, $obj->_timeSlots->list)) $value = $obj->_timeSlots->list[$id];
		else {
			$value=new myOwnTimeSlot();
			$value->days=array(1,1,1,1,1,1,1,1);
			$value->id_family=$fromProduct;
		}
		 //object selection
		$obj_sel=null;
		$obj_exts = $obj->_extensions->getExtByType(extension_type::OBJECT);
		foreach ($obj_exts as $obj_ext)
			if ($obj_ext->id==6)
				$obj_sel = $obj_ext;
		
		//$output .= '<h2 style="margin-top:10px">'.(intval($id)>0 ? ucfirst(myOwnLang::$actions[MYOWN_OPE::ADD]) : ucfirst(myOwnLang::$actions[MYOWN_OPE::EDIT])).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::TIMESLOT]).'</h2>';
		$myOwnResTab = Tools::getValue('myOwnResTab');
		if (!$id_product) $output .= '
		<form id="editTimeSlotForm" name="editTimeSlotForm" action="'.($fromProduct>0 ? myOwnReservationsController::getConfUrl('products').'&editProduct='.$fromProduct.'#key_tab=editTimeslotsTab' : myOwnReservationsController::getConfUrl('timeslots') ). '" method="post">';
		//if ($_GET['editTimeSlot']!=0 ) 
		$output .= '<input type="hidden" name="idTimeSlot" value="'.$id.'" />';
		if ($id_product) {
			$output .= '<input type="hidden" name="timeSlotProduct" value="'.$id_product.'" />';
			$mainProduct = $obj->_products->getResProductFromProduct($id_product);
			if ($mainProduct!=null) $output .= '<input type="hidden" name="timeSlotFamily" value="'.$mainProduct->sqlId.'" />';
		}
		$output .= myOwnUtils::displayInfo(myOwnHelp::$objects['timeslot']['help'], false);
		
		if ($id_product==0) {
			if (_PS_VERSION_ < "1.6.0.0") $output .= '
				<fieldset style="float:left;width:45%">
					<legend><img src="../img/t/AdminStatuses.gif" />'.$obj->l('Properties', 'timeslots').'</legend>';
			else $output .= '
				<div class="panel" id="newTimeSlot" style="float:left;width:45%">
				<h3><i class="icon-tags"></i> '.$obj->l('Properties', 'timeslots').'</h3>';
		} else {
			if (_PS_VERSION_ < "1.6.0.0") $output .= '
				<fieldset style="float:left;width:100%">
					<legend><img src="../img/t/AdminStatuses.gif" />'.ucfirst(myOwnLang::$actions[MYOWN_OPE::NNEW]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::TIMESLOT]).'</legend>';
			else $output .= '
				<div class="panel" id="newTimeSlot" style="float:left;width:100%;display:none">
				<h3><i class="icon-time"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::NNEW]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::TIMESLOT]).'</h3>';
		}
		
		$output .= '
				<ol style="padding:0px;margin:0px;'.($id_product==0 ? '' : 'width:45%;float:left').'">
					<li class="myowndlist">
						<label for="name" class="li-label">
							<span class="mylabel-tooltip" title="'.myOwnHelp::$objects['timeslot']['name'].'">'.$obj->l('Name', 'timeslots').'</span>
						</label>
						<input style="width:25%" type="text" size="14" id="timeSlotName" name="timeSlotName" value="'.Tools::getValue('timeSlotName', $value->name).'" /> 
					</li>';
				if (!array_key_exists(0, $obj->_products->list) && !$id_product) {
				$output .= '
					<li class="myowndlist">
						<label for="name" class="li-label">
							<span class="mylabel-tooltip" title="'.myOwnHelp::$objects['timeslot']['product'].'">'.ucfirst(myOwnLang::$object[MYOWN_OBJ::FAMILY]).'</span>
						</label>
						<div style="display:inline-block">'.MyOwnReservationsUtils::insertProductFamillyPicker($obj, $cookie->id_lang, "timeSlotFamily", "", "", Tools::getValue('timeSlotFamily', $value->id_family), "", false, "timeSlotProduct" , $value->id_product, '.reservationUnit', false).'</div>
						
						
					</li>';
				}
				$mymarketplace = $obj->_extensions->getExtByName('mymarketplace');
				if (!$id_product && $mymarketplace!=null)
				$output .= '
				<li class="myowndlist">
					<label for="name" class="li-label">'.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRODUCT]).'</label>
					
					<div style="width:40%;display:inline-block">'.MyOwnReservationsUtils::insertProductPicker($obj, $cookie->id_lang, "timeSlotProduct", $value->id_product, "", 0, "width:100%", '$(\'#holidayProduct\').trigger(\'chosen:updated\');', 0, '', true).'</div>
				</li>';
				if ($obj_sel!=null) $output .= '
					<li class="myowndlist">
						<label for="id_object" class="li-label">
							<span class="mylabel">'.$obj_sel->label.'</span>
						</label>
						'.$obj_sel->productSelectInput($obj, 0, $value->id_object, 'width:15%;', '', true).'
					</li>';
					$output .= '
					<li class="myowndlist">
						<label for="startTime" class="li-label">
							<span class="mylabel-tooltip" title="'.myOwnHelp::$objects['timeslot']['times'].'">'.$obj->l('Start time', 'timeslots').'</span>
						</label>
						'.self::timeSelect("startTime", $value->startTime, $langue).'
					</li>
					<li class="myowndlist">
						<label for="endTime" class="li-label">
							<span class="mylabel-tooltip" title="'.myOwnHelp::$objects['timeslot']['times'].'">'.$obj->l('End time', 'timeslots').'</span>
						</label>
						'.self::timeSelect("endTime", $value->endTime, $langue, myOwnHelp::$objects['timeslot']['times']).'
					</li>
					<li class="myowndlist">
						<label for="type" class="li-label">
							<span class="mylabel-tooltip" title="'.myOwnHelp::$objects['timeslot']['type'].'">'.$obj->l('Type', 'timeslots').'</span>
						</label>
						<select id="timeSlotType" name="timeSlotType" style="width:25%" >';
							$timeSlotTypes = array(
								$obj->l('Any', 'timeslots'), 
								$obj->l('For start', 'timeslots'),
								$obj->l('For end', 'timeslots'),
							);
							foreach ($timeSlotTypes AS $timeSlotTypeKey => $timeSlotTypeValue){
								$output .= '
								<option value="' . $timeSlotTypeKey. '" '.(Tools::getValue('timeSlotType', $value->type)==$timeSlotTypeKey ? ' selected' : '').'>' . $timeSlotTypeValue . '</option>';
							}
							$output .= '
							</select>
					</li>
					<li class="myowndlist">
						<label for="type" class="li-label">
							<span class="mylabel-tooltip" title="'.myOwnHelp::$objects['timeslot']['week_type'].'">'.$obj->l('Week Restriction', 'timeslots').'</span>
						</label>
						<select id="timeSlotWeekType" name="timeSlotWeekType" style="width:25%" >';
							$timeSlotTypes = array(
								0 => $obj->l('Any', 'timeslots'), 
								-2 => $obj->l('Even', 'timeslots'),
								-1 => $obj->l('Odd', 'timeslots'),
							);
							for ($i=1; $i<54; $i++)
								$timeSlotTypes[$i] = $obj->l('Week', 'timeslots').' #'.$i;
								
							foreach ($timeSlotTypes AS $timeSlotWeekTypeKey => $timeSlotWeekTypeValue){
								$output .= '
								<option value="' . $timeSlotWeekTypeKey. '"'.(Tools::getValue('timeSlotWeekType', $value->week_type)==$timeSlotWeekTypeKey ? ' selected' : '').'>' . $timeSlotWeekTypeValue . '</option>';
							}
							$output .= '
							</select>
					</li>
				</ol>';
			if ($id_product==0) {
				if (_PS_VERSION_ < "1.6.0.0") $output .= '
				</fieldset>
				<fieldset style="float:right;width:45%">
					<legend><img src="../img/t/AdminStatuses.gif" />'.$obj->l('Available days', 'timeslots').'</legend>';
				else $output .= '
				</div>
				<div class="panel" style="float:right;width:45%">
					<h3><i class="icon-calendar-o"></i> '.$obj->l('Available days', 'timeslots').'</h3>';
			}
			
			$output .= '
					<ol style="padding:0px;margin:0px;'.($id_product==0 ? '' : 'width:45%;float:left').'">
						<li class="myowndlist" style="margin-bottom:0px;">
							<label class="li-label">
								<span class="mylabel-tooltip" title="'.myOwnHelp::$objects['timeslot']['days'].'">'.$obj->l('Available days', 'timeslots').'</span>&nbsp;
							</label>
							&nbsp;
						</li>
						<div style="clear:both"></div>';
							foreach(myOwnLang::getDaysNames($langue) as $dayId=>$dayName) {
								$output .= '
								<li class="myowndlist" style="margin-bottom:0px;">
									'.myOwnUtils::getBoolField($obj, 'day'. $dayId, (array_key_exists($dayId,  $value->days) && $value->days[$dayId]==1), $dayName, 'width:30%').'
								</li>';
							}	
							$output .= '
					<p style="color:#7F7F7F;padding-top:15px">' . $obj->l('To disable them depending on dates use availabilities management', 'timeslots'). '</p>
				</ol>';
			
			$output .= '
			<div style="clear:both"></div>';
				
			
			
			if (_PS_VERSION_ >= "1.6.0.0" && $id_product>0)  $output .= '
			<div class="panel-footer">
				<a onclick="$(\'#timeSlotName\').val(\'\');$(\'#newTimeSlot\').hide()" class="btn btn-default"><i class="process-icon-cancel"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::CANCEL]).'</a>
				<button type="submit" class="btn btn-default pull-right" name="submitAddproductAndStay" onclick="$(\'#priceSetBtn\').attr(\'checked\',\'checked\')"><i class="process-icon-save"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::ADD]).'</button>
			</div>';
			
			if (_PS_VERSION_ < "1.6.0.0") $output .= '
			</fieldset>';
			else $output .= '
			</div><div style="clear:both"></div>';
				
			if (!$id_product) $output .= '
			</form>
';
		
		return $output;
	}


}



?>