<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnReservationsController {

	public static $name = 'myownreservations';
	public static $hooks = array('cart', 'postUpdateOrderStatus', 'shoppingCartExtra', 'productActions', 'newOrder', 'adminOrder', 'header', 'productTab', 'productTabContent', 'orderDetailDisplayed', 'leftcolumn', 'rightcolumn', 'home', 'productListAssign', 'orderConfirmation', 'createAccount');
	public static $priceruleImpacts;
	public static $priceruleTypes;
	public static $priceruleTypDescription;
	public static $reservationUnitKeys;
	public static $reservationInterval;
	public static $reservationIntervalDescription;
	public static $reservationViews;
	public static $reservationUnitViews;
	public static $reservTypes;
	public static $availabilityTypes;
	public static $reservSwitchesNext;
	public static $reservSwitchesPrevious;
	public static $productType;
	public static $productTypeDescription;
	public static $combinationType;
	public static $virtualQuantity;
	public static $reservQtyType;
	public static $lengthControl;
	public static $reservationLengthUnits;
	public static $reservationPriceUnits;
	public static $reservPriceType;
	public static $depositTypes;
    
	//Construct objects
	//-----------------------------------------------------------------------------------------------------------------
	public static function _construire ($obj, $force = false) {
		if ($force) {
			/*$obj->_timeSlots = new myOwnTimeSlots();
			$obj->_pricerules = new myOwnPricesRules();
			$obj->_products = new myOwnProductsRules();
			$obj->_stocks = new myOwnStocks();
			$obj->_extensions = new myOwnExtensions();
			$obj->_extensions->setInfos($obj);*/
		}
		$id_lang = $obj->getContext()->cookie->id_lang;
		$iso = Tools::strtolower(Language::getIsoById($id_lang));
		myOwnLang::translate($obj);
		myOwnHelp::translate($obj, $iso);
		
		self::$reservationUnitKeys = array(
				reservation_unit::TIMESLOT,
				reservation_unit::DAY,
				reservation_unit::WEEK,
				reservation_unit::MONTH
		);
			
		self::$reservationUnitViews= array(
			reservation_interval::TIMESLICE 	=> array(reservation_view::DAY, 
														reservation_view::WEEK, 
														reservation_view::TWOWEEKS, 
														reservation_view::MONTH), 
			reservation_interval::TIMESLOT 	=> array(reservation_view::MINIMAL, 
														reservation_view::DAY, 
														reservation_view::WEEK, 
														reservation_view::TWOWEEKS, 
														reservation_view::MONTH), 
			reservation_interval::DAY 		=> array(reservation_view::WEEK, 
														reservation_view::TWOWEEKS, 
														reservation_view::MONTH),
			reservation_interval::WEEK 		=> array(reservation_view::MONTH, 
														reservation_view::QUARTER),
			reservation_interval::MONTH 	=> array(reservation_view::QUARTER, 
														reservation_view::YEAR),
			reservation_interval::PERIOD 	=> array(reservation_view::MINIMAL)
		);

		
		self::$reservationLengthUnits= array(
			reservation_interval::TIMESLICE 	=> array(reservation_unit::TIMESLOT),
			reservation_interval::TIMESLOT 	=> array(reservation_unit::TIMESLOT,
														reservation_unit::DAY,
														reservation_unit::WEEK,
														reservation_unit::MONTH), 
			reservation_interval::DAY 		=> array(reservation_unit::DAY,
														reservation_unit::WEEK,
														reservation_unit::MONTH), 
			reservation_interval::WEEK 		=> array(reservation_unit::WEEK,
														reservation_unit::MONTH), 
			reservation_interval::MONTH 	=> array(reservation_unit::DAY,
														reservation_unit::WEEK,
														reservation_unit::MONTH), 
			reservation_interval::PERIOD 	=> array(reservation_unit::TIMESLOT,
														reservation_unit::DAY,
														reservation_unit::WEEK,
														reservation_unit::MONTH)
		);


	}
	
	public static function _setInfos ($obj) {
		$isproversion=is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");
		$obj->displayName = $obj->l('MyOwnReservations', 'controller').' '.($isproversion ? 'PRO' : '');
		$obj->description = $obj->l('This module manage customer reservations : you can setup your own reservation time slots that the customer can select while adding a product on cart.', 'controller');
		$obj->confirmUninstall = $obj->l('Are you sure you want delete myOwnReservations module and your time slots, configuration and reservations ?', 'controller');
		
		if ($obj->isInstalled($obj->name) && !Configuration::get('MYOWNRES_OLD_CHECK', 0)) {
			$tables = Db::getInstance()->ExecuteS(" SHOW TABLES LIKE '"._DB_PREFIX_."myownreservations%';");
			foreach ($tables as $table)
				foreach ($table as $tabledetail)
					if (stripos(str_replace(_DB_PREFIX_,"",$tabledetail), "_")===false) {
						if (stripos(str_replace(_DB_PREFIX_,"",$tabledetail), "myownreservationspro")!==false && stripos(str_replace(_DB_PREFIX_,"",$tabledetail), "myownreservationsprod")===false) 
							Db::getInstance()->Execute("RENAME TABLE `".$tabledetail."` TO `".strtolower(str_replace("myownreservationspro", "myownreservations_", $tabledetail))."`;");
						else Db::getInstance()->Execute("RENAME TABLE `".$tabledetail."` TO `".strtolower(str_replace("myownreservations", "myownreservations_", $tabledetail))."`;");
					}
			if(file_exists(MYOWNRES_PATH . 'pro/install.sql')) self::installDb('pro/install');
			
			  		
	  		$tables = Db::getInstance()->ExecuteS("SHOW TABLES LIKE '"._DB_PREFIX_."myownreservations_pricesset';");
			if(count($tables)==0) {
				$query = "
					CREATE TABLE IF NOT EXISTS `". _DB_PREFIX_ ."myownreservations_pricesset` (
					  `id_product` int(10) NOT NULL,
					  `id_product_attribute` int(10) NOT NULL,
					  `period` int(2) NOT NULL,
					  `amount` decimal(10,3) NOT NULL
					) DEFAULT CHARSET=utf8 ;";
					Db::getInstance()->Execute($query);
			}
			
			$tables = Db::getInstance()->ExecuteS("SHOW TABLES LIKE '"._DB_PREFIX_."myownreservations_option';");
			if(count($tables)==0) {
				$query = "
					CREATE TABLE IF NOT EXISTS `". _DB_PREFIX_ ."myownreservations_option` (
					  `id_productFamilly` int(10) NOT NULL,
					  `id_extension` int(10) NOT NULL,
					  `id_option` int(10) NOT NULL,
					  `id_internal` int(10) NOT NULL,
					  `type` tinyint(3) NOT NULL,
					  `value` text NOT NULL,
					  PRIMARY KEY (`id_productFamilly`, `id_extension`, `id_option`, `id_internal`)
					) DEFAULT CHARSET=utf8 ;";
					Db::getInstance()->Execute($query);
			}
	
			$tables = Db::getInstance()->ExecuteS("SHOW TABLES LIKE '"._DB_PREFIX_."myownreservations_detail';");
			if(count($tables)==0) {
				$query = "
					CREATE TABLE IF NOT EXISTS `". _DB_PREFIX_ ."myownreservations_detail` (
					  `id_reservation` int(10) NOT NULL,
					  `id_extension` int(10) NOT NULL,
					  `id_option` int(10) NOT NULL,
					  `id_internal` int(10) NOT NULL,
					  `type` tinyint(3) NOT NULL,
					  `value` text NOT NULL,
					  PRIMARY KEY (`id_reservation`, `id_extension`, `id_option`, `id_internal`)
					) DEFAULT CHARSET=utf8 ;";
					Db::getInstance()->Execute($query);			
			}
			if (!MyOwnUtils::isFieldInTable("myownreservations_option", "id_shop") && !MyOwnUtils::isFieldInTable("myownreservations_option", "id_shop")) 
	  			Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_option` ADD `id_shop` int(10) NOT NULL");
			if (!MyOwnUtils::isFieldInTable("myownreservations_option", "id_productFamilly") && !MyOwnUtils::isFieldInTable("myownreservations_option", "id_family")) 
	  			Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_option` ADD `id_productFamilly` int(10) NOT NULL");
			if (!MyOwnUtils::isFieldInTable("myownreservations_option", "id_family")) 
	  			Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_option` CHANGE `id_productFamilly`  `id_family` int(10) NOT NULL");
	  		if (MyOwnUtils::isFieldInTable("myownreservations_option", "id_familly") && MyOwnUtils::isFieldInTable("myownreservations_option", "id_family")) 
	  			Db::getInstance()->Execute("ALTER TABLE `". _DB_PREFIX_ ."myownreservations_option` DROP PRIMARY KEY; ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_option` DROP `id_familly`, ADD PRIMARY KEY (  `id_family`,  `id_shop`,  `id_extension`,  `id_option`,  `id_internal` );");
	  			
	  		if (!MyOwnUtils::isFieldInTable("myownreservations_timeslot", "id_family") && MyOwnUtils::isFieldInTable("myownreservations_timeslot", "id_product")) 
	  			Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_timeslot` CHANGE `id_product`  `id_family` int(10) NOT NULL");
	  		if (!MyOwnUtils::isFieldInTable("myownreservations_timeslot", "id_timeslot")) 
	  			Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_timeslot` CHANGE `id_timeSlot`  `id_timeslot` int(10) NOT NULL AUTO_INCREMENT");
	  		if (!MyOwnUtils::isFieldInTable("myownreservations_pricesrule", "id_product_attribute")) 
	  			Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_pricesrule` CHANGE `product_attribute`  `id_product_attribute` int(10) NOT NULL");
	  		if (!MyOwnUtils::isFieldInTable("myownreservations_pricesrule", "id_pricesrule")) 
	  			Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_pricesrule` CHANGE `id_pricerule`  `id_pricerule` int(10) NOT NULL;ALTER TABLE `". _DB_PREFIX_ ."myownreservations_pricesrule` DROP PRIMARY KEY; ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_pricesrule` CHANGE `id_pricerule`  `id_pricesrule` int(10) NOT NULL AUTO_INCREMENT, ADD PRIMARY KEY (  `id_pricesrule` );");
	  		Configuration::updateValue('MYOWNRES_OLD_CHECK', 1);
  		}
  		//module-myownreservations-reservations
	}
	
	public static function getConfUrl($myOwnResTab='reservation', $args='', $anchor='') {
		global $cookie;
		if (file_exists(_PS_ROOT_DIR_."/override/classes/Tab.php")) {
			if (file_exists(MYOWNRES_PATH . "extensions/Admin".ucfirst($myOwnResTab).".php")) 
				return '?controller=Admin'.ucfirst($myOwnResTab).$args.'&token='.Tools::getAdminToken('Admin'.ucfirst($myOwnResTab).intval(Tab::getIdFromClassName('Admin'.ucfirst($myOwnResTab))).intval($cookie->id_employee));
		}
		if (file_exists(MYOWNRES_PATH . "adminReservation".ucfirst($myOwnResTab).".php")) 
			return '?controller=adminReservation'.ucfirst($myOwnResTab).$args.'&token='.Tools::getAdminToken('adminReservation'.ucfirst($myOwnResTab).intval(Tab::getIdFromClassName('adminReservation'.ucfirst($myOwnResTab))).intval($cookie->id_employee));
		
		if ($myOwnResTab=='stock') return '?controller=adminReservationStock'.$args.'&token='.Tools::getAdminToken('adminReservationStock'.intval(Tab::getIdFromClassName('adminReservationStock')).intval($cookie->id_employee));
		if ($myOwnResTab=='products') return '?controller=adminReservationRules'.$args.'&token='.Tools::getAdminToken('adminReservationRules'.intval(Tab::getIdFromClassName('adminReservationRules')).intval($cookie->id_employee));
		if ($myOwnResTab=='pricesrules') return '?controller=adminReservationPrices'.$args.'&token='.Tools::getAdminToken('adminReservationPrices'.intval(Tab::getIdFromClassName('adminReservationPrices')).intval($cookie->id_employee));
		if (_PS_VERSION_ >= "1.5.0.0") return '?controller=AdminModules&configure=myownreservations&myOwnResTab='.$myOwnResTab.$args.'&token='.Tools::getAdminToken('AdminModules'.intval(Tab::getIdFromClassName('AdminModules')).intval($cookie->id_employee)).'&tab_module=checkout&module_name=myownreservations'.$anchor;
		else return '?tab=AdminModules&configure=myownreservations&myOwnResTab='.$myOwnResTab.$args.'&token='.Tools::getAdminToken('AdminModules'.intval(Tab::getIdFromClassName('AdminModules')).intval($cookie->id_employee)).$anchor;
	}
	
	public static function getAdminUrl($params) {
		global $cookie;
		return '?tab=adminmyownreservations&'.$params.'&orderby=name&orderway=desc&token='.Tools::getAdminToken('adminmyownreservations'.intval(Tab::getIdFromClassName('adminmyownreservations')).intval($cookie->id_employee));
	}
	public static function getAjaxUrl($params) {
		global $cookie;
		return 'ajax-tab.php?tab=adminmyownreservations'.'&token='.Tools::getAdminToken('adminmyownreservations'.intval(Tab::getIdFromClassName('adminmyownreservations')).intval($cookie->id_employee)).'&'.$params;
	}

	public static function installDb($db) {
		//CONSTRUCT TABLES
		if(!file_exists(MYOWNRES_PATH . $db.'.sql')) {
			return false;
		} elseif(!$sql = file_get_contents(MYOWNRES_PATH .$db.'.sql')) {
			return false;
		}
		$sql = str_replace('ps_', _DB_PREFIX_, $sql);
		$sql = preg_split("/;\s*[\r\n]+/", $sql);

		foreach($sql AS $k => $query) {
			try {
				if (strlen(trim($query))>0) Db::getInstance()->Execute(trim($query));
			} catch (Exception $e) {
				return false;
			}
		}
	}
	//==============================================================================================================================================
	//     SETUP
	//==============================================================================================================================================
	
    public static function install($obj, $cookie) {
    	$tablesexists=false;
    	$tables = Db::getInstance()->ExecuteS(" SHOW TABLES LIKE '"._DB_PREFIX_."myownreservations%';");
    	if (count($tables)>0) $tablesexists=true;

    	//install tables
    	self::installDb('sql/install');

    	if(!$tablesexists) {
			//INSERT SAMPLES IN DIFERENT LANG
			$lang_iso=Language::getIsoById($cookie->id_lang);
			if ($lang_iso!='fr') $lang_iso='en';
			if(file_exists(MYOWNRES_PATH . 'pro/samples-'.$lang_iso.'.sql')) self::installDb('pro/samples-'.$lang_iso);
			else self::installDb('sql/samples-'.$lang_iso);
		}

		//Default params
		//-----------------------------------------------------------------------------------------------------------------
		Configuration::updateValue('MYOWNRES_INSTALLED', 1);

		//Resa settings
		Configuration::updateValue('MYOWNRES_REG_LENGTH', 1);

		//Price settings
		Configuration::updateValue('MYOWNRES_ORDER_DEPOSIT', 0);
		Configuration::updateValue('MYOWNRES_ORDER_ADVANCE', 100);
		
		Configuration::updateValue('MYOWNRES_RES_STATUS', "2,3,4,5");
		$fr = Language::getIdByIso('fr');
		$en = Language::getIdByIso('en');
		$es = Language::getIdByIso('es');
		
		$msg_order_advance = array($en => htmlentities("Advance"), $fr => htmlentities("Acompte",ENT_QUOTES,'UTF-8'), $es => htmlentities("Anticipo",ENT_QUOTES,'UTF-8'));
		Configuration::updateValue('MYOWNRES_MSG_ADVANCE', $msg_order_advance);
		$msg_order_deposit = array($en => htmlentities("Deposit"), $fr => htmlentities("Caution",ENT_QUOTES,'UTF-8'), $es => htmlentities("Caución",ENT_QUOTES,'UTF-8'));
		Configuration::updateValue('MYOWNRES_MSG_DEPOSIT', $msg_order_deposit);
		$msg_reservation = array($en => htmlentities("Reservation"), $fr => htmlentities("Reservation",ENT_QUOTES,'UTF-8'), $es => htmlentities("Reservación",ENT_QUOTES,'UTF-8'));
		Configuration::updateValue('MYOWNRES_MSG_RESV', $msg_reservation);
		$msg_balance = array($en => htmlentities("Balance"), $fr => htmlentities("Solde",ENT_QUOTES,'UTF-8'), $es => htmlentities("Saldo",ENT_QUOTES,'UTF-8'));
		Configuration::updateValue('MYOWNRES_MSG_BALANCE', $msg_balance);


		foreach (self::$hooks as $hook)
			$obj->registerHook($hook);
      	self::installModuleTab($obj->name, 'adminmyownreservations');

		return true;
    }
    
    public static function uninstall($obj) {
        $res=true;
		if (_PS_VERSION_ >= "1.4.0.0")
      		$obj->unregisterHook('orderDetailDisplayed');
      		
      	if(file_exists(_PS_ROOT_DIR_.'/override/classes/Cart.php')) unlink(_PS_ROOT_DIR_.'/override/classes/Cart.php');
      	if(file_exists(_PS_ROOT_DIR_.'/override/classes/PaymentModule.php')) unlink(_PS_ROOT_DIR_.'/override/classes/PaymentModule.php');
      	if(file_exists(_PS_ROOT_DIR_.'/override/classes/PDF.php')) unlink(_PS_ROOT_DIR_.'/override/classes/PDF.php');
      		
        foreach (self::$hooks as $hook)
        	$obj->unregisterHook($hook);
        	
        self::uninstallModuleTab('adminmyownreservations');
        self::uninstallModuleTab('adminReservationRules');
        return true;
    }

	//Uninstall tab
	//-----------------------------------------------------------------------------------------------------------------
	public static function uninstallModuleTab($tabClass)
	{
	  $idTab = Tab::getIdFromClassName($tabClass);
	  if($idTab != 0)
	  {
		$tab = new Tab($idTab);
		$tab->delete();
		return true;
	  }
	  return false;
	}
	
	//install tab
	//-----------------------------------------------------------------------------------------------------------------
	public static function installModuleTab($modname, $tabClass)
	{
		//INSTALL TAB
		$tab = new Tab();
		$tab->name = array(1=>'My Own Reservations', 2=>'Mes Réservations', 3=>'My Own Reservations', 4=>'My Own Reservations', 5=>'My Own Reservations', 6=>'My Own Reservations', 7=>'My Own Reservations', 8=>'My Own Reservations', 9=>'My Own Reservations');
		$id_fr = (int)Language::getIdByIso('fr');
		$tab->name[$id_fr] = 'Mes Réservations';
		$id_es = (int)Language::getIdByIso('es');
		$tab->name[$id_es] = 'Mi Reserva';
		$tab->class_name = $tabClass;
		$tab->module = $modname;
		$tab->id_parent = 3;
		if (_PS_VERSION_ >= "1.5.0.0")
			$tab->id_parent = 10;
		if(!$tab->save())
			return false;
		
		return true;
	}
	
	public static function installModuleTabAdmin($obj)
	{
		//INSTALL TAB
		$tab = new Tab();
		$tab->name = array(1=>'Reservations rules', 2=>'Reservations rules', 3=>'Reservations rules', 4=>'Reservations rules', 5=>'Reservations rules', 6=>'Reservations rules', 7=>'Reservations rules', 8=>'Reservations rules', 9=>'Reservations rules');
		$id_fr = (int)Language::getIdByIso('fr');
		$tab->name[$id_fr] = 'Règles de réservation';
		$id_es = (int)Language::getIdByIso('es');
		$tab->name[$id_es] = 'Normas de rerserva';
		$tab->class_name = 'adminReservationRules';
		$tab->module = 'myownreservations';
		$tab->id_parent = 3;
		if (_PS_VERSION_ >= "1.5.0.0")
			$tab->id_parent = 9;
		if(!$tab->save())
			return false;
		
		return true;
	}
	
	public static function installModuleTabConfig($obj)
	{
		//INSTALL TAB
		$tab = new Tab();
		$tab->name = array(1=>'Reservations', 2=>'Reservations', 3=>'Reservations', 4=>'Reservations', 5=>'Reservations', 6=>'Reservations', 7=>'Reservations', 8=>'Reservations', 9=>'Reservations');
		$id_fr = (int)Language::getIdByIso('fr');
		$tab->name[$id_fr] = 'Réservations';
		$id_es = (int)Language::getIdByIso('es');
		$tab->name[$id_es] = 'Rerservas';
		$tab->class_name = 'adminReservationConfig';
		$tab->module = 'myownreservations';
		$tab->id_parent = 3;
		if (_PS_VERSION_ >= "1.5.0.0")
			$tab->id_parent = 16;
		if(!$tab->save())
			return false;
		
		return true;
	}
	
	public static function installModuleTabPrice($obj)
	{
		//INSTALL TAB
		$tab = new Tab();
		$tab->name = array(1=>'Planning Price rules', 2=>'Planning Price rules', 3=>'Planning Price rules', 4=>'Planning Price rules', 5=>'Planning Price rules', 6=>'Planning Price rules', 7=>'Planning Price rules', 8=>'Planning Price rules', 9=>'Planning Price rules');
		$id_fr = (int)Language::getIdByIso('fr');
		$tab->name[$id_fr] = 'Règles de prix planning';
		$id_es = (int)Language::getIdByIso('es');
		$tab->name[$id_es] = 'Reglas de precios de calendario';
		$tab->class_name = 'adminReservationPrices';
		$tab->module = 'myownreservations';
		$tab->id_parent = 3;
		if (_PS_VERSION_ >= "1.5.0.0")
			$tab->id_parent = 12;
		if(!$tab->save())
			return false;
		
		return true;
	}
	
	public static function installModuleTabAvailability($obj)
	{
		//INSTALL TAB
		$tab = new Tab();
		$tab->name = array(1=>'Availabilities', 2=>'Availabilities', 3=>'Availabilities', 4=>'Availabilities', 5=>'Availabilities', 6=>'Availabilities', 7=>'Availabilities', 8=>'Availabilities', 9=>'Availabilities');
		$id_fr = (int)Language::getIdByIso('fr');
		$tab->name[$id_fr] = 'Disponibilités';
		$id_es = (int)Language::getIdByIso('es');
		$tab->name[$id_es] = 'Disponibilidad';
		$tab->class_name = 'adminReservationAvailability';
		$tab->module = 'myownreservations';
		$tab->id_parent = 3;
		if (_PS_VERSION_ >= "1.5.0.0")
			$tab->id_parent = 9;
		if(!$tab->save())
			return false;
		
		return true;
	}
	
	
	public static function installModuleTabStock($obj)
	{
		//INSTALL TAB
		$tab = new Tab();
		$tab->name = array(1=>'Stock', 2=>'Stock', 3=>'Stock', 4=>'Stock', 5=>'Stock', 6=>'Stock', 7=>'Stock', 8=>'Stock', 9=>'Stock');
		$id_fr = (int)Language::getIdByIso('fr');
		$tab->name[$id_fr] = 'Stock';
		$id_es = (int)Language::getIdByIso('es');
		$tab->name[$id_es] = 'Stock';
		$tab->class_name = 'adminReservationStock';
		$tab->module = 'myownreservations';
		$tab->id_parent = 3;
		if (_PS_VERSION_ >= "1.5.0.0")
			$tab->id_parent = 9;
		if(!$tab->save())
			return false;
		
		return true;
	}
	
	public static function scan_dir($name, $folder='') { 
		$files=array();
		if ($dir = opendir($name)) { 
			while($file = readdir($dir)) { 
				if (!in_array($file, array(".",".."))) {
					if(is_dir($name.'/'.$file)) { 
						$folderfiles=self::scan_dir($name.'/'.$file,$file);
						if ($folder=='') $files = array_merge($files, $folderfiles); 
					} else 
						$files[]= ($folder!='' ? $folder.'/' : '').$file;
				}
			} 
			closedir($dir); 
		}
		return $files;
	} 
	
	public static function configCheckKey($obj) {
		$content = '';
		//unexisting func but needed to populate isKeyValid && isLocal
		$obj->check();
		$ask = '&nbsp;&nbsp;&nbsp;<a href="http://www.labulle.net/contact/" target="_blank">'.$obj->l('Request a key.', 'controller').'</a>';
		if (!$obj->isKeyValid && $obj->isLocal) $content.= myOwnUtils::displayWarn($obj->l('Front-end output is enabled for a local host only, you have to enter a valid key.', 'controller').$ask);
		else if (!$obj->isKeyValid) $content.= myOwnUtils::displayError($obj->l('Front-end output is disabled, you must enter a valid key.', 'controller').$ask);

		return $content;
	}
	
	public static function isFieldInTable($table, $field, $type='') {
		$fieldfound=false;
		$columns = Db::getInstance()->ExecuteS(" SHOW COLUMNS FROM " . _DB_PREFIX_ . $table);
	  	foreach ($columns as $column)
	  		if ($column["Field"]==$field && ($type=="" || $type==$column["Type"])) $fieldfound=true;

	  	return $fieldfound;
	}
	
	public static function checkVersion($obj) {	
		$check = Configuration::get('MYOWNRES_CHECK_VERSION', time());

		$last = round((time()-$check)/3600);
		//update each 8h
		if ($last>8) {
			$ctx = stream_context_create(array('http'=>
			    array(
			        'timeout' => 50, // 1 200 Seconds = 20 Minutes
			    )
			));
			$version_json = @file_get_contents('http://www.labulle.net/updates/myownreservations'.$obj->version.'.png', false, $ctx);
			if (stripos($version_json, '{')==0) {
				Configuration::updateValue('MYOWNRES_CHECK_VERSION', time());
				Configuration::updateValue('MYOWNRES_LAST_VERSION', $version_json);
			} else return null;
		} else $version_json = Configuration::get('MYOWNRES_LAST_VERSION');
		
		return json_decode ($version_json);
	}
	
	public static function checkDatabase($obj) {
		$updatedb=array();
		$obj->checkIsProVersion();
		if ($obj->isProVersion) {
	  		if (!MyOwnUtils::isFieldInTable("myownreservations_products", "email")) $updatedb[]="products_email";
	  		if (!MyOwnUtils::isFieldInTable("myownreservations_products", "ids_users")) $updatedb[]="products_ids_users";
	  		if (!MyOwnUtils::isFieldInTable("myownreservations_products", "reservationSelType")) $updatedb[]="products_reservationSelType";
	  		if (!MyOwnUtils::isFieldInTable("myownreservations_products", "qtyParams")) $updatedb[]="products_qtyParams";
	  		if (!MyOwnUtils::isFieldInTable("myownreservations_products", "ids_categories")) $updatedb[]="products_ids_categories";
	  		if (!MyOwnUtils::isFieldInTable("myownreservations_products", "ids_products")) $updatedb[]="products_ids_products";
	  		if (!MyOwnUtils::isFieldInTable("myownreservations_products", "ids_notifs")) $updatedb[]="products_ids_notifs";
	  		if (!MyOwnUtils::isFieldInTable("myownreservations_products", "ids_options")) $updatedb[]="products_ids_options";
	  		if (!MyOwnUtils::isFieldInTable("myownreservations_products", "id_shop")) $updatedb[]="products_id_shop";
	  		if (!MyOwnUtils::isFieldInTable("myownreservations_products", "productOccupyParams")) $updatedb[]="products_productOccupyParams";
	  		if (!MyOwnUtils::isFieldInTable("myownreservations_products", "depositAdvanceParams")) $updatedb[]="products_depositAdvanceParams";
	  		if (!MyOwnUtils::isFieldInTable("myownreservations_products", "productType")) $updatedb[]="products_productType";
	  		if (!MyOwnUtils::isFieldInTable("myownreservations_products", "assignment")) $updatedb[]="products_assignment";
	  		if (!MyOwnUtils::isFieldInTable("myownreservations_stock", "id_shop")) $updatedb[]="stock_id_shop";
	  		if (!MyOwnUtils::isFieldInTable("myownreservations_stock", "enabled")) $updatedb[]="stock_enabled";
	  		if (!MyOwnUtils::isFieldInTable("myownreservations_stock", "quantity")) $updatedb[]="stock_quantity";
  		}
  		if (!MyOwnUtils::isFieldInTable("myownreservations_availability", "start_timeslot")) $updatedb[]="availability_start_timeslot";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_availability", "end_timeslot")) $updatedb[]="availability_end_timeslot";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_availability", "quantity")) $updatedb[]="availability_quantity";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_availability", "id_family")) $updatedb[]="availability_id_family";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_availability", "id_object")) $updatedb[]="availability_id_object";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_pricesrule", "impact")) $updatedb[]="pricesrule_impact";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_pricesrule", "overall")) $updatedb[]="pricesrule_overall";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_pricesrule", "id_family")) $updatedb[]="pricesrule_id_family";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_pricesrule", "style")) $updatedb[]="pricesrule_style";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_reservation", "id_order")) $updatedb[]="reservation_id_order";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_reservation", "reference")) $updatedb[]="reservation_reference";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_reservation", "id_object")) $updatedb[]="reservation_id_object";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_reservation", "stock")) $updatedb[]="reservation_stock";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_reservation", "quantity_refunded")) $updatedb[]="reservation_quantity_refunded";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_reservation", "advance")) $updatedb[]="reservation_advance";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_reservation", "start_time")) $updatedb[]="reservation_start_time";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_reservation", "end_time")) $updatedb[]="reservation_end_time";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_reservation", "id_customization")) $updatedb[]="reservation_id_customization";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_reservation", "id_shop")) $updatedb[]="reservation_idshop";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_reservation", "comment")) $updatedb[]="reservation_comment";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_reservation", "external_fields")) $updatedb[]="reservation_external";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_reservation", "id_assigned")) $updatedb[]="reservation_idassigned";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_reservation", "id_cartproduct")) $updatedb[]="reservation_idcartproduct";
  		//if (!MyOwnUtils::isFieldInTable("myownreservations_timeslot", "id_product")) $updatedb[]="timeslot_id_product";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_cartproducts", "id_customization")) $updatedb[]="cartproducts_id_customization";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_cartproducts", "id_cartproduct")) $updatedb[]="cartproducts_id_cartproduct";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_cartproducts", "id_object")) $updatedb[]="cartproducts_id_object";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_pricesrule", "ids_products")) $updatedb[]="pricesrule_ids_products";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_pricesrule", "ids_attributes")) $updatedb[]="pricesrule_ids_attributes";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_pricesrule", "quantity")) $updatedb[]="pricesrule_quantity";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_pricesrule", "id_object")) $updatedb[]="pricesrule_id_object";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_timeslot", "id_object")) $updatedb[]="timeslot_id_object";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_option", "id_shop")) $updatedb[]="option_id_shop";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_timeslot", "id_product")) $updatedb[]="timeslot_id_product";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_timeslot", "week_type")) $updatedb[]="timeslot_week_type";
  		if (!MyOwnUtils::isFieldInTable("myownreservations_reservation", "id_source")) $updatedb[]="reservation_idsource";
			
  		if (defined('MYOWNRES_SEPARATE_SHOP') && MYOWNRES_SEPARATE_SHOP && !MyOwnUtils::isFieldInTable("myownreservations_timeslot", "id_shop")) $updatedb[]="timeslot_id_shop";
  		if (defined('MYOWNRES_SEPARATE_SHOP') && MYOWNRES_SEPARATE_SHOP && !MyOwnUtils::isFieldInTable("myownreservations_availability", "id_shop")) $updatedb[]="availability_id_shop";
  		if (defined('MYOWNRES_SEPARATE_SHOP') && MYOWNRES_SEPARATE_SHOP && !MyOwnUtils::isFieldInTable("myownreservations_pricesrule", "id_shop")) $updatedb[]="pricesrule_id_shop";
  		//if (defined('MYOWNRES_SEPARATE_SHOP') && MYOWNRES_SEPARATE_SHOP && !MyOwnUtils::isFieldInTable("myownreservations_availability", "id_shop")) $updatedb[]="availability_id_shop";

		return $updatedb;
	}
	
	public static function getLink($link) {
		global $cookie;
		return '?tab=AdminModules&configure=myownreservations&token='.Tools::getAdminToken('AdminModules'.intval(Tab::getIdFromClassName('AdminModules')).intval($cookie->id_employee)).$link;
	}
	
	//Check config
	//-----------------------------------------------------------------------------------------------------------------
	public static function configCheckRules($obj) {
		$out='';
		if (count($obj->_products->list)==0)
			return myOwnUtils::displayInfo(myOwnHelp::$tuto['pro']);

		return '';
	}
	public static function configCheckSetup($obj) {
		global $cookie;
     	$content = "";
     	$myOwnResTab=Tools::getValue('myOwnResTab');
     	$isproversion=is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");
     	if ($obj->isInstalled($obj->name)) {
     		$fr = Language::getIdByIso('fr');
     		$en = Language::getIdByIso('en');
	       
	       $hooksMissing="";
	       foreach(self::$hooks as $hook) {
	       		//echo $hook.' ';
	       		$res = myOwnUtils::isHookForModule($hook, $obj->id);
	       		if ($res===false) {
	       			if ($hooksMissing!='') $hooksMissing.=', ';
	       			$hooksMissing.=$hook; 
	       		}
	       }
	       if ($hooksMissing!='') $content.= myOwnUtils::displayError('<b>'.$obj->l('Hooks missing', 'controller').'</b> : '.$hooksMissing.'<input style="float:right;text-align:left;padding-left:22px;background:#DFDFDF 3px 3px no-repeat url(\'../img/admin/duplicate.gif\')" type="button" name="help" class="configbutton" value="'.$obj->l('Insert Hooks', 'controller').'" onclick="window.location=\''.self::getConfUrl($myOwnResTab,'&hookupdate=1').'\';">');
	       
	     $idTab = Tab::getIdFromClassName('adminmyownreservations');
	     if (intval($idTab)>0) {
			$idTabParent =  Tab::getIdFromClassName('AdminParentOrders');
			$tab= new Tab($idTab);
			if (_PS_VERSION_ >= "1.5.0.0" && $tab->id_parent!=$idTabParent) {
				$tab->id_parent=$idTabParent;
				$tab->update();
			}
		} else self::installModuleTab($obj->name, 'admin'.$obj->name);
		
		$idTab = Tab::getIdFromClassName('adminReservationRules');
	    if (intval($idTab)==0) self::installModuleTabAdmin($obj);
	    
	    $idTab = Tab::getIdFromClassName('adminReservationConfig');
	    if (intval($idTab)==0) self::installModuleTabConfig($obj);
	    
	    $idTab = Tab::getIdFromClassName('adminReservationAvailability');
	    if (intval($idTab)==0) self::installModuleTabAvailability($obj);

	    $idTab = Tab::getIdFromClassName('adminReservationPrices');
	    if (intval($idTab)==0) self::installModuleTabPrice($obj);
	    if ($isproversion) {
		    $idTab = Tab::getIdFromClassName('adminReservationStock');
		    if (intval($idTab)==0) self::installModuleTabStock($obj);
		}
		 	//Check folder permissions
			//-----------------------------------------------------------------------------------------------------------------
			//if (!is_executable ( _PS_MODULE_DIR_ . self::$name . "/controllers/ajax.php" ))
				//$content.= myOwnUtils::displayError('<b>'.$obj->l('Please allow execution on file', 'controller').' /modules/'.self::$name.'/controllers/ajax.php</b>');
			if (!is_writable (_PS_ROOT_DIR_."/override/classes" ))
				$content.= myOwnUtils::displayError('<b>'.$obj->l('Please allow write on folder', 'controller').' /override/classes</b>');
		 
		 
			//Check overrides
			//-----------------------------------------------------------------------------------------------------------------
			$overrideMissing="";
			if (MYOWNRES_CHECK_OVERIDES && $files = self::scan_dir(_PS_MODULE_DIR_ . self::$name . "/override/classes")) {
				foreach ($files as $file) {
					if (stripos($file, '.php') !== false && (stripos($file, '/') === false or _PS_VERSION_ >= "1.5.0.0")) { //
						if (!file_exists(_PS_ROOT_DIR_."/override/classes/".$file)
							or md5_file(_PS_MODULE_DIR_ . self::$name . "/override/classes/".$file)!=md5_file(_PS_ROOT_DIR_."/override/classes/".$file)) {
							 if ($overrideMissing!='') $overrideMissing.=', ';
							 $overrideMissing.=str_replace("_","",$file);
						}
					}
				}
			}
			if (MYOWNRES_CHECK_OVERIDES && $isproversion && file_exists(_PS_MODULE_DIR_ . self::$name . "/override/classes/Tools.ph_") && (
	       		!file_exists(_PS_ROOT_DIR_."/override/classes/Tools.php") 
	       		 or md5_file(_PS_MODULE_DIR_ . self::$name . "/override/classes/Tools.ph_")!=md5_file(_PS_ROOT_DIR_."/override/classes/Tools.php") ) )
	       		 {
		       		 if ($overrideMissing!='') $overrideMissing.=', ';
		             $overrideMissing.='Tools.php';
	       }
			if (MYOWNRES_CHECK_OVERIDES && _PS_VERSION_ < "1.5.0.0" && (
				!file_exists(_PS_ROOT_DIR_."/override/classes/PDF.php") 
				or md5_file(_PS_MODULE_DIR_ . self::$name . "/override/classes/PDF.ph_")!=md5_file(_PS_ROOT_DIR_."/override/classes/PDF.php") ) )
				{
					if ($overrideMissing!='') $overrideMissing.=', ';
					$overrideMissing.='PDF.php';
			}
			if (MYOWNRES_CHECK_OVERIDES && _PS_VERSION_ >= "1.7.0.0" && (
				!file_exists(_PS_ROOT_DIR_."/override/controllers/front/CartController.php") 
				or md5_file(_PS_MODULE_DIR_ . self::$name . "/override/controllers/front/CartController.ph_")!=md5_file(_PS_ROOT_DIR_."/override/controllers/front/CartController.php") ) )
				{
					if ($overrideMissing!='') $overrideMissing.=', ';
					$overrideMissing.='CartController.php';
			}
	       
	       if ($overrideMissing!='') $content.= myOwnUtils::displayError('<b>'.$obj->l('Overrides to update', 'controller').'</b> : '.$overrideMissing.' &nbsp;<i>'.$obj->l('A write access to /override/classes folder is needed', 'controller').'</i><input style="float:right;text-align:left;padding-left:22px;background:#DFDFDF 3px 3px no-repeat url(\'../img/admin/duplicate.gif\')" type="button" name="help" class="configbutton" value="'.$obj->l('Update overrides', 'controller').'" onclick="window.location=\''.self::getConfUrl($myOwnResTab,'&overrideupdate=1').'\';">');
	       
	       if ($obj->isInstalled($obj->name) && !isset($_GET['dbupdate'])) {
	       		$updatedb = self::checkDatabase($obj);
			    if (count($updatedb)>0) {
	        	       $content.= myOwnUtils::displayError('<b>'.$obj->l('Database to update', 'controller').'</b><input style="float:right;text-align:left;padding-left:22px;background:#DFDFDF 3px 3px no-repeat url(\'../img/admin/duplicate.gif\')" type="button" name="help" class="configbutton" value="'.$obj->l('Update database', 'controller').'" onclick="window.location=\''.self::getConfUrl($myOwnResTab,'&dbupdate=1').'\';">');
	            }
	        }
	        $is_productlist_hook=(_PS_VERSION_ >= '1.7.0.0' ? true : false);
	        $is_productlist_filt=(_PS_VERSION_ >= '1.7.0.0'  ? true : false);
	        if (_PS_VERSION_ < '1.7.0.0' && $productlist_tpl=file_get_contents(_PS_THEME_DIR_.'product-list.tpl')) {
		        $is_productlist_hook = (stripos($productlist_tpl, "displayProductListFunctionalButtons"));
		        $is_productlist_filt = (stripos($productlist_tpl, '{if $page_name != \'index\'}'));
		        if ($is_productlist_filt) {
			        $test= explode('{if $page_name != \'index\'}', $productlist_tpl);
			        if (explode('-->', $test[1])>2)
						$is_productlist_filt = false;
		        }
			}
			if (_PS_VERSION_ < '1.7.0.0' && $is_productlist_filt)
				$content.= myOwnUtils::displayWarn('<b>'.$obj->l('A hook is filtered').'</b> '.$obj->l('Remove line with {if $page_name != \'index\'} and corresponding line with {/if} underneath from template product-list.tpl', 'controller'));
			if (!$is_productlist_hook && !Configuration::get('MYOWNRES_PS15_LIST'))
				$content.= myOwnUtils::displayWarn($obj->l('Template hook displayProductListFunctionalButtons is missing in template product-list.tpl', 'controller').'</b><input style="float:right;text-align:left;padding-left:22px;background:#DFDFDF 3px 3px no-repeat url(\'../img/admin/duplicate.gif\')" type="button" name="help" class="configbutton" value="'.$obj->l('Activate compatibility mode', 'controller').'" onclick="window.location=\''.self::getConfUrl($myOwnResTab,'&compatibility=1').'\';">');
			if ($is_productlist_hook && Configuration::get('MYOWNRES_PS15_LIST'))
				$content.= myOwnUtils::displayWarn($obj->l('Template hook displayProductListFunctionalButtons is present but compatibility mode is enabled', 'controller').'</b><input style="float:right;text-align:left;padding-left:22px;background:#DFDFDF 3px 3px no-repeat url(\'../img/admin/duplicate.gif\')" type="button" name="help" class="configbutton" value="'.$obj->l('Disable compatibility mode', 'controller').'" onclick="window.location=\''.self::getConfUrl($myOwnResTab,'&compatibility=0').'\';">');
	        
	        //my-account > my reservations page rewrite
	        $query = "SELECT * FROM `"._DB_PREFIX_."meta` WHERE `page` LIKE 'module-myownreservations-reservations';";
			$metas = Db::getInstance()->ExecuteS($query);
			if (count($metas)==0) {
				$query = "INSERT INTO `"._DB_PREFIX_."meta` (`page`) VALUES ('module-myownreservations-reservations');";
				Db::getInstance()->Execute($query);
				$inserted = Db::getInstance()->ExecuteS("SELECT LAST_INSERT_ID() AS ID;");
				$metaId = $inserted[0]['ID'];
				
				$languages = Language::getLanguages();
				$id_shop = myOwnUtils::getShop();
				if (!$id_shop) $id_shop=1;
				foreach ($languages as $language) {
					$resa_label='reservations';
					if ($language['iso_code']=='fr') $resa_label = 'réservations';
					if ($language['iso_code']=='es') $resa_label = 'reservas';
					$query="INSERT INTO `"._DB_PREFIX_."meta_lang` (`id_meta`, `id_shop`, `id_lang`, `title`, `description`, `keywords`, `url_rewrite`) VALUES ('".$metaId."', '".$id_shop."', '".$language['id_lang']."', '".$resa_label."', '".$resa_label."', '".$resa_label."', 'reservations');";
					Db::getInstance()->Execute($query);
				}
				if (_PS_VERSION_ >= "1.6.1.0" && _PS_VERSION_ < "1.7.0.0") {
					$themes = Theme::getAllThemes();
					foreach ($themes as $theme) {
						$query="INSERT INTO `"._DB_PREFIX_."theme_meta` (`id_theme`, `id_meta`, `left_column`, `right_column`) VALUES ('".$theme->id."', '$metaId', '0', '0');";
						Db::getInstance()->Execute($query);
					}
				}
			}
       }
	   return $content;
	}
	
	public static function getModuleHeader($obj, $cookie) {
		$content = "";
		if (file_exists(_PS_ROOT_DIR_."/override/classes/Tab.php"))
			return '';
		$employee = new Employee($cookie->id_employee);
		$obj->checkIsProVersion();
	
	    $adminRights = Profile::getProfileAccess($employee->id_profile, Tab::getIdFromClassName('adminmyownreservations'));
	    $rulesRights = Profile::getProfileAccess($employee->id_profile, Tab::getIdFromClassName('AdminReservationRules'));
	    $configure = $obj->getPermission('configure', $employee);
		
		$level="";
		$version = self::checkVersion($obj);
		$checkSetup = self::configCheckSetup($obj);
		$checkRules = self::configCheckRules($obj);
		if (!$obj->isKeyValid && $obj->isLocal) $level="warn";
		if ($checkSetup!="") $level="err";
		if (!$obj->isKeyValid && !$obj->isLocal) $level="err";
		
		$myOwnResTab = Tools::getValue('myOwnResTab');
		$ctrl = strtolower(Tools::getValue('controller'));
		if (_PS_VERSION_ >= "1.6.0.0") $btnClass="module_bar16";
		else $btnClass="module_bar15";
		$content .= '
		<script language="javascript"> 
			$("table").eq(0).next().remove();
			$("table").eq(0).remove();
			$(".page-head").css("box-shadow", "none");
		</script>
		<style type="text/css">
			#desc-adminmodules-help {display:none;}
		</style>
		<div class="leadin" style="'.(_PS_VERSION_ >= "1.6.0.0" ? 'margin-left:-10px;margin-right:-10px;margin-top:-28px;margin-bottom:-10px' : '').'">
			<div class="bloc-leadin">
				<div id="container_category_tree" class="module_head_bloc" style="'.(_PS_VERSION_ < "1.6.0.0" ? 'background:#FFF' : 'background:rgb(229, 229, 229)').' url(../modules/'.$obj->name.'/'.($obj->isProVersion ? 'pro/' : 'img/').'logo.png) no-repeat 3px 4px;'.(_PS_VERSION_ >= "1.6.0.0" ? 'margin-bottom:20px;padding:3px;padding-top:6px;padding-left:60px;height:70px;-webkit-font-smoothing: antialiased;overflow:hidden' : 'margin-top:10px;').'">
					<div style="float:left;height:30px">
						<h2 class="module_head_title">&nbsp;&nbsp;'.$obj->l('myOwnReservations', 'controller').' '.($obj->isProVersion ? "PRO" : "").'</h2>
					</div>
					<div tets="category-filter" style="float:right;text-align:right;margin-top:-3px;background:url('.__PS_BASE_URI__.'modules/myownreservations/img/labulle-logo-footer.png) no-repeat top right;">
						<a href="http://www.labulle.net/support/aid/" target="_blank" style="display:block;width:200px;height:35px"></a>
						
					</div>
					<div style="float:left">
						<table border="0" cellspacing="0" style="border-collapse:collapse;">
							<tr>
							<td width="30" class="module_bar_logo">&nbsp;</td>
							<td class="module_bar">
								<a class="'.$btnClass.' module_bar'.(($myOwnResTab=="products" or ($myOwnResTab=='' && $ctrl=="adminreservationrules")) ? '_selected' : ($rulesRights["view"]? '' : '_disabled') ).'" style="background-color:'.($checkRules != '' ? '#BDE5F8' : '').';font-weight:normal;" '.($rulesRights["view"] ? 'href="'.self::getConfUrl('products').'"' : '').'><img src="../modules/'.$obj->name.'/img/ressources.png" >'.ucfirst(myOwnLang::$objects[MYOWN_OBJ::RESVRULE]).($checkRules != '' ? '<img width="24" height="24" style="margin-left:6px;margin-right:-5px;margin-top:-2px" src="../modules/'.$obj->name.'/img/icon-info.png">':'').'</a>
							</td>'.($obj->isProVersion ? '
							<td class="module_bar">
								<a class="'.$btnClass.' module_bar'.($myOwnResTab=="timeslots" ? '_selected' : ($configure ? '' : '_disabled') ).'" href="'.self::getConfUrl('timeslots').'"><img src="../modules/'.$obj->name.'/img/clock.png" >'.ucfirst(myOwnLang::$objects[MYOWN_OBJ::TIMESLOT]).'</a>
							</td>':'').'
							<td class="module_bar">
								<a class="'.$btnClass.' module_bar'.($myOwnResTab=="availability" || $ctrl=="adminreservationavailability" ? '_selected' : ($configure ? '' : '_disabled') ).'" href="'.self::getConfUrl('availability').'"><img src="../modules/'.$obj->name.'/img/timeSlot.png" >'.ucfirst(myOwnLang::$objects[MYOWN_OBJ::AVAILABILITY]).'</a>
							</td>
							<td class="module_bar">
								<a class="'.$btnClass.' module_bar'.($myOwnResTab=="pricesrules" || $ctrl=="adminreservationprices" ? '_selected' : ($configure ? '' : '_disabled') ).'" href="'.self::getConfUrl('pricesrules').'"><img src="../modules/'.$obj->name.'/img/pricesrules.png" >'.ucfirst(myOwnLang::$objects[MYOWN_OBJ::PRICERULE]).'</a>
							</td>
							';
							
							foreach ($obj->_extensions->getExtByType(extension_type::OBJECT) as $ext)
								$content .= '
							<td class="module_bar">
								<a class="'.$btnClass.' module_bar'.($myOwnResTab==$ext->name ? '_selected' : ($configure ? '' : '_disabled') ).'" href="'.self::getConfUrl($ext->name).'"><img src="../modules/'.$obj->name.'/extensions/'.$ext->name.'.png" >'.$ext->title.'</a>
							</td>';
							
							$content .= '
							<td class="module_bar">
								<a class="'.$btnClass.' module_bar'.(($myOwnResTab=="advanced" or ($myOwnResTab=="" && $ctrl=="adminmodules") or strtolower($ctrl)=="adminreservationconfig") ? '_selected' : ($configure ? '' : '_disabled') ).'" style="background-color:'.($level == 'err' ? '#FFBABA' : ($level == 'warn' ? '#FEEFB3' : '')).'" href="'.self::getConfUrl('advanced').'"><img src="../modules/'.$obj->name.'/img/advanced.png" >'.$obj->l('Configuration', 'controller').($level != '' ? '<img width="24" height="24" style="margin-left:6px;margin-right:-5px;margin-top:-2px" src="../modules/'.$obj->name.'/img/icon-'.($level == 'err' ? 'cancel' : 'attention').'.png">':'').'</a>
							</td>
							'.($obj->isProVersion ? '
							<td class="module_bar">
								<a class="'.$btnClass.' module_bar'.($myOwnResTab=="stock" ? '_selected' : ($configure ? '' : '_disabled') ).'" href="'.self::getConfUrl('stock').'"><img src="../modules/'.$obj->name.'/img/calendar-stock.png" >'.ucfirst(myOwnLang::$object[MYOWN_OBJ::STOCK]).'</a>
							</td>
							':'').'
							<td class="module_bar">
								<a class="'.$btnClass.' module_bar'.($ctrl=="adminmyownreservations" ? '_selected' : '' ).'" '.($adminRights["view"] ? 'href="'.myOwnReservationsController::getAdminUrl('').'"' : '').'><img src="../modules/'.$obj->name.'/img/calendar-admin.png" >'.$obj->l('Administration', 'controller').'</a>
							</td>
							<td class="module_bar" style="'.($version!=null && $version->update ? 'background-color:#FEEFB3' : '').'"><a class="'.$btnClass.' module_bar" onclick="$(\'#myownversion\').show()" style="font-weight:normal;padding-right:0px;line-height:22px;text-align:center;'.($version!=null && $version->update ? 'background-color:#FEEFB3;' : 'background-color:#FEEFB3;margin-top:3px').'">v'.$obj->version.'&nbsp;'.($version!=null && $version->update != '' ? '<img width="24" height="24" style="margin-left:6px;margin-right:-5px;margin-top:-2px" src="../modules/'.$obj->name.'/img/icon-attention.png">':'').'</a></td>
							</tr>
						</table>
					</div>
					<div style="clear:both"></div>
				</div>
				'.(($myOwnResTab=="products" or ($myOwnResTab=='' && $ctrl=="adminreservationrules")) ? '<div style="margin-left:10px;margin-right:10px;">'.$checkRules.'</div>' : '').'
			</div>
		</div>';
		if ($version!=null && $version->update) $content.= '<div id="myownversion" style="display:none">'.myOwnUtils::displayWarn($obj->l('New version available', 'controller').' : '.$version->version.' '.(trim($version->details)!='' ? '&nbsp;&nbsp;&nbsp;&nbsp;<i>'.$version->details.'</i>&nbsp;&nbsp;&nbsp;&nbsp;<a href="'.$version->url.'" target="_blank">'.$obj->l('More info', 'controller').'</a> ' :'').' '.(trim($version->donwload)!='' ? '&nbsp;&nbsp;<a href="'.$version->donwload.'" target="_blank">'.$obj->l('Download', 'controller').'</a>' : '')).'</div>';
		return $content;
	}
	
	public static function getMenuHeader($obj, $parent, $title, $buttons, $item='') {
		$content = '';
		$content .= '
		<div class="toolbar-placeholder">
			<div class="toolbarBox toolbarHead" style="padding: 5px 0;">
				'.$buttons.'
				<div class="pageTitle">
					<h3>
						<span id="current_obj" style="font-weight: normal;">
								<span class="breadcrumb item-0 ">'.$parent.'<img alt="&gt;" style="margin-right:5px" src="../img/admin/separator_breadcrumb.png"></span>
								<span class="breadcrumb item-1 ">'.$title.''.($item!='' ? '<img alt="&gt;" style="margin-right:5px" src="../img/admin/separator_breadcrumb.png"></span><span class="breadcrumb item-2 ">'.$item : '').'</span>
						</span>
						
					</h3>
				</div>
			</div>
		</div>';
		return $content;
	}
	
    public static function getButtonsList($obj, $id)
	{
		global $cookie;
		$buttons=array();
		$tab = Tools::getValue('myOwnResTab');
		$ctrl = Tools::getValue('controller');
		if (stripos($ctrl, 'admin')!==false && $tab=='')
			$tab = str_ireplace('admin', '', $ctrl);
			
		$field='';
		if ($tab=='timeslots')
			$field = 'TimeSlot';
		if ($tab=='pricesrules') 
			$field = 'Pricesrule';
		if ($tab=='availability') 
			$field = 'Availability';
		if (strtolower($ctrl)=='adminreservationrules') {
			$tab='products';
			$field = 'Product';
		}
		if (strtolower($ctrl)=='adminreservationprices') {
			$tab='pricesrule';
			$field = 'Pricesrule';
		}
		
		$isproversion=is_file(_PS_MODULE_DIR_ ."myownreservations/pro/products.php");
		$config=($tab=="advanced" or ($tab=="" && strtolower($ctrl)=="adminmodules") or strtolower($ctrl)=="adminreservationconfig");
		
		if (isset($_GET["editStock"])) {
			
			$buttons['return'] = array(
				'desc' => $obj->l('Return to list', null, null, false),
				'href' => myOwnReservationsController::getConfUrl('stock'),
				'icon' => 'process-icon-back'
			);
			
			$buttons['save'] = array(
				'desc' => ucfirst(myOwnLang::$actions[MYOWN_OPE::SAVE]),
				'js' => '$(\'#displayEditStock\').submit();',
				'icon' => 'process-icon-save'
			);
		} else if (((isset($_GET['edit'.$field]) && $_GET['edit'.$field]!="") or ($tab=="products" && !$isproversion) ) && !$config) {
		
			$buttons['save'] = array(
				'desc' => ucfirst(myOwnLang::$actions[MYOWN_OPE::SAVE]),
				'js' => "submitMyOwnForm(false, '".$field."', ".intval($id).");",
				'icon' => 'process-icon-save'
			);
			if ($id>-1) {
				
				$buttons['return'] = array(
					'desc' => $obj->l('Return to list', 'controller'),
					'href' => ($isproversion && $tab=='timeslots' ? myOwnReservationsController::getConfUrl('products', '&editProduct='.Tools::getValue('fromProduct')).'#key_tab=editTimeslotsTab' : myOwnReservationsController::getConfUrl($tab)),
					'icon' => 'process-icon-back'
				);
				$buttons['delete'] = array(
					'desc' => ucfirst(myOwnLang::$actions[MYOWN_OPE::DELETE]),
					'href' => myOwnReservationsController::getConfUrl($tab.'&delete'.$field.'='.$id),
					'icon' => 'process-icon-delete'
				);
				$buttons['savestay'] = array(
					'desc' => ucfirst(myOwnLang::$actions[MYOWN_OPE::SAVESTAY]),
					'js' => "submitMyOwnForm(true, '".$field."', ".intval($id).");",
					'icon' => 'process-icon-save-and-stay'
				);
			}
		} else if (!$config) {
				if ($tab=="availability")  {
					$buttons['availability'] = array(
						'desc' => ucfirst(myOwnLang::$actions[MYOWN_OPE::CREATE]),
						'js' => '$(\'#addAvailability\').show();',
						'icon' => 'process-icon-new'
					);
					$buttons['delete'] = array(
						'desc' => ucfirst(myOwnLang::$actions[MYOWN_OPE::DELETE]),
						'js' => "$('#deleteAvailability').submit()",
						'icon' => 'process-icon-delete'
					);

				} else {
					$buttons['new'] = array(
						'desc' => ucfirst(myOwnLang::$actions[MYOWN_OPE::CREATE]),
						'icon' => 'process-icon-new'
					);
					if ($tab=='stock')
						$buttons['new']['js'] = "$('#addStock').toggle('slow')";
					else
						$buttons['new']['href'] = myOwnReservationsController::getConfUrl($tab, '&edit'.$field.'=0');
				}
				$buttons['export'] = array(
					'desc' => ucfirst(myOwnLang::$actions[MYOWN_OPE::EXPORT]),
					'href' => 'ajax-tab.php?tab=adminmyownreservations'.'&token='.Tools::getAdminToken('adminmyownreservations'.intval(Tab::getIdFromClassName('adminmyownreservations')).intval($cookie->id_employee)).'&action=exportConfig&myOwnResTab='.$tab,
					'icon' => 'process-icon-export'
				);
		} else {
			if ($field=='Stock') 
					$buttons['export'] = array(
						'desc' => ucfirst(myOwnLang::$actions[MYOWN_OPE::EXPORT]),
						'href' => 'ajax-tab.php?tab=admin'.$obj->name.'&token='.Tools::getAdminToken('admin'.$obj->name.intval(Tab::getIdFromClassName('admin'.$obj->name)).intval($cookie->id_employee)).'&action=exportConfig&myOwnResTab='.$tab,
						'icon' => 'process-icon-export'
					);
					
			$buttons['save'] = array(
				'desc' => ucfirst(myOwnLang::$actions[MYOWN_OPE::SAVE]),
				'js' => "submitMyOwnForm(true, 'Config');",
				'icon' => 'process-icon-save'
			);
					
		}
		$buttons['help'] = array(
			'desc' => ucfirst(myOwnLang::$actions[MYOWN_OPE::HELP]),
			'js' => 'toogleHelp();$(\'.hint\').toggle();$(\'.alert-info\').toggle();$(\'.hidehint\').toggle();',
			'icon' => 'process-icon-help'
		);
		return $buttons;
	}

    public static function _getContent($cookie, $obj) {
    	global $rights;
    	$langue=$cookie->id_lang;
    	$htmlOut="";
		$errors = array();
		$id=0;
		$operation="";
		$errorCount = 0;
		$warn="";
		self::_construire($obj);
		$employee = new Employee($cookie->id_employee);
		$rights = Profile::getProfileAccess($employee->id_profile, Tab::getCurrentTabId());
		$htmlOut .= MyOwnReservationsUtils::displayIncludes();
		$htmlOut .= self::getModuleHeader($obj, $cookie);
		
	//==============================================================================================================================================
	// CHECK FORM PARRAMS
	//==============================================================================================================================================

        $tserrors = myOwnReservationsTimeslotsController::check($obj);
        if ($tserrors!=array())
        	$errors += $tserrors;
        	
		foreach($_POST AS $key => $value) {


			
			if ($key=="reservationStartNb" ) {
					if (preg_match( "@^\d*$@i" , $value)== 0 OR intval($value) > 999 )
						$errors[sizeof($errors)+1] = $obj->l('A reservation start delay must be an integer of 3 digit max', 'controller');
					if ($value==0)
						$errors[sizeof($errors)+1] = $obj->l('A reservation start delay must be upper than 0', 'controller');
			}
			if ($key=="reservationPeriod" ) {
					if (preg_match( "@^\d*$@i" , $value)== 0 OR intval($value) > 999 )
						$errors[sizeof($errors)+1] = $obj->l('A reservation Period must be an integer of 3 digit max', 'controller');
			}
			if ($key=="productReservationMinLength" && Tools::getValue("reservationLengthControl")==reservation_length::FIXED) {
					if (preg_match( "@^\d*$@i" , $value)== 0 OR intval($value) > 999 OR intval($value) ==0 )
						$errors[sizeof($errors)+1] = $obj->l('A reservation min length must be an integer of 3 digit max and greater than 0', 'controller');
			}
			if ($key=="productReservationBetweenShift" ) {
					if (preg_match( "@^\d*$@i" , $value)== 0 OR intval($value) > 999 )
						$errors[sizeof($errors)+1] = $obj->l('A shift length between reservation must be an integer of 3 digit max', 'controller');
			}
			if ($key=="productReservationMaxLength" ) {
					if (preg_match( "@^\d*$@i" , $value)== 0 OR intval($value) > 999 )
						$errors[sizeof($errors)+1] = $obj->l('A reservation max length must be an integer of 3 digit max', 'controller');
					elseif (intval($_POST["productReservationMinLength"]) > intval($value))
						$errors[sizeof($errors)+1] = $obj->l('A reservation min length cannot be upper than a reservation max length', 'controller');
			}
			if ($key == "pricesruleAmount") {
					if (preg_match( "@^\d*(\.\d*)?$@i" , $value)== 0 ) $errors[sizeof($errors)+1] = $obj->l('A amount must be formatted with a dot', 'controller');
					if (intval($value)>=100000000) $errors[sizeof($errors)+1] = $obj->l('A amount must be less than 100000000', 'controller');
					
			}
			if ($key == "qtyCapacity") {
					if (preg_match( "@^\d*$@i" , $value)== 0 OR intval($value) > 999  OR intval($value) == 0)
						$errors[sizeof($errors)+1] = $obj->l('An capacity must be an integer of 3 digits max and greater than 0', 'controller');
					
			}
			if ($key == "pricesruleRatio") {
					if (preg_match( "@^\d*(\.\d*)?$@i" , $value)== 0 ) $errors[sizeof($errors)+1] = $obj->l('A ratio must be formatted with a dot', 'controller');
					if (intval($value)>=1000) $errors[sizeof($errors)+1] = $obj->l('A ratio must be less than 1000', 'controller');
			}
			if ($key == "pricesruleName" AND (strlen($value)>64 or (strlen($value)==0 && isset($_POST["pricesruleVisible"])))) {
				$errors[sizeof($errors)+1] = $obj->l('A price rule name cannot be empty if visible and must be smaller than 64 chars', 'controller');
			}
			if ($key=="resaDepositAmount" ) {
					if (preg_match( "@^\d*$@i" , $value)== 0 OR intval($value) > 10000000 )
						$errors[sizeof($errors)+1] = $obj->l('A amount must be less than 10000000', 'controller');
			}
			if ($key=="resaAdvanceRate" ) {
					if (preg_match( "@^\d*$@i" , $value)== 0 OR intval($value) > 100 )
						$errors[sizeof($errors)+1] = $obj->l('An order advance cannot be greater than 100', 'controller');
			}
			if ($key=="resaAdvanceAmount" ) {
					if (preg_match( "@^\d*$@i" , $value)== 0 OR intval($value) > 10000000 )
						$errors[sizeof($errors)+1] = $obj->l('A amount must be less than 10000000', 'controller');
			}
			if ($key=="pricesruleParamMin" or $key=="pricesruleParamMax" ) {
					if (preg_match( "@^\d*$@i" , $value)== 0 OR intval($value) > 999 )
						$errors[sizeof($errors)+1] = $obj->l('A length must be an integer of 3 digit max', 'controller');
					elseif ($key=="pricesruleParamMax" && intval($_POST["pricesruleParamMin"]) > intval($value))
						$errors[sizeof($errors)+1] = $obj->l('A min length cannot be upper than a max length', 'controller');
			}
			if (strpos($key,"pricesruleParamEnd")!==false) {
				if (strlen($_POST["pricesruleParamEnd"]) > 0 OR strlen($_POST["pricesruleParamEnd"])>0) {
						if (preg_match( "@^([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})$@i" , $value)== 0 ) {
							$errors[sizeof($errors)+1] = $obj->l('A period bound must be formated like YYYY-MM-DD', 'controller'); 
						}
				}
			}
			
			
			if (strpos($key,"pricesruleParamStart")!==false) {
				if (strlen($_POST["pricesruleParamStart"]) > 0 OR strlen($_POST["pricesruleParamStart"])>0) {
						if (preg_match( "@^([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})$@i" , $value)== 0 ) {
							$errors[sizeof($errors)+1] = $obj->l('A period bound must be formated like YYYY-MM-DD', 'controller'); 
						} else {
							if (strtotime($_POST["pricesruleParamStart"]) > strtotime($_POST["pricesruleParamEnd"])) {
								$errors[sizeof($errors)+1] = $obj->l('The end date date must be later or equal than the start date', 'controller');
							}
						}
				}
			}
			
/*
			if (strpos($key,"holidayStart")!==false && strpos($key,"Timeslot")===false) {
				if (strlen($_POST["holidayStart"]) > 0 OR strlen($_POST["holidayStart"])>0) {
      				if (preg_match( "@^([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})$@i" , $value)== 0 ) {
          				$errors[sizeof($errors)+1] = $obj->l('A holiday bound must be formated like YYYY-MM-DD', 'controller'); 
          			} else {
     					if (strtotime($_POST["holidayStart"]) > strtotime($_POST["holidayEnd"])) {
           					$errors[sizeof($errors)+1] = $obj->l('The end date date must be later or equal than the start date', 'controller');
          				} else if ($_POST["holidayType"]=="0") {
          					$resas = myOwnResas::getReservationsForProductOnPeriod((isset($_POST["holidayProduct"])?$_POST["holidayProduct"]:-1), (isset($_POST["holidayProductAttribute"])?$_POST["holidayProductAttribute"]:-1), strtotime($_POST["holidayStart"]), strtotime($_POST["holidayEnd"]));
           					if (count($resas)>0) $warn .= myOwnUtils::displayWarn($obj->l('You must reschedule the', 'controller').' '.count($resas).' '.$obj->l('reservation(s) planned on holiday, between the', 'controller').' '.$_POST["holidayStart"].' '.$obj->l('and').' '.$_POST["holidayEnd"] );
                        }
      				}
				}
			}
*/
					
			if ($key=="datatable" && isset($_FILES["file"]) && $_FILES["file"]["size"]>0) {
				if ($_POST['datatable']!='') {
			 		if (stripos($_FILES["file"]["name"], ".csv")!== false) {
			 			if ($_FILES["file"]["error"] > 0)
			 		  		$errors[] = $obj->l('File error', 'controller').' : '.$_FILES["file"]["error"];
			 		} else $errors[] = $obj->l('Please upload a CSV file', 'controller');
			 	} else {
			 		$errors[] = $obj->l('Please select a data type and a file', 'controller');
			 	}
			}
		
		}

	//==============================================================================================================================================
	// UPDATE SETTINGS OR BDD
	//==============================================================================================================================================
	$insertedId=0;

		if(sizeof($errors) < 1) {
			$plur=false;
			$checkDatabase=array();
			if ($obj->isInstalled($obj->name))
				$checkDatabase=self::checkDatabase($obj);
			//compatibility
			//-----------------------------------------------------------------------------------------------------------------
			if(isset($_GET['compatibility']))
				Configuration::updateValue('MYOWNRES_PS15_LIST', $_GET['compatibility']);
			// DATABASE UPDATE
			//-----------------------------------------------------------------------------------------------------------------
			if(isset($_GET['dbupdate'])) {
				$fr = Language::getIdByIso('fr');
				$en = Language::getIdByIso('en');
				$objet = $obj->l('Database', 'controller');
				$operation = myOwnLang::getOperation($obj, MYOWN_OPE::UPDATE, false);
				$result=true;
				foreach ($checkDatabase as $dbtoup) {
					switch ($dbtoup) {
					    case 'availability_start_timeslot':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_availability` ADD  `start_timeslot` int(11) NOT NULL");
					        break;
					    case 'availability_end_timeslot':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_availability` ADD  `end_timeslot` int(11) NOT NULL");
					        break;
					    case 'availability_quantity':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_availability` ADD `quantity` int(10) NOT NULL");
					        break;
					    case 'availability_id_family':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_availability` ADD `id_family` int(11) NOT NULL");
					        break;
					    case 'availability_id_object':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_availability` ADD `id_object` int(11) NOT NULL");
					        break;
					    case 'products_reservationSelType':
							$result &= Db::getInstance()->Execute("ALTER TABLE `". _DB_PREFIX_ ."myownreservations_products` ADD `reservationSelType` tinyint(1) NOT NULL");
							$result &= Db::getInstance()->Execute("ALTER TABLE `". _DB_PREFIX_ ."myownreservations_products` ADD `reservationSelPlanning` tinyint(1) NOT NULL");
							$result &= Db::getInstance()->Execute("ALTER TABLE `". _DB_PREFIX_ ."myownreservations_products` ADD `reservationSelParams` varchar(32) NOT NULL");
					        break;
					    case 'products_qtyParams':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_products` ADD `qtyParams` varchar(32) NOT NULL DEFAULT '1'");
					        break;
					    case 'products_email':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_products` ADD `email` varchar(128) NOT NULL");
					        break;
					    case 'products_ids_users':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_products` ADD `ids_users` varchar(64) NOT NULL");
					        break;
					    case 'products_ids_categories':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_products` ADD `ids_categories` text");
					        break;
					    case 'products_ids_notifs':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_products` ADD `ids_notifs` varchar(64) NOT NULL");
					        $result &= Db::getInstance()->Execute("UPDATE `". _DB_PREFIX_ ."myownreservations_products` SET `ids_notifs` = '1'");
					        break;
					    case 'products_ids_options':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_products` ADD `ids_options` varchar(64) NOT NULL");
					        break;
					     case 'products_id_shop':
					     	$result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_products` ADD  `id_shop` int(10) NOT NULL");
					        break;
					    case 'products_ids_products':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_products` ADD `ids_products` text");
				        	$result &= Db::getInstance()->Execute("UPDATE `". _DB_PREFIX_ ."myownreservations_products` SET `ids_products` = `ids_categories`");
				        	$result &= Db::getInstance()->Execute("UPDATE `". _DB_PREFIX_ ."myownreservations_products` SET `ids_categories` = ''");
					        break;
					    case 'products_productOccupyParams':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_products` ADD `productOccupyParams` varchar(32) NOT NULL");
					        foreach ($obj->_products->list as $mainProduct)
					        	$result &= Db::getInstance()->Execute("UPDATE `". _DB_PREFIX_ ."myownreservations_products` SET `productOccupyParams` =  '0;0;".intval($mainProduct->_optionIgnoreAttrStock)."' WHERE `id_product`=".$mainProduct->sqlId.";");
					        break;
					    case 'products_productType':
					    	$result &= Db::getInstance()->Execute("ALTER TABLE `". _DB_PREFIX_ ."myownreservations_products` ADD `productType` tinyint(1) NOT NULL");
					    	$result &= Db::getInstance()->Execute("ALTER TABLE `". _DB_PREFIX_ ."myownreservations_products` CHANGE  `reservationSelParams`  `reservationSelParams` VARCHAR( 64 ) NOT NULL");
					    	foreach ($obj->_products->list as $mainProduct) {
					    		$type=product_type::PRODUCT;
					    		if ($mainProduct->_optionIgnoreAttrStock) $type=product_type::VIRTUAL;
					    		else if ($mainProduct->_qtyCapacity>1) $type=product_type::SHARED;
					        	$result &= Db::getInstance()->Execute("UPDATE `". _DB_PREFIX_ ."myownreservations_products` SET `productType` = '".$type."' WHERE `id_product`=".$mainProduct->sqlId.";");
					        }
					    	break;
					    case 'products_depositAdvanceParams':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_products` ADD `depositAdvanceParams` varchar(32) NOT NULL");
					        $result &= Db::getInstance()->Execute("UPDATE `". _DB_PREFIX_ ."myownreservations_products` SET `depositAdvanceParams` = '0;0;".intval(Configuration::get('MYOWNRES_ORDER_ADVANCE')).";".Configuration::get('MYOWNRES_ORDER_FIXEDADVANCE')."'");
					        break;
					        
					    case 'products_assignment':
					     	$result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_products` ADD  `assignment` int(10) NOT NULL");
					        break;
					        
					    case 'pricesrule_impact':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_pricesrule` ADD  `impact` tinyint(2) NOT NULL");
					        break;
					    case 'pricesrule_overall':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_pricesrule` ADD  `overall` tinyint(1) NOT NULL");
					        break;
					    case 'pricesrule_id_family':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_pricesrule` ADD  `id_family` int(11) NOT NULL");
					        break;
					    case 'pricesrule_style':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_pricesrule` ADD `style` varchar(255) NOT NULL");
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_pricesrule` CHANGE  `product`  `product` TEXT NOT NULL");
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_pricesset` CHANGE  `period`  `period` int(2) NOT NULL");
					        break;
					    case 'reservation_id_order':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_reservation` ADD  `id_order` int(11) NOT NULL");
					        $result &= Db::getInstance()->Execute("UPDATE `". _DB_PREFIX_ ."myownreservations_reservation` SET  `id_order` =  `id_reservation`;");
					        //$result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_reservation` ADD `id_reservation` int(11) NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (  `id_reservation` )
					        break;
					    case 'reservation_reference':
                    		$query = 'ALTER TABLE  `'._DB_PREFIX_.'myownreservations_reservation` ADD  `reference` varchar(8) NOT NULL';
                    		Db::getInstance()->Execute($query);
                    		$req = 'SELECT * FROM `'._DB_PREFIX_.'myownreservations_reservation` WHERE `reference`  = ""';
        					$orders = Db::getInstance()->ExecuteS($req);
        					foreach($orders as $order) {
        						$ref = strtoupper(Tools::passwdGen(8, 'NO_NUMERIC'));
        						$query = 'UPDATE '._DB_PREFIX_.'myownreservations_reservation SET reference = "'.$ref.'" WHERE id_reservation = "'.$order["id_reservation"].'";';
        						Db::getInstance()->Execute($query);
        					}
        					
                    		break;
					    case 'reservation_stock':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_reservation` ADD  `stock` text NOT NULL");
					        break;
					    case 'reservation_quantity_refunded':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_reservation` ADD `quantity_refunded` int(10) NOT NULL");
					        break;
					    case 'reservation_advance':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_reservation` ADD `advance` decimal(20,6) NOT NULL");
					        break;
					    case 'reservation_start_time':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_reservation` ADD `start_time` TIME NOT NULL");
					        break;
					    case 'reservation_end_time':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_reservation` ADD `end_time` TIME NOT NULL");
					        break;
					    case 'timeslot_id_product':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_timeslot` ADD `id_product` int(10) NOT NULL");
					        break;
					    case 'timeslot_week_type':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_timeslot` ADD `week_type` tinyint(10) NOT NULL");
					        break;
					    case 'cartproducts_id_customization':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_cartproducts` ADD `id_customization` int(11) NOT NULL");
					        break;
					    case 'cartproducts_id_object':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_cartproducts` ADD `id_object` int(11) NOT NULL");
					        break;
					    case 'cartproducts_id_cartproduct':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_cartproducts` ADD `id_cartproduct` int(11) NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (  `id_cartproduct` )");
					        break;
					    case 'reservation_id_object':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_reservation` ADD `id_object` int(11) NOT NULL");
					        break;
					    case 'reservation_id_customization':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_reservation` ADD `id_customization` int(11) NOT NULL");
					        break;
					    case 'reservation_idshop':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_reservation` ADD `id_shop` int(11) NOT NULL");
					        break;
					    case 'reservation_comment':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_reservation` ADD `comment` text NOT NULL");
					        break;
					    case 'reservation_external':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_reservation` ADD `external_fields` varchar(128) NOT NULL");
					        break;
					    case 'reservation_idassigned':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_reservation` ADD `id_assigned` int(11) NOT NULL");
					        break;
					    case 'reservation_idcartproduct':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_reservation` ADD `id_cartproduct` int(11) NOT NULL");
					        break;
					    case 'reservation_idsource':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_reservation` ADD `id_source` int(11) NOT NULL");
					        break;
					    case 'stock_id_shop':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_stock` ADD `id_shop` int(11) NOT NULL");
					        break;
					    case 'stock_enabled':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_stock` ADD `enabled` BOOLEAN NOT NULL");
					        break;
					    case 'stock_quantity':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_stock` ADD `quantity` tinyint(3) NOT NULL");
					        break;
					    case 'pricesrule_ids_products':
					    	$result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_pricesrule` CHANGE `product` `ids_products` TEXT NOT NULL");
					        break;
					    case 'pricesrule_ids_attributes':
					    	$result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_pricesrule` CHANGE `id_product_attribute` `ids_attributes` TEXT NOT NULL;");
					        break;
					    case 'pricesrule_quantity':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_pricesrule` ADD `quantity` tinyint(3) NOT NULL DEFAULT '1'");
					        break;
					    case 'products_id_shop':
					    	$result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_products` ADD `id_shop` int(10) NOT NULL");
					    	break;
					    case 'timeslot_id_shop':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_timeslot` ADD `id_shop` int(10) NOT NULL");
					        break;
					    case 'pricesrule_id_shop':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_pricesrule` ADD `id_shop` int(10) NOT NULL");
					        break;
					    case 'availability_id_shop':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_availability` ADD `id_shop` int(10) NOT NULL");
					        break;
					    case 'timeslot_id_family':
					    	$result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_timeslot` CHANGE `id_product`  `id_family` int(10) NOT NULL");
					    	$result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_products` CHANGE `timeslots`  `timeslots` varchar(128) NOT NULL");
					    	break;
					    case 'option_id_family':
					    	$result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_option` CHANGE `id_productFamilly`  `id_family` int(10) NOT NULL");
					    	break;
					    case 'option_id_shop':
					    	$result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_option` ADD `id_shop` int(10) NOT NULL;ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_option` DROP PRIMARY KEY, ADD PRIMARY KEY (`id_family`, `id_extension`, `id_option`, `id_internal`, `id_shop`)");
					    	break;
					    case 'pricesrule_id_object':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_pricesrule` ADD  `id_object` int(10) NOT NULL");
					        break;
					    case 'timeslot_id_object':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_timeslot` ADD  `id_object` int(10) NOT NULL");
					        break;
					    case 'timeslot_id_product':
					        $result &= Db::getInstance()->Execute("ALTER TABLE  `". _DB_PREFIX_ ."myownreservations_timeslot` ADD  `id_product` int(10) NOT NULL");
					        break;
					}
				}

				$result = true;
			}
		
			// HOOK UPDATE
			//-----------------------------------------------------------------------------------------------------------------
			if(isset($_GET['hookupdate'])) {
				$objet = $obj->l('Hooks', 'controller');
				$operation = myOwnLang::getOperation($obj, MYOWN_OPE::UPDATE);
				$result = true;
				
				$hooksMissing="";
				foreach(self::$hooks as $hook) {
						$res = myOwnUtils::isHookForModule($hook, $obj->id);
						if ($res===false) $result = $result && $obj->registerHook($hook);
				}
			}

			// OVERRIDE UPDATE
			//-----------------------------------------------------------------------------------------------------------------
			if(isset($_GET['overrideupdate'])) {
				$objet = $obj->l('Overrides', 'controller');
				$operation = myOwnLang::getOperation($obj, MYOWN_OPE::UPDATE, false);
				$result=true;
				if ($files = self::scan_dir(_PS_MODULE_DIR_ . self::$name . "/override/classes")) {
	       			foreach ($files as $file) {
				        if (stripos($file, '.php') !== false  && (stripos($file, '/') === false or _PS_VERSION_ >= "1.5.0.0")) { // && stripos($file, '/') === false
				        	if (!file_exists(_PS_ROOT_DIR_."/override/classes/".$file)
				        		or md5_file(_PS_MODULE_DIR_ . self::$name . "/override/classes/".$file)!=md5_file(_PS_ROOT_DIR_."/override/classes/".str_replace("_","",$file))) {
				        		//backup
				        		$result = $result && @copy(_PS_ROOT_DIR_ . '/override/classes/'.$file, _PS_ROOT_DIR_ . '/override/classes/'.str_replace(".php", '.'.date('YmdHis').'.bak' , str_replace("_","",$file)));
				        		//replace
				        		$result = $result && @copy(_PS_MODULE_DIR_ . self::$name . '/override/classes/'.$file, _PS_ROOT_DIR_.'/override/classes/'.str_replace("_","",$file));
				        	}
				        }
				    }
				}
				$isproversion=is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");
				if ($isproversion && (
		       		!file_exists(_PS_ROOT_DIR_."/override/classes/Tools.php") 
		       		 or md5_file(_PS_MODULE_DIR_ . self::$name . "/override/classes/Tools.ph_")!=md5_file(_PS_ROOT_DIR_."/override/classes/Tools.php") ) )
		       		 {
				       	$result = $result && @copy(_PS_MODULE_DIR_ . self::$name . '/override/classes/Tools.ph_', _PS_ROOT_DIR_.'/override/classes/Tools.php');
			    }
				if (_PS_VERSION_ < "1.5.0.0" && (
		       		!file_exists(_PS_ROOT_DIR_."/override/classes/PDF.php") 
		       		 or md5_file(_PS_MODULE_DIR_ . self::$name . "/override/classes/PDF.ph_")!=md5_file(_PS_ROOT_DIR_."/override/classes/PDF.php") ) )
		       		 {
			       		 $result = $result && @copy(_PS_MODULE_DIR_ . self::$name . '/override/classes/PDF.ph_', _PS_ROOT_DIR_.'/override/classes/PDF.php');
		        }
		        if (_PS_VERSION_ >= "1.7.0.0" && (
		       		!file_exists(_PS_ROOT_DIR_."/override/controllers/front/CartController.php") 
		       		 or md5_file(_PS_MODULE_DIR_ . self::$name . "/override/controllers/front/CartController.ph_")!=md5_file(_PS_ROOT_DIR_."/override/controllers/front/CartController.php") ) )
		       		 {
			       		 $result = $result && @copy(_PS_MODULE_DIR_ . self::$name . '/override/controllers/front/CartController.ph_', _PS_ROOT_DIR_.'/override/controllers/front/CartController.php');
		        }
		        //delete cache
		        $cachedir = _PS_ROOT_DIR_ . (_PS_VERSION_ >= "1.7.0.0" ? '/app' : '').'/cache'.(_PS_VERSION_ >= "1.7.0.0" ? '/dev' : '').'/';
		        $result = $result && @copy($cachedir . 'class_index.php', $cachedir . 'class_index_'.date('YmdHis').'.bak');
		        $result = $result && @unlink($cachedir . 'class_index.php');
			}
			
				
		
			// UPDATE RESERVATION SETTINGS
			//-----------------------------------------------------------------------------------------------------------------
			if(isset($_POST['submitAvailability']) && strlen($_POST["holidayStart"]) > 0 && strlen($_POST["holidayEnd"])>0) {
				$msg='';
				$objet = myOwnLang::$object[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::AVAILABILITY]);
				$result = myOwnReservationsAvailabilitiesController::addAction($obj, $msg);
				if (isset($_POST['idAvailability']) && $_POST['idAvailability']>0)
					$operation = myOwnLang::getOperation($obj, MYOWN_OPE::UPDATE, false);
				else $operation = myOwnLang::getOperation($obj, MYOWN_OPE::ADD, false);
			}
			
			if (isset($_POST["deleteTimeslots"])) {
				$msg='';
				$plur=true;
				$objet = myOwnLang::$objects[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$objects[MYOWN_OBJ::TIMESLOT]);
				$result = myOwnReservationsTimeslotsController::deleteAction($obj, $msg);
				$operation = myOwnLang::getOperation($obj, MYOWN_OPE::DELETE, true, false);
			}

			// UPDATE ADVANCED SETTINGS
			//-----------------------------------------------------------------------------------------------------------------
			if(isset($_POST['editConfig'])) {
				$objet = $obj->l('The advanced settings', 'controller');
				$operation = myOwnLang::getOperation($obj, MYOWN_OPE::UPDATE);
				$plur=true;
				
				$languages = Language::getLanguages();
				$disabledReservationMsg = array();
				$notifyReservationMsg = array();
				$notifyProductMsg = array();
				$notAvailableReservationMsg = array();
				$depositMsg = array();
				$advanceMsg = array();
				$resvMsg = array();
				foreach ($languages AS $language) {
					$disabledReservationMsg[$language['id_lang']] = $_POST['disabledReservationMsg_'.$language['id_lang']];
					$notifyReservationMsg[$language['id_lang']] = $_POST['notifyReservationMsg_'.$language['id_lang']];
					$notifyProductMsg[$language['id_lang']] = $_POST['notifyProductMsg_'.$language['id_lang']];
					$notAvailableReservationMsg[$language['id_lang']] = $_POST['notAvailableReservationMsg_'.$language['id_lang']];
					$depositMsg[$language['id_lang']] = $_POST['depositMsg_'.$language['id_lang']];
					$advanceMsg[$language['id_lang']] = $_POST['advanceMsg_'.$language['id_lang']];
					$resvMsg[$language['id_lang']] = $_POST['resvMsg_'.$language['id_lang']];
					$balanceMsg[$language['id_lang']] = $_POST['balanceMsg_'.$language['id_lang']];
				}

				$price = (Configuration::updateValue('MYOWNRES_PRICE_TYPE', intval($_POST['priceType'])) &&
							Configuration::updateValue('MYOWNRES_DEPOSIT_GLOBAL', intval(isset($_POST['MYOWNRES_DEPOSIT_GLOBAL']) && $_POST['MYOWNRES_DEPOSIT_GLOBAL']==1)) &&
							Configuration::updateValue('MYOWNRES_DEPOSIT_SHOW', intval(isset($_POST['MYOWNRES_DEPOSIT_SHOW']) && $_POST['MYOWNRES_DEPOSIT_SHOW']==1)) &&
							Configuration::updateValue('MYOWNRES_PRICE_TAXEXCL', intval(isset($_POST['priceTaxes']) && $_POST['priceTaxes']==1)) &&
							Configuration::updateValue('MYOWNRES_PRICERULE_BEST', intval(isset($_POST['globalPricerule']) && $_POST['globalPricerule']==1)) &&
							Configuration::updateValue('MYOWNRES_PRICERULE_LEGEND', intval($_POST['priceruleLegend'])) &&
							Configuration::updateValue('MYOWNRES_PRICESET_LEGEND', intval($_POST['pricesetLegend'])) &&
							Configuration::updateValue('MYOWNRES_PRICE_RAW', intval(isset($_POST['rawPrice']) && $_POST['rawPrice']==1)) &&
							Configuration::updateValue('MYOWNRES_TOTAL_TAXINC', intval(isset($_POST['totalTaxes']) && $_POST['totalTaxes']==1)) &&
							Configuration::updateValue('MYOWNRES_NO_LOC_ADVANCE', intval(isset($_POST['MYOWNRES_NO_LOC_ADVANCE']) && $_POST['MYOWNRES_NO_LOC_ADVANCE']==1)) &&
							Configuration::updateValue('MYOWNRES_MIN_FOR_ADVANCE', intval($_POST['minAdvanceAmount'])) );
	
			//Configuration::updateValue('MYOWNRES_REG_LENGTH', intval(isset($_POST['reservationlength']))) &&
				$result = $price && (Configuration::updateValue('MYOWNRES_IGNORE_STEP1', intval(isset($_POST['MYOWNRES_IGNORE_STEP1']) && $_POST['MYOWNRES_IGNORE_STEP1']==1)) &&
					Configuration::updateValue('MYOWNRES_IGNORE_STEP2', intval(isset($_POST['MYOWNRES_IGNORE_STEP2']) && $_POST['MYOWNRES_IGNORE_STEP2']==1)) &&
					Configuration::updateValue('MYOWNRES_IGNORE_PURCHA', intval(isset($_POST['MYOWNRES_IGNORE_PURCHA']) && $_POST['MYOWNRES_IGNORE_PURCHA']==1)) &&
					Configuration::updateValue('MYOWNRES_SAME_PERIOD', intval(isset($_POST['MYOWNRES_SAME_PERIOD']) && $_POST['MYOWNRES_SAME_PERIOD']==1)) &&
					Configuration::updateValue('MYOWNRES_ADDTOCART_ONLIST', !intval(isset($_POST['addToCartOnList']) && $_POST['addToCartOnList']==1) ) &&
					Configuration::updateValue('MYOWNRES_CAT_HIDE', (isset($_POST['catHide']) && $_POST['catHide']==1)) &&
					Configuration::updateValue('MYOWNRES_WIDGET_SHOW', intval(isset($_POST['widgetHide']))) &&
					Configuration::updateValue('MYOWNRES_MSG_DESACTIVATE_RES', $disabledReservationMsg) &&
					Configuration::updateValue('MYOWNRES_MSG_NOTIFY_RES', $notifyReservationMsg) &&
					Configuration::updateValue('MYOWNRES_MSG_NOTAVAILABLE', $notAvailableReservationMsg) &&
					Configuration::updateValue('MYOWNRES_MSG_NOTIFY_PROD', $notifyProductMsg) &&
					Configuration::updateValue('MYOWNRES_MSG_ADVANCE', $advanceMsg) &&
					Configuration::updateValue('MYOWNRES_MSG_BALANCE', $balanceMsg) &&
					Configuration::updateValue('MYOWNRES_MSG_RESV', $resvMsg) &&
					Configuration::updateValue('MYOWNRES_MSG_DEPOSIT', $depositMsg) &&
					(isset($_POST['widgetPosition']) ? Configuration::updateValue('MYOWNRES_WIDGET_POS', $_POST['widgetPosition']) : 1) &&
					Configuration::updateValue('MYOWNRES_WIDGET_CHECKLEN', intval(isset($_POST['checkLength']))) &&
					Configuration::updateValue('MYOWNRES_WIDGET_COLSTRICT', intval(isset($_POST['widgetColumnStrict']))) &&
					Configuration::updateValue('MYOWNRES_WIDGET_PROD', $_POST['productLayout']) &&
					Configuration::updateValue('MYOWNRES_WIDGET_TYPE', $_POST['widgetType']) &&
					Configuration::updateValue('MYOWNRES_WIDGET_HOME', $_POST['widgetHome']) &&
					Configuration::updateValue('MYOWNRES_WIDGET_COL', intval(isset($_POST['widgetColumn']) && $_POST['widgetColumn']==1 && $_POST['widgetType']>0)) &&
					Configuration::updateValue('MYOWNRES_WIDGET_TOP', isset($_POST['widgetTop']) && $_POST['widgetTop']==1 && $_POST['widgetType']>0) &&
					Configuration::updateValue('MYOWNRES_WIDGET_IMG', $_POST['widgetHomeImg'])  );
							
			}		

			foreach ($obj->_extensions->getExtByType(extension_type::MODULE) as $ext) {
				if (method_exists($ext, "execConfigGlobal"))
					$htmlOut .= $ext->execConfigGlobal($cookie,$obj);
				if (Tools::getIsset('ext_enabled_'.$ext->id)) {
					$enabled=Tools::getValue('ext_enabled_'.$ext->id);
					if ($ext->enabled != $enabled)
						Configuration::updateValue('MYOWNRES_EXT_DISABLED_'.$ext->id, !$enabled);
				}
			}
							
 			if (isset($_FILES["file"]) && $_FILES["file"]["tmp_name"]!='') {
 				$importQty=0;
				$updateQty=0;
				$objet = $obj->l('The data file', 'controller');
				$operation = myOwnLang::getOperation($obj, MYOWN_OPE::IMPORT, false);
				
				$myowntable = $_POST['datatable'];
				
				if (stripos($myowntable, "myownreservations_")===false) $myowntable="myownreservations_".$myowntable;
				ini_set("auto_detect_line_endings", true);
 				$importfile = file($_FILES["file"]["tmp_name"], FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

 				if (count($importfile)>1) {
 					if (isset($_POST["emptyData"]) && $_POST["emptyData"]==1) {
     					$req="TRUNCATE TABLE `"._DB_PREFIX_.$myowntable."`;";
     					if (Db::getInstance()->Execute($req)) $htmlOut .= myOwnUtils::displayConf($obj->l('all data type deleted', 'controller'));
 					}
 					$head = $importfile[0];
 					$fields = explode(";", $head);
 					$reqhead="";
 					$reqheadnoid="";
 					$cnt=0;
 					foreach ($fields as $key => $field) {
 						if ($reqhead!="") $reqhead.= ", ";
 						$reqhead.="`".trim($field)."`";
 						if ($key>0) {
 							if ($reqheadnoid!="") $reqheadnoid.= ", ";
     						$reqheadnoid.="`".trim($field)."`";
 						}
 					}

 					foreach ($importfile as $linenumber => $line) {
 						$values="";
 						if ($cnt>0 && trim($line)!='') {
 							$vals=explode(";", $line);
 							$firstval="";

 							foreach ($vals as $key => $val) {
 								if ($key==0) $firstval=$val;
 								if (intval($firstval)>0 or $key>0) {
     								if ($values!="") $values.= ", ";
     								$values.="'".trim(str_replace("|", ";", $val))."'";
 								}
 							}

 							if (intval($firstval)>0) {
								$sql = "SELECT * FROM `"._DB_PREFIX_.$myowntable."` WHERE ".$fields[0]."=".intval($firstval).";";
								$elems = Db::getInstance()->ExecuteS($sql);
								if ($myowntable=='myownreservations_pricesset' || count($elems)==0)
									$req="INSERT INTO `"._DB_PREFIX_.$myowntable."` (".$reqhead.") VALUES (".$values.");";
								else {
									$req="UPDATE `"._DB_PREFIX_.$myowntable."` SET ";
									for($i=1; $i<count($fields); $i++)
										$req.="`".$fields[$i].'` = "'.str_replace("|", ";", $vals[$i]).'", ';
									$req=substr($req, 0 ,-2)." WHERE `".$fields[0]."` = ".$vals[0].";";
								}
 							} else $req="INSERT INTO `"._DB_PREFIX_.$myowntable."` (".$reqheadnoid.") VALUES (".$values.");";
								if (Db::getInstance()->Execute($req)) $importQty++;
								else $htmlOut .= myOwnUtils::displayError($obj->l('Problem on line', 'controller').' '.$linenumber.' : '.Db::getInstance()->getMsgError());
 						}
 						$cnt++;
 					}
 					if ($importQty>0) $operation .= ' ('.$importQty.' '.$obj->l('lines imported', 'controller').')';
 				}
 		    }
 		    //echo '@'.Configuration::get('MYOWNRES_SYNC_0');
 		    //print_r(Configuration::get('MYOWNRES_SYNC_0'));
 		    if ($obj->isProVersion && isset($_POST['editConfig'])) {
 		    	foreach (myOwnLang::$sync_platforms AS $platform_key => $platform_label){
 		    		
 		    		//echo 'MYOWNRES_SYNC_'.$platform_key.'='.(Tools::getIsset('synchro_'.$platform_key) && Tools::getValue('synchro_'.$platform_key)==1);
 		    		Configuration::updateValue('MYOWNRES_SYNC_'.$platform_key, Tools::getValue('synchro_'.$platform_key)==1);
 				}
 				if (Configuration::get('MYOWNRES_SYNC_'.sync_platforms::EXPEDIA)==1) {
	 				Configuration::updateValue('MYOWNRES_SYNC_EQC_LOGIN', Tools::getValue('expediaLogin'));
	 				Configuration::updateValue('MYOWNRES_SYNC_EQC_PASS', Tools::getValue('expediaPassword'));
	 				Configuration::updateValue('MYOWNRES_SYNC_EQC_HOTELID', Tools::getValue('expediaHotelId'));
 				}
 				Configuration::updateValue('MYOWNRES_SYNC_EXT', Tools::getValue('synchro_external')==1);
 				Configuration::updateValue('MYOWNRES_SYNC_BETW', Tools::getValue('synchro_between')==1);
			}
			// ADD, UPDATE & DELETE TIME SLOT
			//-----------------------------------------------------------------------------------------------------------------
			if(isset($_POST['idTimeSlot'])){
				$result = myOwnReservationsTimeslotsController::editAction($obj, $msg);
				
				$objet = myOwnLang::$object[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::TIMESLOT]);
				if ((int)$_POST['idTimeSlot']==0) {
					$operation = myOwnLang::getOperation($obj, MYOWN_OPE::CREATE);
					$inserted = Db::getInstance()->ExecuteS("SELECT LAST_INSERT_ID() AS ID;");
					$insertedId = $inserted[0]['ID'];
				} else {
					$operation = myOwnLang::getOperation($obj, MYOWN_OPE::UPDATE);
				}
				$obj->_timeSlots = new myOwnTimeSlots();
			}
			if(isset($_GET['deleteTimeSlot']) && !empty($_GET['deleteTimeSlot'])) {
				$objet = myOwnLang::$object[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::TIMESLOT]);
				$tempTimeSlot = new myOwnTimeSlot();
				$tempTimeSlot->sqlId = $_GET['deleteTimeSlot'];
				$operation = myOwnLang::getOperation($obj, MYOWN_OPE::DELETE);
				$result = $tempTimeSlot->sqlDelete();
			}
			if(isset($_GET['disableTimeSlot'])) {
				$objet = myOwnLang::$object[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::TIMESLOT]);
				$tempTimeSlot = $obj->_timeSlots->list[$_GET['disableTimeSlot']];
				$tempTimeSlot->days[$_GET['day']] = 0;
				$days=myOwnLang::getDaysNames($langue);
				$operation = myOwnLang::getOperation($obj, MYOWN_OPE::DISABLE).' '.$obj->l('on', 'controller').' '.$days[$_GET['day']];
				$result = $tempTimeSlot->sqlUpdate();
			}
			if(isset($_GET['enableTimeSlot'])) {
				$objet = myOwnLang::$object[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::TIMESLOT]);
				$tempTimeSlot = $obj->_timeSlots->list[$_GET['enableTimeSlot']];
				$tempTimeSlot->days[$_GET['day']] = 1;
				$days=myOwnLang::getDaysNames($langue);
				$operation = myOwnLang::getOperation($obj, MYOWN_OPE::ENABLE).' '.$obj->l('on', 'controller').' '.$days[$_GET['day']];
				$result = $tempTimeSlot->sqlUpdate();
			}

			// ADD, UPDATE & DELETE PRICE RULE
			/*-----------------------------------------------------------------------------------------------------------------
			if(isset($_POST['pricesruleName'])) {
				$tempPricesrule = myOwnPricesRules::getFromPost($obj, $cookie);

				$objet = myOwnLang::$object[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRICERULE]);
				if ($tempPricesrule->sqlId==0) {
					$operation = myOwnLang::getOperation($obj, MYOWN_OPE::CREATE, false);
					$result = $tempPricesrule->sqlInsert();
					$inserted = Db::getInstance()->ExecuteS("SELECT LAST_INSERT_ID() AS ID;");
					$insertedId = $inserted[0]['ID'];
					$obj->_pricerules->list[$insertedId]=$tempPricesrule;
				} else {
					$operation = myOwnLang::getOperation($obj, MYOWN_OPE::UPDATE, false);
					$result = $tempPricesrule->sqlUpdate();
				}
			}
			if(isset($_GET['copyPricesrule'])){
				$objet = myOwnLang::$object[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRICERULE]);
				$cpy=(int)$_GET['copyPricesrule'];
				$tempPricesrule = clone $obj->_pricerules->list[$cpy];
				$tempPricesrule->name .= ' '.$obj->l('copy', 'controller');
				$tempPricesrule->sqlId=0;
				$tempPricesrule->id_family=0;
				$tempPricesrule->ids_products='';
				$tempPricesrule->id_product_attribute=0;
				$operation = myOwnLang::getOperation($obj, MYOWN_OPE::COPY, false);
				$result = $tempPricesrule->sqlInsert();
				$inserted = Db::getInstance()->ExecuteS("SELECT LAST_INSERT_ID() AS ID;");
				$insertedId = $inserted[0]['ID'];
				$obj->_pricerules->list[$insertedId]=$tempPricesrule;
			}
			if(isset($_GET['deletePricesrule']) && !empty($_GET['deletePricesrule'])) {
				$objet = myOwnLang::$object[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRICERULE]);
				$tempPricesrule = new myOwnPricesRule();
				$tempPricesrule->sqlId = $_GET['deletePricesrule'];
				$operation = myOwnLang::getOperation($obj, MYOWN_OPE::DELETE, false);
				$result = $tempPricesrule->sqlDelete();
			}
			if(isset($_GET['qeditPricesrule']) && !empty($_GET['qeditPricesrule'])) {
				$objet = myOwnLang::$object[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRICERULE]);
				$tempPricesrule = new myOwnPricesRule();
				$tempPricesrule->sqlId = $_GET['qeditPricesrule'];
				if (isset($_GET['qenable'])) {
					if (intval($_GET['qenable'])) $operation = myOwnLang::getOperation($obj, MYOWN_OPE::ENABLE, false);
					else $operation = myOwnLang::getOperation($obj, MYOWN_OPE::DISABLE);
					$result = $tempPricesrule->sqlEnable(intval($_GET['qenable']));
				}
				if (isset($_GET['qvisible'])) {
					if (intval($_GET['qvisible'])) $operation = $obj->l('displayed', 'controller');
					else $operation = $obj->l('hidden', 'controller');
					$result = $tempPricesrule->sqlVisible(intval($_GET['qvisible']));
				}
			}*/
		}
		
		$htmlOut .= '
		<script>
			function toggle_visibility(eltId)
			{
				var el = document.getElementById(eltId);
				if (el.style.display != \'none\') {
					el.style.display = \'none\';
				} else {
					el.style.display = \'\';
				}
			}
			function toggle_visibilities(eltName)
			{
				var elms = document.getElementsByName(eltName);
				for (var i = 0; i < elms.length; i++) {
					if (elms[i].style.display != \'none\') {
						elms[i].style.display = \'none\';
					} else {
						elms[i].style.display = \'\';
					}
				}
			}
		</script>
		'.(_PS_VERSION_ > "1.5.0.0" ? '
		<link rel="stylesheet" type="text/css" href="'._MODULE_DIR_.'myownreservations/views/css/tooltipster.css" />
		<script type="text/javascript" src="'._MODULE_DIR_.'myownreservations/views/js/jquery.tooltipster.min.js"></script>
		<script type="text/javascript">
	        $(document).ready(function() {
	            $(".tooltip, .mylabel-tooltip").tooltipster({
				   maxWidth: 600,
				   icon: \'(?)\',
				   iconDesktop: false,
				   iconTouch: false,
				   iconTheme: \'.tooltipster-icon\',
				});
	        });
	    </script>' :'');
		$myOwnResTab = Tools::getValue('myOwnResTab');
		if ($operation != "") self::_construire($obj, true);
		$ctrl = strtolower(Tools::getValue('controller'));
		
		//if (!$obj->isKeyValid or isset($_GET["key"])) $htmlOut .= self::askLicence($cookie, $obj);
		$htmlOut .= $warn;
		//starter
		$obj->getContext()->controller->addCSS(_MODULE_DIR_.'onboarding/css/onboarding.css');
		$obj->getContext()->controller->addCSS(_MODULE_DIR_.'myownreservations/views/css/onboarding.css');
		
		$content=MyOwnReservationsUtils::displayStarter($obj);
		$parent='';$title='';$buttons='';$item='';

		if ($myOwnResTab=="timeslots") { // or Tools::getValue('editTimeSlot',-1)!=-1
			$id = intval(Tools::getValue('editTimeSlot', -1));
			if ($id==0) $id = $insertedId;

			//$buttons = myOwnReservationsController::getButtons($cookie, $obj, $id, false, 'TimeSlot', 'timeslots');
			$parent = $obj->l('Localization', 'controller');
			
			if ($id==-1) $title = ucfirst(myOwnLang::$objects[MYOWN_OBJ::TIMESLOT]);
			if ($id==0) $title = ucfirst(myOwnLang::$actions[MYOWN_OPE::NNEW]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::TIMESLOT]);
			if ($id>0) $title = ucfirst(myOwnLang::$object[MYOWN_OBJ::TIMESLOT]).' #'.$id;
			$from = 'global';
			if (Tools::getValue('fromProduct'))
				$from = 'product';
			if (Tools::getValue('myOwnResTab')=='resources')
				$from = 'object';
			if ($id>-1) $content .= myOwnReservationsTimeslotsController::edit($from, $obj, $id);
			else $content .= myOwnReservationsTimeslotsController::show('global', $obj);
		}
		if ($myOwnResTab=="availability") {
			$id = intval(Tools::getValue('editTimeSlot', -1));
			if ($id==0) $id = $insertedId;
			
			//$buttons = myOwnReservationsController::getButtons($cookie, $obj, $id, false, 'TimeSlot', 'availability');
			$parent = $obj->l('Catalog', 'controller');
			
			if ($id==-1) $title = ucfirst(myOwnLang::$objects[MYOWN_OBJ::AVAILABILITY]);
			if ($id==0) $title = ucfirst(myOwnLang::$actions[MYOWN_OPE::FNEW]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::AVAILABILITY]);
			if ($id>0) $title = ucfirst(myOwnLang::$object[MYOWN_OBJ::AVAILABILITY]).' #'.$id;
			
			//if ($id>-1) 
			$content .= myOwnReservationsAvailabilitiesController::add('global',$obj);
			$content .= myOwnReservationsAvailabilitiesController::show('global',$obj);
		}
		if ($myOwnResTab=="pricesrules") {
			$id = intval(Tools::getValue('editPricesrule', -1));
			if ($id==0) $id = $insertedId;
			
			//$buttons = myOwnReservationsController::getButtons($cookie, $obj, $id, false, 'Pricesrule', 'pricesrules');
			$parent = $obj->l('Discounts', 'controller');
			
			if ($id==-1) $title = ucfirst(myOwnLang::$objects[MYOWN_OBJ::PRICERULE]);
			if ($id==0) $title = ucfirst(myOwnLang::$actions[MYOWN_OPE::FNEW]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRICERULE]);
			if ($id>0) $title = ucfirst(myOwnLang::$object[MYOWN_OBJ::PRICERULE]).' #'.$id;
			
			if ($id>-1) $content .=  myOwnReservationsPricerulesController::edit($cookie,$obj,$id);
			else $content .= myOwnReservationsPricerulesController::show($cookie,$obj);
		}
		if ($myOwnResTab=="advanced" or ($myOwnResTab=="" && $ctrl=="adminmodules") ) {
			//$buttons = myOwnReservationsController::getButtons($cookie, $obj, 0, true, 'Configuration', 'advanced');
			$title = $obj->l('Config', 'controller');
			$parent = $obj->l('myOwnReservations', 'controller');
			$content .= self::configCheckKey($obj);
			$content .= self::configCheckSetup($obj);
			$content .= myOwnReservationsSettingsController::settings($cookie,$obj);
		}
		if ($myOwnResTab=="stock") {


		} else {
			foreach ($obj->_extensions->getExtByType(extension_type::OBJECT) as $ext)
				if ($myOwnResTab==$ext->name) 
					$content .= $ext->getContent($obj);
			
		}
		$myOwnResTabObject = $obj->_extensions->getExtByName($myOwnResTab);
		if ($myOwnResTab!='' && class_exists($myOwnResTab) && $myOwnResTabObject!=null && method_exists($myOwnResTab, 'getButtonsList')) 
			$buttons = $myOwnResTabObject->getButtonsList($obj, $id);
		else $buttons = self::getButtonsList($obj, $id);
		$buttonsHtml = MyOwnReservationsUtils::getButtonsList($buttons);
		if (_PS_VERSION_ > "1.6.0.0") {		
			$htmlOut .= '<div id="buttons16html" style="display:none">'.$buttonsHtml.'</div>';
			$htmlOut .= '<div id="pagetitle16" style="display:none">'.$title.'</div>';

			$htmlOut .= '
			<script type="text/javascript">
		        $(document).ready(function() {
					$(".btn-toolbar").html($("#buttons16html").html());
					$(".page-title").html($("#pagetitle16").html());
		        });
		    </script>';
	    }

		if (_PS_VERSION_ >= "1.5.0.0" && _PS_VERSION_ < "1.6.0.0") $htmlOut .= self::getMenuHeader($obj, $parent, $title, $buttonsHtml, $item);
		
		if (count($errors)>0)
		foreach($errors AS $error) {
			$htmlOut .= myOwnUtils::displayError($error);
		}

		if ($operation != "") {
			if ($result) $htmlOut .= myOwnUtils::displayConf($objet.' '.myOwnLang::getResult($obj, true, !$plur).' '.$operation);
			else $htmlOut .= myOwnUtils::displayError($objet.' '.myOwnLang::getResult($obj, false, !$plur).' '.$operation.' : '.Db::getInstance()->getMsgError());
			self::_construire($obj, true);
		}
		
		$htmlOut .=  '<div style="">'.$content.'</div>';
		$htmlOut .= '</div>';
		return $htmlOut;
    }

	
}



?>