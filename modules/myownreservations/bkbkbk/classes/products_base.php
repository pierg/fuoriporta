<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnProducts {
	var $list;
	
	public function getUnitsFamiliesIds($obj) {
		$ids=array();
		$occ_ext = $obj->_extensions->getExtByType(extension_type::PROD_OCC);
		foreach ($this->list as $product)
			if (array_key_exists($product->productType, $occ_ext))
				$ids[$product->sqlId] = $occ_ext[$product->productType]->id;
		return $ids;
	}
	
	public function getListForEmployee($id_employee) {
		$filtered=array();
		$restricted=false;
		foreach ($this->list as $product)
			if (in_array($id_employee, $product->ids_users))
				$restricted=true;
		foreach ($this->list as $product)
			if (!$restricted or in_array($id_employee, $product->ids_users))
				$filtered[$product->sqlId] = $product;
		return $filtered;
	}
	
	public function getProducts($id_lang, $full=false, $productsFilter=array())  {
		$id_shop = myOwnUtils::getShop();
		$products=array();
		
    	//categories selection
		$cats=array();
		foreach ($this->list as $product)
		    foreach(explode(',',$product->ids_categories) as $category)
		    	if (intval($category)>0)
		    		$cats += MyOwnReservationsUtils::getAllSubCategoriesId(intval($category));
		//if (count($cats)==0) return array();
		//removed because getting no products for selection if only products
		if (count($cats)>0) {
			$query = '
			SELECT *
			FROM `'._DB_PREFIX_.'category_product` cp
			INNER JOIN `'._DB_PREFIX_.'product_lang` pl ON pl.`id_product` = cp.`id_product`';
			//full is required for getting all category products
			if ($full) $query .= '
			INNER JOIN `'._DB_PREFIX_.'product` pp ON pp.`id_product` = cp.`id_product`
			LEFT JOIN `'._DB_PREFIX_.'image` i ON i.`id_product` = cp.`id_product`
			LEFT JOIN `'._DB_PREFIX_.'image_lang` il
					ON (i.`id_image` = il.`id_image`
					AND il.`id_lang` = '.(int)$id_lang.')';
			if ($id_shop>0) $query .= '
			INNER JOIN `'._DB_PREFIX_.'category_shop` cs ON cp.`id_category` = cs.`id_category`
			INNER JOIN `'._DB_PREFIX_.'product_shop` ps ON ps.`id_product` = cp.`id_product` AND pl.`id_shop` = ps.`id_shop`';
			$query .= '
			WHERE cp.`id_category` IN ('.implode(',', $cats).') AND pl.`id_lang` = '.$id_lang;
			if ($id_shop>0) $query .= '
			AND ps.`id_shop` ='.$id_shop.' AND cs.`id_shop`='.$id_shop;
			
			$results = Db::getInstance()->ExecuteS($query);
			foreach($results as $result)
				if ($productsFilter==array() || in_array((int)$result["id_product"], $productsFilter))
					$products[intval($result["id_product"])] = $result; 
			
		}	
		//products selection
		$prods=array();
		foreach ($this->list as $product)
			$prods = array_merge($prods, explode(',',$product->ids_products));
		
		if (implode(',', array_filter($prods))!='') {
			$query = '
			SELECT *
			FROM `'._DB_PREFIX_.'product_lang` pl';
			if ($full) $query .= '
			INNER JOIN `'._DB_PREFIX_.'product` pp ON pp.`id_product` = cp.`id_product`
			LEFT JOIN `'._DB_PREFIX_.'image` i ON i.`id_product` = cp.`id_product`
			LEFT JOIN `'._DB_PREFIX_.'image_lang` il
					ON (i.`id_image` = il.`id_image`
					AND il.`id_lang` = '.(int)$id_lang.')';
			if ($id_shop>0) $query .= '
			
			INNER JOIN `'._DB_PREFIX_.'product_shop` ps ON ps.`id_product` = pl.`id_product` AND pl.`id_shop` = ps.`id_shop`';
			$query .= '
			WHERE pl.`id_product` IN ('.implode(',', array_filter($prods)).') AND pl.`id_lang` = '.$id_lang;
			
			$results = Db::getInstance()->ExecuteS($query);
			foreach($results as $result)
				if ($productsFilter==array() || in_array((int)$result["id_product"], $productsFilter))
					$products[intval($result["id_product"])] = $result; 
		}
		return $products;
	}
	
	public function getProductsQuantity() {
		$productQty=0;
		if (count($this->list))
		foreach ($this->list as $product) {
			$productQty+=$product->getProductsQuantity();
		}
		return $productQty;
	}
	
	public function getResProductFromProduct($productId=0) {
		$id_shop = MyOwnReservationsUtils::getShop();
		//if ($productId==0 && array_key_exists(0, $this->list)) return $this->list[0]; 
	  	//check product family per product
	  	
	  	foreach ($this->list as $resProduct)
	  		if ($id_shop==0 || $resProduct->id_shop==0 || $resProduct->id_shop==$id_shop)
	  			if (in_array($productId, explode(',',$resProduct->ids_products))) return $resProduct;
	  		
		//check parent categories
		$productcats = Product::getProductCategories($productId);
		foreach ($productcats as $productcat) {
			//check direct cat (home only checked in this case in PS 1.4)
			foreach ($this->list as $resProduct)
				if ($id_shop==0 || $resProduct->id_shop==0 || $resProduct->id_shop==$id_shop) {
					if (intval($productcat)>0 && in_array(intval($productcat), explode(',',$resProduct->ids_categories))) return $resProduct;
				}
			//check parent cats (direct cat is included instead for home in PS 1.4)
			$tempCat=new Category(intval($productcat));
			if ($tempCat->id_parent) {
				$parentCats = $tempCat->getParentsCategories();
				foreach ($parentCats as $parentCat)
					foreach ($this->list as $resProduct)
						if ($id_shop==0 || $resProduct->id_shop==0 || $resProduct->id_shop==$id_shop) {
							if (intval($parentCat['id_category'])>0 && in_array(intval($parentCat['id_category']), explode(',',$resProduct->ids_categories))) return $resProduct;
						}
			}
		}
	  		
	  	return null;
	}
	
	public function getResProductFromCategory($categoryId) {
		$id_shop = MyOwnReservationsUtils::getShop();
		$catObj=new Category($categoryId);
		if ($catObj->id_parent)
			$catTab=$catObj->getParentsCategories();
		else $catTab=array();
		
		//check cat
		foreach ($this->list as $resProduct) {
			if ($id_shop==0 || $resProduct->id_shop==0 || $resProduct->id_shop==$id_shop) {
				foreach ($catTab as $cat)
					if (in_array(intval($cat['id_category']), explode(',',$resProduct->ids_categories))) return $resProduct;
			}
		}
	  	return null;
	}
	
	public function getDisabledCategories($id) {
		$disabledCategories=array();
		if ($id==-1 || $id==0 ) return $disabledCategories;
		if ($id) $id_shop = intval($this->list[$id]->id_shop);
		else $id_shop=0;
		foreach ($this->list as $mainProd)
			if ($mainProd->sqlId!=$id) {
				if ($id_shop==0 || $mainProd->id_shop==0 || $mainProd->id_shop==$id_shop) {
					$cats = explode(',',$mainProd->ids_categories);
					foreach ($cats as $cat)
						if (intval($cat)>0 && !array_key_exists($cat, $disabledCategories)) {
							$disabledCategories[$cat]=$cat;
						}
				}
			}
		return $disabledCategories;
	}
	
	public function getDisabledProducts($id) {
		if ($id==-1) $id=-0;
		$disabledProducts=array();
		if ($id) $id_shop = intval($this->list[$id]->id_shop);
		else $id_shop=0;
		foreach ($this->list as $mainProd)
			if ($mainProd->sqlId!=$id) {
				if ($id_shop==0 || $mainProd->id_shop==0 || $mainProd->id_shop==$id_shop) {
					$disabledProducts += $mainProd->getProductsId();
				}
			}
		return $disabledProducts;
	}
	
	public function getDefaultCategory($cats=array()) {
		$resa_category=0;
		//if ($cats!=array()) {
			//calculating category product to show
			if (isset($_GET['id_category'])) $resa_category=$_GET['id_category']; // && in_array($_GET['id_category'], $cats)
			else if (isset($_GET['id_product'])) {
				$productcats = Product::getProductCategories($_GET['id_product']);
					foreach ($productcats as $cat)
						if ($resa_category==0 && $cat != 2)
							$resa_category=$cat;
			} else { // if (count($this->list)>1) removed to get default car for std version
				foreach ($this->list as $mainProd) {
					foreach(explode(',',$mainProd->ids_categories) as $catd) {
						if ($resa_category==0 && $catd != 2)
							$resa_category=$catd;
					}
					break;
				}
			}
		/*} else {
			foreach($cats as $cat)
				if ($resa_category == 0 && $cat != 2)
					$resa_category=$cat;
		}*/
		return $resa_category;
	}
	
	public function getCategoriesLink($cats=array(), $all='') {
		global $smarty;
		global $cookie;
		$id_shop = MyOwnReservationsUtils::getShop();
		$resa_category=$this->getDefaultCategory($cats);
		$resa_category = Tools::getValue('myOwnSelectCat', $resa_category);

		//collecting categories for widget
		$categoriesName=array();
		if ($all!='') {
			$categoriesName[0]=$all;
		}
		$protocol_link = (Configuration::get('PS_SSL_ENABLED') || Tools::usingSecureMode()) ? 'https://' : 'http://';
        $useSSL = (Configuration::get('PS_SSL_ENABLED') && Tools::usingSecureMode()) ? true : false; //(isset($this->ssl) && $this->ssl && 
        $protocol_content = ($useSSL) ? 'https://' : 'http://';
        $link = new Link($protocol_link, $protocol_content);
		$url_base = $link->getBaseLink($id_shop);
		$categoriesLink=array();
		$categoriesIdFamilly=array();
		$sortedMainProds = $this->list;
		ksort ($sortedMainProds);

		foreach ($sortedMainProds as $mainProd) 
			if ($id_shop==0 or $mainProd->id_shop==0 or $mainProd->id_shop==$id_shop) {
				$cats = explode(',',$mainProd->ids_categories);
				foreach ($cats as $cat)
					if (intval($cat)>2 && !array_key_exists($cat, $categoriesName)) {
						$catObj = new Category($cat);
	
						$link_rewrite = Category::getLinkRewrite($cat, $cookie->id_lang);
		
						$categoriesName[$cat]=$catObj->getName($cookie->id_lang);
						$categoriesIdFamilly[$cat]=$mainProd->sqlId;
						$categoriesLink[$cat]= $link->getCategoryLink($cat, $link_rewrite);
					}
			}
		
		if (!array_key_exists($resa_category, $categoriesName)) $resa_category=0;
		$smarty->assign('resa_category', $resa_category);
		
		$homeCat = Category::getRootCategory($cookie->id_lang);

	    $smarty->assign('selectCats',$categoriesName);
	    $smarty->assign('homeCategoryId',$homeCat->id_category);
	    
	    if (count($categoriesLink)>0) {
	    	foreach ($categoriesLink as $defaultCatKey => $defaultCatVal) {
		    	$link_rewrite = Category::getLinkRewrite($defaultCatKey, $cookie->id_lang);
		    	$smarty->assign('defaultCategoryLink', $link->getCategoryLink($defaultCatKey, $link_rewrite));
		    	$smarty->assign('defaultCategoryId', $defaultCatKey);
	    	}
		} else {
			$smarty->assign('defaultCategoryLink',$url_base);
			$smarty->assign('defaultCategoryId', $homeCat->id_category);
		}
		$smarty->assign('selectCatLink',Tools::jsonEncode($categoriesLink));
		$smarty->assign('selectCatFammily',Tools::jsonEncode($categoriesIdFamilly));
		
		$smarty->assign('search_query',Tools::getValue('search_query'));
		$smarty->assign('searchLink',$link->getPageLink('search.php'));
		
		return $resa_category;
	}
	
}

?>