<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnTimeSlots {
	var $list=array();
	var $availabilities;
	
	//Construct timeslot list
	//-----------------------------------------------------------------------------------------------------------------
	public function __construct($populate=true, $id=0) {
		if ($populate) {
			$id_shop = myOwnUtils::getShop();
			$sql="SELECT * FROM " . _DB_PREFIX_ . "myownreservations_timeslot";
			if (defined('MYOWNRES_SEPARATE_SHOP') && MYOWNRES_SEPARATE_SHOP)
				$sql.=" WHERE id_shop = ".$id_shop;
			$sql.=" ORDER BY startTime";
			$timeSlotSql = Db::getInstance()->ExecuteS($sql);
			foreach($timeSlotSql AS $timeSlot) 
				if ((int)$id==0 || (int)$timeSlot['id_family']==(int)$id) {
				$tempTimeSlot = new myOwnTimeSlot();
				$tempTimeSlot->sqlId = $timeSlot['id_timeslot'];
				$tempTimeSlot->id_family = $timeSlot['id_family'];
				if (array_key_exists('id_object', $timeSlot)) $tempTimeSlot->id_object = $timeSlot['id_object'];
				if (array_key_exists('id_product', $timeSlot)) $tempTimeSlot->id_product = $timeSlot['id_product'];
				if (array_key_exists('week_type', $timeSlot)) $tempTimeSlot->week_type = $timeSlot['week_type'];
				$tempTimeSlot->startTime = $timeSlot['startTime'];
				$tempTimeSlot->endTime = $timeSlot['endTime'];
				$tempTimeSlot->name = $timeSlot['name'];
				$tempTimeSlot->type = $timeSlot['type'];
				$tempTimeSlot->quota = $timeSlot['quota'];
				for($i=1; $i<8; $i++) {
					$tempTimeSlot->days[$i] = $timeSlot['day'.$i];
				}
				$this->list[ $timeSlot['id_timeslot']] = $tempTimeSlot;
			}
		}
	}
	
	/*public static function sortForGlouton($a,$b) {
		if ($a->getEndTime(true) == $b->getEndTime(true)) {
				//de la plus longue a la plus petite
			if (($a->getEndTime(true)-$a->getStartTime()) == ($b->getEndTime(true)-$b->getStartTime())) {
			    return 0;
			}
			return ($a->getEndTime(true)-$a->getStartTime()) > ($b->getEndTime(true)-$b->getStartTime()) ? -1 : 1;
		}
		//de celle qui fini le plus tard a celle qui fini le plus tot
		return ($a->getEndTime(true) > $b->getEndTime(true)) ? -1 : 1;
	}*/
	
	public static function sortForProductOrder($a,$b) {
		if ($a->id_family == $b->id_family) {
				//de la plus longue a la plus petite
			//if (($a->getEndTime(true)-$a->getStartTime()) == ($b->getEndTime(true)-$b->getStartTime())) {
			    return 0;
			//}
			return ($a->getEndTime(true)-$a->getStartTime()) > ($b->getEndTime(true)-$b->getStartTime()) ? -1 : 1;
		}
		//de celle qui fini le plus tard a celle qui fini le plus tot
		return ($a->id_family > $b->id_family) ? -1 : 1;
	}
	
	public function getTimeSlotsColumnsDays($tstype) {
		$tsObjects=array();
        for($i=1; $i<8; $i++) {
        	$dailyslots = $this->getTimeSlotsOfDayOfWeek($i, $tstype);
        	uasort($dailyslots, array('myOwnTimeSlots', 'sortForProductOrder') );
        	$tsObjects[$i]=$dailyslots;
        }
        
		$dayParallel = array();
		//creating lines for reservation without serial
		$timeSlotsColumnDays = array();
		for($i=1; $i<8; $i++) {
			$flatObjects = array();
			$columnTs=array();
			//$flatResasLinesCount=0;
			if (count($tsObjects[$i]))
				foreach ($tsObjects[$i] as $key => $tsObject) {
					$flatTsLinesCount=0;
					$canbeplaced=false;
					//echo "resaObject:".$key."(".date('Y-m-d H:i:s',$resaObject->_end)." ".date('Y-m-d H:i:s',$resaObject->_start).")--------<br>";
					foreach($flatObjects as $key2 => $flatTsLine) {
						$canbeplaced=true;
						$flatTsLinesCount++;
	// 					echo "flatResasLine:".$key2."--------<br>";
						foreach($flatTsLine as $key3 => $flatTs) {
							
							$canbeplaced = $canbeplaced && ($flatTs->getStartTime() >= $tsObject->getEndTime(true) or $flatTs->getEndTime(true) <= $tsObject->getStartTime());
	// 						echo "flatResa:".$key3."(".date('Y-m-d H:i:s',$flatTs->getStartTime()).">=".date('Y-m-d H:i:s',$tsObject->getEndTime(true))." or ".date('Y-m-d H:i:s',$flatTs->getEndTime(true))." <= ".date('Y-m-d H:i:s',$tsObject->getStartTime()).")<br>";
						}
	// 					echo "key:".$key."=".intval($canbeplaced)."<br>";
						if ($canbeplaced) break;
					}
					if (!$canbeplaced) $flatTsLinesCount++;
					$flatObjects[$flatTsLinesCount][] = $tsObject;
					$columnTs[$flatTsLinesCount][] = $tsObject->sqlId;
					
				}
			else $columnTs[1]=array();
	        $timeSlotsColumnDays[$i] = $flatObjects;
	        $dayParallel[$i]=$columnTs;
	    }

		return $dayParallel;
	}
	
	public static function getDefault($id=0, $allchecked=false) {
		$defaultTimeSlot = new myOwnTimeSlot();
		$defaultTimeSlot->sqlId = $id;
		$defaultTimeSlot->type = 0;
		$defaultTimeSlot->quota = 0;
		$defaultTimeSlot->startTime = "00:00:00";
		$defaultTimeSlot->endTime = "00:00:00";
		if ($allchecked) $defaultTimeSlot->days = array(1=>1,2=>1,3=>1,4=>1,5=>1,6=>1,7=>1);
		return $defaultTimeSlot;
	}
	
	public function isProductAssigned()
    {
    	$start_time=null;
    	$start_id=0;
        foreach ($this->list as $time_slot) {
        	if ($time_slot->id_product > 0) return true;
        }
        return false;
    }
    
	public function getFirst()
    {
    	$start_time=null;
    	$start_id=0;
        foreach ($this->list as $time_slot) {
            if ($start_time==null || $time_slot->startTime<$start_time) {
                $start_time=$time_slot->startTime;
                $start_id = $time_slot->sqlId;
            }
        }
		if ($start_id>0)
			return $this->list[$start_id];
        return null;
    }
    
    public function getLast()
    {
    	$end_time=null;
    	$end_id=0;
        foreach ($this->list as $time_slot) {
            if ($end_time==null || $time_slot->endTime>$end_time) {
                $end_time=$time_slot->endTime;
                $end_id = $time_slot->sqlId;
            }
        }
		if ($end_id>0)
			return $this->list[$end_id];
        return null;
    }
    
	public function getTimeSlotOfTimeStr($day, $timeStr, $keys=array(), $col)
    {
    	//echo '<br><br>getTimeSlotOfTimeStr:'.$timeStr.' '.date('Y-m-d', $day).' '.$col.' '.implode('-', $keys);
        $timecheck = strtotime(date('Y-m-d', $day).' '.$timeStr.':00');
        foreach ($this->getTimeSlotsOfDay($day) as $time_slot) {
        	//if ($keys==array() || in_array($time_slot->sqlId, $keys)) echo '<br>'.$time_slot->name.' '.date('H:i', $time_slot->getStartDate($day)).' <= '.date('H:i', $timecheck).' && '.date('H:i', $time_slot->getEndDate($day)).' > '.date('H:i', $timecheck).' :'.intval($time_slot->getStartDate($day) <= $timecheck && $time_slot->getEndDate($day) >= $timecheck);
            if (($keys==array() || in_array($time_slot->sqlId, $keys)) && $time_slot->getStartDate($day) <= $timecheck && ($time_slot->getEndDate($day) > $timecheck OR $time_slot->endTime=='00:00:00' OR $time_slot->endTime<$time_slot->startTime)) {
                //echo 'ts'.$time_slot->sqlId.':'.$time_slot->id_family.' ';
                return $time_slot;
            }
        }

        return null;
    }
    public function isTimeSlotOfTime($timecheck)
    {
        foreach ($this->list as $time_slot) {
        	//echo '<br>'.date('H:i', $time_slot->getStartTime()).' <= '.date('H:i', $timecheck).' && '.date('H:i', $time_slot->getEndTime()).' >= '.date('H:i', $timecheck);
            if ($time_slot->getStartTime()<=$time_slot->getEndTime() && $time_slot->getStartTime() <= $timecheck && $time_slot->getEndTime() >= $timecheck) {
                return $time_slot;
            }
            //case for day selection with start time < end time
            //echo '<br>'.date('H:i', $time_slot->getStartTime()).' >= '.date('H:i', $timecheck).' && '.date('H:i', $time_slot->getEndTime()).' <= '.date('H:i', $timecheck);
            if ($time_slot->getStartTime()>$time_slot->getEndTime() && $time_slot->getStartTime() >= $timecheck && $time_slot->getEndTime() <= $timecheck) {
                return $time_slot;
            }
        }

        return null;
    }
	
	//List available timeslots on day
	//-----------------------------------------------------------------------------------------------------------------
	public function getTimeSlotsOfDay($day, $type=0) { 
		$dayTimeSlots = array(); 
		if (count($this->list)>0)
		foreach($this->list AS $timeSlot) {
			if (($type==0 or $timeSlot->type==0 or $timeSlot->type==$type)
				&& $timeSlot->isEnableOnDay($day)) {
				$dayTimeSlots[$timeSlot->sqlId] = $timeSlot;
			}
		}
		return $dayTimeSlots;
	}
	public function isDaily() {
		return (count($this->list)==1 && array_key_exists(0, $this->list));
	}
	public function getDaily() {
		if ($this->isDaily()) return $this->list[0];
		else return null;
	}
	public function getTimeSlotsOfDayOfWeek($dayOfWeek, $type=0) {
        $dayTimeSlots = array(); 
		if (count($this->list)>0)
		foreach($this->list AS $timeSlot) {
			if (($type==0 or $timeSlot->type==0 or $timeSlot->type==$type)
				&& $timeSlot->days[$dayOfWeek]==1) {
				$dayTimeSlots[$timeSlot->sqlId] = $timeSlot;
			}
		}
		return $dayTimeSlots;
	}
	
	public function getAdminTimeSlots() {
		$ts=array();
		if (count($this->list)>0)
		foreach($this->list AS $timeSlot)
			$ts[$timeSlot->sqlId] = $timeSlot;
		return $ts;
	}
	
	public function getAvailableDays($type=0) {
		$days = array();
		for($i=1; $i<8; $i++) {
			$days[$i]=0;
			if (is_array($this->list))
			foreach($this->list AS $timeSlot)
				if ($timeSlot->type==0 or $type==0 or $timeSlot->type==$type)
					if ($timeSlot->isEnableOnDayOfWeek($i))
						$days[$i]=1;
		}
		return $days;
	}
	
	public function getAvailableDaysStr($type=0) {
		$days = "";
			foreach($this->getAvailableDays($type) AS $day)
				$days.= ";".$day;
		return $days;
	}
	
	//Get quota sum for available timeslots on a day
	//-----------------------------------------------------------------------------------------------------------------
	public function getTimeSlotsQuotaOfDay($day) {
		$total=0;
		foreach($this->getTimeSlotsOfDay($day) AS $timeSlot) {
			$total += $timeSlot->quota;
		}
		return $total;
	}
	
	//ara quotas on all timeslots of a day
	//-----------------------------------------------------------------------------------------------------------------
	public function areTimeSlotsQuotaOnDay($day) {
		foreach($this->getTimeSlotsOfDay($day) AS $timeSlot) {
			if ($timeSlot->quota==0) return false;
		}
		return true;
	}

	//get timeslot of a time
	//-----------------------------------------------------------------------------------------------------------------
	public function getTimeSlotOfTime($time, $with_generated=true) {
		$out;
		foreach($this->getTimeSlotsOfDay($time) AS $timeSlot) {
			if ((!$timeSlot->_generated || $with_generated) && $timeSlot->getStartHourTime($time) <= $time &&  $timeSlot->getEndHourTime($time) > $time ) return  $timeSlot;
			if ((!$timeSlot->_generated || $with_generated) && $timeSlot->startTime == $timeSlot->endTime && $timeSlot->startTime=='00:00:00') return  $timeSlot;
		}
		return null;
	}
	public function getTimeSlotsOfFamily($id_family=0) {
		$timeslots=array();
		foreach($this->list AS $timeSlot)
			if ($timeSlot->id_family ==  $id_family)
				$timeslots[$timeSlot->sqlId] =  $timeSlot;

		return $timeslots;
	}
	public function getTimeSlotOfTimeAndFamily($time, $id_family=0, $with_generated=true) {
		$out;
		foreach($this->list AS $timeSlot) 
			if ($timeSlot->isEnableOnDay($time) && $timeSlot->id_family ==  $id_family) {
				if ((!$timeSlot->_generated || $with_generated) && $timeSlot->getStartHourTime($time) <= $time &&  $timeSlot->getEndHourTime($time) > $time ) return  $timeSlot;
				if ((!$timeSlot->_generated || $with_generated) && $timeSlot->startTime == $timeSlot->endTime && $timeSlot->startTime=='00:00:00') return  $timeSlot;
		}
		return null;
	}

	
	public function getNearestStart($time) {
		$score=null;
		$out=null;
		foreach($this->list AS $timeSlot) {
			$diff=$time-$timeSlot->getStartHourTime($time);
			if ($score===null or (abs($diff) < abs($score)) ) {
				$score = $diff;
				$out = $timeSlot;
			}
		}
		return $out;
	}
	
	public function getNearestEnd($time) {
		$score=null;
		$out=null;
		foreach($this->list AS $timeSlot) {
			$diff=$time-$timeSlot->getEndHourTime($time);
			if ($score===null or (abs($diff) < abs($score)) ) {
				$score = $diff;
				$out = $timeSlot;
			 }
		}
		return $out;
	}

	//Check if timeslots are available between two dates
	//-----------------------------------------------------------------------------------------------------------------
	public function areTimeSlotsEnablesBetween($start, $end, $availabilities) {
      		$dayCount=0;

			do {
     			$curDay=strtotime("+".$dayCount."day",$start);
				$dayCount++;
				if (is_array($this->list))
      				foreach ($this->list AS $timeSlot) {
     					if( $availabilities->isAvailable($curDay, $timeSlot)
     						&& $timeSlot->isEnableOnDay($curDay) 
     						&& $timeSlot->getStartHourTime($curDay)>=$start 
     						&& $timeSlot->getStartHourTime($curDay)<$end)
      						return true;
      			}
			} while ($curDay < $end);

			return false;
		}
	
	public static function areTimeSlotsEnableOnDays($timeslots, $start, $end) {
		$length = -1;
		do {
			$length++;
			$checkLimit = strtotime("+".$length." day",$start);
			if (!self::areTimeSlotsEnableOnDay($timeslots, $checkLimit)) return false;
		} while (date('Y-m-d',$checkLimit) != date('Y-m-d',$end)
				 && date('Y-m-d',$checkLimit) < date('Y-m-d',$end) && $length<10000 );
				 
		return true;
	}
	
	public static function areTimeSlotsEnableOnDay($timeslots, $day) {
		foreach ($timeslots AS $timeSlot) {
			if($timeSlot->isEnableOnDay($day))
				return true;
		}
		return false;
	}
	
	public function areTimeSlotsEnableOnDayOfWeek($dayOfWeek, $type) {
		foreach ($this->getTimeSlotsOfDayOfWeek($dayOfWeek, $type) AS $timeSlot) {
			if($timeSlot->isEnableOnDayOfWeek($dayOfWeek))
				return true;
		}
		return false;
	}
	
	public function areEnableOnDay($day, $type=0) {
		foreach ($this->list AS $timeSlot) {
			if ($timeSlot->type==0 or $type==0 or $timeSlot->type==$type)
				if($timeSlot->isEnableOnDay($day))
					return true;
		}
		return false;
	}
	
	//Check if timeslots are available between two dates
	//-----------------------------------------------------------------------------------------------------------------
	public function getEnablesTimeSlotsBetween($start, $end, $type, $availabilities, $id_product=0) {
		$filteredList=array();
      		foreach ($this->list AS $timeSlot) {

      			if (($id_product == -1 || $timeSlot->id_product==0 || $timeSlot->id_product==$id_product) && ($timeSlot->id_object==0 || Tools::getValue('place', 0)==0 || $timeSlot->id_object==Tools::getValue('place') )) {
	          		$dayCount=0;
	          		$enable=false;
	     			do {
	           			$curDay=strtotime("+".$dayCount."day",$start);
	     			
	      				$dayCount++;
	      				//added + to start date check because of fixed length test
	      				if ($availabilities->isAvailable($curDay, $timeSlot)) {
	     					if($timeSlot->isEnableOnDay($curDay) && $timeSlot->getStartHourTime($curDay)>=$start && $timeSlot->getStartHourTime($curDay)<$end) {
	      						$enable=true;
	      						break;
	      					}
	      				}
	     			} while ($curDay < $end);
	     			if ($enable && (intval($timeSlot->type)==0 or $type==0 or $timeSlot->type==$type)) $filteredList[$timeSlot->sqlId] = $timeSlot;
	 			}
	 		}

			return $filteredList;
		}
	
	//Check if there si timeslots available on wekends
	//-----------------------------------------------------------------------------------------------------------------
	public function isWeekEndDays() {
		foreach($this->list AS $timeSlot) {
			if ($timeSlot->days[6]==1) return true;
			if ($timeSlot->days[7]==1) return true;
		}
		return false;
	}
	
	//Return true if day is in holiday
	//-----------------------------------------------------------------------------------------------------------------
	public function isDayInHoliday($day) {
		//check availability
		$inHoliday=true;
		$availabilities = explode(";",$this->availabilities);
		$typeAvailable=false;
		foreach($availabilities AS $availability) {
			if (strlen(trim($availability))>0) {
				$availabilityBounds = explode(":",$availability);
				if (sizeof($availabilityBounds)==3 && $availabilityBounds[0] == 1) {
					$typeAvailable=true;
					if ( $day >= strtotime($availabilityBounds[1]) AND $day <= strtotime($availabilityBounds[2]) ) $inHoliday=false;
				}	
			}	
		}
		if ($typeAvailable && $inHoliday) return true;
		
		//check unavailability
		foreach($availabilities AS $availability) {
			if (strlen(trim($availability))>0) {
				$availabilityBounds = explode(":",$availability);
				if (sizeof($availabilityBounds)==3 && $availabilityBounds[0] == 0) {
					if ( $day >= strtotime($availabilityBounds[1]) AND $day <= strtotime($availabilityBounds[2]) ) return true;
				}		
			}	
		}
		
		//check general holiday
		$holidays = explode(";",Configuration::get('MYOWNRES_RES_HOLIDAYS'));
		foreach($holidays AS $holiday) {
			if (strlen(trim($holiday))>0) {
				$holidaysBounds = explode(":",$holiday);
				if (sizeof($holidaysBounds)==2) {
					if ( $day >= strtotime($holidaysBounds[0]) AND $day <= strtotime($holidaysBounds[1]) ) return true;
				}	
			}	
		}
		return	false;
	}

}

?>