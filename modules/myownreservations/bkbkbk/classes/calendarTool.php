<?php
/**
* 2010-2015 LaBulle
*
* NOTICE OF LICENSE
*
* This source file is subject to Terms and Conditions of Use 
* Available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade module to newer
* versions in the future. If you wish to customize module for your
* needs please refer to http://www.labulle.net for more information.
*
* @author    laBulle.net <info@labulle.net>
* @copyright 2010-2015 laBulle
* @license   http://www.labulle.net/download/CGV-en.pdf
*/

class MyOwnCalendarTool extends MyOwnCalendar {
	
	public static function getNiceLength($obj, $minutes) {
		$minutes=abs($minutes);
		if ($minutes<60) return $minutes.' '.myOwnLang::getUnit(reservation_unit::MINUTE, ($minutes>1));
		else if ($minutes<(60*24)) return round($minutes/60).' '.myOwnLang::getUnit(reservation_unit::HOUR, (round($minutes/60)>1));
		else if ($minutes<(60*24*7)) return round($minutes/(60*24)).' '.myOwnLang::getUnit(reservation_unit::DAY, (round($minutes/(60*24))>1));
		else return round($minutes/(60*24*7)).' '.myOwnLang::getUnit(reservation_unit::WEEK, (round($minutes/(60*24*7))>1));
	}
	
	public static function getFirstTimeSlot($timeslots, $day) {
		$firsttimeslot=0;
		if (count($timeslots)>0)
			foreach($timeslots as $timeslot)
				if ($timeslot->isEnableOnDay($day) && $firsttimeslot==0) $firsttimeslot = $timeslot->sqlId;
		return $firsttimeslot;
	}
	
	public static function isEndEarlierThanStart($timeslots, $startTimeSlot, $endTimeSlot) {
		$ended=false;
		if (array_key_exists($endTimeSlot, $timeslots) && array_key_exists($startTimeSlot, $timeslots))
			if (strtotime($timeslots[$endTimeSlot]->startTime) < strtotime($timeslots[$startTimeSlot]->startTime))
				return true;
		/*if (count($timeslots)>0) {
			foreach($timeslots as $timeslot) {
				if ($ended && $timeslot->sqlId == $startTimeSlot) return true;
				if ($timeslot->sqlId == $endTimeSlot) $ended=true;
			}
		}*/
		return false;
	}
	
	public static function getDaysBetween($start, $end) {
		$length = -1;
		$days=array();
		do {
			$length++;
			$checkLimit = strtotime("+".$length." day",$start);
			$days[]=$checkLimit;
			
		} while (date('Y-m-d',$checkLimit) != date('Y-m-d',$end)
				 && date('Y-m-d',$checkLimit) < date('Y-m-d',$end) && $length<10000 );
				 
		return $days;
	}
	
	public static function countDaysDefault($start, $end) {
		$length = -1;
		$dayLength = 0;
		do {
			$length++;
			$checkLimit = strtotime("+".$length." day",$start);
			$dayLength++;
		} while (date('Y-m-d',$checkLimit) < date('Y-m-d',$end));
				 
		return $dayLength;
	}
	
	public static function countWeekDayOfResa($resa, $ts, $days) {
		$timeslots = $resa->_mainProduct->_timeslotsObj->list;
		if ($resa->startDate=="") $start = strtotime($resa->endDate);
		else $start = strtotime($resa->startDate);
		$end = strtotime($resa->endDate);
		$length = -1;
		$daylength = 0;
		do {
			$length++;
			$checkLimit = strtotime("+".$length." day",$start);
			$startTime = "00:00:00";
			if (array_key_exists($ts, $timeslots)) $startTime = $timeslots[$ts]->startTime;
			$resaEndStartTime = "00:00:00";
			if (array_key_exists($resa->endTimeslot, $timeslots)) $resaEndStartTime = $timeslots[$resa->endTimeslot]->startTime;
			if ($days[date('N',$checkLimit)]==1)
				if ( $ts==0
					or ( ($checkLimit!=$start or strtotime($startTime)>=strtotime($resa->getStartHour()))
						 && ($checkLimit!=$end or strtotime($startTime)<=strtotime($resaEndStartTime)) 
						) )
					$daylength++;
		} while (date('Y-m-d',$checkLimit) != date('Y-m-d',$end)
				 && date('Y-m-d',$checkLimit) < date('Y-m-d',$end) && $length<10000 );
			
		if ($daylength>0 && $days[date('N',$end)]==1 && $resa->_mainProduct->_priceEndTimeSlot && MyOwnCalendarTool::getFirstTimeSlot($timeslots, $end)==$resa->endTimeslot) $daylength--;	
		return $daylength;
	}
	
	public static function countEachWeeks($start, $end) {
		$length = -1;
		$weekLength = 0;
		$currentWeek="";
		do {
			$length++;
			$checkLimit = strtotime("+".$length." day",$start);
			if ($currentWeek!=date('o-W',$checkLimit)) {
				$weekLength++;
				$currentWeek=date('o-W',$checkLimit);
			}
		} while (date('Y-m-d',$checkLimit) != date('Y-m-d',$end)
				 && date('Y-m-d',$checkLimit) < date('Y-m-d',$end) && $length<10000 );
				 
		return $weekLength;
	}
	
	public static function countEachMonths($start, $end) {
		$length = -1;
		$monthLength = 0;
		$currentWeek="";
		do {
			$length++;
			$checkLimit = strtotime("+".$length." day",$start);
			if ($currentWeek!=date('Y-m',$checkLimit)) {
				$monthLength++;
				$currentWeek=date('Y-m',$checkLimit);
			}
		} while (date('Y-m-d',$checkLimit) != date('Y-m-d',$end)
				 && date('Y-m-d',$checkLimit) < date('Y-m-d',$end) && $length<10000 );
				 
		return $monthLength;
	}
	
	//Getting weeks
	//-----------------------------------------------------------------------------------------------------------------
	public static function getWeeksOnPeriod($start, $end) {
		$weekList = array();
		$weekObj=null;
		$weekIndex=$start;

		do {
			if ($weekObj!=null) $weekList[] = $weekObj;
			$weekObj = myOwnCalendarWeek::getOnDate($weekIndex);
			$weekIndex = strtotime("+1 week",$weekIndex);
		} while ($weekObj->getStart()<$end);
		return $weekList;
	}
	
	public static function getFilteredWeeksOnPeriod($start, $end, $_timeSlots, $type, $availabilities = null, $nofilter=false) {
		$filteredWeekList = array();
		$weekList = self::getWeeksOnPeriod($start, $end);
		if (count($_timeSlots->list) > 10) {
			$dstart=strtotime(date('Y-m-d',$start).' 00:00:00');
			$dend=strtotime(date('Y-m-d',$end).' 00:00:00');
		}

		foreach ($weekList as $weekObj) {
			$tsEnableOnWeek=$nofilter;
			foreach ($weekObj->getDays() as $day) {
				if (count($_timeSlots->list) > 100) {
					if ($day<=$dend
						&& $day>=$dstart
						&& $availabilities->isAvailable($day)) {
						$tsEnableOnWeek=true;
						break 1;
					}
				} else {
					foreach ($_timeSlots->getTimeSlotsOfDay($day, $type) AS $timeSlot) {
						//echo ' '.date('Y-m-d H:i:s', $timeSlot->getEndHourTime($day)).' <= '.date('Y-m-d H:i:s', $end);
						//echo ' '.date('Y-m-d H:i:s', $timeSlot->getStartHourTime($day)).' >= '.date('Y-m-d H:i:s', $start);
						if ($timeSlot->getEndHourTime($day)<=$end
							&& $timeSlot->getStartHourTime($day)>=$start
							&& ( $availabilities == null or $timeSlot->isAvailable($day, $availabilities)) ) {
							$tsEnableOnWeek=true;
							break 2;
						}
					}
				}
			}
			
			if ($tsEnableOnWeek) $filteredWeekList[]=$weekObj;
		}
		return $filteredWeekList;
	}
	
	
	public static function getFilteredMonthsForTimeslots($start, $end, $_timeSlots, $type, $availabilities = null, $checkTs) {
		$filteredMonthList = array();
		$monthList = myOwnCalendarMonth::getOnPeriod($start, $end);

		if (!$checkTs or count($_timeSlots->list) > 10) {
			$dstart=strtotime(date('Y-m-d',$start).' 00:00:00');
			$dend=strtotime(date('Y-m-d',$end).' 00:00:00');
		}
		foreach ($monthList as $monthObj) {
			$tsEnableOnMonth=false;

			foreach ($monthObj->getWeeks() as $weekObj) {
				foreach ($weekObj->getDays() as $day)
					if (date('Y-m', $day) == $monthObj->year.'-'.$monthObj->month) {
						if (!$checkTs && count($_timeSlots->list) > 10) {
							
							if ($day<=$dend
								&& $day>=$dstart
								&& $availabilities->isAvailable($day)) {
								$tsEnableOnMonth=true;
								break 1;
							}
						} else {
							foreach ($_timeSlots->getTimeSlotsOfDay($day, $type) AS $timeSlot) 
								//echo '<br>'.date('Y-m-d H:i',$timeSlot->getEndHourTime($day)).'<='.date('Y-m-d H:i',$end);
								//echo '<br>'.date('Y-m-d H:i',$timeSlot->getStartHourTime($day)).'>='.date('Y-m-d H:i',$start);
								//echo '<br>'.(int)$timeSlot->isAvailable($day, $availabilities);
								if ( $timeSlot->getEndHourTime($day)<=$end
									&& $timeSlot->getStartHourTime($day)>=$start
									&& ( $availabilities==null or $timeSlot->isAvailable($day, $availabilities)) ) {
									$tsEnableOnMonth=true;
									//echo '>>';
									break 3;
								}
						}
					}
			}		
			if ($tsEnableOnMonth) $filteredMonthList[]=$monthObj;
		}
		return $filteredMonthList;
	}
	
	public static function getTimeslotsGroups($timeSlotsList, $day=0) {
		$timeslotsGroups=array();
		$i=0;
		if (count($timeSlotsList)>7) {
			$groupsize=floor(count($timeSlotsList)/(ceil(count($timeSlotsList)/6)));
			foreach ($timeSlotsList as $key => $value) {
// 				if ($day==0 || $value->isEnableOnDay($day)) {
					$timeslotsGroups[floor($i/$groupsize)][$key]=$value;
			    	$i++;
// 			    }
			}
		}
		else $timeslotsGroups[]=$timeSlotsList;
		return $timeslotsGroups;
	}
	
	public static function getFilteredMonthsForWeeks($start, $end, $_timeSlots, $type, $dayofweek, $availabilities=null) {
		$filteredMonthList = array();
		$monthList = myOwnCalendarMonth::getOnPeriod($start, $end, $dayofweek, null);
		foreach ($monthList as $monthObj) {
			$tsEnableOnMonth=false;
			foreach ($monthObj->getWeeks() as $weekObj) {
				$day = $weekObj->getSelection();
				//getting timeslot of start or end
				if (array_key_exists($type-1, $_timeSlots->list)) $timeSlot = $_timeSlots->list[$type-1];
				else $timeSlot = $_timeSlots->list[0];
				//echo date('Y-m-d H:i:s', $timeSlot->getEndHourTime($day)).'<='.date('Y-m-d H:i:s', $end);
				if ( ( $availabilities==null or $timeSlot->isAvailable($day, $availabilities))
					&& $timeSlot->getEndHourTime($day)<=$end
					&& $timeSlot->getStartHourTime($day)>=$start)
					$tsEnableOnMonth=true;
			}
						
			if ($tsEnableOnMonth) $filteredMonthList[]=$monthObj;
		}
		return $filteredMonthList;
	}
	
	public static function getFilteredMonthsForMonths($start, $end, $_timeSlots, $type, $selofmonth, $availabilities=null) {
		$filteredMonthList = array();
		$monthList = myOwnCalendarMonth::getOnPeriod($start, $end, null, $selofmonth);

		foreach ($monthList as $monthObj) {
			$monthObj->_dayOfMonth = myOwnCalendarMonth::getDay($monthObj->month, $monthObj->year, ($type===1 ? "start" : "end"), $selofmonth);
			$day = $monthObj->getSelection();
			//getting default timeslot 
			$timeSlot = $_timeSlots->list[0];
			if ( count ($filteredMonthList) > 0
				or ( ( $availabilities==null or $timeSlot->isAvailable($day, $availabilities))
				&& $timeSlot->getEndHourTime($day)<=$end
				&& $timeSlot->getStartHourTime($day)>=$start) )
				$filteredMonthList[]=$monthObj;
		}
		return $filteredMonthList;
	}

	public static function formatDateCompact($time, $lang) {
		$iso = strtolower(Language::getIsoById($lang));
		if ($iso=='en') return date("m/d/Y", $time);
		elseif ($iso=='fr' || $iso=='es') return date("d/m/Y", $time);
		else return date("Y-m-d", $time);
	}
	
    
    public static function getDaysString($langue) {
		return myOwnLang::getDaysNames($langue);
	}
	
	//Getting day from timestamp
	//-----------------------------------------------------------------------------------------------------------------
	public static function getDayString($date,$langue,$short=false) {
		$dayId = date("N",$date);
		$daysNames = myOwnLang::getDaysNames($langue);
		if ($short) return strtolower(substr($daysNames[$dayId],0,3)).'.';
		return $daysNames[$dayId];
	}
	public static function getShortDayString($date,$langue) {
		$dayId = date("N",$date);
		$daysNames = myOwnLang::getDaysNames($langue);
		return substr($daysNames[$dayId],0,3);
	}
	public static function getDayOfMonth($date,$langue=0) {
		return date("j",$date);
	}
	
	//Getting month name from month number
	//-----------------------------------------------------------------------------------------------------------------
	public static function getMonthString($monthNumber,$langue) {
		$labels = myOwnLang::getMonthsNames($langue);
		return $labels[intval($monthNumber)];
	}
	
	public static function getMonthName($date,$langue, $upper=false) {
		$labels = myOwnLang::getMonthsNames($langue);
		$monthNumber = date('m',$date);
		if ($upper) return strtoupper($labels[intval($monthNumber)]);
		else  return $labels[intval($monthNumber)];
	}
	
	//Format month year from timestamp
	//-----------------------------------------------------------------------------------------------------------------
	public static function formatMonthAndYear($date, $langue) {
		$monthNumber = date("n",$date);
		$yearNumber = date("Y",$date);
		return  self::getMonthString($monthNumber,$langue).' '.$yearNumber;
		return $dayStr;
	}
	
	//Format full date with day name from timestamp
	//-----------------------------------------------------------------------------------------------------------------
	public static function formatDateWithDay($date, $langue, $displayMonth=true, $displayYear=true) {
		$iso = strtolower(Language::getIsoById($langue));
		$dayId = date("N",$date);
		$dayNumber = date("j",$date);
		$monthNumber = date("n",$date);
		$yearNumber = date("Y",$date);
		$daysNames = myOwnLang::getDaysNames($langue);
		$monthsNames = myOwnLang::getMonthsNames($langue);
		$dayName = $daysNames[$dayId];
		if ($iso=='en') {
			$ext="th";
			if ($dayNumber==1 or $dayNumber==21 or $dayNumber==31) $ext="st";
			if ($dayNumber==2 or $dayNumber==22) $ext="nd";
			if ($dayNumber==3 or $dayNumber==23) $ext="rd";
			$dayStr = $dayName.' '.($displayMonth ? $monthsNames[$monthNumber].' ' : '').$dayNumber.($displayYear ? ', '.$yearNumber : '');
		} elseif ($iso=='de') {
			$dayStr = $dayName.', '.$dayNumber.($displayMonth ? ' den '.$monthsNames[$monthNumber] : '').($displayYear ? ' '.$yearNumber : '');
		} elseif ($iso=='es') {
			$dayStr = $dayName.' '.$dayNumber.($displayMonth ? ' de '.$monthsNames[$monthNumber] : '').($displayYear ? ' de'.$yearNumber : '');
		} else {
			$dayStr = $dayName.' '.$dayNumber.($displayMonth ? ' '.self::getMonthString($monthNumber,$langue) : '').($displayYear ? ' '.$yearNumber : '');
		}
		return $dayStr;
	}
	
	public static function formatDateWithDayShort($date, $langue) {
		$iso = strtolower(Language::getIsoById($langue));
		$dayId = date("N",$date);
		$dayNumber = date("j",$date);
		$monthNumber = date("n",$date);
		$yearNumber = date("Y",$date);
		if ($iso=='en') {
			$format="m/d/Y";
		} else {
			$format="d/m/Y";
		}
		return substr(self::getDayString($date, $langue),0,1).' '.date($format, $date);
	}
	
	public static function formatDateWithMonthShort($date, $langue) {
		$iso = strtolower(Language::getIsoById($langue));
		$dayId = date("N",$date);
		$short_found=false;
		$sar = 'months_short_'.$iso;
		$monthsNames = myOwnLang::getMonthsNames($langue);
		if (property_exists('myOwnCalendar', $sar)) {
			$monthsNames = self::$$sar;
			$short_found=true;
		}
		$dayNumber = date("j",$date);
		$monthNumber = date("n",$date);
		$yearNumber = date("Y",$date);
		if ($iso=='en') {
			$format="m/d/Y";
			return strtoupper((!$short_found ? mb_substr($monthsNames[$monthNumber], 0, 3) : $monthsNames[$monthNumber])).'. '.$dayNumber.' '.substr($yearNumber, -2, 0);
		} else {
			$format="d/m/Y";
			return $dayNumber.' '.strtoupper((!$short_found ? mb_substr($monthsNames[$monthNumber], 0, 3) : $monthsNames[$monthNumber])).'. '.substr($yearNumber, -2, 0);
		}
		return substr(self::getDayString($date, $langue),0,1).' '.date($format, $date);
	}
	
	//Format full date with day name from timestamp
	//-----------------------------------------------------------------------------------------------------------------
	public static function formatDate($date, $langue, $displayMonth=true, $displayYear=true) {
		$iso = strtolower(Language::getIsoById($langue));
		$dayId = date("N",$date);
		$dayNumber = date("j",$date);
		$monthNumber = date("n",$date);
		$yearNumber = date("Y",$date);
		$monthsNames = myOwnLang::getMonthsNames($langue);
		if ($iso=='en') {
			$ext="th";
			if ($dayNumber==1 or $dayNumber==21 or $dayNumber==31) $ext="st";
			if ($dayNumber==2 or $dayNumber==22) $ext="nd";
			if ($dayNumber==3 or $dayNumber==23) $ext="rd";
			$dayStr = ($displayMonth ? $monthsNames[$monthNumber].' ' : '').$dayNumber.($displayYear ? ', '.$yearNumber : '');
		} elseif ($iso=='de') {
			$daysTab = myOwnLang::getDaysNames($langue);
			$dayStr = $dayNumber.($displayMonth ? ' den '.$monthsNames[$monthNumber] : '').($displayYear ? ' '.$yearNumber : '');
		} elseif ($iso=='es') {
			$daysTab = myOwnLang::getDaysNames($langue);
			$dayStr = $dayNumber.($displayMonth ? ' de '.$monthsNames[$monthNumber] : '').($displayYear ? ' de '.$yearNumber : '');
		} else {
			$dayStr = $dayNumber.($displayMonth ? ' '.self::getMonthString($monthNumber,$langue) : '').($displayYear ? ' '.$yearNumber : '');
		}
		return $dayStr;
	}
	
	//Format short date from timestamp
	//-----------------------------------------------------------------------------------------------------------------
	public static function formatDateShort($date, $langue,$short=false) {
		$iso = strtolower(Language::getIsoById($langue));
		$dayNumber = date("j",$date);
		$sar = 'months_short_'.$iso;
		if ($short) return $dayNumber;
		$monthNumber = date("n",$date);
		if (property_exists('myOwnCalendar', $sar)) {
			$monthShortr = self::$$sar; $monthShort = $monthShortr[$monthNumber];
		} else 
			$monthShort = substr(self::getMonthString($monthNumber,$langue),0,3);

		if ($iso=='en') {
			$ext="th";
			if ($dayNumber==1 or $dayNumber==21 or $dayNumber==31) $ext="st";
			if ($dayNumber==2 or $dayNumber==22) $ext="nd";
			if ($dayNumber==3 or $dayNumber==23) $ext="rd";
			$dayStr = substr(self::getMonthString($monthNumber,$langue),0,3).' '.$dayNumber;
		} else {
			$dayStr = $dayNumber.' '.$monthShort;
		}
		return $dayStr;
	}
	
	public static function formatDateShortWithDay($date, $langue) {
		$iso = strtolower(Language::getIsoById($langue));
		$dayId = date("N",$date);
		$dayNumber = date("j",$date);
		$monthNumber = date("n",$date);
		$yearNumber = date("Y",$date);
		$daysTab = myOwnLang::getDaysNames($langue);
		if ($iso=='en') {
			$ext="th";
			if ($dayNumber==1 or $dayNumber==21 or $dayNumber==31) $ext="st";
			if ($dayNumber==2 or $dayNumber==22) $ext="nd";
			if ($dayNumber==3 or $dayNumber==23) $ext="rd";
			$dayStr = $daysTab[$dayId].' '.$dayNumber.$ext;
		} elseif ($iso=='de') {
			$dayStr = $daysTab[$dayId].', '.$dayNumber;
		} elseif ($iso=='es') {
			$dayStr = $daysTab[$dayId].' '.$dayNumber;
		} else {
			$dayStr = $daysTab[$dayId].' '.$dayNumber;
		}
		return $dayStr;
	}

}
	
?>