<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnProductsRules extends myOwnProducts {
	
	//Construct timeslot list
	//-----------------------------------------------------------------------------------------------------------------
	public function __construct() {
		//MYOWNRES_RES_START_HOUR ??
		$tempProduct = new myOwnProductsRule();
		$tempProduct->sqlId = 0;
		$tempProduct->name = "";
		$tempProduct->email = "";
		$tempProduct->ids_categories = Configuration::get('MYOWNRES_RES_CATEGORY');
		$tempProduct->id_shop = Configuration::get('MYOWNRES_RES_SHOP');
		$tempProduct->ids_products = Configuration::get('MYOWNRES_RES_PRODUCTS');
		$tempProduct->ids_notifs = array_filter(explode(',', Configuration::get('MYOWNRES_RES_NOTIFS')));
		$tempProduct->assignment = (int)Configuration::get('MYOWNRES_RES_ASSIGNMENT', 0);

		$tempProduct->productType = Configuration::get('MYOWNRES_PRODUCT_TYPE');
		$tempProduct->_occupyQty = Configuration::get('MYOWNRES_OCCUPY_QTY');
		$tempProduct->_occupyCombinations = Configuration::get('MYOWNRES_OCCUPY_COMB');

		$tempProduct->email = Configuration::get('MYOWNRES_RES_NOTIF');
		$tempProduct->reservationStartType = Configuration::get('MYOWNRES_RES_START_TYPE');
		if ($tempProduct->reservationStartType==reservation_start::AFTER_DAYS
			or $tempProduct->reservationStartType==reservation_start::SOME_WEEKS)
			$reservationStartParams = Configuration::get('MYOWNRES_RES_START_NB');
		if ($tempProduct->reservationStartType==reservation_start::AFTER_MINUT
			or $tempProduct->reservationStartType==reservation_start::AFTER_HOURS)
			$reservationStartParams = Configuration::get('MYOWNRES_RES_START_NB').';'.Configuration::get('MYOWNRES_RES_START_CURRENT');
		if ($tempProduct->reservationStartType==reservation_start::SAME_DAY)
			$reservationStartParams = Configuration::get('MYOWNRES_RES_START_CURRENT');
	    if ($tempProduct->reservationStartType==reservation_start::NEXT_WEEK)
			$reservationStartParams = Configuration::get('MYOWNRES_RES_START_DAY').';00:00:00';
		$tempProduct->reservationStartParams = $reservationStartParams;
		
		$tempProduct->reservationPeriod = Configuration::get('MYOWNRES_RES_PERIOD');
		
		$tempProduct->_reservationUnit=intval(Configuration::get('MYOWNRES_UNIT'));
		$tempProduct->_reservationMinLength=intval(Configuration::get('MYOWNRES_RES_MIN_LENGTH'));
		if (intval($tempProduct->_reservationMinLength)==0) $tempProduct->_reservationMinLength=1;
		$tempProduct->_reservationMaxLength=intval(Configuration::get('MYOWNRES_RES_MAX_LENGTH'));
		$tempProduct->_reservationLengthControl=intval(Configuration::get('MYOWNRES_RES_LENGTH_CTRL', '-1'));
		$tempProduct->_reservationLengthFreq=intval(Configuration::get('MYOWNRES_RES_FREQ_LENGTH'));
		$tempProduct->_reservationLengthStrict=(Configuration::get('MYOWNRES_RES_FREQ_STRICT')==1);
		$tempProduct->_reservationLengthCanExceed=(Configuration::get('MYOWNRES_RES_LENGTHEXCEED')==1);
		$tempProduct->_reservationLengthQuantity=(Configuration::get('MYOWNRES_RES_LENGTHQTY')==1);
		$tempProduct->_allowResBetweenDays=($tempProduct->reservationSelType != reservation_interval::TIMESLICE && Configuration::get('MYOWNRES_RES_LENGTHBETWDAYS')==1);

		//compatibility with version without this option
		if ($tempProduct->_reservationLengthControl<0) {
			if ($tempProduct->_reservationMinLength==$tempProduct->_reservationMaxLength) {
				if ($tempProduct->_reservationMinLength == 0) $tempProduct->_reservationLengthControl = reservation_length::FREE;
				else $tempProduct->_reservationLengthControl = reservation_length::FIXED;
			} else $tempProduct->_reservationLengthControl = reservation_length::LIMIT;
		}
		$tempProduct->_reservationBetweenShift=intval(Configuration::get('MYOWNRES_BETWEEN_SHIFT'));
		$tempProduct->_allowResOnHolidays=(Configuration::get('MYOWNRES_HOLIDAY_ENABLE')==1);
		$tempProduct->_allowResOnRestDays=($tempProduct->reservationSelType != reservation_interval::TIMESLICE
											&& (Configuration::get('MYOWNRES_RESTDAY_ENABLE')==1 
												|| ($tempProduct->_reservationUnit!=reservation_unit::DAY  
													&& $tempProduct->_reservationUnit!=reservation_unit::TIMESLOT) ) );
		
		$tempProduct->reservationSelType = Configuration::get('MYOWNRES_SEL_TYPE');
		//$tempProduct->_reservationSelMultiple = intval(Configuration::get('MYOWNRES_SEL_MULTI'));
		$tempProduct->_reservationSelMode = intval(Configuration::get('MYOWNRES_SEL_MODE'));
		$tempProduct->_reservationSelMultiple = ($tempProduct->_reservationSelMode == reservation_mode::MULTIPLE);
		$tempProduct->reservationSelPlanning = Configuration::get('MYOWNRES_SEL_PLANNING');

		$tempProduct->_reservationSelDays=Configuration::get('MYOWNRES_SEL_DAYS');
		$tempProduct->_reservationSelWeekStart=intval(Configuration::get('MYOWNRES_SEL_STARTDAY'));
		$tempProduct->_reservationSelWeekEnd=intval(Configuration::get('MYOWNRES_SEL_ENDDAY'));
		$tempProduct->_reservationSelMonthStart=Configuration::get('MYOWNRES_SEL_STARTMONTH');
		$tempProduct->_reservationSelMonthEnd=Configuration::get('MYOWNRES_SEL_ENDMONTH');
		$tempProduct->_reservationShowTime=Configuration::get('MYOWNRES_SEL_SHOWTIME');
		$tempProduct->_reservationStartTime=Configuration::get('MYOWNRES_SEL_STARTTIME', '00:00');
		$tempProduct->_reservationEndTime=Configuration::get('MYOWNRES_SEL_ENDTIME', '00:00');
		$tempProduct->_reservationCutTime=Configuration::get('MYOWNRES_SEL_CUTTIME', false);
		$tempProduct->_reservationTimeByDay=Configuration::get('MYOWNRES_SEL_TIMEBYDAY', true);
		$tempProduct->_timeSliceLength=Configuration::get('MYOWNRES_TIMESLICE_LEN', 15);
	
		$tempProduct->_reservationPriceType=(intval(Configuration::get('MYOWNRES_PRICE_CALC')));
		$tempProduct->_priceDoubleIfLength=(intval(Configuration::get('MYOWNRES_PRICE_DOUBLE')));
		$tempProduct->_priceCombination=(intval(Configuration::get('MYOWNRES_PRICE_COMBINATION')));
		$tempProduct->_coefPerProduct=(intval(Configuration::get('MYOWNRES_PRICE_COEFPROD')));
		$priceUnit = Configuration::get('MYOWNRES_PRICE_UNIT','-1');
		if ($priceUnit=='') $tempProduct->_priceUnit = $tempProduct->_reservationUnit;
		else $tempProduct->_priceUnit = $priceUnit;
		$tempProduct->_priceUnitSameAsLength = ($priceUnit=='' || $tempProduct->_priceUnit == $tempProduct->_reservationUnit);

		$tempProduct->_priceEndTimeSlot=(Configuration::get('MYOWNRES_ENDTS_IGNORE')==1);
		$tempProduct->_priceDisabledTimeSlots=(Configuration::get('MYOWNRES_DISABLED_PRICE')==1);
		$tempProduct->_priceStrict=(Configuration::get('MYOWNRES_PRICE_STRICT')==1);
		$tempProduct->_pricePreview=(Configuration::get('MYOWNRES_PRICE_PREVIEW')==1);
		$tempProduct->_priceHolidays=(Configuration::get('MYOWNRES_PRICE_HOLIDAYS')==1);
		
		$tempProduct->_qtyDisplay=intval(Configuration::get('MYOWNRES_QTY_DISPLAY'));
		$tempProduct->_qtyType=intval(Configuration::get('MYOWNRES_QTY_TYPE'));
		$tempProduct->_qtySearch=intval(Configuration::get('MYOWNRES_QTY_SEARCH'));
		///$tempProduct->_occupyCombinations = occupy_combinations::IGNORE;
		$tempProduct->_qtyFixed=intval(Configuration::get('MYOWNRES_QTY_FIXED'));
		if (!$tempProduct->_qtyFixed) $tempProduct->_qtyFixed=1;
		$tempProduct->_qtyCapacity=intval(Configuration::get('MYOWNRES_QTY_CAPACITY'));
		
		$tempProduct->_optionIgnoreAttrStock=( $tempProduct->_qtyType==reservation_qty::SEARCH or $tempProduct->productType==product_type::MUTUEL or ($tempProduct->productType==product_type::VIRTUAL 
												&& ($tempProduct->_occupyCombinations==occupy_combinations::SUM or $tempProduct->_occupyCombinations==occupy_combinations::DEF)));
		$tempProduct->_optionAlsoSell=(Configuration::get('MYOWNRES_RES_SELL') && ($tempProduct->productType==product_type::PRODUCT));
		$tempProduct->_optionValidationStatus=Configuration::get('MYOWNRES_RES_STATUS');
		
		$tempProduct->ids_options=array_filter(explode(',', Configuration::get('MYOWNRES_RES_OPTIONS')));

		$tempProduct->_depositAmount = Configuration::get('MYOWNRES_ORDER_DEPOSIT', 0);
		$tempProduct->_depositCalc = Configuration::get('MYOWNRES_DEPOSIT_CALC',0);
		$tempProduct->_depositByProduct = Configuration::get('MYOWNRES_DEPOSIT_BYPROD', 0);
		$tempProduct->_advanceRate = Configuration::get('MYOWNRES_ORDER_ADVANCE', 100);
		$tempProduct->_advanceAmount = Configuration::get('MYOWNRES_ORDER_FIXEDADVANCE', 0);
		
		$tempProduct->_timeslotsObj = new myOwnTimeSlots();
		
		$tempProduct->_timeslotsObj->list = $tempProduct->getTimeslots(true);
		
		$this->list[$tempProduct->sqlId] = $tempProduct;
	}
	public function getGlobal($isproducts=false) {
		return $this->list[0];
	}
}

?>