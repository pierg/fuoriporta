<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnPricesSets {
	var $list;
	
	//Construct rules list
	//-----------------------------------------------------------------------------------------------------------------
	public function __construct() {
		$this->list=array();
	}
	
	public static function getPricesSetForProduct($id_product, $id_product_attribute, $tax_rate=0) {
		$list=array();
		$pricessetSql = Db::getInstance()->ExecuteS("SELECT * FROM `"._DB_PREFIX_."myownreservations_pricesset` WHERE `id_product` = ".(int)$id_product." AND `id_product_attribute` = ".(int)$id_product_attribute);
		foreach($pricessetSql AS $pricesset) {
			$list[$pricesset['period']] = $pricesset['amount']*(1+($tax_rate/100));
		}
		return $list;
	}
	
	public static function getPricesSetForFamily($productFamilly, $kind=0) {
		$list=array();
		$pricessetSql = Db::getInstance()->ExecuteS("SELECT * FROM `"._DB_PREFIX_."myownreservations_pricesset` WHERE `id_product` = ".(int)-$productFamilly->sqlId." AND `id_product_attribute` = ".(int)$kind);
		foreach($pricessetSql AS $pricesset) {
			$list[$pricesset['period']] = $pricesset['amount'];
		}
		return $list;
	}
	
	public static function isPricesSetForProduct($id_product) {
		$list=array();
		$pricessetSql = Db::getInstance()->ExecuteS("SELECT * FROM `"._DB_PREFIX_."myownreservations_pricesset` WHERE `id_product` = ".(int)$id_product." ");

		return count($pricessetSql);
	}
	
	public static function deletePricesSetForProduct($id_product, $id_product_attribute) {
		return Db::getInstance()->Execute("DELETE FROM `"._DB_PREFIX_."myownreservations_pricesset` WHERE `id_product` = ".(int)$id_product." AND `id_product_attribute` = ".(int)$id_product_attribute);
	}
	
}


?>