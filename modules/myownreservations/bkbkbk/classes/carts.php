<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnCarts {
	var $list=array();
	var $id_cart;
	var $fee=0;
	var $fee_details=array();
	var $total=0;
	var $total_wt=0;
	var $total_goods=0;
	var $total_goods_wt=0;
	var $advance=0;
	var $advance_wt=0;
	var $deposit=0;
	var $suppl=0;
	var $suppl_wt=0;
	var $cartChanged=false;
	var $reservedProdCnt=0;
	var $isReservation=false;
	var $isPurchase=false;
	
	public function __construct($id_cart, $_products, $_pricerules, $_extensions=null)
	{
		if (intval($id_cart) > 0) {
			$this->id_cart = $id_cart;
			$this->list = myOwnCarts::getProducts($id_cart, $_products, $_pricerules);
		}
		
		if ($_extensions!=null) {
			//Exec extenssion cart
			foreach ($_extensions->getExtByType(extension_type::CART) as $ext) 
				if (method_exists($ext, "execShoppingCart"))
					$ext->execShoppingCart($id_cart);
						
		  	//Add extenssion attachements
			foreach ($_extensions->getExtByType(extension_type::GLOBFEE) as $ext) 
				if (method_exists($ext, "getGlobalFee")) {
					$dfee = $ext->getGlobalFee($id_cart, $this);
					$this->fee+=$dfee;
					if ($dfee>0) $this->fee_details[get_class($ext)]=$dfee;
				}
		}

		//caculating deposit
		$total=0;$global=Configuration::get('MYOWNRES_DEPOSIT_GLOBAL');

		foreach ($this->getFamiliesResas() as $key => $famillyResas) {
			$deposit=0;
			$mainProduct = $_products->list[$key];
			$amount=intval($mainProduct->_depositAmount);
			if (count($famillyResas))
				$this->isReservation=true;

			switch ($mainProduct->_depositCalc) {
			    case deposit_type::ONCE:
			        $deposit = (count($famillyResas) ? $amount : 0);
			        break;
			    case deposit_type::PRODUCT:
			    	$products=array();
			    	foreach($famillyResas as $famillyResa)
			    		if (!in_array($famillyResa->id_product, $products))
			    			$products[]=$famillyResa->id_product;
			    	if ($mainProduct->_depositByProduct) {
			    		foreach ($products as $id_product) {
							$prod_amount = Configuration::get('MYOWNRES_RES_DEPOSIT_'.$id_product, 0);
							$deposit += ($prod_amount > 0 ? $prod_amount : $amount);
						}
			        } else $deposit = (count($products)*$amount);
			        break;
			    case deposit_type::RESA:
			    	if ($mainProduct->_depositByProduct) {
			    		foreach($famillyResas as $famillyResa) {
			    			$prod_amount = Configuration::get('MYOWNRES_RES_DEPOSIT_'.$famillyResa->id_product, 0);
							$deposit += ($prod_amount > 0 ? $prod_amount : $amount);
			    		}
			    	} else $deposit = count($famillyResas)*$amount;
		
			        break;
			    case deposit_type::QTY:
			    	if ($mainProduct->_depositByProduct) {
			    		foreach($famillyResas as $famillyResa) {
			    			$prod_amount = Configuration::get('MYOWNRES_RES_DEPOSIT_'.$famillyResa->id_product, 0);
							$deposit +=  $famillyResa->quantity*($prod_amount > 0 ? $prod_amount : $amount);
			    		}
			    	} else {
			    		foreach ($famillyResas as $famillyResa)
			        		$deposit += $famillyResa->quantity*$amount;
					}
			        break;
			}
			if (!$global)
				$this->deposit += $deposit;
			else if ($deposit>$total) 
				$this->deposit = $deposit;
		}
	}
	
	public static function getAllCustomerCarts($id_customer, $negative=false, $reject=0) {
		$sql='SELECT id_cartproduct FROM `ps_myownreservations_cartproducts` LEFT JOIN `ps_cart` ON (`ps_myownreservations_cartproducts`.`id_cart` = `ps_cart`.`id_cart`) WHERE `id_customer` = '.$id_customer;
		$ret=array();
		$res = Db::getInstance()->ExecuteS($sql);
		foreach ($res as $re) {	
			if ($reject==0 || (int)$re['id_cartproduct']!=$reject) {
				if ($negative) $ret[] = -$re['id_cartproduct'];
				else $ret[] = $re['id_cartproduct'];
			}
		}
		return $ret;
	}
	
	public function getAllOldCustomerCarts($id_customer, $negative=false) {
		$sql='SELECT id_cartproduct FROM `ps_myownreservations_cartproducts` LEFT JOIN `ps_cart` ON (`ps_myownreservations_cartproducts`.`id_cart` = `ps_cart`.`id_cart`) WHERE `id_customer` = '.$id_customer;
		$ret=array();
		$exc=array();
		foreach ($this->list as $cartitem)
			$exc[]=$cartitem->sqlId;
		$res = Db::getInstance()->ExecuteS($sql);

		foreach ($res as $re) {	
		//echo 'array_key_exists'.$re['id_cartproduct'];
			if (!in_array($re['id_cartproduct'], $exc)) {
				if ($negative) $ret[] = -$re['id_cartproduct'];
				else $ret[] = $re['id_cartproduct'];
			}
		}

		return $ret;
	}
	
	public function getResas($id_product=-1, $id_product_attribute=-1, $ignorePack=false) {
		global $cookie;
		$resas = array();

		if (!$ignorePack && Configuration::get('PS_PACK_STOCK_TYPE') == 1) {
			$packs=array();
			if (Pack::isPack($id_product)) {
				$packitems = Pack::getItems($id_product, $cookie->id_lang);
	
				foreach ($packitems as $packitem) {
					$packs[] = $packitem->id.'-'.$packitem->id_pack_product_attribute;
					if (Pack::isPacked($packitem->id)) {
						$packstmp = Pack::getPacksTable($packitem->id, $cookie->id_lang, false);
						foreach ($packstmp as $pack) {
							$packs[] = $pack['id_product'].'-'.$pack['id_product_attribute'];
						}
						
					}
				}
			}
			if (Pack::isPacked($id_product)) {
				$packstmp = Pack::getPacksTable($id_product, $cookie->id_lang, false);
				foreach ($packstmp as $pack) {
					$packs[] = $pack['id_product'].'-'.$pack['id_product_attribute'];
				}
				
			}
			//get cart reservation for prod and packs with this prod
			foreach($this->list AS $cart) {
				//check attribute if 
				if (in_array($cart->id_product.'-'.$cart->id_product_attribute, $packs)
					OR ($cart->id_product==$id_product && ($id_product_attribute==-1 or $cart->id_product_attribute == $id_product_attribute)) )
					$resas[] = $cart;
			}
		} else 
			foreach($this->list AS $cart)
				if (($id_product==-1 or $cart->id_product==$id_product) && ($id_product_attribute==-1 or $cart->id_product_attribute == $id_product_attribute) )
					$resas[] = $cart;

		return $resas;
	}
	
	public function isResaSamePeriodAsCart($resa) {
		$firstcart=null;
		foreach($this->list AS $cart)
			$firstcart = $cart;
		if ($firstcart!=null) {
			if ($firstcart->startDate==$resa->startDate && $firstcart->endDate==$resa->endDate && $firstcart->startTimeslot==$resa->startTimeslot && $firstcart->endTimeslot==$resa->endTimeslot)
				return true;
			else return false;
		} else return true;
	}
	
	public function getResasTotal() {
		$total = 0;

		foreach($this->list AS $cart)
			$total += $cart->getPriceTaxes(true);

		return $total;
	}
	
	public function getResasQty($id_product, $id_product_attribute=-1) {
		$qty=0;
		foreach ($this->getResas($id_product, $id_product_attribute) as $resa)
			$qty += $resa->quantity;
		return $qty;
	}
	
	public function getFamillyResas($mainProduct) {
		$resas = array();
		//$products = $mainProduct->getProductsId();
		foreach($this->list AS $cart)
			if ($cart->_mainProduct->sqlId==$mainProduct->sqlId) //if ( in_array($cart->id_product, $products) )
				$resas[] = $cart;

		return $resas;
	}
	
	public function getFamiliesResas() {
		$familliesResas = array();
		foreach($this->list AS $resa) {
			if (!array_key_exists($resa->_mainProduct->sqlId, $familliesResas))
				$familliesResas[$resa->_mainProduct->sqlId]=array();
			$familliesResas[$resa->_mainProduct->sqlId][$resa->sqlId]=$resa;
		}
		return $familliesResas;
	}
	
	public static function getResCartFromResa($obj, $resa) {
		$id_cartproduct=0;
		if ($resa->id_order) {
			//$order=new Order($resa->id_order);//echo '()'.$order->id_cart;echo ''.$resa->sqlId;
			$id_cart = MyOwnUtils::getCartIdFromOrder($resa->id_order);
			
			//$resaCarts = new myOwnCarts($id_cart, $obj->_products, $obj->_pricerules, $obj->_extensions);
			$resaCarts = new myOwnCarts(0, $obj->_products, $obj->_pricerules);
			$resaCarts->list = myOwnCarts::getProducts($id_cart, $obj->_products, $obj->_pricerules, null, null, false);
			foreach ($resaCarts->list as $resaCart) {
				/*echo '<br>'.(int)$resaCart->sqlId;
				echo '@'.$resaCart->id_customization.' == '.$resa->id_customization;
				echo '#'.$resaCart->id_product.' == '.$resa->id_product;
				echo '_'.$resaCart->id_product_attribute.' == '.$resa->id_product_attribute;
				echo '_'.$resaCart->startDate.' == '.$resa->startDate;
				echo '_'.$resaCart->endDate.' == '.$resa->endDate;*/
				if ($resaCart->id_product == $resa->id_product 
					&& $resaCart->id_product_attribute == $resa->id_product_attribute
					&& $resaCart->id_customization == $resa->id_customization
					&& $resaCart->startDate == $resa->startDate
					&& $resaCart->startTimeslot == $resa->startTimeslot
					&& $resaCart->endDate == $resa->endDate
					&& $resaCart->endTimeslot == $resa->endTimeslot) {
						return $resaCart;
				}
			}
		}
		return null;
	}
	
	public static function getResCartIdFromResa($obj, $resa) {
		$id_cartproduct=0;
		if ($resa->id_cartproduct) return $resa->id_cartproduct;
		if ($resa->id_order) {
			//$order=new Order($resa->id_order);//echo '()'.$order->id_cart;echo ''.$resa->sqlId;
			$id_cart = MyOwnUtils::getCartIdFromOrder($resa->id_order);
			
			//$resaCarts = new myOwnCarts($id_cart, $obj->_products, $obj->_pricerules, $obj->_extensions);
			$resaCarts = new myOwnCarts(0, $obj->_products, $obj->_pricerules);
			$resaCarts->list = myOwnCarts::getProducts($id_cart, $obj->_products, $obj->_pricerules, null, null, false);
			foreach ($resaCarts->list as $resaCart) {
				/*echo '<br>'.(int)$resaCart->sqlId;
				echo '@'.$resaCart->id_customization.' == '.$resa->id_customization;
				echo '#'.$resaCart->id_product.' == '.$resa->id_product;
				echo '_'.$resaCart->id_product_attribute.' == '.$resa->id_product_attribute;
				echo '_'.$resaCart->startDate.' == '.$resa->startDate;
				echo '_'.$resaCart->endDate.' == '.$resa->endDate;*/
				if ($resaCart->id_product == $resa->id_product 
					&& $resaCart->id_product_attribute == $resa->id_product_attribute
					&& $resaCart->id_customization == $resa->id_customization
					&& $resaCart->startDate == $resa->startDate
					&& $resaCart->startTimeslot == $resa->startTimeslot
					&& $resaCart->endDate == $resa->endDate
					&& $resaCart->endTimeslot == $resa->endTimeslot) {
						$id_cartproduct = $resaCart->sqlId;
						$resa->id_cartproduct = $id_cartproduct;
						//echo '@id_cartproduct'.$id_cartproduct;
						$resa->sqlUpdate();
						break;
				}
			}
		}
		return $id_cartproduct;
	}
	
	//Products of cart				
	//-----------------------------------------------------------------------------------------------------------------		
	public function checkProducts($obj, $cart, $products) {
		foreach ($products AS $key => $product) {
			$resasQty = $this->getResasQty($product['id_product'], $product['id_product_attribute']);
			$mainProduct = $obj->_products->getResProductFromProduct($product['id_product']);
			if ($mainProduct!=null && $resasQty<$product['cart_quantity'] && !$mainProduct->_optionAlsoSell) {
				//content_only patch for pix-loc bug
				if ($resasQty==0 && !Tools::getIsset('content_only')) $cart->deleteProduct($product['id_product'], $product['id_product_attribute']);
			}
		}
	}
	
	
	public function changeProducts($obj, $products, $customProducts=false, $payment=false, $displayPrice=true, $changeAttribute=true) {
		global $cookie, $cart;
		$fixedAdvance=0;//intval(Configuration::get('MYOWNRES_ORDER_FIXEDADVANCE'));
		//if ($this->suppl_wt!=0 && !$customProducts && $payment) return $products;
		$ispricechangedone=($this->cartChanged);
		//if custom products on second pass we must not recalculate sums
		if (!$customProducts && !$ispricechangedone) {
			$this->total=0;
			$this->total_wt=0;
			$this->total_goods=0;
			$this->total_goods_wt=0;
			$this->advance=0;
			$this->advance_wt=0;
			$this->reservedProdCnt=0;
		}
		$productsResv=array();
		
		foreach ($products AS $key => $product) {
			if (array_key_exists('price_wt', $product)) $productPrice_wt=$product['price_wt'];
			else $productPrice_wt=$product['unit_price_tax_incl'];
			$productPrice=$product['price'];
// 			$productPrice_wt=$product['price_wt'];
			if (array_key_exists('total', $product)) $productTotal=$product['total'];
			else $productTotal=$product['total_price_tax_excl'];
			
			$productTotal_wt=$product['total_wt'];
			if (array_key_exists('cart_quantity', $product)) $productQty = $product['cart_quantity'];
			else $productQty = $product['product_quantity'];
			$product['resas_total_wt']=0;
			$product['resas_total']=0;
			$product['resas_qty']=0;
			$originalAttr='';
			if (array_key_exists('attributes', $product)) $originalAttr=$product['attributes'];

			//some times we have to change product from order details and not from cart
			if (isset($product['id_product_attribute'])) $resas = $this->getResas($product['id_product'], $product['id_product_attribute'], true);
			else $resas = $this->getResas($product['product_id'], $product['product_attribute_id'], true);

			if (count($resas)>0) {
				$this->reservedProdCnt++;
     			$resaStrSmall='';
				if (array_key_exists('attributes_small', $product)) $resaStrSmall=$product['attributes_small'];
     			if (!$payment) $resaStrSmall .= "</a>";

     			foreach ($resas AS $resa) {
	     			//displaying resa as attributes 
	     			if ($resaStrSmall!="</a>") $resaStrSmall .= "<br />";
	     			if (!$payment) {
	     				
	     				if (count($resas)>1) $resaStrSmall .= '&bull; ';
	     				if ($resa->quantity < $productQty) //$resa->quantity > 1 && 
	     					$resaStrSmall .= $resa->quantity.'x ';
     				}
     				$resaStrSmall .= $resa->toString($obj, $cookie->id_lang, true);
           			$advance = ($resa->_mainProduct->_advanceRate/100);
           			if (
           				(Configuration::get('PS_SHOP_ENABLE') && Configuration::get('MYOWNRES_NO_LOC_ADVANCE') && in_array(Tools::getRemoteAddr(), explode(',', Configuration::get('PS_MAINTENANCE_IP'))))
           				 OR (Configuration::get('MYOWNRES_MIN_FOR_ADVANCE') && $this->getResasTotal()<Configuration::get('MYOWNRES_MIN_FOR_ADVANCE'))
           				 OR (Tools::getValue('controller')=='AdminCarts' || Tools::getValue('controller')=='adminmyownreservations')
           				) //PS_SHOP_ENABLE
		   				$advance = 1;
		   			
		   			$product['resas_qty']+=$resa->quantity;
		   			$product['mainProduct']=$resa->_mainProduct->sqlId;
		   			$product['test']=Tools::getValue('controller').' '.$resa->_productPrice;
           			$product['resas_total_wt']+= $resa->getPriceTaxes(true)*$displayPrice;
           			//echo '@'.$resa->_productPrice.':'.$resa->getPriceTaxes(true);
           			$product['resas_total']+= $resa->getPriceTaxes(false)*$displayPrice;
           			$product['advance_wt'] = $product['resas_total_wt']*$advance+$resa->_mainProduct->_advanceAmount;
           			$product['advance'] = $product['resas_total']*$advance+$resa->_mainProduct->_advanceAmount;

           			//duplicate line by resa for payement
           			if ($payment || _PS_VERSION_ >= '1.7.0.0') {
           				//$product['attributes'] = $resaStrSmall.(_PS_VERSION_ >= '1.7.0.0' ? ':' : '');
           				$product['allow_oosp'] = 1;
           				$product['id_resa'] = $resa->sqlId;
           				$product['id_customization'] = -$resa->sqlId;
           				$product['cart_quantity'] = $resa->quantity;
           				$product['quantity'] = $resa->quantity;
           				$product['price_wt'] = $resa->getUnitPriceWithReduc(true)*$advance*$displayPrice;
           				$product['price'] = $resa->getUnitPriceWithReduc(false)*$advance*$displayPrice;
           				//$product['total_wt'] = $product['resas_total_wt']*$advance;
		   				//$product['total'] = $product['resas_total']*$advance;
			   			$product['total_wt'] = $resa->getPriceTaxes(true)*$advance;
			   			$product['total'] = $resa->getPriceTaxes(false)*$advance;
           				$product['is_reservation'] = true;
           				$productsResv[] = $product;//if ($advance>0) 
           			}
           		}
           		
           		$prod_amount=0;	
				if ($resa->_mainProduct!=null && $resa->_mainProduct->_depositByProduct)
					$prod_amount = Configuration::get('MYOWNRES_RES_DEPOSIT_'.$resa->id_product, 0);
				
				//$id_currency = $cart->id_currency;
				$id_currency = myOwnUtils::getCurrencyObj()->id;
				$currency = new Currency($id_currency);
				if ($prod_amount >0) $resaStrSmall .= '<br/><b>'.$obj->l('Deposit', 'cart').'</b> : '.($resa->_mainProduct->_depositCalc==deposit_type::QTY ? $resa->quantity.'x ' : '').Tools::displayPrice($prod_amount, $currency, false);
					
           		//resa summ
           		if (!$customProducts && !$ispricechangedone) {
	     			$this->total += $product['resas_total'];
	     			$this->total_wt += $product['resas_total_wt'];
					$this->advance += $product['advance'];
					$this->advance_wt += $product['advance_wt'];
				}
     			$product['allow_oosp'] = 1;
     			
     			//purchase with reservation (addind a new line for payment)
				if ($payment && $product['resas_qty'] < $productQty) {
     				$product['attributes'] = $originalAttr;
					$product['price_wt'] = $productPrice_wt;
           			$product['price'] = $productPrice;
           			$product['total_wt'] = $productPrice_wt*($productQty-$product['resas_qty']);
           			$product['total'] = $productPrice*($productQty-$product['resas_qty']);
					$product['cart_quantity']=($productQty-$product['resas_qty']);
					$productsResv[] = $product;
				}

				$newProductTotal = (($productQty-$product['resas_qty'])*$productPrice)+$product['resas_total'];
				$newProductTotal_wt = (($productQty-$product['resas_qty'])*$productPrice_wt)+$product['resas_total_wt'];
				if ($product['resas_qty']>0 && !$ispricechangedone
					) { //&& (!array_key_exists('is_reservation', $product) || !$product['is_reservation'])
     				$this->suppl_wt += ($newProductTotal_wt*$advance+$resa->_mainProduct->_advanceAmount)-$productTotal_wt;//$product['total_wt']);
     				$this->suppl += ($newProductTotal*$advance+$resa->_mainProduct->_advanceAmount)-$productTotal;//$product['total']);
     			}
				$product['total'] = $newProductTotal;
				$product['total_wt'] = $newProductTotal_wt;
				
				$product['id_customization'] = 99;
				$product['is_reservation'] = true;
     			if ($changeAttribute) $product['attributes_small']=$resaStrSmall;
     			
     			//prevent out of stock probelm
     			if (array_key_exists('stock_quantity', $product)) {
					$product['stock_quantity'] = $resa->_mainProduct->_qtyCapacity*$product['stock_quantity'];
					if (($product['stock_quantity']-$productQty)<0) $product['stock_quantity'] = $productQty;
				}

     			if (!$payment && _PS_VERSION_ < '1.7.0.0') $productsResv[] = $product;
			} else {
				if (!$customProducts && !$ispricechangedone) {
					$this->total_goods+=$product['total'];
					$this->total_goods_wt+=$product['total_wt'];
					$this->isPurchase=true;
				}
				$product['is_reservation'] = false;
				$productsResv[] = $product;
			}
		}
		$this->cartChanged=true;
		return $productsResv;
	}
	
	public function constructFromPost($obj, $mainProduct, $id_product, $id_product_attribute, $quantity, $post) {
		global $cookie;
		$reservations=array();
		if ($id_product_attribute==NULL) $id_product_attribute=0;
		$postResas = explode('|',$post);
		foreach ($postResas as $postResa) {
			$tempResa = new myOwnCart();
			$tempResa->id_product = $id_product;
    		$tempResa->id_product_attribute = $id_product_attribute;
    		$tempResa->id_object = (int)Tools::getValue('place', 0);
   		    $tempResa->_mainProduct = $mainProduct;
   		    
   		    $qtyExts = $obj->_extensions->getExtByQty();
					
			if (array_key_exists($mainProduct->_qtyType, $qtyExts))
				$tempResa->quantity = $qtyExts[$mainProduct->_qtyType]->getQtyCnt($mainProduct, $quantity);
    		else if ($mainProduct->_qtyType == reservation_qty::FIXED)
    			$tempResa->quantity = $mainProduct->_qtyFixed;
    		else $tempResa->quantity = $quantity;
    		
			if (stripos($postResa, '@') !== false) {
			
				$postResaArgs = explode('@',$postResa);
				$postResaStart = explode('_',$postResaArgs[0]);
				$tempResa->startTimeslot = $postResaStart[1];
	    		$tempResa->startDate = $postResaStart[0];
	    		
	    		if (count($postResaArgs) > 1 && trim($postResaArgs[1])!='') $postResaEnd = explode('_',$postResaArgs[1]);
				else $postResaEnd = explode('_',$postResaArgs[0]);
	
				if (count($postResaEnd)>1) {
		    		$tempResa->endTimeslot = $postResaEnd[1];
		    		$tempResa->endDate = $postResaEnd[0];

					if (count($postResaArgs)>2) 
		    			$tempResa->id_object = intval($postResaArgs[2]);
		    		$reservations[] = $tempResa;
	    		}
			}
			if (stripos($postResa, '!') !== false) {
				$postResaArgs = explode('!',$postResa);
				$postResaStart = explode('_',$postResaArgs[0]);
				$tempResa->startTimeslot = $postResaStart[1];
	    		$tempResa->startDate = $postResaStart[0];
	    		
	    		if (count($postResaArgs) > 1)
	    			if (stripos($postResaArgs[1], '@')!==false) {
	    				$postResaArgsTmp = explode('@',$postResaArgs[1]);
	    				$postResaLength = intval($postResaArgsTmp[0]);
	    				$tempResa->id_object = intval($postResaArgsTmp[1]);
	    			} else $postResaLength = intval($postResaArgs[1]);
	    		else $postResaLength = 1;
	    		$endDate=0;
	    		$endTimeslot='';
				$tempResa->_mainProduct->getShiftTimeSlot(strtotime($tempResa->startDate), $tempResa->startTimeslot, $postResaLength, false, null, $endDate, $endTimeslot, $tempResa->id_product, $tempResa->id_product_attribute);
			
				$tempResa->endDate = date('Y-m-d',$endDate);
				$tempResa->endTimeslot = $endTimeslot;
				$tempResa->_fixedLength = $postResaLength;
				
				$reservations[] = $tempResa;
			}
		}
		return self::factorizeResas($mainProduct, $reservations);
	}
	
	public static function factorizeResas($mainProduct, $reservations) {
		$orderedResas=array();
		foreach ($reservations as $reservation)
			$orderedResas[$reservation->getStartDateTime()] = $reservation;
		
		ksort($orderedResas);
		$tempResa=null;
		$factorizedResas=array();
		foreach ($orderedResas as $reservation) {
			if ($tempResa==null)
				$tempResa=$reservation;
			else {
				if ($reservation->getStartDateTime()==$tempResa->getEndDateTime() && $reservation->id_object==$tempResa->id_object) {
					$tempResa->endDate = $reservation->endDate;
					$tempResa->endTimeslot = $reservation->endTimeslot;
				} else {
					$factorizedResas[]=$tempResa;
					$tempResa=$reservation;
				}
			}
		}
		if ($tempResa!=null) $factorizedResas[]=$tempResa;
		
		return $factorizedResas;
	}
	
	public function isCustomization($id_customization) {
		$resas = array();
		foreach($this->list AS $cart)
			if ($cart->id_customization == $id_customization )
				return true;

		return false;
	}
	
	public function updateQty($obj, $id_product, $id_product_attribute, $id_custom_or_resa_cnt, $operator, $cartQuantity, $cart, $availabilities, $stocks) {
		$resaToChange=null;
		foreach($this->list AS &$resa) {
			if ($resa->id_product==$id_product && $resa->id_product_attribute == $id_product_attribute ) {

				if (($resa->id_customization>0 && $resa->id_customization==$id_custom_or_resa_cnt)
					or ($id_custom_or_resa_cnt<0 && $resa->sqlId==-$id_custom_or_resa_cnt) 
					or $id_custom_or_resa_cnt==0) {
					if ($cartQuantity>0) $res= $resa->checkAndUpdateQty($operator, $cartQuantity, $cart, $availabilities, $stocks);
					else $res=true;

					if ($resa->_mainProduct->_reservationLengthQuantity) {
						$desiredShift = $resa->_mainProduct->getDesiredShift($resa);
						$realShif = $resa->_mainProduct->getShiftTimeSlot(strtotime($resa->startDate), $resa->startTimeslot, $desiredShift, true, null, $endDate, $resa->endTimeslot, $resa->id_product, $resa->id_product_attribute);
						if ($realShif<0)
							return false;
						else $resa->sqlUpdate();
					}
					
					foreach ($obj->_extensions->getExtByType(extension_type::CARTUPDATE) as $ext) {
						if (method_exists($ext, "execCartUpdate"))
							$ext->execCartUpdate($obj, $resa);
					}
					
					return $res;
				}
			}
		}
		return false;
	}
	
	public function getResa($obj, $id_product, $id_product_attribute, $id_custom_or_resa_cnt) {
		$resaToChange=null;
		foreach($this->list AS &$resa) {
			if ($resa->id_product==$id_product && $resa->id_product_attribute == $id_product_attribute ) {

				if (($resa->id_customization>0 && $resa->id_customization==$id_custom_or_resa_cnt)
					or ($id_custom_or_resa_cnt<0 && $resa->sqlId==-$id_custom_or_resa_cnt) 
					or $id_custom_or_resa_cnt==0) {
					return $resa;
				}
			}
		}
		return false;
	}
	
	public function checkUpdateQty($id_product, $id_product_attribute, $id_custom_or_resa_cnt, $operator, $cartQuantity, $cart, $availabilities, $stocks) {
		$resaToChange=null;
		foreach($this->list AS $resastemp) {
			//$resastemp = clone $resa;
			if ($resastemp->id_product==$id_product && $resastemp->id_product_attribute == $id_product_attribute ) {

				if (($resastemp->id_customization>0 && $resastemp->id_customization==$id_custom_or_resa_cnt)
					or ($id_custom_or_resa_cnt<0 && $resastemp->sqlId==-$id_custom_or_resa_cnt) 
					or $id_custom_or_resa_cnt==0) {
					
					$res= $resastemp->checkQty($operator, $cartQuantity, $cart, $availabilities, $stocks);
					return $res;
				}
			}
		}
		return false;
	}
	
	public static function getProducts($id_cart, $_products, $_pricerules, $id_product=null, $id_product_attribute=null, $setprice=true) {
		$resas = array();
		if (intval($id_cart)>0) {
			$sql = "SELECT *, " . _DB_PREFIX_ . "myownreservations_cartproducts.quantity AS qty FROM " . _DB_PREFIX_ . "myownreservations_cartproducts RIGHT JOIN " . _DB_PREFIX_ . "cart_product ON (" . _DB_PREFIX_ . "myownreservations_cartproducts.id_cart = " . _DB_PREFIX_ . "cart_product.id_cart AND " . _DB_PREFIX_ . "myownreservations_cartproducts.id_product = " . _DB_PREFIX_ . "cart_product.id_product AND " . _DB_PREFIX_ . "myownreservations_cartproducts.id_product_attribute = " . _DB_PREFIX_ . "cart_product.id_product_attribute) WHERE " . _DB_PREFIX_ . "myownreservations_cartproducts.id_cart = ".$id_cart;
			if ($id_product!=null) $sql .= " AND " . _DB_PREFIX_ . "myownreservations_cartproducts.id_product = ".$id_product;
			if ($id_product_attribute!=null) $sql .= " AND " . _DB_PREFIX_ . "myownreservations_cartproducts.id_product_attribute = ".$id_product_attribute;

			$cartSql = Db::getInstance()->ExecuteS($sql);

			if (is_array($cartSql) && count($cartSql)>0) 
			foreach($cartSql AS $cart) {
				$tempCart = new myOwnCart();
				$tempCart->sqlId = $cart['id_cartproduct'];
				$tempCart->id_cart = $cart['id_cart'];
				$tempCart->id_shop = $cart['id_shop'];
				$tempCart->id_product = $cart['id_product'];
				$tempCart->id_product_attribute = $cart['id_product_attribute'];
				$tempCart->id_customization = $cart['id_customization'];
				$tempCart->id_object = $cart['id_object'];
				$tempCart->quantity = $cart['qty'];
				$tempCart->startDate = $cart['start_date'];
				$tempCart->startTimeslot = $cart['start_timeslot'];
				$tempCart->endDate = $cart['end_date'];
				$tempCart->endTimeslot = $cart['end_timeslot'];
				
				$tempCart->_mainProduct = $_products->getResProductFromProduct($tempCart->id_product);
				if ($tempCart->_mainProduct!=null) {
					$resas[] = $tempCart;
				}
			}
			if ($setprice)
			foreach($resas AS $tempCart) {
				if ($tempCart->_mainProduct!=null) {
					// we can set price from admin
					$specific_price=false;
					if (isset($context->customer) && (Tools::getValue('controller')=='AdminCarts' || Tools::getValue('controller')=='adminmyownreservations')) {
						$context = Context::getContext();
						$id_group=0;
						$id_group = $context->customer->id_default_group;
				        $id_currency = (int)$context->currency->id;
				        $ids = Address::getCountryAndState((int)$context->cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')});
				        $id_country = $ids['id_country'] ? (int)$ids['id_country'] : (int)Configuration::get('PS_COUNTRY_DEFAULT');
	        
						$specific_price = SpecificPrice::getSpecificPrice($tempCart->id_product, $tempCart->id_shop, $id_currency, $id_country, $id_group, $tempCart->quantity, $tempCart->id_product_attribute, 0, $id_cart);
					} 
					$tempCart->setPrice($_pricerules->list, null, null, -1, -1, $resas);
					if ($specific_price)
						$tempCart->price = $specific_price['price'];
					//$tempCart->setPrice($_pricerules->list, null, null, ($specific_price ? $specific_price['price'] : -1), -1, $resas);
					//echo '!!'.$tempCart->getPriceTaxes(true);
				}
			}
		}
		return $resas;
	}
	
	public function getCartResasPrice() {
		$prices=array();
		foreach ($this->list as $resa)
			$prices[$resa->sqlId] = Tools::displayPrice($resa->getPriceTaxes(true), myOwnUtils::getCurrencyObj());
		return $prices;
	}
	
	public function add($obj, $_products, $_pricerules, $id_customization, $postResa) {
		$found=false;
		if (is_array($this->list))
			foreach($this->list as &$cartProduct)
				if ($cartProduct->id_product == $postResa->id_product
					&& $cartProduct->id_product_attribute == $postResa->id_product_attribute
					&& $cartProduct->id_object == $postResa->id_object
					&& $cartProduct->id_customization == $id_customization
					&& $cartProduct->startDate == $postResa->startDate
					&& $cartProduct->startTimeslot == $postResa->startTimeslot 
					&& $cartProduct->endDate == $postResa->endDate
					&& $cartProduct->endTimeslot == $postResa->endTimeslot) {
						$found = true;
						$cartProduct->quantity += $postResa->quantity;
						$cartProduct->sqlUpdateQty();
						foreach ($obj->_extensions->getExtByType(extension_type::CARTUPDATE) as $ext) {
							if (method_exists($ext, "execCartUpdate"))
								$ext->execCartUpdate($obj, $cartProduct);
						}
					}
					
		if (!$found) {
  			$newCart = new myOwnCart();
    		//$newCart->sqlId = 
    		$newCart->id_cart = $this->id_cart;
    		$newCart->id_product = $postResa->id_product;
    		$newCart->id_product_attribute = $postResa->id_product_attribute;
    		$newCart->id_customization = $id_customization;
    		$newCart->id_object = $postResa->id_object;
    		$newCart->quantity = $postResa->quantity;
    		$newCart->startDate = $postResa->startDate;
    		$newCart->startTimeslot = $postResa->startTimeslot;
    		$newCart->endDate = $postResa->endDate;
    		$newCart->endTimeslot = $postResa->endTimeslot;
    		$newCart->_mainProduct = $_products->getResProductFromProduct($newCart->id_product);
    		$newCart->setPrice($_pricerules->list);
    		$newCart->sqlId = $newCart->sqlInsert();
    		$this->list[]=$newCart;
    		foreach ($obj->_extensions->getExtByType(extension_type::CARTINSERT) as $ext) {
		// 				if (in_array($ext->id, $resa->_mainProduct->ids_notifs)) {
					if (method_exists($ext, "execCartInsert"))
						$ext->execCartInsert($obj, $newCart);
			}
    	}
	}
	
	public function deleteProducts($id_product, $id_product_attribute, $id_custom_or_resa_cnt) {
		$removedList=array();$qty=0;//$cnt=0;
		$totalresasqtyforprod=0;
		$deletedqty=0;
		if (is_array($this->list))
		foreach($this->list as $cartProduct) {
			if ($cartProduct->id_product == $id_product
				&& $cartProduct->id_product_attribute == $id_product_attribute) {
				//$cnt++;
				$totalresasqtyforprod+=$cartProduct->quantity;
				if (($id_custom_or_resa_cnt>0 && $cartProduct->id_customization == $id_custom_or_resa_cnt)
					or ($id_custom_or_resa_cnt<0 && $cartProduct->sqlId == -$id_custom_or_resa_cnt)
					or $id_custom_or_resa_cnt==0 ) {
					$deletedqty=$cartProduct->quantity;
					$cartProduct->sqlDelete();
				} else {
					//$qty+=$cartProduct->quantity;
					$removedList[] = $cartProduct;
				}
			} else $removedList[] = $cartProduct;
		}
		$this->list = $removedList;
		//return remaining qty
		return $totalresasqtyforprod-$deletedqty;
	}
	
	public static function deleteCart($id_cart) {
		if (intval($id_cart)>0) {
			$query  = 'DELETE FROM '. _DB_PREFIX_ .'myownreservations_cartproducts WHERE id_cart = '.$id_cart.';';
			return Db::getInstance()->Execute($query);
		}
	}
	
}

?>