<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class reservation_interval {
	const TIMESLICE = 5;	
	const TIMESLOT = 0;
	const DAY = 1;
	const WEEK = 2;	
	const MONTH = 3;
	const NIGHT = 4;
	const PERIOD = 6;
}

class product_occupation {
	const STOCK = 0;
	const FREE = 1;
	const FIXED = 2;
	const SHARED = 3;		
}

class widget_position {
	const TABS = 0;
	const RIGHT = 1;
	const POPUP = 2;
	const COL = 3;
	const TOP = 4;
	const NONE = 5;
}

class product_type {
	const PRODUCT = 0;
	const VIRTUAL = 1;
	const STOCK = 2;
	const MUTUEL = 3;		
	const SHARED = 4;
	const AVAILABLE = 5;
	const PLACE = 6;
	const RESOURCE = 11;
}

class occupy_combinations {
	const EACH = 0;
	const SUM = 1;
	const DEF = 2;	
	const IGNORE = 3;
}

class stock_assignation {
	const NONE = 0;
	const AUTO = 1;
	const USER = 2;	
}

class reservation_qty {
	const PRODUCT = 0;
	const FIXED = 1;
	const PARTIAL = 2;
	const SEARCH = 3;
	const ATTENDEES = 7;
}

class reservation_price_display {
	const UNIT = 0;
	const MIN = 1;
	const HIDE = 2;
}

class reservation_view {
	const DAY = 0;
	const MINIMAL = 6;
	const WEEK = 1;	
	const TWOWEEKS = 2;
	const MONTH = 3;
	const QUARTER = 4;
	const YEAR = 5;
}

class reservation_error {
	const NO_PRODUCT_AVAILABLE = -3;
	const ALL_PRODUCTS_BOOOKED = -2;
	const PERIOD_IN_HOLIDAYS = -1;	
	const INVALID_LENGTH = -4;
	const INVALID_PERIOD = -5;
	const DISTINCT_PERIOD_FORBIDDEN = -6;
	const NO_PERIOD_SELECTED = -7;
	const PRODUCT_ISNT_RENTED = -8;
	const NOT_ENOUGH_AVAILABLE = -9;
}

class reservation_start {
	const SAME_DAY = 0;
	const AFTER_DAYS = 1;	
	const NEXT_WEEK = 2;
	const SOME_WEEKS = 3;
	const AFTER_HOURS = 4;
	const AFTER_MINUT = 5;
}

class reservation_length {
	const LIMIT	= 0;
	const FIXED	= 1;
	const FREE = 2;
	const PRODUCT = 3;
	const COMBINATION = 4;
	const NONE = 5;
	const OPTIONAL = 6;
}

class reservation_mode {
	const START_END	= 0;
	const START_LENGTH	= 3;
	const UNIQUE = 2;
	const MULTIPLE = 4;
}
	
class reservation_price {
	const PRODUCT_TIME = 0;
	const PRODUCT_DATE = 1;
	const TABLE_TIME = 2;
	const TABLE_DATE = 3;
	const TABLE_RATE = 4;
}


class reservation_unit {
	const TIMESLOT = 0;
	const DAY = 1;
	const WEEK = 2;
	const MONTH = 3;
	const NIGHT = 4;
	const MINUTE = 5;
	const HOUR = 6;
}

class pricesrules_impact {
	const DECREASE = 0;
	const SET = 1;
	const INCREASE = 2;	
	const PERIOD = 3;	
}

class pricesrules_type {
	const LENGTH = 2;
	const DATES = 3;
	const RECURRING = 4;
	const DELAY = 5;
	const QTY = 6;
	const HISTORY = 8;
}

class availability_type {
	const UNAVAILABLE = 0;
	const AVAILABLE = 1;
}

class deposit_type {
	const ONCE = 0;
	const PRODUCT = 1;
	const RESA = 2;
	const QTY = 3;
}

class sync_platforms {
	const ABRITEL = 1;
	const AIRBNB = 2;
	const BOOKING = 3;
	const EXPEDIA = 4;
	const TRIPADVISOR = 5;
}

class MYOWN_OBJ {
	const TIMESLOT = 0;
	const AVAILABILITY = 1;
	const FAMILY = 2;
	const PRODUCT = 3;
	const PRICERULE = 4;
	const STOCK = 5;
	const RESVRULE = 6;
	const PRODFAMILY = 7;
	const RESV = 8;
	const STOCKITEM = 9;
	const PREFIX = 10;
	const ORDER = 11;
	const PRICESET = 12;
	const TIMESLICE = 13;
}

class MYOWN_OPE {
	const DELETE = 0;
	const CREATE = 1;
	const ADD = 2;
	const UPDATE = 3;
	const IMPORT = 4;
	const DISABLE = 5;
	const ENABLE = 6;
	const COPY = 7;
	const VALIDATE = 8;
	const UNVALIDATE = 9;
	const SAVE = 10;
	const EXPORT = 11;
	const PPRINT = 12;
	const CANCEL = 13;
	const EDIT = 14;
	const NNEW = 15;
	const HELP = 16;
	const ORDER = 17;
	const NORES = 18;
	const SAVESTAY = 19;
	const FNEW = 20;
}

?>