<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnCalendarLengthSlice extends myOwnCalendarTimeSlice {
	//public $startTime;
	//public $endTime;
	public $length;
	
	public function __construct($date, $timeSlot=null, $length, $view=null)
	{
		//for a day summary (when choosing a day before it timeslotss
		if ($timeSlot==null) $startTimeslot=0;
		else $startTimeslot = $timeSlot->sqlId;

		$this->length = $length;
		if ($view!=null) $this->label = $length.' '.myOwnLang::getUnit($view->potentialResa->_mainProduct->_reservationUnit, $length>1);
		else $this->label = $length;

		//$this->idTimeSlot = $timeSlot->sqlId;
		if ($timeSlot!=null) $this->start = strtotime(date('Y-m-d',$date).' '.$timeSlot->startTime);
		else  $this->start = strtotime(date('Y-m-d',$date).' 00:00');

		if ($view!=null) {
			$potentialResa = clone $view->potentialResa;
			$endDate = 0;
			$endTimeslotId = 0;

			$desiredShift = $this->length;
			$potentialResa->_mainProduct->getShiftTimeSlot($date, $startTimeslot, $desiredShift, false, null, $endDate, $endTimeslotId, $potentialResa->id_product, $potentialResa->id_product_attribute);
			$this->idTimeSlot = $endTimeslotId;
			$endTimeslot = $view->timeslotsObj->list[$endTimeslotId];
			//if ($desiredShift==1 or $desiredShift==2) echo date('Y-m-d',$date).' +'.$this->length.' ='.date('Y-m-d',$endDate);
			//$potentialResa->_fixedLength = $desiredShift;
			$this->end=strtotime(date('Y-m-d',$endDate).' '.$endTimeslot->endTime);
		} else
			$this->end=strtotime(date('Y-m-d',$date).' '.$timeSlot->endTime);
			
		if ($this->start == $this->end) {
			//dleted because instrad we have same en d of length =1 or length=2
			/*if ($view->potentialResa->_mainProduct!=null && $view->potentialResa->_mainProduct->reservationSelType==reservation_interval::DAY)
				$this->end = strtotime("+1 day",$this->end);*/
			if ($view->potentialResa->_mainProduct!=null && $view->potentialResa->_mainProduct->reservationSelType==reservation_interval::WEEK)
				$this->end = strtotime("+1 week",$this->start);
		}
		$this->key=date('Y-m-d',$this->start).'_'.$timeSlot->sqlId.'!'.$length;

	}
	public function getDate() {
		return strtotime(date('Y-m-d',$this->end).' 00:00:00');
	}
	
}