<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnCalendarView {
	public $start;
	public $end;
	public $length;
	public $potentialResa;
	public $timeSlots;
	public $timeslotsObj;
	public $days;
	public $weeks;
	public $months;
	public $availabilities;
	public $reservationEnabled;
	public $selTypeNum;
	public $multiSel;
	public $homeProducts=array();
	public $ext_object=null;
	
	public function __construct($obj, $potentialResa, $calendarType="", $ajax=true)
	{
	  	global $cookie;
	  	$debug=false;
	  	if ($debug) $rustart = getrusage();
	  	
		//get planning base bounds
		$this->potentialResa = $potentialResa;

		$mainProduct = $potentialResa->_mainProduct;
		$this->start = $mainProduct->getReservationStart();
		$this->length = $mainProduct->reservationPeriod; 
		$planningOriginalEnd = strtotime("+".$this->length." day",$this->start);

		$this->end = $planningOriginalEnd;
		$this->multiSel = ((int)$mainProduct->_reservationSelMultiple==1
							&& (($mainProduct->_reservationLengthControl == reservation_length::FIXED) || $mainProduct->_reservationLengthControl == reservation_length::NONE)
							);	
		$this->selTypeNum=1;
		//we need strict product if product = 0 because when selector we only care global holidays
		$strictProduct = (intval($potentialResa->id_product)==0);

		if (Tools::getIsSet('res_way')) {
			$res_way = Tools::getValue('res_way', 'both');
			//if ($res_way=='both') $this->selTypeNum=0;
			if ($res_way=='start') $this->selTypeNum=1;
			if ($res_way=='stop') $this->selTypeNum=2;
			if ($res_way=='start') $potentialResa->_selTypeNum=1;
			if ($res_way=='stop') $potentialResa->_selTypeNum=2;
			if ($res_way!='both') $potentialResa->_selType = "";
		}

		$weekDay=$mainProduct->_reservationSelWeekStart;
		$monthDay=$mainProduct->_reservationSelMonthStart;
		
		$this->timeslotsObj = clone $mainProduct->_timeslotsObj;

		if ($mainProduct->_timeslotsObj->isProductAssigned() && $potentialResa->id_product==0 && $calendarType!='home') {
			$this->timeSlots = array (0 => myOwnTimeSlots::getDefault(0, true));
			$this->timeslotsObj->list = $this->timeSlots;
			
		}

		//adjust planning after resa begin
		//-----------------------------------------------------------------------------------------------------------------
		if ($potentialResa->_selType == "end") {
			$this->selTypeNum=2;
			$weekDay=$mainProduct->_reservationSelWeekEnd;
			$monthDay=$mainProduct->_reservationSelMonthEnd;

			$reservationStartDate = strtotime($potentialResa->startDate);
			$reservationStartTimeslotId = $potentialResa->startTimeslot;

			if ($mainProduct->_reservationLengthControl != reservation_length::FREE && $mainProduct->_reservationLengthControl != reservation_length::NONE && $mainProduct->_reservationLengthControl != reservation_length::OPTIONAL) {
				// If one sel or fixed sel calculating end
				//-----------------------------------------------------------------------------------------------------------------
				 //get resa params

		     	$id_timeslot_end = 0; 
		     	$productLength=0;
		     	$desiredShift = $mainProduct->_reservationMinLength; 
		     	if ($mainProduct->_reservationLengthControl == reservation_length::PRODUCT)
		     		$productLength = intval(Configuration::get('MYOWNRES_RES_LENGTH_'.$potentialResa->id_product, 0));
		     	if ($mainProduct->_reservationLengthControl == reservation_length::COMBINATION)
		     		$productLength = intval(Configuration::get('MYOWNRES_RES_LENGTH_'.$potentialResa->id_product.'-'.$potentialResa->id_product_attribute, 0));
		     	if ($mainProduct->_reservationLengthQuantity)
		     		$productLength = $productLength * $potentialResa->quantity;
//echo '$desiredShift:'.(int)$desiredShift;
			 	if ($productLength) $desiredShift=$productLength;
		     	if ($desiredShift>0 && $mainProduct->_reservationUnit==reservation_unit::MONTH)
		     		$desiredShift--;
		     	
		     	//Need only to set minimal at 1 week when start day = and day
		     	if ($desiredShift>0 && $mainProduct->_reservationUnit==reservation_unit::WEEK )  	//&& $mainProduct->_reservationSelWeekStart != $mainProduct->_reservationSelWeekEnd
		     		$desiredShift--;

		     	/*if ($desiredShift>0 && $mainProduct->_reservationUnit==reservation_unit::DAY && array_key_exists($reservationStartTimeslotId, $mainProduct->_timeslotsObj->list)) {
		     		$reservationStartTimeslot = $mainProduct->_timeslotsObj->list[$reservationStartTimeslotId];
		     		if ($reservationStartTimeslot->startTime <= $reservationStartTimeslot->endTime)  	//&& $mainProduct->_reservationSelWeekStart != $mainProduct->_reservationSelWeekEnd
		     			$desiredShift--;
		     	}*/

		     	$this->length = $mainProduct->_reservationMaxLength; 
		     	if ($productLength) $this->length=$productLength;
			 	
			 	//getting availabiltiies if have to count days available
			 	if (!$mainProduct->_lengthHolidays) {
			 		$availabilities = new myOwnAvailabilities($this->start, $planningOriginalEnd, $mainProduct, intval($potentialResa->id_product), intval($potentialResa->id_product_attribute), false, $strictProduct);
			 		myOwnAvailabilities::setInstance($availabilities);
			 	} else $availabilities = null;

		     	//get planning start date (calendar begins at reservation date +min length
		     	$test  = $mainProduct->getShiftTimeSlot($reservationStartDate, $reservationStartTimeslotId, $desiredShift, false, $availabilities, $planningStartDate, $planningStartTimeslot, $potentialResa->id_product, $potentialResa->id_product_attribute);
		     
		     	$this->start = strtotime(date('Y-m-d',$planningStartDate).' '.$this->timeslotsObj->list[$planningStartTimeslot]->startTime);

 
		     	//get planning end date && checking resa rules for length (calendar end at calendar begin date +max length )
		     	$availabilities = new myOwnAvailabilities($this->start, strtotime('+'.$this->length.'day',$this->start), $mainProduct, intval($potentialResa->id_product), intval($potentialResa->id_product_attribute), false, $strictProduct);	
			 	
			 	if ($mainProduct->_reservationLengthCanExceed) 
			 		$planningOriginalEnd = strtotime('+'.$this->length.'days', $planningOriginalEnd);

		     	$mainProduct->getShiftTimeSlot($reservationStartDate, $reservationStartTimeslotId, $this->length, true, $availabilities, $planningEndDate, $planningEndTimeslot, $potentialResa->id_product, $potentialResa->id_product_attribute, $planningOriginalEnd);

		     	$this->end = strtotime(date('Y-m-d',$planningEndDate).' '.$this->timeslotsObj->list[$planningEndTimeslot]->endTime);

		     	if ($this->timeslotsObj->list[$planningEndTimeslot]->endTime=='00:00:00') $this->end = strtotime('+1day',$this->end);
		     	
	     	} else {
				$this->end = $planningOriginalEnd;
				$this->start = strtotime(date('Y-m-d',$reservationStartDate).' '.$this->timeslotsObj->list[$reservationStartTimeslotId]->startTime);
				if ($mainProduct->_reservationSelWeekStart > $mainProduct->_reservationSelWeekEnd) $this->start = strtotime('-1day',$this->start);
	     	}
		}
		
		$this->potentialResa->_reservationEnd = $this->end;
		
if ($debug) $ruend = getrusage();
if ($debug) echo "#" . myOwnCalendarView::rutime($ruend, $rustart, "utime");

		if ($calendarType=='home') {
			$this->availabilities = new myOwnAvailabilities($this->start, $this->end, $mainProduct, 0, 0, false, false);
			
		} else if ($calendarType=='topcolumn' || $calendarType=='column') {
			$this->availabilities = new myOwnAvailabilities($this->start, $this->end, $mainProduct, 0, 0, false, false, true);
			
		} else
			$this->availabilities = new myOwnAvailabilities($this->start, $this->end, $mainProduct, (int) $potentialResa->id_product, (int) $potentialResa->id_product_attribute, false, $strictProduct);

if ($debug) $ruend = getrusage();
if ($debug) echo "#" . myOwnCalendarView::rutime($ruend, $rustart, "utime");

		//calculating calendar objects filtered depending on availabilties
		//-----------------------------------------------------------------------------------------------------------------
		if ($calendarType=='home')
			$this->timeSlots = $this->timeslotsObj->getEnablesTimeSlotsBetween($this->start, $this->end, $this->selTypeNum, $this->availabilities, -1);
		else $this->timeSlots = $this->timeslotsObj->getEnablesTimeSlotsBetween($this->start, $this->end, $this->selTypeNum, $this->availabilities, (int) $potentialResa->id_product);
		
		$this->timeslotsObj->list = $this->timeSlots;

		$this->days = myOwnCalendarDay::getFilteredOnPeriod($this->start, $this->end, $this->timeslotsObj, $this->selTypeNum, $this->availabilities);

		$this->weeks = MyOwnCalendarTool::getFilteredWeeksOnPeriod($this->start, $this->end, $this->timeslotsObj, $this->selTypeNum, $this->availabilities, ($calendarType=='home'));

		if ($mainProduct->reservationSelType==reservation_unit::TIMESLOT
			or $mainProduct->reservationSelType==reservation_unit::DAY
			or $mainProduct->reservationSelType==reservation_unit::NIGHT
			or ($mainProduct->_reservationSelExt!=null && $mainProduct->_reservationSelExt->parentUnit==reservation_unit::DAY)) {

			$this->months = MyOwnCalendarTool::getFilteredMonthsForTimeslots($this->start, $this->end, $this->timeslotsObj, $this->selTypeNum, $this->availabilities, ($mainProduct->reservationSelType==reservation_unit::TIMESLOT));
		}

		if ($mainProduct->reservationSelType==reservation_unit::WEEK) 
			$this->months = MyOwnCalendarTool::getFilteredMonthsForWeeks($this->start, $this->end, $this->timeslotsObj, $this->selTypeNum, $weekDay, $this->availabilities);

		if ($mainProduct->reservationSelType==reservation_unit::MONTH)	
			$this->months = MyOwnCalendarTool::getFilteredMonthsForMonths($this->start, $this->end, $this->timeslotsObj, $this->selTypeNum, $monthDay, $this->availabilities);
		
		if (count($this->months)==0 && $mainProduct->reservationSelType==reservation_unit::WEEK || $mainProduct->reservationSelType==reservation_unit::MONTH)
			 $this->reservationEnabled = false;
			 
		//check if there is timeslots after filter
		//-----------------------------------------------------------------------------------------------------------------
 		$this->reservationEnabled = true;
       	if ($mainProduct->reservationPeriod==0 && $mainProduct->reservationStartType!=0)
             $this->reservationEnabled = false;

 		if ($calendarType!='home' && !$this->timeslotsObj->areTimeSlotsEnablesBetween($this->start, $this->end, $this->availabilities))
      		$this->reservationEnabled = false;

      	if ($this->potentialResa->id_product>0 || $calendarType=='home') 
			$this->potentialResa->_reservations->list = myOwnResas::getReservationsOnPeriod($obj, $mainProduct, $this->potentialResa->id_product, $this->potentialResa->id_product_attribute, $this->start, $this->end);
		
		if ($debug) $ruend = getrusage();
		if ($debug) echo "#" . myOwnCalendarView::rutime($ruend, $rustart, "utime");
	}
	
	public function getMinStart() {
		if (count($this->months)) return $this->months[0]->getStart();
		else return $this->start;
	}
	
	public function getAvailableProducts($id_lang, $day, $timeslot)  {
		$productsavailable=array();

		foreach ($this->homeProducts as $homeProductKey => $homeProduct) {

			if ($timeslot->id_family==0 || $timeslot->id_family==$homeProductKey || $homeProductKey==0) {
				foreach ($homeProduct as $product) {
					$productsavailable[$product['id_product']]=$product;
					if ($this->availabilities->isAvailable($day, $timeslot, $product['id_product'])) {
						$productsavailable[$product['id_product']]['available']=true;
					} else {
						$productsavailable[$product['id_product']]['available']=false;
					}
					$productsavailable[$product['id_product']]['id_product_attribute']=MyOwnReservationsUtils::getDefaultAttribute($product['id_product']);
				}
			}
		}
		return $productsavailable;
	}
	
	public function getAllProducts($id_lang)  {
		$productsavailable=array();
		foreach ($this->homeProducts as $homeProductKey => $homeProduct) {
			foreach ($homeProduct as $product) {
				$productsavailable[$product['id_product']]=$product;
				$productsavailable[$product['id_product']]['available']=true;
			}
		}
		return $productsavailable;
	}
	
	//-----------------------------------------------------------------------------------------------------------------
	//  CHECKING TIME SLICES
	//-----------------------------------------------------------------------------------------------------------------
	
	public function checkTimeSlicesAvailabilities($obj, $timeSlots, &$timeSlices, $calendarType) {
		//checking availability type
		if(session_id() == '') session_start();
		$objectSelection = Tools::getValue('place', (int)$_SESSION['objectSelection']);

		$isAvailable=true;
		foreach ($this->availabilities->list as $availability) 
			if ($availability->type==availability_type::AVAILABLE)
				$isAvailable=false;
		$resources=array();
		if ($this->potentialResa->_mainProduct->productType == product_type::RESOURCE && $this->potentialResa->_mainProduct->_occupyObj!=null) {
			$isAvailable=false;
			$resourceSelected =  Tools::getValue('resource', 0);
			if ($resourceSelected > 0)
				$resources = array($resourceSelected);
			else $resources = $this->potentialResa->_mainProduct->_occupyObj->getFamilyOptions($this->potentialResa->_mainProduct);
			$occupyValue = $this->potentialResa->_mainProduct->_occupyObj->getConfig($this->potentialResa->_mainProduct->sqlId, resources::CONFIG_OCCUPATION, 0);
		}

		if (!$isAvailable)
			foreach ($timeSlices as &$timeSlice)
				$timeSlice->isAvailable=false;

		foreach ($this->availabilities->list as $availability) {
			
			if ($objectSelection==0 || $availability->id_object == 0 || $objectSelection==$availability->id_object)
				//possible optimisation goto first time slice on start day
				foreach ($timeSlices as &$timeSlice) 
					if ($this->potentialResa->id_product==0 or $availability->id_product==0 or $this->potentialResa->id_product==$availability->id_product or ($resources!=array() && $availability->id_product<0 && in_array(-$availability->id_product, $resources) )) {
						//if availability type available first consider all false
						//if ($availability->type==availability_type::AVAILABLE)
							//$timeSlice->isAvailable=false;
						//if not an index slice
						if ($timeSlice->idTimeSlot!=-1) {
							
							//in case of an timeslot the availability cannot be inside the timeslice period
							//echo '<br>'.$this->potentialResa->id_product.' '.$availability->id_product.'<br>';
							//echo date('Y-m-d H:i:s',$availability->getStart($timeSlots)).' < '.date('Y-m-d H:i:s', $timeSlice->end);
							//echo '<br>';
							//echo date('Y-m-d H:i:s', $availability->getEnd($timeSlots)).' > '.date('Y-m-d H:i:s', $timeSlice->start);
							//echo '<br>'.date('Y-m-d H:i:s', $timeSlice->start).'-'.date('Y-m-d H:i:s', $timeSlice->start);
							//the availability can end at begin on another day 
							if (($availability->type!=availability_type::AVAILABLE && $availability->getStart($timeSlots) < $timeSlice->end && 
								$availability->getEnd($timeSlots) > $timeSlice->start) OR 
								($availability->type==availability_type::AVAILABLE && $availability->getStart($timeSlots) <= $timeSlice->start && 
								$availability->getEnd($timeSlots) >= $timeSlice->end))
								if (!(MYOWNRES_WEEK_TS && $this->potentialResa->_mainProduct->_reservationUnit==reservation_unit::WEEK) || $availability->startTimeslot==$timeSlice->idTimeSlot) {

								if ($this->potentialResa->id_product>0 || $calendarType=='home' || $calendarType=='topcolumn' || $calendarType=='column') 
									$timeSlice->isAvailable=($availability->type==availability_type::AVAILABLE);
								
								if ($availability->id_product < 0 && $this->potentialResa->_mainProduct->productType == product_type::RESOURCE) {
							
									$availableResource = false;
									if (!in_array(-$availability->id_product, $timeSlice->availableResources)) {
										if (count($obj->_timeSlots->getTimeSlotsOfFamily($availability->id_product))) {
											$resourcets = $obj->_timeSlots->getTimeSlotOfTimeAndFamily($timeSlice->start, $availability->id_product);
											$availableResource = ($resourcets != null);
										}
										else $availableResource = true;
										if ($availableResource) 
											$timeSlice->availableResources[]=-$availability->id_product;
									}

									if ($availableResource) {
										if ($timeSlice->availableQuantity==-1)
											$timeSlice->availableQuantity++;
										if ($occupyValue==0)
											$timeSlice->availableQuantity += $this->potentialResa->_productQty;
										else $timeSlice->availableQuantity += 1;
									} else $timeSlice->availableQuantity = false;
								} elseif ($availability->quantity > 0)
									$timeSlice->availableQuantity = $availability->quantity;
								if ($this->potentialResa->id_product==0 && $availability->id_product>0) {
									$timeSlice->availableQuantities[$availability->id_product] = $availability->quantity;
									//on home need to set each product qty
									if ($availability->quantity==0 && $availability->type==availability_type::AVAILABLE && $timeSlice->defaultQuantities!=array())
										$timeSlice->availableQuantities[$availability->id_product] = $timeSlice->defaultQuantities[$availability->id_product];
								}

								if ($availability->id_object > 0 && $this->ext_object!=null)
									if (array_key_exists($availability->id_object, $this->ext_object->objects->list)) 
										$timeSlice->object = $this->ext_object->objects->list[$availability->id_object];
								//echo '<br/>#'.date('Y-m-d H:i:s',$timeSlice->start).'-'.date('Y-m-d H:i:s',$timeSlice->start).':'.intval($timeSlice->isAvailable).'   '.date('Y-m-d H:i:s',$availability->getEnd($timeSlots));
							}
						} else {
							//in case of an index the availability must cover the whole period
							
							//echo '<br>'.date('Y-m-d H:i:s', $timeSlice->start).' <= '.date('Y-m-d H:i:s', $timeSlice->end);
							//echo date('Y-m-d H:i:s',$availability->getStart($timeSlots)).' >= '.date('Y-m-d H:i:s', $availability->getEnd($timeSlots));
							if (
							($availability->getStart($timeSlots) <= $timeSlice->start && $availability->getEnd($timeSlots) >= $timeSlice->end)
							 or ($availability->getStart($timeSlots) >= $timeSlice->start && $availability->getEnd($timeSlots) <= $timeSlice->end && $availability->type==availability_type::AVAILABLE) ) {
								$timeSlice->isAvailable = ($availability->type==availability_type::AVAILABLE);
								
								if ($availability->quantity > 0)
									$timeSlice->availableQuantity = $availability->quantity;
								if ($this->potentialResa->id_product==0 && $availability->id_product > 0) {
									$timeSlice->availableQuantities[$availability->id_product] = $availability->quantity;
									if ($availability->quantity==0 && $availability->type==availability_type::AVAILABLE && $timeSlice->defaultQuantities!=array())
										$timeSlice->availableQuantities[$availability->id_product] = $timeSlice->defaultQuantities[$availability->id_product];
								}
							}
						}
							//possible optimisation
							//if ($availability->getEnd($timeSlots)<strtotime($timeSlice->date.' '.$timeSlice->startTime)) break; 
					}	
		}
	}
	
	public function checkTimeSlicesResaRules($obj, $potentialResa, &$timeSlices, $id_lang, $type) {	
		global $haveToFreqNext;
		foreach ($timeSlices as &$timeSlice) {

			//if not an index slice
			if ($timeSlice->idTimeSlot!=-1) {
				//echo '&'.date('Y-m-d H:i', $timeSlice->getDate());
				$cart = $potentialResa->getCart($timeSlice->getDate(), $timeSlice->idTimeSlot, $this->availabilities);
				
				if ($cart!=null) {
					//check object  $this->potentialResa->_mainProduct->productType == product_type::PLACE && 
					if ($this->ext_object!=null && in_array($this->ext_object->id, $potentialResa->_mainProduct->ids_options) && Tools::getIsset('place') && array_key_exists(Tools::getValue('place'), $this->ext_object->objects->list))
						$timeSlice->object = $this->ext_object->objects->list[Tools::getValue('place')];
					//check frequency
					$freq = $this->potentialResa->_mainProduct->_reservationLengthFreq;
					//echo '@'.$cart->startDate.'_'.$cart->endDate.'_'.$timeSlice->idTimeSlot;
					if ($freq>1 && $potentialResa->_selType == "end") {
						$length = $cart->getLength(false, $this->availabilities);
// 						echo $cart->endDate.'#'.$length;
						if ($this->potentialResa->_mainProduct->_reservationLengthStrict && $this->potentialResa->_mainProduct->_reservationUnit==reservation_unit::WEEK) {
							$resStartDate = strtotime($cart->startDate);
							$resEndDate = strtotime($cart->endDate);
							$strictcheck = (date('N', $resStartDate)==date('N', $resEndDate));
						} else $strictcheck=true;
					}
					//added here to appy occupation on days that are < min length
					if ($potentialResa->_selType != "end" && 
						(	$this->potentialResa->_mainProduct->_reservationMinLength>1
							|| $this->potentialResa->_mainProduct->_reservationLengthControl == reservation_length::PRODUCT
							|| $this->potentialResa->_mainProduct->_reservationLengthControl == reservation_length::COMBINATION)
						) {
						$timeSlice->end = $cart->getEndDateTime();
						//echo '@'.date('Y-m-d H:i:s', $timeSlice->end);
						$tempts=clone $this->potentialResa->_mainProduct->_timeslotsObj;
						$tempts->list = $this->potentialResa->_mainProduct->getTimeslots();
						$timeSlice->isEnable = ($tempts->areEnableOnDay($timeSlice->end, 2)) ;
					}
						
					if ($potentialResa->_selType != "end" or $freq<2 or ($length%$freq==0 && $strictcheck) or $haveToFreqNext) {
						if ($this->potentialResa->_mainProduct->_reservationUnit==reservation_unit::WEEK && array_key_exists(1, $this->timeslotsObj->list))
							$timeSlot = $this->timeslotsObj->list[1];
						else if ($this->potentialResa->_mainProduct->_reservationUnit==reservation_unit::WEEK)
							$timeSlot = $this->timeslotsObj->list[0];
						else
							$timeSlot = $this->timeslotsObj->list[$timeSlice->idTimeSlot];
						
						if ($potentialResa->_selType == "end" || $potentialResa->_selType == "") $timeSlice->isEnable=$timeSlot->isEnableOnDay(strtotime($cart->endDate));
						else if ($potentialResa->_selType == "start") $timeSlice->isEnable=$timeSlot->isEnableOnDay(strtotime($cart->startDate));
						else $timeSlice->isEnable=true;
						if (!$timeSlice->isEnable && $freq>1 && MYOWNRES_FREQ_NEXT)
							$haveToFreqNext=true;
						else $haveToFreqNext=false;
// 						echo '<br/>--'.$cart->getLength(true).':'.$cart->getPrice();
						$timeSlice->price=$cart->getPrice();
						$timeSlice->_highPrice=$cart->getHighPrice();
						//Key for on click when length is fixed
						if ($potentialResa->_selType == '' && Tools::getValue('res_way')!='stop')
							$timeSlice->key=$cart->getFullKey();
						$timeSlice->css=$cart->getCss($obj->_pricerules->list);
						$timeSlice->isReduc=$cart->_isReducVisible;
						//If available qty not set on timeslice (form availability) getting from pot resa
						if ($timeSlice->availableQuantity == -1 && $potentialResa->_mainProduct->productType != product_type::RESOURCE)
							$timeSlice->availableQuantity = $potentialResa->_productQty;

						//need to set end time to slice when estimating cart moved into timeslice construct
						//$timeSlice->end = $cart->getEndDateTime(false);//getEndDateTimeRaw();
						
						$timeSlice->label = $cart->showPeriod($obj, $timeSlice->getDate(), $timeSlot, $id_lang, ($type=="column" || $type=="topcolumn"));
						//echo $timeSlice->label.' '.$cart->startDate.' '.$cart->endDate.' '.$cart->getLength().'<br>';
					} else $timeSlice->isEnable=false;
				} else $timeSlice->isEnable=false;

			} else {
				$timeSlice->isEnable=true;
				if ($this->potentialResa->_mainProduct->reservationSelPlanning==reservation_view::MONTH)
					$timeSlice->label=MyOwnCalendarTool::getDayOfMonth($timeSlice->getDate());
				else $timeSlice->label=MyOwnCalendarTool::formatDateShort($timeSlice->getDate(), $id_lang);
			}
		}
		
	}
	
	public function checkTimeSlicesOccupation($potentialResa, &$timeSlices, $cart=null) {
		$startTimeslice=reset($timeSlices);
		$endTimeslice=end($timeSlices);
		/*if ($potentialResa->_selType == "end") $startresaswindow = $this->start;
		else $startresaswindow = $startTimeslice->getDate();*/
		//need to get from begining instead we don't get reservations with betweenshift
		$startresaswindow = $this->start;
					
		//get with selection start
		if ($potentialResa->_selType == "end")
			$lines = $this->potentialResa->_reservations->getResaLines($cart, null, $this->potentialResa->_mainProduct, $this->potentialResa->id_product, $this->potentialResa->id_product_attribute,  strtotime($potentialResa->startDate), $endTimeslice->getEndDate(), $startTimeslice->idTimeSlot, $endTimeslice->idTimeSlot);
		//get with planning start
		else 
			$lines = $this->potentialResa->_reservations->getResaLines($cart, null, $this->potentialResa->_mainProduct, $this->potentialResa->id_product, $this->potentialResa->id_product_attribute, $startresaswindow, $endTimeslice->getEndDate(), $startTimeslice->idTimeSlot, $endTimeslice->idTimeSlot);

		foreach ($lines as $line){
			//if we're on en d selection we need to check if there is hole during period
			if ($potentialResa->_selType == "end") {
				//get earlier resa of line only to decrease from earlier resa to end of line
				$earlier_resa=null;
				foreach ($line as $resa)
					if ($earlier_resa==null or $resa->_start<$earlier_resa->_start)
						$earlier_resa=$resa;
				if ($earlier_resa!=null)
					$resas = array($earlier_resa);
				else $resas = array();
			} else
				$resas = $line;
			foreach ($resas as $resa){
				//echo 'start '.date("Y-m-d H:i:s", $resa->_start);
				//echo 'end  '.date("Y-m-d H:i:s", $resa->_end);
				foreach ($timeSlices as &$timeSlice) {
					//do not check occupation of a index because we donnot want to check if the whole day is available
					//syslog(LOG_WARNING, 'startts '.date("Y-m-d H:i:s", $timeSlice->start));
				//syslog(LOG_WARNING, 'endts  '.date("Y-m-d H:i:s", $timeSlice->end));
					if ($timeSlice->idTimeSlot>-1) {
						//syslog(LOG_WARNING, date("Y-m-d H", $resa->_start)."<".date("Y-m-d H", $timeSlice->end));
						//echo '<br>'.$resa->_isCart;
						$tmsstart=$timeSlice->start;
						$tmsend=$timeSlice->end;
						if ($timeSlice->end<$timeSlice->start && $potentialResa->_selType == "start") {
							$tmsstart=$timeSlice->start;
							$tmsend=strtotime('+1day', $timeSlice->end);
						}
						
						//echo '<br>'.date(" Y-m-d H:i:s", $resa->_start)."<".date("Y-m-d H:i:s", $tmsend);
						//echo '<br>'.date(" Y-m-d H:i:s", $resa->_end).">".date("Y-m-d H:i:s", $tmsstart);
						//echo '_'.$resa->startTimeslot.'_'.$resa->endTimeslot;
						//syslog(LOG_WARNING, date("Y-m-d H", $resa->_end).">".date("Y-m-d H", $timeSlice->start));
						//echo date("Y-m-d H", $timeSlice->start).'@'.date("Y-m-d H", $timeSlice->end);
						if (!(MYOWNRES_WEEK_TS && $resa->_mainProduct->_reservationUnit==reservation_unit::WEEK) || $resa->startTimeslot==$timeSlice->idTimeSlot)
						if ($resa->_start < $tmsend && 
							($resa->_end > $tmsstart or $potentialResa->_selType == "end")
						) {
							//set quantity -- because each line is an unique qty 
							// minimum 0 because if negative it's no limit on qty
							if ($timeSlice->availableQuantity>0) {
								if ($timeSlice->availableResources==array() || !$resa->id_assigned || in_array((int)$resa->id_assigned, $timeSlice->availableResources)) {
									$timeSlice->availableQuantity--;
								}
							}
							if ($this->potentialResa->_mainProduct->_occupyMaxOrder && !$resa->_isCart)
								$timeSlice->availableQuantity=0;
							if (array_key_exists($resa->id_product, $timeSlice->availableQuantities) && $timeSlice->availableQuantities[$resa->id_product]>0) {
								$timeSlice->availableQuantities[$resa->id_product]--;
									//In case of only one order whatever qty we block product but only for true reservations not cart items
								if ($this->potentialResa->_mainProduct->_occupyMaxOrder && !$resa->_isCart)
									$timeSlice->availableQuantities[$resa->id_product]=0;
							}
						}
						//echo $timeSlice->availableQuantity.'*';
					} else {
						if ($resa->_start < $timeSlice->end && 
							($resa->_end > $timeSlice->start or $potentialResa->_selType == "end")) {
							//set quantity -- because each line is an unique qty 
							// minimum 0 because if negative it's no limit on qty
							
							if ($timeSlice->availableQuantity>0) $timeSlice->availableQuantity--;
							if ($this->potentialResa->_mainProduct->_occupyMaxOrder && !$resa->_isCart)
								$timeSlice->availableQuantity=0;
							if (array_key_exists($resa->id_product, $timeSlice->availableQuantities) && $timeSlice->availableQuantities[$resa->id_product]>0) {
								$timeSlice->availableQuantities[$resa->id_product]--;
								//In case of only one order whatever qty we block product but only for true reservations not cart items
								if ($this->potentialResa->_mainProduct->_occupyMaxOrder && !$resa->_isCart)
									$timeSlice->availableQuantities[$resa->id_product]=0;
							}
							/*else {
								if (array_key_exists($resa->id_product, $timeSlice->unavailableQuantity))
									$timeSlice->unavailableQuantity[$resa->id_product]++; //for home saving unavailable  because don't know available
								else $timeSlice->unavailableQuantity[$resa->id_product]=1;
							}*/
						}
					}
				}
			}
		}
		//checking index quantity (for the wholeperiod)
		foreach ($timeSlices as $key => &$timeSlice) { // && array_key_exists(substr($key, 0, 10), $timeSlices)
			if (array_key_exists(substr($key, 0, 10), $timeSlices) && $timeSlice->idTimeSlot > -1 && $timeSlice->availableQuantity >-1 && $timeSlice->availableQuantity > $timeSlices[substr($key, 0, 10)]->availableQuantity) 
				$timeSlices[substr($key, 0, 10)]->availableQuantity = $timeSlice->availableQuantity;
		}
	}
	
	//-----------------------------------------------------------------------------------------------------------------
	//  VIEW TEMPLATE
	//-----------------------------------------------------------------------------------------------------------------
	public function getTemplate($obj, $selection, $type="", $ajax) {
		if(session_id() == '') session_start();
		global $smarty;
		
		global $cookie;
		global $cart;
		global $haveToFreqNext;
		$debug=false;
		$objectSelection = (int)$_SESSION['objectSelection'];
       
       $legend="";
       $template="";
       $output="";$outputExtra="";
       $mainProduct = $this->potentialResa->_mainProduct;

       $isWeekEndDays = $this->timeslotsObj->isWeekEndDays();
       $lengthWidget = ((Configuration::get('MYOWNRES_WIDGET_TYPE')==3 && $this->potentialResa->_selType == "end" && ($type=='topcolumn' || $type=='column'))
       					|| ($mainProduct->_reservationSelMode==reservation_mode::START_LENGTH && $this->potentialResa->_selType == "end" )
       					 );
       $isDaySelType = ($mainProduct->reservationSelType==reservation_interval::DAY
       					or $mainProduct->reservationSelType==reservation_interval::NIGHT
       					or ($mainProduct->_reservationSelExt!=null && $mainProduct->_reservationSelExt->parentUnit==reservation_interval::DAY) 
       					or ($mainProduct->_timeslotsObj->isProductAssigned() && $this->potentialResa->id_product==0 && $type!='home') );

       $isDaySel = ($isDaySelType && !$mainProduct->_reservationShowTime);
       $isNightSel = ($isDaySelType && ($mainProduct->reservationSelType==reservation_interval::NIGHT OR (array_key_exists(0, $this->timeSlots) && $this->timeSlots[0]->endTime<$this->timeSlots[0]->startTime)));

          		//smarty assign
		$myOwnCalendar = new MyOwnCalendarTool();

       $smarty->assign('timeSlotList',$this->timeSlots);
       
	   $smarty->assign('potentialResa',$this->potentialResa);
	   		$smarty->assign(array(
			'calendar' => $myOwnCalendar,
			'hidePrice'=>(Configuration::get('MYOWNRES_PRICE_TYPE')==reservation_price_display::HIDE),
			'previewPriceLabel'=>$obj->l('from(price)', 'calendarView'),
			'previewQuantityLabel'=>$obj->l('available', 'calendarView'),
			'previewQuantityLabels'=>$obj->l('availables', 'calendarView'),
			'previous_sub_label'=>'',
			'myOwnResCurrency'=>myOwnUtils::getCurrency(),
			'reservationLabel'=>Configuration::get('MYOWNRES_MSG_RESV', $cookie->id_lang),
			'resSelType'=>$mainProduct->reservationSelType,
			'reservationView'=>$this,
			'isWeekEndDays'=>$isWeekEndDays,
			'isNightSel'	=>	$isNightSel,
			'linetype'		=> 'sel',
			'subSelector'=>array(),
			'selector'=>$this->getSelector($obj)
			));
			
		$weekList=array();
		$timeSlices = array();
		$maxSelection=0;
		$reservationSelPlanning=$mainProduct->reservationSelPlanning;

		//force minimal planning depending on sel unit
		if ($type=='home') {
			//if ($mainProduct->reservationSelType==reservation_interval::DAY)
			//	$reservationSelPlanning=reservation_view::WEEK;
			if ($mainProduct->reservationSelType==reservation_interval::WEEK)
				$reservationSelPlanning=reservation_view::MONTH;
			if ($mainProduct->reservationSelType==reservation_interval::MONTH)
				$reservationSelPlanning=reservation_view::QUARTER;
		}
		if (!$lengthWidget && $this->reservationEnabled)
   		switch ($reservationSelPlanning) {
   			case reservation_view::DAY: //a day
   			//----------------------------------------------------------------------------------------------------------------
   				//selection
      		    $maxSelection=count($this->days)-1;
				if ($maxSelection==-1) $maxSelection=0;
				if ($selection>$maxSelection) $selection=$maxSelection;
				
	   			//time units
		       	if (array_key_exists($selection, $this->days)) {
		       		$currentDay = $this->days[$selection];
		       		$timeSlices = $currentDay->getTimeSlices($this, $this->selTypeNum);
		       		$smarty->assign('daysList', array($currentDay));
			   	} else $smarty->assign('daysList', array());
			   	
		       	$smarty->assign('timeslotsGroups',MyOwnCalendarTool::getTimeslotsGroups($this->timeSlots));

       			$template="view_day.tpl";
  				break;
  				
  			case reservation_view::MINIMAL: //minimal
  			//-----------------------------------------------------------------------------------------------------------------
  				//selection
      		    $maxSelection=0;
      		    $selection=0;
      		   
      		    $dayCount=0;$availableDays=array();
				foreach($this->days as $dayObj) {
					foreach($this->timeSlots as $timeSlot) {
						if ( $timeSlot->getEndHourTime($dayObj->date)<=$this->end
							&& $timeSlot->getStartHourTime($dayObj->date)>=$this->start)
							{
							$slice = new myOwnCalendarTimeSlice($dayObj->date, $timeSlot, $this->selTypeNum, $this);
				 			$timeSlices[date('Y-m-d', $dayObj->date).' '.$timeSlot->startTime]=$slice;
				  			$dayObj->_slices[$timeSlot->sqlId] = $slice;
				  			$availableDays[date('Y-m-d',$dayObj->date)]=$dayObj;
			  			}
		  			}
				};
       			$smarty->assign('availableDays',$availableDays);
       			$template="view_list.tpl";
  				break;
  				
   			case reservation_view::WEEK: 
   			//-----------------------------------------------------------------------------------------------------------------
   				//selection
				$maxSelection=count($this->weeks)-1;
				if ($maxSelection==-1) $maxSelection=0;
				if ($selection>$maxSelection) $selection=$maxSelection;					
				
				//time units
				$currentWeek = $this->weeks[$selection];
				$timeSlices = $currentWeek->getTimeSlices($this, $this->selTypeNum);
	
				$daysList = $this->weeks[$selection]->_days;
				$weekList[$selection] = $this->weeks[$selection];

				$smarty->assign('weeksList',$weekList);
				$template="view_week.tpl";
				$legend=myOwnLang::getUnit(reservation_unit::WEEK).' '.$this->weeks[$selection]->week;
				break;
				
   			case reservation_view::TWOWEEKS: 
   			//-----------------------------------------------------------------------------------------------------------------
   				//selection
        		$maxSelection=ceil(count($this->weeks)/2)-1;
        		if ($maxSelection==-1) $maxSelection=0;
        		if ($selection>$maxSelection) $selection=$maxSelection;
        		
        		//time units
				$timeSlices = $this->weeks[($selection*2)]->getTimeSlices($this, $this->selTypeNum);
				if (array_key_exists((int)($selection*2)+1, $this->weeks)) $timeSlices += $this->weeks[($selection*2)+1]->getTimeSlices($this, $this->selTypeNum);
				$daysList = $this->weeks[($selection*2)]->_filteredDays;
				if (array_key_exists((int)($selection*2)+1, $this->weeks)) $daysList += $this->weeks[($selection*2)+1]->_filteredDays;

        		for($i=0; $i<count($this->weeks); $i++)
        			if (floor($i/2)==$selection) {
        				$weekList[$i] = $this->weeks[$i];
        			}
        			
        		$smarty->assign('weeksList',$weekList);
        		$template="view_week.tpl";
   				break;
   				
   			case reservation_view::MONTH:
   			//-----------------------------------------------------------------------------------------------------------------
   				//selection
   				$maxSelection=count($this->months)-1;
   				if ($maxSelection==-1) $maxSelection=0;
    			if ($selection>$maxSelection) $selection=$maxSelection;
    			if (array_key_exists($selection, $this->months)) {
	    			//time units
		    		$currentMonth = $this->months[$selection];
					$timeSlices = $currentMonth->getTimeSlices($this, $this->selTypeNum);
					$daysList = $currentMonth->_filteredDays;

	        		if ($mainProduct->reservationSelType==reservation_interval::WEEK) {
	
	   					if ($this->potentialResa->_selType=="end")
	        				$timeslot=$mainProduct->_timeslotsObj->list[1];
	        			else $timeslot=$mainProduct->_timeslotsObj->list[0];
	        			//$smarty->assign('timeSlot',$timeslot);
	        			if (MYOWNRES_WEEK_TS) {
	        				$timeslots = $mainProduct->getTimeslots();
	        				unset($timeslots[0]);
	        				unset($timeslots[1]);
	        				$smarty->assign('timeSlots',$timeslots);
						} else $smarty->assign('timeSlots', array(0 => $timeslot));
						
	        			$smarty->assign('isMultipleMonth',false);
	        			$smarty->assign('reservationMonths',array($currentMonth));
	        			$template="view_weekmonth.tpl";
	        		} else {
	        			$smarty->assign('reservationMonth',$currentMonth);
	        			$template="view_daymonth.tpl";
	        			$legend = $this->months[$selection]->toString($cookie->id_lang);
	        		}
        		}
   				break;
   				
   			case reservation_view::QUARTER:	
   			//-----------------------------------------------------------------------------------------------------------------
   				//Selection
   				$maxSelection=ceil(count($this->months)/3)-1;
				if ($selection>$maxSelection) $selection=$maxSelection;
				
				//still need a timeslot for reservation cell
   				if (!$mainProduct->isTimeSelect())
   					$timeslot=$mainProduct->_timeslotsObj->list[0];
				if ($mainProduct->reservationSelType==reservation_interval::WEEK && $this->potentialResa->_selType=="end")
					$timeslot=$mainProduct->_timeslotsObj->list[1];		
				//time units
				$monthList=array();
				$quarterList=array();
				
				$timeSlices=array();
				for($i=0; $i<count($this->months); $i++)
					if (floor($i/3)==$selection) {
						$quarterMonths[$i] = $this->months[$i];
						$timeSlices += $quarterMonths[$i]->getTimeSlices($this, $this->selTypeNum);
					}
				$quarterList[$selection]=$quarterMonths;

				$smarty->assign('timeSlot',$timeslot);
				$smarty->assign('timeSlots', array(0 => $timeslot));
				$smarty->assign('quartersList',$quarterList);
				$smarty->assign('reservationMonths',$quarterMonths);
				$smarty->assign('isMultipleMonth',true);
				$smarty->assign('nbMonths',3);
				if ($mainProduct->reservationSelType==reservation_interval::WEEK) $template="view_weekmonth.tpl";
				if ($mainProduct->reservationSelType==reservation_interval::MONTH) $template="view_months.tpl";
				break;
				
			case reservation_view::YEAR: 
			//-----------------------------------------------------------------------------------------------------------------
				$maxSelection=ceil(count($this->months)/12)-1;
				if ($selection>$maxSelection) $selection=$maxSelection;
				
				//time units
				$quarterList=array();
				for($i=0; $i<count($this->months); $i++)
					if (floor($i/12)==$selection) {
						if (!array_key_exists(intval(floor($i/4)), $quarterList)) $quarterList[intval(floor($i/4))] = array();
						$quarterList[intval(floor($i/4))][$i] = $this->months[$i];
						$timeSlices += $quarterList[intval(floor($i/4))][$i]->getTimeSlices($this, $this->selTypeNum);
					}

				$smarty->assign('quartersList',$quarterList);
				$smarty->assign('timeSlot',$mainProduct->_timeslotsObj->list[0]);
				$smarty->assign('nbMonths',4);
				$template="view_months.tpl";
				break;
   		}
   		if ($template=='' && !$lengthWidget) $this->reservationEnabled=false;
   		//creating timeslices
   		//-----------------------------------------------------------------------------------------------------------------

		$cutTimes=($mainProduct->_reservationCutTime || $type=='home');
   		
		$bounds = array();
        $lengths = array();
        $min = 24*60;
        $max = 0;
        //getting array of bounds
        foreach ($this->timeSlots as $temp_timeslot) {
            if (!in_array($temp_timeslot->startTime, $bounds)) {
                $bounds[] = $temp_timeslot->startTime;
            }
            if (!in_array($temp_timeslot->endTime, $bounds)) {
                $bounds[] = $temp_timeslot->endTime;
            }
            $start = (strtotime($temp_timeslot->startTime)-strtotime('00:00'))/60;
            $stop = (strtotime($temp_timeslot->endTime)-strtotime('00:00'))/60;
            if ($start < $min) {
                $min = $start;
            }
            if ($stop > $max) {
                $max = $stop;
            }
        }
        
        if ($mainProduct->reservationSelType==reservation_unit::TIMESLOT) asort($bounds);

        //getting array of lengths
        $last_time='';
        foreach ($bounds as $bound) {
            if ($last_time!='') {
                $tmp_length = (strtotime($bound)-strtotime($last_time))/60;
                if (!in_array($tmp_length, $lengths)) {
                    $lengths[] = $tmp_length;
                }
            }
            $last_time = $bound;
        }
        $length = self::gcd_array($lengths);

        if ($max>0) {
	        //setting array of times
	        $timeList=array();
	        if ($cutTimes)
		        for ($i=$min; $i<=$max; $i+=$length) {
		        	$temptime = strtotime('00:00')+$i*60;
		            
					if ($mainProduct->_timeslotsObj->isTimeSlotOfTime($temptime)) {
						$timeList[$temptime] = date('H:i', $temptime);
					}
		        }
		    else
		    	foreach ($bounds as $bound_key => $bound_val) {
		        	$temptime = strtotime($bound_val);
		            //echo 'isTimeSlotOfTime:'.$bound_val;
					if ($mainProduct->_timeslotsObj->isTimeSlotOfTime($temptime)) {
						$timeList[$temptime] = date('H:i', $temptime);
					}
		        }
        } else {
	        $timeList[strtotime('00:00')] = '00:00';
        }

        $tstype=0;

		$dayParallel=$mainProduct->_timeslotsObj->getTimeSlotsColumnsDays($tstype);
        $smarty->assign(array('cutTimes' => $cutTimes,
        					'timeList' => $timeList,
        					'timeLength' => $length,
      						'timeSlotObj' => $mainProduct->_timeslotsObj,
							'dayParallel' => $dayParallel));
		
   		//checking timeslices
   		//-----------------------------------------------------------------------------------------------------------------

	   	if ($debug) $rustart = getrusage();
	   	if (count($timeSlices)) {
	   	

			if (!$lengthWidget) {
				if ($type=="home") {
					$homeproducts = $this->getAllProducts($cookie->id_lang);
					foreach($homeproducts as $homeproduct) {
						$isAvailabilities = myOwnAvailabilities::isAvailabilities(null, $homeproduct['id_product']);
						$homeproductsqty = $mainProduct->getProductQuantity($homeproduct['id_product']);
						foreach ($timeSlices as $timeSlice) {
							if (!$isAvailabilities) $timeSlice->availableQuantities[$homeproduct['id_product']] = $homeproductsqty;
							$timeSlice->defaultQuantities[$homeproduct['id_product']] = $homeproductsqty;
						}
					}
				}
				//respect order !
				//availabilities first because can give product qty
				
				$this->checkTimeSlicesResaRules($obj, $this->potentialResa, $timeSlices, $cookie->id_lang, $type);$this->checkTimeSlicesAvailabilities($obj, $mainProduct->_timeslotsObj->list, $timeSlices, strtolower($type));
				$this->checkTimeSlicesOccupation($this->potentialResa, $timeSlices, $cart);

			}
			
		}

		//debug time
		if ($debug) {
			$ruend = getrusage();
			echo "check Time Slices " . self::rutime($ruend, $rustart, "utime") .  "<br>";
		}
		
		$planningStyle = (strtolower($type)=='product' && Configuration::get('MYOWNRES_WIDGET_PROD', 0)==widget_position::RIGHT ? 'topproduct' : strtolower($type));
		
		
		//changing a timeslots list per month/week selection view, into a day with timeslot list per month/week view.
		//-----------------------------------------------------------------------------------------------------------------
		if (!$lengthWidget && $mainProduct->_reservationTimeByDay && $mainProduct->isTimeSelect() && ($reservationSelPlanning==reservation_view::MONTH || $reservationSelPlanning==reservation_view::TWOWEEKS)) {

			//getting index timeslot
			$defaultTimeSlot = myOwnTimeSlots::getDefault(-1, true);
			$timeslotsGroups = MyOwnCalendarTool::getTimeslotsGroups($this->timeSlots);
	
			//set selecatble days for days select input
			$subSelector=array();
			foreach($daysList as $subDay) {
				$tempSlice = $subDay->getSlice(-1);
				if ($tempSlice!=null && $tempSlice->isAvailable && $tempSlice->isEnable && $tempSlice->availableQuantity!=0 )
					$subSelector[date('Y-m-d',$subDay->date)] = MyOwnCalendarTool::formatDate($subDay->date, $cookie->id_lang);
			}
		    $smarty->assign(array ('subSelector'			=>	$subSelector,
		    						'previous_sub_label'	=>	myOwnLang::getUnit($mainProduct->reservationSelPlanning),
		    						'timeslotsGroups'		=>	$timeslotsGroups,
		    						'timeSlotList'			=>	array($defaultTimeSlot),
		    						'isDaySel'				=>	true,
		    						'daysList'				=>	$daysList,
		    						'planningType'			=>	strtolower($type).'sub',
		    						'planningStyle' 		=>  strtolower($type).'sub',
		    						'planningSelector'		=>	strtolower($type)));
			
			//changing the default view to ignore timeslots
			$isDaySel=true;
		       		
			if ($debug) $rustart = getrusage();
			$outputExtra= $obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/front/planning/view_day.tpl');
			//debug time
			if ($debug) {
				$ruend = getrusage();
				echo "Extra month " . self::rutime($ruend, $rustart, "utime") .  "<br>";
			}
		}
		
		//display
		//-----------------------------------------------------------------------------------------------------------------
		$planningType = $type;
		
		
		
		$next_label='';$previous_label='';
		if ($type!="column" && $type!='topcolumn' && $planningStyle!="topproduct")  {
			$next_label=myOwnLang::$reservSwitchesNext[$mainProduct->reservationSelPlanning];
			$previous_label=myOwnLang::$reservSwitchesPrevious[$mainProduct->reservationSelPlanning];
		}
		if ($type=="home" || $type=='topcolumn')  {
				$cats = explode(',',$mainProduct->ids_categories);
				$widgetHome = Configuration::get('MYOWNRES_WIDGET_HOME');
				$obj->_products->getCategoriesLink($cats, ($widgetHome>1 ? $obj->l('All', 'calendarView') : ''));
		}

		
		//if ($type=='topcolumn')
 		//	$type='column';
   		//smarty assign
		$smarty->assign(array(
 			'isDaySel'=>$isDaySel,
			'sel'=>$selection,
			'legend'=>$legend,
			'isajaxloaded'=>$ajax,
			'maxSelection'=>$maxSelection,
			'next_label'=>$next_label,
			'previous_label'=>$previous_label,
			'multiSel'	=> ($type!='column' && $type!='topcolumn' ? intval($this->multiSel) : 0),
			'planningSelector'	=>	strtolower($planningType),
			'planningType' => strtolower($type),
			'planningStyle' => $planningStyle,
			'lengthWidget' => $lengthWidget,
			'myPlanningBox' => Configuration::get('MYOWNRES_WIDGET_PROD', 0)==widget_position::POPUP,
			'widgetTypeVal' => Configuration::get('MYOWNRES_WIDGET_TYPE'),
			'_PS_VERSION_' => _PS_VERSION_
			));
		
		if ($this->reservationEnabled)	{
			//if less than 3 time slots in a month view we don't have to select day so we made the planning header floatbble
			$isFloatingPlanning = ($planningType!='column' && $mainProduct->reservationSelType==reservation_unit::TIMESLOT && $reservationSelPlanning==reservation_view::MONTH && $mainProduct->_reservationTimeByDay && Configuration::get('MYOWNRES_WIDGET_PROD', 0)==widget_position::TABS);
			if ($isFloatingPlanning)
				$output.= '<div class="myFloatBtn">';
	
			$extraSel = '';
			if ($planningType=='column' && $mainProduct->productType== product_type::AVAILABLE)
			foreach ($obj->_extensions->getExtByType(extension_type::SEL_WIDGET) as $ext) {	
// 				if (in_array($ext->id, $resa->_mainProduct->ids_notifs)) {
					if (method_exists($ext, "widgetSelect"))
							$extraSel.=$ext->widgetSelect($obj, $mainProduct, $cookie->id_lang, "column", $objectSelection);
			}
			$output.= $extraSel;
	 		$ext_content='';
	 		$extra_step='';
	 		$header_extra='';
	 		if ($mainProduct->_qtyType == reservation_qty::ATTENDEES && Tools::getIsset('qty_field') && $this->potentialResa->_selType=="end") {
	 			foreach ($obj->_extensions->getExtByType(extension_type::CARTDETAILS) as $ext) {
					if (method_exists($ext, 'displayCartDetails') && method_exists($ext, 'displayShoppingCart') && !$ext->getConfig(0, attendees::CONFIG_FROMCART, 0 ,1)) {
						$header_extra = $ext->displayCartDetails($obj, null, -1, 'addcart');
						$extra_step = '$(\'.primary_block table\').hide();$(\'.myOwnUnvalidate\').parent().hide();$(\'.myOwnCalendarHeader\').eq(0).show();$(\'.attendees_form\').show();';
						$header_extra .= $ext->displayShoppingCart($obj);
					}
				}
	 		}
	 		
	 		$smarty->assign('extra_step' , $extra_step);
	 		
	 		//Add extenssion content
			foreach ($obj->_extensions->getExtByType(extension_type::SHOW_OBJECTS) as $ext) 
				if (method_exists($ext, "getTab"))
					$output.=$ext->getTab($obj, $mainProduct, $planningStyle);
					
	 		if (strtolower($planningType)!='topcolumn') {
	 			$output.= $obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/front/planning/header.tpl').$header_extra;
	 		}
		 	if (!$lengthWidget || $this->potentialResa->_selType!="end") {
		 		$output.= $obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/front/planning/navigate.tpl');
	     	}
	     	
		 	
	     	
	     	if ($lengthWidget && $this->potentialResa->_selType=="end") {
				//$output.= $this->getLengths($obj, $this->availabilities);
				$lengthSlices = $this->getLengths($obj, $this->availabilities);
				
				$this->checkTimeSlicesResaRules($obj, $this->potentialResa, $lengthSlices, $cookie->id_lang, $type);$this->checkTimeSlicesAvailabilities($obj, $mainProduct->_timeslotsObj->list, $lengthSlices, strtolower($planningType));
				$this->checkTimeSlicesOccupation($this->potentialResa, $lengthSlices, $cart);
		
				
				//$this->checkTimeSlicesOccupation($this->potentialResa, $timeSlices, $cart);
				$this->reservationEnabled = count($lengthSlices)>1;
				//$smarty->assign('availableLengths',$lengths);
				$smarty->assign('lengthSlices',$lengthSlices);
		   		$output.= $obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/front/planning/length.tpl');
				
			} else $output.= $obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/front/planning/'.$template).$outputExtra;
			
			if (!$lengthWidget && $type=="product" && $reservationSelPlanning==reservation_view::MINIMAL) $output.= $obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/front/planning/footer.tpl');
			
			//close myFloatBtn
			//if less than 3 time slots in a month view we don't have to select day so we made the planning header floatbble
			if ($isFloatingPlanning)
				$output.= '</div>';
		}
		
		$output.= $obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/front/planning/script.tpl');
	
			
   		return $output;
   	}


   	public function getMaxLength ($availabilities=null) {
   		$planningEndDate=0;
   		$planningEndTimeslot=0;
   		$tmpresa = clone $this->potentialResa;

		$planningEndDate = $this->end;
		$planningEndTimeslot = $this->potentialResa->_mainProduct->getLastTimeSlot($planningEndDate, $this->start, $availabilities);

		if ($planningEndTimeslot!=null) $tmpresa->endTimeslot = $planningEndTimeslot->sqlId;
		else $tmpresa->endTimeslot = $tmpresa->startTimeslot; 

   		$tmpresa->endDate = date('Y-m-d',$planningEndDate);

		return $tmpresa->getLength(false, $availabilities)+1;
   	}

   	public function getLengths ($obj, $availabilities=null) {
   		global $smarty;
   		$lengths=array();
   		$mainProduct = $this->potentialResa->_mainProduct;
   		$freq = $mainProduct->_reservationLengthFreq;
   		if ($this->potentialResa->startTimeslot) $timeSlot = $obj->_timeSlots->list[$this->potentialResa->startTimeslot];
   		else $timeSlot = $mainProduct->_timeslotsObj->list[$this->potentialResa->startTimeslot];
   		$slices=array();

   		if ($mainProduct->_reservationLengthControl == reservation_length::LIMIT || $mainProduct->_reservationLengthControl == reservation_length::FIXED) {
   			$maxLength = $mainProduct->_reservationMaxLength;
   			if (!$mainProduct->_reservationLengthCanExceed) {
	   			$_maxLength = $this->getMaxLength ($availabilities=null);
	   			if ($_maxLength<$maxLength) $maxLength=$_maxLength;
   			}
   			$min = $mainProduct->_reservationMinLength;
   			if (!$min) $min=1;
   			if ($min>$maxLength) $min = $maxLength;
   			for ($i=$min; $i<$maxLength+1; $i++) {
   				$timeSlice = new myOwnCalendarLengthSlice(strtotime($this->potentialResa->startDate), $timeSlot, $i, $this);
   				if ($freq<2 or ($i%$freq==0)) {
   					$lengths[$i]=$i.' '.myOwnLang::getUnit($mainProduct->_reservationUnit, $i>1);
   					$timeSlice->isEnable=true;
   				} else $timeSlice->isEnable=false;
   				$slices[$i] = $timeSlice;
   			}
   				
   		} else if ($mainProduct->_reservationLengthControl == reservation_length::FREE) {
	   		$maxLength = $this->getMaxLength ($availabilities=null);
			
			for ($i=1; $i<$maxLength+1; $i++) {
				$timeSlice = new myOwnCalendarLengthSlice(strtotime($this->potentialResa->startDate), $timeSlot, $i, $this);
				if ($freq<2 or ($i%$freq==0)) {
   					$lengths[$i]=$i.' '.myOwnLang::getUnit($mainProduct->_reservationUnit, $i>1);
   					$timeSlice->isEnable=true;
   				} else $timeSlice->isEnable=false;
   				$slices[$i] = $timeSlice;
   			}
   			//myOwnLang::getUnit($mainProduct->_reservationUnit
   		} else {

	   		$lengths[1]='1 '.myOwnLang::getUnit($mainProduct->_reservationUnit, false);
	   		$slices[1] = new myOwnCalendarLengthSlice(strtotime($this->potentialResa->startDate), $timeSlot, 1, $this);
   		}
   		return $slices;
   		
   	}
   	
   	public static function gcd($a, $b)
    {
        if ($a == 0 || $b == 0) {
            return abs(max(abs($a), abs($b)));
        }
            
        $r = $a % $b;
        return ($r != 0) ?
            self::gcd($b, $r) :
            abs($b);
    }
    
    
    public static function gcd_array($array, $a = 0)
    {
        $b = array_pop($array);
        return ($b === null) ?
            (int)$a :
            self::gcd_array($array, self::gcd($a, $b));
    }

   	public function getSelector ($obj) {
   		global $cookie;
   		$selector = array();
   	   	switch ( $this->potentialResa->_mainProduct->reservationSelPlanning) {
   			case reservation_view::DAY: //a day
       			//for($i=0; $i<$this->length; $i++) strtotime("+".$i."day",$this->start)
       			foreach ($this->days as $sday)
        			$selector[] = MyOwnCalendarTool::formatDate($sday->date, $cookie->id_lang);
  				break;
			case reservation_view::WEEK:
				for($i=0; $i<count($this->weeks); $i++)
        			$selector[] = $this->weeks[$i]->toString(null, $obj->l('week of', 'calendarView'));
				break;
			case reservation_view::TWOWEEKS:
				for($i=0; $i<count($this->weeks); $i++)
        			if ($i%2==0)
        				if (array_key_exists($i+1, $this->weeks)) {
	        				$selector[] = $this->weeks[$i]->toString($this->weeks[$i+1]);
        				} else {
	        				$selector[] = $this->weeks[$i]->toString();
        				}
				break;
	   		case reservation_view::MONTH:
	   			foreach($this->months as $month) {
		   			$selector[]=$month->toString($cookie->id_lang);
	   			}
	   			break;
   			case reservation_view::QUARTER:	
   				for($i=0; $i<count($this->months); $i++)
        			if ($i%3==0)
        				if (array_key_exists($i+2, $this->months)) {
	        				$selector[] = $this->months[$i]->toString($cookie->id_lang, $this->months[$i+2]);
	        			} else if (array_key_exists($i+1, $this->months)) {
	        				$selector[] = $this->months[$i]->toString($cookie->id_lang, $this->months[$i+1]);
        				} else {
	        				$selector[] = $this->months[$i]->toString($cookie->id_lang);
        				}
				break;
			case reservation_view::YEAR: 
				for($i=0; $i<count($this->months); $i++)
					if ($i%12==0) {
						$selector[] = $this->months[$i]->year;
					}
				break;
   		}
   		return $selector;
   	}
   	
	public static function rutime($ru, $rus, $index) {
    	return ($ru["ru_$index.tv_sec"]*1000 + intval($ru["ru_$index.tv_usec"]/1000))
     -  ($rus["ru_$index.tv_sec"]*1000 + intval($rus["ru_$index.tv_usec"]/1000));
	 }
	
}
	
?>