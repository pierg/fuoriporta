<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class MyOwnReservationsUtils extends myOwnUtils {

	public static function displayStarter($obj) {
		$step = (int)Configuration::get('MYOWNRES_STARTER', 0);
		$out = '';
		if ($step != -1 && $step < 5) {
			$out = '
	<div class="alert alert-onboarding"><div id="onboarding-starter" class="">
			<div class="row">
				<div class="col-md-12">
					<h3>'.$obj->l('Your first steps with myOwnReservations', 'utils').'</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-3 col-md-2 col-md-offset-2">
					<div class="onboarding-step step-first '.($step==1 ? 'step-in-progress' : '').' '.($step>=1 ? 'active step-success' : 'step-todo').' step-success"></div>
				</div>
				<div class="col-xs-3 col-md-2">
					<div class="onboarding-step '.($step==2 ? 'step-in-progress' : '').' '.($step>=2 ? 'active step-success' : 'step-todo').'"></div>
				</div>
				<div class="col-xs-3 col-md-2">
					<div class="onboarding-step '.($step==3 ? 'step-in-progress' : '').' '.($step>=3 ? 'active step-success' : 'step-todo').'"></div>
				</div>
				<div class="col-xs-3 col-md-2">
					<div class="onboarding-step step-final '.($step==4 ? 'step-in-progress' : '').' '.($step>=4 ? 'active step-success' : 'step-todo').'"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-3 col-md-2 col-md-offset-2 text-center">
					<a '.($step==1 ? 'href="'.myOwnReservationsController::getConfUrl('products').'"' : 'style="color:gray; text-decoration:none"').'>'.myOwnHelp::$tuto['1'].'</a>
				</div>
				<div class="col-xs-3 col-md-2 text-center">
					<a '.($step==2 ? 'href="'.myOwnReservationsController::getConfUrl('availability').'"' : 'style="color:gray; text-decoration:none"').'>'.myOwnHelp::$tuto['2'].'</a>
				</div>
				<div class="col-xs-3 col-md-2 text-center">
					<a '.($step==3 ? 'href="'.myOwnReservationsController::getConfUrl('pricesrules').'"' : 'style="color:gray; text-decoration:none"').'>'.myOwnHelp::$tuto['3'].'</a>
				</div>
				<div class="col-xs-3 col-md-2 text-center">
					<a '.($step==4 ? 'href="'.myOwnReservationsController::getConfUrl('advanced').'"' : 'style="color:gray; text-decoration:none"').'>'.myOwnHelp::$tuto['4'].'</a>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-lg-8">
					<h4>'.myOwnHelp::$tuto[$step].'</h4>
					<p>'.str_ireplace('\n', '<br>', myOwnHelp::$tuto['step'.$step]).'</p>
				</div>
				<div class="col-lg-4 onboarding-action-container">
					<a href="'.myOwnReservationsController::getConfUrl('products', '&current_step='.($step+1)).'" class="btn btn-default btn-lg quick-start-button pull-right">'.($step==0 ? $obj->l('Let\'s Go', 'utils') : $obj->l('Ok, go to next step', 'utils')).'&nbsp;&nbsp;
						<i class="icon icon-angle-right icon-lg"></i>
					</a>
					<a class="btn btn-default btn-lg pull-right" href="'.myOwnReservationsController::getConfUrl('products', '&current_step=-1').'" id="onboarding-close">
						'.$obj->l('No thanks !', 'utils').'&nbsp;&nbsp;
						<i class="icon icon-times icon-lg"></i>
					</a>
				</div>
			</div>
		</div></div>';
		}
		return $out;
	}

	
		//list addresses for geocoding once user logged
	//
	public static function listAddresses($cookie) {
		$out='';
		if ($cookie->id_customer>0) {
		    $cust = new Customer($cookie->id_customer);
	    	$addresses = $cust->getAddresses($cookie->id_lang);
	    	$default = Address::getFirstCustomerAddressId($cookie->id_customer, true);
	        $out.= '
	        <script type="text/javascript">
	        		//var defaultCustAddress='.intval($default).';
	        		myCollectionPlacesAdrresses = new Array();';
	        foreach ($addresses as $address)
	        	$out.= 'myCollectionPlacesAdrresses['.$address["id_address"].']="'.addslashes($address["address1"].' '.$address["address2"].' '.$address["postcode"].' '.$address["city"].' '.$address["country"]).'";';
	        if (Tools::getValue('labulleCustAddrPosition','') == '')
	        	$out .= '$(document).ready(function() { myCollectionPlacesGeocodeAddresses(); });';
	       	$out .= '
	        </script>';
        }
		return $out;
	}
	
	public static function getImage($obj, $name='placeImage', $folder='places', $id=0, $maxwidth=120, $maxheight=80) {
		$out='';
		$errors=array();
		if (isset($_FILES[$name]["name"]) && $_FILES[$name]["size"]>0) {
			$id_place=Tools::getValue('editplace', $id);
				$types = array('.gif' => 'image/gif', '.jpeg' => 'image/jpeg', '.jpg' => 'image/jpg', '.png' => 'image/png');
				$ext = substr($_FILES[$name]["name"], -4);
				$type = (isset($types[$ext]) ? $types[$ext] : '');
				if (_PS_VERSION_ >= "1.5.0.0") $error = ImageManager::validateUpload($_FILES[$name], 2000000);
				else $error = checkImage($_FILES[$name], 2000000);
				if ($error) $errors[] = $error;
				$error = MyOwnReservationsUtils::checkImageUploadError($_FILES[$name]);
				if ($error) $errors[] = $error;
				
				if (count($errors)==0) {
					if (!is_dir (_PS_IMG_DIR_.$folder)) mkdir(_PS_IMG_DIR_.$folder);
					list($sourceWidth, $sourceHeight, $type, $attr) = getimagesize($_FILES[$name]["tmp_name"]);
					if ($sourceWidth > $maxwidth or $sourceHeight > $maxheight) {
						$width=$maxwidth;
						$height=$maxheight;
					} else {
						$width=$sourceWidth;
						$height=$sourceHeight;
					}
					if (_PS_VERSION_ >= "1.5.0.0") ImageManager::resize($_FILES[$name]["tmp_name"], _PS_IMG_DIR_.$folder.'/'.$id_place.'.png', $width, $height, 'png');
					else imageResize($_FILES[$name]["tmp_name"], _PS_IMG_DIR_.$folder.'/'.$id_place.'.png', $width, $height, 'png');
				}
				if (count($errors)>0) {
					unlink($_FILES[$name]["tmp_name"]);
					foreach ($errors as $error) 
						$out .= self::displayError($error);
				} else {
					$out .= self::displayConf($obj->l('The place picture', 'utils').' '.$_FILES[$name]["name"].' '.$obj->l('has been saved in', 'utils').' '._PS_IMG_DIR_.$folder.'/'.$id_place.'.png');
				}
		}
		return $out;
	}
	

	
	public static function getsubtitle($title)
	{
		if (_PS_VERSION_ >= "1.6.0.0") return '<h4>'.$title.'</h4>';
		else return '<h3  style="margin-bottom:5px;margin-top:8px">'.$title.'</h3>';
	}
	
	public static function getOrderLink($prodFamily, $filter='') {
		global $cookie;
		$output = '<input type="image" onclick="$(\'#orderby\').val(\''.$filter.'\');$(\'#orderway\').val(\'desc\')" src="../modules/myownreservations/img/down';
			if (isset($_GET['orderby']) && $_GET['orderby']==$filter && $_GET['orderway']=='desc') $output .='_d';
			$output .='.gif"> <input type="image" onclick="$(\'#orderby\').val(\''.$filter.'\');$(\'#orderway\').val(\'asc\')" src="../modules/myownreservations/img/up';
				if (isset($_GET['orderby']) && $_GET['orderby']==$filter && $_GET['orderway']=='asc') $output .='_d';
				$output .='.gif">';
		return $output;
	}
	
	public static function getModuleLink($link, $method) {
		if (_PS_VERSION_ < "1.5.0.0") {
			return __PS_BASE_URI__.'/modules/myownreservations/controllers/ajax.php?method='.$method;
		} else 
			return str_ireplace('http://', (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS']!='' ? 'https://' : 'http://' ), $link->getModuleLink('myownreservations', 'actions', array('method'=>$method)));
	}
	
	public static function table16wrap($obj, $tab, $objet, $title, $count=-1, $btns='') {
		global $cookie;
			$output = '
			<div class="panel tablepanel col-lg-12">
				<div class="panel-heading" '.($tab == 'stocks' || $tab == 'reservations' ? 'style="height:auto;text-verflow: auto;white-space: unset;"' : '').'>
					'.$title.'				'.($count >= 0 ? '<span class="badge">'.$count.'</span>' : '').$btns.'
					'.($tab != '' ?
					'<span class="panel-heading-action">
							'.($objet!='' ? '<a id="desc-customer-new" class="list-toolbar-btn" '.(stripos($objet, ';')===false ?'href="'.myOwnReservationsController::getConfUrl($tab, '&edit'.$objet.'=0').'"' : 'onclick="'.$objet.'"').'>
								<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="'.ucfirst(myOwnLang::$actions[MYOWN_OPE::ADD]).'" data-html="true">
									<i class="process-icon-new "></i>
								</span>
							</a>' : '').'
							<a id="desc-customer-export" class="list-toolbar-btn" '.($tab=='reservations' ? 'onclick="exportPeriod();"' : 'href="ajax-tab.php?tab=adminmyownreservations'.'&token='.Tools::getAdminToken('adminmyownreservations'.intval(Tab::getIdFromClassName('adminmyownreservations')).intval($cookie->id_employee)).'&action=exportConfig&myOwnResTab='.$tab.'"').'>
								<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="'.ucfirst(myOwnLang::$actions[MYOWN_OPE::EXPORT]).'" data-html="true">
									<i class="process-icon-export "></i>
								</span>
							</a>				
					</span>'
					: '').'
				</div>';
		return $output;
	}
	
	public static function getDateField($name, $value, $label='', $style='', $onchange='', $class='') {
		if (_PS_VERSION_ >= "1.6.0.0") return '
				<div class="input-group" style="'.$style.'">
					'.( $label!='' ? '<span class="input-group-addon">'.$label.'</span>' : '').'
					<input type="text" class="datepicker input-medium" style="width:105px;" name="'.$name.'" value="'.$value.'" id="'.$name.'" onChange="'.$onchange.'">
					<span class="input-group-addon"><i class="icon-calendar-empty"></i></span>
				</div>';
		else return ( $label!='' ? '<span>'.$label.'</span>&nbsp;' : '').'<input class="'.$class.'" size="10" type="text" style="'.$style.'" name="'.$name.'" id="'.$name.'" value="'.$value.'" onChange="'.$onchange.'" />';
	}
	
	public static function getFileField($name='file') {
		$output = '
		<script type="text/javascript">
		function checkFileUpload(str) {
			/*str=elem.value;
			alert(str);*/
			$("#importType option").each(function( index ) {
				if ($(this).val()!="") {
					if (str.indexOf($(this).val()) > -1) $(this).attr("selected", true);
				}
			});
		}
		</script>';
		
		if (_PS_VERSION_ < "1.6.0.0") return $output.'<input style="display:inline-block;width:25%" name="file" type="file" onchange="checkFileUpload($(this).val())">';
		else 
		return $output.'
		<script type="text/javascript">
			$(document).ready(function(){
				$("#'.$name.'-selectbutton").click(function(e) {
					$("#'.$name.'").trigger("click");
				});
		
				$("#'.$name.'-name").click(function(e) {
					$("#'.$name.'").trigger("click");
				});
		
				$("#'.$name.'-name").on("dragenter", function(e) {
					e.stopPropagation();
					e.preventDefault();
				});
		
				$("#'.$name.'-name").on("dragover", function(e) {
					e.stopPropagation();
					e.preventDefault();
				});
		
				$("#'.$name.'-name").on("drop", function(e) {
					e.preventDefault();
					var files = e.originalEvent.dataTransfer.files;
					$("#'.$name.'")[0].files = files;
					$(this).val(files[0].name);
				});
		
				$("#'.$name.'").change(function(e) {
					if ($(this)[0].files !== undefined)
					{
						var files = $(this)[0].files;
						var name  = "";
		
						$.each(files, function(index, value) {
							name += value.name+", ";
						});
		
						$("#'.$name.'-name").val(name.slice(0, -2));
					}
					else // Internet Explorer 9 Compatibility
					{
						var name = $(this).val().split(/[\\/]/);
						$("#'.$name.'-name").val(name[name.length-1]);
					}
					checkFileUpload(name);
				});
		
			});
		</script>
		<div class="col-sm-6">
			<input id="'.$name.'" type="file" name="'.$name.'" class="hide">
			<div class="dummyfile input-group">
				<span class="input-group-addon"><i class="icon-file"></i></span>
				<input id="'.$name.'-name" type="text" name="filename" readonly="">
				<span class="input-group-btn">
					<button id="'.$name.'-selectbutton" type="button" name="submitAddAttachments" class="btn btn-default">
						<i class="icon-folder-open"></i> Ajouter un fichier				</button>
								</span>
			</div>
		</div>
		<div style="clear:both"></div>';
	}
	
	public static function displayIncludes($front=false) {
		$out = '
		<link href="'._MODULE_DIR_.'myownreservations/views/css/back.css" rel="stylesheet" type="text/css" media="all" />
		<script src="'._MODULE_DIR_.'myownreservations/views/js/back.js"></script>
		'.(_PS_VERSION_ > "1.5.0.0" ? '
		<link rel="stylesheet" type="text/css" href="'._MODULE_DIR_.'myownreservations/views/css/tooltipster.css" />
		'.(!$front ? '<script type="text/javascript" src="'._MODULE_DIR_.'myownreservations/views/js/jquery.tooltipster.min.js"></script>':'').'
		<script type="text/javascript">
	        $(document).ready(function() {
	            $(\'.tooltip\').tooltipster({
				   maxWidth: 600,
				   icon: \'(?)\',
				   iconDesktop: false,
				   iconTouch: false,
				   iconTheme: \'.tooltipster-icon\',
				});
	        });
	    </script>' :'');
	    return $out;
	}

	
	public static function getListButton($obj, $id, $name='place', $field=' ', $args='', $anchor = '') {
		$out='';
		if ($field==' ') $field=$name;
		$out.='
								
		<div class="btn-group-action">
			<div class="btn-group pull-right baseActions" id="baseActions'.$id.'">
				<a '.($name=='deliveries' ? 'onclick="rescheduleAction('.$id.');"' : 'href="'.$obj->getConfUrl($name, '&edit'.$field.'='.$id.$args, $anchor).'"').' title="Modifier" class="edit btn btn-default">
					<i class="icon-pencil"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::EDIT]).'
				</a>
				<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					<i class="icon-caret-down"></i>&nbsp;
				</button>
				<ul class="dropdown-menu">
					
					<li>
						<a href="'.$obj->getConfUrl($name, '&edit'.$field.'='.$id.$args, $anchor).'" class="" title="'.$obj->l('Details', 'utils').'">
							<i class="icon-eye-open"></i> '.$obj->l('Details', 'utils').'
						</a>
					</li>
					<li>
						<a href="'.$obj->getConfUrl(($name=='timeslots' && $args!='' ? 'products' : $name), '&delete'.$field.'='.$id.$args, $anchor).'" onclick="if (confirm(\''.$obj->l('Delete item ?', 'utils').'\')){ return true; }else{ event.stopPropagation(); event.preventDefault();};" title="'.ucfirst(myOwnLang::$actions[MYOWN_OPE::DELETE]).'" class="delete">
							<i class="icon-trash"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::DELETE]).'
						</a>
					</li>
					'.($name!='availabilities' && $name!='timeslots' && $name!='productPlaces' ? '
					<li>
						<a href="'.$obj->getConfUrl($name, '&copy'.$field.'='.$id.$args, $anchor).'" title="'.$obj->l('Duplicate', 'utils').'">
							<i class="icon-copy"></i> '.$obj->l('Duplicate', 'utils').'
						</a>
					</li>
					' : '
					
					').'
				</ul>
			</div>
		</div>';
		return $out;
	}

	public static function adjustBrightness($hex, $percent) {
	
	    // Format the hex color string
	    $hex = str_replace('#', '', $hex);
	    if (strlen($hex) == 3) {
	        $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
	    }
	
	    // Get decimal values
	    $r = hexdec(substr($hex,0,2));
	    $g = hexdec(substr($hex,2,2));
	    $b = hexdec(substr($hex,4,2));

	    if ($percent>0) {
		    $r += round((256 - $r) * $percent / 100);
		    $g += round((256 - $g) * $percent / 100);
		    $g += round((256 - $b) * $percent / 100);
	    } else {
	    	$r += round($r * $percent / 100);
		    $g += round($g * $percent / 100);
		    $g += round($b * $percent / 100);
		}
	
	    $r_hex = str_pad(dechex(max(0,min(255,$r))), 2, '0', STR_PAD_LEFT);
	    $g_hex = str_pad(dechex(max(0,min(255,$g))), 2, '0', STR_PAD_LEFT);
	    $b_hex = str_pad(dechex(max(0,min(255,$b))), 2, '0', STR_PAD_LEFT);
	    return '#'.$r_hex.$g_hex.$b_hex;
	}

	public static function insertProductFamillyPicker($obj, $id_lang, $title="productFamillyFilter", $class="", $style="font-weight:bold;", $value="", $onchange="", $forced=false, $productField="", $ids_products="", $unitSelector="", $multiple=true) {
		$output='';
		$output .= '
		<script type="text/javascript">
			var familiesProducts=new Array();
			var idFamily=0;
			var familliesSelect="";
			var selFamily='.intval($value).';
			var familyUnits=new Array();
			familiesProducts[0]=new Array();';
			$output .= myOwnReservationsProductsController::getUnitLabelJS($obj);
		foreach($obj->_products->list as $productFamily) {
			$products=$productFamily->getProducts($id_lang);
			$output .= '
			familiesProducts['.$productFamily->sqlId.']=new Array();
			familyUnits['.$productFamily->sqlId.']='.$productFamily->_reservationUnit.';
			familiesProducts['.$productFamily->sqlId.'][0]="'.$obj->l('All', 'utils').'";';
			foreach($products as $product)$output .= '
			familiesProducts['.$productFamily->sqlId.']['.$product['id_product'].']="'.addslashes($product['name']).'";';
		}
		$output .= '
			function populateMainFamily() {
				var id_product = $("#'.$productField.'").val();
				var selFamily = productFamilly[id_product];
				$("#'.$title.'").val(selFamily);
			}
			function populateProducts(id_family, savedProds) {	
				idFamily = id_family;
				if (id_family>0) {
					if ($("#'.$productField.'").length) {
						if (familliesSelect=="") familliesSelect = $("#'.$productField.'").html();
						var prods = familiesProducts[id_family];
						if(prods.length>0) {
							var contents="";
							for (i = 0; i < prods.length; i++) {
								if (prods[i]!=undefined && prods[i].length>0) {
									contents+="<option value=\""+i+"\"";
									if (savedProds!=undefined && savedProds!="") {
										savedProdsTab = savedProds.split(",");
										if (savedProdsTab.indexOf(i.toString())!=-1) contents+=" selected";
									}
									else if (i==0 && savedProds=="") contents+=" selected";
									contents+=">"+prods[i]+"</option>";
								}
							}
							'.($multiple ? '
							$("#'.$productField.'").attr("name", "'.$productField.'[]");
							$("#'.$productField.'").attr("multiple","multiple");
							$("#'.$productField.'").attr("size","3");
							' : '
							$("#'.$productField.'").attr("name", "'.$productField.'");
							').'
							$("#'.$productField.'").html(contents);						
							
							populateAttribute($("#'.$productField.'Attribute").val());
						} else $("#'.$productField.'").css("display","none");
					}
					'.($unitSelector!='' ? 'var resvUnit = familyUnits[id_family];
					var unitstr = getUnitLabel(resvUnit);
					if ($("'.$unitSelector.'").parent().hasClass("input-group-addon")) {
						if (unitstr=="") $("'.$unitSelector.'").parent().hide();
						if (unitstr!="") $("'.$unitSelector.'").parent().show();
					}
					$("'.$unitSelector.'").html(unitstr);' : '').'
					
				} else {
					$("'.$unitSelector.'").html("");
					if ($("'.$unitSelector.'").parent().hasClass("input-group-addon"))
						$("'.$unitSelector.'").parent().hide();
					if (familliesSelect!="") {
						$("#'.$productField.'").html(familliesSelect);
						//$("#'.$productField.'").removeAttr("multiple");
						$("#'.$productField.'").attr("name", "'.$productField.'");
						$("#'.$productField.'").removeAttr("size");
					}
				}
				if (typeof(populateTimeslot)!=\'undefined\') {
					populateTimeslot(\'start\');
					populateTimeslot(\'end\');
				}
				$("#'.$productField.'").trigger("chosen:updated");
			}
			$(document).ready(function() {
				populateProducts('.intval($value).', "'.(is_array($ids_products) ? implode(',', $ids_products) : $ids_products).'");
				if ("'.$productField.'"!="" && typeof(populateAttribute)!=\'undefined\') populateAttribute($("#'.$productField.'Attribute").val());
			});
			//# sourceURL=populateProducts.js
		</script>
		';
		
		$output .= '
		<select onchange="populateProducts(this.value);'.$onchange.'" class="'.$class.'" style="'.$style.'" id="'.$title.'" name="'.$title.'" '.($forced ? 'disabled' : '').'>';
		$output .= '<option value=""';
		if ($value=="") $output .= ' selected';
		$output .= '>'.$obj->l('All', 'utils').'</option>';
		if (count($obj->_products->list))
		foreach($obj->_products->list AS $product) 
			if (stripos($title, 'timeSlot')===false OR $product->reservationSelType ==reservation_interval::TIMESLOT ) {
			$output .= '<option value="' . $product->sqlId. '"';
			if ($value==$product->sqlId) $output .= ' selected';
			$output .= '>'.(trim($product->name) == '' ? ucfirst(myOwnLang::$object[MYOWN_OBJ::FAMILY]).' #'.$product->sqlId : $product->name).'</option>';
		}
		$output .= '
		</select>';
		return $output;
	}

	public static function getOccupationColor($percent) {
		$rmin=180; $rmax=255;
		$gmin=$rmin; $gmax=$rmax;
		//$percent=100-$invper;
		$g=($percent>50 ? round($rmax-(($rmax-$rmin)/100*(($percent-50)*2))) : $rmax);
		$r=($percent>50 ? $gmax : round($gmin+(($gmin-$gmax)/100*($percent*2))));
		$b=180;		
		$hex = "#";
		$hex.= str_pad(dechex($r), 2, "0", STR_PAD_LEFT);
		$hex.= str_pad(dechex($g), 2, "0", STR_PAD_LEFT);
		$hex.= str_pad(dechex($b), 2, "0", STR_PAD_LEFT);
		
		return $hex;
	}

	public static function insertProductPicker($obj, $id_lang, $productField="product", $productVal=0, $productAttrField="productAttribute", $productAttrVal=0, $style, $action, $productFilter=0, $unitSelector="", $allSelect=true, $filter='') {
		$products=$obj->_products->getProducts($id_lang);
		$multiple=($productField=="pricesruleProduct");
		$output='';
		$output .= '
		<script type="text/javascript">
			var productFamilly=new Array();
			var product=new Array();
			var familyUnits=new Array();
			var familyOptions=new Array();
			'.(is_array($productAttrVal) ? 'var productAttrValArr = "'.implode(',', $productAttrVal).'".split(",");' : 'var productAttrVal = '.(int)$productAttrVal.';').'
			var productFamilly=new Array();
			product[0]=new Array();';
		$output .= myOwnReservationsProductsController::getUnitLabelJS($obj);
		foreach($obj->_products->list as $productFamily) $output .= '
			familyUnits['.$productFamily->sqlId.']='.$productFamily->_reservationUnit.';
			familyOptions['.$productFamily->sqlId.']=\''.implode(',', $productFamily->ids_options).'\';';
		foreach($products as $product) {
			$productFamilly = $obj->_products->getResProductFromProduct($product['id_product']);
			
			
				//with standard version we have a default product
				if ($productFamilly==null && array_key_exists(0, $obj->_products->list))
					$productFamilly=$obj->_products->list[0];
				$output .= '
					product[' . $product['id_product']. ']=new Array();';
				if ($productFamilly!=null) {
					$output .= '
							productFamilly[' . $product['id_product']. ']='.intval($productFamilly->sqlId).';';
					$attributes = self::getProductAttributes($product['id_product']);
					foreach($attributes as $attribute){
						if ($attribute>0) {
							$productAttr = self::getAttributeCombinaisons($product['id_product'],$attribute,$id_lang);
							$output .= '
							product['.$product['id_product'].']['.$attribute.']="'.addslashes(str_replace("<br />", " - ", $productAttr)).'";'; 
						}
					}
				}
			
		}
		$output .= '
			function populateAttribute(sel) {
				if (sel==null || typeof(sel)=="undefined") {
					if (typeof(productAttrVal)!="undefined") sel = productAttrVal;
					if (typeof(productAttrValArr)!="undefined") selArr = productAttrValArr;
				} else {
					productAttrVal = sel;
					productAttrValArr = Array(sel);
				}
				prods = $("#'.$productField.'");
				if (prods!=null) {
					var attributes = product[prods.val()];
					if(typeof(attributes)!="undefined" && attributes.length>0) {
						$("#'.$productAttrField.'").css("display","block");
						'.(is_array($productAttrVal) ? '$("#'.$productAttrField.'").attr("multiple", true);' : '$("#'.$productAttrField.'").removeAttr("multiple");').'
						'.(($allSelect || is_array($productFilter)) && !is_array($productAttrVal) ? 'var contents="<option value=\"0\">' . $obj->l('All', 'utils') . '</option>";' : 'var contents="";').'
						for (i = 0; i < attributes.length; i++) {
							if (attributes[i]!=undefined && attributes[i].length>0) {
								contents+="<option value=\""+i+"\"";
								if ((sel>0 && sel==i) || (typeof(selArr)!="undefined" && selArr.indexOf(i.toString()) !== -1)) contents+=" selected";
								contents+=">"+attributes[i]+"</option>";
							}
						}
						$("#'.$productAttrField.'").html(contents);
					} else {
						$("#'.$productAttrField.'").css("display","none");
						$("#'.$productAttrField.'").html("");
						$("#'.$productAttrField.'Span").css("display","none");
					}
				} else {
					$("#'.$productAttrField.'").css("display","none");
					$("#'.$productAttrField.'").html("");
					$("#'.$productAttrField.'Span").css("display","none");
				}
				'.$action.'
			}
			function getProductLengthUnit(selProduct) {
				if (typeof(idFamily)=="undefined") idFamily=$("#pricesruleFamily").val();
					if (typeof(idFamily)=="undefined") var idFamily=0;
					if (idFamily>0)
						var resvUnit = familyUnits[idFamily];
					else {
						var selFamily=productFamilly[selProduct];
						var resvUnit = familyUnits[selFamily];
					}
					return getUnitLabel(resvUnit);
			}
			function populateFamily() {
				if (typeof(populateMainFamily)!="undefined") populateMainFamily();
				var id_product = $("#'.$productField.'").val();
				var selFamily = productFamilly[id_product];
				var options = familyOptions[selFamily];
				optionsTab = (""+options).split(",");
				for(var i in optionsTab) {
					var seloption = optionsTab[i];
					$(".obj_sel").hide();
					$("#obj_sel_"+seloption).show();
				}
			}
			function populateUnits() {
				/*var id_product = $("#'.$productField.'").val();
				unitstr = getProductLengthUnit(id_product);
				$("'.$unitSelector.'").html(unitstr);
				if ($("'.$unitSelector.'").parent().hasClass("input-group-addon")) {
					if (unitstr=="") $("'.$unitSelector.'").parent().hide().prev().css("border-right-radius", "3px");
					if (unitstr=="") $("'.$unitSelector.'").parent().prev().css("border-right-radius", "3px");
					if (unitstr!="") $("'.$unitSelector.'").parent().show();
				}*/
			}
			$(document).ready(function() {
				populateUnits();
				'.($productAttrVal==0 && !$allSelect ? 'populateAttribute(0);' : '').'
				'.(count($productVal)==1 ? 'populateAttribute('.intval($productAttrVal).');' : '').'
				$("select.mychosen").each(function(k, item){
					if (typeof($(item).chosen)!=undefined) $(item).chosen({disable_search_threshold: 5, search_contains: true, width: "100%"});
					if ($("#scheduleStart").length) $("#scheduleStart").css("z-index", 0);
					if ($("#scheduleStart").length) $("#scheduleEnd").css("z-index", 0);
				});

			});
		</script>';
		if ($productFilter == 0 || is_array($productFilter)) {
			$output .= '
			<div id="prodcont" style="width:100%">
			<select class="upsInput mychosen" id="'.$productField.'" '.(is_array($productVal) || $multiple ? 'multiple' : '').' name="'.$productField.($multiple ? '[]' : '').'" style="'.$style.'" onchange="populateAttribute();populateUnits();populateFamily();'.$action.'" '.($productFilter>0 && !is_array($productFilter) ? 'disabled' : '').' >
				'.($allSelect ? '<option value="0" '.($productVal==array() ? 'selected' : '').'>' .$obj->l('All', 'utils') . '</option>' : '');
				
			foreach($obj->_products->list as $productFamily) {
				$unit=$productFamily->getUnitLabel($obj, $id_lang, true);
				if (($productField!='stock_id_product' or $productFamily->productType==product_type::STOCK)
					&& ($productField!='timeSlotProduct' or $productFamily->reservationSelType==reservation_interval::TIMESLOT)
					&& ((int)Tools::getValue('productFamilyFilter', 0)==0 or (int)Tools::getValue('productFamilyFilter', 0)==(int)$productFamily->sqlId) ) {
					$products = $productFamily->getProductsId();
					$prodcount=0;
					foreach($products as $id_product)
						if (!is_array($productFilter) || $productFilter == 0 || $productFilter==array() || in_array($id_product, $productFilter))
							$prodcount++;
					if ($productFamily->sqlId>0 && $prodcount) 
						$output .= '
				<optgroup label="'.(trim($productFamily->name)!='' ? $productFamily->name : ucfirst(myOwnLang::$object[MYOWN_OBJ::FAMILY]).' #'.$productFamily->sqlId).(trim($unit)!='' ? ' (/'.$unit.')' : '').'">';
					foreach($products as $id_product) {
						if (!is_array($productFilter) || $productFilter == 0 || $productFilter==array() || in_array($id_product, $productFilter)) {
							$name=self::getProductName($id_product, $id_lang);
							$selected=(is_array($productVal) ? in_array($id_product, $productVal) : $id_product==$productVal);
							$output .= '<option value="' . $id_product. '" '.($selected ? 'selected' : '').'>' . $name . '</option>';
						}
					}
				
				$output .= '
			</optgroup>';
				}
			}
/*
			$output .= '
			<select class="upsInput" id="'.$productField.'" name="'.$productField.'" style="'.$style.'" onchange="populateAttribute();populateUnits();" '.($productFilter>0 ? 'disabled' : '').' >
				'.($allSelect ? '<option value="0">' .$obj->l('All', 'utils') . '</option>' : '');
			foreach($products as $product){
				$output .= '<option value="' . $product['id_product']. '" '.($product['id_product']==$productVal ? 'selected' : '').'>' . $product['name'] . '</option>';
			}
	*/
			$output .= '
			</select></div>
			<select onchange="'.$action.'" class="upsInput" id="'.$productAttrField.'" name="'.$productAttrField.(is_array($productAttrVal) ? '[]' : '').'" style="'.$style.';display:none"></select>';
		} else {
			$output .= '
			<input type="hidden" id="'.$productField.'" name="'.$productField.'" value="'.$productFilter.'">
			<select onchange="'.$action.'" class="upsInput" id="'.$productAttrField.'" name="'.$productAttrField.(is_array($productAttrVal) ? '[]' : '').'" style="'.$style.';display:none"></select>';
		}
		return $output;
	}
	
	public static function insertTimeslotPickerScript($obj, $fieldHead = 'holiday', $productFamilyId = 0, $all = '') {
		global $cookie;
		$output = '';
		$productFamillies=array();
		if ($productFamilyId == 0) $productFamillies = $obj->_products->list;
		else $productFamillies[$productFamilyId]=$obj->_products->list[$productFamilyId];
		$products=$obj->_products->getProducts($cookie->id_lang);

		$output .= '
		<script type="text/javascript">
			var famillyTimeslots=new Array();';
			foreach($productFamillies as $productFamily){
				$output .= '
				famillyTimeslots[' . (int)$productFamily->sqlId. ']=new Array();';
				foreach($productFamily->_timeslotsObj->list as $timeslot){
					if ($timeslot->sqlId!=0) $output .= '
					var tempTimeSlot=new Array();
					tempTimeSlot[\'startTime\'] = "'.$timeslot->startTime.'";
					tempTimeSlot[\'endTime\'] = "'.$timeslot->endTime.'";
					tempTimeSlot[\'type\'] = "'.$timeslot->type.'";
					tempTimeSlot[\'days\'] = "'.$timeslot->getAvailableDaysStr().'".split(";");
					tempTimeSlot[\'name\'] = "'.addslashes ($timeslot->name).'";
					tempTimeSlot[\'id\'] = "'.$timeslot->sqlId.'";
					tempTimeSlot[\'object\'] = "'.$timeslot->id_object.'";
					famillyTimeslots['.$productFamily->sqlId.'].push(tempTimeSlot);';
				}
			}
			$output .= '
			var productFamilly=new Array();';
			foreach($products as $product) {
				$productFamilly = $obj->_products->getResProductFromProduct($product['id_product']);
				if ($productFamilly!=null) {
					$output .= '
							productFamilly[' . $product['id_product']. ']='.intval($productFamilly->sqlId).';';
				}
			}
			$output .= '
			function populateTimeslots() {
				populateTimeslot("start");
				populateTimeslot("end");
			}
			function populateObject(type) {
				if (type=="start")
					var tsfield = $("#'.$fieldHead.'StartTimeslot");
				else var tsfield = $("#'.$fieldHead.'EndTimeslot");
				var ts_id = tsfield.val();


				';
				if ($productFamilyId == 0) $output .= '
					var id_product=$("#'.$fieldHead.'Product").val();
					if ($("#'.$fieldHead.'Family").length) 
						var family = $("#'.$fieldHead.'Family").val();
					else if (id_product)
						var family = productFamilly[id_product];
					if (id_product==0 && (0 in famillyTimeslots))
						family=0;
					if (family!=undefined) 
						var timeslots = famillyTimeslots[family];
					else var timeslots = new Array();
					';
				else $output .= '
					var timeslots = famillyTimeslots['.$productFamilyId.'];';
				$output .= '
				var ts_obj = 0;
				for (t in timeslots) {
					if (timeslots[t][\'id\']==ts_id)
						ts_obj=timeslots[t][\'object\'];
				}
				$("#myownr_object").val(ts_obj);
				populateTimeslots();
			}
			function populateTimeslot(type, forcedfamily) {
				';
			if ($productFamilyId == 0) $output .= '
				var id_product=$("#'.$fieldHead.'Product").val();
				if ($("#'.$fieldHead.'Family").length) 
					var family = $("#'.$fieldHead.'Family").val();
				else if (id_product)
					var family = productFamilly[id_product];
				else if (typeof(forcedfamily)!="undefined" && forcedfamily!=0)
					var family = forcedfamily;
				if (id_product==0 && (0 in famillyTimeslots))
					family=0;
				if (family!=undefined) 
					var timeslots = famillyTimeslots[family];
				else var timeslots = new Array();
					
				';
			else $output .= '
				var timeslots = famillyTimeslots['.$productFamilyId.'];';

				$output .= '
				var id_object=0;
				if ($("#myownr_object").length)
					id_object=$("#myownr_object").val();
					
				if (type=="start") {
					var tsfield = $("#'.$fieldHead.'StartTimeslot");
					var datevalue = $("#'.$fieldHead.'Start").val();
					var typeInt=1;
				} else {
					var tsfield = $("#'.$fieldHead.'EndTimeslot");
					var datevalue = $("#'.$fieldHead.'End").val();
					var typeInt=2;
				}
				if (datevalue!=null) {
					var dateTab=datevalue.split("-");
					var dateday=-1;
					if (dateTab.length>2) {
						var datesel = new Date(dateTab[0], dateTab[1]-1, dateTab[2]);
						dateday=datesel.getDay();
						if (dateday==0) dateday=7;
					}
					if (type=="end") typeInt=2;
					var filteredTimeslots = new Array();
					if (timeslots !=undefined)
					for (i = 0; i < timeslots.length; i++) {
						if (timeslots[i]!=undefined
							&& (timeslots[i][\'type\']==typeInt || timeslots[i][\'type\']==0)
							&& (id_object==0 || timeslots[i][\'object\']==id_object || timeslots[i][\'object\']==0)
							&& (dateday==-1 || timeslots[i][\'days\'][dateday]=="1" )	) {
							filteredTimeslots[i]=timeslots[i];
						}
					}
					if(filteredTimeslots.length>0) {
						tsfield.css("display","inline-block");';
						if ($all!='') $output .= '
						if (filteredTimeslots.length>1) var contents="<option value=\"0\">' . $all . '</option>";';
						else $output .= '
						var contents="";';
						$output .= '
						for (i = 0; i < filteredTimeslots.length; i++) {
							if (filteredTimeslots[i]!=undefined) {
								if (type=="start") var hourDisplay = filteredTimeslots[i][\'startTime\'];
								else var hourDisplay = filteredTimeslots[i][\'endTime\']
								contents+="<option value=\""+filteredTimeslots[i][\'id\']+"\">"+filteredTimeslots[i][\'name\']+" "+hourDisplay+"</option>";
							}
						}
						tsfield.html(contents);
					} else {
						tsfield.css("display","none");
						tsfield.html("<option value=\"0\">0</option>");
					}
				}
			}
			$(document).ready(function() {
				populateTimeslot("start");
				populateTimeslot("end");
			});
			//# sourceURL=populateTimeslot.js
		</script>';
			return $output;
	}
	

	public static function validateOrderExtraVars($id_cart, $extraVars = array())
	{
		global $cart;
		$context = Context::getContext();
		//need to set cart to contect if not Product get Price static will fail on getting products..
		if (!is_object($cart) || $cart->id!=$id_cart) {
			$cart=new Cart($id_cart);
			$context->cart = $cart;
		}
		$obj = Module::getInstanceByName('myownreservations');
		myOwnReservationsController::_construire($obj);
		$displayPrice =intval((Configuration::get('MYOWNRES_PRICE_TYPE')!=reservation_price_display::HIDE));
		$products = $cart->getProducts();
		$id_currency = $cart->id_currency;
		$currency = new Currency($id_currency);
		$id_lang = $cart->id_lang;
		$productsList = '';
		$count=0;
		foreach ($products AS $key => $product)
		{
			$resas = myOwnCarts::getProducts($cart->id, $obj->_products, $obj->_pricerules, $product['id_product'], $product['id_product_attribute']);
			if (count($resas)>0) {
				foreach ($resas AS $resa) {
					$count++;
					$resaUnitPrice = $resa->getUnitPriceWithReduc(false);
					$resaUnitPrice_wt = $resa->getUnitPriceWithReduc(true);
					$resaPrice_wt = $resa->getPriceTaxes(true);
					$resaPrice = $resa->getPriceTaxes(false);
					PrestaShopLogger::addLog(date('Y-m-d H:i:s').' laBulle myOwnReservations validateOrderExtraVars '.$resaPrice_wt.' #'.$resa->sqlId.' '.$resaUnitPrice_wt);
					$resaStr = $resa->toString($obj, $id_lang, false);
					$discount = $resa->reductionsToString(Product::getTaxCalculationMethod() == PS_TAX_EXC);
					$resaStr .= ($discount!='' ? '<br/>'.(stripos(',', $discount)!==false ? $obj->l('Discounts', 'cart') : $obj->l('Discount', 'cart')).' : '.$discount : '');
					foreach ($obj->_extensions->getExtByType(extension_type::CARTDETAILS) as $ext) 
						if (method_exists($ext, "displayCartDetails")) {
							$ext_content = $ext->displayCartDetails($obj, $resa, -1, 'reservation');
							if ($ext_content!='') $resaStr.='<br/>'.$ext_content;
						}
					$productsList .=
					'<tr style="background-color: '.($count % 2 ? '#DDE2E6' : '#EBECEE').';">
						<td style="padding: 0.6em 0.4em;">'.$product['reference'].'</td>
						<td style="padding: 0.6em 0.4em;"><strong>'.$product['name'].(isset($product['attributes_small']) ? ' '.$product['attributes_small'] : '').'</strong><br />'.$resaStr.'</td>
						<td style="padding: 0.6em 0.4em; text-align: right;">'.Tools::displayPrice(Product::getTaxCalculationMethod() == PS_TAX_EXC ? $resaUnitPrice*$displayPrice : $resaUnitPrice_wt*$displayPrice, $currency, false).'</td>
						<td style="padding: 0.6em 0.4em; text-align: center;">'.$resa->quantity.'</td>
						<td style="padding: 0.6em 0.4em; text-align: right;">'.Tools::displayPrice(Product::getTaxCalculationMethod() == PS_TAX_EXC ? $resaPrice*$displayPrice : $resaPrice_wt*$displayPrice, $currency, false).'</td>
					</tr>';
				}
			} else {
				$count++;
				$productsList .=
				'<tr style="background-color: '.($count % 2 ? '#DDE2E6' : '#EBECEE').';">
					<td style="padding: 0.6em 0.4em;">'.$product['reference'].'</td>
					<td style="padding: 0.6em 0.4em;"><strong>'.$product['name'].(isset($product['attributes_small']) ? ' '.$product['attributes_small'] : '').'</strong></td>
					<td style="padding: 0.6em 0.4em; text-align: right;">'.Tools::displayPrice(Product::getTaxCalculationMethod() == PS_TAX_EXC ? $product['price'] : $product['price_wt'], $currency, false).'</td>
					<td style="padding: 0.6em 0.4em; text-align: center;">'.$product['cart_quantity'].'</td>
					<td style="padding: 0.6em 0.4em; text-align: right;">'.Tools::displayPrice(Product::getTaxCalculationMethod() == PS_TAX_EXC ? $product['total'] : $product['total_wt'], $currency, false).'</td>
				</tr>';
			}
		}
		$summary = $cart->getSummaryDetails();
		$myExtraVars = array('{products}' => $productsList, 
							'{total_products}' => Tools::displayPrice($cart->getOrderTotal(true, Cart::ONLY_PRODUCTS), $currency, false), 
							'{total_order}' => Tools::displayPrice($cart->getOrderTotal(true, Cart::BOTH)+$summary['balance_reservations_wt'], $currency, false), 
							'{total_balance}' => Tools::displayPrice($summary['balance_reservations_wt'], $currency, false)); 
		return array_merge($extraVars, $myExtraVars);
	}
	
	public static function getOrderTotalReservations($id_order, $taxes=true) {
		$total=0;
		$resas = myOwnResas::getReservations($id_order);
		foreach($resas as $resa) {
			$total+=$resa->getTotalPriceSaved($taxes);
		}
		return $total;
	}
	
	public static function getCustomizations($id_cart, $id_product, $id_product_attribute)
	{
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('SELECT `id_customization`, `quantity` FROM `'._DB_PREFIX_.'customization` WHERE `id_cart` = '.(int)($id_cart).' AND `id_product` = '.(int)($id_product).' AND `id_product_attribute` = '.(int)($id_product_attribute));
		$customizations = array();

		foreach ($result AS $row)
			$customizations[(int)($row['id_customization'])] = $row;
		return $customizations;
	}
	
	public static function getProductName($id_product, $id_lang, $id_shop=0) {
		if ($id_shop==0) $id_shop = myOwnUtils::getShop();
		$query = '
		SELECT pl.`name` AS name
		FROM `'._DB_PREFIX_.'product_lang` pl';
		if ($id_shop>0) $query .= '
	    INNER JOIN `'._DB_PREFIX_.'product_shop` ps ON ps.`id_product` = pl.`id_product` AND pl.`id_shop` = ps.`id_shop`';
		$query .= '
		WHERE pl.`id_product` = '.intval($id_product).' AND pl.`id_lang` = '.intval($id_lang);
		if ($id_shop>0) $query .= ' AND ps.`id_shop`='.$id_shop;
		$p = Db::getInstance()->ExecuteS($query);
		if (count($p)>0) return $p[0]['name'];
		else return "";
	}
	
	public static function getProductsIdFromCategory($category)  {
		$query = '
		SELECT id_product 
		FROM `'._DB_PREFIX_.'category_product`
		WHERE `id_category` = '.$category.';';

		$products = Db::getInstance()->ExecuteS($query);
		$productIds=array();
		foreach ($products as $product) 
			$productIds[(int)$product['id_product']]=(int)$product['id_product'];
		return $productIds;
	}
	
	public static function getProductsIdFromCategoryAndSub($category)  {
		$categories = self::getAllSubCategoriesId($category);
    	$categoriesStr=implode(',',$categories);
		$query = '
		SELECT id_product
		FROM `'._DB_PREFIX_.'category_product`
		WHERE `id_category` in ('.$categoriesStr.');';

		$products = Db::getInstance()->ExecuteS($query);
		$productIds=array();
		foreach ($products as $product) 
			$productIds[(int)$product['id_product']]=(int)$product['id_product'];
		return $productIds;
	}
	
	public static function getAllSubCategoriesId($id, $categories=null, $cnt=0, $subCategories=array()) {
		if ($categories==null) {
			$query="SELECT id_category, id_parent FROM `"._DB_PREFIX_."category` WHERE active = 1;";
			$categories = Db::getInstance()->ExecuteS($query);
		}
		$cnt++;
		if ($subCategories==array()) $subCategories[$id]=$id;
		
		foreach($categories as $category)
			if (intval($category['id_parent'])==$id && !in_array($category['id_category'], $subCategories)) {
				$subCategories += self::getAllSubCategoriesId((int)$category['id_category'],$categories, $cnt, $subCategories);
			}

		return $subCategories;
	}
	

	public static function getProductPrice($id_product, $id_product_attribute, $sell=false) {
		return self::getProductPriceTaxes($id_product, $id_product_attribute, (Product::getTaxCalculationMethod() != PS_TAX_EXC), $sell);
	}
	
	public static function getProductPriceTaxes($id_product, $id_product_attribute, $taxes, $sell=false, $qty=1) {
		global $cart;
		if ($cart!=null) {
			$id_cart=(int)$cart->id;
			$id_customer=$cart->id_customer;
			$id_address_delivery=$cart->id_address_delivery;
		} else {
			$id_cart=0;
			$id_customer=0;
			$id_address_delivery=0;
		}

		$price = Product::getPriceStatic($id_product, $taxes, ((isset($id_product_attribute) AND !empty($id_product_attribute)) ? (int)$id_product_attribute : NULL), 6, null, false, true, $qty, false, $id_customer , $id_cart, $id_address_delivery, $null, true, true);
		
		if ($sell) {
			$ratio = self::getProductPriceRatio($id_product);
			if ($ratio>0) return ($price/$ratio);
			else return $price;
		} else return $price;
	}
	
	public static function getOldProductPriceTaxes($id_product, $id_product_attribute, $taxes, $sell=false, $qty=1) {
		global $cart;
		if ($cart!=null) {
			$id_cart=(int)$cart->id;
			$id_customer=$cart->id_customer;
			$id_address_delivery=$cart->id_address_delivery;
		} else {
			$id_cart=0;
			$id_customer=0;
			$id_address_delivery=0;
		}

		$price = Product::getPriceStatic($id_product, $taxes, ((isset($id_product_attribute) AND !empty($id_product_attribute)) ? (int)$id_product_attribute : NULL), 6, null, false, false, $qty, false, $id_customer , $id_cart, $id_address_delivery, $null, true, true);
		
		if ($sell) {
			$ratio = self::getProductPriceRatio($id_product);
			if ($ratio>0) return ($price/$ratio);
			else return $price;
		} else return $price;
	} 
	
	public static function getOrder($id_order) {
		$query = ' SELECT *  FROM  `'._DB_PREFIX_.'orders` WHERE  `id_order` ='.$id_order;
		return Db::getInstance()->ExecuteS($query);
	}
	
	public static function getOrderDetail($id_order, $id_product=0, $id_product_attribute=0) {
		$query = ' SELECT *  FROM  `'._DB_PREFIX_.'order_detail` WHERE `id_order` ='.(int)$id_order;
		if ($id_product!=0) $query .= ' AND  `product_id` ='.(int)$id_product;
		if ($id_product_attribute!=0) $query .= ' AND  `product_attribute_id` ='.(int)$id_product_attribute;
		return Db::getInstance()->ExecuteS($query);
	}
	
	public static function deleteOrderDetail($id_orderDetail) {
		$query = 'DELETE FROM `'._DB_PREFIX_.'order_detail` WHERE `id_order_detail` = '.$id_orderDetail;
		return Db::getInstance()->Execute($query);
	}
	
	public static function setOrderDetail($resa, $id_lang) {
		$name = self::getProductName($resa->id_product, $id_lang);
		if ($resa->id_product_attribute>0) $name .= " - ".str_replace("<br />", ", ", self::getAttributeCombinaisons($resa->id_product, $resa->id_product_attribute, $id_lang));
		$qty = self::getProductAttrQuantity($resa->id_product, $resa->id_product_attribute);
		$taxrate=Tools::ps_round($resa->tax_rate,_PS_PRICE_DISPLAY_PRECISION_);
		$taxid= Tax::getTaxIdByRate($taxrate);
		$tax=new Tax($taxid);
		$query = "
		INSERT INTO  `" . _DB_PREFIX_ . "order_detail` ( `id_order` ,  `product_id` ,  `product_attribute_id` ,  `product_name` ,  `product_quantity` ,  `product_quantity_in_stock` ,  `product_price` ,  `tax_name` ,  `tax_rate`) 
		VALUES (".$resa->id_order.", ".$resa->id_product.", ".$resa->id_product_attribute.",  '".$name."', ".$resa->quantity.", ".$qty.", '".$resa->getPriceTaxes(false)."', '".$tax->name[$id_lang]."',  '".$taxrate."') ;";
		return Db::getInstance()->ExecuteS($query);
	}
	
	public static function updateCartDetail($obj, $resa, $reScheduleQty, $startDate, $startTimeslot, $endDate, $endTimeslot) {
	 	if ($resa->id_order>0) {
 			$order = new Order($resa->id_order);
 			$id_cart = $order->id_cart;
 			//check resa cart item
 			$cart_resas = myOwnCarts::getProducts($id_cart, $obj->_products, $obj->_pricerules, $resa->id_product, $resa->id_product_attribute);
 		
 			foreach ($cart_resas as $cart_resa) {
 				if ($cart_resa->startDate == $resa->startDate
 					&& $cart_resa->startTimeslot == $resa->startTimeslot
 					&& $cart_resa->endDate == $resa->endDate
	 				&& $cart_resa->endTimeslot == $resa->endTimeslot) {
	 				$cart_resa->quantity += $reScheduleQty;
		 			$cart_resa->startDate = $startDate;
			 		$cart_resa->startTimeslot = $startTimeslot;
			 		$cart_resa->endDate = $endDate;
			 		$cart_resa->endTimeslot = $endTimeslot;
			 		
			 		$id = $cart_resa->sqlId;
			 		$cart_resa->sqlUpdate();
		 		}
 			}
 			PrestaShopLogger::addLog(date('Y-m-d H:i:s').'laBulle myOwnReservations Reschedule : updateCartDetail '.count($cart_resas));
 			if (count($cart_resas)==0) {
	 			$new_cart_resa = new myOwnCart();
	 			$new_cart_resa->_mainProduct = $resa->_mainProduct;
	 			$new_cart_resa->id_cart = $id_cart;
	 			$new_cart_resa->id_object = $resa->id_object;
	 			$new_cart_resa->id_product = $resa->id_product;
	 			$new_cart_resa->id_product_attribute = $resa->id_product_attribute;
	 			$new_cart_resa->quantity = $resa->quantity;
	 			$new_cart_resa->startDate = $_POST["startDate"];
		 		$new_cart_resa->startTimeslot = $_POST["startTimeslot"];
		 		$new_cart_resa->endDate = $_POST["endDate"];
		 		$new_cart_resa->endTimeslot = $_POST["endTimeslot"];
		 		if ($tempCart->_mainProduct!=null)
			 		$tempCart->setPrice($obj->_pricerules->list);
			 	$id = $new_cart_resa->sqlInsert();
			 	
			 	//check base cart item
			 	$req = "SELECT * FROM `"._DB_PREFIX_."cart_product` WHERE `id_cart` = '".$id_cart."' AND `id_product` = ".$resa->id_product.";";
			 	if ($resa->id_product_attribute) $req .= " AND `id_product_attribute` = ".$resa->id_product_attribute.";";
			 	$cart_items = Db::getInstance()->ExecuteS($req);
			 	if (count($cart_items)==0) {
			 		$id_shop = myOwnUtils::getShop();
			 		$req = "INSERT INTO `"._DB_PREFIX_."cart_product` (`id_cart`, `id_product`, `id_address_delivery`, `id_shop`, `id_product_attribute`, `quantity`, `date_add`) VALUES ('".$id_cart."', '".$resa->id_product."', '0', '".$id_shop."', '".$resa->id_product_attribute."', '".$resa->quantity."', '".date('Y-m-d H:i:s')."');";
			 		Db::getInstance()->Execute($req);
				}
 			}
 		}
 		return $id;
	}
	
	public static function updateOrderDetail($obj, $resa, $id_lang, $updateOrder=true, $updateInvoice=0, $oldResa=null, $addResa=0) {
		$query='';
		$id_order = $resa->id_order;
		$id_product = $resa->id_product;
		$id_product_attribute = $resa->id_product_attribute;
		//get order details
		$existingOrderDetails = self::getOrderDetail($id_order, $id_product, $id_product_attribute);
		$total_orderDetail_wt = 0;
		$total_orderDetail_ht = 0;
		
		if (count($existingOrderDetails)>0) {
			$existingOrderDetail = $existingOrderDetails[0];
			$id_orderDetail = $existingOrderDetail['id_order_detail'];
			if (_PS_VERSION_ >= "1.5.0.0") {
				$total_orderDetail_wt += $existingOrderDetail['total_price_tax_incl'];
				$total_orderDetail_ht += $existingOrderDetail['total_price_tax_excl'];
			} else if ($oldResa!=null) {
				$total_orderDetail_wt += $oldResa->getTotalPriceSaved(true);
				$total_orderDetail_ht += $oldResa->getTotalPriceSaved(false);
			}
		} else $id_orderDetail = 0;
		PrestaShopLogger::addLog(date('Y-m-d H:i:s').'laBulle myOwnReservations Reschedule : OrderDetail '.$id_orderDetail.' : '.$total_orderDetail_wt);

		//reservation totals
		$total_reservations_wt = 0;
		$total_reservations_ht = 0;
		$total_advance_wt = 0;
		$total_advance_ht = 0;
		$total_quantity = 0;
		$reservations = myOwnResas::getReservations($id_order, $id_product, $id_product_attribute);
		$mainProduct = $obj->_products->getResProductFromProduct($id_product);
		foreach ($reservations as $reservation) {
			$reservation->_mainProduct = $mainProduct;
			$advance = ($mainProduct->_advanceRate/100);
			$reservation->setPrice($obj->_pricerules->list);
			if ($resa->sqlId == $reservation->sqlId) { // || $addResa == $reservation->sqlId
	
				$resa->setPrice($obj->_pricerules->list);
				
				$submited_price = Tools::getValue('resa_'.$resa->sqlId.'_price', 0);
				if ($submited_price>0) {
					$tax_rate = ($resa->getPriceTaxes(false)/$resa->getPriceTaxes(true));
					
					$total_reservations_wt += $submited_price;
					$total_reservations_ht += $submited_price * $tax_rate;
					
					$total_advance_wt += $submited_price * $advance;
					$total_advance_ht += $submited_price * $tax_rate * $advance;
				} else {
					$total_reservations_wt += $resa->getPriceTaxes(true);
					$total_reservations_ht += $resa->getPriceTaxes(false);
					
					$total_advance_wt += $resa->getPriceTaxes(true) * $advance;
					$total_advance_ht += $resa->getPriceTaxes(false) * $advance;
				}
				$total_quantity += $resa->quantity;
				PrestaShopLogger::addLog(date('Y-m-d H:i:s').'laBulle myOwnReservations Reschedule : isresa +'.$resa->quantity.' total_quantity:'.$total_quantity);
			} else {

				$total_reservations_wt += $reservation->getPriceTaxes(true);
				$total_reservations_ht += $reservation->getPriceTaxes(false);
				$total_advance_wt += $reservation->getPriceTaxes(true) * $advance;
				$total_advance_ht += $reservation->getPriceTaxes(false) * $advance;
				$total_quantity += $reservation->quantity;
				PrestaShopLogger::addLog(date('Y-m-d H:i:s').'laBulle myOwnReservations Reschedule +'.$reservation->quantity.'  : total_quantity:'.$total_quantity);
			}
		}
		PrestaShopLogger::addLog(date('Y-m-d H:i:s').'laBulle myOwnReservations Reschedule : Reservations cnt:'.count($reservations).' : '.$total_reservations_wt);

		$diff_wt = $total_reservations_wt-$total_orderDetail_wt;
		$diff_ht = $total_reservations_ht-$total_orderDetail_ht;

		if ($total_reservations_wt==0 && $id_orderDetail>0) {
			self::deleteOrderDetail($id_orderDetail);
		} else {			
			if ($id_orderDetail>0) {
				//updating product to order details
				if (_PS_VERSION_ < "1.5.0.0") {
					$query = "UPDATE `"._DB_PREFIX_."order_detail` SET `product_quantity` =  '".$total_quantity."' WHERE  `id_order_detail` =".$id_orderDetail.";";
				} else if ($diff_wt!=0 || $updateInvoice) {
					$query = "UPDATE `"._DB_PREFIX_."order_detail` SET `product_quantity` =  '".$total_quantity."', ".($updateInvoice ? "`id_order_invoice` =  '".$updateInvoice."'," : "")." `total_price_tax_incl` = '".$total_reservations_wt."', `total_price_tax_excl` = '".$total_reservations_ht."' WHERE  `id_order_detail` =".$id_orderDetail.";";
				}
				if ($query!='') Db::getInstance()->Execute($query);
			} else {
				//adding product to order details
				$product = new Product($id_product, true);
				$name = self::getProductName($id_product, $id_lang);
				if ($id_product_attribute>0) $name .= " - ".str_replace("<br />", ", ", self::getAttributeCombinaisons($id_product, $id_product_attribute, $id_lang));
				$productPrice_ht = self::getProductPriceTaxes($id_product, $id_product_attribute, false, $mainProduct->_optionAlsoSell);
				$productPrice_wt = self::getProductPriceTaxes($id_product, $id_product_attribute, true, $mainProduct->_optionAlsoSell);

				if (_PS_VERSION_ < "1.5.0.0") {
					//getting taxes
					$taxName = ""; $taxRate = 0; $product_tax=null;
					foreach (Tax::getTaxes($id_lang) as $taxItem)
						if ($taxItem->rate == $product->tax_rate)
							$product_tax = $taxItem;
					if ($product_tax!=null) {
						$taxName = $product_tax->name;
						$taxRate = $product_tax->rate;
					}
					$query = "INSERT INTO `"._DB_PREFIX_."order_detail` (`id_order`, `product_id`, `product_attribute_id`, `product_name`, `product_quantity`, `product_quantity_in_stock`, `product_price`, `tax_name`, `tax_rate`) 
				VALUES (".$id_order.", ".$id_product.", ".$id_product_attribute.",  '".$name."', ".$total_quantity.", ".$total_quantity.", '".$productPrice_ht."', '".$taxName."', '".$taxRate."') ;";
				} else {
					$query = "INSERT INTO `"._DB_PREFIX_."order_detail` (`id_order`, ".($updateInvoice ? "`id_order_invoice`," : "")." `id_shop`, `product_id`, `product_ean13`, `product_upc`, `product_reference`, `product_weight`, `product_attribute_id`, `product_name`, `product_quantity`, `product_quantity_in_stock`, `product_price`, `total_price_tax_incl`, `total_price_tax_excl`, `unit_price_tax_incl`, `unit_price_tax_excl`) 
				VALUES (".$id_order.", ".($updateInvoice ? "'".$updateInvoice."'," : "")." ".$resa->id_shop.", ".$id_product.", '".$product->ean13."', '".$product->upc."', '".$product->reference."', '".$product->weight."', ".$id_product_attribute.",  '".addslashes($name)."', ".$total_quantity.", ".$total_quantity.", '".$productPrice_ht."', '".$total_reservations_wt."', '".$total_reservations_ht."', '".$productPrice_wt."', '".$productPrice_ht."') ;";
				}
				Db::getInstance()->Execute($query);
			}
		}
		if ($updateOrder && $diff_wt) {
			$order = new Order($id_order);
			if (_PS_VERSION_ < "1.5.0.0") {
				$query = "UPDATE `"._DB_PREFIX_."orders` SET `total_products_wt` =  '".($order->total_products_wt+$diff_wt)."', `total_products` =  '".($order->total_products+$diff_ht)."', `total_paid` =  '".($order->total_paid+$diff_wt)."' WHERE `id_order` =".$order->id.";";
			} else {
				$query = "UPDATE `"._DB_PREFIX_."orders` SET `total_products_wt` =  '".($total_advance_wt)."', `total_products` =  '".($total_advance_ht)."', `total_paid` =  '".($order->total_paid+$diff_wt)."', `total_paid_tax_incl` =  '".($order->total_paid_tax_incl+$diff_wt)."', `total_paid_tax_excl` =  '".($order->total_paid_tax_excl+$diff_ht)."' WHERE `id_order` =".$order->id.";";
			}
			Db::getInstance()->Execute($query);
		}
		return array('diff_wt' => $diff_wt, 'diff_ht' => $diff_ht, 'total_advance_wt' => $total_advance_wt, 'total_advance_ht' => $total_advance_ht);
	}
	
	public static function getProductPriceRatio($id_product) {
		$query = 'SELECT * FROM  `'._DB_PREFIX_.'product` WHERE  `id_product` ='.$id_product;
		$p = Db::getInstance()->ExecuteS($query);
		if (count($p)>0) return $p[0]['unit_price_ratio'];
		else return 0;
	}
	
	public static function getDefaultAttribute($id_product) {
		$query = '
		SELECT pa.`id_product_attribute` AS pid
		FROM `'._DB_PREFIX_.'product_attribute` pa
		WHERE pa.`id_product` = '.intval($id_product).' 
		AND pa.`default_on` = 1';
		$p = Db::getInstance()->ExecuteS($query);
		if (count($p)>0) return $p[0]['pid'];
		else return 0;
	}
	
	public static function getProductAttributes($id_product) {
		$attributes = array();
		$query = '
		SELECT pa.`id_product_attribute` AS pid
		FROM `'._DB_PREFIX_.'product_attribute` pa
		WHERE pa.`id_product` = '.intval($id_product);
		$p = Db::getInstance()->ExecuteS($query);
		if (count($p)>0) {
			foreach ($p as $attr)
				$attributes[] = $attr['pid'];
		} else {
			$attributes[] = 0;
		}
		return $attributes;
	}

	
	public static function listPayments() {
		return Db::getInstance()->ExecuteS("SELECT * FROM `" . _DB_PREFIX_ . "carrier` WHERE `deleted` = 0");
	}
			
	public static function getPath($obj, $tpl)
	{
		if (Tools::file_exists_cache(_PS_THEME_DIR_.'modules/'.$obj->name.'/templates/'.str_replace("views/templates/front/planning/_partials/", '', $tpl)))
			return _PS_THEME_DIR_.'modules/'.$obj->name.'/templates/'.str_replace("views/templates/front/planning/_partials/", '', $tpl);
		else if (Tools::file_exists_cache(_PS_THEME_DIR_.'modules/'.$obj->name.'/'.$tpl))
			return _PS_THEME_DIR_.'modules/'.$obj->name.'/'.$tpl;
		else
		return _PS_MODULE_DIR_.$obj->name.'/'.$tpl;
	}
	
	public static function getAllProducts($id_lang, $full=true)  {
		$id_shop = myOwnUtils::getShop();
		
	    $query = '
	    SELECT '.($full ? '*' : 'pl.id_product, pl.name').'
	    FROM  `'._DB_PREFIX_.'product_lang` pl';
	    if ($id_shop>0) $query .= '
	    INNER JOIN `'._DB_PREFIX_.'product_shop` ps ON ps.`id_product` = pl.`id_product` AND pl.`id_shop` = ps.`id_shop`';
	    $query .= '
	    WHERE pl.`id_lang` = '.$id_lang;
	    if ($id_shop>0) $query .= ' AND ps.`id_shop`='.$id_shop;
		
		$results = Db::getInstance()->ExecuteS($query);
		foreach($results as $result)
			$products[intval($result["id_product"])] = $result; 
		return $products;
	}
	
	public static function getProductsImages($productsIds, $id_lang) {
		$productsIdList = implode(',',$productsIds);
		$productsImages=array();
		if (trim($productsIdList)!='') $productsImages = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT i.id_image, p.id_product, il.legend, p.active, pl.name, pl.description_short, pl.link_rewrite, cl.link_rewrite AS category_rewrite
			FROM '._DB_PREFIX_.'product p
			LEFT JOIN '._DB_PREFIX_.'product_lang pl ON (pl.id_product = p.id_product)
			LEFT JOIN '._DB_PREFIX_.'image i ON (i.id_product = p.id_product AND i.cover = 1)
			LEFT JOIN '._DB_PREFIX_.'image_lang il ON (il.id_image = i.id_image)
			LEFT JOIN '._DB_PREFIX_.'category_lang cl ON (cl.id_category = p.id_category_default)
			WHERE p.id_product IN ('.$productsIdList.')
			AND pl.id_lang = '.(int)($id_lang).'
			AND cl.id_lang = '.(int)($id_lang)
			);

		$productsViewedObj = array();
		
		$productsImagesArray = array();
			foreach ($productsImages AS $pi)
				$productsImagesArray[$pi['id_product']] = $pi;

		foreach ($productsIds AS $productViewed)
		{
			$obj = (object)'Product';
			if (!isset($productsImagesArray[$productViewed]) || (!$obj->active = $productsImagesArray[$productViewed]['active']))
				continue;
			else
			{
				$obj->id = (int)($productsImagesArray[$productViewed]['id_product']);
				$obj->cover = (int)($productsImagesArray[$productViewed]['id_product']).'-'.(int)($productsImagesArray[$productViewed]['id_image']);
				$obj->legend = $productsImagesArray[$productViewed]['legend'];
				$obj->name = $productsImagesArray[$productViewed]['name'];
				$obj->description_short = $productsImagesArray[$productViewed]['description_short'];
				$obj->link_rewrite = $productsImagesArray[$productViewed]['link_rewrite'];
				$obj->category_rewrite = $productsImagesArray[$productViewed]['category_rewrite'];

				if (!isset($obj->cover) || !$productsImagesArray[$productViewed]['id_image'])
				{
					$obj->cover = Language::getIsoById($id_lang).'-default';;
					$obj->legend = '';
				}
				$productsViewedObj[$obj->id] = $obj;
			}
		}
		return $productsViewedObj;
	}

	
	public static function getProductAttrQuantity($id_product, $id_product_attribute=0) {
		global $cookie;
		if (_PS_VERSION_ >= "1.5.0.0") {
			$id_shop = self::getShop();
			if ($id_shop==0) $id_shop=null;
			if (Pack::isPack($id_product) && Configuration::get('PS_PACK_STOCK_TYPE') == 1) {
				$packitems = Pack::getItems($id_product, $cookie->id_lang);
				$qty=0;
				foreach ($packitems as $packitem) {
					$tmp_qty = StockAvailable::getQuantityAvailableByProduct($packitem->id, $packitem->id_pack_product_attribute, $id_shop);
					if ($qty==0) $qty = floor($tmp_qty / $packitem->pack_quantity);
					else $qty = min ($qty, $tmp_qty);
				}
				return $qty;
			} else
				return StockAvailable::getQuantityAvailableByProduct($id_product, $id_product_attribute, $id_shop);
		}
		if ($id_product_attribute==0 ) {
			$query = '
			SELECT p.`quantity` AS qty
			FROM `'._DB_PREFIX_.'product` p
			WHERE p.`id_product` = '.intval($id_product);
			$p = Db::getInstance()->ExecuteS($query);
			if (count($p)>0) return $p[0]['qty'];
			else return 0;
		} else {
			$query = '
			SELECT pa.`quantity` AS qty
			FROM `'._DB_PREFIX_.'product_attribute` pa
			WHERE pa.`id_product` = '.intval($id_product).' AND pa.`id_product_attribute` = '.intval($id_product_attribute);
			$p = Db::getInstance()->ExecuteS($query);
			if (count($p)>0) return $p[0]['qty'];
			else return 0;
		}
	}
	
	public static function getCategoryQuantity($id_category) {
		if ($id_category > 0) {
			//sum of product without attr
			$query = '
			SELECT SUM(p.`quantity`) AS Q
			FROM `'._DB_PREFIX_.'category_product` cp
			LEFT JOIN `'._DB_PREFIX_.'product` p ON p.`id_product` = cp.`id_product`
			LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON pa.`id_product` = cp.`id_product`
			WHERE cp.`id_category` = '.intval($id_category).' AND pa.`id_product_attribute` IS NULL ';
			$p = Db::getInstance()->ExecuteS($query);
			if (count($p)>0) $q = $p[0]['Q'];
			else $q = 0;
			//sum of product with attr
			$query = '
			SELECT SUM(quantity) AS Q
			FROM `'._DB_PREFIX_.'product_attribute` pa
			LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON cp.`id_product` = pa.`id_product`
			WHERE cp.`id_category` = '.intval($id_category);
			$p = Db::getInstance()->ExecuteS($query);
			if (count($p)>0) $q += $p[0]['Q'];
			else $q += 0;
		} else {
			//sum of product without attr
			$query = '
			SELECT SUM(p.`quantity`) AS Q
			FROM `'._DB_PREFIX_.'product` p
			LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON pa.`id_product` = p.`id_product`
			WHERE pa.`id_product_attribute` IS NULL ';
			$p = Db::getInstance()->ExecuteS($query);
			if (count($p)>0) $q = $p[0]['Q'];
			else $q = 0;
			//sum of product with attr
			$query = '
			SELECT SUM(quantity) AS Q
			FROM `'._DB_PREFIX_.'product_attribute` pa';
			$p = Db::getInstance()->ExecuteS($query);
			if (count($p)>0) $q += $p[0]['Q'];
			else $q += 0;
		}
		return $q;
	}

	public static function getAttributeCombinaisons($id_product, $id_product_attribute, $id_lang, $inline=true)
	{
		$out = "";
		$query = '
		SELECT agl.`public_name` AS group_name, ag.id_attribute_group, al.`name` AS attribute_name, a.`id_attribute`
		FROM `'._DB_PREFIX_.'product_attribute` pa
		LEFT JOIN `'._DB_PREFIX_.'product_attribute_combination` pac ON pac.`id_product_attribute` = pa.`id_product_attribute`
		LEFT JOIN `'._DB_PREFIX_.'attribute` a ON a.`id_attribute` = pac.`id_attribute`
		LEFT JOIN `'._DB_PREFIX_.'attribute_group` ag ON ag.`id_attribute_group` = a.`id_attribute_group`
		LEFT JOIN `'._DB_PREFIX_.'attribute_lang` al ON (a.`id_attribute` = al.`id_attribute` AND al.`id_lang` = '.intval($id_lang).')
		LEFT JOIN `'._DB_PREFIX_.'attribute_group_lang` agl ON (ag.`id_attribute_group` = agl.`id_attribute_group` AND agl.`id_lang` = '.intval($id_lang).')
		WHERE pa.`id_product` = '.intval($id_product).' AND pa.`id_product_attribute` = '.intval($id_product_attribute).'
		ORDER BY pa.`id_product_attribute`';
		$attrs = Db::getInstance()->ExecuteS($query);
		
		if (!$inline) return $attrs;
		 
		foreach ($attrs as $attr) {
			if ($out!="") $out.='<br />';
			$out.=$attr['group_name'].' : '.$attr['attribute_name'];
		}
			
		return $out;
	}
	
	public static function getCategoryProducts($category_id, $id_lang)  {
	    if (intval($category_id)>0)
			$query = '
			SELECT *
			FROM `'._DB_PREFIX_.'category_product` cp
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON pl.`id_product` = cp.`id_product`
			WHERE cp.`id_category` ='.intval($category_id).' AND pl.`id_lang` = '.$id_lang;
		else	
		    $query = '
		    SELECT *
		    FROM  `'._DB_PREFIX_.'product_lang` pl 
		    WHERE pl.`id_lang` = '.$id_lang;
		return Db::getInstance()->ExecuteS($query);
	}
	
	
	public static function getCartSelection($id_cart){
		$selections=Db::getInstance()->ExecuteS("SELECT * FROM `" . _DB_PREFIX_ . "myownreservations_cart` WHERE `id_cart`=".$id_cart);
		if (count($selections)==0) {
			return "";
		} else {
			$selection=$selections[0];
			return $selection['selection'];
		}
	}
	
	public static function setCartSelection($id_cart, $selection) {
		$selections=Db::getInstance()->ExecuteS("SELECT * FROM `" . _DB_PREFIX_ . "myownreservations_cart` WHERE `id_cart`=".$id_cart);
		if (count($selections)==0) {
 			$req="INSERT INTO  `" . _DB_PREFIX_ ."myownreservations_cart` (`id_cart`, `selection`) VALUES (".$id_cart.", '".$selection."');";
		} else {
			$req = "UPDATE `" . _DB_PREFIX_ ."myownreservations_cart` SET  `selection` =  '".$selection."' WHERE `id_cart` =". $id_cart .";";
		}
 		return Db::getInstance()->Execute($req);
	}
	
	public static function sendMsg($obj, $reservation, $start_date, $start_timselot, $end_date, $end_timselot, $employee) {
  		$order = new Order(intval($reservation->id_order));

   	    if (Validate::isLoadedObject($order))
  		{
  			$customer = new Customer($order->id_customer);
  			if (Validate::isLoadedObject($customer)) {
  			    $msg = htmlentities(myOwnLang::getMessage('NOTIFY_RES',$order->id_lang));
  			    $order_name='';
  			    if (_PS_VERSION_ >= '1.5.0.0')
                	$order_name = $order->getUniqReference();

  			    $plbla="";
     			$plbl = self::getProductName($reservation->id_product, $order->id_lang);
     			$plbla = self::getAttributeCombinaisons($reservation->id_product, $reservation->id_product_attribute, $order->id_lang);
     			if ($plbla!="") $plbl .= ' '.$plbla;
     			$msg = str_replace('%product%', $plbl, $msg);
     			if ($start_timselot != null && $start_timselot->sqlId > 0) $startts=' '.MyOwnCalendarTool::formatTime($start_timselot->startTime,$order->id_lang); else $startts='';
     			$msg = str_replace('%start%', myOwnCalendar::formatDateWithDay($start_date,$order->id_lang).$startts, $msg);
     			if ($end_timselot != null && $end_timselot->sqlId > 0) $endts=' '.MyOwnCalendarTool::formatTime($end_timselot->endTime,$order->id_lang); else $endts='';
     			$msg = str_replace('%end%', MyOwnCalendarTool::formatDateWithDay($end_date,$order->id_lang).$endts, $msg);
				if (trim($msg)!="") {
      			    $message = new Message();
              		$message->id_employee = $employee; //intval($cookie->id_employee);
              		$message->message = $msg;
              		$message->id_order = intval($reservation->id_order);
              		$message->private = false;
              		if (!$message->add())
              			return false;
       			
      				$varsTpl = array('{lastname}' => $customer->lastname, '{firstname}' => $customer->firstname, '{order_name}' => $order_name, '{id_order}' => $message->id_order, '{message}' => (Configuration::get('PS_MAIL_TYPE') == 2 ? $message->message : str_replace(array("\r\n", "\r", "\n"), '<br />', $message->message)));
      				if (!Mail::Send(intval($order->id_lang), 'order_merchant_comment', "reservation", $varsTpl, $customer->email, $customer->firstname.' '.$customer->lastname))
      					return false;
  				}
  			}
  		}
		return true;		
	}
	
	public static function sendProdMsg($obj, $reservation, $start_date, $start_timselot, $end_date, $end_timselot, $employee, $productFamilly, $reschedule) {
  		$order = new Order(intval($reservation->id_order));

   	    if (Validate::isLoadedObject($order))
  		{
  			$customer = new Customer($order->id_customer);
  			if (Validate::isLoadedObject($customer)) {
  			    $msg = htmlentities(myOwnLang::getMessage('NOTIFY_PROD',$order->id_lang));
  			    $order_name='';
  			    if (_PS_VERSION_ >= '1.5.0.0')
                	$order_name = $order->getUniqReference();
  			    $plbla="";
     			$plbl = self::getProductName($reservation->id_product, $order->id_lang);
     			$plbla = self::getAttributeCombinaisons($reservation->id_product, $reservation->id_product_attribute, $order->id_lang);
     			if ($plbla!="") $plbl .= ' '.$plbla;
     			$msg = str_replace('%product%', $plbl, $msg);
     			if ($start_timselot != null && $start_timselot->sqlId > 0) $startts=' '.MyOwnCalendarTool::formatTime($start_timselot->startTime,$order->id_lang);else $startts='';
     			$msg = str_replace('%start%', MyOwnCalendarTool::formatDateWithDay($start_date,$order->id_lang).$startts, $msg);
     			if ($end_timselot != null && $end_timselot->sqlId > 0) $endts=' '.MyOwnCalendarTool::formatTime($end_timselot->endTime,$order->id_lang); else $endts='';
     			$msg = str_replace('%end%', MyOwnCalendarTool::formatDateWithDay($end_date,$order->id_lang).$endts, $msg);
     			if (!$reschedule) $msg = str_replace('%re%', '', $msg);
     			else $msg = str_replace('%re%', 're', $msg);
     			$email = trim($productFamilly->email);
     			if (stripos($email, ';')) {
       					$email = explode(';', $email);
	   					$email = array_map('trim', $email);
     			}

				if (trim($msg)!='' && (is_array($email) || ($email!='' && Validate::isEmail($email)))) {
       				
      				$varsTpl = array('{lastname}' => $productFamilly->name, '{firstname}' => '', '{order_name}' => $order_name, '{id_order}' => $reservation->id_order, '{message}' => (Configuration::get('PS_MAIL_TYPE') == 2 ? $msg : str_replace(array("\r\n", "\r", "\n"), '<br />', $msg)));
      				if (!Mail::Send(intval($order->id_lang), 'order_merchant_comment', "reservation", $varsTpl, $email, $productFamilly->name))
      					return false;
  				}
  			}
  		}
		return true;		
	}
	
	public static function notificationMsg($obj, $order)
	{
		// Getting differents vars
		$displayPrice =intval((Configuration::get('MYOWNRES_PRICE_TYPE')!=reservation_price_display::HIDE));
		$productFamillies=array();
		$productFamilliesMails=array();
		$id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
	 	$currency = new Currency($order->id_currency);
		$configuration = Configuration::getMultiple(array('PS_SHOP_EMAIL', 'PS_MAIL_METHOD', 'PS_MAIL_SERVER', 'PS_MAIL_USER', 'PS_MAIL_PASSWD', 'PS_SHOP_NAME'));
		$customer = new Customer($order->id_customer);
		$cart = new Cart($order->id_cart);
		$delivery = new Address((int)($order->id_address_delivery));
		$invoice = new Address((int)($order->id_address_invoice));
		if (_PS_VERSION_ >= "1.5.0.0") $order_date_text = Tools::displayDate($order->date_add);
		else $order_date_text = Tools::displayDate($order->date_add, (int)($id_lang));
		$carrier = new Carrier((int)($order->id_carrier));
		$message = $order->getFirstMessage();
		if (!$message OR empty($message))
			$message = $obj->l('No message');
		$resv_title=Configuration::get('MYOWNRES_MSG_RESV', $id_lang);
		if ($resv_title=='') $resv_title = ucfirst(myOwnLang::$object[MYOWN_OBJ::RESV]);
		$itemsTable = '';
		
		$products = $order->getProducts();//$products = $cart->getProducts();
		$customizedDatas = Product::getAllCustomizedDatas(intval($order->id_cart));
		Product::addCustomizationPrice($products, $customizedDatas);
		foreach ($products AS $key => $product)
		{
			if (array_key_exists('price_wt', $product)) $unit_price = $product['price_wt'];
			else $unit_price = $product['product_price_wt'];
			
			$customizationText = '';
			if (isset($customizedDatas[$product['product_id']][$product['product_attribute_id']]))
			{

				foreach ($customizedDatas[$product['product_id']][$product['product_attribute_id']] AS $customization)
				{
					if (isset($customization['datas'][_CUSTOMIZE_TEXTFIELD_]))
						foreach ($customization['datas'][_CUSTOMIZE_TEXTFIELD_] AS $text)
							$customizationText .= $text['name'].':'.' '.$text['value'].'<br />';
					
					if (isset($customization['datas'][_CUSTOMIZE_FILE_]))
						$customizationText .= sizeof($customization['datas'][_CUSTOMIZE_FILE_]) .' '. Tools::displayError('image(s)').'<br />';
						
					$customizationText .= '---<br />';
				}
				
				$customizationText = rtrim($customizationText, '---<br />');
			}
			$customizedDatasKeys = array_keys($product['customizedDatas'][$order->id_address_invoice]);
			$resas = myOwnResas::getReservations($order->id, $product['product_id'], $product['product_attribute_id']);
			$count=0;
			if (count($resas)>0) {
				foreach ($resas AS $resa) 
					if (_PS_VERSION_ < "1.7.0.0" OR count($customizedDatasKeys)==0 OR in_array(-$resa->id_cartproduct, $customizedDatasKeys)) {
					$resa->_mainProduct = $obj->_products->getResProductFromProduct($product['product_id']);
					$resaPrice = $resa->getPriceTaxes(false);
					$resaPrice_wt = $resa->getPriceTaxes(true);
					if (!array_key_exists($resa->_mainProduct->sqlId, $productFamillies)) 
						$productFamillies[$resa->_mainProduct->sqlId] = $resa->_mainProduct;
						
					$email = trim($resa->_mainProduct->email);
	     			if (stripos($email, ';')) {
	       					$emailTab = explode(';', $email);
		   					$emailTab = array_map('trim', $emailTab);
		   					foreach($emailTab as $emailElem)
		   						if (trim($emailElem)!='' && Validate::isEmail($emailElem)) 
									$productFamilliesMails[] = $emailElem;

	     			} else if (trim($email)!='' && Validate::isEmail($email))
	     				$productFamilliesMails[] = $email;
	     			
	     			/*PrestaShopLogger::addLog(date('Y-m-d H:i:s') . 'test place email '.$resa->id_object.' : '.intval($resa->_mainProduct!=null));
	     			foreach ($obj->_extensions->getExtByType(extension_type::OBJECT) as $ext) 
	     				PrestaShopLogger::addLog(date('Y-m-d H:i:s') . 'test place email $ext'.$ext->id.' : '.$ext->name.':'.intval(method_exists($ext, "objectSelect")).'_'.intval(array_key_exists($resa->id_object, $ext->objects->list)).' '.trim($ext->objects->list[$resa->id_object]->email));*/
	     			if ($resa->id_object>0 && $resa->_mainProduct!=null)
						foreach ($obj->_extensions->getExtByType(extension_type::OBJECT) as $ext) 
							if (method_exists($ext, "objectSelect"))
								if (in_array($ext->id, $resa->_mainProduct->ids_options) )
									if (array_key_exists($resa->id_object, $ext->objects->list))
										if (trim($ext->objects->list[$resa->id_object]->email)!='')
											$productFamilliesMails[] = $ext->objects->list[$resa->id_object]->email;
				
					$count++;
					$resaUnitPrice = $resa->getUnitPriceWithReduc(false);
					$resaUnitPrice_wt = $resa->getUnitPriceWithReduc(true);
					$resaStr = $resa->toString($obj, $id_lang, false);
					$ext_text = '';
					foreach ($obj->_extensions->getExtByType(extension_type::CARTDETAILS) as $ext) 
					if (method_exists($ext, "displayCartDetails")) {
						$ext_content = $ext->displayCartDetails($obj, $resa, -1, 'notification');
						if ($ext_content!='') $ext_text.='<br/>'.$ext_content;
					}
					$itemsTable .=
					'<tr style="background-color:'.($key % 2 ? '#DDE2E6' : '#EBECEE').';">
						<td style="padding:0.6em 0.4em;">'.$product['product_reference'].'</td>
						<td style="padding:0.6em 0.4em;"><strong>'.$product['product_name'].(isset($product['attributes_small']) ? ' '.$product['attributes_small'] : '').(!empty($customizationText) ? '<br />'.$customizationText : '').'</strong><br /><b>'.$resv_title.'</b> : '.$resaStr.$ext_text.'</td>
						<td style="padding:0.6em 0.4em; text-align:right;">'.Tools::displayPrice(Product::getTaxCalculationMethod() == PS_TAX_EXC ? $resaUnitPrice*$displayPrice : $resaUnitPrice_wt*$displayPrice, $currency, false).'</td>
						<td style="padding:0.6em 0.4em; text-align:center;">'.$resa->quantity.'</td>
						<td style="padding:0.6em 0.4em; text-align:right;">'.Tools::displayPrice(Product::getTaxCalculationMethod() == PS_TAX_EXC ? $resaPrice*$displayPrice : $resaPrice_wt*$displayPrice, $currency, false).'</td>
					</tr>';
				}
			} else {
				$count++;
				$itemsTable .=
				'<tr style="background-color:'.($key % 2 ? '#DDE2E6' : '#EBECEE').';">
					<td style="padding:0.6em 0.4em;">'.$product['product_reference'].'</td>
					<td style="padding:0.6em 0.4em;"><strong>'.$product['product_name'].(isset($product['attributes_small']) ? ' '.$product['attributes_small'] : '').(!empty($customizationText) ? '<br />'.$customizationText : '').'</strong></td>
					<td style="padding:0.6em 0.4em; text-align:right;">'.Tools::displayPrice($unit_price, $currency, false).'</td>
					<td style="padding:0.6em 0.4em; text-align:center;">'.(int)($product['product_quantity']).'</td>
					<td style="padding:0.6em 0.4em; text-align:right;">'.Tools::displayPrice(($unit_price * $product['product_quantity']), $currency, false).'</td>
				</tr>';
			}
		}
		// Insert discounts from cart into order_discount table
		if (_PS_VERSION_ < "1.7.0.0") {
			if (_PS_VERSION_ >= "1.5.0.0") $discounts = $cart->getCartRules();
			else $discounts = $cart->getDiscounts();
			$discountsList = '';
			$total_discount_value = 0;
			foreach ($discounts AS $discount)
			{
				$objDiscount = new Discount((int)$discount['id_discount']);
				$value = $objDiscount->getValue(sizeof($discounts), $cart->getOrderTotal(true, Cart::ONLY_PRODUCTS), $order->total_shipping, $cart->id);

				$discountsList .=
				'<tr style="background-color:#EBECEE;">
						<td colspan="4" style="padding: 0.6em 0.4em; text-align: right;">'.$obj->l('Voucher code:').' '.$objDiscount->name.'</td>
						<td style="padding: 0.6em 0.4em; text-align: right;">'.($value != 0.00 ? '-' : '').Tools::displayPrice($value, $currency, false).'</td>
				</tr>';
			}
		}

		if ($delivery->id_state)
			$delivery_state = new State((int)($delivery->id_state));
		if ($invoice->id_state)
			$invoice_state = new State((int)($invoice->id_state));

		// Filling-in vars for email
		$summary = $cart->getSummaryDetails();
		$template = 'new_order';
		$subject = $obj->l('New order', 'utils', (int)$id_lang).' - '.sprintf('%06d', $order->id);
		$templateVars = array(
			'{firstname}' => $customer->firstname,
			'{lastname}' => $customer->lastname,
			'{email}' => $customer->email,
			'{delivery_block_txt}' => self::_getFormatedAddress($delivery, "\n"),
			'{invoice_block_txt}' => self::_getFormatedAddress($invoice, "\n"),
			'{delivery_block_html}' => self::_getFormatedAddress($delivery, "<br />", 
				array(
					'firstname'	=> '<span style="color:#DB3484; font-weight:bold;">%s</span>', 
					'lastname'	=> '<span style="color:#DB3484; font-weight:bold;">%s</span>')),
			'{invoice_block_html}' => self::_getFormatedAddress($invoice, "<br />", 
				array(
					'firstname'	=> '<span style="color:#DB3484; font-weight:bold;">%s</span>',
					'lastname'	=> '<span style="color:#DB3484; font-weight:bold;">%s</span>')),
			'{delivery_company}' => $delivery->company,
			'{delivery_firstname}' => $delivery->firstname,
			'{delivery_lastname}' => $delivery->lastname,
			'{delivery_address1}' => $delivery->address1,
			'{delivery_address2}' => $delivery->address2,
			'{delivery_city}' => $delivery->city,
			'{delivery_postal_code}' => $delivery->postcode,
			'{delivery_country}' => $delivery->country,
			'{delivery_state}' => $delivery->id_state ? $delivery_state->name : '',
			'{delivery_phone}' => $delivery->phone,
			'{delivery_other}' => $delivery->other,
			'{invoice_company}' => $invoice->company,
			'{invoice_firstname}' => $invoice->firstname,
			'{invoice_lastname}' => $invoice->lastname,
			'{invoice_address2}' => $invoice->address2,
			'{invoice_address1}' => $invoice->address1,
			'{invoice_city}' => $invoice->city,
			'{invoice_postal_code}' => $invoice->postcode,
			'{invoice_country}' => $invoice->country,
			'{invoice_state}' => $invoice->id_state ? $invoice_state->name : '',
			'{invoice_phone}' => $invoice->phone,
			'{invoice_other}' => $invoice->other,
			'{order_name}' => sprintf("%06d", $order->id),
			'{shop_name}' => Configuration::get('PS_SHOP_NAME'),
			'{date}' => $order_date_text,
			'{carrier}' => (($carrier->name == '0') ? Configuration::get('PS_SHOP_NAME') : $carrier->name),
			'{payment}' => Tools::substr($order->payment, 0, 32),
			'{items}' => $itemsTable,
			'{total_paid}' => Tools::displayPrice($order->total_paid, $currency),
			'{total_products}' => Tools::displayPrice($order->getTotalProductsWithTaxes(), $currency),
			'{total_discounts}' => Tools::displayPrice($order->total_discounts, $currency),
			'{total_shipping}' => Tools::displayPrice($order->total_shipping, $currency),
			'{total_wrapping}' => Tools::displayPrice($order->total_wrapping, $currency),
			'{currency}' => $currency->sign,
			'{message}' => $message,
			'{total_products}' => Tools::displayPrice($cart->getOrderTotal(true, Cart::ONLY_PRODUCTS), $currency, false), 
			'{total_order}' => Tools::displayPrice($cart->getOrderTotal(true, Cart::BOTH)+$summary['balance_reservations_wt'], $currency, false), 
			'{total_balance}' => Tools::displayPrice($summary['balance_reservations_wt'], $currency, false)
		);
		$path=_PS_MODULE_DIR_ . "myownreservations/";
		$iso = Language::getIsoById((int)($id_lang));
		
		//Add extenssion attachements
		$file_attachments=array();
		foreach ($obj->_extensions->getExtByType(extension_type::NOTIFICATION) as $ext) 
			if (method_exists($ext, "getAttachements"))
				$file_attachments = array_merge($file_attachments , $ext->getAttachements($obj, $order, $resas));
						
		
		if (file_exists($path.'/mails/'.$iso.'/'.$template.'.txt') AND file_exists($path.'/mails/'.$iso.'/'.$template.'.html'))
			if (!empty($productFamilliesMails)) Mail::Send($id_lang, $template, $subject, $templateVars, $productFamilliesMails, NULL, $configuration['PS_SHOP_EMAIL'], "", $file_attachments, NULL, $path.'/mails/');
	}
	
	public static function isModuleActive($name)
	{
		$result = Db::getInstance()->ExecuteS("
			SELECT * 
			FROM  `"._DB_PREFIX_."module` 
			WHERE `name` LIKE '".$name."'");
			if (count($result)==0) return false;
			$id_module = $result[0]["id_module"];
			$active = $result[0]["active"];
			
		if (_PS_VERSION_ >= "1.5.0.0") {
			$list = Shop::getContextListShopID();
			$sql = 'SELECT `id_shop` FROM `'._DB_PREFIX_.'module_shop`
					WHERE `id_module` = '.$id_module.
					' AND `id_shop` IN('.implode(', ', $list).')';
			$result = Db::getInstance()->ExecuteS($sql);
			if (count($result)==0) return false;
			return true;
		} else {
			return ($active);
		}
	}

}

?>