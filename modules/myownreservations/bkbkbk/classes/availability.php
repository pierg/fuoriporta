<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnAvailability {
	var $sqlId;
	var $id_product;
	var $id_product_attribute;
	var $id_object;
	var $id_family=0;
	var $type;
	var $startDate;
	var $endDate;
	var $startTimeslot;
	var $endTimeslot;
	var $startTime;
	var $endTime;
	var $quantity=0;
	
	//Create a timeslot in database
	//-----------------------------------------------------------------------------------------------------------------
	function sqlInsert() {
		$id_shop = 0;
		if (defined('MYOWNRES_SEPARATE_SHOP') && MYOWNRES_SEPARATE_SHOP)
			$id_shop = myOwnUtils::getShop();
		$query  = 'INSERT INTO '. _DB_PREFIX_ .'myownreservations_availability (id_family, id_product, id_product_attribute, id_object, type, start_date, end_date, start_timeslot, end_timeslot, quantity'.($id_shop ? ', id_shop' :'').') VALUES ('.(int)$this->id_family.', '.(int)$this->id_product.', '.$this->id_product_attribute.', '.(int)$this->id_object.', '.$this->type.', "'.$this->startDate.'", "'.$this->endDate.'", "'.$this->startTimeslot.'", "'.$this->endTimeslot.'", "'.$this->quantity.'"'.($id_shop ? ', "'.$id_shop.'"' :'').');';
		return Db::getInstance()->Execute($query);
	}
	
	function sqlUpdate() {
		$query  = 'UPDATE '. _DB_PREFIX_ .'myownreservations_availability SET id_product = "'.$this->id_product.'", id_family = "'.$this->id_family.'", id_product_attribute = "'.$this->id_product_attribute.'", id_object = "'.$this->id_object.'", type = "'.$this->type.'", start_date = "'.$this->startDate.'", end_date = "'.$this->endDate.'", start_timeslot = "'.$this->startTimeslot.'", end_timeslot = "'.$this->endTimeslot.'", quantity = "'.$this->quantity.'" WHERE `id_availability` = ' . $this->sqlId . ';';
		
		return Db::getInstance()->Execute($query);
	}
	
	//Delete a prices rule in database
	//-----------------------------------------------------------------------------------------------------------------
	function sqlDelete() {
		$query='DELETE FROM ' . _DB_PREFIX_ . 'myownreservations_availability WHERE id_availability = "' . $this->sqlId . '"';
		return Db::getInstance()->Execute($query);
	}
	static function sqlIdDelete($id) {
		$query='DELETE FROM ' . _DB_PREFIX_ . 'myownreservations_availability WHERE id_availability = "' . $id . '"';
		return Db::getInstance()->Execute($query);
	}
	
	static function sqlUpdateQty($id, $quantity=0) {
		$query="UPDATE `" . _DB_PREFIX_ . "myownreservations_availability` SET `quantity` = '".$quantity."' WHERE `id_availability` = " . $id . ";";
		return Db::getInstance()->Execute($query);
	}
	
	function getStart($_timeslots) {
		//if ($this->startTimeslot && array_key_exists($this->startTimeslot, $_timeslots))  echo '#'.$this->startDate.' '.$_timeslots[$this->startTimeslot]->startTime;
		if ($this->startTimeslot && array_key_exists($this->startTimeslot, $_timeslots)) 
			return strtotime($this->startDate.' '.$_timeslots[$this->startTimeslot]->startTime);
		else return strtotime($this->startDate.' 00:00:00');
	}
	
	function getEnd($_timeslots) {
		if ($this->endTimeslot && array_key_exists($this->endTimeslot, $_timeslots)) 
			return strtotime($this->endDate.' '.$_timeslots[$this->endTimeslot]->endTime);
		else if ($this->startDate == $this->endDate)
			return strtotime('+1 day',strtotime($this->endDate.' 00:00:00'));
		else 
			return strtotime($this->endDate.' 00:00:00');
	}

}


?>