<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnProductsRule extends myOwnPlanning {
	
	//Update a product in database
	//-----------------------------------------------------------------------------------------------------------------
	function save() {

		Configuration::updateValue('MYOWNRES_RES_CATEGORY', $this->ids_categories);
		Configuration::updateValue('MYOWNRES_RES_SHOP', $this->id_shop);
		Configuration::updateValue('MYOWNRES_RES_PRODUCTS', $this->ids_products);
		Configuration::updateValue('MYOWNRES_RES_NOTIFS', implode(',', $this->ids_notifs));
		Configuration::updateValue('MYOWNRES_RES_NOTIF', $this->email);
		Configuration::updateValue('MYOWNRES_RES_ASSIGNMENT', $this->assignment);
		
		Configuration::updateValue('MYOWNRES_PRODUCT_TYPE', $this->productType);
		Configuration::updateValue('MYOWNRES_OCCUPY_QTY', $this->_occupyQty);
		Configuration::updateValue('MYOWNRES_OCCUPY_COMB', $this->_occupyCombinations);
		
		Configuration::updateValue('MYOWNRES_RES_START_TYPE', $this->reservationStartType);
		if ($this->reservationStartType==reservation_start::AFTER_DAYS or $this->reservationStartType==reservation_start::SOME_WEEKS)
			Configuration::updateValue('MYOWNRES_RES_START_NB', $this->reservationStartParams);
		if ($this->reservationStartType==reservation_start::SAME_DAY)
			Configuration::updateValue('MYOWNRES_RES_START_CURRENT', $this->reservationStartParams);
	    if ($this->reservationStartType==reservation_start::NEXT_WEEK)
	     	Configuration::updateValue('MYOWNRES_RES_START_DAY', $this->reservationStartParams);
			//$reservationStartParams Configuration::updateValue('MYOWNRES_RES_START_DAY').';00:00:00';
		if ($this->reservationStartType==reservation_start::AFTER_HOURS or $this->reservationStartType==reservation_start::AFTER_MINUT) {
			$reservationStartParamsTab = explode(';', $this->reservationStartParams);
			Configuration::updateValue('MYOWNRES_RES_START_NB', $reservationStartParamsTab[0]);
			Configuration::updateValue('MYOWNRES_RES_START_CURRENT', $reservationStartParamsTab[1]);
		}
		Configuration::updateValue('MYOWNRES_RES_PERIOD', $this->reservationPeriod);
		
		Configuration::updateValue('MYOWNRES_UNIT',$this->_reservationUnit);
		Configuration::updateValue('MYOWNRES_RES_MIN_LENGTH', $this->_reservationMinLength);
		Configuration::updateValue('MYOWNRES_RES_LENGTH_CTRL', $this->_reservationLengthControl);
		Configuration::updateValue('MYOWNRES_RES_MAX_LENGTH', $this->_reservationMaxLength);
		Configuration::updateValue('MYOWNRES_RES_FREQ_LENGTH', $this->_reservationLengthFreq);
		Configuration::updateValue('MYOWNRES_RES_FREQ_STRICT', intval($this->_reservationLengthStrict));
		Configuration::updateValue('MYOWNRES_RES_LENGTHEXCEED', intval($this->_reservationLengthCanExceed));
		Configuration::updateValue('MYOWNRES_RES_LENGTHBETWDAYS', intval(intval($this->_allowResBetweenDays)));
		Configuration::updateValue('MYOWNRES_RES_LENGTHQTY', intval($this->_reservationLengthQuantity));
		Configuration::updateValue('MYOWNRES_BETWEEN_SHIFT', $this->_reservationBetweenShift);
		Configuration::updateValue('MYOWNRES_HOLIDAY_ENABLE', $this->_allowResOnHolidays);
		Configuration::updateValue('MYOWNRES_RESTDAY_ENABLE', $this->_allowResOnRestDays);
		
		Configuration::updateValue('MYOWNRES_SEL_TYPE', $this->reservationSelType);
		//Configuration::updateValue('MYOWNRES_SEL_MULTI', intval($this->_reservationSelMultiple));
		Configuration::updateValue('MYOWNRES_SEL_MODE', intval($this->_reservationSelMode));
		Configuration::updateValue('MYOWNRES_SEL_PLANNING', $this->reservationSelPlanning);

		Configuration::updateValue('MYOWNRES_SEL_DAYS', $this->_reservationSelDays);
		Configuration::updateValue('MYOWNRES_SEL_STARTDAY', $this->_reservationSelWeekStart);
		Configuration::updateValue('MYOWNRES_SEL_ENDDAY', $this->_reservationSelWeekEnd);
		Configuration::updateValue('MYOWNRES_SEL_STARTMONTH', $this->_reservationSelMonthStart);
		Configuration::updateValue('MYOWNRES_SEL_ENDMONTH', $this->_reservationSelMonthEnd);
		Configuration::updateValue('MYOWNRES_SEL_SHOWTIME', $this->_reservationShowTime);

		Configuration::updateValue('MYOWNRES_SEL_STARTTIME', $this->_reservationStartTime);
		Configuration::updateValue('MYOWNRES_SEL_ENDTIME', $this->_reservationEndTime);
		Configuration::updateValue('MYOWNRES_SEL_CUTTIME', $this->_reservationCutTime);
		Configuration::updateValue('MYOWNRES_SEL_TIMEBYDAY', $this->_reservationTimeByDay);
		Configuration::updateValue('MYOWNRES_TIMESLICE_LEN', $this->_timeSliceLength);

		Configuration::updateValue('MYOWNRES_PRICE_UNIT', $this->_priceUnit);
		Configuration::updateValue('MYOWNRES_PRICE_CALC', $this->_reservationPriceType);
		Configuration::updateValue('MYOWNRES_PRICE_DOUBLE', intval($this->_priceDoubleIfLength));
		Configuration::updateValue('MYOWNRES_PRICE_COMBINATION', intval($this->_priceCombination));
		Configuration::updateValue('MYOWNRES_PRICE_COEFPROD', intval($this->_coefPerProduct));
		Configuration::updateValue('MYOWNRES_ENDTS_IGNORE', $this->_priceEndTimeSlot);
		Configuration::updateValue('MYOWNRES_DISABLED_PRICE', $this->_priceDisabledTimeSlots);
		Configuration::updateValue('MYOWNRES_PRICE_STRICT', $this->_priceStrict);
		Configuration::updateValue('MYOWNRES_PRICE_PREVIEW', $this->_pricePreview);
		Configuration::updateValue('MYOWNRES_PRICE_HOLIDAYS', $this->_priceHolidays);
		
		Configuration::updateValue('MYOWNRES_RES_SELL', $this->_optionAlsoSell);
		Configuration::updateValue('MYOWNRES_RES_STATUS', $this->_optionValidationStatus);

		Configuration::updateValue('MYOWNRES_QTY_DISPLAY', $this->_qtyDisplay);
		Configuration::updateValue('MYOWNRES_QTY_TYPE', $this->_qtyType);
		Configuration::updateValue('MYOWNRES_QTY_SEARCH', $this->_qtySearch);
		Configuration::updateValue('MYOWNRES_QTY_FIXED', $this->_qtyFixed);
		Configuration::updateValue('MYOWNRES_QTY_CAPACITY', $this->_qtyCapacity);
		
		Configuration::updateValue('MYOWNRES_DEPOSIT_CALC', $this->_depositCalc);
		Configuration::updateValue('MYOWNRES_DEPOSIT_BYPROD', $this->_depositByProduct);
		Configuration::updateValue('MYOWNRES_ORDER_DEPOSIT', $this->_depositAmount);
		Configuration::updateValue('MYOWNRES_ORDER_ADVANCE', $this->_advanceRate);
		Configuration::updateValue('MYOWNRES_ORDER_FIXEDADVANCE', $this->_advanceAmount);
		
		Configuration::updateValue('MYOWNRES_RES_OPTIONS', implode(',', $this->ids_options));
		
		return true;
	}

}
?>