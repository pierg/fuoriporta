<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnAvailabilities {
	private static $_instance = null;
	var $list=array();
	var $isAvailabilities = false;
	var $_timeslots=null;
	
	public static function setInstance($instance) {
		self::$_instance = $instance;
	}
	public static function getInstance() {
		if(is_null(self::$_instance)) {
	      self::$_instance = new myOwnAvailabilities();
	      $trace=debug_backtrace();
	    }
	    return self::$_instance;
	}
	
	//Construct rules list
	//-----------------------------------------------------------------------------------------------------------------
	public function __construct($start=0, $end=0, $mainProduct=null, $id_product=0, $id_product_attribute=0, $strictPeriod=false, $strictProduct=false, $widget=false) {
		$this->isAvailabilities = self::isAvailabilities($mainProduct, $id_product, $id_product_attribute);
		if ($this->_timeslots==null) {
			if ($mainProduct==null) $this->_timeslots = new myOwnTimeSlots();
			else $this->_timeslots = $mainProduct->_timeslotsObj;
		}
		if ($start==0) $this->list = self::getAvailabilities();
		else $this->list = $this->getAvailabilitiesOnPeriod($start, $end, $mainProduct, $id_product, $id_product_attribute, $strictPeriod, $strictProduct, $widget);
	}
	
	public static function getAvailabilities($id_product=-1, $id_product_attribute=-1) {
		$availabilities = array();
		$id_shop = myOwnUtils::getShop();
		if ($id_product==0 && $id_product_attribute==0) {
			$sql = "SELECT * FROM " . _DB_PREFIX_ . "myownreservations_availability WHERE id_availability > 0 AND id_product=0 AND id_product_attribute=0";
		} else {
			$sql = "SELECT * FROM " . _DB_PREFIX_ . "myownreservations_availability WHERE id_availability > 0";
			if ($id_product!=-1) $sql .= " AND (id_product = ".$id_product." OR id_product = 0)";
			if ($id_product_attribute!=-1) $sql .= " AND (id_product_attribute = ".$id_product_attribute." OR id_product_attribute = 0)";
		}
		if (defined('MYOWNRES_SEPARATE_SHOP') && MYOWNRES_SEPARATE_SHOP)
			$sql.=" AND id_shop = ".$id_shop;
		$availabilitiesSql = Db::getInstance()->ExecuteS($sql);
		if (is_array($availabilitiesSql)) 
		foreach($availabilitiesSql AS $availability) {
			$tempAvailability = new myOwnAvailability();
			$tempAvailability->sqlId = $availability['id_availability'];
			$tempAvailability->id_family = $availability['id_family'];
			if (isset($availability['id_shop'])) $tempAvailability->id_shop = $availability['id_shop'];
			$tempAvailability->id_product = $availability['id_product'];
			$tempAvailability->id_product_attribute = $availability['id_product_attribute'];
			$tempAvailability->type = $availability['type'];
			$tempAvailability->startDate = $availability['start_date'];
			$tempAvailability->endDate = $availability['end_date'];
			$tempAvailability->startTimeslot = $availability['start_timeslot'];
			$tempAvailability->endTimeslot = $availability['end_timeslot'];
			$tempAvailability->quantity = $availability['quantity'];
			if (isset( $availability['id_object'])) $tempAvailability->id_object  = $availability['id_object'];
			$availabilities[] = $tempAvailability;
		}
		return $availabilities;
	}
	
	public static function isAvailabilities($mainProduct=null, $id_product=0, $id_product_attribute=0) {
		$availabilities = array();
		$id_shop = myOwnUtils::getShop();
		$sql = "SELECT COUNT(*) as myowncnt FROM " . _DB_PREFIX_ . "myownreservations_availability WHERE id_availability > 0 AND type=1 AND id_product >=0";
		if ($id_product!=0 && $mainProduct==null) $sql .= " AND (id_product = ".$id_product." OR id_product = 0)";
		if ($id_product!=0 && $mainProduct!=null) $sql .= " AND (id_product = ".$id_product." OR id_family = ".$mainProduct->sqlId.")";
		//if id_familly ==-1 mean merged famillies so we get all
		if ($id_product==0 && $mainProduct!=null && $mainProduct->sqlId>-1) $sql .= " AND (id_family = ".$mainProduct->sqlId.")";
		if ($id_product==0 && $mainProduct!=null && $mainProduct->sqlId==-1) $sql .= " AND id_product = 0 AND id_family=0";
		if ($id_product_attribute!=0) $sql .= " AND (id_product_attribute = ".$id_product_attribute." OR id_product_attribute = 0)";
		if (defined('MYOWNRES_SEPARATE_SHOP') && MYOWNRES_SEPARATE_SHOP)
			$sql.=" AND id_shop = ".$id_shop;
		$availabilitiesSql = Db::getInstance()->ExecuteS($sql);

		return ($availabilitiesSql[0]['myowncnt']>0);
	}
	
	//$strictProduct=true when checking availabilities on product list
	//$widget=true when using selection widget
	public function getAvailabilitiesOnPeriod($start, $end, $mainProduct=null, $id_product=0, $id_product_attribute=0, $strictPeriod=false, $strictProduct=false, $widget=false) {
		$availabilities = array();
		$id_shop = myOwnUtils::getShop();
		$sql = "SELECT * FROM " . _DB_PREFIX_ . "myownreservations_availability WHERE start_date <= '".date('Y-m-d', $end)."' AND end_date >= '".date('Y-m-d', $start)."'";
		if ($strictPeriod) $sql .= " AND start_date >= '".date('Y-m-d', $start)."' AND end_date <= '".date('Y-m-d', $end)."'";
		//select case we need availabilities to apply on all even if they are one a prodcut
		if (0 && MYOWNRES_WEEK_TS && $mainProduct!=null) {
			$start_ts = $mainProduct->_timeslotsObj->getTimeSlotOfTime($start, false);
			if ($start_ts!=null && $start_ts->sqlId) $sql .= " AND start_timeslot = '".$start_ts->sqlId."'";
		}
		if ($mainProduct->productType == product_type::RESOURCE) {
			$sql .= " AND (id_product < 0)";
		} else {
			if (!$strictProduct && $id_product>0) $sql .= " AND (id_product = ".$id_product." OR (id_product = 0 AND (id_family = ".($mainProduct==null ? 0 : $mainProduct->sqlId." OR id_family = 0").")))";
			if (!$strictProduct && $id_product==0) {
				if ($mainProduct==null || $mainProduct->sqlId>-1)
					$sql .= " AND ((id_family = ".($mainProduct==null ? 0 : $mainProduct->sqlId." OR id_family = 0").")".($widget ? " AND (id_product = 0 OR type = 1)" : "").")"; //added id_prod=0 to get global availabilities only, added && id_prod>0 insead we don't get all when checking qty by availabilities // 2016 07 removed  AND id_product = 0 because we need to get even ones on products
				else {
					$sql .= ' AND ( '.(count(array_filter(explode(',', $mainProduct->ids_products)))>0 ? 'id_product IN ('.$mainProduct->ids_products.') OR ' : '').'id_family IN ('.implode(',', $mainProduct->_mergedFamilies).'))';
				}
			}
			if ($strictProduct && $id_product>0) $sql .= " AND (id_product = ".$id_product." AND (id_family = ".($mainProduct==null ? 0 : $mainProduct->sqlId)." OR id_family = 0))"; //added && id_prod>0 insead we don't get all when checking qty by availabilities 
			if ($strictProduct && $id_product==0) $sql .= " AND ((id_product = 0 AND (id_family = ".($mainProduct==null ? 0 : $mainProduct->sqlId)." OR id_family = 0)) ".($mainProduct==null ? '' : 'OR (id_product > 0 AND id_family = '.$mainProduct->sqlId.') '.(count(array_filter(explode(',', $mainProduct->ids_products)))>0 ? 'OR (id_product IN ('.$mainProduct->ids_products.') AND id_family = '.$mainProduct->sqlId.') ' : '').' ').")"; //added && id_prod>0 insead we don't get all when checking qty by availabilities 
			if ($id_product_attribute>0) $sql .= " AND (id_product_attribute = ".$id_product_attribute." OR id_product_attribute = 0)";
		}
		if (defined('MYOWNRES_SEPARATE_SHOP') && MYOWNRES_SEPARATE_SHOP)
			$sql.=" AND id_shop = ".$id_shop;

		$availabilitiesSql = Db::getInstance()->ExecuteS($sql);
		if (is_array($availabilitiesSql)) 
		foreach($availabilitiesSql AS $availability) {
			$tempAvailability = new myOwnAvailability();
			$tempAvailability->sqlId = $availability['id_availability'];
			$tempAvailability->id_family = $availability['id_family'];
			if (array_key_exists('id_shop', $tempAvailability)) $tempAvailability->id_shop = $availability['id_shop'];
			$tempAvailability->id_product = $availability['id_product'];//($strictProduct || $availability['id_family']>0 ? $availability['id_product'] : 0); removed because we nned to apply on avail on right product on home
			$tempAvailability->id_product_attribute = $availability['id_product_attribute'];
			$tempAvailability->type = $availability['type'];
			$tempAvailability->startDate = $availability['start_date'];
			$tempAvailability->endDate = $availability['end_date'];
			$tempAvailability->startTimeslot = $availability['start_timeslot'];
			$tempAvailability->endTimeslot = $availability['end_timeslot'];
			$tempAvailability->quantity = $availability['quantity'];
			if (array_key_exists($tempAvailability->startTimeslot, $this->_timeslots->list))
				$tempAvailability->startTime=$this->_timeslots->list[$tempAvailability->startTimeslot]->startTime;
			else $tempAvailability->startTime='00:00:00';
			if (array_key_exists($tempAvailability->endTimeslot, $this->_timeslots->list))
				$tempAvailability->endTime=$this->_timeslots->list[$tempAvailability->endTimeslot]->endTime;
			else $tempAvailability->endTime='00:00:00';
			if (isset( $availability['id_object'])) $tempAvailability->id_object  = $availability['id_object'];
			$availabilities[] = $tempAvailability;
		}

		return $availabilities;
	}
	
		
	public static function getAvailabilitiesFiltered($start=0, $end=0, $id_product = 0, $product = '', $id_lang=1)
	{
		$id_shop = myOwnUtils::getShop();
		$availabilities = array();
		$sql = 'SELECT *, '._DB_PREFIX_.'myownreservations_availability.id_product as availprod FROM '._DB_PREFIX_.'myownreservations_availability 
				LEFT JOIN '._DB_PREFIX_.'product_lang ON ('._DB_PREFIX_.'myownreservations_availability.id_product = '._DB_PREFIX_.'product_lang.id_product AND '._DB_PREFIX_.'product_lang.id_lang = '.$id_lang.' '.($id_shop ? ' AND '._DB_PREFIX_.'product_lang.id_shop = '.$id_shop.' ' : ' AND '._DB_PREFIX_.'product_lang.id_shop=(SELECT MAX(id_shop)  FROM '._DB_PREFIX_.'product_lang)').') 
				WHERE id_availability > 0 ';
		if (is_array($id_product) && count(array_filter($id_product))>0) $sql .= 'AND '._DB_PREFIX_.'myownreservations_availability.id_product IN ( '.implode(',',array_filter($id_product)).')';
		elseif ($id_product!=0) $sql .= 'AND '._DB_PREFIX_.'myownreservations_availability.id_product = '.(int)$id_product.' ';
		if ($start>0) $sql .= 'AND start_date >= "'.date('Y-m-d', $start).'" ';
		if ($end>0) $sql .= 'AND end_date <= "'.date('Y-m-d', $end).'" ';
		if ($product != '') $sql .= 'AND '._DB_PREFIX_.'product_lang.name LIKE "%'.$product.'%"';
		if (defined('MYOWNRES_SEPARATE_SHOP') && MYOWNRES_SEPARATE_SHOP)
			$sql.=" AND "._DB_PREFIX_."myownreservations_availability.id_shop = ".$id_shop;

		$availabilities_results = Db::getInstance()->ExecuteS($sql);
		if (is_array($availabilities_results))
		foreach($availabilities_results AS $availability) {
			$tempAvailability = new myOwnAvailability();
			$tempAvailability->sqlId = $availability['id_availability'];
			$tempAvailability->id_family = $availability['id_family'];
			if (array_key_exists('id_shop', $tempAvailability)) $tempAvailability->id_shop = $availability['id_shop'];
			$tempAvailability->id_product = $availability['availprod'];
			$tempAvailability->id_product_attribute = $availability['id_product_attribute'];
			$tempAvailability->type = $availability['type'];
			$tempAvailability->startDate = $availability['start_date'];
			$tempAvailability->endDate = $availability['end_date'];
			$tempAvailability->startTimeslot = $availability['start_timeslot'];
			$tempAvailability->endTimeslot = $availability['end_timeslot'];
			$tempAvailability->quantity = $availability['quantity'];

			if (isset( $availability['id_object'])) $tempAvailability->id_object  = $availability['id_object'];
			$availabilities[] = $tempAvailability;
		}
		return $availabilities;
	}
	
	public static function getAvailabilitiesStr($type) {
		$availabilities = self::getAvailabilities(0, 0);
		
		$availabilitiesStr="";
		foreach($availabilities AS $availability) {
			if ($availability->type==$type) {
				if ($availabilitiesStr!="") $availabilitiesStr.=";";
				$availabilitiesStr.= $availability->startDate.":".$availability->endDate;
			}
		}
		return $availabilitiesStr;
	}
	
	public function isAvailable($day, $timeslot=null, $id_product=0, $id_product_attribute=0, $id_object=0) {
		$day=strtotime(date("Y-m-d",$day)." 00:00:00");

		if ($timeslot != null && $timeslot->sqlId>0) {
			$dayStartTime = strtotime(date('Y-m-d',$day).' '.$timeslot->startTime);
			$dayEndTime = strtotime(date('Y-m-d',$day).' '.$timeslot->endTime);
		/*} else if ($timeslot != null && $timeslot->startTime > $timeslot->endTime) {
			$dayStartTime = strtotime(date('Y-m-d',$day).' '.$timeslot->startTime);
			$dayEndTime = strtotime('+1day',strtotime(date('Y-m-d',$day).' '.$timeslot->endTime));
		*/} else {
			$dayStartTime = $day;
			$dayEndTime = strtotime('+1 day',$day);
		}
		
		return $this->isPeriodAvailable($dayStartTime, $dayEndTime, $id_product, $id_product_attribute, $id_object);
	}
	
	public function isPeriodTsAvailable($start, $end, $start_timeslot, $end_timeslot, $id_product=0, $id_product_attribute=0, $id_object=0) {
		$timeslots= $this->_timeslots->list;
		if ($start_timeslot==0)
			$dayStartTime = strtotime(date('Y-m-d',$start).' 00:00:00');
		else $dayStartTime = strtotime(date('Y-m-d',$start).' '.$timeslots[$start_timeslot]->startTime);
		if ($end_timeslot==0)
			$dayEndTime = strtotime('+1day',strtotime(date('Y-m-d',$end).' 00:00:00'));
		else $dayEndTime = strtotime(date('Y-m-d',$end).' '.$timeslots[$end_timeslot]->endTime);

		return $this->isPeriodAvailable($dayStartTime, $dayEndTime, $id_product, $id_product_attribute, $id_object);
	}
	
	public function isPeriodAvailable($dayStartTime, $dayEndTime, $id_product=0, $id_product_attribute=0, $id_object=0) {
		$timeslots= $this->_timeslots->list;

		//check availability
		$inPeriod=false;
		if ($this->isAvailabilities)
			foreach($this->list AS $availability) {
				//$id_product==0 or 
				if ($availability->type==1
					&& ($id_product == 0 or $availability->id_product <= 0 or $id_product == $availability->id_product)
					&& ($id_product_attribute==0 or $availability->id_product_attribute==0 or $availability->id_product_attribute == $id_product_attribute)
					&& ($id_object == 0 or $availability->id_object == 0 or $id_object == $availability->id_object) ) {
					
					if (MYOWNRES_WEEK_TS) $availabilityStart = strtotime($availability->startDate.' 00:00:00');
					else $availabilityStart = $availability->getStart($timeslots);
					
					if ( $dayStartTime >= $availabilityStart AND $dayEndTime <= $availability->getEnd($timeslots) ) {
						$inPeriod=true;
						break;
					}
				}
			}
		if ($this->isAvailabilities && !$inPeriod) return false;
		//check unavailability
		foreach($this->list AS $availability) {
			if ($availability->type==0
				&& ($id_product == 0 or $availability->id_product <= 0 or $id_product == $availability->id_product)
				&& ($id_product_attribute==0 or $availability->id_product_attribute==0 or $availability->id_product_attribute == $id_product_attribute)
				&& ($id_object == 0 or $availability->id_object == 0 or $id_object == $availability->id_object) ) {
					//echo "availability".date('y-m-d H:i',$availability->getStart($timeslots)).'-'.date('y-m-d H:i',$availability->getEnd($timeslots));
				if ( $availability->getEnd($timeslots) > $dayStartTime AND $availability->getStart($timeslots) < $dayEndTime ) {/*echo date('y-m-d H:i',$availability->getEnd($timeslots)).'>'.date('y-m-d H:i',$dayStartTime).':'.date('y-m-d H:i',$availability->getStart($timeslots)).'<'.date('y-m-d H:i',$dayEndTime);*/return false;};	
			}	
		}
		return true;
	}
}

?>