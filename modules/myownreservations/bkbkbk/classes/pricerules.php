<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnPricesRules {
	var $list;
	
	//Construct rules list
	//-----------------------------------------------------------------------------------------------------------------
	public function __construct() {
		$this->list=array();
		$id_shop = myOwnUtils::getShop();
		$sql="SELECT * FROM " . _DB_PREFIX_ . "myownreservations_pricesrule";
		if (defined('MYOWNRES_SEPARATE_SHOP') && MYOWNRES_SEPARATE_SHOP)
			$sql.=" WHERE id_shop = ".$id_shop;
		$pricesruleSql = Db::getInstance()->ExecuteS($sql);
		foreach($pricesruleSql AS $pricesrule) {
			$tempPricesrule = new myOwnPricesRule();
			$tempPricesrule->sqlId = $pricesrule['id_pricesrule'];
			$tempPricesrule->name = $pricesrule['name'];
			if (isset($pricesrule['id_object']))
				$tempPricesrule->id_object = $pricesrule['id_object'];
			if (isset($pricesrule['id_family']))
				$tempPricesrule->id_family = $pricesrule['id_family'];
			if (isset($pricesrule['product']))
				$tempPricesrule->ids_products = array($pricesrule['product']);
			else $tempPricesrule->ids_products = array_filter(explode(',', $pricesrule['ids_products']));
			if (array_key_exists('id_product_attribute', $pricesrule))
				$tempPricesrule->id_product_attribute = array($pricesrule['id_product_attribute']);
			else $tempPricesrule->ids_attributes = array_filter(explode(',', $pricesrule['ids_attributes']));
			$tempPricesrule->type = $pricesrule['type'];
			$tempPricesrule->impact = $pricesrule['impact'];
			$tempPricesrule->ratio = $pricesrule['ratio'];
			$tempPricesrule->amount = $pricesrule['amount'];
			$tempPricesrule->overall = $pricesrule['overall'];
			$tempPricesrule->quantity = $pricesrule['quantity'];
			$tempPricesrule->params = $pricesrule['params'];
			$tempPricesrule->enable = $pricesrule['enable'];
			$tempPricesrule->visible = ($pricesrule['visible']==1);
			if (isset($pricesrule['style']))
				$tempPricesrule->style = $pricesrule['style'];
			$this->list[$pricesrule['id_pricesrule']] = $tempPricesrule;
		}
	}
	public function getForProduct($id_family, $productId, $productAttributeId) {
		$list = array();
		foreach($this->list as $priceRule)
			if (  $priceRule->doesApplyToProduct($id_family, $productId, $productAttributeId)  )
					$list[$priceRule->sqlId] = $priceRule;
		return $list;
	}
	
	public function getForFamily($id_family) {
		$list = array();
		foreach($this->list as $priceRule)
			if (  $priceRule->id_family==$id_family )
					$list[$priceRule->sqlId] = $priceRule;
		return $list;
	}
	
	public static function getFromPost($obj, $cookie) {
		$tempPricesrule = new myOwnPricesRule();
		
		$tempPricesrule->type = intval($_POST['pricesruleType']);
		$tempPricesrule->name = $_POST['pricesruleName'];

		if (isset($_POST['pricesruleProduct'])) {
			if (is_array($_POST['pricesruleProduct'])) {
				$tempPricesrule->ids_products = array_filter($_POST['pricesruleProduct']);
			} else $tempPricesrule->ids_products = array($_POST['pricesruleProduct']);
		}
		$tempPricesrule->id_family = (int)Tools::getValue('pricesruleFamily',0);
		if (isset($_POST['pricesruleProductAttribute'])) {
			if (is_array($_POST['pricesruleProductAttribute'])) {
				$tempPricesrule->ids_attributes = array_filter($_POST['pricesruleProductAttribute']);
			} else $tempPricesrule->ids_attributes = array($_POST['pricesruleProductAttribute']);
		}
		/*$pricesruleProductAttribute=0;
		if (isset($_POST['pricesruleProductAttribute'])) $pricesruleProductAttribute=$_POST['pricesruleProductAttribute'];
		$tempPricesrule->id_product_attribute = $pricesruleProductAttribute;*/
		$tempPricesrule->impact = (int)Tools::getValue('pricesruleImpact');
		$tempPricesrule->amount = Tools::getValue('pricesruleAmount');
		$tempPricesrule->ratio = (int)$_POST['pricesruleRatio']/100;
		$tempPricesrule->overall = intval(isset($_POST['pricesruleOverallImpact']) && $_POST['pricesruleOverallImpact']==1);
		$tempPricesrule->quantity = intval(isset($_POST['pricesruleQuantityImpact']) && $_POST['pricesruleQuantityImpact']==1);
		$tempPricesrule->id_object = (int)Tools::getValue('object');
		$tempPricesrule->enable = intval(isset($_POST['pricesruleEnable']) && $_POST['pricesruleEnable']==1);
		$tempPricesrule->visible = intval(isset($_POST['pricesruleVisible']) && $_POST['pricesruleVisible']==1);
		$params = "";
		if ($tempPricesrule->type == pricesrules_type::DATES) {
			$params .= $_POST['pricesruleParamStart'].';';
			$params .= $_POST['pricesruleParamEnd'].';';
			$params .= intval(isset($_POST['pricesruleDateParamImpact'])).';';
		}
		if ($tempPricesrule->type == pricesrules_type::LENGTH) {
			$params .= $_POST['pricesruleParamMin'].';';
			$params .= $_POST['pricesruleParamMax'].';';
			$params .= intval(isset($_POST['pricesruleLengthParamImpact'])).';';
			$params .= $_POST['pricesruleParamLengthTimeslot'].';';
			$params .= $_POST['pricesruleParamOneDay'].';';
		}
		if ($tempPricesrule->type == pricesrules_type::DELAY) {
			$params .= $_POST['pricesruleParamMinDelay'].';';
			$params .= $_POST['pricesruleParamMaxDelay'].';';
		}
		if ($tempPricesrule->type == pricesrules_type::QTY) {
			$params .= $_POST['pricesruleParamMinQty'].';';
			$params .= $_POST['pricesruleParamMaxQty'].';';
			$params .= intval(isset($_POST['pricesruleQtyParamImpact'])).';';
			$params .= intval(isset($_POST['pricesruleQtyParamSameProd'])).';';
			$params .= intval(isset($_POST['pricesruleQtyParamSameComb'])).';';
			$params .= intval(isset($_POST['pricesruleQtyParamSamePeriod'])).';';
		}
		if ($tempPricesrule->type == pricesrules_type::HISTORY) {
			$params .= $_POST['pricesruleParamMinTotal'].';';
			$params .= $_POST['pricesruleParamMaxTotal'].';';
			$params .= intval(isset($_POST['pricesruleQtyParamImpact'])).';';
			$params .= intval(isset($_POST['pricesruleQtyParamSameProd'])).';';
			$params .= intval(isset($_POST['pricesruleQtyParamSameComb'])).';';
			$params .= intval(isset($_POST['pricesruleQtyParamSameAttendee'])).';';
		}
		if ($tempPricesrule->type == pricesrules_type::RECURRING) {
			$params .= $_POST['pricesruleParamTimeslot'].';';
			foreach (myOwnLang::getDaysNames($cookie->id_lang) as $dayId=>$dayName) {
				$params .= (isset($_POST['pricesruleParamDay'.$dayId]) && $_POST['pricesruleParamDay'.$dayId]==1).';';
			}
			$weeks=array();
			$weeksTab = explode(',', Tools::getValue('pricesruleParamWeeks'));
			foreach ($weeksTab as $week)
				if ((int)$week>0 && (int)$week<53 && !in_array((int)$week, $weeks))
					if ($weeks!='') $weeks[]=(int)$week;
			$params .= implode(',', $weeks);
			
		}
		$backColor=Tools::getValue("pricesruleBackColor");
		$textColor=Tools::getValue("pricesruleTextColor");
		if ($backColor!='' or $textColor!='') $tempPricesrule->style="color:$textColor;background-color:$backColor";
		$tempPricesrule->params = $params;
		$idPricesrule=0;
		if (isset($_POST['idPricesrule'])) $idPricesrule=intval($_POST['idPricesrule']);
		$tempPricesrule->sqlId = $idPricesrule;
		return $tempPricesrule;
	}

	
	public function getStyles($id_family, $productId=0) {
		$stylesHtml = '<style type="text/css">
			';
			$backColor='';
			$textColor='';
		foreach($this->list as $priceRule)
			if ($priceRule->enable)
				if ( $priceRule->doesApplyToProduct($id_family, $productId) ) {
					//
					$styles=explode(';', $priceRule->style);
			        foreach($styles as $styles) {
				        $keyval=explode(':', $styles);
				        if (array_key_exists(1, $keyval) && $keyval[0]=='background-color') $backColor=strtoupper(trim($keyval[1]));
				        if (array_key_exists(1, $keyval) && $keyval[0]=='color') $textColor=strtoupper(trim($keyval[1]));
			        }
			        //$low = MyOwnReservationsUtils::adjustBrightness($hex, $percent)
			        $low = $backColor;
			        $low = MyOwnReservationsUtils::adjustBrightness($backColor, -15);
			        $high = MyOwnReservationsUtils::adjustBrightness($backColor, 15);
					$stylesHtml .= '
					table.myOwnCalendarLine div.priceRule'.$priceRule->sqlId.', #potentialRules .priceRule'.$priceRule->sqlId.' {
						'.($textColor != '' ? 'color: '.$textColor.' !important;' : '').'
						font-weight: bold;
						'.($backColor != '' ? '
						background: '.$backColor.' !important;
						background: -webkit-gradient(linear, left top, left bottom, from('.$high.'), to('.$low.')) !important;
						background: -moz-linear-gradient(top, '.$high.', '.$low.') !important;
						filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr=\''.$high.'\', endColorstr=\''.$low.'\') !important; 
						border-top: 1px solid '.$high.' !important;
						border-left: 1px solid '.$high.' !important;
						border-bottom: 1px solid '.$low.' !important;
						border-right: 1px solid '.$low.' !important;' : '').'
					}';
				}
		$stylesHtml .= '
		</style>';
		return $stylesHtml;
	}
	
}


?>