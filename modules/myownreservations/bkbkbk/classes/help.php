<?php
/*
* 2010-2012 LaBulle All right reserved
*/
/*
require_once('config/config.inc.php');
require_once('init.php');
require_once('modules/myownreservations/myownreservations.php');
require_once('modules/myownreservations/classes/lang.php');


$obj = new myownreservations();
//$cookie=$obj->getContext()->cookie;
//$cart->id_lang = intval($cookie->id_lang);
//$id_lang = $obj->getContext()->cookie->id_lang;
//$iso = Tools::strtolower(Language::getIsoById($id_lang));
myOwnHelp::translate($obj, 'fr');
myOwnLang::translate($obj);
echo myOwnHelp::format($obj);
die();
*/
class myOwnHelp {
	
	public static $tabs;
	public static $tabNames;
	public static $sections=array();
	public static $prodFamilySectionTypes;
	public static $optionNames;
	public static $options;
	public static $extras;
	
	public static $labels;
	public static $objects;
	public static $chaptersObj;
	public static $chaptersDesc;

	public static $tuto;
	public static $settings;

	public static $admin;
	public static $adminSectionNames;
	public static $adminOptions;
	
	public static $chapters;
	public static $sectionNames;
	public static $sectionContent;
	
	function listFolderFiles($dir, $ext='', $help) {
		$out='<ul>';
	    $ffs = scandir($dir);
	    foreach($ffs as $ff){
	    	$name=str_ireplace($ext, '', $ff);
	        if($ff != '.' && $ff != '..'){
	            if(is_dir($dir.'/'.$ff)) $out.= '<li><strong>'.$ff.'</strong>'.self::listFolderFiles($dir.'/'.$ff, $ext, $help).'</li>';
	            else if ($ext=='' || stripos($ff, $ext)!==false) $out.='<li>'.(array_key_exists($name, $help) ? '<b>'.$pref.$ff.'</b><br/><i>'.$help[$name].'</i>' : $pref.$ff).'</li>';
	        }
	    }
	    return  $out.'</ul>';
	}

	public static function printStyle($full) {
		$out='
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js" type="text/javascript"></script>
		<script src="../views/js/jquery.easing.1.3.js" type="text/javascript"></script>
		
		<style type="text/css">
			body {
				font-family: \'Lato\', sans-serif;
				font-size: 14px;
				color:rgb(51, 51, 51);
				background-color: rgb(93, 93, 93);
				/*background-color: rgb(241, 241, 241);
background-image: url(http://labulle.net/wp-content/themes/pixelpress-child/img/squairy_light.png);*/
			}
			h1 {
				
				margin-left:10px;
				color:rgb(34, 34, 34);
			}
			h2 {
				padding-top:0px;
				
				border-bottom: 1px dotted #e6e6e6;
			}
			a {
				color: rgb(50, 102, 147);
				text-decoration: none;
			}
			li {
				margin-top: 5px;
			}
			h3 a {
				color:#FFF;
				text-decoration: none;
			}
			.chapter_header {
				margin-left:200px;
			}
			.chapter {
				margin-left:200px;
				    margin-right: 20px;
			}
			.section {
				background-color:#FFF;
				padding:10px;
				margin-bottom:20px;
				border: 1px solid #e6e6e6;
    border-radius: 5px;
    -webkit-box-shadow: 0 -1px 0 #d5d5d5, 0 1px 0 #d5d5d5;
    box-shadow: 0 -1px 0 #d5d5d5, 0 1px 0 #d5d5d5;
			}
			.chapter img {
				max-width: 100%;
				height: auto;
				padding: 1em;
			    -webkit-box-sizing: border-box;
			    -moz-box-sizing: border-box;
			    box-sizing: border-box;
			    -webkit-border-radius: 3px;
			    border-radius: 3px;
			    background-color:#FFF;
			    margin:20px 0px;
			    box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.085);
			    -webkit-box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.085);
			}
			.help-chapter {
				float:left;margin:20px; width:300px;
			}
			.help-chapters-container {
				width:100%;
				text-align:center;
				margin-left:50px;
			}
			.help-chapters {
				width:400px;
				text-align:left;
				background-color:rgb(93, 93, 93);
				padding:30px 50px;
				margin-left: auto;
				margin-right: auto;
			}
			@media (min-width: 800px) {
				.help-chapters {
					width:700px;
				}
				.help-chapter {
					 height:360px
				}
			}
			@media (min-width: 1200px) {
				.help-chapters {
					width:1200px;
				}
				.help-chapter {
					 /*height:auto*/
					 height:360px;
				}
			}
			
			.help-chapter-header {
				    border-bottom: 2px solid #dfe4e6;
				    padding-bottom:5px;
				    color:#FFF;
			}
			.help-chapter-header h3 {
				display:inline-block;
				line-height: 20px;
				margin:0px
			}
			.help-list a {
				color:#AAA;
			}
			.help-list li {
				border-bottom: 1px solid #AAA;
			    margin: 0 0 8px;
			    padding: 0 0 8px;
			    list-style: none;
			    color:#AAA;
	    	}
	    	body{margin:0px; padding:0px;}
			#fl_menu {
				position:absolute;
				background:#FFF;
				z-index:9999;
				width:165px;
				margin-left:10px;
				margin-top:10px;
				padding: 0.5em;
				border: 1px solid #e6e6e6;
				border-radius:5px;
				-webkit-box-shadow: 0 -1px 0 #d5d5d5, 0 1px 0 #d5d5d5;
				box-shadow: 0 -1px 0 #d5d5d5, 0 1px 0 #d5d5d5;
			}
			#fl_menu ul {
			    padding-left: 0px;
			}
			#fl_menu ul li{
				list-style: decimal;
				list-style-position: inside !important;
				padding: .5em;
				border-bottom: 1px dotted #e6e6e6;
			}
			.section_link {
				float: right;
				font-size: 12px;
	    	}
	    	.breadcrumb {
	    		padding:10px;
	    		color:#222;
	    	}
		</style>
		
		<script type="text/javascript">
 
			//cache vars
			$fl_menu=$("#fl_menu");
			$fl_menu_menu=$("#fl_menu .menu");
			$fl_menu_label=$("#fl_menu .label");
			 
			$( document ).ready(function() {
				if ($(\'#fl_menu\').length) {
				    menuPosition=$(\'#fl_menu\').position().top;
				    FloatMenu();
			   } 
			});
			 
			$(window).scroll(function () {
			    if ($(\'#fl_menu\').length)
			    	FloatMenu();
			});
			 
			function FloatMenu(){
				var height=$(window).height();
				var scroll=$(document).scrollTop();
				var newPosition=scroll;
			
			    if (menuPosition<scroll)
			    	$("#fl_menu").stop().animate({top: newPosition}, 0, "easeOutQuint");
			}
		</script>';
		$out.='
		<div style="padding:20px 30px;background:#FFF">
			<img src="http://labulle.net/wp-content/themes/pixelpress-child/img/'.(!$full ? 'logo-footer.png' : 'logo-header.png').'" alt="laBulle">
		</div>
		<div style="margin: 0 0 10px 0;height:30px;background:transparent url(http://labulle.fr/wp-content/themes/pixelpress-child/img/shadows.png) no-repeat center bottom;;"></div>';
		return $out;
	}
	
	public static function printFull($obj, $iso) {
		self::translate($obj, $iso);
		$out = myOwnHelp::printStyle();
		$out = myOwnHelp::printHeader($obj, true);
		$out .= myOwnHelp::format($obj, $iso, '', true);
		foreach(self::$chapters as $chapterKey => $chapterVal)
			$out .= myOwnHelp::format($obj, $iso, $chapterKey, false);
			
		$out .= myOwnHelp::printFooter($obj);
		return $out;
	}
	
	public static function printChapter($obj, $iso, $chapter) {
		self::translate($obj, $iso);
		$out = myOwnHelp::printStyle();
		$out = myOwnHelp::printHeader($obj, ($chapter==''));
		$out .= myOwnHelp::format($obj, $iso, $chapter, true);
		$out .= myOwnHelp::printFooter($obj);
		return $out;
	}
	
	
	public static function printHeader($obj, $full) {
		
		
		$isproversion=is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");

		
		if ($full) 
			$out.='
		<div style="float:left;width:50%;text-align:right">
			  <img width="250" height="250" src="http://labulle.net/wp-content/uploads/2015/10/myOwnReservations-250x.png" class="attachment-full wp-post-image" alt=""><br/><br/><br/><br/><br/><br/>
		</div>
		<div style="float:left;width:50%">
			  <h1 style="padding-top:60px;font-weight: 700;color: #FFF;font-size: 2.5em;">'.lcfirst($obj->displayName).'</h1>	
			  <h2 style="padding-top:0px;margin-left: 10px;border-bottom:none;font-size: 28px;color:#AAA">'.self::$labels['doc'].'</h2>
		</div>	
		<div style="clear:both;"></div>';
		return $out;
	}
	
	public static function printFooter($obj) {
		$out.='<div id="copyright" style="color: rgb(138, 138, 138);display: block;font-family: Armata, sans-serif;font-size: 12px;padding:20px">
								<a href="http://labulle.fr" title="logistic optimisation for e-commerce">
			    	<img src="http://labulle.fr/wp-content/themes/pixelpress-child/img/logo-footer.png" width="176" height="37" alt="laBulle">
			    </a>
			    					        <div class="textwidget">Nos modules e-commerce vous permettent d’organiser la distribution de vos produits, et d’optimiser celle-ci dans le temps et l’espace. Que vous vendiez des produits materiels, consommables ou immatériels, nos solutions sont là pour optimiser votre activité en ligne tout en améliorant l’experience d’achat de vos clients.</div>
							</div>';
		return $out;
	}

	public static function format($obj, $iso, $chapter, $header=true) {
		
		$isproversion=is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");



		/*--------------------------------------------------------------------------------------------------------
															chapters
		--------------------------------------------------------------------------------------------------------*/
		if ($chapter=='') {
			$out.='<div class="help-chapters-container"><div class="help-chapters">';
			foreach(self::$chapters as $chapterKey => $chapterVal) {
				$out.='<div class="help-chapter">
							<div class="help-chapter-header">
								<i class="fa fa-folder-o fa-2x" style="line-height: 30px;"></i>
								<h3 style=""><a href="'.($chapter=='' ? $chapter.myOwnReservationsController::getAjaxUrl('action=printHelp').'&chapter='.$chapterKey : '#'.$chapterKey).'">'.$chapterVal.'</a></h3>
							</div>
							<ul class="help-list">';
				if ($chapterKey=='config')
					foreach(self::$chaptersObj['config'] as $objectKey => $objectVal) {
						$out.='<li><i class="fa fa-file-text-o fa-1x" style="line-height: 26px;padding-right:5px"></i><a href="'.($chapter=='' ? myOwnReservationsController::getAjaxUrl('action=printHelp').'&chapter='.$chapterKey : '').'#'.$objectKey.'">'.$objectVal.'</a></li>';
					}
				else if ($chapterKey=='back')
					foreach(myOwnHelp::$tabs as $tabKey => $tabVal) {
						$out.='<li><i class="fa fa-file-text-o fa-1x" style="line-height: 26px;padding-right:5px"></i>'.myOwnHelp::$tabNames[$tabKey].'</li>';
					}
				else
					foreach(self::$sectionNames[$chapterKey] as $adminTabKey => $adminTabVal) {
						$out.='<li><i class="fa fa-file-text-o fa-1x" style="line-height: 26px;padding-right:5px"></i><a href="'.($chapter=='' ? myOwnReservationsController::getAjaxUrl('action=printHelp').'&chapter='.$chapterKey : '').'#'.$adminTabKey.'">'.$adminTabVal.'</a></li>';
					}
				$out.='
							</ul>
						</div>
						';
			}
			$out.='<div style="clear:both"></div>
			</div></div>';
		
		} else  {
			/*--------------------------------------------------------------------------------------------------------
															chapter mneu
			--------------------------------------------------------------------------------------------------------*/
			if ($header)
			$out.='
			<div style="">
				<div style="float:left;width:50%;text-align:right">
					<img style="margin:10px" src="../modules/myownreservations/img/logo.png">

				</div>
				<div style="float:left;width:50%;text-align:left">
					<br/><span style="margin-left:10px;margin-bottom:4px;color: #FFF;font-size:20px">'.lcfirst($obj->displayName).'</span>
					<div style="margin-left:10px;"><a href="'.myOwnReservationsController::getAjaxUrl('action=printHelp').'" style="color:#AAA">'.self::$labels['doc'].'</a></div>
				</div>
			</div>
			<div style="clear:both;"></div>';
			
			$out.='
			<div style="background-color:rgb(93, 93, 93);padding:10px;padding-bottom:30px;margin:20px 0px;color:#FFF">
				<h1 id="doc-title" style="color:#FFF;font-weight:500"><i class="fa fa-folder-o" style="line-height: 30px;"></i> '.self::$chapters[$chapter].'</h1>
				<span style="margin-left:50px">'.self::$chaptersDesc[$chapter].'</span>
			</div>';
			
			if ($header) {
				$out.='
			<div id="fl_menu">
			    <ul class="menu">';
			
			if ($chapter=='config')
				foreach(self::$chaptersObj['config'] as $objectKey => $objectVal)
					$out.='
			        <li>
			        	<a href="#'.$objectKey.'" class="menu_item">'.$objectVal.'</a>
			        </li>';
			else
				foreach(self::$sectionNames[$chapter] as $objectKey => $objectVal)
					$out.='
			        <li>
			        	<a href="#'.$objectKey.'" class="menu_item">'.$objectVal.'</a>
			        </li>';
			
			$out.='
			    </ul>
			</div>
			<div class="chapter_header"></div>';
			}
		}
		
		
		$out.='<div class="chapter">';

		/*--------------------------------------------------------------------------------------------------------
														chapter config
		--------------------------------------------------------------------------------------------------------*/
		if ($chapter=='config') {
			foreach(self::$chaptersObj['config'] as $objectKey => $objectVal) {
				$out.='<div class="section">';
				$out.='<h2 id="'.$objectKey.'"><i class="fa fa-file-text-o" style="line-height: 26px;padding-right:5px"></i> '.$objectVal.' <a href="#doc-title" class="section_link" title="Back to top">↑ '.self::$optionNames['backtotop'].'</a></h2>';
				if (array_key_exists($objectKey, self::$tabs)) { //property_exists('myOwnHelp', $objectKey.'Tabs')) {
					if (array_key_exists($objectKey, self::$objects) && array_key_exists('help', self::$objects[$objectKey]))
						$out.=str_ireplace('\n', '<br/>', self::$objects[$objectKey]['help']);
								
					//-------section-------
					foreach(self::$tabs[$objectKey] as $tabKey => $tabVal) { 
						$out.='<h3>'.(array_key_exists($objectKey, myOwnHelp::$tabNames) && array_key_exists($tabKey, myOwnHelp::$tabNames[$objectKey]) ? myOwnHelp::$tabNames[$objectKey][$tabKey] : $tabKey) .'</h3>'.$tabVal;
						
						if (array_key_exists($tabKey, myOwnHelp::$sections) ) {
							foreach(myOwnHelp::$sections[$tabKey] as $sectionKey => $sectionval) {
								if ($sectionKey!='img' && $sectionKey!='img_pro')
									$out.='<h4>'.(array_key_exists($sectionKey, myOwnHelp::$optionNames) ? myOwnHelp::$optionNames[$sectionKey] : $sectionKey).'</h4>'.$sectionval;
								else if (($sectionKey=='img_pro' && $isproversion) || ($sectionKey=='img' && (!$isproversion || !array_key_exists('img_pro', myOwnHelp::$sections[$tabKey])))) $out.='<img src="'.$sectionval.'">';
								//-------options-------
								if (array_key_exists($tabKey, self::$options) && array_key_exists($sectionKey, self::$options[$tabKey])) {
									$out.='<ul>';
									foreach (self::$options[$tabKey][$sectionKey] as $optionKey => $optionValue) {
	
										if (array_key_exists($sectionKey, self::$prodFamilySectionTypes) && array_key_exists($optionKey, self::$prodFamilySectionTypes[$sectionKey]))
											$out.= '<li><label style="width:100px"><b>'.self::$prodFamilySectionTypes[$sectionKey][$optionKey].'</b></label>';
										else if (array_key_exists($optionKey, self::$optionNames))
											$out.= '<li><label style="width:100px"><b>'.myOwnHelp::$optionNames[$optionKey].'</b></label>';
										else $out.= '<li><label style="width:100px"><b>'.$optionKey.'</b></label>';
	
										$out.= '<br/>'.$optionValue;
										//-------extra options-------;
										if (array_key_exists($tabKey, self::$extras) && array_key_exists($sectionKey, self::$extras[$tabKey]) && array_key_exists($optionKey, self::$extras[$tabKey][$sectionKey])) 
											$out.= '<br/>'.self::$extras[$tabKey][$sectionKey][$optionKey];
										$out.= '</li>';
									}
									$out.='</ul>';
								}
							}
						}
					}
				} else if (array_key_exists($objectKey, myOwnHelp::$objects) ) {
								$objectOption=self::$objects[$objectKey];
								$out.=str_ireplace('\n', '<br/>', $objectOption['help']);
								if (array_key_exists('img_list', $objectOption)) {
									if (array_key_exists('img_list_pro', $objectOption)) 
										$out.='<img src="'.$objectOption['img_list_pro'].'">';
									else $out.='<img src="'.$objectOption['img_list'].'">';
								}
								if (array_key_exists('doc', $objectOption))
									$out.='<br/><br/>'.$objectOption['doc'];
	
								if (array_key_exists($objectKey, self::$options)) {
									$out.='<h4>'.myOwnHelp::$optionNames['types'].'</h4>';
									$out.='<ul>';
									foreach (self::$options[$objectKey] as $optionKey => $optionValue) {
	
										if (array_key_exists($objectKey, self::$prodFamilySectionTypes) && array_key_exists($optionKey, self::$prodFamilySectionTypes[$objectKey]))
											$out.= '<li><label style="width:100px"><b>'.self::$prodFamilySectionTypes[$objectKey][$optionKey].'</b></label>';
										else if (array_key_exists($optionKey, self::$optionNames))
											$out.= '<li><label style="width:100px"><b>'.myOwnHelp::$optionNames[$optionKey].'</b></label>';
										else $out.= '<li><label style="width:100px"><b>'.$optionKey.'</b></label>';
	
										$out.= '<br/>'.$optionValue;
										//-------extra options-------;
										if (array_key_exists($tabKey, self::$extras) && array_key_exists($sectionKey, self::$extras[$tabKey]) && array_key_exists($optionKey, self::$extras[$tabKey][$sectionKey])) 
											$out.= '<br/>'.self::$extras[$tabKey][$sectionKey][$optionKey];
										$out.= '</li>';
									}
									$out.='</ul>';
								}
								
								if (array_key_exists('img_one', $objectOption)) 
									$out.='<img src="'.$objectOption['img_one'].'" >';
									
								$out.='<h4>'.self::$adminSectionNames['details'].'</h4>';
								$out.='<ul>';
								foreach ($objectOption as $optionKey => $optionValue) {
									if ($optionKey!='help' && $optionKey!='doc' && $optionKey!='img_list' && $optionKey!='img_list_pro' && $optionKey!='img_one') {
										if (array_key_exists($optionKey, self::$optionNames))
											$out.= '<li><label style="width:100px"><b>'.myOwnHelp::$optionNames[$optionKey].'</b></label>';
										else $out.= '<li><label style="width:100px"><b>'.$optionKey.'</b></label>';
	
										$out.= '<br/>'.str_ireplace('\n', '<br/>', $optionValue);
										$out.= '</li>';
									}
								}
								$out.='</ul>';		
				}
				$out.='</div>';
			}
		}
		
		/*--------------------------------------------------------------------------------------------------------
														chapter config
		--------------------------------------------------------------------------------------------------------*/
		if ($chapter!='config' && $chapter!='') {

			foreach(self::$sections[$chapter] as $sectionKey => $sectionVal) {
				$out.='<div class="section">';
				$out.='<h2 id="'.$sectionKey.'"><i class="fa fa-file-text-o" style="line-height: 26px;padding-right:5px"></i> '.myOwnHelp::$sectionNames[$chapter][$sectionKey].' <a href="#doc-title" class="section_link" title="Back to top">↑ '.self::$optionNames['backtotop'].'</a></h2>'.$sectionVal;
				
				if (strtolower($sectionKey)=='classes' || strtolower($sectionKey)=='templates') {
					//$out.='<ul>';
					$ext = (strtolower($sectionKey)=='classes' ? '.php' : '.tpl');
					$out.=self::listFolderFiles(MYOWNRES_PATH .(strtolower($sectionKey)=='templates' ? 'views/' : ''). strtolower($sectionKey), $ext, self::$sectionContent[$chapter][strtolower($sectionKey)]);
					/*foreach (scandir(MYOWNRES_PATH .(strtolower($sectionKey)=='templates' ? 'views/' : ''). strtolower($sectionKey)) as $filename) {
						
						if (stripos($filename, $ext)!==false) {
							$file = str_ireplace('.tpl', '', str_ireplace('.php', '', basename($filename)));
						//$out.=$file;
							if (array_key_exists($file, self::$sectionContent[$chapter][strtolower($sectionKey)]) )
								$out.= '<li><label style="width:100px;display:block"><b>'.$file.'</b></label> '.self::$sectionContent[$chapter][strtolower($sectionKey)][$file].'</li>';
						}
					}*/
					//$out.='</ul>';
				} else if (array_key_exists($sectionKey, myOwnHelp::$sectionContent[$chapter]) ) {
					foreach(myOwnHelp::$sectionContent[$chapter][$sectionKey] as $sectionContentKey => $sectionContentVal) {
						if ($isproversion || $sectionContentKey!='pro') {
							if ($sectionContentKey=='help')
								$out.='<br/>'.str_ireplace('\n', '<br/>', $sectionContentVal);
							else if ($sectionContentKey!='img' && $sectionContentKey!='pro')
								$out.='<h3>'.myOwnHelp::$adminSectionNames[$sectionContentKey].'</h3>'.str_ireplace('\n', '<br/>', $sectionContentVal);
							else if ($sectionContentKey!='pro') $out.='<br/><img src="'.$sectionContentVal.'">';

							//-------options-------
							if (array_key_exists($sectionKey, self::$adminOptions) && array_key_exists($sectionContentKey, self::$adminOptions[$sectionKey])) {
								if ($isproversion && array_key_exists('img_pro', self::$adminOptions[$sectionKey][$sectionContentKey])) 
									$out.='<br/><img src="'.self::$adminOptions[$sectionKey][$sectionContentKey]['img_pro'].'">';
								else if (array_key_exists('img', self::$adminOptions[$sectionKey][$sectionContentKey])) 
									$out.='<br/><img src="'.self::$adminOptions[$sectionKey][$sectionContentKey]['img'].'">';
				
								$out.='<ul '.($sectionContentKey=='numbered' ? 'style="list-style: decimal"' : '').'>';
								foreach (self::$adminOptions[$sectionKey][$sectionContentKey] as $optionKey => $optionValue) {
									if ($optionKey!='img' && $optionKey!='img_pro' && ($optionKey!='pro' || $isproversion)) {
										if (array_key_exists($sectionContentKey, self::$prodFamilySectionTypes) && array_key_exists($optionKey, self::$prodFamilySectionTypes[$sectionContentKey]))
											$out.= '<li><label style="width:100px"><b>'.self::$prodFamilySectionTypes[$sectionContentKey][$optionKey].'</b></label>';
										else if (array_key_exists($optionKey, self::$adminSectionNames))
											$out.= '<li><label style="width:100px"><b>'.myOwnHelp::$adminSectionNames[$optionKey].'</b></label>';
										else if (!is_int($optionKey) && $optionKey!='pro')
											$out.= '<li><label style="width:100px"><b>'.$optionKey.'</b></label>';
										
										if ($sectionContentKey=='numbered')
											$out.= '<li>'.$optionValue.'</li>';
										else
											$out.= ($optionKey!='pro' ? '<br/>' : '').str_ireplace('\n', '<br/>', $optionValue);
									}
									//-------extra options-------;
									if (array_key_exists($sectionKey, self::$extras) && array_key_exists($sectionContentKey, self::$extras[$sectionKey]) && array_key_exists($optionKey, self::$extras[$sectionKey][$sectionContentKey])) 
										$out.= '<br/>'.self::$extras[$sectionKey][$sectionContentKey][$optionKey];
									$out.= '</li>';
								}
								$out.='</ul>';
							}
						}
					}
				}
				$out.='</div>';
			}
		}
		$out.='</div>
		
		';
		return $out;
	}
	
	
	//Translation
	//-----------------------------------------------------------------------------------------------------------------
	
	public static function translate($obj, $iso) {
		$isproversion=is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");
		//-------------------- labels --------------------
		if ($iso == 'fr') {
			self::$labels = array(
				'help'					=> 'Aide',
				'editPlanningTab'				=> 'Planification',
				'editTimeslotsTab'				=> 'Créneaux horaires',
				'editPriceTab'					=> 'Paiement',
				'editNotifTab'					=> 'Notification',
				'doc'							=> 'Documentation d\'utilisation'
			);
		} else {
			self::$labels = array(
				'help'					=> 'Help',
				'editPlanningTab'				=> 'Planification',
				'editTimeslotsTab'				=> 'Time slots',
				'editPriceTab'					=> 'Payment',
				'editNotifTab'					=> 'Notification',
				'doc'							=> 'User manual'
			);
			
		}
		
		if ($iso == 'fr') {
			self::$chaptersObj['config'] = array(
				'prodFamily' => 'Règles de réservation',
				'timeslot' => 'Créneaux horaires',
				'availability' => 'Disponibilités',
				'pricerules' => 'Règles de prix',
				'settings' => 'Configuration',
			);
				
			self::$chapters = array(
				'install' => 'Installation du module',
				'front' => 'Fonctionnement sur le Front',
				'config' => 'Configuration du module',
				'admin' => 'Administration des réservations',
				'usecases' => 'Cas d\'utilisation',
				'customize' => 'Personnalisation',
			);
			self::$chaptersDesc = array(
				'install' => 'L\'installation du module est simple, elle consiste à télécharger et à envoyer le zip du module contenant tous les fichiers.',
				'front' => 'Cette partite décrit le processus de commande avec réservation et notamment le fonctionnement du planning.',
				'config' => 'Pour configurer le module il faut principalement s\'occuper des règles de réservation, on peut également ajouter des indisponibilités et des règles de prix pour les cas particuliers.',
				'admin' => 'Ce chapitre détaille comment consulter les réservations et les manipuler.',
				'usecases' => 'Ce chapitre décrit la configuration de quelques cas d\'utilisation, cela vous aidera à mieux comprendre les possibilités du module.',
				'customize' => 'Voici le détail des fichiers templates et classes pour personnaliser le module.',
			);
		} else {
			self::$chaptersObj['config'] = array(
				'prodFamily' => 'Reservation Rules',
				'timeslot' => 'Time Slots',
				'availability' => 'Availabilities',
				'pricerules' => 'Price Rules',
				'settings' => 'Settings',
			);
			self::$chapters = array(
				'install' => 'Module Installation',
				'front' => 'Working on Front',
				'config' => 'Module configuration',
				'admin' => 'Reservations administration',
				'usecases' => 'Use cases',
				'customize' => 'Personalization',
			);
			self::$chaptersDesc = array(
				'install' => 'Installing the module is simple, it is to download and send the module zip file containing all files.',
				'front' => 'This section describes the control process with reservation including the planning.',
				'config' => 'To configure the module must primarily take care of the reservation rules, we can also add unavailability and pricing rules for special cases.',
				'admin' => 'This chapter explains how to view and handle bookings.',
				'usecases' => 'This chapter describes the configuration of some use cases, this will help you better understand the possibilities of the module.',
				'customize' => 'Here are the details templates and class files to customize the module',
			);
		}
		if ($obj->_extensions!=null) foreach ($obj->_extensions->getExtByType(extension_type::OBJECT) as $ext)
				self::$chaptersObj['config'][strtolower($ext->field)]=$ext->title;
		
		
		//-------------------- places --------------------
		if ($iso == 'fr') {
			self::$objects['place']['help'] = 'Les lieux seront affichés aux clients quand il choisiront une disponibilité qui est associée à un lieu. Vous pouvez indiquer une adresse pour que le lieu soit localisé.';
			self::$tabs['place'] = array(
				'editPlaceInfoTab'					=> 'Informations principales du lieu',
				'editLocationTab'				=> 'Localisez le lieu sur une carte',
				'editRestrictionTab'				=> 'Options de restriction de lieu',
			);
			self::$tabNames['place']['editPlaceInfoTab'] = 'Informations';
			self::$tabNames['place']['editLocationTab'] = 'Localisation';
			self::$tabNames['place']['editRestrictionTab'] = 'Restriction';
		} else {
			self::$objects['place']['help'] = 'The places would be displayed to the customer when they select an availability that is associated to a place. You can indicate an address for the place to be located. ';
			self::$tabs['place'] = array(
				'editPlaceInfoTab'					=> 'Principal informations of place.',
				'editLocationTab'				=> 'Localise the place on a map',
				'editRestrictionTab'				=> 'Options for restriction of place',
			);
			self::$tabNames['place']['editPlaceInfoTab'] = 'Informations';
			self::$tabNames['place']['editLocationTab'] = 'Location';
			self::$tabNames['place']['editRestrictionTab'] = 'Restriction';
		}

		if ($iso == 'fr') {
			self::$sections['editPlaceInfoTab'] = array(
				'email' 				=> 'Vous pouvez indiquer l\'adresse email du lieu, cette adresse sera utilisée pour envoyer une notification de retrait quand validé, cet email n\'est pas affiché au client',
				'sign'			=> 'Le nom d\'un lieu doit avoir une longueur inférieure à 64 caractères.',
				'description'	=> 'Vous pouvez remplir une description pour afficher plus de détails au client (ces détails sont affichés en dessous de la carte pendant la commande)'
			);
			
		} else {
			self::$sections['editPlaceInfoTab'] = array(
				'email' 				=> 'You can set the email address of the place, this email will be use to send notification of collection when validated, this email address is not displayed to the customer',
				'sign'			=> 	'cannot be empty and must be smaller than 10 chars. The name is show for place selection during shipping.',
				'description'		=> 'You can fill a description to show more place details to the customer (theses details are shown under the map during checkout)'
			);
		}
		
		//-------------------- resource --------------------
		if ($iso == 'fr') {
			self::$objects['resource']['help'] = 'Les resources permetent au module de vérifier l\'occupation d\'une resource associée à un produit. Vous pouvez indiquer des créneaux horaires et les informations sur les resources puis les associer aux produits depuis ces derniers';
			self::$tabs['resource'] = array(
				'editResourceInfoTab'			=> 'Informations principales de la ressource',
				'editTimeslotsTab'				=> 'Indiquez les créneaux horaires de la ressource',
				'editAvailabilitiesTab'			=> 'Indiquez les disponibilités de la ressource',
			);
			self::$tabNames['resource']['editResourceInfoTab'] = 'Informations';
 			//self::$tabNames['resource']['editLocationTab'] = 'Localisation';
 			self::$tabNames['resource']['editTimeslotsTab'] = 'Créneaux horaires';
			self::$tabNames['resource']['editAvailabilitiesTab'] = 'Disponibilités';
		} else {
			self::$objects['resource']['help'] = 'The places would be displayed to the customer when they select an availability that is associated to a place. You can indicate an address for the place to be located. ';
			self::$tabs['resource'] = array(
				'editResourceInfoTab'			=> 'Principal informations of resource.',
				'editTimeslotsTab'				=> 'Indicate time slots of resource.',
				'editAvailabilitiesTab'			=> 'Indicate availabilities of resource',
			);
			self::$tabNames['resource']['editResourceInfoTab'] = 'Informations';
			//self::$tabNames['resource']['editLocationTab'] = 'Location';
			self::$tabNames['resource']['editTimeslotsTab'] = 'Time Slots';
			self::$tabNames['resource']['editAvailabilitiesTab'] = 'Availabilities';
		}

		if ($iso == 'fr') {
			self::$sections['editResourceInfoTab'] = array(
				'email' 				=> 'Vous pouvez indiquer l\'adresse email du lieu, cette adresse sera utilisée pour envoyer une notification de retrait quand validé, cet email n\'est pas affiché au client',
				'alias'			=> 'Le nom d\'un lieu doit avoir une longueur inférieure à 64 caractères.',
				'description'	=> 'Vous pouvez remplir une description pour afficher plus de détails au client (ces détails sont affichés en dessous de la carte pendant la commande)'
			);
		} else {
			self::$sections['editResourceInfoTab'] = array(
				'email' 				=> 'You can set the email address of the place, this email will be use to send notification of collection when validated, this email address is not displayed to the customer',
				'alias'			=> 	'cannot be empty and must be smaller than 10 chars. The name is show for place selection during shipping.',
				'description'		=> 'You can fill a description to show more place details to the customer (theses details are shown under the map during checkout)'
			);
		}
				
		
		//-------------------- Location Tab --------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$sections['editLocationTab'] = array(
				'address' 		=> 'Indiquez l\'adresse complète et exacte du lieu et cliquer sur le bouton géocoder pour enregistrer les cordonnées du lieu qui seront utilisées pour calculer la distance entre le client et le lieu',
				'geocode'	=> 'Une fois que vous avez indiqué l\'adresse cliquez sur le bouton "geocoder" pour trouver les coordonnées GPS. La localisation obtenue ne doit pas être imprécise.',
				'coordinates'	=> 'Les coordonnées obtenue par le geocodage sont enregistrées dans ce champ, si vous avez une position plus précise vous pouvez l\'indiquer ici.'
			);
			self::$options['editLocationTab']['geocode'] = array(
				'none' 			=> 'Aucun résultat n\'a été obtenu. Veillez à ce que le code postal et la ville soit correct.',
				'inaccurate'	=> 'Indique que le résultat retourné est le centre géométrique d\'un tel résultat une ligne (par exemple, une route) ou d\'un polygone (région). Vérifiez le nom de la rue car un géocodage imprécis n\'est pas sufisant.',
				'approximate' 	=> 'Indique que le résultat retourné reflète une approximation entre deux points précis (comme les intersections). Les résultats interpolés sont généralement renvoyés lorsque le géocodage au numéro n\'est pas disponible pour une adresse de rue. Vérifiez le nom et la numéro de la rue.',
				'precise' 		=> 'La géocodage obtenu est précis.'
			);
		} else {
			self::$sections['editLocationTab'] = array(
				'address' 		=> 'Indicate the complete and exact address of the place then click on geocode button to set the location coordinates of the place that will be use to calculate distance between customer address and the place. If you cannot get the address coordinates from address with "geocode" button, you can indicates the GPS coordinates directly.',
				'geocode'	=> 'Once you have specified the address click the "geocode" to find the GPS coordinates. The location obtained must not be imprecise.',
				'coordinates'		=> 'The coordinates obtained by the geocoding are recorded in this field, if you have a more specific position you can set it here.'
			);
			self::$options['editLocationTab']['geocode'] = array(
				'none' 			=> 'No results were obtained. Make sure the postal code and the city is correct.', 
				'inaccurate' 	=> 'Indicates that the returned result is the geometric center of a result such as a polyline (for example, a street) or polygon (region). Please check street name because inaccurate geocoding is not enough.',
				'approximate' 	=> 'Indicates that the returned result reflects an approximation (usually on a road) interpolated between two precise points (such as intersections). Interpolated results are generally returned when rooftop geocodes are unavailable for a street address. Please check street number and name.',
				'precise' 		=> 'The location obtained is precise'
			);
		}
		
		
		
		
		
		
		//-------------------- prodFamily Tabs --------------------
		if ($iso == 'fr') {
			self::$tabs['prodFamily'] = array(
				'editInfoTab'					=> 'Cette section vous permet de choisir les produits et comment ils sont occupés par les réservations',
				'editPlanningTab'				=> 'Ici vous pouvez choisir l\'unité de temps que le client sélectionne et le type de planning. Vous pouvez aussi ajouter des restrictions de durée.',
				'editTimeslotsTab'				=> 'Configurer les créneaux horaires de la famille de produits',
				'editPriceTab'					=> 'Choisissez comment le prix de la réservation est calculé et les options de paiement globales.',
				'editNotifTab'					=> 'Cocher les chaines de communication disponibles pour envoyer des notifications de réservation.',
				'editOptionsTab'				=> 'Cochez les options que le client devra sélectionner pour faire réservation.'
			);
		} else {
			self::$tabs['prodFamily'] = array(
				'editInfoTab'					=> 'This section let you select products and how they are occupied by reservations.',
				'editPlanningTab'				=> 'Here you can select the time unit of customer selection and kind of planning. You can also set length restrictions.',
				'editTimeslotsTab'				=> 'Configure time slots of the product family.',
				'editPriceTab'					=> 'Choose the way reservation price is calculated and global payment options.',
				'editNotifTab'					=> 'Check available communication channel to send notification of reservations.',
				'editOptionsTab'				=> 'Check the options that the user will have to select for a reservation.'
			);
		}
		
			//-------------------- prodFamily Tab Names --------------------
		if ($iso == 'fr') {
			self::$tabNames['prodFamily'] = array(
				'editInfoTab'					=> 'Informations',
				'editPlanningTab'				=> 'Planification',
				'editTimeslotsTab'				=> 'Créneaux horaires',
				'editPriceTab'					=> 'Paiement',
				'editNotifTab'					=> 'Notification'
			);
		} else {
			self::$tabNames['prodFamily'] = array(
				'editInfoTab'					=> 'Informations',
				'editPlanningTab'				=> 'Planification',
				'editTimeslotsTab'				=> 'Time slots',
				'editPriceTab'					=> 'Payment',
				'editNotifTab'					=> 'Notification'
			);
		}
		
		//-------------------- sections Sections --------------------
		if ($iso == 'fr') {
			self::$optionNames = array(
				'backtotop'		=> 'Revenir en haut',
				'types'			=> 'Types',
				'details'		=> 'Détails',
				'products'		=> 'Produits',
				'product'		=> 'Produit',
				'occupation'	=> 'Occupation',
				'quantity'		=> 'Quantité',
				'unit'			=> 'Unité',
				'planning'		=> 'Planning',
				'length'		=> 'Durée',
				'price'			=> 'Prix',
				'payment'		=> 'Paiement',
				'start'			=> 'Début',
				'period'		=> 'Période',
				'validation'	=> 'Validation',
				'name'			=> 'Nom',
				'admin'			=> 'Administration',
				'category'		=> 'Catégorie',
				'timeslot'		=> 'Créneau Horaire',
				'days'			=> 'Jours',
				'times'			=> 'Heures',
				'type'			=> 'Type',
				'date'			=> 'Date',
				'qty'			=> 'Quantité',
				'enable'		=> 'Activé',
				'visible'		=> 'Visible',
				'impact'		=> 'Impact',
				'ratio'			=> 'Ratio',
				'amount'		=> 'Montant',
				'over'			=> 'Appliquer à toute',
				'email'			=> 'E-mail',
				'icalendar'		=> 'iCalendar',
				'rss'			=> 'Flux RSS',
				'sms'			=> 'SMS',
				'columnWidget'	=> 'Widget colonne',
				'homeWidget'	=> 'Widget accueil',
				'priceDisplay'	=> 'Affichage prix',
				'ignoreCarrier'	=> 'Ignorer Transport',
				'ignoreAddress'	=> 'Ignorer Adresse',
				'messages'		=> 'Messages',
				'import'		=> 'Import',
				'importempty'	=> 'Vider données',
				'availabilities'=> 'Disponibilités'
			);
		} else {
			self::$optionNames = array(
				'backtotop'		=> 'Back to top',
				'types'			=> 'Types',
				'details'		=> 'Details',
				'products'		=> 'Products',
				'product'		=> 'Product',
				'occupation'	=> 'Occupation',
				'quantity'		=> 'Quantity',
				'unit'			=> 'Payment',
				'planning'		=> 'Planning',
				'length'		=> 'Length',
				'price'			=> 'Price',
				'payment'		=> 'Payment',
				'start'			=> 'Start',
				'period'		=> 'Period',
				'validation'	=> 'Validation',
				'name'			=> 'Name',
				'admin'			=> 'Administration',
				'category'		=> 'Category',
				'timeslot'		=> 'Time Slot',
				'days'			=> 'Days',
				'times'			=> 'Times',
				'type'			=> 'Type',
				'date'			=> 'Date',
				'qty'			=> 'Quantity',
				'enable'		=> 'Enable',
				'visible'		=> 'Visible',
				'impact'		=> 'Impact',
				'ratio'			=> 'Ratio',
				'amount'		=> 'Amount',
				'over'			=> 'Over All',
				'email'			=> 'E-mail',
				'icalendar'		=> 'iCalendar',
				'rss'			=> 'RSS Feed',
				'sms'			=> 'SMS',
				'columnWidget'	=> 'Column widget',
				'homeWidget'	=> 'home widget',
				'priceDisplay'	=> 'Price display',
				'ignoreCarrier'	=> 'Ignore Transport',
				'ignoreAddress'	=> 'Ignore Address',
				'messages'		=> 'Messages',
				'import'		=> 'Import',
				'importempty'	=> 'Empty data',
			);
		}
		
		self::$prodFamilySectionTypes = array(
			'occupation'	=> myOwnLang::$productType,
			'quantity'		=> myOwnLang::$qtyControl,
			'unit'			=> myOwnLang::$reservationInterval,
			'length'		=> myOwnLang::$lengthControl,
			'availability'	=> myOwnLang::$availabilityTypes,
			'pricerules'	=> myOwnLang::$priceruleTypes,
		);
		
		//-------------------- InfoTab Sections --------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$sections['editInfoTab'] = array(
				'img'					=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-config-reservation-info1.png',
				'img_pro'					=> 'http://labulle.fr/wp-content/uploads/2015/06/myownrpro-config-regle.png',
				'products' 				=> 'Choisissez les produits qui deviendront automatiquement reservable par les clients. Vous pouvez choisir plusieurs produits (en pressant la touche\'ctrl\') et ou catégories. Attention les catégories filles sont inclues et un produit ne peut appartenir à plusieurs familles.',
				'occupation'			=> 'Avec les réservations il n\'y a pas de mouvement de stock à la commande. (un produit loué reste en stock). Alors indiquez au module comment prendre la quantité de produit et comment ils sont occupés par les réservations.',
				'quantity'				=> 'Vous pouvez choisir d\'autres moyens de prendre la quantité de la réservation plutôt que le champ habituel de quantité du produit sur le front.',
				'admin'				=> 'En sélectionnant des utilisateurs du back-office vous pouvez restreindre leur vue du planning à cette famille de produit. En permettant l\'assignation d\'un employé vous pouvez affecter des réservations à des utilisateurs du back-office.'
			);
		} else {
			self::$sections['editInfoTab'] = array(
				'img'					=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-settings-reservation-info.png',
				'img_pro'					=> 'http://labulle.net/wp-content/uploads/2015/06/myownrpro-settings-rule.png',
				'products' 				=> 'Select products that will become automatically reservable by customers. You can select multiple products (by pressing \'ctrl\' key) and or categories. Warning children categories are included and a product cannot belong to multiple product families.',
				'occupation'			=> 'With reservations there is no stock movement at order (a rented product remains in stock). So tell the module how to get the products quantities and how they are occupied by reservations.',
				'quantity'				=> 'You can choose other way to get reservation quantity than the classic product quantity field on front.',
				'admin'					=> 'By selecting back-office users you can restrict their planning view for this product family. By assigning an employee you can assign reservations to back-office users.'
			);
		}
		
		//-------------------- PlanningTab Sections --------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$sections['editPlanningTab'] = array(
				'img'		=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-config-reservation-plannif1.png',
				'unit' 		=> 'Choisissez la période minimum que le client pourra sélectionner. Ainsi que les paramètres qui permettent de générer des périodes sélectionnables de manière récurrente.',
				'planning'	=> 'Indiquez les bornes de la fenêtre de réservation (la période pendant laquelle le client peut sélectionner une réservation) et la forme du planning.',
				'length'	=> 'En choisissant les limites de durée vous pouvez modifier la manière dont le client fait sa sélection. Attention cette durée peut prendre en compte des jours qui ne le sont pas pour le tarif.'
			);
		} else {
			self::$sections['editPlanningTab'] = array(
				'img'		=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-settings-reservation-schedule.png',
				'unit' 		=> 'Select minimum period that the customer will select. And also parameters that allow selectable period to be generated recurrently.',
				'planning'	=> 'Indicate reservation boundaries (period within customer can select a reservation) and the planning shape.',
				'length'		=> 'By selecting length limits you\'re changing the way customer make his selection. Warning this length can include days that are not included for price.'
			);
		}
		
		//-------------------- PriceTab Sections --------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$sections['editPriceTab'] = array(
				'img'			=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-config-reservation-paiement1.png',
				'price' 		=> 'Indiquez comment est calculé le tarif de base de la réservation (avant les règles de prix). Par défaut le prix du produit est multiplié par la durée de la réservation. Il est possible d\'utiliser un tableau depuis l\'administration du produit si la durée de réservation est limitée à une plage.',
				'payment'		=> 'Remplissez le montant de l\'acompte fixe ou en pourcentage par rapport au montant de la réservation. Indiquez si une caution doit être demandée et son mode de calcul. Veuillez noter que l\'avance sera de 100% si votre IP est en maintenance.',
				'validation'	=> 'Cette option permet de choisir sur quelles étapes de la commande la réservation est validée. Appuyez sur la touche "control" de votre clavier (ou "cmd" sur Mac) pour sélectionner plusieurs statuts. Une réservation qui est en attente de validation n\'est pas prise en compte dans le calcul des disponibilités. Attention en sélectionnant également le statut "en attente de paiement…" des produits peuvent être indisponibles alors que les réservations ne sont pas payées.'
			);
		} else {
			self::$sections['editPriceTab'] = array(
				'img'				=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-settings-reservation-payment.png',
				'price' 			=> 'Indicate how is calculated base price of the reservation (before price rules). By default the product price is multiplied by the duration of the reservation. It is possible to use a table from product administration if reservation duration is limited to a range.',
				'payment'			=> 'Fill in the amount of advance, fixed or in percentage of the booking amount. Specify whether a deposit is requested and how it is calculated. Please note that advance will be 100% if your IP is in maintenance.',
				'validation'		=> 'This option allows you to choose for which steps of the order the booking is validated. Press the "Control" key on your keyboard (or "cmd" on a Mac) to select multiple statuses. A reservation in awaiting validation is not taken into account in the calculation of availabilities. Warning also selecting the "pending payment ..." products may not be available when reservations are not paid.'
			);
		}
		
		//-------------------- Reservation Unit --------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$options['editPriceTab']['price'] = array(
				reservation_price::PRODUCT_DATE 	=> 'Le prix de la réservation est le prix du produit peut importe la durée de réservation.',
				reservation_price::PRODUCT_TIME		=> 'Le prix de la réservation est le prix du produit multiplié par la durée, dont vous pouvez choisir l\'unité de temps.',
				reservation_price::TABLE_TIME		=> 'Le prix de la réservation est le prix renseigné pour la durée de la réservation dans la fiche produit.',
				reservation_price::TABLE_DATE 		=> 'Le prix de la réservation est la somme des tarifs pour chaque jour/semaine de l\'année.',
				reservation_price::TABLE_RATE		=> 'Le prix de la réservation est le prix du produit multiplié par le ratio de la durée de réservation et la moyenne des ratios de la période.'
			);

		} else {
			self::$options['editPriceTab']['price'] = array(
				reservation_price::PRODUCT_DATE 	=> 'The price of the reservation is the price of the product, regardless of the length of the reservation.', 
				reservation_price::PRODUCT_TIME 	=> 'The price of the reservation is the price of the product multiplied by the duration, of which you can choose the unit of time.',
				reservation_price::TABLE_TIME 		=> 'The price of the reservation is the price indicated for the duration of the reservation in the product sheet.',
				reservation_price::TABLE_DATE 		=> 'The price of the reservation is the sum of the rates for each day / week of the year.',
				reservation_price::TABLE_RATE		=> 'The price of the reservation is the price of the product multiplied by the ratio of the reservation duration and the average of the ratios of the period.'
			);
		}
		
		//-------------------- Notifs Sections --------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$sections['editNotifTab'] = array(
				'img' 		=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-config-reservation-notif1.png',
				'email'		=> 'Indiquez un email pour recevoir une notification lors de la réservation/replanification des produits de cette famille. Vous pouvez indiquer plusieurs emails séparés par un ;',
				'icalendar' => 'Vous pouvez indiquer dans l\'url le paramètre sel pour modifier la période. Ex "-3 months" pour les réservations des 3 derniers mois, "+3 months" pour les 3 prochains mois.',
				'rss'		=> 'Un flux RSS est mis à la disposition des clients lors de la confirmation de commande. Avec un numéro de commande du client passé en reference dans le lien fourni, celui-ci reçoit toutes ces réservations à venir (et celles des autres commandes).',
				'sms'		=> 'Vous pouvez autoriser l\'envoi de SMS de rappel avant la réservation, à condition d\'avoir configuré votre compte de SMS dans la configuration du module.'
			);
		} else {
			self::$sections['editNotifTab'] = array (
				'img'		=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-settings-reservation-notif.png',
				'email' 	=>	'Set an email associated to the product family, this email will receive reservation notification. An admin account with the same email will be restricted to this product family on reservation administration. You can indicate multiple email addresses separated by a ;',
				'icalendar' => 'You can specify the url in the salt parameter to change the period. Ex "-3 months" for bookings last 3 months, "three months" for the next three months.',
				'rss'		=> 'An RSS feed is available to customers in the order confirmation. With with as reference a customer order number passed in the link provided, it receives all his upcoming reservations (and those of other commands).',
				'sms'		=> 'You can allow SMS reminder sending before the reservation, if you provided your SMS account in the module configuration.'
			);
		}
		
		//-------------------- TimeSlot Sections --------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$sections['editTimeslotsTab'] = array(
				'timeslot' 	=> 'Sélectionner les créneaux horaires communs disponibles pour la famille de produit, les créneaux désactivés sont soit attribués à une autre famille de produit soit affectés à la famille de produit courante.'
			);
		} else {
			self::$sections['editTimeslotsTab'] = array(
				'timeslot' 	=> 'Select common time slots available for product family, disabled time slots are allocated to another product family or assigned to the current product family.'
			);
		}
		
		//-------------------- Quantity Options --------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$options['editInfoTab']['quantity'] = array(
				reservation_qty::PRODUCT 		=> 'C\'est le comportment par défaut, le champ quantité de produit est utilisé comme quantité de réservation.',
				reservation_qty::FIXED			=> 'Cochez ceci pour ignorer la quantité de produit indiquée pour utiliser une valeur fixe personnalisée.',
				reservation_qty::SEARCH			=> 'Choisissez cette option si un de vos attributs contient une valeur numérique à utiliser en quantité, la quantité de produit qui reste est utilisée comme le nombre de réservations.'
			);
		}

        elseif ($iso == 'it') {
            self::$options['editInfoTab']['quantity'] = array(
                reservation_qty::PRODUCT 		=> 'Questo è il comportamento predefinito, il campo quantità prodotto viene utilizzato come quantità di prenotazione.',
                reservation_qty::FIXED			=> 'Ingora la quantità del prodotto per utilizzare una quantità fissa personalizzata.',
                reservation_qty::SEARCH			=> 'Selezionare questa opzione se uno degli attributi contiene un valore numerico da utilizzare come quantità, la quantità di prodotto rimanente viene utilizzata come numero di prenotazioni.'
            );
        }

		else {
			self::$options['editInfoTab']['quantity'] = array(
				reservation_qty::PRODUCT 		=> 'This is the default behavior, the product quantity field is used as reservation quantity.\'',
				reservation_qty::FIXED			=> 'Check this to ignore product quantity filled by customer to use a custom fixed quantity.',
				reservation_qty::SEARCH			=> 'Select this option if one of your attributes contains a numeric value to use as quantity, the remaining product quantity is used as number of reservations.'
			);
		}

		//die('---->'.$iso);
		
		//-------------------- Occupation Options --------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$options['editInfoTab']['occupation'] = array(
				product_type::PRODUCT 		=> 'C\'est l\'occupation par défaut, le produit est considéré comme un produit réel. Donc la quantité du produit détermine le nombre de réservations possibles en même temps.',
				product_type::VIRTUAL 		=> 'Le produit est considéré comme virtuel, cela veut dire que les combinaisons ne sont pas réelles comme une couleur ou une taille mais elles concernent des options ou améliorations qui n\'affectent pas l\'occupation. Dans ce cas vous avez à choisir comment prendre la quantité peut importe la combinaison choisie.',
				product_type::AVAILABLE 	=> 'Vous ne pouvez pas indiquer la quantité de produit parce que cela dépend de quand. Alors choisissez cette option pour indiquer la quantité de produits lors de la création de disponibilités pour cette famille.',
			);
			if ($isproversion) {
				self::$options['editInfoTab']['occupation'] += array(
					product_type::STOCK 		=> 'Au lieu de travailler avec une quantité, vous pouvez lister tous les produits avec un identifiant unique dans l\'onglet stock. Si aucun élément de stock n\'est trouvé alors la quantité de produit classique est utilisée.',
					product_type::MUTUEL 		=> 'Choisissez cette option seulement si chacun de vos produits peut supporter plus d\'une réservation par quantité de produit. Si la quantité de réservation est plus grande que la capacité indiquée alors un autre produit sera occupé.',
					product_type::SHARED 		=> 'Les réservations sont partagées entre les produits de la famille, cela veut dire que si un produit est occupé sur une période alors les autres produits de famille seront occupés en même temps.',
					
				);
			}
		}

		elseif ($iso == 'it') {
            self::$options['editInfoTab']['occupation'] = array(
                product_type::PRODUCT 		=> 'Questa è l\'occupazione predefinita, il prodotto è considerato come un prodotto reale. Quindi la quantità del prodotto determina la quantità di prenotazioni che possono essere effettuate contemporaneamente.',
                product_type::VIRTUAL 		=> 'Il prodotto è considerato come intangibile, il che significa che le combinazioni non sono reali come dimensioni o colore, ma le combinazioni riguardano opzioni o caratteristiche che non influenzano l\'occupazione. In questo caso devi selezionare come ottenere la quantità di prodotto qualunque combinazione selezionata.',
                product_type::AVAILABLE 	=> 'Non è possibile impostare la quantità del prodotto perché dipende da quando. Quindi imposta questa opzione per indicare la quantità del prodotto durante la creazione di disponibilità per questa famiglia.',
            );
            if ($isproversion) {
                self::$options['editInfoTab']['occupation'] += array(
                    product_type::STOCK 		=> 'Invece di lavorare con una quantità, puoi elencare tutti i prodotti con un identificativo univoco nella scheda stock. Se non viene trovato alcun elemento di stock, viene utilizzata la quantità di prodotto classica.',
                    product_type::MUTUEL 		=> 'Seleziona questa opzione solo se ognuno dei tuoi prodotti può gestire più di una prenotazione per quantità di prodotto. Se la quantità di prenotazione è superiore alla capacità specificata, verrà occupato un altro prodotto.',
                    product_type::SHARED 		=> 'Le prenotazioni sono condivise sui prodotti della famiglia, il che significa che se un prodotto è occupato in un periodo, allora altri prodotti delle famiglie sono occupati allo stesso tempo.',

                );
            }
        }

		else {
			self::$options['editInfoTab']['occupation'] = array(
				product_type::PRODUCT 		=> 'This is the default occupation, product is considered as a real product. So product quantity determines the amount of reservations that can be done at same time.',
				product_type::VIRTUAL 		=> 'The product is considered as intangible, that means that combinations are not real like size or color but combinations concern options or features that don\'t affect occupation. In this case you have to select how to get product quantity whatever combination selected.',
				product_type::AVAILABLE 	=> 'You cannot set product quantity because it depends on when. Then set this option to indicate product quantity when creating availabilities for this family.'
			);
			if ($isproversion) {
				self::$options['editInfoTab']['occupation'] += array(
					product_type::STOCK 		=> 'Instead of working with a quantity, you can list all products with an unique identifier in stock tab. If no stock element is found then classic product quantity us used.',
					product_type::MUTUEL 		=> 'Select this option only if each one of your product can handle more than one reservations per product quantity. If reservation quantity is greater than specified capacity then another product will be occupy.',
					product_type::SHARED 		=> 'Reservations are shared over products of the family, that means if a product is occupied on a period then other products of the families are occupied at same time.',
					
				);
			}
		}
		
		//-------------------- Occupation Extra Options --------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$extras['editInfoTab']['occupation'] = array(
				product_type::PRODUCT 		=> 'Achat: Vous pouvez autoriser tant la réservation que l\'achat pour le même produit. Dans ce cas vous devez aussi définir le prix unitaire du produit qui sera le prix de réservation, sinon l\'achat du produit ne sera pas permis. Si le produit a des déclinaisons leur prix de réservation sera le prix de la déclinaison divisé par le ratio (le ratio est le prix du produit divisé par le prix unitaire).',
				product_type::VIRTUAL 		=> 'Quantité: Cette option permet d\'ignorer la quantité des déclinaisons et d\'utiliser la quantité de l\'ensemble du stock du produit (en prenant la somme de chaque combinaison ou en prenant la quantité de celle par défaut). ',
				product_type::MUTUEL 		=> 'Occupation: Indiquer ici le nombre de réservations maximum pour tous les produits.',
				product_type::SHARED 		=> 'Capacité: Vous pouvez définir ici la quantité de la réservation qu\'un produit peut supporter, par défaut la capacité est de 1. Si vous définissez une capacité supérieure à 1, vous devez permettre la commande hors stock pour les produits de la famille.',
				product_type::STOCK 		=> 'Vous pouvez choisir si l\'élément de stock est à choisir côté admin par défaut ou automatiquement à la validation ou par le client lors du choix de période.',
			);
		} else {
			self::$extras['editInfoTab']['occupation'] = array(
				product_type::PRODUCT 		=> 'Purchase: You can enable both reservation and purchase for the same product. In this case you must also set the unit price of the product that will be the reservation price, otherwise the purchase of the product won\'t be enable. If the product have some variations their reservation price will be the product variation price divided by the ratio (the ratio is product price divided by the unit price).',
				product_type::VIRTUAL 		=> 'Quantity: This option allows you to ignore the amount of variations and use the amount of the entire stock of the product (by getting sum of each combination or the default combination quantity). ',
				product_type::MUTUEL 		=> 'Occupation: Set here the maximum number of reservations for all products.',
				product_type::SHARED 		=> 'Capacity: You can set here the quantity of reservation that a product can handle, by default the capacity is 1. If you set a capacity greater than 1 you must allow the order without stock to the products of the family.',
				product_type::STOCK 		=> 'You can choose whether the stock item is to be chosen from the default admin side or automatically at validation or by the customer when choosing the period.'
			);
		}
		
				
				
		//-------------------- Reservation Unit --------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$options['editPlanningTab']['unit'] = array(
				reservation_interval::TIMESLOT 	=> 'Sélectionnez des créneaux horaires globaux ou créez des créneaux horaires spécifiques depuis l\'onglet dédié',
				reservation_interval::TIMESLICE 	=> 'Au lieu de gérer des horaires spécifiques, la journée entre l\'heure de début et de fin est découpée en tranches de temps.',
				reservation_interval::DAY		=> 'Choisissez au moins un jour de la semaine disponible pour la sélection, si vous ne choisissez pas les jours de week-end, ils seront cachés sur le calendrier.',
				reservation_interval::WEEK		=> 'Choisissez le jour de la semaine qui sera utilisé pour obtenir la date de réservation de début et de fin de la semaine.',
				reservation_interval::MONTH 	=> 'Vous pouvez choisir le premier ou dernier jour de la semaine qui sera utilisé pour obtenir la date de réservation de début et de fin. Vous pouvez également choisir des jours exact du mois.'
			);
			self::$extras['editPlanningTab']['unit'] = array(
				reservation_interval::TIMESLOT 	=> 'Découper le temps: Cochez cette case pour découper les heures du jour afin d\'afficher les créneaux de manière proportionnelle à leurs durées',
				reservation_interval::MONTH 	=> 'Heures: L\'heure de début doit être ultérieure à l\'heure de fin sauf pour une sélection par nuit',
			);

		}

        elseif ($iso == 'it') {
            self::$options['editPlanningTab']['unit'] = array(
                reservation_interval::TIMESLOT 	=> 'Seleziona slot globali o crea slot specifici dalla scheda dedicata',
                reservation_interval::TIMESLICE 	=> 'Invece di gestire programmi specifici, il giorno tra l\'inizio e l\'ora di fine è suddiviso in intervalli di tempo.',
                reservation_interval::DAY		=> 'Scegli almeno un giorno della settimana disponibile per la selezione, se non scegli i giorni del fine settimana saranno nascosti sul calendario.',
                reservation_interval::WEEK		=> 'Scegli il giorno della settimana che verrà utilizzato per ottenere la data di inizio e fine della prenotazione della settimana.',
                reservation_interval::MONTH 	=> ' Puoi scegliere pugno e fine giorno della settimana che verranno utilizzati per ottenere la data di inizio e fine prenotazione. Puoi anche scegliere il numero esatto del giorno del mese.'
            );
            self::$extras['editPlanningTab']['unit'] = array(
                reservation_interval::TIMESLOT 	=> 'Tagliare il tempo: selezionare per ridurre le ore del giorno in modo da visualizzare le fasce orarie in proporzione alla loro lunghezza',
                reservation_interval::MONTH 	=> 'Orari: l\'ora di inizio deve essere successiva all\'ora di fine, ad eccezione di una selezione per notte',
            );

        }

        else {
			self::$options['editPlanningTab']['unit'] = array(
                reservation_interval::TIMESLOT 	=> 'Select global slots or create specific slots from dedicated tab',
                reservation_interval::TIMESLICE 	=> 'Instead managing specific schedules, the day between start and end time is divided into time slices.',
                reservation_interval::DAY 		=> 'Choose at least one day of week available for selection, if you don\'t choose week-end days they would be hidden on calendar.',
                reservation_interval::WEEK 		=> 'Choose the day of week that will be used to get the start and end reservation date of the week.',
                reservation_interval::MONTH 	=> 'You can choose fist and end day of week that will be used to get the start and end reservation date. You can also choose the exact day number of the month.'
            );
            self::$extras['editPlanningTab']['unit'] = array(
                reservation_interval::TIMESLOT 	=> 'Cut time: Check to cut the hours of the day in order to display time slots in proportion to their length',
                reservation_interval::MONTH 	=> 'Times: The start time must be later than the end time except for one selection per night',
			);
		}
		
		
				
		//-------------------- Reservation Planning --------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$options['editPlanningTab']['planning'] = array(
				'start' 	=> 'Cette option vous permet de choisir quand les réservations démarrent.  Sélectionnez "le jour même" pour que les réservations puissent être faite le jour de la commande, dans ce cas il vous faut préciser si la réservation peut être faite pendant le créneau horaire en cours, sinon la réservation sera uniquement possible sur le créneau horaire suivant.  Sélectionnez "Après quelques jours" pour que les réservations puissent être faite un ou plusieurs jours après le jour de la commande, dans ce cas il vous faut préciser le nombre de jours de délai (1 pour le lendemain, 2 pour le sur-lendemain etc..). Sélectionnez "la semaine suivante" pour que les réservations soient uniquement possibles d\'une semaine sur l\'autre (si la semaine en cours est démarrée, la réservation sera uniquement possible à partir du début de la semaine suivante). Dans ce cas vous pouvez modifier le jour et l\'heure du démarrage de la semaine. Sélectionnez "Après quelques semaines" pour que les réservations puissent être faite une ou plusieurs semaines après le jour de la commande, dans ce cas il vous faut préciser le nombre de semaines de délai.',
				'length'	=> 'Cette option vous permet de choisir quel type de planning est affiché lors de la sélection d\'un créneau horaire lors de la commande.',
				'period' 	=> 'Cette option permet de choisir la durée de reservation en jours. Par exemple si le début de reservation est configuré à 1 jours après, le client peut sélectionner un créneau horaire pendant les 2 prochaines jours. Si le paramètre est configuré à 0, les reservations sont possibles jusqu\'à la fin de la journée de début.'
			);
		} else {
			self::$options['editPlanningTab']['planning'] = array(
				'start' 	=> 'This option allows you to choose when the reservation start. Select "the current day" so that reservation can be made the day of order, in which case you must specify whether the reservations can be made during the current slot, otherwise the reservations will only be possible on the next time slot. Select "After a few days" so that reservations can be made one or more days after the date of the order, in this case you need to specify the number of days in period (1 for next day, 2 for on-day etc. ..). Select "next week" so that reservations are only possible one week to the next (if the current week is started, the booking will only be possible from the beginning of the following week). In this case you can change the day and time of the start of the week. Select "After a few weeks" so that reservations can be made a week or more after the date of the order, in this case you must specify the number of weeks of delay.', 
				'length' 	=> 'These option let you set which planning is show during the time slot selection on checkout.',
				'period' 	=> 'This option let you chose the reservation period in days. For instance if the reservation start is set to 1 day after and the reservation period is set to 2, the customer can select a reservation during the two next days. If this parameter is set to 0, the reservations are available until the end of start day.'
			);
		}
				
		//-------------------- Reservation Length --------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$options['editPlanningTab']['length'] = array(
				reservation_length::NONE 		=> 'La réservation n\'aura pas de date de fin. La durée sera de 1 donc le prix de la réservation sera le prix du produit.',
				reservation_length::FREE 		=> 'Le client peut choisir une fin, jusqu\'à la fin de fenêtre de réservation.',
				reservation_length::LIMIT		=> 'Vous devez choisir la durée minimum et maximum de réservation (au-dessus de 0). La durée max de réservation sera limitée par la durée de la fenêtre de réservation.',
				reservation_length::FIXED		=> 'Indiquez la durée des réservations, le client n’aura qu’a sélectionner la date de début et pourra ajouter sa réservation au panier.',
				reservation_length::PRODUCT		=> 'Vous pouvez fixer un durée différente pour chaque produit depuis l\'administration de ceux-ci dans l\'onglet myOwnReservation.',
				reservation_length::OPTIONAL	=> 'Le client peut choisir un seul créneau pour le début ou la fin ou les deux, ce qui est utile pour les cas dépôt/retrait ou aller/retour.',
				9			=> 'Le client choisit le début de la réservation et la durée est fonction de la quantité du produit.'
			);
		} else {
			self::$options['editPlanningTab']['length'] = array(
				reservation_length::NONE 		=> 'The reservation will have no end date. The length will be 1 so the reservation price will be the product price.',
				reservation_length::FREE 		=> 'The customer can select a end, until the end of the reservation window.',
				reservation_length::LIMIT		=> 'You must choose the minimum and maximum reservation (above 0). The reservation duration max is limited by the length of the reservation window.',
				reservation_length::FIXED		=> 'Indicate the reservation duration , the client will only have to select the start date and will be able to add his reservation to the cart.',
				reservation_length::PRODUCT		=> 'You can set a distinct length for each product from the administration of them in myOwnReservation tab.',
				reservation_length::OPTIONAL	=> 'The customer can choose only one time for start or for end or both, this is useful for deposit/collection or going/coming selection.',
				9			=> 'The customer choose the reservation start and the length is in function of product quantity.'
			);
		}
		//-------------------- Product Extra Options --------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$options['editInfoTab']['products'] = array(
				'name' 		=> 'Ce nom n\'est pas visible par le client',
				'admin' 	=> 'Sélectionnez les utilisateurs qui auront le droit d\'afficher les réservations. Pour gérer finement les droits en modification et suppression : configurez les permissions de l\'onglet "Mes réservations".',
				'products' 	=> 'Sélectionnez plusieurs produits en appuyant sur la touche cmd ou ctrl.',
				'category' 	=> 'Sélectionnez une ou plusieurs catégories de produits. Inutile de cocher les catégories filles de votre sélection.',
			);
		} else {
			self::$options['editInfoTab']['products'] = array(
				'name' 		=> 'This name is not shown to the customer',
				'admin' 		=> 'Select the users who are allowed to view reservations. To manage finely rights on delete and modification configure the permissions for tab "My bookings".',
				'products' 		=> 'Select multiple products by pressing cmd or ctrl key.',
				'category'			=> 'Select one or more categories. Needless to check child categories your selection.'
			);
		}
				
		//-------------------- Length Extra Options --------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$extras['editPlanningTab']['length'] = array(
				'run-over' 		=> 'Cochez ces cases pour indiquer si une réservation peut courir par dessus une période de congé ou une jour/créneau désactivé. Si décoché une réservation qui commence avant l\'indisponibilité se terminera au plus tard avant cette indisponibilité.',
				'between' 		=> 'Vous pouvez définir une période creuse entre chaque réservation pour être certain que les produits seront de retour en stock avant la réservation suivante.',
				'preview' 		=> 'Si longueur par plage. Cochez cette case pour afficher sur le planning de début, un prix estimatif basé sur la durée minimum qui peut être sélectionnée par le client.',
				'frequency' 	=> 'La fréquence permet de limiter le durée à certains intervalles, par exemple pour une fréquence de 3 jours, le client pourra faire une réservation qui dure 3 jours, 6 jours, 9 jours etc...',
				'combination'	=> 'Quand la durée de réservation est fixée par produit vous pouvez chosir avec cette option de la définir par combinaison',
				'quantity'		=> 'Si la durée de réservation est fixe, vous pouvez choisir d\'utiliser la quantité pour multiplier la durée au lieux de réservations parallèles'
			);
		} else {
			self::$extras['editPlanningTab']['length'] = array(
				'run-over' 		=> 'Check the boxes to tell if a reservation cab run over unavailabilities or disabled day or time slot. If unchecked a booking that begins before an unavailability will end before this unavailability.',
				'between' 		=> 'You can set a gap period between each reservation to be sure products are back to stock before the next reservation. ',
				'preview' 		=> 'If length on a range. Check this box to display on start planning an estimated price based on minimum length that can be selected by the customer',
				'frequency' 		=> 'The frequency limits the duration at intervals, for example at a frequency of 3 days, the client may make a reservation that lasts 3 days, 6 days, 9 days etc. ..',
				'combination'	=> 'When a reservation length is fixed per product you can choose with this option to also set it depending on combination',
				'quantity'		=> 'If a reservation length is fixed, you can choose that quantity is used to multiply the length instead of parallel reservations'
			);
		}
		
		//-------------------- Price Extra Options --------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$extras['editPlanningTab']['price'] = array(
				'unit' 		=> '',
				'label' 		=> 'Remplissez le libellé qui sera affiché à droite du prix pour indiquer à quelle durée se rapporte le prix. Laissez le champ vide pour afficher l\'unité de durée choisie ci-dessus ou indiquez un espace pour ne pas afficher d\'unité de prix.',
				'strict' 		=> 'Ce paramètre va affecter la manière dont les prix sont calculés, si une réservation se termine sur un jour diffèrent, un autre jour sera compté. Par exemple si une réservation se termine le jour suivant sur un créneau horaire précédent, 2 jours seront comptés au lieu de 1.',
				'end' 		=> 'You can choose to ignore the last time slot of reservation in it length count. That means that if product price is set to time slot and reservation is not only on one time slot, then the reservation price would be the product price multiply by the number of time slots less one. If the product price is set to day and the last time slot is the first time slot of day, then the reservation price would be the product price multiply by the number of days less one.',
				'advance' 		=> 'Vous pouvez choisir de laisser le client payer seulement une partie du montant de la commande en ligne et le reste au magasin. Donner en pourcentage la valeur payée par le client, le calcul est basé sur le montant des réservations (les produits achetés, les frais de livraison et la caution ne sont pas pris en compte). Les variables suivantes sont disponibles pour vous permettre de personnaliser le template de l\'email récapitulatif de commande; total payé : {total_paid}; total commande : {total_order}; solde : {total_balance}',
				'deposit' 		=> 'Vous pouvez choisir ici le montant de la caution requise si il y a des réservations dans le panier. Le dépôt est le même pour une ou plusieurs réservations. Dans les paramètres avancés vous pouvez choisir le libellé utilisé pour faire référence à cette caution.'
				
			);
		} else {
			self::$extras['editPlanningTab']['price'] = array(
				'unit' 		=> '',
				'label' 		=> 'Fill the text to be displayed right to the price to indicate how long the price refers. Leave blank to display the time unit selected above or enter a space not to display unit prices.',
				'strict' 		=> 'This parameter will affect the way the price is calculated, if a reservation end on a different day, another day would be count. For instance if a reservation end on the next day on a previous time slot, 2 days would be count instead of 1.',
				'end' 		=> 'The frequency limits the duration at intervals, for example at a frequency of 3 days, the client may make a reservation that lasts 3 days, 6 days, 9 days etc. ..',
				'advance' 		=> 'You can choose to let the customer pay only a part of the order amount online and the rest at the store. Give a percent value of part paid by customer, the calculation is based on the reservation amount (purchased products, transport fees and deposit are not taken into account). Following variables are available to let you customize order confirmation email template; total paid : {total_paid}; total order : {total_order}; balance : {total_balance}',
				'deposit' 		=> 'You can set here the deposit amount needed if there is some reservations in cart. The deposit is the same for one or multiple reservations. In advanced params you can set the label use to refer to this deposit.'
			);
		}
		
		//------------------------------------------------------------------------------------------------------------------------
		//												TIME SLOTS
		//------------------------------------------------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$objects['timeslot'] = array (
				'help'		=> 'Un créneau horaire est une période pendant laquelle un client peut être livré. Vous pouvez créer autant de créneaux horaires que vous le souhaitez, mais l\'heure de début ou de fin d\'un créneau horaire ne peuvent être comprises entre l\'heure de début et de fin d\'un autre créneau horaire.',
				'doc'		=> 'Vous pouvez créer et gérer les créneaux horaires depuis le menu Modules > myOwnReservations | Configuration puis dans l\'onglet règles de réservation sélectionnez la section créneaux horaires puis appuyez sur le bouton + en haut de la page ou depuis le coin supérieur droit du tableau.',
				'name'		=> 'Le nom d\'un créneau horaire ne doit pas être vide et doit avoir une longueur inférieure à 10 caractères. Le nom est affiché au client quand il sélectionne un créneau horaire.',
				'product'	=> 'Vous pouvez choisir un produit pour le créneau horaire, cela vous autorise à définir des créneaux horaires qui se chevauchent',
				'times'		=> 'L\'heure de début et de fin d\'un créneau horaire ne peut pas être incluse entre l\'heure de départ (exclue) et l\'heure d\'arrivée (inclue) d\'un autre créneau horaire.',
				'type'		=> 'Vous pouvez forcer un créneau horaire à être visible seulement pour le début ou la fin de la réservation, cela peut vous aider à limiter le choix du client et à accroître votre occupation des créneaux horaires.',
				'week_type'		=> 'Vous pouvez forcer un créneau horaire à être activé seulement les semaines paires ou impaires ou à une semaine bien précise',
				'days'		=> 'Cochez les jours pour lesquels le créneau horaire sera disponible chaque semaine.',
				'img_list'		=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-config-reservation-creneaux1.png',
				'img_list_pro'		=> 'http://labulle.fr/wp-content/uploads/2015/06/myownrpro-config-creneaux.png',
				'img_one'		=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-config-reservation-creneau1.png',
			);
		} else {
			self::$objects['timeslot'] = array (
				'help'		=> 'The time slot is a period during which a customer can reserve a product. You can create as many time slots that you want but the end time or start time of a time slot cannot be included between the start time and the end time of another time slot.',
				'doc'		=> 'You can create and manage time slots from the Modules menu> myOwnReservations | Configuration and then in tab Reservation Rules select the time slots section. Press the + button at the top or from the top right corner of the table to a create new time slot.',
				'name'		=> 'cannot be empty and must be smaller than 10 chars. The name is show when a customer select a time slot.',
				'product'	=> 'You can assign a product to a time slot that allow you to set overlapping time slots.',
				'times'		=> 'Time slots must be uniques and disjoints. That means that a time slot can\'t begin or end in another timeslot, and that also means that a time slot can\'t begin before a time slot and end after the same time slot. However a time slot can begin at the same time that the end of another time slot. 12:00 am is consider as the begin of the day when it is use as the start time, and it is consider as the end of the day when it is use as the end time.',
				'type'		=> 'You can force a time slot to be visible only for reservation start or end, that can help you to limit the customer choice and increase your time slots occupation.',
				'week_type'		=> 'You can force a time slot to be enabled only for even or odd weeks on on precise week number.',
				'days'		=> 'Check the days for which time slot would be available in each week',
				'img_list'		=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-config-reservation-creneaux1.png',
				'img_list_pro'		=> 'http://labulle.net/wp-content/uploads/2015/06/myownrpro-settings-timeslots.png',
				'img_one'		=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-config-reservation-creneau1.png',
			);
		}
		
		//------------------------------------------------------------------------------------------------------------------------
		//												AVAILABILITY
		//------------------------------------------------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$objects['availability'] = array (
				'help'		=> 'Vous pouvez indiquer des période de disponibilité pour vos produits, préférez  l’usage des créneaux horaires pour des disponibilités récurrentes. Par défaut un produit est disponible avec les règles de base, il n\'est pas nécessaire de rajouter de disponibilités. Si vous créez une disponibilité pour un produit, celui-ci sera considéré comme indisponible tout le temps sauf aux périodes de disponibilités indiquées.',
				'doc'		=> 'Vous pouvez créer et gérer les disponibilités depuis le menu Modules > myOwnReservations | Configuration puis dans l\'onglet disponibilités puis appuyez sur le bouton + en haut de la page ou depuis le coin supérieur droit du tableau. Il est également possible de le faire depuis l\administration d\'un produit en réservation.',
				'type' 	=> 'Si vous réglez au moins une disponibilité, le produit sélectionné sera considéré comme indisponible tout le temps sauf sur la période de disponibilité, les autres produits resteront disponibles sauf si vous choisissez de l\'appliquer à tous les produits.',
				'date'		=> 'Attention si l\'heure de fin est 00:00 sélectionnez le lendemain du dernier jour de disponibilité ou d\'indisponibilité. Exemple pour bloquer la journée 17 Janvier 2013 indiquez le 17/01/2013 00:00 comme début et 18/01/2013 00:00 comme fin.',
				'qty'	=> 'Indiquez le nombre de réservations possibles sur ce créneau.',
				'img_list'		=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-settings-availabilities.png',
				'img_list_pro'		=> 'http://labulle.fr/wp-content/uploads/2015/06/myownrpro-admin-disponibilites.png'
			);
		} else {
			self::$objects['availability'] = array (
				'help'		=> 'You can specify the period of availability for your products, prefer the use of time slots for recurrent availability. By default a product is available with the basic rules, there is no need to add more. If you create an availability for a product, it will be considered unavailable all the time except for the availability periods indicated.',
				'doc'		=> 'You can create and manage availabilities from the Modules menu> myOwnReservations | Configuration and then in tab Availabilities tab. Press the + button at the top or from the top right corner of the table to create a new availability. It is also possible to do it from the product administration.',
				'type'		=> 'If you set at least an availability, the product selected will be considered unavailable all the time except during the period of availability, other products will remain available unless you choose to apply to all products.',
				'date'	=> 'Beware if the end time is 00:00 select the day following the last day of availability or unavailability. Example, to block the day January 17, 2013 1/17/2013 00:00 as specify the start and end as 18/01/2013 00:00.',
				'qty'		=> 'Indicate the number of possible reservations on that period',
				'img_list'		=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-settings-availabilities.png',
				'img_list_pro'		=> 'http://labulle.net/wp-content/uploads/2015/06/myownrpro-settings-availabilities.png'
			);
		}
		
		
		//-------------------- availabilities types --------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$options['availability'] = array(
				availability_type::UNAVAILABLE  	=> 'Une indisponibilité permet de rendre le produit indisponible peut importe la quantité de produit en stock. Il n\'est donc pas possible de sélectionner les jours concernés mais une réservation peut courir par dessus selon les options de planification.',
				availability_type::AVAILABLE  		=> 'Les disponibilités permettent de travailler différemment en indiquant uniquement les périodes disponibles. Dès qu\'une disponibilité se trouve dans la fenêtre de réservation pour ce produit ou sa famille alors le produit est indisponible tout le temps, sauf sur les disponibilités trouvées.'
			);
		} else {
			self::$options['availability'] = array(
				availability_type::UNAVAILABLE 	=> 'Unavailability can make the product unavailable regardless of the amount of product in stock. There is therefore not possible to select the days concerned, but a reservation can run over depending on scheduling options',
				availability_type::AVAILABLE 		=> 'For a dates rule you\'ve to choose the start and end date between which the discount apply. If \'later\' the rule applies on the days of the period even if the reservation continues after the period.'
			);
		}
		
		//------------------------------------------------------------------------------------------------------------------------
		//												PRICE RULES
		//------------------------------------------------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$objects['pricerules'] = array (
				'help'		=> 'Les règles de prix vous permettent de personnaliser le calcul du prix de la réservation en appliquant des réductions qui peuvent être un pourcentage du prix ou un montant. \n Le calcul du prix s\'effectue ainsi : d\'abord le module va chercher les tarifs par tableau selon la durée ou la période et les applique sur les jours concernés, le tarif de base du produit est pris pour les autres jours. \n Ensuite la règle de prix qui a comme impact de fixer directement le tarif est appliquée (Si il y en a plusieurs, celle qui a la plus grande borne est appliquée, si elle est partielle elle est appliquée uniquement sur la partie concernée). \n Enfin les autres règles de prix sont appliquées en gardant en dernier celle qui s\'appliquent sur toute la réservation.',
				'doc'		=> 'Vous pouvez créer et gérer les disponibilités depuis le menu Modules > myOwnReservations | Configuration puis dans l\'onglet disponibilités puis appuyez sur le bouton + en haut de la page ou depuis le coin supérieur droit du tableau. Il est également possible de le faire depuis l\'administration d\'un produit en réservation.',
				'name'		=>	'Indiquez le nom de la réduction affiché au client',
				'product'		=>	'Ce paramètre vous permet de choisir la portée de votre réduction, vous pouvez choisir un produit en particulier sinon elle s\'appliquera à tous les produits de la catégorie de produit des paramètres généraux.',
				'enable' 	=> 'Vous pouvez activer ou désactiver la règle de prix sans la supprimer. Si la règle de prix est désactivée, elle n\'entrera pas en compte dans le calcul du prix de la réservation.',
				'visible'		=> 'Cochez visible si vous souhaitez que cette règle de prix soit affichée au client',
				'impact'	=> 'Choisissez le type de calcul de tarif, si plusieurs règles de prix sont trouvées elles s\'appliquent dans l\'ordre suivant : La règle de prix fixe avec la plus grande borne , puis la règle de prix unitaire sur la période concernée, puis les règles d\'augmentation et de réduction.',
				'ratio'		=>	'Le ratio donne le montant de la réduction comme une partie de prix original du produit. Par exemple si vous remplissez 10% et que le produit coûte 150, la réduction sera de 15.',
				'amount'	=>	'Le montant ne s’applique une seule fois peu importe la durée',
				'over'		=>	'Cochez cette case pour que la réduction s\'applique sur tout le prix de la réservation au lieu la partie concernée par la règle de réduction.',
				'quantity'		=>	'Décochez cette case pour que la réduction s\'applique sur un seul produit.',
				'img_list'		=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-config-reglesprix1.png',
				'img_list_pro'	=> 'http://labulle.fr/wp-content/uploads/2015/06/myownrpro-config-creneaux.png',
				'img_one'		=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-config-regleprix1.png',
			);
		} else {
			self::$objects['pricerules'] = array (
				'help'		=> 'The prices rules let you customize the price calculation of a reservation by applying a discount that can be a percent of the price or an amount. You can choose different types of rules : "Length" let you apply discount depending on the length of the reservation; "Dates" let you apply a discount on a period between two dates; "Recurring" let you apply discount on a certain time slot, on all or some days of the week. You can enable or disable theses rules at any moment.',
				'doc'		=> 'You can create and manage availabilities from the Modules menu> myOwnReservations | Configuration and then in tab Availabilities tab. Press the + button at the top or from the top right corner of the table to create a new availability. It is also possible to do it from the product administration.',
				'name'		=>	'Fill a name that will be visible by the customer',
				'product'	=>	'This parameter let you choose the impact of your discount, you can choose a particular product otherwise it will be applied to ail the products from the product category of general parameters.',
				'enable' 	=> 'You can enable or disable the price rule without deleting it. If the price rule is disabled it will not be applied on reservation price calculation.',
				'visible'	=> 'Check visible if you want the discount to be displayed to the customer',
				'impact'	=> 'Choose the type of price calculation, if multiple pricing rules are found they apply in the following order: Rule fixed price with larger bound and the rule of unit price over the relevant period, then the rules increase and reduction.',
				'ratio'		=>	'The ratio give the discount amount as a part of original product price. For instance if you fill 10% and the product cost 150, the discount would be 15.',
				'amount'	=>	'The amount is applicable only once regardless of the length',
				'over'		=>	'Check to apply the price modification on all reservation instead of the part concerned by the discount rule.',
				'quantity'		=>	'UnCheck to apply the price modification on one item only.',
				'img_list'		=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-settings-pricerules.png',
				'img_list_pro'	=> 'http://labulle.net/wp-content/uploads/2015/06/myownrpro-settings-pricerules.png',
				'img_one'		=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-settings-pricerule.png'
			);
		}
		
				
		//-------------------- pricerules types --------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$options['pricerules'] = array(
				pricesrules_type::LENGTH 	=> 'Pour une règle de durée vous devez choisir une durée min et max entre lesquelles la réduction s\'applique. Si \'ou plus\' coché la règle s\'applique sur les jours qui correspondent à la plage de durée même si la durée totale de réservation est plus longue.',
				pricesrules_type::DATES 	=> 'Pour une règle de dates vous devez choisir une date de début et de fin entre lesquelles la réduction s\'applique. Si \'ou plus tard\' coché la règle s\'applique sur les jours de la période même si la réservation continue après la période.',
				pricesrules_type::RECURRING		=> 'Pour un règle de récurrence vous pouvez choisir tous les créneaux horaires ou un créneau spécifique et tous les jours ou certains jours de la semaine pour lesquels la réduction s\'applique.',
				pricesrules_type::DELAY		=> 'Pour une règle de délai vous devez choisir une durée min et max (entre la date de la commande et le début de la réservation) entre lesquelles la réduction s\'applique. ',
				pricesrules_type::QTY		=> 'Pour une règle de quantité vous devez choisir une quantité min et max entre lesquelles la réduction s\'applique. ',
				pricesrules_type::HISTORY	=> 'Pour une règle de fidélité vous devez choisir une durée totale des réservations du client min et max entre lesquelles la réduction s\'applique. ',
			);
		} elseif ($iso == 'it') {
            self::$options['pricerules'] = array(
                pricesrules_type::LENGTH 	=> 'Per una regola di lunghezza devi scegliere la lunghezza minima e massima tra cui si applica lo sconto. Se settato "o più" la regola si applica ai giorni corrispondenti all\'intervallo di lunghezza anche se la lunghezza totale della prenotazione è più lunga.',
				pricesrules_type::DATES 	=> 'Per una regola delle date devi scegliere la data di inizio e di fine tra le quali si applica lo sconto. Se settato "o più tardi" la regola si applica ai giorni del periodo, anche se la prenotazione continua dopo il periodo.',
				pricesrules_type::RECURRING	=> 'Per una regola ricorrente è possibile scegliere tutte le fasce orarie o una fascia oraria specifica e tutti i giorni o alcuni giorni della settimana per cui si applica lo sconto.',
				pricesrules_type::DELAY		=> 'Per una regola di ritardo devi scegliere il ritardo minimo e massimo (tra la data dell\'ordine e l\'inizio della prenotazione) tra cui si applica lo sconto.',
				pricesrules_type::QTY		=> 'Per una regola di quantità devi scegliere la quantità minima e massima tra cui si applica lo sconto.',
				pricesrules_type::HISTORY	=> 'Per una regola della cronologia devi scegliere la lunghezza minima e massima delle prenotazioni totali del cliente tra le quali si applica lo sconto.'
            );
        }

		else {
			self::$options['pricerules'] = array(
				pricesrules_type::LENGTH 	=> 'For a length rule you\'ve to choose the minimum and maximal length between which the discount apply. If \'more\' checked the rule applies on the days corresponding to the length range even if the total length of reservation is longer.',
				pricesrules_type::DATES 	=> 'For a dates rule you\'ve to choose the start and end date between which the discount apply. If \'later\' the rule applies on the days of the period even if the reservation continues after the period.',
				pricesrules_type::RECURRING	=> 'For a recurring rule you can choose all time slots or a specific time slot and all days or a some days of the week for which the discount apply.',
				pricesrules_type::DELAY		=> 'For a delay rule you\'ve to choose the minimum and maximal delay (between order date and reservation start) between which the discount apply. ',
				pricesrules_type::QTY		=> 'For a quantity rule you\'ve to choose the minimum and maximal quantity between which the discount apply. ',
				pricesrules_type::HISTORY	=> 'For an history rule you\'ve to choose the minimum and maximal length of total customer reservations between which the discount apply. '
			);
		}
		
		
		//------------------------------------------------------------------------------------------------------------------------
		//						CONFIG
		//------------------------------------------------------------------------------------------------------------------------


		//-------------------- Settings Length --------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$objects['settings'] = array (
				'help'		=> '',
				'doc'		=> '',
				'img_list'		=> 'http://labulle.fr/wp-content/uploads/2015/06/myownr-config-config1.png',
				'img_list_pro'	=> 'http://labulle.fr/wp-content/uploads/2015/06/myownrpro-config-config.png',
				'priceDisplay'	=> 	'Si vous choisissez l\'unité de prix, l\'unité de temps sera ajoutée au prix des produits avec un /. Sinon "le prix minimum" affiche le prix pour la durée minimum choisie dans les règles de réservation.',
				'ignoreCarrier' 	=> 'Pour ignorer le choix du transporteur, les CGV doivent être ignorées et un transporteur par défaut choisi.',
				'ignoreAddress'	=> 'L\'adresse par défaut du client sera utilisée',
				'columnWidget' 	=> 'Afficher un widget sur la colonne de droite ou de gauche pour permettre au client de présélectionner sa période de réservation. Lors de la validation le widget affiche les produits de la catégorie de réservation avec le prix de la période. La période sélectionnée est sauvée dans la session et permet d\'ajouter directement la réservation au panier si elle est disponible.',
				'homeWidget'	=> 'Le widget permet d\'afficher les disponibilités des produits sur un planning en page d\'accueil.',
				'messages'	=> 'Vous pouvez personnaliser ici les messages qui seront affichés au client quand la réservation ne sera pas disponible à cause des contraintes de réservation ou quand une notification de est envoyée par message de commande et par email.',
				
			);
			if ($isproversion)
			self::$objects['settings'] += array (
				'import'		=> 'Le fichier importé doit avoir le même format que le fichier exporté en gardant la ligne d\'entête sans la modifier. Indiquez un identifiant à 0 pour ajouter des données.  Les données existantes ne sont pas remplacées.',
				'importempty' 	=> 'Cochez cette option pour vider la table avant l\'insertion de données, cette option est utile si vous insérez toutes les données en même temps.',
			);
		} else {
			self::$objects['settings'] = array (
				'help'		=> '',
				'doc'		=> '',
				'img_list'		=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-settings-settings.png',
				'img_list_pro'	=> 'http://labulle.net/wp-content/uploads/2015/06/myownrpro-settings-settings.png',
				'priceDisplay'  => 'If you choose price unit, the time unit will be added to product prices with a /. If you wish you can display minimum price that means the price for the shorter period that can be selected depending on reservation length.',
				'ignoreCarrier' 	=> 'To ignore the choice of carrier, the TCU should be ignored and default carrier selected.',
				'ignoreAddress'	=> 'Default customer address will be used',
				'columnWidget' 	=> 'Show a widget on left or right column to let the user preselect his reservation period. On validation the widget display the products of the reservation category with period price. The selected period is saved in session and let add directly the reservation of products to the cart if they are available.',
				'homeWidget'	=> 'The widget displays the availability of products on a planning on the homepage.',
				'messages'	=> 'Here you can customize messages that are displayed to the customer when reservation is not available due to reservation window constraints or when a reschedule notification is sent by order msg and email.',
				
			);
			if ($isproversion)
			self::$objects['settings'] += array (
				'import' 		=>	'The imported file must have the same format as the exported keeping the header row unchanged. Enter an id to 0 to add data. Existing data are not replaced.',
				'importempty' 	=> 'Check this option to clear the table before inserting data, this option is useful if you insert all the data simultaneously.',
			);
		}

		
		//-------------------- tuto Length --------------------------------------------------------------------------------
		$name = '';
		if (isset($obj->context->employee->firstname))
			$name = $obj->context->employee->firstname;
			
		if ($iso == 'fr') {

			self::$tuto = array (
				'0'			=>		'Suivez le guide: découvrez myOwnReservations',
				'step0'		=>		ucfirst($name).' merci d\'avoir fait confiance à laBulle pour gérer vos réservations.\nSuivez ce guide pour configurer rapidement les réservations dans votre boutique.\nVous pouvez aussi afficher les bulles d\'aide lors de la configuration en cliquant sur le bouton en haut à droite ou <a href="'.myOwnReservationsController::getAjaxUrl('action=printHelp').'" target="_blank">lire la documentation</a>.',
				'1'			=>		'Sélectionnez les produits en location',
				'step1'		=>		($isproversion ? 'Créez au moins une règle de réservation' : '').'\nChoisissez de preferences les produits par catégorie (pour tous les produits choisissez acceuil, ou ses sous-catégories pour les retrouver sur le widget de sélection).\n Pour les produits concernés la quantité deviendra le nombre de places disponible (si la gestion de stock est activée) et le prix de vente sera le tarif de l\'unité de temps.',
				'2'			=>		'Contrôler la sélection de la réservation',
				'step2'		=>		'Par défaut le client peut choisir tous les jours mais vous pouvez changer le type de période qui sera sélectionné et affiner les heures de début et de fin de sélection.\nVous pouvez aussi créer des indisponibilités pour indiquer les dates de fermeture mais attention si vous indiquer des disponibilités pour un produit il n\'est alors plus disponible par défaut.',
				'registration' =>	'Pour activer le module l\'enregistrement d\'une clé de licence est nécessaire, sans cela la partie front-office est désactivée. Pour obtenir la clé connectez-vous sur votre compte www.labulle.net afin de générer celle-ci pour votre nom de domaine. Si vous de disposez pas encore d\'un compte envoyez un mail à info@labulle.net en joignant votre facture Prestashop Addons.',
				'pro' 		=>		'Créez au moins une règle de réservation avec une sélection de produits.',
				'std' 		=>		'Sélectionnez au moins un produit ou une catégorie',
				'3'			=>		'Configurez le tarif de réservation',
				'step3'		=>		'Par défaut le tarif est le prix du produit par la durée mais vous pouvez changer l\'unité de durée ou passer par des tableaux pour indiquer directement le tarif par déclinaison et par durée (si limitée) ou par période.\nPour affiner vous pouvez ensuite ajouter des règles de prix en fonction de la période, de la durée, de la fréquence ou du délai.',
				'4'			=>		'Personnalisez le processus de commande',
				'step4'		=>		'Choisissez les widgets de sélection, d\'affichage des réservations depuis la configuration et ajustez leurs positions.\nPersonnalisez les messages et libellés depuis la configuration du module.'
			);
		} else {
			self::$tuto = array (
				'0'			=>		'Follow the guide: discover myOwnReservations',
				'step0'		=>		ucfirst($name).' thank you for trusting Labulle to manage your reservations.\nFollow the guide to quickly configure the reservations in your store.\nYou can also view the assistance bubbles in the configuration by clicking the button 2 on top or <a href="'.myOwnReservationsController::getAjaxUrl('action=printHelp').'" target="_blank">read the documentation</a>.',
				'1'			=>		'Select the products for rent',
				'step1'		=>		($isproversion ? 'Create at least one reservation rule' : '').'\n Choose preferences for products by category (for all products Choose Home or its sub-categories to find them on the selection widget).\n For the products concerned the amount becomes the number of places available (if stock management is enabled) and the sale price will be the price of the unit time.',
				'2'			=>		'Control the selection of the reservation',
				'step2'		=>		'By default the client can choose every day but you can change the type of period that will be selected and refine the start and end selection.\nYou can also create unavailabilities indicating closure dates but beware if you indicate availability product is no longer available by default.',
				'registration' =>	'To enable the module registration of a license key is required, otherwise the front office part is disabled. To get a key log onto your account www.labulle.net to generate it for your domain name. If you not already have an account please email and attach your Prestashop Addons invoice to info@labulle.net.',
				'pro' 		=>		'Create at least one reservation rule with a selection of products.',
				'std' 		=>		'Select at least one product or category',
				'3'			=>		'Configure the booking price',
				'step3'		=>		'Default cost is the price of the product by the duration but you can change the unit of time or go through the tables to indicate directly the cost per declination and duration (if limited) or period.\nTo narrow you can then add pricing rules based on the time, duration, frequency or time.',
				'4'			=>		'Customize order process',
				'step4'		=>		'Choose the selection widgets, display reservations in the settings and adjust their positions.\nCustomize messages and labels for module configuration.'
			);
		}
		
				

		
		//==========================================================================================================================================
		//												ADMIN
		//==========================================================================================================================================

		//-------------------- views --------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$sectionNames['admin'] = array (
				'planning'	=> 'Planning d\'administration',
				'display'	=> 'Affichage des réservations',
				'edit'		=> 'Modifier une réservation',
				'create'	=> 'Création d\'une réservation'
			);
			self::$sections['admin'] = array (
				'planning'	=> 'Le planning permet de présenter les réservations de manière graphiques avec différentes vues.',
				'display'	=> 'La popup d\'affichage d\'une réservation peut s\'afficher depuis le détail des commandes et le planning.',
				'edit'		=> 'Vous pouvez modifier ou replanifier une réservation. Il n\' est pas possible de modifier les produits de la réservation mais pouvez en modifier la quantité ou faire un commentaire.',
				'create'	=> 'Il est possible de créer des réservations depuis l\'administration du module pour la prise manuelle de réservation ou le changement de produits.'
			);
			if ($isproversion) {
				self::$sectionNames['admin']['stock'] = 'Gestion de stock';
				self::$sections['admin']['stock'] = 'Avec la version PRO vous pouvez ajouter des numéros de série au lieu d\'utiliser la quantité de produit. Pour être utilisé, l\'occupation des produits des règles de réservation doit être réglé sur stock.';
				
			}
		} else {
			self::$sectionNames['admin'] = array (
				'planning'	=> 'Administration planning',
				'display'	=> 'Display reservations',
				'edit'		=> 'Reservation modification',
				'create'	=> 'Create a reservation'
			);
			self::$sections['admin'] = array (
				'planning'	=> 'The planning allows to present the reservations so graphic with different views.',
				'display'	=> 'The popup of reservation display can show up from order details and planning.',
				'edit'		=> 'You can change or schedule a reservation. It is not possible to change the booking but products can change the amount or make a comment',
				'create'	=> 'It is possible to create reservations for administration module for the handgrip reservation or change of products.'
			);
			if ($isproversion) {
				self::$sectionNames['admin']['stock'] = 'Stock management';
				self::$sections['admin']['stock'] = 'With PRO version you can add serial numbers instead of using product quantity. To be used reservation rules product occupation must be set on stock.';
			}
		}
		
		//------------------------------------------------------------------------------------------------------------------------
		//						planning
		//------------------------------------------------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$sectionContent['admin']['planning'] = array (
				'img'		=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-admin-mois1.png',
				'help'		=>	'Le planning d\'administration est accessible depuis l\'onglet administration via la configuration du module ou via le menu Commandes > myOwnReservations.',
				'navigation'=> 'La barre du haut vous permettra de choisir la vue et de changer de période en utilisant le sélecteur de date ou les flèches.
Le planning est actualisé toutes les 60 secondes automatiquement.',
				'views'		=> 'Différentes vues vous sont proposées chacune d\'elles présente les réservations sur des durées différente.',
				'export'	=> 'Le bouton avec une flèche en haut à droite du tableau ou en haut de la page vous permet d\'exporter les réservations de la vue en cours vers un fichier CSV.',
				'pro'		=> 'Un filtre vous permet de choisir la famille de produits à afficher. Ce filtre peut être restreint si l\'utilisateur est assigné à des familles de produits.',
			);
		} else {
			self::$sectionContent['admin']['planning'] = array (
				'img'		=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-admin-month.png',
				'help'		=>	'The administration schedule is accessible from the Administration tab by configuring the module or via the menu Orders > myOwnReservations',
				'navigation'	=> 'The top bar will let you choose the view and change period using date selector or arrows.
The schedule is updated every 60 seconds automatically.',
				'views'		=> 'Multiple views are offered, each of them presents reservations on distinct lengths.',
				'export'	=> 'The button with an arrow on top right of the table or on top of page let you export reservations displayed on the current view to a CSV file.',
				'pro'		=> 'A filter let choose product family to display. This filter can be restricted if the user is assignated to product families.',
			);
		}
		
				
		//-------------------- views --------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$adminOptions['planning']['views'] = array (
				'month'	=> 'La vue par mois permet de visualiser l\'ensemble des réservation d\'un mois. Les heures de la réservation ne sont pas présentées mais on peut voir un réservation s\'étaler sur plusieurs jours.',
				'chrono'	=> 'La vue chronologique ne présente pas le détail des heures mais permet de lister en colonne l\'ensemble des produits (y compris ceux qui n\'ont pas réservation afin d\'avoir une idée de l\'occupation globale) et leurs réservations sur 2 semaines.',
				'week'		=> 'La vue par semaine permet de voir l\'ensemble des réservations de la semaine avec le détail des heures. Les réservations qui durent tout la journée sont sur une ligne en haut de l\'écran.',
				'grid' 		=> 'La vue par grille permet d\'afficher les réservations d\'une journée avec plus de détails en texte. Il est possible de trier les résultats en cliquant sur le titre des colonnes.'
			);
		} else {
			self::$adminOptions['planning']['views'] = array (
				'month'		=> 'The month view lets you view all the booking on a month. The hours of booking are not presented but you can see a book that take several days.',
				'chrono'	=> 'The timeline view don\'t show details of hours but can list all products on column (including those who have not booking to get an idea of the overall occupancy) and their reservations for 3 weeks.',
				'week'		=> 'The week view allows you to see all weekly bookings with details of hours. Reservations that last all day are on a line on top of the screen.',
				'grid' 		=> 'The grid view displays the reservations of a day in more detail in the text. You can sort the results by clicking the column headings.'
			);
		}
		
		//-------------------- sections Sections --------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$adminSectionNames = array(
				'types'			=> 'Types',
				'display'		=> 'Afficher',
				'create'		=> 'Créer',
				'modification'	=> 'Modification',
				'reschedule'	=> 'Replanifier',
				'help'			=> 'Aide',
				'navigation'	=> 'Navigation',
				'views'			=> 'Vues',
				'export'		=> 'Export',
				'import'		=> 'Import',
				'selection'		=> 'Selection',
				'select'		=> 'Selecteur',
				'search'		=> 'Recherche',
				'home'			=> 'Accueil',
				'steps'			=> 'Etapes',
				'addons'		=> 'Prestashop Addons',
				'labulle'		=> 'laBulle',
				'version'		=> 'Version',
				'download'		=> 'Télécharger',
				'upload'		=> 'Envoyer',
				'install'		=> 'Installer',
				'requirement'	=> 'Pre-requis',
				'local'			=> 'En local',
				'test'			=> 'Test',
				'comment'		=> 'Commentaire',
				'popup'			=> 'popup',
				'validate'		=> 'Valider',
				'config'		=> 'Configuration',
				'popup'			=> 'Popup',
				'order'			=> 'Commande',
				'products'		=> 'Produits',
				'details'		=> 'Détails',
				'refresh'		=> 'Rafraichissement',
				'period'		=> 'Période',
				'month'			=> 'Mois',
				'week'			=> 'Semaine',
				'chrono'		=> 'Chronologie',
				'grid'			=> 'Grille',
				'product'		=> 'Produit',
				'combination'	=> 'Combinaison',
				'history'		=> 'Historique',
				'serial'		=> 'Numéro série',
				'add'			=> 'Ajouter',
				'edit'			=> 'Modifier',
				'assign'		=> 	'Assigner',
				'items'			=> 'Eléments de stock',
				'category'		=> 'Catégorie',
				'position'		=> 'Position',
				'delay'			=> 'Délai',
				'unavailabilities' => 'Indisponibilités',
				'reservations'	=> 'Réservations',
				'availabilities' => 'Disponibilités',
			);
		} else {
			self::$adminSectionNames = array(
				'types'			=> 'Types',
				'display'		=> 'Display',
				'modification'	=> 'Modification',
				'reschedule'	=> 'Reschedule',
				'create'		=> 'Create',
				'help'			=> 'Help',
				'navigation'	=> 'Navigate',
				'views'			=> 'Views',
				'export'		=> 'Export',
				'import'		=> 'Import',
				'selection'		=> 'Selection',
				'select'		=> 'Select',
				'search'		=> 'Search',
				'home'			=> 'Home',
				'steps'			=> 'Steps',
				'addons'		=> 'Prestashop Addons',
				'labulle'		=> 'laBulle',
				'version'		=> 'Version',
				'download'		=> 'Download',
				'upload'		=> 'Upload',
				'install'		=> 'Install',
				'requirement'	=> 'Requirements',
				'local'			=> 'Local use',
				'test'			=> 'Test',
				'comment'		=> 'Comment',
				'popup'			=> 'popup',
				'validate'		=> 'Validate',
				'config'		=> 'Settings',
				'popup'			=> 'Popup',
				'order'			=> 'Order',
				'products'		=> 'Products',
				'details'		=> 'Details',
				'refresh'		=> 'Refresh',
				'period'		=> 'Period',
				'month'			=> 'Month',
				'week'			=> 'Week',
				'chrono'		=> 'Chrono',
				'grid'			=> 'Grid',
				'product'		=> 'Product',
				'combination'	=> 'Combination',
				'history'		=> 'History',
				'serial'		=> 'Serial',
				'add'			=> 'Add',
				'edit'			=> 'Edit',
				'assign'		=> 'Assign',
				'items'			=> 'Stock items',
				'category'		=> 'Category',
				'position'		=> 'Position',
				'delay'			=> 'Delay',
				'unavailabilities' => 'Unavailabilities',
				'reservations'	=> 'reservations',
				'availabilities' => 'Availabilities',
			);
		}

		
		//------------------------------------------------------------------------------------------------------------------------
		//						display
		//------------------------------------------------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$sectionContent['admin']['display'] = array (
				'img'		=> 'http://labulle.fr/wp-content/uploads/2015/06/myownr-admin-commande1.png',
				'popup'	=> 'Pour afficher une réservation double cliquez dessus depuis la planning, ou appuyez sur le bouton "reservations" à droite de la liste des produits depuis les détails de commande.',
			);
		} else {
			self::$sectionContent['admin']['display'] = array (
				'img'		=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-admin-order.png',
				'popup'	=> 'To view a booking double click on it from the planning or press the button "reservations" to the right of the list of products for ordering details.',
			);
		}

		if ($iso == 'fr') {
			self::$adminOptions['display']['popup'] = array (
				'img'		=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-admin-modif1.png',
				'order'	=> 'Les informations de la commande à laquelle la réservation appartient sont affichées en haut de la fenêtre. Il est possible d\'afficher la commande et le client en cliquant sur le numéro de commande et le nom du client. \nLe montant payé et le montant total de la commande sont affichés, ces montants sont différents dans le cas de l\'utilisation de l\'avance.',
				'products'		=> 'Le milieu de la fenêtre affiche le produit associé à la réservation (il ne peut y avoir qu\'un seul produit par réservation mais avec une quantité multiple). Il est possible de modifier la quantité mais pas le produit (dans ce cas il faut supprimer la réservation et en ajouter une autre). Un encart en dessous du produit affiche les quantités disponibles pour la période (la réservation en cours est prise en compte si validée). Cela est fait à titre informatif, il est possible de surbooker le produit depuis l\'administration.',
				'details' 		=> 'Le bas de la fenêtre affiche la période réservée et les différents détails : la durée, le prix unitaire, les réductions appliquées etc..',
				'modification'	=> 'Vous pouvez modifier les détails de la réservation en cliquant sur le bouton éditer avec une icône de crayon.',
				
			);
		} else {
			self::$adminOptions['display']['popup'] = array (
				'img'		=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-admin-edit.png',
				'order'		=> 'The information of the order to which the reservation belongs are displayed at the top of the window. It is possible to view the order and the customer by clicking the order number and customer name. \n The amount paid and the total amount of the order are displayed, these amounts are different in the case of the use of the advance.',
				'products'		=> 'The middle of the window displays the product associated with the reservation (there can be only one product per booking but with multiple quantities). You can change the quantity but not the product (in this case must be deleted booking and add another). An displayed below the product shows the quantities available for the period (the current reservation is taken into account if enabled). This is made for information purposes, you can overbook the product from the administration..',
				'details' 		=> 'The bottom of the window displays the reserved period and the various details: the duration, the unit price, the reductions applied etc ..',
				'modification'	=> 'You can edit the reservation details by clicking on edit button with a pencil icon.',
			);
		}
		
				
		//------------------------------------------------------------------------------------------------------------------------
		//						edit
		//------------------------------------------------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$sectionContent['admin']['edit'] = array (
				
				'validate'	=> 'La réservation est validée automatiquement en fonction du statut de la commande associée. Les statuts sélectionnés dans les règles de réservation permettent de valider les réservations, ceux qui ne sont pas sélectionnés invalideront les réservations.\nPar défaut, le statut "paiement accepté" est sélectionné, pour valider la réservation et donc bloquer le produit uniquement quand la commande est payée (et éviter ainsi de bloquer les produits pour des commandes en attente).\nLors d\'un changement manuel du statut de la commande, il est possible que le produit de la réservation ne soit plus disponible (car une autre commande validée avec le même produit a été passée depuis). Un message est alors affiché pour prévenir l\'administrateur qu\'il n\'est plus possible d\'honorer cette réservation, et il est possible de générer le message pour le client.',
				'reschedule'		=> 'Il est possible de déplacer une réservation en la glissant déposant à partir du rebord de couleur plus foncée. Il également possible d\'allonger une réservation en tirant sur le rebord extérieur. La fenêtre de la réservation concernée sera ouverte en mettant en avant les modifications, cliquez alors sur enregistrer pour valider.',

				'comment' 		=> 'Vous pouvez rédiger un commentaire sur la réservation, celui-ci n\'est pas visible par le client.',
			);
		} else {
			self::$sectionContent['admin']['edit'] = array (
				
				'validate'	=> '',
				'reschedule'		=> 'It is possible to move a reservation by filing sliding from the darker edge. It is also possible to extend a reservation by pulling on the outside edge. The relevant booking window will be opened by highlighting the changes, then click Save to confirm.',

				'comment' 		=> 'You can write a comment on the reservation, this is not visible by the customer.',
				'refresh' 		=> 'You can choose to update the order to apply the price change.'
			);
		}

		if ($iso == 'fr') {
			self::$adminOptions['edit']['reschedule'] = array (
				'img'	=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-admin-replanifier.png',
				'period'		=> 'La période est remplie avec le premier jour de la vue ou avec le jour qui a été cliqué sur le planning. La liste des créneaux horaires est affichée (si vous travaillez avec) et rafraichie à chaque changement de date.',

			);
		} else {
			self::$adminOptions['edit']['reschedule'] = array (
				'img'	=>	'http://labulle.net/wp-content/uploads/2015/10/myownr-admin-reschedule.png',
				'period'		=> 'The period is filled with the first day of the order or the day that was clicked on the schedule. The time slot list will be displayed (if working with time slots) and refreshed at each date change.',
			);
		}

		//------------------------------------------------------------------------------------------------------------------------
		//						create
		//------------------------------------------------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$sectionContent['admin']['create'] = array (
				'create'	=> 'Pour créer une réservation vous pouvez appuyer sur le bouton + en haut de page ou dans le coin supérieur droit du tableau. Il est également possible de cliquer directement sur la planning au jour de début désiré (et vers l\'heure désirée  en mode créneau horaire : l\'heure sera ramenée à celle du début du créneau le plus proche.',
				'import'	=>	'L\'import des données utilise le même format que les données exportées, la premiere colonne qui contient les identifiants peut être supprimée : les données seront alors insérées au lieu d\'être remplacées. Une case à cocher permet de vider la table avant d\'insérer les données.'
			);
		} else {
			self::$sectionContent['admin']['create'] = array (
				'create'   => 'To make a reservation you can press the + button at the top or in the top right corner of the table. It is also possible to click directly on the schedule to the desired start day (and toward the desired time slot in fashion: time will be brought to the early nearest time slot.',
				'import'	=> 'The import data uses the same format as the exported data, the first column contains the IDs can be deleted: the data will be inserted instead of being replaced. A check box allows you to clear the table before inserting data.'
			);
		}
		
		if ($iso == 'fr') {
			self::$adminOptions['create']['create'] = array (
				'img'	=> 'http://labulle.net/wp-content/uploads/2015/06/myownr-admin-new.png',
				'img_pro'	=> 'http://labulle.net/wp-content/uploads/2015/06/myownrpro-admin-new.png',
				'pro'	=> 'La réservation peut être associée avec une commande Prestashop ou pas dans le cas d\'une réservation externe ajoutée depuis le planning avec la version PRO. Dans ce cas il est possible d\'indiquer le montant payé et le mode de règlement, ainsi que le nom du client et le moyen de transport.',
				'period'		=> 'La période est remplie avec le premier jour de la vue ou avec le jour qui a été cliqué sur le planning. La liste des créneaux horaires est affichée (si vous travaillez avec) et rafraichie à chaque changement de date.',
				'refresh' 		=> 'Vous pouvez choisir de mettre à jour la commande pour appliquer le changement de tarif ou non.',
			);
		} else {
			self::$adminOptions['create']['create'] = array (
				'img'	=>	'http://labulle.net/wp-content/uploads/2015/06/myownr-admin-new.png',
				'img_pro'	=> 'http://labulle.net/wp-content/uploads/2015/06/myownrpro-admin-new.png',
				'pro'	=> 'Reservation can be associated with PrestaShop order or not in the case of an external reservation added from the schedule with the PRO version. In this case it is possible to indicate the amount paid and the regulation mode and the client name and the means of transport.',
				'period'		=> 'The period is filled with the first day of the order or the day that was clicked on the schedule. The time slot list will be displayed (if working with time slots) and refreshed at each date change.',
				'refresh' 		=> 'You can choose to update the order to reflect the change of price or not.',
			);
		}
		
		//------------------------------------------------------------------------------------------------------------------------
		//						stock
		//------------------------------------------------------------------------------------------------------------------------
		if ($iso == 'fr') {
			self::$sectionContent['admin']['stock'] = array (
				'img'		=> 'http://labulle.fr/wp-content/uploads/2015/06/myownrpro-admin-stock-elements.png',
				'help'		=> 'La vue stock présente une ligne pour chaque élément de stock, si les numéros de série ne sont pas renseignées alors une ligne est générée pour chaque quantité du produit.\nIl est possible de filtrer par famille de produit et ainsi avoir une vue d\'ensemble de l\'occupation des produits pour la journée en cours.\nLe champ de sélection permet de filtrer les réservations pour n\'afficher que celles qui commencent ou se terminent et ainsi faciliter le check-in ou check-out.',
				'add'		=> 'Cliquez sur le bouton + en haut du tableau ou de la page pour ajouter des éléments de stock.',
				'edit'		=> 'Vous pouvez cliquer sur un élément de stock pour en afficher les détails.',
				'assign'	=> 'Pour assigner un élément de stock à une réservation cherchez la réservation depuis n\'importe quelle vue et ouvrez la.'
			);
		} else {
			self::$sectionContent['admin']['stock'] = array (
				'img'		=> 'http://labulle.net/wp-content/uploads/2015/06/myownrpro-admin-stock-elems.png',
				'help'		=>	'The stock view shows a line for each stock item, if the serial numbers are not indicated than a line is generated for each quantity of the product.\nIt can be filtered by product family to have a view of occupation of all products for the current day. \nThe selection field \'on reservation\' allows filtering of beginning or ending reservations that facilitate check-in or check-out.',
				'add'		=> 'Click the + button at the top of the table or page to add stock items.',
				'edit'		=> 'You can click on a stock item to view its details.',
				'assign'	=> 'To assign a stock item to a booking, look for the reservation from any view and open it.'
			);
		}
		
		if ($iso == 'fr') {
			self::$adminOptions['stock']['add'] = array (
				'product'		=> 'D\'abord sélectionner un produit dans la liste, si la liste est vide, cela veut dire qu\'aucune occupation des règles de réservation ne sont sur stock ou aucun produit n\'est sélectionné pour cette règle.',
				'combination'	=> 'Vous pouvez sélectionner une combinaison si le produit en possède et si la règle de réservation n\'est pas en produit virtuel.',
				'history'		=> 'Toutes les réservations qui ont été assignées à cet élément de stock sont visible en bas de page.',
				'serial'		=> 'Ajouter une ligne pour chaque numéro de série. Les numéros de série doivent avoir une longueur de 12 caractères maximum et ceux avec des lettres ne peuvent pas être imprimée comme des codes barres.'
			);
		} else {
			self::$adminOptions['stock']['add'] = array (
				'product'		=> 'First select a product in the list, if list is empty that means that no reservation rules occupation are on stock or no product are selected for that rule.',
				'combination'	=> 'You can select a combination if the product have one and if it reservation rule in not on virtual product.',
				'history'		=> 'All reservations that have been assigned to this stock element are visible at the bottom of page.',
				'serial'		=> 'Add a line for each serial number. Serial number must have a length of 12 characters maximum and ones with letters cannot be printed as barcode.'
			);
		}
		
		if ($iso == 'fr') {
			self::$adminOptions['stock']['edit'] = array (
				'img'		=> 'http://labulle.fr/wp-content/uploads/2015/06/myownrpro-admin-stock-element.png',
				'details'	=> 'Une fois créé, vous pouvez personnaliser le nom de l\'élément de stock et ajouter un commentaire (le commentaire peut aussi servir à suivre l\'usure du produit.',
				'history'	=> 'Toutes les réservations qui ont été assignées à cet élément de stock sont visible en bas de page.',
			);
		} else {
			self::$adminOptions['stock']['edit'] = array (
				'img'		=>	'http://labulle.net/wp-content/uploads/2015/06/myownrpro-admin-stock-item.png',
				'details'	=> 'Once created, you can customize the stock item name and add a comment (comment can also be used to track product usury.',
				'history'	=> 'All reservations that have been assigned to this stock element are visible at the bottom of page.'
			);
		}
		
		if ($iso == 'fr') {
			self::$adminOptions['stock']['assign'] = array (
				'img'		=> 'http://labulle.fr/wp-content/uploads/2015/06/myownrpro-admin-stock-assigner.png',
				'items'		=> 'Sur les réservations qui se produisent sur des produits en stock, chaque quantité de produit affiche un champ pour sélectionner ou saisir un numéro de série, seul les éléments de stock non réservées sont affichés, mais les champs de sélection sont aussi des champs d\'entrée pour être utilisé avec un scanner de code-barres qui fonctionne comme clavier.',
			);
		} else {
			self::$adminOptions['stock']['assign'] = array (
				'img'		=> 'http://labulle.net/wp-content/uploads/2015/06/myownrpro-admin-stock-assign.png',
				'items'	=> 'On a reservation that happen on stock products, each product quantity will display a field to select or input a serial number, only non booked stock items are displayed but select boxes are also an inputs field to be used with a barcode scanner that work as keyboard input.',
			);
		}


		//==========================================================================================================================================
		//												FRONT
		//==========================================================================================================================================
		
		//-------------------- FRONT SECTIONS --------------------
		if ($iso == 'fr') {
			self::$sectionNames['front'] = array (
				'selection'		=> 'Sélection',
				'widgets'		=> 'Widgets',
				'home'			=> 'Accueil',
				'products'		=> 'Liste produits',
				'cart'			=> 'Panier',
				'popup'			=> 'Popup',
				'confirmation'	=> 'Confirmation'
			);
			self::$sections['front'] = array (
				'selection'		=> 'Il n\'est pas possible d\'ajouter un produit en réservation directement au panier, pour des produits en réservation il faut passer par le planning de sélection. Cette partie va vous en expliquer le fonctionnement et les codes couleur.',
				'widgets'		=> 'Le module propose plusieurs widgets, notamment le widget de sélection qui à deux modes de fonctionnement possible.',
				'home'			=> '',
				'products'		=> '',
				'cart'			=> 'Cette partie explique comment est modifié le panier quand des réservations sont ajoutées.',
				'popup'			=> '',
				'confirmation'	=> 'Voici les différentes confirmation de réservation.'
			);
		} else {
			self::$sectionNames['front'] = array (
				'selection'		=> 'Selection',
				'widgets'		=> 'Widgets',
				'home'			=> 'Home',
				'products'		=> 'Products list',
				'cart'			=> 'Cart',
				'popup'			=> 'Popup',
				'confirmation'	=> 'Confirmation'
			);
			self::$sections['front'] = array (
				'selection'		=> 'It is not possible to add a product reservation directly to the cart, for booked products selection planning must be used. This part will explain how it works and color codes.',
				'widgets'		=> 'This part explain how the basket is changed when reservations are added.',
				'home'			=> '',
				'products'		=> '',
				'cart'			=> 'This section explains how the cart is changed when reservations are added.',
				'popup'			=> '',
				'confirmation'	=> 'Here are the booking confirmation.'
			);
		}
		
		//-------------------- front selection ----------------------------------------
		if ($iso == 'fr') {
			self::$sectionContent['front']['selection'] = array (
				'img'		=> (_PS_VERSION_ >= "1.7.0.0" ? 'http://labulle.fr/wp-content/uploads/2015/06/myownr-front-produit-ps17.png' : 'http://labulle.net/wp-content/uploads/2015/06/myownr-front-planning-fixe1.png'),
				'help'		=>	'Il existe deux moyens de placer sa réservation pour un client :\nAller sur la fiche produit et faire directement sa selection dans son planning puis cliquer sur ajouter au panier qui s\'active dés que la période de fin est saisie.\nIl est aussi possible d\'utiliser le widget de sélection qui permet d\'enregistrer la période de réservation et d\'afficher uniquement les produits disponibles. \nLes périodes de réservations sont affichées en dessous des produits comme une personnalisation.',
				'selection'	=> 'Code couleur des périodes',
				'steps'		=> 'La sélection se fait en plusieurs étapes en fonction de l\'option de durée des règles de réservation.\n Si la durée est fixée (même avec une valeur plus grande que 1) alors le module connaitra la date de fin et propose directement les différentes périodes avec le prix final. Avec une durée fixe il est possible de cocher l\'option \'sélection multiple\' qui permet de choisir plusieurs cases du planning et donc différentes périodes en même temps (Si les périodes se suivent elles sont groupées).\n Par contre si la durée est variable le client choisit d\'abord la date de début puis valide pour rafraichir le planning sur le choix de la date de fin.\n Si le planning est sur une vue par mois avec plus de 3 créneaux horaires, alors il faut d\'abord sélectionner le jour pour pouvoir choisir le créneau horaire.',
			);
		} else {
			self::$sectionContent['front']['selection'] = array (
				'img'		=> (_PS_VERSION_ >= "1.7.0.0" ? 'http://labulle.net/wp-content/uploads/2015/06/myownr-front-product-ps17.png' : 'http://labulle.net/wp-content/uploads/2015/06/myownr-front-planning-fixe1.png'),
				'help'		=>	'The administration schedule is accessible from the Administration tab by configuring the module or via the menu Orders > myOwnReservations',
				'periods'	=> 'There are two ways to place a reservation for a client:\nGo to product page directly and make his selection in his schedule and click add to cart that is active as soon as the end of the period is entered.\nIt is also possible to use the selection widget that allows record the time of booking and let\'s view only the available products.\nThe periods bookings are displayed below as product customization.',
				'selection'	=> 'Color codes of periods',
				'steps'		=> 'The selection is made in stages according to the length option of booking rules.\n If the duration is fixed (even with a value larger than 1) then the module will know the end date and offers directly different periods with the final price. With a fixed time you can select the option \'multiple selection\' from which allow to choose several boxes of planning and therefore different periods simultaneously (following periods are grouped). \n However if the duration is variable then customer chooses first start date and valid to refresh the planning on the choice of the end date. \n If the schedule is on views per month with more than 3 slots, then first select the day to be able to choose the time slot.'
			);
		}
		//--------------------front selection options ----------------------------------------
		if ($iso == 'fr') {
			self::$adminOptions['selection']['selection'] = array (
				'Périodes actives'	=> 'Le planning permet de placer une réservation dans la fenêtre de temps qui a été configurée, en dehors de cette période et sur les jours/créneaux désactivés les cases sont blanches. Dans la mesure du possible ces périodes sont filtrées pour faciliter la navigation.',
				'Périodes désactivés'		=> 'Le planning tient compte des réservations validées pour permettre au client de choisir uniquement une période disponible. Les périodes disponibles sont affichées en vert et les périodes indisponible en gris.\nLes jours disponibles peuvent être quand mêmes désactivés car ils ne peuvent donner lier à une réservation dont la durée minimale respectée.',

				'Périodes indisponibles' 		=> 'Les indisponibilités sont affichées sur un fond gris hachuré. Une réservation peut être autorisée à courir par dessus si la selection de fin est disponible. Des périodes grises peuvent se rajouter avant ou après ce qui est logique dans le cas d\'une durée minimale.',
				'Périodes réservées' => 'Une réservation dans un panier est uniquement prise en compte pour les réservation du même client. Si un panier est transformé, en commande, dont le statut est de ceux qui valident la réservation, alors la réservation est prise en compte pour les autres clients. Si une réservation dans un panier n\'est plus valide (car elle n\'est plus dans la fenêtre de réservation qu\'une indisponibilité a été ajoutée ou qu\'un autre client a été plus rapide) alors elle et retirée avec son produit et un message explique au client l\'indisponibilité.',
			);
		} else {
			self::$adminOptions['selection']['selection'] = array (
				'Active periods'	=>	'The schedule allows you to place a reservation in the time window that was configured outside this period and on the days / slots off the boxes are white. Wherever possible, these periods are filtered for easier navigation.',
				'Disabled periods'		=> 'The schedule takes into account the reservations validated to allow the customer to choose only an available period. Available periods are shown in green and unavailable periods in gray.\nThe days available can be deactivated when the same because they cannot bind to a reservation which met minimum.',
				'Unavailable periods' 		=> 'Availability is displayed on a gray background hatched. A reservation can be allowed to run over the end of selection if available. Gray periods may be added before or after what is logical in the case of a minimum period.',
				'Booked periods' 		=> 'A booking in a basket is only taken into account for the reservation of the same client. If a basket is transformed, in order, with the status of those who validate the reservation, the reservation is then considered for other customers. If a reservation in a basket is no longer valid (because it\'s no more in the booking window, that an unavailability was added or that another client was faster) so she and removed with its product and a message tells the customer about the unavailability.'
			);
		}
		//-------------------- front cart ----------------------------------------
		if ($iso == 'fr') {
			self::$sectionContent['front']['cart'] = array (
				'img'		=> (_PS_VERSION_ >= "1.7.0.0" ? 'http://labulle.fr/wp-content/uploads/2015/06/myownr-front-panier-ps17.png' : 'http://labulle.net/wp-content/uploads/2015/06/myownr-front-panier1.png'),
				'help'		=>	'Le prix unitaire de la réservation, tiens compte des règles de prix panier ou catalogue mais pas des règles de prix du module.\nIl est possible de modifier la quantité de la réservation depuis le panier, à condition que l\'occupation du produit le permette, sinon un message d\'erreur indique qu\'il n\'y a plus de quantité disponible. Si l\'acompte est configuré une ligne après les produits indique le montant de l\'avance sur les produits (produits en vente y compris).\nSi la caution est configurée la ligne emballage cadeau est utilisée pour indiquer et facturer le montant de la caution, mais la caution peut être aussi affichée à titre informatif.',
			);
		} else {
			self::$sectionContent['front']['cart'] = array (
				'img'		=> (_PS_VERSION_ >= "1.7.0.0" ? 'http://labulle.net/wp-content/uploads/2015/06/myownr-front-cart-ps17.png' : 'http://labulle.net/wp-content/uploads/2015/06/myownr-front-panier1.png'),
				'help'		=>	'The unit price of the reservation, is taking into account the cart or catalog rules but not module price rules.\nYou can change the amount of the reservation from the cart as long as the occupation of the product allows, otherwise an error message indicates that there is more available quantity. If the deposit is configured a line after the product indicates the amount of the advance on products (including products for sale).\nIf security is configured gift packaging line is used to indicate and charge the amount of the bond, but the bond can also be displayed for information.',
			);
		}
		
				//-------------------- front cart ----------------------------------------
		if ($iso == 'fr') {
			self::$sectionContent['front']['popup'] = array (
				'img'		=> (_PS_VERSION_ >= "1.7.0.0" ? 'http://labulle.fr/wp-content/uploads/2015/06/myownr-front-popup-indisponible-ps17.png' : 'http://labulle.fr/wp-content/uploads/2015/06/myownr-front-popup-indisponible.png'),
				'help'		=>	'',
				'check' => 'Les réservations qui sont dans le panier du client sont vérifiées à tout moment et peuvent être retirées du panier'
			);
		} else {
			self::$sectionContent['front']['popup'] = array (
				'img'		=> (_PS_VERSION_ >= "1.7.0.0" ? 'http://labulle.net/wp-content/uploads/2015/06/myownr-front-popup-unavailable-ps17.png' : 'http://labulle.net/wp-content/uploads/2015/06/myownr-front-popup-unavailable.png'),
				'help'		=>	'',
				'check' 	=> 'Reservations that are in the customer cart are verified at all times and can be removed from cart'
			);
		}
		if ($iso == 'fr') {
			self::$adminOptions['popup']['check'] = array (
				'delay'	=> 'Le délai minimum entre la commande et le début des réservations est terminé. Cela est utile quand un panier abandonné edt restoré quand un client reviens.',
				'unavailabilities'	=>	'Une période d\'indisponibilité a été ajoutée',
				'reservations'		=> 'Une réservation a été passée par un autre client et validée. En effet il n\'y a pas de durée de validité pour une réservation passée le premier client qui paye voit sa réservation validée'
			);
		} else {
			self::$adminOptions['popup']['check'] = array (
				'delay'	=> 'The minimum delay between the order and reservations start has ended. It\'s usefull when a abandoned cart is restored when a old customer came back',
				'unavailabilities'	=>	'An unavailabilities has been added.',
				'reservations'		=> 'A reservation has neen placed by another customer and vzlidared. Indeed the is no validity time for a reservation the first ccustomer that pays it has a valodation done'
			);
		}
		
		//-------------------- front widgets ----------------------------------------
		if ($iso == 'fr') {
			self::$sectionContent['front']['widgets'] = array (
				'img'		=> (_PS_VERSION_ >= "1.7.0.0" ? 'http://labulle.fr/wp-content/uploads/2015/06/myownr-front-widget-acceuil-ps17.png' : 'http://labulle.fr/wp-content/uploads/2015/06/myownr-front-widget-acceuil.png'),
				'help'		=>	'',
				'position' => 'Il est possible d\'afficher le widget à plusieurs emplacements. Pensez à les activer dans la configuration du module et à les agencer depuis le menu modules>position de Prestashop',
				'select'	=> 'Il existe deux modes de widget de colonne',
				'pro'		=>	'Avec la version PRO le changement de catégorie permet aussi d\'avoir les règles de la catégorie sélectionnée appliquée. Si la catégorie affichée depend de la famille de produit pour laquelle la selection a été faite, alors la période sélectionnée est mise en avant, et les produits de la liste sont filtrés en fonction de leurs disponibilités.',
				'search'	=> 'Le mode recherche est moins contraignant car il propose un champ de recherche et deux champs de dates sans contrainte. En outre le période n\'est pas enregistrée donc les disponibilités sont affichées uniquement pour les resultats de recherche.',
				'category' => 'Vous pouvez afficher un selecteur de catégorie si vous choisissez plusieurs catégories de même niveau plutôt que l\'acceuil dans les règles de réservation',
			);
		} else {
			self::$sectionContent['front']['widgets'] = array (
				'img'		=> (_PS_VERSION_ >= "1.7.0.0" ? 'http://labulle.net/wp-content/uploads/2015/06/myownr-front-widget-home-ps17.png' : 'http://labulle.net/wp-content/uploads/2015/06/myownr-front-widget-home.png'),
				'help'		=>	'',
				'position' => 'You can display the widget on multiple locations. Don\'t forget to activate them from module configuration and arrange them from module>position menu of Prestashop',
				'select'	=> 'There is two kind of columns widgets',
				'pro'		=>	'With version PRO the category change also allows to have the rules applied in the selected category. If the displayed category depends upon the product family for which the selection was made, then the selected period is highlighted, and the list of products are filtered according to their availability.',
				'search'	=> 'The search mode is less restrictive because it offers a search field and two fields of dates without constraint. In addition, the period is not recorded so availability are only displayed for the search results.',
				'category' => 'You can add a categry choice selector in you select multple same level caegories instead of choosing home in reservation rules',
			);
		}

		
		//--------------------front selection positions ----------------------------------------
		if ($iso == 'fr') {
			self::$adminOptions['widgets']['position'] = array (
				'home'	=> 'Vous pouvez afficher le widget en page d\'acceuil, ce dernier comporte 2 selecteurs de dates pour le début et la fin (seulement un si la durée est fixée). Pour aller plus vite les date de début au plus tôt sont affichées en fonction de la catégorie sélectionnée et la règle de réservation correspondante.',
				'columns'	=>	'Le widget colonne affiche le planning en deux étapes : début puis fin en fonction de la catégorie en cours et de sa règle de réservation.',
			);
		} else {
			self::$adminOptions['widgets']['position'] = array (
				'home'	=> 'You can display the widget on home page, it is composed by 2 date selector for start and end (only one if length is fixed). To go faster the earlier start date are displayed depending on category selected and corresponding price rule.',
				'columns'	=>	'The column widget display the planning in 2 steps : the start then the end depending on current category and it\'s reservation rule.',
			);
		}
		
		//--------------------front selection options ----------------------------------------
		if ($iso == 'fr') {
			self::$adminOptions['widgets']['select'] = array (
				'select'	=> 'Le mode selection permet d\'avoir un widget avec un mini planning qui permet de faire respecter les contraintes des règles de réservation. Ce widget permet de sélectionner la catégorie de produit (celle en cours est sélectionnée).',
				'pro'		=>	'Avec la version PRO le changement de catégorie permet aussi d\'avoir les règles de la catégorie sélectionnée appliquée. Si la catégorie affichée depend de la famille de produit pour laquelle la selection a été faite, alors la période sélectionnée est mise en avant, et les produits de la liste sont filtrés en fonction de leurs disponibilités.',
				'search'	=> 'Le mode recherche est moins contraignant car il propose un champ de recherche et deux champs de dates sans contrainte. En outre le période n\'est pas enregistrée donc les disponibilités sont affichées uniquement pour les resultats de recherche.',
			);
		} else {
			self::$adminOptions['widgets']['select'] = array (
				'select'	=> 'The selection mode allows a widget with a mini schedule that allows to enforce constraints booking rules. This widget lets you select the product category (the current one is selected).',
				'pro'		=>	'With version PRO the category change also allows to have the rules applied in the selected category. If the displayed category depends upon the product family for which the selection was made, then the selected period is highlighted, and the list of products are filtered according to their availability.',
				'search'	=> 'The search mode is less restrictive because it offers a search field and two fields of dates without constraint. In addition, the period is not recorded so availability are only displayed for the search results.',
			);
		}
		
		
		//-------------------- front widgets ----------------------------------------
		if ($iso == 'fr') {
			self::$sectionContent['front']['home'] = array (
				'help'		=>	'Il est possible d\'activer depuis la configuration un onglet des produits disponibles en page d\'acceuil',
				'products'  =>'',
				'availabilities'  =>'',
			);
		} else {
			self::$sectionContent['front']['home'] = array (
				'help'		=>	'It is possible to activate from the configuration a tab of the products available on the homepage',
				'products'  =>'',
				'availabilities'  =>'',
			);
		}
		if ($iso == 'fr') {
			self::$adminOptions['home']['products'] = array (
				'img'		=> (_PS_VERSION_ >= "1.7.0.0" ? 'http://labulle.fr/wp-content/uploads/2015/06/myownr-front-widget-produits-disponibles-ps17.png' : 'https://labulle.fr/wp-content/uploads/2015/06/myownr-front-widget-produits-disponibles.png'),
				'help'	=> 'Ce mode là est utile si il y a peu de disponibilités par produit car il affiche uniquement les produits disponibles que vous pouvez ajouter directement au panier. Sinon préférez le mode suivant.',
			);
			self::$adminOptions['home']['availabilities'] = array (
				'img'		=> (_PS_VERSION_ >= "1.7.0.0" ? 'http://labulle.fr/wp-content/uploads/2015/06/myownr-front-widget-disponibilites-produits-ps17.png' : 'http://labulle.fr/wp-content/uploads/2015/06/myownr-front-widget-disponibilites-produits.png'),
				'help'	=> 'Ce mode "disponibilités par produit" affiche chaque produit et son planning avec les quantités disponibles. Il n\'est pas possible de faire de reservation depuis ce tableau mais uniquement d\'aller vers les produits.',
			);
		} else {
			self::$adminOptions['home']['products'] = array (
				'img'		=> (_PS_VERSION_ >= "1.7.0.0" ? 'http://labulle.net/wp-content/uploads/2015/06/myownr-front-widget-products-available-ps17.png' : 'http://labulle.net/wp-content/uploads/2015/06/myownr-front-widget-products-available.png'),
				'help'	=> 'This mode is useful if there is a few availability by product because it shows only available products that you can add to cart directly. Otherwise, please check next mode',
			);
			self::$adminOptions['home']['availabilities'] = array (
				'img'		=> (_PS_VERSION_ >= "1.7.0.0" ? 'http://labulle.net/wp-content/uploads/2015/06/myownr-front-widget-availabilities-products-ps17.png' : 'http://labulle.net/wp-content/uploads/2015/06/myownr-front-widget-availabilities-products.png'),
				'help'	=> 'This mode "Availability by product" displays each product and its schedule with quantity available. It is not possible to make reservation, since this table but only to go to the products.',
			);
		}
		
		//-------------------- front products ----------------------------------------
		if ($iso == 'fr') {
			self::$sectionContent['front']['products'] = array (
				'img'		=> (_PS_VERSION_ >= "1.7.0.0" ? 'http://labulle.fr/wp-content/uploads/2015/06/myownr-front-facette-ps17.png' : 'http://labulle.fr/wp-content/uploads/2015/06/myownr-front-facette1.png'),
				'help'		=>	'Par defaut le module afiche tous les produits en réservation mais retire le bouton d\'ajout au panier et ajoute l\'unité de temps après le prix du produit.\n Si une période a été sélectionnée abec le widget alors la disponibilité des produits est affichée et le prix du produit est mis à jour pour toute la période. Depuis la configuration vous pouvez choisir de cacher les produits qui ne sont pas disponibles à cause des disponibilités ou des réservations validées.',
			);
		} else {
			self::$sectionContent['front']['products'] = array (
				'img'		=> (_PS_VERSION_ >= "1.7.0.0" ? 'http://labulle.net/wp-content/uploads/2015/06/myownr-front-layered-ps17.png' : 'http://labulle.net/wp-content/uploads/2015/06/myownr-front-layered.png'),
				'help'		=>	'By default the module displays all reservation products but removes the add ro cart button and add the time unit after the product price.\n If a period has been selected with the widget then product availability is displayed and product price is updated for the whole period. From the configuration you can choose to hide products that are not available due to availabilities or validated reservations.',
			);
		}
		
		//-------------------- front confirmation ----------------------------------------
		if ($iso == 'fr') {
			self::$sectionContent['front']['confirmation'] = array (
				'img'		=> (_PS_VERSION_ >= "1.7.0.0" ? 'http://labulle.fr/wp-content/uploads/2015/06/myownr-front-commande-ps17.png' : 'http://labulle.fr/wp-content/uploads/2015/06/myownr-front-commande1.png'),
				'help'		=>	'A la fin du processus de commande si des notifications sont cochées, elles sont affichées et proposent le téléchargement de fichier iCal par exemple. Les réservations (leurs dates et leurs statuts) seront ensuite confirmés dans l\'email de récapitulatif de la commande, la facture les détails de la commande depuis son compte client.',
			);
		} else {
			self::$sectionContent['front']['confirmation'] = array (
				'img'		=> (_PS_VERSION_ >= "1.7.0.0" ? 'http://labulle.net/wp-content/uploads/2015/06/myownr-front-order-ps17.png' : 'http://labulle.net/wp-content/uploads/2015/06/myownr-front-order.png'),
				'help'		=>	'At the end of the ordering process whether notifications are checked, they are displayed and offer downloading iCal file for example. Reservations (their dates and status) will subsequently confirmed in email summary of the order, the invoice details of the order from its customer account.',
			);
		}
		
		//=====================================================================
		//						INSTALL
		//=====================================================================	
		
		//-------------------- INSTALL SECTIONS --------------------
		if ($iso == 'fr') {
			self::$sectionNames['install'] = array (
				'download'		=> 'Téléchargement',
				'install'		=> 'Installation',
				'register'		=> 'Enregistrement',
				'update'		=> 'Mise à jour'
			);
		} else {
			self::$sectionNames['install'] = array (
				'download'		=> 'Download',
				'install'		=> 'Install',
				'register'		=> 'Registration',
				'update'		=> 'Update'
			);
		}
		
		//-------------------- SECTIONS --------------------
		if ($iso == 'fr') {
			self::$sections['install'] = array (
				'download'		=> ' ',
				'install'		=> '',
				'register'		=> '',
				'update'		=> 'Pour obtenir les dernières mises à jour connectez vous à votre compte sur labulle.net.'
			);
		} else {
			self::$sections['install'] = array (
				'download'		=> ' ',
				'install'		=> '',
				'register'		=> '',
				'update'		=> 'To get the latest updates connect to your account on labulle.net'
			);
		}
		
		//-------------------- download --------------------
		if ($iso == 'fr') {
			self::$sectionContent['install']['download'] = array (
				'download'		=>	'Pour télécharger le module, connectez-vous à la place de marché où vous avez procédé à l\'achat',
				'version'	=> 'Pour la version 1.4 de Prestashop seule la version 2.x du module est utilisable, la version 3 est compatible avec PS 1.5 à 1.6.',

			);
		} else {
			self::$sectionContent['install']['download'] = array (
				'download'		=>	'To download the module, log on to the marketplace where you have made purchase',
				'version'	=> 'For version 1.4 of Prestashop only 2.x version of the module is usable, version 3 is compatible with PS 1.5 to 1.6.'
			);
		}
		
		//-------------------- download download --------------------
		if ($iso == 'fr') {
			self::$adminOptions['download']['download'] = array (
				'addons'	=> 'Si vous avez commandé sur Prestashop Addons connectez-vous sur http://addons.prestashop.com/ puis dans la rubrique téléchargements. Pour avoir une version plus fréquente vous pouvez vous rendre sur la page http://labulle.fr/clients-addons/ et renseignez vos informations de commande.',
				'labulle'	=>	'Si vous avez commandé sur laBulle.net connectez-vous sur http://labulle.fr/mon-compte/, repérez le module et cliquez sur le bouton \'téléchargement\' un ? vous indiquera éventuellement les changements effectués.',
			);
		} else {
			self::$adminOptions['download']['download'] = array (
				'addons'	=> 'If you ordered on PrestaShop Addons log on http://addons.prestashop.com/ then in the downloads section. To have a more frequent version you can go to the http://labulle.net/addons-customers/ page and enter your order information.',
				'labulle'	=> 'If you ordered on laBulle.net login to https://labulle.net/my-account/, find the module and click the \'Download\' button, a ? you possibly indicate the changes.',
			);
		}
		
		//-------------------- install --------------------
		if ($iso == 'fr') {
			self::$sectionContent['install']['install'] = array (
				'requirement'	=>	'Pour fonctionner correctement certains fichiers doivent être copiés dans le répertoire « override » de Prestashop.
Pour cela il faut ajouter les droits en écriture pour « tout le monde » sur ce dossier avec votre client FTP ou en tapant la commande suivante depuis le répertoire Prestashop en SSH : “chmod 777 -r override”.',
				'upload'	=> 'Pour télécharger le module sur votre boutique Prestashop Addons il existe deux méthodes :',
				'install'	=>	'Cliquez ensuite simplement sur le bouton Installer depuis la liste des modules.',

			);
		} else {
			self::$sectionContent['install']['install'] = array (
				'requirement'	=>	'To run properly some files need to be copied to “override” folder of Prestashop. For that, write rights must be added for “everybody” on that folder with your FTP client or by taping the following command by SSH from the Prestashop folder : “chmod 777 -r override”.',
				'upload'	=> 'To download the module on PrestaShop Addons your shop there are two methods:',
				'install'	=> 'Once sent the module appears in the list of modules. Just click on the Install button.'
			);
		}
		
		//--------------------install upload --------------------
		if ($iso == 'fr') {
			self::$adminOptions['install']['upload'] = array (
				'admin'	=> 'via la gestion des modules en back-office : Sélectionnez juste le fichier .zip qui a été mis à votre disposition.',
				'ftp'	=>	'Via un client FTP : Décompressez le module et déposez le dossier « myOwnReservations » dans le dossier /modules/ du dossier Pretashop sur votre serveur web.',
			);
		} else {
			self::$adminOptions['install']['upload'] = array (
				'admin'	=> 'Through module admin on back-office : Just select the zip file that aas been placed at your disposal',
				'ftp'	=> 'Through a FTP client : Unzip the module and drop the folder « myOwnReservations » in the folder /modules/ on the Pretashop folder on your web server.',
			);
		}
		
		//-------------------- front confirmation --------------------
		if ($iso == 'fr') {
			self::$sectionContent['install']['register'] = array (
				'help'	=>	'Pour activer le module l\'enregistrement d\'une clé de licence est nécessaire, sans cela la partie front-office est désactivée. \nPour obtenir la clé connectez-vous sur votre compte www.labulle.net afin de générer celle-ci pour votre nom de domaine. Si vous de disposez pas encore d\'un compte remplissez le formulaire à l\'adresse http://labulle.fr/clients-addons/ à l\'aide de votre facture Prestashop Addons.',
				'local'	=> 'Si vous êtes sur un serveur de test qui tourne en local (IP en 192.169.0.X ou en 127.0.0.1, hôte en localhost) alors il n\'est pas nécessaire d\'indiquer une clé pour que le front office s\'affiche.',
				'test'	=>	'Il vous sera possible de modifier votre clé de license seulement si cette dernière contient "test." sinon il vous faudra nous indiquer votre nom de domaine pour que l\'on vous génère une autre clé de licence par email.',

			);
		} else {
			self::$sectionContent['install']['register'] = array (
				'help'	=>	'To enable the module registration of a license key is required, otherwise the front office part is disabled.\nTo get a key log onto your account www.labulle.net to generate it for your domain name. If you not already have an account fill the form at http://labulle.fr/clients-addons/ with your Prestashop Addons invoice.',
				'local'	=> 'If you are on a test server running locally (IP 192.169.0.X or 127.0.0.1, host localhost) then it is not necessary to indicate a key for the front office to display',
				'test'	=> 'You will be able to change your license key only if it contains "test." otherwise indicate us your domain name so that we generate you another license key by email.'
			);
		}
		
			//-------------------- front confirmation --------------------
		if ($iso == 'fr') {
			self::$sectionContent['install']['update'] = array (
				'requirement'	=> 'Avant de mettre à jour le module il est fortement conseiller de faire une sauvegarde de la base de donnée et des fichiers. \nPensez en particulier à sauvegarder vos templates modifiés ou à les déplacer dans le dossier /themes/<votre theme>/modules/myownreservations/templates/',
				'numbered'		=> 'Pour mettre à jour le module :'
			);
		} else {
			self::$sectionContent['install']['update'] = array (
				'requirement'	=> 'Before updating the module it is strongly advised to make a backup of the database and files. \nEspecially think to save your modified templates or move them in the /themes/<your theme>/modules/myownreservations/templates/',
				'numbered'		=> 'To update the addon :'
			);
		}
		//-------------------- front confirmation --------------------
		if ($iso == 'fr') {
			self::$adminOptions['update']['numbered'] = array (
			 '',
				'envoyez la dernière version depuis l\'ajout de module dans le back-office sans desinstaller l\'ancien module.',
				'vérifiez ensuite dans la configuration du module qu\'aucune mise à jour n\'est à confirmer',
				'effacez le cache si vous êtes sur Prestashop 1.4 ou inférieur',
				'effacez le fichier cache/class_index.php si vous êtes sur Prestashop 1.6.'
			);
		} else {
			self::$adminOptions['update']['numbered'] = array (
				'',
				'send the last version using module upload menu in the back-office without uninstalling the older version.',
				'next check that any update has to be confirmed in the module configuration',
				'delete the cache if you\'re on Prestashop 1.4 or earlier',
				'delete the file cache/class_index.php if you\'re on Prestashop 1.6.',
			);
		}
		
		//=====================================================================
		//						USE CASES
		//=====================================================================	
		if ($iso == 'fr') {
			self::$sectionNames['usecases'] = array (
				'holidays'		=> 'Séjours',
				'host'			=> 'Hébergement',
				'reservation'	=> 'Réservation',
				'event'			=> 'Evénement',
				'transport'		=> 'Transport',
				'night'			=> 'Nuitée',
				'prestation'	=> 'Prestation'
			);
			self::$sections['usecases'] = array (
				'holidays'		=> 'Les produits en vente sont des semaines de séjour à l\'étranger, chaque produit est un destination et des options permettent de choisir le type de pension souhaité.',
				'host'			=> 'Le module peut être utilisé pour de la location longue durée comme de la location meublée ou du credit bail. Le cas qui nous préoccupe présentement est la location d\'appartements',
				'reservation'	=> 'Il existe plusieurs produits correspondant à des prestations de durée fixe. Le client peut réserver plusieurs créneaux et les créneaux horaires sont communs entre les produits.',
				'event'			=> 'Le module peut être utilisé pour la réservation sans durée pour le cas d\'événements ou le nombre de place dépend de la date.',
				'transport'		=> 'Il existe des cas particuliers comme pour les transports ou l\'on souhaite réserver une horaire différente pour le départ ou de retour du trajet.',
				'night'			=> 'Le module est bien adapté pour la location de chambre d\'hôtels car il permet de compter les nuits et non pas les journées.',
				'prestation'	=> 'Les produits sont des prestations de durées variables pour lequel on souhaite proposer la prise de rendez-vous.'
			);
		} else {
			self::$sectionNames['usecases'] = array (
				'holidays'		=> 'Holidays',
				'host'			=> 'Hosting',
				'reservation'	=> 'Reservation',
				'event'			=> 'Event',
				'transport'		=> 'Transport',
				'night'			=> 'Night',
				'prestation'	=> 'Prestation'
			);
			self::$sections['usecases'] = array (
				'holidays'		=> 'Sale products are week stay abroad, each is a destination and options allow you to select the desired type of pension.',
				'host'			=> 'The module can be used for long term rental as furnished rentals or lease credit. The case that concerns us now is renting apartments.',
				'reservation'	=> 'There are several products corresponding to fixed-term benefits. The customer can reserve multiple slots and slots are shared between products.',
				'event'			=> 'The add-on can be used for the reservation without time for the case of events while the number of seat depends on the date.',
				'transport'		=> 'There are special cases such as for transport where you wish to book a different time for departure or return journey.',
				'night'			=> 'The module is well suited for rental of hotels room because he used to count the nights and not days.',
				'prestation'	=> 'Products are prestations with varying duration for which you want a proposal to make an appointment.'
			);
		}
		
		//-------------------- USECASE holidays --------------------
		if ($iso == 'fr') {
			self::$sectionContent['usecases']['holidays'] = array (
				'img'	=>	'http://labulle.fr/wp-content/uploads/2015/06/myownr-front-planning-semaine1.png',
				'help'	=> 'C\'est est cas un d\'utilisation assez frequent qui n\'est pas compliqué à paramètrer avec le mode de selection semaine. Les tarifs saisonniers peuvent être configurés avec des règles de prix de type date mais ici de nombreux prix spécifiques nécessitent l\'utilisation de tableaux.',
				'config'	=>	'',
			);
			self::$adminOptions['holidays']['config'] = array (
				'information'	=> 'Plusieurs catégories correspondant aux destinations sont cochées et l\'occupation de produit est sur intangible car les déclinaisons sont utilisées pour le type de pension, on prendra donc la quantité de la déclinaison par défaut.',
				'planification'	=>	'La sélection se fait pas semaine avec un debut de semaine au lundi et la fin au dimanche. La durée est fixée de 1 à 3 semaines et le tarif "à partir de" est affiché.',
				'tarif' => 'Le tarif des réservation utilise un tableau des tarifs selon la période car les semaines en pleine saison sont plus chères et inversement en basse saison. L\'acompte est configuré à 30%.'
			);
		} else {
			self::$sectionContent['usecases']['holidays'] = array (
				'img'	=>	'http://labulle.net/wp-content/uploads/2015/06/myownr-front-planning-week.png',
				'help'	=> 'It\'s is a fairly frequent use of that is not complicated to configure with the week selection. Seasonal rates can be configured with period price rules but here many specific prices require the use of tables.',
				'config'	=> ''
			);
			self::$adminOptions['holidays']['config'] = array (
				'information'	=> 'Several categories corresponding to the destinations are checked and product occupation is on intangible because variations are used for the type of pension, it will therefore take the amount of the default combination.',
				'planification'	=>	'Selection is done by week with a week beginning Monday and ending Sunday. The duration is set to 1 to 3 weeks and the rate "from" is displayed.',
				'tarif'	=> 'The booking fee uses a table of rates by period for the weeks during the high season are more expensive and vice versa in low season.The deposit is set at 30%.',
			);
		}
		
		//-------------------- USECASE host --------------------
		if ($iso == 'fr') {
			self::$sectionContent['usecases']['host'] = array (
				'img'	=>	'http://labulle.fr/wp-content/uploads/2015/06/myownr-front-planning-mois1.png',
				'help'	=> 'La configuration est assez simple car le module va proposer des mois complets et le durée n\'est pas limitée. Il faut juste penser à mettre en place la caution.',
				'config'	=>	'',
			);
			self::$adminOptions['host']['config'] = array (
				'information'	=> 'L\'occupation produit est de type produit réel et la quantité est fixée à 1.',
				'planification'	=>	'La selection se fait par mois (du 1er au dernier jour du mois) sur un planning par an et la fenêtre commence à J+10 pendant 300 jours (car on peut s\'y prendre au plus tard 10 jours avant le début de la location.',
				'tarif' => 'Le prix de la réservation est le prix du produit hors durée et l\'acompte est à 100% car on ne paye qu\'un seul mois d\'avance. Une caution est fixée à 1000€ mais n\'est pas facturée. Le statut paiement accepté est coché car il n\'est pas nécessaire de payer l\'acompte pour bloquer la réservation.'
			);
		} else {
			self::$sectionContent['usecases']['host'] = array (
				'img'	=>	'http://labulle.net/wp-content/uploads/2015/06/myownr-front-planning-month.png',
				'help'	=> 'The setup is quite simple because the module will offer full month and the length is not limited. Just remember to set up the deposit.',
				'config'	=> ''
			);
			self::$adminOptions['host']['config'] = array (
				'information'	=> 'The occupation is set to real product because and quantity is fixed at 1.',
				'planification'	=>	'The selection is done a month (from 1st to last day of month) on a schedule per year and the window starts at D + 10 for 300 days (because you can take no later than 10 days before the start of the rental.',
				'tarif'	=> 'The reservation price is the price of the product without duration and deposit is 100% because it pays only one month for advance. A deposit is set at € 1,000 but is not charged. The status "payment accepted" is checked because it\'s not necessary to pay the deposit to secure the booking.',
			);
		}

		
		//-------------------- USECASE reservation --------------------
		if ($iso == 'fr') {
			self::$sectionContent['usecases']['reservation'] = array (
				'img'	=>	'http://labulle.fr/wp-content/uploads/2015/06/myownr-front-planning-fixe1.png',
				'help'	=> 'La configuration initiale est rapide par contre la difficulté porte sur les car particuliers (créneaux désactivés en fonction des produits) que vous pouvez ajouter avec les indisponibilités.',
				'config'	=>	'',

			);
			self::$adminOptions['reservation']['config'] = array (
				'information'	=> 'L\'occupation produit est de type produit réel car il n\'y a pas d\'options, la quantité de chaque produit correspond donc aux nombre de places par prestation.',
				'planification'	=>	'La selection se fait par créneau horaire et la durée est fixée à 1 afin de pouvoir autoriser les selections multiples de créneaux.',
				'tarif' => 'Le tarif de la réservation est le prix du produit par la durée afin de multiplier le tarif par le nombre de créneaux sélectionnés.'
			);
		} else {
			self::$sectionContent['usecases']['reservation'] = array (
				'img'	=>	'http://labulle.net/wp-content/uploads/2015/06/myownr-front-planning-fixed.png',
				'help'	=> 'Initial setup is fast but the difficulty is against the individuals cases (time slots disabled by product) that you can add with unavailabilities.',
				'config'	=> ''
			);
			self::$adminOptions['reservation']['config'] = array (
				'information'	=> 'The occupation product type real product because there isn\'t options, the quantity of each product corresponds to the number of seats per activity.',
				'planification'	=>	'The selection is done by time slot is the period is set to 1 in order to allow multiple time slots selections.',
				'tarif'	=> 'The reservation rate is the price of the product by the duration in order to multiply the price by the number of selected time slots.',
			);
		}

		
		//-------------------- USECASE event --------------------
		if ($iso == 'fr') {
			self::$sectionContent['usecases']['event'] = array (
				'img'	=>	'http://labulle.fr/wp-content/uploads/2015/06/myownr-front-planning-evennement1.png',
				'help'	=> 'Les produits sont les événements qui n\'ont lieu qu\'une fois par jour au maximum et dont les horaires sont indiqués sur la fiche produit.',
				'config'	=>	'La configuration de ce cas est simple mais nécessite la création d\'une disponibilité pour chaque évènement.',

			);
		} else {
			self::$sectionContent['usecases']['event'] = array (
				'img'	=>	'http://labulle.net/wp-content/uploads/2015/06/myownr-front-planning-event.png',
				'help'	=> 'The products are the events that will take place once a day at most and whose schedules are indicated on the product sheet.',
				'config'	=> 'The configuration of this case is simple but requires the creation of availability for each event.'
			);
		}
		//-------------------- event options --------------------
		if ($iso == 'fr') {
			self::$adminOptions['event']['config'] = array (
				'information'	=> 'L\'occupation est de type produit variable car le nombre de places depend de la date.',
				'planification'	=>	'La selection se fait par jour pour tous les jours de la semaine, la durée ignorée. Des disponibilités sont créées pour indiquer les jours d’événement des produits. Les quantités disponibles sont affichés sur la planning. Comme la durée est fixe on peut autoriser la selection multiple.',
				'tarif' => 'Le prix de la réservation est le prix du produit hors durée car celle-ci est ignorée et l\'acompte est à 100%'
			);
		} else {
			self::$adminOptions['event']['config'] = array (
				'information'	=> 'Occupation is set on type "variable" because the number of seats depends on the date.',
				'planification'	=>	'The selection is made by day for every day of the week, the duration is ignored. Availability are created to indicate event day for each product. The quantities available are displayed on the schedule. As the duration is fixed we can allow multiple selection.',
				'tarif'	=> 'The price of the reservation is the price of the product without length as it is ignored and the deposit is 100%.',
			);
		}
		
		//-------------------- USECASE transport --------------------
		if ($iso == 'fr') {
			self::$sectionContent['usecases']['transport'] = array (
				'img'	=>	'http://labulle.net/wp-content/uploads/2015/06/myownr-front-planning-sens.png',
				'help'	=> 'Chaque produit est un trajet qui peut être un aller un retour ou un aller/retour et les créneaux horaires ne sont pas les mêmes pour l\'aller et le retour.',
				'config'	=>	'Le trajet nécessite un véhicule avec un certain nombre de places, si il est dépasse un véhicule supplémentaire sera nécessaire.',

			);
		} else {
			self::$sectionContent['usecases']['transport'] = array (
				'img'	=>	'http://labulle.net/wp-content/uploads/2015/06/myownr-front-planning-way.png',
				'help'	=> 'Each product is a path which can be a go or a return or a go/return and slots are not the same for go and return.',
				'config'	=> 'The path requires a vehicle with a number of places, if it exceeds a further vehicle is necessary.'
			);
		}
		//-------------------- transport options --------------------
		if ($iso == 'fr') {
			self::$adminOptions['transport']['config'] = array (
				'information'	=> 'L\'occupation est de type produit partagé et chaque produit peut supporter jusqu\'à 6 places.',
				'planification'	=>	'La selection se fait pas créneaux horaires et des créneaux de 30 minutes sont créés de 8Hà 20H le créneaux commençant à l\'heure pile sont pour le début et les autres pour la fin. La durée est sur aucune, l\'option avancée double selection est en fonction du client.',
				'tarif' => 'Le tarif est pour la réservation, l\'option pour facturer en double si il a deux dates différentes est cochée et l\'acompte est de 100%'
			);
		} else {
			self::$adminOptions['transport']['config'] = array (
				'information'	=> 'The occupation is shared product type and each product can support up to 6 places.',
				'planification'	=>	'The selection is not slots and 30 minutes slots are created 8ha 20H starting the slots are on the hour for the start and the other to the end. Time is on no, advanced dual selection option is based on the client.',
				'tarif'	=> 'Rate is for reservation, the option to charge double if there are two different dates is checked and the deposit is 100%',
			);
		}
		
		//-------------------- USECASE night --------------------
		if ($iso == 'fr') {
			self::$sectionContent['usecases']['night'] = array (
				'img'	=>	'http://labulle.fr/wp-content/uploads/2015/06/myownr-front-planning-mois1.png',
				'help'	=> 'Les réservations sont facturées à la nuit pour cela on crée 2 demi créneaux horaires le premier pour l\'arrivée et jusqu\'à minuit et l\'autre de minuit à l\'heure de départ.',
				'config'	=>	'',

			);
			self::$adminOptions['night']['config'] = array (
				'information'	=> 'Le produit est de type produit intangible car la déclinaison sert à facturer en fonction du nombre de personnes mais la quantité est bloquée à 1 pour éviter les confusions.',
				'planification'	=>	'La sélection se fait par créneau horaire mais la durée est illimitée et est comptée en jour afin de faciliter les règles de prix. Un aperçu du prix est affiché car la selection se fait en deux étapes.',
				'tarif' => 'Le tarif est en fonction de la durée mais la durée est comptée en jours et "Exclure le jour de fin" est coché afin de compte les nuits et pas les journées'
			);
		} else {
			self::$sectionContent['usecases']['night'] = array (
				'img'	=>	'http://labulle.net/wp-content/uploads/2015/06/myownr-front-planning-night.png',
				'help'	=> 'Reservations are billed at night for that : creates 2 half slots one from the  arrival to midnight and another from midnight to departure time.',
				'config'	=> ''
			);
			self::$adminOptions['night']['config'] = array (
				'information'	=> 'The product is intangible product such as the declination is used to charge based on the number of people but the amount is blocked at 1 to avoid confusion.',
				'planification'	=>	'The selection is made by slot but the duration is unlimited and is counted as day to facilitate price rules. An overview of the price is displayed because the selection is done in two steps.',
				'tarif'	=> 'The rate is based on the duration but the duration is counted in days and "Exclude end of the day" is checked to account nights and not days.',
			);
		}
		
			//-------------------- USECASE prestation --------------------
		if ($iso == 'fr') {
			self::$sectionContent['usecases']['prestation'] = array (
				'img'	=>	'http://labulle.net/wp-content/uploads/2015/06/myownr-front-planning-duree.png',
				'help'	=> 'La particularité de cas est que la durée de la réservation n\'est pas au choix du client mais depend de la prestation. Et aussi la durée étant exprimée en nombre de créneaux horaires il convient de les créer avec des durées identiques.',
				'config'	=>	'',

			);
			self::$adminOptions['prestation']['config'] = array (
				'information'	=> 'Une catégorie spécifique est sélectionnée car une partie des produits sont des prestations. L\'occupation produit est de type produit virtuel car les produits ont des déclinaisons qui font varier la durée mais pas le produit qui est le même.',
				'planification'	=>	'La sélection se fait par créneaux horaires de 1 heure activés du lundi au vendredi sur un planning par semaine. La durée est fixée par produit et par déclinaison.',
				'tarif' => 'C\'est le prix du produit pour la reservation car il ne dépend pas de la durée. 1 règle de prix est crée : une de délai  pour une réduction de 15% pour les réservations commandées 1 mois à l\'avance.  Une autre règle de prix de durée pour offrir le 7ème jour (de 7 à 7 jours ou plus avec prix unitaire à 0€).'
			);
		} else {
			self::$sectionContent['usecases']['prestation'] = array (
				'img'	=>	'http://labulle.net/wp-content/uploads/2015/06/myownr-front-planning-night.png',
				'help'	=> 'The cases feature is that the duration of the booking is not on customer choice but depends on the product. And the duration is expressed in number of slots so it must be created with the same periods.',
				'config'	=> ''
			);
			self::$adminOptions['prestation']['config'] = array (
				'information'	=> 'A specific category is selected as part of the goods are benefits. Product occupation type is virtual product because the products have variations that vary the duration, but not the product that is the same.',
				'planification'	=>	'The selection is made by slots of one hour activated from Monday to Friday on a schedule week. The duration is determined by product and by declination.',
				'tarif'	=> 'It is the price of the product for the booking because it doesnt\'t depend on length. 1 price rule is created: a time for a 15% discount for bookings ordered one month in advance. Another term price adjusts itself to the 7th day (from 7 to 7 days or more with unit price € 0).',
			);
		}
		
		//=====================================================================
		//						CUSTOMIZE
		//=====================================================================	
		if ($iso == 'fr') {
			self::$sectionNames['customize'] = array (
				'translations'	=> 'Traductions',
				'templates'		=> 'Templates',
				'classes'		=> 'Classes',
			);
			self::$sections['customize'] = array (
				'translations'	=> '',
				'templates'		=> '',
				'classes'		=> '',
			);
		} else {
			self::$sectionNames['customize'] = array (
				'translations'	=> 'Translation',
				'templates'		=> 'Templates',
				'classes'		=> 'Classes',
			);
			self::$sections['customize'] = array (
				'translations'	=> '',
				'templates'		=> '',
				'classes'		=> '',
			);
		}
		
		//-------------------- translations --------------------
		if ($iso == 'fr') {
			self::$sectionContent['customize']['translations'] = array (
				'help'		=> 'Il y a principalement deux types de traduction les phrases et libellés ou des dates et heures.',
				'labels'	=> 'D\'abord regardez le paramètrage du module car quelques libellés sont personnalisables directement depuis les règles de réservations ou la configuration.\nEnsuite vous pouvez modifier les traductions depuis le menu du back-office Localisation > Traduction, choisissez Traductions des modules comme type puis le coeur comme theme.',
				'dates'		=> 'En raison d\'un trop grand nombre de problèmes avec les traductions de dates par les systèmes d\'exploitation, les traductions sont faites par le module.\nPour modifier les traductions des mois ou des jours il faut changer les variables de la classe myOwnLang qui commencent par $days ou $months suivi du code Iso de la langue dans le fichier classes/lang.php.'

			);
		} else {
			self::$sectionContent['customize']['translations'] = array (
				'help'		=> 'There are two main types of translation sentences and labels, or dates and times.',
				'labels'	=> 'First look at the module settings as some labels are customizable directly from the reservation rules or configuration.\nThenr you can edit translations from the back office menu Location> Translation, choose modules translations as the type and the core as theme.',
				'dates'		=> 'Due to too many problems with the translations dates by the system operation, the translations are done by the module. \nTo edit translations months or days we must change the class variables myOwnLang starting with $days or $months followed by Iso language code in the  file classes/lang.php.'
			);
		}
		
		if ($iso == 'fr') {
			self::$sectionContent['customize']['classes'] = array (
				'availabilities'	=> 'La classe permet d\'obtenir les disponibilités pour une période et de determiner si un produit est disponible sur un créneau.',
				'availability'		=> 'La classe est l\'objet des disponibilités ou indisponibilités.',
				'calendar'			=> 'Cette casse statique contient les traductions de dates.',
				'calendarDay'		=> 'L\'objet jour permet de générer les tranches de temps en fonction des créneaux horaires.',
				'calendarMonth'		=> 'L\'objet mois permet de générer des objets jours et tous les tranches de temps du mois.',
				'calendarTimeSlice'	=> 'L\'objet représente une tranche de temps, sa description, sa disponibilité et quantité pour le produit en cours.',
				'calendarTool'		=> 'Cette classe permet de formater des périodes ou d\'en générer.',
				'calendarView'		=> 'L\'objet représente une vue de planning de sélection pour un produit ou widget généré à partir des tranches de temps.',
				'calendarWeek'		=> 'L\'objet représente une semaine et permet de générer des journées.',
				'cart'				=> 'La classe représente une réservation dans le panier pour un produit.',
				'carts'				=> 'La classe représente le panier des réservations du client.',
				'const'				=> 'La classe statique contient toutes les constantes et leur arborescence.',
				'help'				=> 'Cette casse statique contient les traductions d\'aide et permet de générer la doc.',
				'lang'				=> 'Cette casse statique contient les traductions de constantes et énumérations.',
				'myownutils'		=> 'Cette classe toutes les fonctions utiles pour Prestashop.',
				'potentialResa'		=> 'Cette classe est une estimation de reservation ou la date de fin est générée au plus tôt ou récupérée en session.',
				'pricerule'			=> 'C\'est l\'objet d\'une règle de prix permettant de savoir si elle s\'applique.',
				'pricerules'		=> 'C\'est la classe des règles de prix qui permet de les obtenir pour un panier.',
				'priceset'			=> 'L\'objet jeux de prix contient les tarifs pour un produit en fonction de la durée ou des dates.',
				'pricesets'			=> 'La classe permet d\'obtenir le tarif d\'une réservation à partir des jeux de prix crées.',
				'product'			=> 'C\'est l\'objet d\'une règle de réservation.',
				'products'			=> 'Cette classe permet de gérer les règles de réservation.',
				'reservation'		=> 'C\'est l\'objet d\'une réservation associé à une commande.',
				'reservations'		=> 'C\'est la classe qui permet de récupérer les réservations pour une période ou pour créneau.',
				'stock'				=> 'C\'est l\'objet d\'un élément de stock.',
				'stocks'			=> 'Cette classe permet de gérer les éléments de stock par produit.',
				'timeslot'			=> 'C\'est l\'objet d\'un créneau horaire avec une heure de début de fin et les jours pour lesquels il est activé.',
				'timeslots'			=> 'Cette classe permet de gérer les créneaux horaires et d\'obtenir ceux d\'un jour.',
				'utils'				=> 'Cette classe statique contient les fonctions utiles entre le module et Prestashop.'
			);
		} else {
			self::$sectionContent['customize']['classes'] = array (
				'availabilities'	=> 'The class provides availability for a period and determine whether a product is available on a time slot.',
				'availability'		=> 'This class is an object of availability or unavailability',
				'calendar'			=> 'This static class contains translations.',
				'calendarDay'		=> 'The day object generate the time slices based on the time slots.',
				'calendarMonth'		=> 'The months subject generates day objects and every time slot of the month.',
				'calendarTimeSlice'	=> 'The object represents a slice of time, description, availability and amount for the current product.',
				'calendarTool'		=> 'This class allows you to format periods or generate.',
				'calendarView'		=> 'The object represents a view of planning for a product or widget generated from the time slots.',
				'calendarWeek'		=> 'The object represents a week and to generate days.',
				'cart'				=> 'This base class represents a reservation in the basket for a product.',
				'carts'				=> 'The class represents the basket of client reservations.',
				'const'				=> 'The static class contains all the constants and their tree.',
				'help'				=> 'This static class contains translations and help to generate the doc.',
				'lang'				=> 'This static class contains translations of constants and enumerations.',
				'myownutils'		=> 'This class all the useful functions for PrestaShop.',
				'potentialResa'		=> 'This class is an estimate of booking or the end date is generated earlier or recovered in session.',
				'pricerule'			=> 'This is the subject of a pricing rule on whether it applies.',
				'pricerules'		=> 'This is the class of pricing rules that produces it to a basket.',
				'priceset'			=> 'The price set object contains the rate for a product based on the length or dates.',
				'pricesets'			=> 'The class provides the price of a book created from the price of sets.',
				'product'			=> 'It is the object of a reservation rules.',
				'products'			=> 'This class allows to manage the reservation rules.',
				'reservation'		=> 'This is the purpose of a reservation associated with a command.',
				'reservations'		=> 'This is the class that retrieves bookings for a period or time slot.',
				'stock'				=> 'This is the purpose of a stock item.',
				'stocks'			=> 'This class is used to manage inventory items by product.',
				'timeslot'			=> 'This is subject to a time slot with a start time and end days for which it is enabled.',
				'timeslots'			=> 'This class allows you to manage slots and get those a day.',
				'utils'				=> 'This static class contains useful functions between the module and PrestaShop.'
			);
		}
		
		if ($iso == 'fr') {
			self::$sectionContent['customize']['templates'] = array (
				'orderconfirmation'			=> 'C\'est le template qui permet d\'afficher les notification en fin de commande.',
				'orderdetail'				=> 'C\'est le template contenant le javascript pour modifier l\'affichage des commandes sur le front.',
				'cell_home'		=> 'Cellule d\'un planning de page d’accueil.',
				'cell_product'		=> 'Cellule d\'un planning de page produit.',
				'cell_widget'		=> 'Cellule d\'un planning de widget de sélection.',
				'day'				=> 'Planning des créneaux horaires par jour.',
				'daymonth'			=> 'Planning par mois.',
				'footer'			=> 'Pied du planning affiché pour un grand nombre de créneaux horaires.',
				'header'			=> 'En tête du planning qui affiche la selection et les boutons.',
				'line_day'			=> 'Templates inclus pour afficher la ligne des jours.',
				'line_time'		=> 'Templates inclus pour afficher la ligne des heures.',
				'list'				=> 'Planning minimum.',
				'months'			=> 'Planning des mois par trimestre ou année.',
				'week'				=> 'Planning des semaines.',
				'weekmonth'		=> 'Planning des semaines par mois.',
				'reservation_display'		=> 'Template pour afficher la réservation dans la popup.',
				'reservation_tab'			=> 'C\'est le template qui permet d\'insérer le planning sur la page produit.',
				'script_cart'				=> 'C\'est le template contenant les constantes javascript.',
				'script_header'				=> 'C\'est le template contenant le javascript pour modifier le tunnel de commande.',
				'script_product'			=> 'C\'est le template contenant le javascript pour gérer la selection de réservation sur la page produit.',
				'script_shoppingcart'		=> 'C\'est le template contenant le javascript pour modifier l\'affichage du panier.',
				'widgetcolumns'				=> 'C\'est le template qui permet d\'insérer le planning dans la colonne.',
				'widgethome'				=> 'C\'est le template qui permet d\'insérer le planning sur la page d\'acceuil.',
				'widgetsearch'				=> 'C\'est le template qui permet d\'afficher le widget de recherche.',
			);
		} else {
			self::$sectionContent['customize']['templates'] = array (
				'orderconfirmation'			=> 'This is the template that displays the end of the order notification.',
				'orderdetail'				=> 'This is the template containing the javascript to change the display order on the front.',
				'cell_home'		=> 'Planning cell on a home page.',
				'cell_product'		=> 'Planning cell on a product page.',
				'cell_widget'		=> 'Planning cell on a widget.',
				'day'				=> 'Planning of slots per day.',
				'daymonth'			=> 'Planning by month.',
				'footer'			=> 'Foot displayed on a schedule for a large number of slots.',
				'header'			=> 'Ahead of schedule that displays the selection and buttons.',
				'line_day'			=> 'Templates included to show the line of days.',
				'line_time'		=> 'Templates included to show the line of hours.',
				'list'				=> 'Minimum planning .',
				'months'			=> 'Schedule months by quarter or year.',
				'week'				=> 'Planning for weeks.',
				'weekmonth'		=> 'Planning of weeks per month.',
				'reservation_display'		=> 'Template to display the book in the popup.',
				'reservation_tab'			=> 'This is the template used to insert the schedule on the product page.',
				'script_cart'				=> 'This is the template containing the javascript constants.',
				'script_header'				=> 'This is the template containing the javascript to change the order checkout.',
				'script_product'			=> 'This is the template containing javascript to manage the booking selection on the product page.',
				'script_shoppingcart'		=> 'This is the template containing the javascript to change the cart display.',
				'columns'				=> 'This is the template used to insert the schedule in the column.',
				'home'				=> 'This is the template used to insert the schedule on the home page.',
				'search'				=> 'This is the template that displays the search widget.',
			);
		}
	}
	
}

?>