<?php
/*
* 2010-2012 LaBulle All right reserved
*/

require_once(_PS_MODULE_DIR_ . "myownreservations/myownreservations.php");
require_once(MYOWNRES_PATH . "controllers/admin.php");
require_once(MYOWNRES_PATH . "controllers/planning.php");
require_once(MYOWNRES_PATH . "controllers/ajax_admin.php");


class adminReservationPrices extends AdminController
{
	var $module = "myownreservations";
	var $name = "myownreservations";
	var $multishop_context_group;
	var $multishop_context;
	var $moduleObj;
	var $ruleId;
	var $insertedId=0;

    public function __construct()
    {
        global $cookie, $_LANGADM;

        $this->moduleObj = Module::getInstanceByName('myownreservations');
        $this->moduleObj->checkPro();
        //if (!is_file(_PS_MODULE_DIR_ ."myownreservations/pro/products.php")) 
        $this->multishop_context = true;
        myOwnReservationsController::_setInfos ($this->moduleObj);
        myOwnReservationsController::_construire($this->moduleObj);
		$this->display = 'view';
		$this->class_name = 'adminReservationPrices';
		$this->bootstrap=true;
		$this->doActions();
        parent::__construct();
    }
    
    public function displayErrors() {
	    foreach($this->_errors as $err) echo $err;
    }
    
    public function initPageHeaderToolbar()
	{
		global $cookie;
		$tab = Tools::getValue('myOwnResTab');
		$ctrl = strtolower(Tools::getValue('controller'));
		$field='';
		if ($tab=='timeslots')
			$fieldl = 'TimeSlot';
		if ($tab=='pricesrules') 
			$field = 'Pricesrule';
		if ($tab=='availability') 
			$field = 'Availability';
		if ($ctrl=='adminreservationrules') {
			$tab='products';
			$field = 'Product';
		}
		
		if ($this->ruleId > 0) $this->page_header_toolbar_title = ucfirst(myOwnLang::$object[MYOWN_OBJ::RESVRULE]).' #'.$this->ruleId;
		else $this->page_header_toolbar_title = ucfirst(myOwnLang::$object[MYOWN_OBJ::RESVRULE]);

		$this->page_header_toolbar_btn = myOwnReservationsController::getButtonsList( $this->moduleObj, $this->ruleId);

		if (_PS_VERSION_ >= "1.6.0.0") parent::initPageHeaderToolbar();
		$this->context->smarty->assign('help_link', null);
	}
	

	
	public function doActions()
	{
		global $cookie;
		$obj = $this->moduleObj;

		$operation='';
		$this->insertedId=0;
		$objet = $obj->l('The reservation rules', 'adminreservationrules');
		$male = true;
		$errors=array();$plur=false;
		$isproversion=is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");
		

			// ADD, UPDATE & DELETE PRICE RULE
			//-----------------------------------------------------------------------------------------------------------------
			if(isset($_POST['pricesruleName'])) {
				$tempPricesrule = myOwnPricesRules::getFromPost($obj, $cookie);

				$objet = myOwnLang::$object[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRICERULE]);
				if ($tempPricesrule->sqlId==0) {
					$operation = myOwnLang::getOperation($obj, MYOWN_OPE::CREATE, false);
					$result = $tempPricesrule->sqlInsert();
					$inserted = Db::getInstance()->ExecuteS("SELECT LAST_INSERT_ID() AS ID;");
					$this->insertedId = $inserted[0]['ID'];
					$obj->_pricerules->list[$this->insertedId]=$tempPricesrule;
				} else {
					$operation = myOwnLang::getOperation($obj, MYOWN_OPE::UPDATE, false);
					$result = $tempPricesrule->sqlUpdate();
				}
			}
			if(isset($_GET['copyPricesrule'])){
				$objet = myOwnLang::$object[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRICERULE]);
				$cpy=(int)$_GET['copyPricesrule'];
				$tempPricesrule = clone $obj->_pricerules->list[$cpy];
				$tempPricesrule->name .= ' '.$obj->l('copy', 'controller');
				$tempPricesrule->sqlId=0;
				$tempPricesrule->id_family=0;
				$tempPricesrule->ids_products='';
				$tempPricesrule->id_product_attribute=0;
				$operation = myOwnLang::getOperation($obj, MYOWN_OPE::COPY, false);
				$result = $tempPricesrule->sqlInsert();
				$inserted = Db::getInstance()->ExecuteS("SELECT LAST_INSERT_ID() AS ID;");
				$this->insertedId = $inserted[0]['ID'];
				$obj->_pricerules->list[$this->insertedId]=$tempPricesrule;
			}
			if(isset($_GET['deletePricesrule']) && !empty($_GET['deletePricesrule'])) {
				$objet = myOwnLang::$object[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRICERULE]);
				$tempPricesrule = new myOwnPricesRule();
				$tempPricesrule->sqlId = $_GET['deletePricesrule'];
				$operation = myOwnLang::getOperation($obj, MYOWN_OPE::DELETE, false);
				$result = $tempPricesrule->sqlDelete();
			}
			if(isset($_GET['editPricesrule']) && !empty($_GET['editPricesrule'])) {
				$objet = myOwnLang::$object[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRICERULE]);
				$tempPricesrule = new myOwnPricesRule();
				$tempPricesrule->sqlId = $_GET['qeditPricesrule'];
				if (isset($_GET['qenable'])) {
					if (intval($_GET['qenable'])) $operation = myOwnLang::getOperation($obj, MYOWN_OPE::ENABLE, false);
					else $operation = myOwnLang::getOperation($obj, MYOWN_OPE::DISABLE);
					$result = $tempPricesrule->sqlEnable(intval($_GET['qenable']));
				}
				if (isset($_GET['qvisible'])) {
					if (intval($_GET['qvisible'])) $operation = $obj->l('displayed', 'controller');
					else $operation = $obj->l('hidden', 'controller');
					$result = $tempPricesrule->sqlVisible(intval($_GET['qvisible']));
				}
			}


		$male=true;
		if ($operation != "") {
			if ($result) $this->confirmations[] = $objet.' '.myOwnLang::getResult($obj, true, !$plur).' '.$operation;
			else $this->errors[] = $objet.' '.myOwnLang::getResult($obj, false, !$plur).' '.$operation.' : '.Db::getInstance()->getMsgError();

			myOwnReservationsController::_construire($obj, true);
		}
		
		$this->ruleId = intval(Tools::getValue('editPricesrule', -1));
		if (!$isproversion) $this->ruleId=-1;
		if ($this->ruleId==0) $this->ruleId = $this->insertedId;
		
	} 


    public function renderView()
	{
		global $cookie;
		$htmlOut='';$content='';
		$obj = $this->moduleObj;
		if (_PS_VERSION_ < "1.6.0.0") {
			$this->initPageHeaderToolbar();
			$buttons = MyOwnReservationsUtils::getButtonsList($this->page_header_toolbar_btn, $this->ruleId);
		} else $buttons='';
		//$buttons = myOwnReservationsController::getButtons($cookie, $obj, $this->ruleId, false, 'Product',  'products');
		
		foreach($this->errors AS $error) {
			$htmlOut .= myOwnUtils::displayError($error);
		}
		foreach($this->confirmations AS $confirmation) {
			$htmlOut .= myOwnUtils::displayConf($confirmation);
		}
		$this->errors=array();
		$this->confirmations=array();
		
		$parent = $obj->l('Price Rules', 'adminReservationPrices');
		if ($this->ruleId==-1) $title = ucfirst(myOwnLang::$object[MYOWN_OBJ::PRICERULE]);
		if ($this->ruleId==0) $title = ucfirst(myOwnLang::$actions[MYOWN_OPE::FNEW]).' '.ucfirst(myOwnLang::$objects[MYOWN_OBJ::PRICERULE]);
		if ($this->ruleId>0) $title = ucfirst(myOwnLang::$object[MYOWN_OBJ::PRICERULE]).' #'.$this->ruleId;
		
		$header = MyOwnReservationsUtils::displayIncludes();
		$header .= myOwnReservationsController::getModuleHeader($obj, $cookie);
		$header .= $htmlOut;
		$header .= MyOwnReservationsUtils::displayStarter($obj);
		if (_PS_VERSION_ < "1.6.0.0") $header .= myOwnReservationsController::getMenuHeader($obj, $parent, $title, $buttons);
		$isproversion=is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");

			$id = intval(Tools::getValue('editPricesrule', -1));
			if ($id==0) $id = $this->insertedId;
			
			//$buttons = myOwnReservationsController::getButtons($cookie, $obj, $id, false, 'Pricesrule', 'pricesrules');
			$parent = $obj->l('Discounts', 'controller');
			
			if ($id==-1) $title = ucfirst(myOwnLang::$objects[MYOWN_OBJ::PRICERULE]);
			if ($id==0) $title = ucfirst(myOwnLang::$actions[MYOWN_OPE::FNEW]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRICERULE]);
			if ($id>0) $title = ucfirst(myOwnLang::$object[MYOWN_OBJ::PRICERULE]).' #'.$id;
			
			if ($id>-1) $content .=  myOwnReservationsPricerulesController::edit($cookie,$obj,$id);
			else $content .= myOwnReservationsPricerulesController::show($cookie,$obj);
		
		return $header.$content;
  	}
  
}
?>