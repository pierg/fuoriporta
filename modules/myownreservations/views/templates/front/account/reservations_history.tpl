{capture name=path}
	<a href="{$myownlink->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
		{l s='My account'}
	</a>
	{if $_PS_VERSION_<"1.7"}
	<span class="navigation-pipe">{$navigationPipe}</span>
	<span class="navigation_page">{l s='Order history' mod='myownreservations'}</span>
	{/if}
{/capture}
{if $_PS_VERSION_<"1.7"}{include file="$tpl_dir./errors.tpl"}{/if}
{if $_PS_VERSION_<"1.7"}<h1 class="page-heading bottom-indent">{l s='Reservations' mod='myownreservations'}</h1>{/if}
        <ul id="home-page-tabs" class="nav nav-tabs clearfix">
			<li {if $myresafilter==""}class="active" {/if} {if $_PS_VERSION_>="1.7"}style="border-left: #ebebeb 2px solid;padding: 5px 20px;color: #acaaa6;color: #414141;text-transform:uppercase;float:left;font-weight:bold"{/if}><a href="{$myownlink->getPageLink('module-myownreservations-reservations', true)|escape:'html':'UTF-8'}?admin=reservations" class="blockmyownreservations">{l s='all' mod='myownreservations'}</a></li>
			<li {if $myresafilter=="past"}class="active" {/if} {if $_PS_VERSION_>="1.7"}style="border-left: #ebebeb 2px solid;padding: 5px 20px;color: #acaaa6;color: #414141;text-transform:uppercase;float:left;font-weight:bold"{/if}><a href="{$myownlink->getPageLink('module-myownreservations-reservations', true)|escape:'html':'UTF-8'}?admin=reservations&filter=past" class="blockmyownreservations">{l s='Past' mod='myownreservations'}</a></li>
			<li {if $myresafilter=="current"}class="active" {/if} {if $_PS_VERSION_>="1.7"}style="border-left: #ebebeb 2px solid;padding: 5px 20px;color: #acaaa6;color: #414141;text-transform:uppercase;float:left;font-weight:bold"{/if}><a href="{$myownlink->getPageLink('module-myownreservations-reservations', true)|escape:'html':'UTF-8'}?admin=reservations&filter=curent" class="blockmyownreservations">{l s='Current' mod='myownreservations'}</a></li>
			<li {if $myresafilter=="incoming"}class="active" {/if} {if $_PS_VERSION_>="1.7"}style="border-left: #ebebeb 2px solid;padding: 5px 20px;color: #acaaa6;color: #414141;text-transform:uppercase;float:left;font-weight:bold"{/if}><a href="{$myownlink->getPageLink('module-myownreservations-reservations', true)|escape:'html':'UTF-8'}?admin=reservations&filter=incoming" class="blockmyownreservations">{l s='Incoming' mod='myownreservations'}</a></li>
		</ul>
<br/>
<div class="col-md-12" id="block-history">
	{if $resas && count($resas)}
		<table id="resas-list" class="table table-bordered footab">
			<thead>
				<tr>
					<th></th>
					<th class="item">{l s='Product' mod='myownreservations'}</th>
					<th class="item">{l s='Period' mod='myownreservations'}</th>
					<th class="item">{l s='Total price' mod='myownreservations'}</th>
					<th class="item">{l s='Validated' mod='myownreservations'}</th>
					
					<th class="first_item" data-sort-ignore="true">{l s='Order reference' mod='myownreservations'}</th>
					<th class="item">{l s='Date' mod='myownreservations'}</th>
					<th class="item">{l s='Status' mod='myownreservations'}</th>
					<th data-sort-ignore="true" data-hide="phone,tablet" class="last_item">&nbsp;</th>
				</tr>
			</thead>
			<tbody class="resas_conf{if $_PS_VERSION_>="1.7"}_new{/if}">
				{foreach from=$resas item=resa name=myLoop}
					<tr class="{if $smarty.foreach.myLoop.first}first_item{elseif $smarty.foreach.myLoop.last}last_item{else}item{/if} {if $smarty.foreach.myLoop.index % 2}alternate_item{/if}">
						<td>
							{if array_key_exists($resa->id_product,$productsImages)}
				    			{assign var='viewedProduct' value=$productsImages[$resa->id_product]}
				    			<a class="product_img_link"	href="{$myownlink->getProductLink($viewedProduct->id, $viewedProduct->link_rewrite, $viewedProduct->category_rewrite)}">
									<img class="replace-2x" src="{$myownlink->getImageLink($viewedProduct->link_rewrite, $viewedProduct->cover, {$mediumSize->name})}" alt="{if !empty($viewedProduct->legend)}{$viewedProduct->legend|escape:'html':'UTF-8'}{else}{$viewedProduct->name|escape:'html':'UTF-8'}{/if}" title="{if !empty($viewedProduct->legend)}{$viewedProduct->legend|escape:'html':'UTF-8'}{else}{$viewedProduct->name|escape:'html':'UTF-8'}{/if}" {if isset($homeSize)} width="{$homeSize.width}" height="{$homeSize.height}"{/if} itemprop="image" />
								</a>
				    		{/if}
						</td>
						<td>x{$resa->quantity} <b>{$productsNames[$resa->id_product]}</b>{if $resa->id_product_attribute>0}<br />{$resasAttributes[$resa->sqlId] nofilter}{/if}</td>
						<td>{$resa->toString($myownr, $id_lang, false)}</td>
						<td class="history_price" data-value="{$resa->_order->total_paid}">
							<span class="price">
								{displayPrice price=$resa->_order->total_paid currency=$resa->_order->id_currency no_utf8=false convert=false}
							</span>
						</td>
						<td>{if $resa->validated}{l s='Validated' mod='myownreservations'}{else}{l s='Validation pending' mod='myownreservations'}{/if}
						</td>
						<td class="history_link bold">
							<a class="color-myaccount" href="javascript:showOrder(1, {$resa->_order->id}, '{$myownlink->getPageLink('order-detail', true)|escape:'html':'UTF-8'}');">
								{Order::getUniqReferenceOf($resa->id_order)}
							</a>
						</td>
						<td data-value="{$resa->_order->date_add|regex_replace:"/[\-\:\ ]/":""}" class="history_date bold">
							{dateFormat date=$resa->_order->date_add full=0}
						</td>
						<td data-value="{$resa->_order->current_state}" class="history_state">
							{if isset($resa->_order) && isset($resa->_order->current_state)}
								<span class="label{if $resa->_order->current_state == 1 || $resa->_order->current_state == 10 || $resa->_order->current_state == 11} label-info{elseif $resa->_order->current_state == 5 || $resa->_order->current_state == 2 || $resa->_order->current_state == 12} label-success{elseif $resa->_order->current_state == 6 || $resa->_order->current_state == 7 || $resa->_order->current_state == 8} label-danger{elseif $resa->_order->current_state == 3 || $resa->_order->current_state == 9 || $resa->_order->current_state == 4} label-warning{/if}" {if $resa->_order->current_state > 12}style="background-color:{$resa->_order->order_state_color};"{/if}>
									{$statuses[$resa->_order->current_state].name}
								</span>
							{/if}
						</td>
						<td class="history_detail">
							{foreach from=$resasExt item=resaExt}
								{if $resa->_mainProduct!=null && $resaExt->id|in_array:$resa->_mainProduct->ids_notifs}
									{$resaExt->displayOrderConfResa($myownr, $resa) nofilter}
								{/if}
							{/foreach}

						</td>
					</tr>
				{/foreach}
			</tbody>
		</table>
		<div id="block-order-detail" class="unvisible">&nbsp;</div>
	{else}
		<p class="alert alert-warning">{l s='You have not placed any reservations.' mod='myownreservations'}</p>
	{/if}
</div>