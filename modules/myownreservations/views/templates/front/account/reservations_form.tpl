{*
* 2010-2014 LaBulle
*
* NOTICE OF LICENSE
*
* This source file is subject to Terms and Conditions of Use 
* Available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade module to newer
* versions in the future. If you wish to customize module for your
* needs please refer to http://www.labulle.net for more information.
*
* @author    laBulle.net <info@labulle.net>
* @copyright 2010-2014 laBulle
* @license   http://www.labulle.net/download/CGV-en.pdf
*}
{capture name=path}
		
{/capture}
{if $_PS_VERSION_>="1.7"}
{/if}
{extends file='customer/page.tpl'}

{block name='page_title'}
  {if $adminpage=='availabilities'}{l s='Availabilities' mod='myownreservations'}{else}{if isset($exttitle) && $exttitle!=""}{$exttitle|escape:'htmlall':'UTF-8'}{else}{l s='Reservations' mod='myownreservations'}{/if}{/if}
{/block}

{block name='page_content'}

<form action="{$myownlink->getPageLink('module-myownreservations-reservations', true)|escape:'html'}?admin={$adminpage|escape:'htmlall':'UTF-8'}" method="post" id="" class="std">
	<div class="col-xs-12">
		<div id="mycollectionplaces_block_account">


			{$front nofilter}
			
			<ul class="footer_links clearfix">
				<li>
			        <a class="btn btn-default button button-small" href="{$base_dir|escape:'htmlall':'UTF-8'}">
			            <span>
			                <i class="icon-chevron-left"></i>{l s='Home' mod='myownreservations'}
			            </span>
			        </a>
			    </li>
			</ul>

		</div>
	</div>
</form>

{/block}