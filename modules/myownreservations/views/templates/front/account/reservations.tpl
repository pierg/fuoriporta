{*
* 2010-2014 LaBulle
*
* NOTICE OF LICENSE
*
* This source file is subject to Terms and Conditions of Use 
* Available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade module to newer
* versions in the future. If you wish to customize module for your
* needs please refer to http://www.labulle.net for more information.
*
* @author    laBulle.net <info@labulle.net>
* @copyright 2010-2014 laBulle
* @license   http://www.labulle.net/download/CGV-en.pdf
*}
{capture name=path}
	<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
		{l s='My account'}
	</a>
	<span class="navigation-pipe">{$navigationPipe}</span>
	{if isset($formexttitle)}
		<a href="{$myownlink->getPageLink('module-myownreservations-reservations', true)|escape:'html'}?{if isset($formextargs)}{$formextargs|escape:'htmlall':'UTF-8'}{else}admin={$adminpage|escape:'htmlall':'UTF-8'}{/if}">
	{/if}
		{if $adminpage=='availabilities'}{l s='Availabilities' mod='myownreservations'}{else}{if isset($exttitle) && $exttitle!=""}{$exttitle|escape:'htmlall':'UTF-8'}{else}{l s='Reservations' mod='myownreservations'}{/if}{/if}
	{if isset($formexttitle)}
		</a>
		<span class="navigation-pipe">{$navigationPipe}</span>
		{$formexttitle}
	{/if}
{/capture}

<form action="{if isset($formextaction)}{$formextaction|escape:'htmlall':'UTF-8'}{else}{$myownlink->getPageLink('module-myownreservations-reservations', true)|escape:'html'}?{if isset($formextargs)}{$formextargs|escape:'htmlall':'UTF-8'}{else}admin={$adminpage|escape:'htmlall':'UTF-8'}{/if}{/if}" method="post" id="" class="std">
	<div class="col-xs-12">
		<div id="mycollectionplaces_block_account">


			{$front nofilter}
			
			<ul class="footer_links clearfix">
				<li>
			        <a class="btn btn-default button button-small" href="{$base_dir|escape:'htmlall':'UTF-8'}">
			            <span>
			                <i class="icon-chevron-left"></i>{l s='Home' mod='myownreservations'}
			            </span>
			        </a>
			    </li>
			</ul>

		</div>
	</div>
</form>
