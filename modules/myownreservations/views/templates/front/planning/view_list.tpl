<script type="text/javascript">
	var timeslotsLabel=new Array();
{foreach from=$timeSlotList item=timeSlot name=timeSlotsLoop}
	timeslotsLabel[{$timeSlot->sqlId}]='{if $potentialResa->_mainProduct->_reservationShowTime}{if $potentialResa->_selType=="end"}{$timeSlot->getEndHourTimeStr($id_lang)}{else}{$timeSlot->getStartHourTimeStr($id_lang)}{/if}{else}{$timeSlot->name}{/if}';
{/foreach}
	var availableTimeslots=new Array();
{foreach from=$availableDays item=day key=DayKey name=daysLoop}
	{assign var='isTimeslots' value=false}
	availableTimeslots['{$day->key}']=new Array();
	{foreach from=$timeSlotList item=timeSlot name=timeSlotsLoop}
		{assign var='slice' value=$day->getSlice($timeSlot->sqlId)}
		
		{if $slice!=null && $slice->isEnable}
		{assign var='isTimeslots' value=true}
		availableTimeslots['{$DayKey}'][{$timeSlot->sqlId}]='{$slice->key}';
		{/if}
	{/foreach}
	{if $isTimeslots}{append var='selectableDays' value=$calendar->formatDate($day->date,$id_lang) index=$DayKey}{/if}
{/foreach}

function populateTimeslots() {
	//var selectionDays = $("#availableDays");
	//selectedday = selectionDays.val();
	var selectedday = $("#availableDays option:selected").val();
	if (selectedday=='') selectedday = '{$calendar->getDayKey($reservationView->start)}';
	var selectionTimeslots = $("#availableTimeslots");
	
	selectionTimeslots.empty('{$planningType}')
	selectionTimeslots.append($("<option />").val('').text('{l s='Select a time slot..' mod='myownreservations'}'));
	for (var tsKey in availableTimeslots[selectedday]) {
		selectionTimeslots.append($("<option />").val(availableTimeslots[selectedday][tsKey]).text(timeslotsLabel[tsKey]));
	}
}
$( document ).ajaxComplete(function() {
	populateTimeslots();
});


</script> 
{assign var='lastEndTime' value=""}
<table width="100%" id="myOwn{$planningStyle}ListCalendarLine" class="myOwnCalendarLine">
        <tr>
            <td><select id="availableDays" name="availableDays" onchange="populateTimeslots();">
	            {foreach from=$availableDays item=day key=DayKey name=daysLoop}
	            	<option value="{$day->key}" {if $potentialResa->_selType=="end" && $potentialResa->startDate==$day->key} selected{/if}>{$calendar->formatDate($day->date, $id_lang)}</option>
	            {/foreach}
            </select></td>
            <td><select class="myOwnDeliveriesRes" id="availableTimeslots" name="myOwnDeliveriesRes" onChange="my{$planningSelector}Selector.slotSelection(this);"></select></td>
        </tr>
</table>