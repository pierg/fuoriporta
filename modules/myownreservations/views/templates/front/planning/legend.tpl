<section class="page-product-box">
	<h3 class="page-product-heading">{l s='Special offers' mod='myownreservations'}</h3>
	<div id="potentialRules">
		<table class="std table-product-pricerules">
			<thead>
				<tr>
					<th style="width:40%">{l s='Condition' mod='myownreservations'}</th>
					<th>{l s='Discount' mod='myownreservations'}</th>
				</tr>
			</thead>
			<tbody>
			  {foreach from=$potentialRules key=id_rule item=potentialRule name=potentialRulesLoop}
				<tr id="potentialRule_{$id_rule}">
					<td>{$potentialRulesConditions[$id_rule]}</td>
					<td class="priceRule{$id_rule}">{$potentialRule->getInpactToString($mainProduct)}</td>
				</tr>
			  {/foreach}
			</tbody>
		</table>
	</div>
</section>