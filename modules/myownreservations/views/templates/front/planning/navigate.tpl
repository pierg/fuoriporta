{if $potentialResa->_selType!="end"}
{assign var='selStartValue' value='0'}
{else}
{capture name='my_module_tempvar'}{$potentialResa->startDate}_{$potentialResa->startTimeslot}{/capture}
{assign var='selStartValue' value=$smarty.capture.my_module_tempvar}
{/if}

<table width="100%" id="myOwn{$planningStyle}CalendarHeader" class="myOwnCalendarHeader">
	  <tr class="myOwnNavigateTop">
	        <td align="left" width="30%">
						{if $sel>0}
						<a tabindex="5" style="font-weight:normal" onclick="my{$planningSelector}Selector.showView({math equation="x-1" x=$sel}, '{$planningType}', '{$selStartValue}');" title=">{$previous_label}" title="&laquo; {$previous_label}">
						<button type="button" role="presentation" class="nav-prev"><span aria-label="Previous">‹</span></button>
						{/if}
					</td>
	        <td style="text-align:center" align="center">
	        {if $planningType=="home" && !$ishomeselector}
	    		<h3>{$potentialResa->_mainProduct->name}</h3><br/>
	    	{/if}
	        {if $planningType=="home" && $ishomeselector && count($selectCats)>0}
				{if $_PS_VERSION_>="1.6" && $_PS_VERSION_<"1.7"}
		    		<div class="selector" id="" style="display:inline-block;text-align:center">
		    			<span style="width: 190px; -webkit-user-select: none;">{$selectCats[$resa_category]}</span>
		    		{/if}
						<select id="myOwnSelectCat" style="padding:2px" class="button_large" type="{$planningSelector}" onChange="my{$planningSelector}Selector.checkHomeCat();">
		    		{foreach from=$selectCats key=selectCatId item=selectCatName name=selectCatLoop}
		    			<option value="{$selectCatId}" {if $resa_category==$selectCatId}selected{/if}>{$selectCatName}</option>
		    		{/foreach}
		    	</select>
		    	{if $_PS_VERSION_>="1.6" && $_PS_VERSION_<"1.7"}</div>{/if}
	        {/if}
	    	{if $selector|@count>1}
	    		{if $_PS_VERSION_>="1.6" && $_PS_VERSION_<"1.7"}
	    		<div class="selector" id="" style="display:inline-block;text-align:center">
	    			<span style="width: 190px; -webkit-user-select: none;">{$selector[$sel]}</span>
	    		{/if}
	    		<select style="padding:2px" class="button_large" onChange="my{$planningSelector}Selector.viewSelectorChange(this);" id="viewSelector" name="viewSelector">
	    		{foreach from=$selector key=selectorVal item=selectorLabel name=selectorLoop}
	    			<option value="{$selectorVal}" {if $selectorVal==$sel}selected{/if}>{$selectorLabel}</option>
	    		{/foreach}
	    		</select>
	    		{if $_PS_VERSION_>="1.6" && $_PS_VERSION_<"1.7"}</div>{/if}
	    	{/if}
	        </td>
					<td align="right" style="text-align:right" width="30%">
						{if $sel<$maxSelection}
						<a  tabindex="6" style="font-weight:normal" onclick="my{$planningSelector}Selector.showView({math equation="x+1" x=$sel}, '{$planningType}', '{$selStartValue}');" title="{$next_label}" >
							<button type="button" role="presentation" class="nav-next"><span aria-label="Next">›</span></button>
						</a>
						{/if}
					</td>
	  </tr>

  <tr class="myOwnSubNavigate" style="display:none">
        <td width="150" align="left"><a style="font-weight:normal" onclick="viewSubSelectorParent('{$planningType}')" title="{$previous_sub_label}" title="&laquo; {$previous_sub_label}" class="{if $_PS_VERSION_>="1.6"}btn btn-default button button-small{else}button{/if}"><span>&laquo; {$previous_sub_label}</span></a></td>
        <td style="text-align:center" align="center" {if $planningType=="column"}colspan="2"{/if}>
    	{if $subSelector|@count}
    		<select style="padding:2px" class="button_large" onChange="my{$planningSelector}Selector.viewSubSelectorChange(this);" id="view{$planningType}SubSelector" name="viewSubSelector">
    		{foreach from=$subSelector key=subSelectorVal item=subSelectorLabel name=subSelectorLoop}
    			<option value="{$subSelectorVal}">{$subSelectorLabel}</option>
    		{/foreach}
    		</select>
    	{/if}
        </td>
        {if $planningType=="product"}<td width="150" align="right"></td>{/if}
  </tr>
</table>
