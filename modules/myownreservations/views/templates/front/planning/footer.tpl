<table width="100%" id="myOwn{$planningStyle}CalendarBottom" class="myOwnCalendarBottom">
    <tr>
        <td align="left" width="30%">{if $sel>0}<a style="font-weight:normal" onclick="my{$planningSelector}Selector.showView({math equation="x-1" x=$sel}, '{$planningType}', 0);" title=">{$previous_label}" title="&laquo; {$previous_label}" class="{if $_PS_VERSION_>="1.6"}btn btn-default button button-small{else}button{/if}"><span>&laquo; {$previous_label}</span></a>{/if}</td>
        <td align="center" style="text-align:center">
        {if !$widget} 
        {if $potentialResa->_selType=="start"}
        <span class="exclusive myOwnUnvalidate">{l s='Validate start' mod='myownreservations'}</span>
        <a class="myOwnValidate {if $_PS_VERSION_>="1.6"}button btn btn-default{/if}" style="display:none" onclick="my{$planningSelector}Selector.validateProductStart();goToResaTabScroll();"><span>{l s='Validate start' mod='myownreservations'}</span></a>
        {/if}
        {if $potentialResa->_selType=="end" or $potentialResa->_selType==""}
        <span class="exclusive myOwnUnvalidate">{l s='Add to cart' mod='myownreservations'}</span>
        <a id="myOwnValidate" class="myOwnValidate {if $_PS_VERSION_>="1.6"}button btn btn-default{/if}" style="display:none" onclick="myownreservationAddToCart(this);"><span>{l s='Add to cart' mod='myownreservations'}</span></a>
        {/if}
        {/if}
        </td>
        <td align="right" style="text-align:right" width="30%">{if $sel<$maxSelection}<a style="font-weight:normal" onclick="my{$planningSelector}Selector.showView({math equation="x+1" x=$sel}, '{$planningType}', 0);" title="{$next_label} &raquo;" class="{if $_PS_VERSION_>="1.6"}btn btn-default button button-small{else}button{/if}"><span>{$next_label} &raquo;</span></a>{/if}</td>
	<tr/>
</table>