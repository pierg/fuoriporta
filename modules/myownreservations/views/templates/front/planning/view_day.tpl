{foreach from=$daysList key=dayKey item=day name=daysLoop}
	<table width="100%" id="myOwn{$planningStyle}CalendarTop" class="myOwnCalendarTop" day="{$calendar->getDayKey($day->date)}" {if $isDaySel}style="display:none"{/if}>
		<tr>
			<td class="myOwnCalendarTopItem ">
				{$calendar->formatDateWithDay($day->date,$id_lang)}
			</td>
		</tr>
	</table>
	{foreach from=$timeslotsGroups item=timeSlotsGroup name=timeSlotsGroupLoop}
		{assign var='lastEndTime' value=""}
		<table width="100%" id="myOwn{$planningStyle}DayCalendarLine" class="myOwnCalendarLine" day="{$calendar->getDayKey($day->date)}" {if $isDaySel}style="display:none"{/if}>
		    <tr class="myOwnCalendarTopHoursLine">
		    	{if $reservationView->homeProducts!=array()}
		    		<td class="myOwnCalendarProduct myOwnCalendarTopProduct"></td>
		    	{/if}
		    	<td class="myOwnCalendarTopHoursEmpty"></td>
		    {foreach from=$timeSlotsGroup item=timeSlot name=timeSlotsLoop}
		        {if $lastEndTime!=$timeSlot->startTime and $lastEndTime!=""}
		            <td class="myOwnCalendarTopHoursEmpty"><span>{$calendar->formatTime($lastEndTime,$id_lang)}</span></td>
		        {/if}
		        {assign var='lastEndTime' value=$timeSlot->endTime}
		        <td class="myOwnCalendarTopHours" colspan="2"><span>{$calendar->formatTime($timeSlot->startTime,$id_lang)}</span></td>
		        {if $smarty.foreach.timeSlotsLoop.last}
		            <td class="myOwnCalendarTopHoursEmpty"><span>{$calendar->formatTime($timeSlot->endTime,$id_lang)}</span></div>
		        {/if}
		    {/foreach}
		    <tr/>
		{assign var='lastEndTime' value=""}
		    <tr>
		    	{if $widgetHome!=2 && $reservationView->homeProducts!=array()}
					<td class="myOwnCalendarProduct" width="{$mediumSize->width}px">
						{if $linetype=="sel"}
							{foreach from=$reservationView->getAllProducts($id_lang) key=id_product item=product name=productsLoop}
								{if array_key_exists($id_product,$productsImages)}
					            	{assign var='viewedProduct' value=$productsImages[$id_product]}
					            			<div class="block_content" style="padding-top:1px">
												<center>
													{if $imageSize>0}
														<div style="{if $product.available}background-image:url({$myownlink->getImageLink($viewedProduct->link_rewrite, $viewedProduct->cover, {$mediumSize->name})});{/if}height:{$mediumSize->height}px;width:{$mediumSize->width}px" alt="{$viewedProduct->legend|escape:html:'UTF-8'}"></div>
													{else}
														<div style="height:{$mediumSize->width}px;width:{$mediumSize->width}px"></div>
													{/if}
													<h6 class="widget_title">{if $product.available}<a id="product_link_{$viewedProduct->id}" href="{$myownlink->getProductLink($viewedProduct->id, $viewedProduct->link_rewrite, $viewedProduct->category_rewrite)}" title="{$viewedProduct->name|escape:html:'UTF-8'}">{$viewedProduct->name|truncate:14:'...'|escape:html:'UTF-8'}</a>{/if}</h5>
												</center>
											</div>
								{/if}
							{/foreach}
						{/if}
					</td>
			{/if}
		    	<td class="myOwnCalendarTopHoursEmpty"></td>
		    {foreach from=$timeSlotsGroup item=timeSlot name=timeSlotsLoop}
		        {if $lastEndTime!=$timeSlot->startTime and $lastEndTime!=""}
		            <td class="myOwnCalendarTopHoursEmpty">&nbsp;</td>
		        {/if}
		        {assign var='lastEndTime' value=$timeSlot->endTime}
		         <td class="myOwnCalendarLineItem" colspan="2">
	                 {include file=$planning_cell}
		        </td>
		        {if $smarty.foreach.timeSlotsLoop.last}
		            <td class="myOwnCalendarTopHoursEmpty">&nbsp;</td>
		        {/if}
		    {/foreach}
		    <tr/>
		</table>
	{/foreach}
{/foreach}
