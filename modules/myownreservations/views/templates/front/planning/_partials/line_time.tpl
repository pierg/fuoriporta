
	{if !$isDaySel}
		{if $last_line or ($lastEndTime!=$timeSlot->startTime and $lastEndTime!="")}
	        <tr>
	        	{if $reservationView->homeProducts!=array()}<td class="myOwnCalendarProduct"></td>{/if}
	            <td class="myOwnCalendarHours">
	            	{if !$last_line}
	                	{$calendar->formatTime($lastEndTime,$id_lang)}
	                {else}
	                	{$timeListStr|escape:'htmlall':'UTF-8'}
	                {/if}
	            </td>
	            {foreach from=$cell_days item=day name=myLoop}
        	            <td class="{if !isset($reservationMonth) or $reservationMonth->isDayIn($day->date)}timeSlotCell{else}emptyCell{/if}" {if $isNightSel}colspan="2"{/if}></td>
	            {/foreach}
	            {if $isNightSel}<td></td>{/if}
	        <tr/>
		{/if}
		{if !$last_line}
	    	{assign var='lastEndTime' value=$timeSlot->endTime}
	    {/if}
    {/if}