			{if $widgetHome!=2 && $reservationView->homeProducts!=array()}
					<td class="myOwnCalendarProduct" width="{$mediumSize->width}px">
						{if $linetype=="sel"}
							{foreach from=$reservationView->getAllProducts($id_lang) key=id_product item=product name=productsLoop}
								{if array_key_exists($id_product,$productsImages)}
					            	{assign var='viewedProduct' value=$productsImages[$id_product]}
					            			<div class="block_content thumbnail-container" style="padding-top:1px;height:inherit;width:inherit;margin-bottom:2px">
												<center>
													{if $mediumSize->width>0}
														<div style="{if $product.available}background-image:url({$myownlink->getImageLink($viewedProduct->link_rewrite, $viewedProduct->cover, {$mediumSize->name})});{/if}height:{$mediumSize->height}px;width:{$mediumSize->width}px" alt="{$viewedProduct->legend|escape:html:'UTF-8'}"></div>
													{else}
														<div style="height:30px;width:{$mediumSize->width}px"></div>
													{/if}
													<h6 class="widget_title">{if $product.available}<a id="product_link_{$viewedProduct->id}" href="{$myownlink->getProductLink($viewedProduct->id, $viewedProduct->link_rewrite, $viewedProduct->category_rewrite)}" title="{$viewedProduct->name|escape:html:'UTF-8'}">{$viewedProduct->name|truncate:14:'...'|escape:html:'UTF-8'}</a>{/if}</h5>
												</center>
											</div>
								{/if}
							{/foreach}
						{/if}
					</td>
			{/if}
			{if !$isDaySel}
            	    <td class="myOwnCalendarHours">
            	        {if $linetype=="sel"}{$timeListStr|escape:'htmlall':'UTF-8'}{*{$calendar->formatTime($timeSlot->startTime,$id_lang)}*}{/if}
            	    </td>
			{/if}
			{if $isNightSel && (($linetype=="sel" && $potentialResa->_selType!="end") || ($linetype=="top" && $potentialResa->_selType=="end"))}
					<td{if $linetype=="sel"} class="myOwnCalendarLineItem"{/if}></td>
			{/if}
			{if $cell_days!=array()}
	            {foreach from=$cell_days key=cell_key item=cell_day name=cellDaysLoop}
	            	{if !isset($reservationMonth) or $linetype=="top" or (isset($reservationMonth) && $reservationMonth->isDayIn($cell_day->date))}
					
 {foreach from=$dayParallel[$cell_day->getDayNumber()] key=col_key item=col_ts name=colsLoop}

					   {if !$isDaySel && $linetype!="top"}
						   	 {if isset($timeListStr) && $calendar->getTimeListTime($cell_day->date, $timeListStr)>=$reservationView->start && $calendar->getTimeListTime($cell_day->date, $timeListStr)<$reservationView->end}
						       {assign var='timeSlot' value=$timeSlotObj->getTimeSlotOfTimeStr($cell_day->date, $timeListStr, $col_ts, $col_key)}
						       <!--ts--> {if $timeSlot==null}<!--null-->{/if}
						     {else}
						     	{assign var='timeSlot' value=null}
						     	<!--null--> {if isset($timeListStr)}{/if}
						     {/if}
					   {else}
					   {/if}
					   <!--linetype:{$linetype}-->
					   {if $linetype=="top" || $linetype=="" || $timeSlot!=null}
					   		{if $linetype!="top" && $timeSlot!=null}
					   		{/if}
					   		{if $isDaySel || ($linetype=="" && $col_key==1) || ($linetype=="top" && $col_key==1) || ($linetype!="top" && $timeSlot!=null && $timeSlot->getStartTime() == $timeListTime)}
				                <td col="{$col_key}"
				                	class="{$cell_class}"
				                	{if $isNightSel}colspan="2"{/if}
				                	{if ($linetype=="top" || $linetype=="") && $col_key==1}colspan="{$dayParallel[$cell_day->getDayNumber()]|@count}"{/if}
				                	{if !$isDaySel && $linetype!="top" && $linetype!="" && $cutTimes}rowspan="{$timeSlot->getLength($timeLength*60)|escape:'htmlall':'UTF-8'}"{/if}>
					                	{if $linetype=="sel"}
					                		{include file=$planning_cell day=$cell_day}
					                	{else if $linetype=="top"}
					                		{if $planningType=="column" || $planningType=="topcolumn" || $planningStyle=="topproduct"}
					                    		{$calendar->getShortDayString($cell_day->date,$id_lang)}
					                    	{else if isset($cell_content) && $cell_content!=""}
					                    		{$calendar->$cell_content($cell_day->date,$id_lang)}
					                    	{else}
					                    		{$calendar->getDayString($cell_day->date,$id_lang)}
					                    	{/if}
					                	{else}
					                		{$calendar->$cell_content($cell_day->date,$id_lang)}
					                	{/if}
			                	</td>
							{/if}
						{else}	
													<td col="{$col_key}" class="{$cell_class}" {if $isNightSel}colspan="2"{/if}></td>
						{/if}

{/foreach}
	                	
					{else}
						{if $cell_content!="ggetDayOfMonth"}<td class="myOwnCalendarLineItem" {if $isNightSel}colspan="2"{/if}></td>{/if}
					{/if}
	            {/foreach}
            {/if}
            {if $isNightSel && (($linetype=="top" && $potentialResa->_selType!="end") || ($linetype=="sel" && $potentialResa->_selType=="end"))}
            		<td{if $linetype=="sel"} class="myOwnCalendarLineItem"{/if}></td>
            {/if}
