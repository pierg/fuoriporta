				    {foreach from=$reservationView->getAvailableProducts($id_lang, $day->date, $timeSlot) key=id_product item=product name=productsLoop}
				    {if array_key_exists($id_product,$productsImages)}
				    	{assign var='viewedProduct' value=$productsImages[$id_product]}
	{assign var='slice' value=$day->getSlice($timeSlot->sqlId)}
	{if $slice==null}
	    <div class="emptySlot {$planningType}Slot" style="height:{$mediumSize->height + 15}px;margin-bottom:5px">&nbsp;</div>
	{else}
		{if !$slice->isAvailable || !$id_product|array_key_exists:$slice->availableQuantities}
			<div class="holidaySlot {$planningType}Slot" style="height:{$mediumSize->height + 15}px;margin-bottom:5px"><div class="unslotLabel">{$slice->label}</div></div>
		{else}
			{if !$slice->isEnable}
				<div class="unavailableSlot {$planningType}Slot" style="height:{$mediumSize->height}px;margin-bottom:16px"><div class="unslotLabel">{$slice->label}</div></div>
			{else}
			    {assign var='timeSlotAvailable' value=( $id_product|array_key_exists:$slice->availableQuantities && $slice->availableQuantities[$id_product] >= $potentialResa->getOccupationQty() or $slice->availableQuantity <0)}
			    <div {if $timeSlotAvailable}
						style="height:{$mediumSize->height + 15}px" 
				    	time="{$slice->start}" 
				    	key="{$slice->getKey()}" 
				    	product="{$id_product}"
				    	id="{$planningType}{$slice->getKey()}" 
				    	{if ($potentialResa->_selType=="start")}
				    		label="{$calendar->formatDateWithDay($slice->start, $id_lang)} {if $slice->idTimeSlot}{$calendar->formatTime($slice->getStartTime(), $id_lang)}{/if}" 
				    	{/if}
				    	{if ($potentialResa->_selType=="end")}
				    		label="{$calendar->formatDateWithDay($slice->end, $id_lang)} {if $slice->idTimeSlot}{$calendar->formatTime($slice->getEndTime(), $id_lang)}{/if}" 
				    	{/if}
				    	onclick="my{$planningSelector}Selector.{if $timeSlot->sqlId!=-1}slotSelection(this){else}showDay('{$day->key}'){/if};" 
				    	class="{if $slice->css!=""}{$slice->css}{else}availableSlot{/if} {$planningType}Slot" 
						name="{if $slice->css!=""}{$slice->css}{else}availableSlot{/if}"
				    {else}
				    	class="unavailableSlot {$planningType}Slot"
				    	name="unavailableSlot"
				    	style="height:{$mediumSize->height + 15}px"
			    	{/if}
			    >
				    <div class="slotInput">
				    	{if !$widget}
				            <input name="myOwnDeliveriesRes" type="{if $reservationView->multiSel}checkbox{else}radio{/if}" {if !$timeSlotAvailable}disabled{/if} value="{$slice->getKey()}" />
				        {/if}
				    </div>
			        <div class="slotLabel">
				        {$slice->label}{if $slice->label!=''}{/if}
			        	{if $timeSlot->sqlId>-1}
				        	{if $potentialResa->showQuantity()}
				        		<br/><span class="quantityLabel">{if $slice->availableQuantities[$id_product] == 0} {l s='not' mod='myownreservations'} {$previewQuantityLabel}{else}{if $slice->availableQuantities[$id_product] >0}{$slice->availableQuantities[$id_product]} {if $slice->availableQuantity >1}{$previewQuantityLabels}{else}{$previewQuantityLabel}{/if}{/if}{/if}</span>
				        	{/if}
			        	{/if}
			        </div>
			    </div>
			{/if}
		{/if}
	{/if}
	{/if}
{/foreach}