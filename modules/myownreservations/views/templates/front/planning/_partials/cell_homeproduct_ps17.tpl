				    <ul class="featured-products" style="margin:1px;overflow:hidden">
				    {foreach from=$reservationView->getAvailableProducts($id_lang, $day->date, $timeSlot) key=id_product item=product name=productsLoop}
				    {if array_key_exists($id_product,$productsImages) && ($timeSlot->id_product==0 || $timeSlot->id_product==$id_product)}
				    	{assign var='viewedProduct' value=$productsImages[$id_product]}
	<li class="ajax_block_product myOwnPad5">
	{assign var='slice' value=$day->getSlice($timeSlot->sqlId)}
	{if $slice==null}
	    <div class="emptySlot {$planningType}Slot" style="{if $widgetHome!=2}height:{$mediumSize->height + 15}px;margin-bottom:5px{/if}">&nbsp;</div>
	{else}
		{if 0}
			<div class="emptySlot {$planningType}Slot" style="{if $widgetHome!=2}height:{$mediumSize->height + 15}px;margin-bottom:5px{/if}"></div>
		{else}
			{if !$slice->isEnable}
				<div class="emptySlot {$planningType}Slot" style="{if $widgetHome!=2}height:{$mediumSize->height}px;margin-bottom:16px{/if}"></div>
			{else}
			    {assign var='timeSlotAvailable' value= $slice->isAvailable && $id_product|array_key_exists:$slice->availableQuantities && $slice->availableQuantities[$id_product]>0 && ($slice->availableQuantities[$id_product] >= $potentialResa->getOccupationQty() or $slice->availableQuantity <0)}
			    
	
				    
				    
				    
{if $timeSlotAvailable}
				    
				    
				    
	<article class="product-miniature js-product-miniature" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" itemscope itemtype="http://schema.org/Product">
  <div class="thumbnail-container" style="height:{$mediumSize->height + 80}px;width:{$mediumSize->width + 15}px;">
    {block name='product_thumbnail'}
      <a href="{$myownlink->getProductLink($viewedProduct->id, $viewedProduct->link_rewrite, $viewedProduct->category_rewrite)}?&selection={$slice->getKey()}&sel={$sel}" class="thumbnail product-thumbnail">
        <img
          src = "{$myownlink->getImageLink($viewedProduct->link_rewrite, $viewedProduct->cover, {$mediumSize->name})}"
          alt = "{if !empty($viewedProduct->legend)}{$viewedProduct->legend|escape:'html':'UTF-8'}{else}{$viewedProduct->name|escape:'html':'UTF-8'}{/if}"
        >
      </a>
    {/block}

    <div class="product-description" style="width:{$mediumSize->width + 15}px;">
      {block name='product_name'}
        <h1 class="h3 product-title" style="margin-top:2px;" itemprop="name"><a style="font-size: 0.75rem;" href="{$myownlink->getProductLink($viewedProduct->id, $viewedProduct->link_rewrite, $viewedProduct->category_rewrite)}?&selection={$slice->getKey()}&sel={$sel}">{$product.name|truncate:30:'...'}</a></h1>
      {/block}

      {block name='product_price_and_shipping'}
        {if 1}{*$product.show_price*}
          <div class="product-price-and-shipping">
            {*if $product.has_discount}
              {hook h='displayProductPriceBlock' product=$product type="old_price"}

              <span class="regular-price">{$product.regular_price}</span>
              {if $product.discount_type === 'percentage'}
                <span class="discount-percentage">{$product.discount_percentage}</span>
              {/if}
            {/if*}

            {hook h='displayProductPriceBlock' product=$product type="before_price"}

            <span itemprop="price" class="price">{$product.price}</span>

            {hook h='displayProductPriceBlock' product=$product type='unit_price'}

            {hook h='displayProductPriceBlock' product=$product type='weight'}
          </div>
        {/if}
      {/block}
    </div>
    {*block name='product_flags'}
      <ul class="product-flags">
        {foreach from=$product.flags item=flag}
          <li class="{$flag.type}">{$flag.label}</li>
        {/foreach}
      </ul>
    {/block*}
    <div class="highlighted-informations{*if !$product.main_variants} no-variants{/if*} hidden-sm-down"  style="width:{$mediumSize->width + 15}px;">
      <a
        href="#"
        class="quick-view"
        data-link-action="quickview"
         style="font-size: 0.75rem;"
      >
        <i class="material-icons search">&#xE8B6;</i> {l s='Quick view' d='Shop.Theme.Actions'}
      </a>

      <a tabindex style="font-size: 0.70rem;" onclick="myownreservationCallCart({$product.id_product|intval}, 0, 1, '{$slice->getKey()}{if $potentialResa->_selType == "start"}@{/if}', this);" rel="nofollow" title="{l s='Add to cart'}" data-id-product="{$product.id_product|intval}">
		<i class="material-icons search">shopping_cart</i> {l s='Add to cart'}
									</a>
    </div>

  </div>
</article>

				    
				    
			{/if}	    
				    
					</div>
							
			        	
			        
			   
			{/if}
		{/if}
	{/if}
	</li>
					{/if}
				    {/foreach}
				    </ul>