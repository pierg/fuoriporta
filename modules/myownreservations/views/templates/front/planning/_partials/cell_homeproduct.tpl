				    <ul class="product_list grid" style="margin:1px;overflow:hidden">
				    {foreach from=$reservationView->getAvailableProducts($id_lang, $day->date, $timeSlot) key=id_product item=product name=productsLoop}
				    {if array_key_exists($id_product,$productsImages) && ($timeSlot->id_product==0 || $timeSlot->id_product==$id_product)}
				    	{assign var='viewedProduct' value=$productsImages[$id_product]}
	<li class="ajax_block_product myOwnPad5">
	{assign var='slice' value=$day->getSlice($timeSlot->sqlId)}
	{if $slice==null}
	    <div class="emptySlot {$planningType}Slot" style="{if $widgetHome!=2}height:{$mediumSize->height + 15}px;margin-bottom:5px{/if}">&nbsp;</div>
	{else}
		{if 0}
			<div class="emptySlot {$planningType}Slot" style="{if $widgetHome!=2}height:{$mediumSize->height + 15}px;margin-bottom:5px{/if}"></div>
		{else}
			{if !$slice->isEnable}
				<div class="emptySlot {$planningType}Slot" style="{if $widgetHome!=2}height:{$mediumSize->height}px;margin-bottom:16px{/if}"></div>
			{else}
			    {assign var='timeSlotAvailable' value= $slice->isAvailable && $id_product|array_key_exists:$slice->availableQuantities && $slice->availableQuantities[$id_product]>0 && ($slice->availableQuantities[$id_product] >= $potentialResa->getOccupationQty() or $slice->availableQuantity <0)}
			    
				    <div class="slotInput">
				    	{if !$widget}
				            <input name="myOwnDeliveriesRes" type="{if $reservationView->multiSel}checkbox{else}radio{/if}" {if !$timeSlotAvailable}disabled{/if} value="{$slice->getKey()}" />
				        {/if}
				    </div>
				    
				    
				    
				    
				    
				    
				    
				    
				    {if $timeSlotAvailable}
				    
				    
				    
				    <div class="product-container" style="padding-top:1px">
				    
				    
				    
				    
				    
<div class="left-block">
					{if $imageSize>-1}
					<div class="product-image-container" style="padding:0px;margin-bottom:5px">
						<a class="product_img_link"	href="{$myownlink->getProductLink($viewedProduct->id, $viewedProduct->link_rewrite, $viewedProduct->category_rewrite)}?&selection={$slice->getKey()}&sel={$sel}#idTabResa">
							<img class="replace-2x" src="{$myownlink->getImageLink($viewedProduct->link_rewrite, $viewedProduct->cover, {$mediumSize->name})}" alt="{if !empty($viewedProduct->legend)}{$viewedProduct->legend|escape:'html':'UTF-8'}{else}{$viewedProduct->name|escape:'html':'UTF-8'}{/if}" title="{if !empty($viewedProduct->legend)}{$viewedProduct->legend|escape:'html':'UTF-8'}{else}{$viewedProduct->name|escape:'html':'UTF-8'}{/if}" {if isset($homeSize)} width="{$homeSize.width}" height="{$homeSize.height}"{/if} itemprop="image" />
						</a>


							<div class="content_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
								<span itemprop="price" class="price product-price" style="font-size:16px">
									{displayPrice price=$product.price}
								</span>
							</div>

						{if isset($product.new) && $product.new == 1}
							<span class="new-box">
								<span class="new-label">{l s='New'}</span>
							</span>
						{/if}
						{if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
							<span class="sale-box">
								<span class="sale-label">{l s='Sale!'}</span>
							</span>
						{/if}
					</div>
				</div>
				{/if}
				<div class="right-block" style="min-height:55px">
					{*{if $resa_category==0}<h6 class="widget_title"><b>{$potentialResa->_mainProduct->_mergedNames[$timeSlot->id_family]}</b></h6>{/if}*}
					
					<h5 itemprop="name" style="margin-top:2px;margin-bottom:2px;height: 30px;overflow-y: hidden;">
						<a style="font-size: 12px;line-height: 14px;" class="product-name" href="{$myownlink->getProductLink($viewedProduct->id, $viewedProduct->link_rewrite, $viewedProduct->category_rewrite)}?&selection={$slice->getKey()}&sel={$sel}#idTabResa" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url" >
							{$viewedProduct->name|truncate:45:'...'|escape:'html':'UTF-8'}
						</a>
					</h5>
					<p class="product-desc" itemprop="description">
						{$product.description_short|strip_tags:'UTF-8'|truncate:360:'...'}
					</p>
					{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price)))}
					<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
						{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}
							<span itemprop="price" class="price product-price">
								{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}
							</span>
							<meta itemprop="priceCurrency" content="{$priceDisplay}" />
							{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
								<span class="old-price product-price">
									{displayPrice price=$product.price_without_reduction}
								</span>
								{if $product.specific_prices.reduction_type == 'percentage'}
									<span class="price-percent-reduction">-{$product.specific_prices.reduction * 100}%</span>
								{/if}
							{/if}
						{/if}
					</div>
					{/if}
					<div class="button-container">
						{if !isset($restricted_country_mode) && !$PS_CATALOG_MODE}
							{if $slice->isAvailable && $id_product|array_key_exists:$slice->availableQuantities && $slice->availableQuantities[$id_product] > 0}
									<a style="padding: 3px 6px;" class="button-small ajax_add_to_cart_button btn btn-default" onclick="myownreservationCallCart({$product.id_product|intval}, 0, 1, '{$slice->getKey()}{if $potentialResa->_selType == "start"}@{/if}', this);" rel="nofollow" title="{l s='Add to cart'}" data-id-product="{$product.id_product|intval}">
										<span>{l s='Add to cart'}</span>
									</a>					
							{else}
								<span style="padding: 3px 6px;" class="button-small ajax_add_to_cart_button btn btn-default disabled">
									<span>{l s='Add to cart'}</span>
								</span>
							{/if}
						{/if}
					</div>
					{if isset($product.color_list)}
						<div class="color-list-container">{$product.color_list} </div>
					{/if}
					<div class="product-flags">
						{if (!$PS_CATALOG_MODE && isset($product.show_price) && $product.show_price)}
							{if isset($product.online_only) && $product.online_only}
								<span class="online_only">{l s='Online only'}</span>
							{/if}
						{/if}
						{if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
							{elseif isset($product.reduction) && $product.reduction && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
								<span class="discount">{l s='Reduced price!'}</span>
							{/if}
					</div>
					{if 0}
					<span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="availability">
								<span class="{if $timeSlotAvailable}label-success{else}out-of-stock{/if}">
									<link itemprop="availability" href="http://schema.org/InStock" />{if $potentialResa->showQuantity()}{if !$slice->isAvailable || !$id_product|array_key_exists:$slice->availableQuantities || $slice->availableQuantities[$id_product] == 0} {l s='not' mod='myownreservations'} {$previewQuantityLabel}{else}{if $slice->availableQuantities[$id_product] >0}{$slice->availableQuantities[$id_product]} {if $slice->availableQuantity >1}{$previewQuantityLabels}{else}{$previewQuantityLabel}{/if}{/if}{/if}{else}{if !$timeSlotAvailable}{l s='not' mod='myownreservations'} {/if}{$previewQuantityLabel}{/if}
								</span>
							</span>
					{/if}
				</div>
				    
				    
			{/if}	    
				    
					</div>
							
			        	
			        
			   
			{/if}
		{/if}
	{/if}
	</li>
					{/if}
				    {/foreach}
				    </ul>