{*
* 2010-2017 LaBulle
*
* NOTICE OF LICENSE
*
* This source file is subject to Terms and Conditions of Use 
* Available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade module to newer
* versions in the future. If you wish to customize module for your
* needs please refer to http://www.labulle.net for more information.
*
* @author    laBulle.net <info@labulle.net>
* @copyright 2010-2017 laBulle
* @license   http://www.labulle.net/download/CGV-en.pdf
*}
{$datepicker nofilter}
{$tspicker nofilter}
<link href="{$module_dir}views/css/widget.css" rel="stylesheet" type="text/css" media="all" />
<link href="{$module_dir}views/css/planning.css" rel="stylesheet" type="text/css" media="all" />
<link href="{$module_dir}views/css/widget-top.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" defer>
var minDate=new Date({$minDate}000);
var maxDate=new Date({$maxDate}000);
document.addEventListener("DOMContentLoaded", function(event) {
	initPickerSearch ();
});
</script>
{if $_PS_VERSION_>="1.7"}
<div class="container" id="widget-top-new">
{else}
<div id="period_block" class="block exclusive">
{/if}
	<form action="{$myownlink->getPageLink('search', true)|escape:'html'}" name="myownresWidgetSearch" method="POST">
	{if $_PS_VERSION_>="1.7"}
		<h2>{$reservationLabel}</h2>
	{else}
		<h4>{$reservationLabel}</h4>
	{/if}
	<div class="{if $_PS_VERSION_<"1.7"}block_content{else}myownr_form{/if}" style="padding:0px">
		{if $_PS_VERSION_<"1.7"}
		<p style="padding-bottom:0px;padding-top:5px;">
			{l s='Select a period' mod='myownreservations'}
		</p>
		<div style="myOwnResField">
			<label class="myOwnResLabelField">{l s='Start' mod='myownreservations'}</label>
			<input size="8" type="text" class="myOwnResDateField" onchange="validateSearchStartDate();populateTimeslot('start');" name="searchStartDay" id="searchStart" value="" {if isset($searchStartDay)}default="{$searchStartDay}000"{/if}> 
			<select class="myOwnResTSField" id="searchStartTimeslot" name="searchStartTimeslot" style="display:none"></select><div style="clear:both"></div>
		</div>
		{else}
		<div class="myownr_field"><input type="text" name="s" class="form-control ui-autocomplete-input" placeholder="{l s='Search' mod='myownreservations'}" autocomplete="off"></div>
		<div class="myownr_field">
			<a class="myownr_buttons" id="myownr_btn_start" onclick="" onblur="">
				{if $_PS_VERSION_>="1.7"}
					<i class="material-icons">event</i>
				{else}
					<i class="icon-calendar"></i>
				{/if}
				<div class="content">
					<span class="label" default="{$myOwnResLblStart}">{$myOwnResLblStart}</span>
					<span class="sel"><input size="9" type="text" class="myOwnResDateFieldSearch" onchange="validateSearchStartDate();populateTimeslot('start');" name="searchStartDay" id="searchStart" value="" {if isset($searchStartDay)}default="{$searchStartDay}000"{/if} onChange="populateTimeslot('start');"> </span>
				</div>
			</a>
		</div>
		{/if}
		{if $_PS_VERSION_<"1.7"}
		<div style="myOwnResField">
			<label class="myOwnResLabelField">{l s='End' mod='myownreservations'}</label>
			<input size="8" type="text" class="myOwnResDateField" name="searchEndDay" id="searchEnd" value="" {if isset($searchEndDay)}default="{$searchEndDay}000"{/if} onChange="populateTimeslot('end');"> 
			<select class="myOwnResTSField" id="searchEndTimeslot" name="searchEndTimeslot" style="display:none"></select>
		</div>
		{else}
		<div class="myownr_field">
			<a class="myownr_buttons" id="myownr_btn_start" onclick="" onblur="">
				{if $_PS_VERSION_>="1.7"}
					<i class="material-icons">event</i>
				{else}
					<i class="icon-calendar"></i>
				{/if}
				<div class="content">
					<span class="label" default="{$myOwnResLblEnd}">{$myOwnResLblEnd}</span>
					<span class="sel"><input size="9" type="text" class="myOwnResDateFieldSearch" onchange="validateSearchStartDate()" name="searchEndDay" id="searchEnd" value="" {if isset($searchEndDay)}default="{$searchEndDay}000"{/if} onChange="populateTimeslot('end');"> </span>
				</div>
			</a>
		</div>
		{/if}
		<div style="clear:both"></div>
		{if $_PS_VERSION_<"1.7"}
		<p id="cart-buttons" style="text-align:center;padding-bottom:0px;padding-top:5px;">
			<span id="searchStartDisabledButton" class="exclusive" style="">{l s='Search' mod='myownreservations'}</span>
			<button onclick="validatePeriod()" id="searchStartEnabledButton" class="btn btn-default button button-small" style="display:none"><span>{l s='Search' mod='myownreservations'} <i class="icon-search right"></i></span></button>
		</p>
		{else}
		<div class="myownr_field" style="width:140px">
			<a class="myownr_buttons myownr_disabledbtn" id="myownr_btn_validate" onclick="validatePeriod()" >
				<i class="icon-search"></i>
				<span class="action">
					{l s='Search' mod='myownreservations'}
				</span>
			</a>
		</div>
		{/if}
	</div>
	</form>
</div>
