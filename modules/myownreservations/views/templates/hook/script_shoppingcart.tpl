{if !$prePS16}
	{if $is_stock_management}
		{assign var='col_span_subtotal' value='3'}
	{else}
		{assign var='col_span_subtotal' value='2'}
	{/if}
{else}
	{assign var='col_span_subtotal' value='6'}
{/if}
{if !$prePS16 && $stdCheckout && $currentStep==3}
	{assign var='order_payment' value='1'}
{else}
	{assign var='order_payment' value='0'}
{/if}
<script type="text/javascript">

 	    {if !$isAjaxLoaded}{literal}document.addEventListener("DOMContentLoaded", function(event) {{/literal}{/if}
 {literal}

 	    
 	    	setReservationPrices(eval({/literal}{$myOwnCartList nofilter}{literal})); 
 	    	setCarSummaryUnits();
 	    	var subadvance = "";
 	    	var carttotaltdc = "";
 	    	//patch for cart packaging disapair on PS1.5
 	    	$(".cart_total_voucher").eq(1).attr("class", "cart_total_deposit");
 	{/literal}
 			//display purchases if total_products greater than resa amount 
			{assign var='newcolspan' value=0} 
			{assign var='cart_total_rowspan' value=2} 
 	        {if $myowncart.total_reservations_wt>0}
 	        	{if $myowncart.total_goods_wt>0}
 	        		{assign var=newcolspan value=$newcolspan+1}
 					$(".cart_total_purchases").remove();
 					var subtotal = "<tr class=\"cart_total_purchases\"><td colspan=\"{$col_span_subtotal}\">{l s='Total purchases (taxes incl.)' mod='myownreservations'}</td><td {if !$prePS15}colspan=\"2\"{/if} class=\"price\">{displayPrice price=$myowncart.total_goods_wt}</td></tr>";
 					$("#cart_summary tfoot tr:first").before(subtotal);
 	        	 {else}
 	        	 	{if $prePS16}if ($(".cart_total_price").length==3) $(".cart_total_price").eq(1).remove();{/if}
 	        	 {/if}
 	        {/if}
 			{if $order_payment}{assign var=col_span_subtotal value=$col_span_subtotal+1}{/if}
 			
 	        var extra_fees='';
	        {if isset($fee_details)}
	        		{foreach from=$fee_details item=fee_detail_val key=fee_detail_key name=feeLoop}
	        			
						extra_fees += "<tr class=\"cart_fees_reservations\"><td class=\"text-right\" colspan=\"{$col_span_subtotal}\">{$fee_titles.$fee_detail_key} :</td><td {if !$prePS15}colspan=\"2\"{/if} class=\"price\">{displayPrice price=$fee_detail_val}</td></tr>";
        		{/foreach}
        		$(".cart_total_price").last().before(extra_fees);
        	{/if}
	        
 	        //display reservations details if there is
 	        {if $myowncart.total_reservations_wt>0} 
 	        	//display advance and order total
 	        	var carttotaltdc = $("tr.cart_total_price:first td").length;
 	       
     	        {if $myowncart.balance_reservations_wt>0}
     	        	{assign var=newcolspan value=$newcolspan+3}
     	        	
     	       		$(".cart_total_advance").remove();
     	       		$(".cart_total_balance").remove();
     	       		{if $myowncart.total_goods_wt>0}
     	       			
     	        		subadvance = "<tr class=\"cart_total_advance\">{if !$order_payment}<td {if $myowncart.total_goods_wt>0}rowspan=\"2\" {/if}colspan=\"2\"></td>{/if}<td class=\"text-right\" colspan=\"{$col_span_subtotal}\">{l s='Total' mod='myownreservations'} {$advance_title} {$resv_title} ({l s='taxes incl.' mod='myownreservations'}):</td><td {if !$prePS15}colspan=\"2\"{/if} class=\"price\">{displayPrice price=$myowncart.advance_reservations_wt}</td></tr>";
     	        	{else}
     	        		$(".cart_total_price td").eq({if !$order_payment}1{else}0{/if}).html('{$advance_title} {$resv_title} ({l s='taxes incl.' mod='myownreservations'})');
     	        	{/if}
     	        	{if $prePS16}$(".cart_total_price").first().after(subadvance);{else}$("#cart_summary tfoot tr:first").before(subadvance);{/if}
     	        	
     	        	var subbalance = "<tr class=\"cart_total_balance\">{if !$order_payment}<td rowspan=\"2\" colspan=\"2\"></td>{/if}<td colspan=\"{$col_span_subtotal}\">{$balance_title} ({l s='taxes incl.' mod='myownreservations'})</td><td {if !$prePS15}colspan=\"2\"{/if} class=\"price\"><b>{displayPrice price=$myowncart.balance_reservations_wt}</b></td></tr>";
     	        	$(".cart_total_order").remove();
     	        	var ordertotal = "<tr class=\"cart_total_order\"><td colspan=\"{$col_span_subtotal}\"><b>{l s='Total order' mod='myownreservations'} ({l s='taxes incl.' mod='myownreservations'})</b></td><td {if !$prePS15}colspan=\"2\"{/if} class=\"price\"><b>{displayPrice price=$myowncart.total_products_wt+$myowncart.balance_reservations_wt+$myowncart.total_shipping+$myowncart.fee_reservations-$myowncart.total_discounts+$myowncart.balance_discounts_wt}</b></td></tr>";
     	        	$("#cart_summary tfoot tr:last").after(subbalance+ordertotal);
     	    	{else}
     	    		$(".cart_total_reservations").remove();
     	        	var subtotal = "<tr class=\"cart_total_reservations\">{if !$order_payment}<td colspan=\"2\"></td>{/if}<td class=\"text-right\" colspan=\"{$col_span_subtotal}\">{l s='Total' mod='myownreservations'} {$resv_title} {if $totalResaTaxInc}{l s='taxes incl.' mod='myownreservations'}{else}{l s='(taxes excl.)' mod='myownreservations'}{/if}:</td><td {if !$prePS15}colspan=\"2\"{/if} class=\"price\">{if $totalResaTaxInc}{displayPrice price=$myowncart.total_reservations_wt}{else}{displayPrice price=$myowncart.total_reservations}{/if}</td></tr>";
     	        	$(".cart_total_price").first().parent().html(subtotal+$(".cart_total_price").first().parent().html());
     	        {/if}
 	        {/if}

 	        {if $show_deposit > 0}
 	        	{assign var=newcolspan value=$newcolspan+1}
 	        	if ($(".cart_total_deposit").length==0)
 	        	var show_deposit = "<tr class=\"cart_total_deposit\"><td colspan=\"{$col_span_subtotal}\"><b>{$deposit_title}</b></td><td {if !$prePS15}colspan=\"2\"{/if} class=\"price\"><b>{displayPrice price=$show_deposit}</b></td></tr>";
     	        	$("#cart_summary tfoot tr:last").after(show_deposit);
 	        {/if}

 	        var newcolspan = $('#cart_summary tfoot tr:visible').length - {$newcolspan};
 	        if (carttotaltdc>2) $("tr.cart_total_price:first td:first").first().attr('rowspan', newcolspan);
 	        
 	 {if !$isAjaxLoaded}
 {literal}
 		});
 {/literal}
 	{/if}
 	
</script>