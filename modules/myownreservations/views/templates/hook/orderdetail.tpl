{*
* 2010-2017 LaBulle
*
* NOTICE OF LICENSE
*
* This source file is subject to Terms and Conditions of Use 
* Available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade module to newer
* versions in the future. If you wish to customize module for your
* needs please refer to http://www.labulle.net for more information.
*
* @author    laBulle.net <info@labulle.net>
* @copyright 2010-2017 laBulle
* @license   http://www.labulle.net/download/CGV-en.pdf
*}
{if $order_total_balance>0 && !$hidePrice}
<script type="text/javascript">
{if $_PS_VERSION_>="1.7"}
{literal}
document.addEventListener("DOMContentLoaded", function(event) {
	$(".product-customization-line").each( function(index) {
		var period = $(this).find(".value").html();
		var label = $(this).find(".label").html();
		//alert($(this).parents("li").html());
		//$(this).parents("li").find(".product-line-actions").css("padding-bottom", "20px");
		if ($(this).parents("td").find(".reservation_label").length==0) 
			$(this).parents("td").append('<div class="product-line-grid-right"><span class="label reservation_label">'+label+':</span><span class="value">'+period+'</span></div>');
		//$(this).parents("li").find(".reservation_label").after('<div class="product-line-grid-right"></div>');
		$(this).parents("td").find(".customization").remove();
	});
{/literal}
{/if}
{if $order_total_advance>0}
	$(".std tfoot, #order-detail-content tfoot").append('<tr class="item" style="border-top: 1px solid #bdc2c9;"><td colspan="5">{$advance_label}: <span class="price-shipping">{displayPrice price=$order_total_advance}</span></td></tr><tr class="item"><td colspan="5">{$balance_label}: <span class="price-shipping">{displayPrice price=$order_total_balance}</span></td></tr>');
	$("#order-products tfoot").append('<tr class="text-xs-right line-total"><td colspan="3">{$advance_label}</td><td>{displayPrice price=$order_total_advance}</td></tr><tr class="text-xs-right line-total"><td colspan="3">{$balance_label}</td><td>{displayPrice price=$order_total_balance}</td></tr>');
{/if}
{if $_PS_VERSION_>="1.7"}
{literal}
});
{/literal}
{/if}
</script>
{/if}
