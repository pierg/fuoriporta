<div id="reservations_prices">
	<table class="std table-product-priceset">
		<thead>
			<tr>
				<th>{l s='Prices' mod='myownreservations'} {if !$istimeprice}/{$unit}{/if}</th>
				{foreach from=$productAttributes key=productAttributeKey item=productAttributeValue}
				<th>{$productAttributeValue}</th>
				{/foreach}
			</tr>
		</thead>
		<tbody>
		{if $istimeprice}
			{for $length=1 to $maxpricelength}
			<tr>
				<td>{$length} {if $length==1}{$unit}{else}{$units}{/if}</td>
			  {foreach from=$productAttributes key=productAttributeKey item=productAttributeValue}
				<td>{convertPrice price=$productAttributesPrices[$productAttributeKey][$length]}</td>
			  {/foreach}
			</tr>
			{/for}
			{if $maxpricelength<$maxlength}
			<tr>
				<td>{l s='Extra day' mod='myownreservations'}</td>
			  {foreach from=$productAttributes key=productAttributeKey item=productAttributeValue}
				<td>{convertPrice price=$lastRaise[$productAttributeKey]}</td>
			  {/foreach}
			</tr>
			{/if}
		{else}
			{foreach from=$productPricesPeriod key=periodKey item=periodLabel}
			<tr>
				<td>{$periodLabel}</td>
			  {foreach from=$productAttributes key=productAttributeKey item=productAttributeValue}
				<td>{convertPrice price=$productAttributesPrices[$productAttributeKey][$periodKey]}</td>
			  {/foreach}
			</tr>
			{/foreach}
		{/if}
			{foreach from=$accessoriesPrices key=accessoriesName item=accessoriesPrice}
				<td>{$accessoriesName}</td>
				<td colspan="{$productAttributes|@count}">{convertPrice price=$accessoriesPrice}</td>
			{/foreach}
		</tbody>
	</table>
</div>