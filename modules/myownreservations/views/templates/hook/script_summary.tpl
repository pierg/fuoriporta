{if isset($fee_details)}
	{foreach from=$fee_details item=fee_detail_val key=fee_detail_key name=feeLoop}
		<div class="cart-summary-line" id="cart-subtotal-products"><span class="label">{$fee_titles.$fee_detail_key}</span><span class="value">{displayPrice price=$fee_detail_val}</span></div>
	{/foreach}
{/if}

<script type="text/javascript">

 	    {if !$isAjaxLoaded}{literal}document.addEventListener("DOMContentLoaded", function(event) {{/literal}{/if}
 {literal}
 			//display customization details
 			if ($(".reservation_label").length==0)
 			 $(".product-customization-line").each( function(index) {
 	    		var period = $(this).find(".value").html();
 	    		var label = $(this).find(".label").html();
 	    		//alert($(this).parents("li").html());
 	    		$(this).parents("li").find(".product-line-actions").css("padding-bottom", "20px");
 	    		if ($(this).parents("li").find(".reservation_label").length==0) 
 	    			$(this).parent().parent().parent().parent().prev().parent().next().after('<div class="product-line-grid-right"><span class="label reservation_label">'+label+':</span></div>');
 	    		$(this).parents("li").find(".reservation_label").after('<div class="product-line-grid-right"><span class="value">'+period+'</span></div>');
 	    		$(this).parent().parent().parent().parent().prev().remove();
 	    	});
 {/literal}
 	        //display reservations details if there is
 	        {if $myowncart.total_reservations_wt>0} 
 	        	//display advance and order total

     	        {if $myowncart.balance_reservations_wt>0}

     	       		{if $myowncart.total_goods_wt>0}
     	       			var detail_cart = '<div class="cart-summary-line" id="cart-subtotal-products"><span class="label">{l s='Total purchases' mod='myownreservations'}</span><span class="value">{displayPrice price=$myowncart.total_goods_wt}</span></div><div class="cart-summary-line" id="cart-subtotal-products"><span class="label">{$advance_title} {$resv_title}</span><span class="value">{displayPrice price=$myowncart.advance_reservations_wt}</span></div>';
     	       			if ($(".cart-summary-products").length) 
     	        			$(".cart-summary-products").after(detail_cart+'<hr/>');
     	        		else $("#cart-subtotal-products").parent().before('<div class="card-block">'+detail_cart+'</div><hr/>');
     	        	{else}
     	        		$("#cart-subtotal-products .label").html("{$advance_title} {$resv_title}");
     	        	{/if}
     	        	
     	        	var total_resas='<div class="cart-summary-line" id="cart-subtotal-products"><span class="label">{l s='Total' mod='myownreservations'} {$resv_title}</span><span class="value">{displayPrice price=$myowncart.total_reservations_wt}</span></div>';
     	        	if ($(".cart-summary-products").length) 
     	        		$(".cart-summary-products").after(total_resas+'<hr/>');
     	        	else $("#cart-subtotal-products").parent().before('<div class="card-block">'+total_resas+'</div><hr/>');
     	        	
     	        	$(".cart-total").css("font-weight", "bold").parent().after('<hr/><div class="card-block" id="cart-total-after"><div class="cart-summary-line"><span class="label">{$balance_title}</span><span class="value">{displayPrice price=$myowncart.balance_reservations_wt}</span></div><div class="cart-summary-line" id="cart-subtotal-products"><span class="label">{l s='Total order' mod='myownreservations'}</span><span class="value">{displayPrice price=$myowncart.total_products_wt+$myowncart.balance_reservations_wt+$myowncart.total_shipping+$myowncart.fee_reservations-$myowncart.total_discounts}</span></div></div>');
     	        	{if $show_deposit > 0}
     	        		$("#cart-total-after").append('<div class="cart-summary-line" id="cart-subtotal-deposit"><span class="label">{$deposit_title}</span><span class="value">{displayPrice price=$show_deposit}</span></div>');
     	        	{/if}
     	    	{else}
	 				{if $show_deposit > 0}
	 					$(".cart-total").css("font-weight", "bold").parent().after('<hr/><div class="card-block"><div class="cart-summary-line" id="cart-subtotal-deposit"><span class="label">{$deposit_title}</span><span class="value">{displayPrice price=$show_deposit}</span></div></div>');
	 				{/if}
     	        {/if}
 	        {/if}
 	        
 	 {if !$isAjaxLoaded}
 {literal}
 		});
 {/literal}
 	{/if}
 	
</script>