<table style="width: 100%">
	<tr style="{if $_PS_VERSION_<"1.7"}line-height:5px;{/if}">
		<td style="width: 85%; text-align: right; font-weight: bold;background-color: #4D4D4D; color: #FFF;{if $_PS_VERSION_>="1.7"}height:12px;{/if}">{$advance_label} {$resv_label}&nbsp;&nbsp;</td>
		<td style="width: 15%; text-align: right; font-weight: bold; background-color: #4D4D4D; color: #FFF;" {if $_PS_VERSION_>="1.7"}height:12px;{/if}>{$myown_advance}</td>
	</tr>
	<tr {if $_PS_VERSION_<"1.7"}style="line-height:5px;"{/if}>
		<td style="width: 85%; text-align: right; font-weight: bold;{if $_PS_VERSION_>="1.7"}height:12px;{/if}" {if $_PS_VERSION_>="1.7"}class="grey"{/if}>{$balance_label}</td>
		<td style="width: 15%; text-align: right;" {if $_PS_VERSION_>="1.7"}class="white"{/if}>{$myown_balance}</td>
	</tr>
</table>