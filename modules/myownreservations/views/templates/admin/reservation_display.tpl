
<table class="table" width="100%;" cellspacing="0" cellpadding="0" id="documents_table" style="background-color:#FFF;padding-bottom:6px;">
	<thead>
	<tr>
		<th style="width:50%;text-align:center;padding:0px">{l s='Start' mod='myownreservations'}</th>
		<th></th>
		<th style="width:50%;text-align:center;padding:0px">{l s='End' mod='myownreservations'}</th>
	</tr>
	</thead>
	<tbody>
		<tr class="odd">
			<td class="day_head"{if $endKeys['month']==$startKeys['month']}colspan="3"{/if}>{$calendar->getMonthName($startDate, $id_lang, true)} {$startDate|date_format:"%Y"}</td>
			{if $endKeys['month']!=$startKeys['month']}
			<td></td>
			<td class="day_head">{$calendar->getMonthName($endDate, $id_lang, true)} {$endDate|date_format:"%Y"}</td>
			{/if}
		</tr>
		<tr>
			<td class="day_body" {if $endKeys['day']==$startKeys['day']}colspan="3"{/if}>
				<span class="dayName">{$calendar->getDayString($startDate,$id_lang,true)}</span>
				<span class="dayNumber">{$startDate|date_format:"%e"}</span>
			</td>
			{if $endKeys['day']!=$startKeys['day']}
			<td class="day_body" {if !$weekTs}rowspan="2"{/if}>
				<div class="arrow"></div>
			</td>
			<td class="day_body">
				<span class="dayName">{$calendar->getDayString($endDate,$id_lang,true)}</span>
				<span class="dayNumber">{$endDate|date_format:"%e"}</span>
			</td>
			{/if}
		</tr>
		{if $showTime}
		<tr>
			<td class="day_body" {if ($endKeys['hour']==$startKeys['hour'] && $endKeys['day']==$startKeys['day'])}colspan="3"{/if}>
				{$resa->getStartHour($myownreservations)}
			</td>
			{if ($endKeys['hour']!=$startKeys['hour'] || $endKeys['day']!=$startKeys['day'])}
				{if $endKeys['day']==$startKeys['day'] || $weekTs}
					<td class="day_body">
						<div class="arrow"></div>
					</td>
				{/if}
				<td class="day_body">
					{if $weekTs}{$resa->getStartEndHour($myownreservations)}{else}{$resa->getEndHour($myownreservations)}{/if}
				</td>
			{/if}
		</tr>
		{/if}
	</tbody>
</table>