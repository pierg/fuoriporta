/*

* 2010-2012 LaBulle All right reserved

*/

var panelSources=new Array();

var panelSourcesIndex=new Array();

var panelSources2=new Array();

var panelSourcesIndex2=new Array();

	function listElem(panel) {
	

		if (typeof(panel)!='undefined') {
			var cnt=0;

			var sel=0;

			var help="";

			$("#"+panel).first().find(".listElem").each(function( index ) {

				var back = $(this).parent().parent();

				cnt++;

				if ($(this).attr("checked")) {

					//change back

					sel = cnt;

					if (back.find(".subhint").length)

						help = back.find(".subhint").html();

					back.addClass("myownr_options");

				}

				else {

					back.removeClass("myownr_options");

				}

			});

			var total = cnt;

			var th_len = $("#"+panel).first().find("thead th").length;

			

			if (typeof(panelSources[panel])=='undefined')

				panelSources[panel]=new Array();

			//$("#"+panel).first().html(panelSources[panel]);

			

			if ($("#"+panel).first().attr("type")=="single") {

					var cnt=0;

					var content = "";

					$("#"+panel).first().find("tbody td.content").remove();

					//alert($("#"+panel).first().find("tbody td:not(.content):eq(3)").html());

					$("#"+panel+" > tbody").children().find("td:eq(3)").each(function( index ) {

						if (panelSourcesIndex[panel]==index)

							$(this).html(panelSources[panel]);

					});

					$("#"+panel+" > tbody").children().find("td:eq(3)").each(function( index ) {

						var rowspan = $(this).attr("rowspan");

						if (typeof(rowspan)=="undefined") rowspan=1;

						else rowspan=parseInt(rowspan);

						cnt++;

						

						if (sel >= cnt && sel < (cnt+rowspan)) {

							//$(this).find("input").removeAttr("disabled");

							//$(this).find("select").removeAttr("disabled");

							panelSources[panel] = $(this).html();

							panelSourcesIndex[panel] = index;

							content = $(this).html(); //$(this).attr("rowspan", total).show();

							$(this).html('');

						}

						$(this).hide();

						//$(this).find("input").remove();

						//$(this).find("select").remove();

						cnt+=rowspan-1;

					});

					$("#"+panel+" > tbody td:eq(3)").after('<td class="content" style="box-shadow: inset 10px 0px 15px -10px #999;padding-top:3px;padding-left: 15px;vertical-align:top;background-color:#FCFDFE;color: #666;" rowspan="'+total+'">'+(help!="" ? '<div class="alert alert-info" style="display:block;'+(th_len>3 && panel!='panelPrice' ? 'width:150%' : '')+'">'+help+"</div>" : "")+content+"</td>");

					var cnt=0;

					var content = "";

					console.log(panel+'='+$("#"+panel+" tbody > tr:eq(0) > td:not(.content):eq(4)").length);

					if ($("#"+panel+" tbody > tr:eq(0) > td:not(.content):eq(4)").length) {

						$("#"+panel).first().find("tbody").children().find("td:not(.content):eq(4)").each(function( index ) {

						if (panelSourcesIndex2[panel]==index)

							$(this).html(panelSources2[panel]);

						});

						$("#"+panel).first().find("tbody").children().find("td:not(.content):eq(4)").each(function( index ) {

							var rowspan = $(this).attr("rowspan");

							if (typeof(rowspan)=="undefined") rowspan=1;

							else rowspan=parseInt(rowspan);

							

							cnt++;

							if (sel >= cnt && sel < (cnt+rowspan)) {

								//$(this).find("input").removeAttr("disabled");

								//$(this).find("select").removeAttr("disabled");

								panelSources2[panel] = $(this).html();

								panelSourcesIndex2[panel] = index;

								content = $(this).html(); //$(this).attr("rowspan", total).show();

								$(this).html('');

							}

							//$(this).find("input").attr("disabled", "disabled");

							//$(this).find("select").attr("disabled", "disabled");

							$(this).hide();



							cnt+=rowspan-1;

						});

						$("#"+panel).first().find("tbody td:eq(4)").after('<td class="content" style="padding-top:3px;vertical-align:top;background-color:#FCFDFE;color: #666;'+(th_len>3 && panel!='panelPrice' && help!="" ? 'padding-top:85px;' : '')+'" rowspan="'+total+'">'+content+"</td>");

					}

			}

			if ($("#"+panel).first().attr("type")=="multiple") {

				colcount = $("#"+panel).first().find("tbody tr:first").children().length;

				$("#"+panel).first().find(".listElem").each(function( index ) {

					var back = $(this).parent().parent();

					var help="";

					back.find(".subhint").attr("class", "hidehint")

					cnt++;

					if ($(this).attr("checked")) {

						back.find("td:eq("+(colcount-1)+")").attr("style", "padding: 0px;padding-left: 15px;-webkit-box-shadow: inset 10px 0px 15px -10px rgba(0,0,0,0.05);box-shadow: inset 10px 0px 15px -10px rgba(0,0,0,0.05);vertical-align:top;background-color:#FCFDFE;color: #666;");

						back.find("td:eq("+(colcount-1)+")").children().show();

					}

					else {

						back.find("td:eq("+(colcount-1)+")").attr("style", "");

						back.find("td:eq("+(colcount-1)+")").children().hide();

					}

					back.css("height", "77px");

				});

			}

		}

	}

			

	function setupCustomer(idCustomer)

	{

		showResaBox(0);

		newOrder(idCustomer, 0, 'create');

	}

	

	function getSummary()

	{

		showResaBox(0);

		newOrder(id_customer, id_cart, 'validate');

	}

	

	

	

function myOwnSearchCustomers(url, hint)

{

	$.ajax({

		type:"POST",

		url : url,

		async: true,

		dataType: "json",

		data : {

			ajax: "1",

			tab: "AdminCustomers",

			action: "searchCustomers",

			customer_search: hint},

		success : function(res)

		{

			if(res.found)

			{

				var html = '';

				$.each(res.customers, function() {

					html += '<div class="customerCard col-lg-4">';

					html += '<div class="panel">';

					html += '<div class="panel-heading">'+this.firstname+' '+this.lastname;

					html += '<span class="pull-right">#'+this.id_customer+'</span></div>';

					html += '<span>'+this.email+'</span><br/>';

					html += '<span class="text-muted">'+((this.birthday != '0000-00-00') ? this.birthday : '')+'</span><br/>';

					html += '<div class="panel-footer">';

					html += '<a href="'+url+'&id_customer='+this.id_customer+'&viewcustomer&liteDisplaying=1" class="btn btn-default" target="_blank"><i class="icon-search"></i> '+detail_lalbel+'</a>';

					html += '<button type="button" onclick="newOrder('+this.id_customer+', 0, \'create\')" class="btn btn-default pull-right"><i class="icon-arrow-right"></i> '+choose_label+'</button>';

					html += '</div>';

					html += '</div>';

					html += '</div>';

				});

			}

			else

				html = '<div class="bootstrap"><div class="alert alert-warning">'+not_found_label+'</div></div>';

			$('#customers').html(html);

		}

	});

}

	function myOwnAvailabilityUpdate(id_availability, quantity)

	{
		console.log("A1");

		$.ajax({

			type:"POST",

			url: ajaxAdmin,

			async: true,

			dataType: "json",

			data : {

				ajax: "1",

				action: "updateAvailability",

				id_availability: id_availability,

				quantity: quantity

			},

			success : function(res)

			{

				//displaySummary(res);

				$.growl({ title: "myOwnReservations", message: availability_label });

				$("#editAvailability").hide();

				$("#editAvailability").prev().show();

				refreshAvailability();

			}

		});

	}

	

	function myOwnAvailabilityDelete(id_availability)

	{
        console.log("A2");
		$.ajax({

			type:"POST",

			url: ajaxAdmin,

			async: true,

			dataType: "json",

			data : {

				ajax: "1",

				action: "deleteAvailability",

				id_availability: id_availability

			},

			success : function(res)

			{

				//displaySummary(res);

				$.growl({ title: "myOwnReservations", message: availability_label });

				$("#editAvailability").hide();

				$("#editAvailability").prev().show();

				refreshAvailability();

			}

		});

	}

	

	function myOwnAvailabilityCreate(id_product, id_product_attribute, id_family, period, id_object, quantity)

	{
        console.log("A3");
		$.ajax({

			type:"POST",

			url: ajaxAdmin,

			async: true,

			dataType: "json",

			data : {

				ajax: "1",

				action: "createAvailability",

				id_product: id_product,

				id_product_attribute: id_product_attribute,

				id_family: id_family,

				period: period,

				id_object: id_object,

				quantity: quantity

			},

			success : function(res)

			{

				//displaySummary(res);

				$.growl({ title: "myOwnReservations", message: availability_label });

				$("#editAvailability").hide();

				$("#editAvailability").prev().show();

				refreshAvailability();

			}

		});

	}

	

function myOwnSearchProducts(hint, attr_id)

{

		$.ajax({

			type:"POST",

			url: ajaxAdmin,

			async: true,

			dataType: "json",

			data : {

				ajax: "1",

				action: "searchProducts",

				family: $('#scheduleFamily option:selected').val(),

				product_search: hint,

				id: (typeof(id)!='undefined' ? id : 0),

			},

			success : function(res)

			{

				var products_found = '';

				var attributes_html = '';

				var customization_html = '';

				stock = {};



				if(res.found)

				{

					/*if (!customization_errors)

						$('#products_err').addClass('hide');

					else

						customization_errors = false;*/

					$('#products_found').show();

					$('#products_err').hide();

					products_found += '<label class="control-label col-lg-3">'+prod_label+'</label><div class="col-lg-6"><select id="id_product" onchange="display_product_attributes();display_product_customizations();refreshAvailability();"></div>';

					attributes_html += '<label class="control-label col-lg-3">'+comb_label+'</label><div class="col-lg-6">';

					$.each(res.products, function() {

						products_found += '<option '+(this.combinations.length > 0 ? 'rel="'+this.qty_in_stock+'"' : '')+' value="'+this.id_product+'">'+this.name+(this.combinations.length == 0 ? ' - '+this.formatted_price : '')+'</option>';

						attributes_html += '<select class="id_product_attribute" id="ipa_'+this.id_product+'" style="display:none;">';

						var id_product = this.id_product;

						stock[id_product] = new Array();

						if (this.customizable == '1')

						{

							customization_html += '<div class="bootstrap"><div class="panel"><div class="panel-heading">Customization</div><form id="customization_'+id_product+'" class="id_customization" method="post" enctype="multipart/form-data" action="'+addcarturl+'" style="display:none;">';

							customization_html += '<input type="hidden" name="id_product" value="'+id_product+'" />';

							customization_html += '<input type="hidden" name="id_cart" value="'+id_cart+'" />';

							customization_html += '<input type="hidden" name="action" value="updateCustomizationFields" />';

							customization_html += '<input type="hidden" name="id_customer" value="'+id_customer+'" />';

							customization_html += '<input type="hidden" name="ajax" value="1" />';

							$.each(this.customization_fields, function() {

								class_customization_field = "";

								if (this.required == 1){ class_customization_field = 'required' };

								customization_html += '<div class="form-group"><label class="control-label col-lg-3 ' + class_customization_field + '" for="customization_'+id_product+'_'+this.id_customization_field+'">';

								customization_html += this.name+'</label><div class="col-lg-9">';

								if (this.type == 0)

									customization_html += '<input class="form-control customization_field" type="file" name="customization_'+id_product+'_'+this.id_customization_field+'" id="customization_'+id_product+'_'+this.id_customization_field+'">';

								else if (this.type == 1)

									customization_html += '<input class="form-control customization_field" type="text" name="customization_'+id_product+'_'+this.id_customization_field+'" id="customization_'+id_product+'_'+this.id_customization_field+'">';

								customization_html += '</div></div>';

							});

							customization_html += '</form></div></div>';

						}



						$.each(this.combinations, function() {

							attributes_html += '<option rel="'+this.qty_in_stock+'" '+(this.default_on == 1 ? 'selected="selected"' : '')+' value="'+this.id_product_attribute+'">'+this.attributes+' - '+this.formatted_price+'</option>';

							stock[id_product][this.id_product_attribute] = this.qty_in_stock;

						});



						stock[this.id_product][0] = this.stock[0];

						attributes_html += '</select>';

					});

					attributes_html += '</div>';

					products_found += '</select></div>';

					$('#products_found #product_list').html(products_found+'<div style="clear:both"></div>');

					$('#products_found #attributes_list').html(attributes_html+'<div style="clear:both"></div>');

					$('link[rel="stylesheet"]').each(function (i, element) {

						sheet = $(element).clone();

						$('#products_found #customization_list').contents().find('head').append(sheet);

					});

					$('#products_found #customization_list').contents().find('body').html(customization_html);

					display_product_attributes();

					display_product_customizations();

					display_product_quantity();

					$('#id_product').change();

					$('#addProduct').show();

					if (typeof(attr_id)!='undefined' && res.products.length==1) $(".id_product_attribute").val(attr_id);

					//$('#addQuantity').show();

				}

				else

				{

					$('#products_found').hide();

					$('#products_err').show();

					$('#products_err').html(not_found_label);

					$('#addProduct').hide();

					$(".addQuantity").hide();

				}

			}

		});

}



	function display_product_quantity()

	{

		$(".addQuantity").hide();

		$(".addQuantity").find('input').removeClass("qty_field");

		var product = $('#scheduleProduct  option:selected').val();

		var family = $('#scheduleFamily option:selected').val();

		if (typeof(family)=='undefined' && typeof(product)!='undefined' && typeof(productFamilly)!='undefined')

			family = productFamilly[product];

		

		if (family>0 && $("#addQuantityExtra"+family).length) {

			$("#addQuantityExtra"+family).show();

			$("#addQuantityExtra"+family).find('div').show();

			$("#addQuantityExtra"+family).find('input').addClass("qty_field");

		} else if (typeof(product)!='undefined' && product>0 && typeof(productFamilly)!='undefined') {

			family = productFamilly[product];

			if ($("#addQuantityExtra"+family).length) {

				$("#addQuantityExtra"+family).show();

				$("#addQuantityExtra"+family).find('div').show();

				$("#addQuantityExtra"+family).find('input').addClass("qty_field");

			}else 

			$('#addQuantity').show();

		} else 

			$('#addQuantity').show();

	}

	function display_product_customizations()

	{

		if ($('#products_found #customization_list').contents().find('#customization_'+$('#id_product option:selected').val()).children().length === 0)

			$('#customization_list').hide();

		else

		{

			$('#customization_list').show();

			$('#products_found #customization_list').contents().find('.id_customization').hide();

			$('#products_found #customization_list').contents().find('#customization_'+$('#id_product option:selected').val()).show();

			$('#products_found #customization_list').css('height',$('#products_found #customization_list').contents().find('#customization_'+$('#id_product option:selected').val()).height()+95+'px');

		}

	}



	function display_product_attributes()

	{

		if ($('#ipa_'+$('#id_product option:selected').val()+' option').length === 0)

			$('#attributes_list').hide();

		else

		{

			$('#attributes_list').show();

			$('.id_product_attribute').hide();

			$('#ipa_'+$('#id_product option:selected').val()).show();

		}

	}

	

	

	function myOwnGetSummary()

	{

		$.ajax({

			type:"POST",

			url: addcarturl,

			async: true,

			dataType: "json",

			data : {

				ajax: "1",

				action: "getSummary",

				id_cart: id_cart,

				id_customer: id_customer

				},

			success : function(res)

			{

				$('#products_err').hide();

				myOwnUpdateCartProducts(res.summary.products, res.summary.gift_products, res.cart.id_address_delivery);

			}

		});

	}

	

	function myOwnDeleteProduct(id_product, id_product_attribute, id_customization)

	{

		$.ajax({

			type:"POST",

			url: addcarturl,

			async: true,

			dataType: "json",

			data : {

				ajax: "1",

				action: "deleteProduct",

				id_product: id_product,

				id_product_attribute: id_product_attribute,

				id_customization: id_customization,

				id_cart: id_cart,

				id_customer: id_customer

				},

			success : function(res)

			{

				myOwnUpdateCartProducts(res.summary.products, res.summary.gift_products, res.cart.id_address_delivery);

				var errors = '';

				if (typeof(res.errors)!='undefined' && res.errors.length)

				{

					$.each(res.errors, function() {

						errors += this + '<br />';

					});

					$('#products_err').show();

				}

				else

					$('#products_err').hide();

				$('#products_err').html(errors);

			}

		});

	}

	

	function myOwnAddProduct()

	{

		var quantity=$("#qty").val();

		var family = $("#scheduleFamily").val();

		var id_product=$("#scheduleProduct").val();

		if (typeof(family)=='undefined' && typeof(id_product)!='undefined' && typeof(productFamilly)!='undefined')

			family = productFamilly[id_product];

			

		var id_object = $("#myownr_object").val();

		if (typeof(id_object)=='undefined') id_object=0;

		if ($("#products_found #addQuantityExtra"+family+" .qty_field").length) {

			var tmp_qty=0;

			var cat_qty="";

			$("#products_found #addQuantityExtra"+family+"  .qty_field").each(function(index) {

				if ($(this).val()!="")

					tmp_qty+=parseInt($(this).val());

			});

			quantity=tmp_qty;

			$("#products_found #addQuantityExtra"+family+"  .qty_field").each(function(index) {

				if ($(this).val()!="") {

					if (cat_qty != "")

						cat_qty += ",";

					cat_qty += $(this).val()+" "+ $(this).attr("key");

				}

			});

		}

		

		if ($("#scheduleProduct").length) {

			id_product=$("#scheduleProduct").val();

			var id_product_attribute=$("#scheduleProductAttribute").val();

		}

		else var id_product_attribute=$("#ipa_"+$("#id_product").val()+" option:selected").val();

		

		var ts_end = $("#scheduleEndTimeslot").val();

		if (ts_end==null) ts_end=0;

		var ts_start = $("#scheduleStartTimeslot").val();

		if (ts_start==null) ts_start=0;

		

		var resa = $("#scheduleStart").val()+"_"+ts_start+"@"+$("#scheduleEnd").val()+"_"+ts_end;

		myOwnUpdateQty(id_product, id_product_attribute, 0, quantity, resa, ($(".qty_field").length ? cat_qty : ""), id_object);

	}

	



	

	function myOwnUpdateQty(id_product, id_product_attribute, id_customization, qty, resa, extra_qty, id_object)

	{

		$.ajax({

			type:"POST",

			url: addcarturl,

			async: true,

			dataType: "json",

			data : {

				ajax: "1",

				action: "updateQty",

				id_product: id_product,

				id_product_attribute: id_product_attribute,

				id_customization: id_customization,

				place: (typeof(id_object)=='undefined' ? 0 : id_object),

				qty: qty,

				id_customer: id_customer,

				id_cart: id_cart,

				reservations: resa,

				qty_field: (typeof(extra_qty)=='undefined' ? "" : extra_qty)

			},

			success : function(res)

			{

				//displaySummary(res);

				id_cart = res.cart.id; 

				if (id_cart) 

					$("#badge_id_cart").show().html("N°"+id_cart);



				myOwnUpdateCartProducts(res.summary.products, res.summary.gift_products, res.cart.id_address_delivery);

				var errors = '';

				if (res.errors.length)

				{

					$.each(res.errors, function() {

						errors += this + '<br />';

					});

					$('#products_err').show();

				}

				else

					$('#products_err').hide();

				$('#products_err').html(errors);

			}

		});

	}

	

	function myOwnUpdateProductPrice(id_product, id_product_attribute, new_price)

	{

		$.ajax({

			type:"POST",

			url: addcarturl,

			async: true,

			dataType: "json",

			data : {

				ajax: "1",

				action: "updateProductPrice",

				id_cart: id_cart,

				id_product: id_product,

				id_product_attribute: id_product_attribute,

				id_customer: 0,

				price: new Number(new_price.replace(",",".")).toFixed(4).toString()

				},

			success : function(res)

			{

				myOwnUpdateCartProducts(res.summary.products, res.summary.gift_products, res.cart.id_address_delivery);

			}

		});

	}

	

	function myOwnUpdateDeliveryOption()

	{

		$.ajax({

			type:"POST",

			url: url,

			async: true,

			dataType: "json",

			data : {

				ajax: "1",

				action: "updateDeliveryOption",

				delivery_option: $('#delivery_option option:selected').val(),

				gift: $('#order_gift').is(':checked')?1:0,

				gift_message: $('#gift_message').val(),

				recyclable: $('#carrier_recycled_package').is(':checked')?1:0,

				id_customer: id_customer,

				id_cart: id_cart

				},

			success : function(res)

			{

				myOwnDisplaySummary(res);

			}

		});

	}

	function updateCurrency()

	{

		$.ajax({

			type:"POST",

			url: "{$link->getAdminLink('AdminCarts')|addslashes}",

			async: true,

			dataType: "json",

			data : {

				ajax: "1",

				token: "{getAdminToken tab='AdminCarts'}",

				tab: "AdminCarts",

				action: "updateCurrency",

				id_currency: $('#id_currency option:selected').val(),

				id_customer: id_customer,

				id_cart: id_cart

				},

			success : function(res)

			{

					displaySummary(res);

			}

		});

	}



	function updateLang()

	{

		$.ajax({

			type:"POST",

			url: "{$link->getAdminLink('AdminCarts')|addslashes}",

			async: true,

			dataType: "json",

			data : {

				ajax: "1",

				token: "{getAdminToken tab='AdminCarts'}",

				tab: "admincarts",

				action: "updateLang",

				id_lang: $('#id_lang option:selected').val(),

				id_customer: id_customer,

				id_cart: id_cart

				},

			success : function(res)

			{

					displaySummary(res);

			}

		});

	}

	function myOwnDisplaySummary(jsonSummary)

	{

		currency_format = jsonSummary.currency.format;

		currency_sign = jsonSummary.currency.sign;

		currency_blank = jsonSummary.currency.blank;

		priceDisplayPrecision = jsonSummary.currency.decimals ? 2 : 0;



		//updateCartProducts(jsonSummary.summary.products, jsonSummary.summary.gift_products, jsonSummary.cart.id_address_delivery);

		//updateCartVouchers(jsonSummary.summary.discounts);

		//updateAddressesList(jsonSummary.addresses, jsonSummary.cart.id_address_delivery, jsonSummary.cart.id_address_invoice);



		/*if (!jsonSummary.summary.products.length || !jsonSummary.addresses.length || !jsonSummary.delivery_option_list)

			$('#carriers_part,#summary_part').hide();

		else

			$('#carriers_part,#summary_part').show();



		updateDeliveryOptionList(jsonSummary.delivery_option_list);



		if (jsonSummary.cart.gift == 1)

			$('#order_gift').attr('checked', true);

		else

			$('#carrier_gift').removeAttr('checked');

		if (jsonSummary.cart.recyclable == 1)

			$('#carrier_recycled_package').attr('checked', true);

		else

			$('#carrier_recycled_package').removeAttr('checked');

		if (jsonSummary.free_shipping == 1)

			$('#free_shipping').attr('checked', true);

		else

			$('#free_shipping_off').attr('checked', true);



		$('#gift_message').html(jsonSummary.cart.gift_message);

		*/

		/*if (!changed_shipping_price)

			$('#shipping_price').html('<b>' + formatCurrency(parseFloat(jsonSummary.summary.total_shipping), currency_format, currency_sign, currency_blank) + '</b>');*/

		//shipping_price_selected_carrier = jsonSummary.summary.total_shipping;



		$('#total_vouchers').html(formatCurrency(parseFloat(jsonSummary.summary.total_discounts_tax_exc), currency_format, currency_sign, currency_blank));

		$('#total_shipping').html(formatCurrency(parseFloat(jsonSummary.summary.total_shipping_tax_exc), currency_format, currency_sign, currency_blank));

		$('#total_taxes').html(formatCurrency(parseFloat(jsonSummary.summary.total_tax), currency_format, currency_sign, currency_blank));

		$('#total_without_taxes').html(formatCurrency(parseFloat(jsonSummary.summary.total_price_without_tax), currency_format, currency_sign, currency_blank));

		$('#total_with_taxes').html(formatCurrency(parseFloat(jsonSummary.summary.total_price), currency_format, currency_sign, currency_blank));

		$('#total_products').html(formatCurrency(parseFloat(jsonSummary.summary.total_products), currency_format, currency_sign, currency_blank));

		id_currency = jsonSummary.cart.id_currency;

		$('#id_currency option').removeAttr('selected');

		$('#id_currency option[value="'+id_currency+'"]').attr('selected', true);

		id_lang = jsonSummary.cart.id_lang;

		$('#id_lang option').removeAttr('selected');

		$('#id_lang option[value="'+id_lang+'"]').attr('selected', true);

		$('#send_email_to_customer').attr('rel', jsonSummary.link_order);

		$('#go_order_process').attr('href', jsonSummary.link_order);

		$('#order_message').val(jsonSummary.order_message);

	}

	

	function myOwnUpdateCartProducts(products, gifts, cart_id_address_delivery)

	{

		var cart_content = '';

		$.each(products, function() {

			var product = this;

			var id_product = Number(this.id_product);

			var id_product_attribute = Number(this.id_product_attribute);

			var id_resa = Number(this.id_resa);

			if (cart_id_address_delivery==null) id_address_delivery = 0;

			else id_address_delivery= cart_id_address_delivery;

			cart_quantity[Number(this.id_product)+'_'+Number(this.id_product_attribute)+'_'+Number(this.id_customization)] = this.cart_quantity;

			cart_custom='';

			if (this.customized_datas!=null && typeof(this.customized_datas[id_product])!='undefined' && typeof(this.customized_datas[id_product][id_product_attribute])!='undefined') {

				$.each(this.customized_datas[id_product][id_product_attribute][id_address_delivery], function( index ) {

					if (-id_resa == index || id_resa == 0) {

						var customized_desc = '';

						if (this.datas[1].length)

						{

							$.each(this.datas[1],function() {

								customized_desc += this.name + ': ' + this.value + '<br />';

								id_customization = this.id_customization;

							});

							

						}

						if (this.datas[0] && this.datas[0].length)

						{

							$.each(this.datas[0],function() {

								customized_desc += this.name + ': <img src="' + pic_dir + this.value + '_small" /><br />';

								id_customization = this.id_customization;

							});

						}

				

						cart_custom += '<tr><td colspan="5">'+customized_desc+'</td></tr>';

					}

				});

			}

			cart_content += '<tr><td><img width="30px" height="30px" src="'+this.image_link+'" title="'+this.name+'" /></td><td>'+this.name+'<br />'+this.attributes_small+'</td><td><input style="padding: 3px 4px;" type="text" rel="'+this.id_product+'_'+this.id_product_attribute+'" class="product_unit_price" value="' + this.numeric_price + '" /></td><td>';

			

			if ($('<table>'+cart_custom+'</table>').find("#qty_field_"+product.id_resa).length)

				cart_content += $('<table>'+cart_custom+'</table>').find("#qty_field_"+product.id_resa).html();

			/*if ($("#addQuantityExtra"+this.mainProduct).length) {

				cart_content += $("#addQuantityExtra"+this.mainProduct).find('div').html();*/

			else {

				cart_content += '<div class="qty-group input-group fixed-width-sm" style="width:90px"><div class="input-group-btn"><a href="#" style="width: 20px;padding: 4px 3px;" class="btn btn-default increaseqty_product" rel="'+this.id_product+'_'+this.id_product_attribute+'_-'+product.id_resa+'" ><i class="icon-caret-up"></i></a><a href="#" style="width: 20px;padding: 4px 3px;" class="btn btn-default decreaseqty_product" rel="'+this.id_product+'_'+this.id_product_attribute+'_-'+product.id_resa+'"><i class="icon-caret-down"></i></a></div>';

				cart_content += '<input  style="padding: 3px 4px;" type="text" rel="'+this.id_product+'_'+this.id_product_attribute+'_'+0+'" class="cart_quantity" value="'+this.cart_quantity+'" />';

				cart_content += '<div class="input-group-btn"><a href="#" style="width: 20px;padding: 4px 3px;" class="delete_product btn btn-default" rel="delete_'+this.id_product+'_'+this.id_product_attribute+'_-'+product.id_resa+'" ><i class="icon-remove text-danger"></i></a></div></div>';

			}

			cart_content += '</td><td>' + formatCurrency(this.numeric_total, currency_format, currency_sign, currency_blank) + '</td></tr>';

			

			//if (this.id_customization && this.id_customization != 0)

			//{

			cart_content += cart_custom;

			//}

		});

		if (products.length) $('#add_products').removeClass("in");

		$('#customer_cart tbody').html(cart_content);

		attachButtons();

	}





// ----------------------------------- buttons -----------------------------------

function submitDatePickerStock() {

	$("#submitDatePicker").click();

}

function submitProdFamilyStock() {

	$("#productFamillyFilterStock").val($("#productFamillyFilter").val());

	$('#stockform').submit();

}



function submitMyOwnForm(stay, field, id) {

	var curMyOwnForm=$('form[name="edit'+field+'Form"]');

	if (stay) {

		curMyOwnForm.attr('action',curMyOwnForm.attr('action')+'&edit'+field+'='+(typeof(id)!='undefined' && id>0 ? id : 0));

		curMyOwnForm.submit();

	} else {

		curMyOwnForm.attr('action',curMyOwnForm.attr('action'));

		curMyOwnForm.submit();

	}

}

function submitDatePickerPlanning(date) {

	var datesplit = date.split('-');

	currentViewDate = new Date(datesplit[0], (datesplit[1]-1), datesplit[2]);

	scheduler.setCurrentView(currentViewDate, currentViewMode);

}

function toogleHelp(force) {

	var lastname="";

	$(".tooltip").each(function( index ) {

		if ($(this).is(":visible")) { //lastname!=$(this).attr("name") && 

		   lastname=$(this).attr("name");

	       if (typeof(force)!='undefined') $(this).tooltipster("hide");

		   else $(this).tooltipster("show");

	    }

	});

}

function tabSel(panel) {

	$(".tab-pane").hide();

	toogleHelp(true);

	$("#"+panel).show();

	//1.5

	$(".tab-page").removeClass("selected");

	$("#"+panel+"-pane").addClass("selected");

	//1.6

	$(".list-group-item").removeClass("active");

	$("#link-"+panel).addClass("active");

	$("#key_tab").val(panel);

	if (window.location.href.indexOf("#")===-1)

		window.location.href = window.location.href + "#key_tab=" + panel;

	else {

		var tab = window.location.href.split("#");

		window.location.href = tab[0] + "#key_tab=" + panel;

	}

}

function setStyle(objName, style)

{   

	$('td[name="'+objName+'"]').each(function(index) {

	    $(this).toggleClass(style);

	});

}

function padStr(i) {

    return (i < 10) ? "0" + i : "" + i;

}

function showNewResa() {

	if (typeof(scheduler)!='undefined') {

		var newperiod = myOwnConvertDate(currentViewDate);

	} else {

		var pperiod = new Date()

		newperiod= padStr(pperiod.getFullYear())+"-"+padStr(1 + pperiod.getMonth())+"-"+padStr(pperiod.getDate());

	}

	showResaBox(0);

	showResaDetails(0, ''); //newperiod+'#'+newperiod, '')

}



// ----------------------------------- reservations -----------------------------------



function showResaDetails(resa_id, params, product, stock) {

	if (("" + resa_id).indexOf('#') > -1) {

		resa_id = resa_id.split('#')[0];

	}

	if (typeof(params)=='undefined') var params = getFormParams();

	if (typeof(product)=='undefined') var product='';

	if (typeof(stock)=='undefined') var stock='';

	if (typeof(currentProductFamily)!='undefined') productFamilyFilter = currentProductFamily;

	if (typeof(productFamilyFilter)=='undefined') var productFamilyFilter=0;

	if (typeof(objectFilter)!='undefined') idObjectFilter = objectFilter;

	if (typeof(idObjectFilter)=='undefined') var idObjectFilter=0;

    //var url = baseDir + 'modules/myownreservations/controllers/ajax_admin.php';

	$('#myOwnReservationLoading').css("display","");

	$('#myOwnReservationContent').css("display","none");

	$('#myOwnReservationContent').load(ajaxAdmin, {

           resa_id: resa_id, 

           reschedule: params,

           product: product,

           stock: stock,

           productFamilyFilter: productFamilyFilter,

           objectFilter: idObjectFilter,

       }, function() {

	    $('#myOwnReservationLoading').css("display","none");

	    $('#myOwnReservationContent').css("display","");

	});

    return false;

}



function editForm(resa_id, del) {

	if (del!=undefined) var quantity = 0;

	else {

		var quantity = $("#quantity").val();

		if (typeof(cat_qty_total)!='undefined')

			quantity = cat_qty_total;

	}

	$("#actionWaiting").show();

	$(".pageTitle h3").hide();

	$(".page-title h2").hide();

	$(".page-head h2").hide();

	$("div.reservationContent div.page-bar").hide();

    //var url = baseDir + 'modules/myownreservations/controllers/ajax_admin.php';

	//$('#myOwnReservationLoading').css("display","");

	//$('#myOwnReservationContent').css("display","none");

	params = {

           resa_id: (typeof(resa_id)!='undefined' ? resa_id : 0), 

           edit : true,

           quantity: quantity,

           reschedule: (del ? '' : getFormParams()),

           validationStatus: $("#validationStatus").val(),

           assignmentEmployee: $("#assignmentEmployee").val(),

           object: $("#myownr_object").val(),

           customer: $("#customer").val(),

           carrier: $("#carrier").val(),

           payment: $("#payment").val(),

           advance: $("#advance").val(),

           comment: $("#comment").val(),

           qty_field: (typeof(cat_qty)!='undefined' ? cat_qty : '')

       };

    $(".stock_product").each(function( index ) {

    	params[$(this).attr("name")] = $(this).val();

	});

	$(".itemSelect:checked").each(function( index ) {

    	params[$(this).attr("name")] = $(this).val();

	});

	if ($("#editProductsDetails").is(":visible")) {

		var data_attendees = $('.attendees_form').serializeArray();

		for (i = 0; i < data_attendees.length; i++) {  

		    params[data_attendees[i]['name']] = data_attendees[i]['value'];

		}

		params['saveDetails']=true;

	}

	$('#myOwnReservationContent').load(ajaxAdmin, params, function() {

	    //$('#myOwnReservationLoading').css("display","none");

		$("#reservationContent .nav-pills").show();

		$(".myown-btn-edit").show();

	    //$('#myOwnReservationContent').css("display","");

	});

    return false;

}



function actionFactorised(resa_ids, action, param1, param2) {

	$("#actionWaiting").show();

	$(".pageTitle h3").hide();

	$(".page-title h2").hide();

	$(".page-head h2").hide();

	$("div.reservationContent div.page-bar").hide();

    //var url = baseDir + 'modules/myownreservations/controllers/ajax_admin.php';

	//$('#myOwnReservationLoading').css("display","");

	//$('#myOwnReservationContent').css("display","none");

	params = {

           resa_id: resa_ids, 

           param0: action,

           param1: param1,

           param2: param2,

           edit : true

       };

	$('#myOwnReservationContent').load(ajaxAdmin, params, function() {

	    //$('#myOwnReservationLoading').css("display","none");

		$("#reservationContent .nav-pills").show();

		$(".myown-btn-edit").show();

	    //$('#myOwnReservationContent').css("display","");

	});

    return false;

}



function editFactorised(resa_ids) {

	$("#actionWaiting").show();

	$(".pageTitle h3").hide();

	$(".page-title h2").hide();

	$(".page-head h2").hide();

	$("div.reservationContent div.page-bar").hide();

    //var url = baseDir + 'modules/myownreservations/controllers/ajax_admin.php';

	//$('#myOwnReservationLoading').css("display","");

	//$('#myOwnReservationContent').css("display","none");

	params = {

           resa_id: resa_ids, 

           edit : true

       };

    $(".stock_product").each(function( index ) {

    	params[$(this).attr("name")] = $(this).val();

	});

	$(".assignmentEmployee").each(function( index ) {

    	params[$(this).attr("name")] = $(this).val();

	});

	$('#myOwnReservationContent').load(ajaxAdmin, params, function() {

	    //$('#myOwnReservationLoading').css("display","none");

		$("#reservationContent .nav-pills").show();

		$(".myown-btn-edit").show();

	    //$('#myOwnReservationContent').css("display","");

	});

    return false;

}



function newForm(resa_id) {

	if ($("#orderId").val()==-1) {

		alert(customerErrorMsg);

		return false;

	}

	var orderId=0;

	if (ordertypeval>0) orderId = $("#orderId").val();

	if (parseInt($("#quantity").val())==0) {

		$("#reservationResult").html('<div id="content"><div class="error">'+qtyErrorMsg+'</div></div>');

		return false;

	}

    //var url = baseDir + 'modules/myownreservations/controllers/ajax_admin.php';

	$('#myOwnReservationLoading').css("display","");

	$('#myOwnReservationContent').css("display","none");

	$('#myOwnReservationContent').load(ajaxAdmin, {

		   resa_id: (typeof(resa_id)!='undefined' ? resa_id : 0), 

		   id_order : orderId,

           id_product : $("#rescheduleProduct").val(),

           id_product_attribute : $("#rescheduleProductAttribute").val(),  

           quantity: $("#quantity").val(),

           period: getFormParams(),

           validationStatus: $("#validationStatus").val(),

           customer: $("#customer").val(),

           carrier: $("#carrier").val(),

           payment: $("#payment").val(),

           advance: $("#advance").val(),

           comment: $("#comment").val()

       }, function() {

	    $('#myOwnReservationLoading').css("display","none");

	    $('#myOwnReservationContent').css("display","");

	});

    return false;

}



function sendMailToCustomer(link)
{
	$.ajax({
		type:"POST",
		url: link,
		async: true,
		dataType: "json",
		data : {
			ajax: "1",
			tab: "AdminOrders",
			action: "sendMailValidateOrder",
			id_customer: id_customer,
			id_cart: id_cart
			},
		success : function(res)
		{
			if (res.errors)
				$('#reservationResult').removeClass('hide').removeClass('alert-success').addClass('alert-danger');
			else
				$('#reservationResult').removeClass('hide').removeClass('alert-danger').addClass('alert-success');
			$('#reservationResult').html(res.result);
		}
	});
}



function newOrder(customer_id, cart_id, step) {

	if (typeof(step)=='undefined')	var step='create';

	if (typeof(cart_id)=='undefined')	var cart_id=0;

    //var url = baseDir + 'modules/myownreservations/controllers/ajax_admin.php';

    if (customer_id && $("#scheduleProduct").length && !cart_id) {

	    alert(noproductErrorMsg);

	    return false;

    }

    data = {};

    if (step=='validate') {

    	var data_attendees = $('.attendees_form').serializeArray();

		for (i = 0; i < data_attendees.length; i++) {  

		    data[data_attendees[i]['name']] = data_attendees[i]['value'];

		}

    }

    data['resa_id']=0;

	data['create']=(customer_id ? true : '');

	data['validate']=(step=='validate' ? true : '');

	data['id_order']= 0;

	data['id_cart']=cart_id;

    data['customer']=customer_id;

    data['rescheduleStart']=$("#rescheduleStart").val();

    data['rescheduleStartTimeslot']=$("#rescheduleStartTimeslot").val();

    data['rescheduleEnd']=$("#rescheduleEnd").val();

    data['rescheduleEndTimeslot']=$("#rescheduleEndTimeslot").val();

    data['rescheduleProduct']=$("#rescheduleProduct").val();

    data['rescheduleProductAttribute']=$("#rescheduleProductAttribute").val();



    //attendees_data= attendees_data.split("%23").join("#");

	$('#myOwnReservationLoading').css("display","");

	$('#myOwnReservationContent').css("display","none");

	$('#myOwnReservationContent').load(ajaxAdmin, data, function() {

	    $('#myOwnReservationLoading').css("display","none");

	    $('#myOwnReservationContent').css("display","");

	    if (typeof(data['rescheduleStartTimeslot'])!='undefined') $('#scheduleStartTimeslot').val(data['rescheduleStartTimeslot']);

	    if (typeof(data['rescheduleEndTimeslot'])!='undefined') $('#scheduleEndTimeslot').val(data['rescheduleStartTimeslot']);

	});

    return false;

}



function getFormParams() {

	if ($("#rescheduleStart").length == 0 && $("#scheduleStart").length == 0) return '';

	var startTimeslot = 0;

	var endTimeslot = 0;

	var startdate = '';

	var enddate = '';

	if ($("#rescheduleStartTimeslot").length > 0 && $("#rescheduleStartTimeslot").val()!=null)

		startTimeslot = $("#rescheduleStartTimeslot").val();

	else if ($("#scheduleStartTimeslot").length > 0 && $("#scheduleStartTimeslot").val()!=null)

		startTimeslot = $("#scheduleStartTimeslot").val();

	if ($("#rescheduleEndTimeslot").length > 0 && $("#rescheduleEndTimeslot").val()!=null)

		endTimeslot = $("#rescheduleEndTimeslot").val();

	else if ($("#scheduleEndTimeslot").length > 0 && $("#scheduleEndTimeslot").val()!=null)

		endTimeslot = $("#scheduleEndTimeslot").val();

	if ($("#rescheduleStart").length > 0 && $("#rescheduleStart").val()!=null)

		startdate = $("#rescheduleStart").val();

	else if ($("#scheduleStart").length > 0 && $("#scheduleStart").val()!=null)

		startdate = $("#scheduleStart").val();

	if ($("#rescheduleEnd").length > 0 && $("#rescheduleEnd").val()!=null)

		enddate = $("#rescheduleEnd").val();

	else if ($("#scheduleEnd").length > 0 && $("#scheduleEnd").val()!=null)

		enddate = $("#scheduleEnd").val();

	return startdate+"_"+startTimeslot+"#"+enddate+"_"+endTimeslot;

}



function submitReschedule(resa_id, startDate, startTimeslot, endDate, endTimeslot, quantity, validated, comment, stock, qty_field, selectedItems) {

	//var url = baseDir + 'modules/myownreservations/controllers/ajax_admin.php';

    var data = 'resa_id=' + resa_id + '&action=reschedule&startDate=' + startDate + '&startTimeslot=' + startTimeslot + '&endDate=' + endDate + '&endTimeslot=' + endTimeslot + '&quantity=' + quantity + '&validated=' + validated + '&comment=' + comment + '&stock=' + stock + '&qty_field=' + qty_field + '&selectedItems=' + selectedItems;

    if ($().jquery<'1.6.0') {

	    if ($('#cb_notify').length && $('#cb_notify').attr('checked')) data +='&notify=1';

	    if ($('#cb_refresh').attr('checked')) data +='&refreshOrder=1';

	    if ($('#cb_status').attr('checked')) data +='&updateStatus='+$('#orderStatus').val();

	    if ($('#cb_invoice').attr('checked')) data +='&updateInvoice='+$('#invoiceDoc').val();

    } else {

	    if ($('#cb_notify').length && $('#cb_notify').prop('checked')) data +='&notify=1';

	    if ($('#cb_refresh').prop('checked')) data +='&refreshOrder=1';

	    if ($('#cb_status').prop('checked')) data +='&updateStatus='+$('#orderStatus').val();

	    if ($('#cb_invoice').prop('checked')) data +='&updateInvoice='+$('#invoiceDoc').val();

	}

    $("#actionWaiting").show();

    $("#adminbox_cancel").attr("disabled", "disabled");

    $("#adminbox_submit").attr("disabled", "disabled");

    $("#reservationActions").hide();

    $(".cc_button").hide();

	$.ajax({

		type: 'POST',

		headers: { "cache-control": "no-cache" },

		url: ajaxAdmin,

		async: true,

		cache: false,

		dataType : "json",

		data: data,

		success: function(jsonData)

		{

			$("#actionWaiting").hide();

			$("#reservationContent .nav-pills").show();

			$(".cc_button").show();

			$("#adminbox_cancel").removeAttr("disabled");

			$("#adminbox_submit").removeAttr("disabled");  

			if (jsonData.resultBool) {

				$("#reservationResult").html('<div id="content" style="background:none;margin-left:0px;">'+jsonData.resultLabel+'</div>');

				if (typeof(unmodifiedStartDate)!='undefinded') unmodifiedStartDate = new Date(jsonData.resultStart*1000);

				if (typeof(unmodifiedEndDate)!='undefinded') unmodifiedEndDate = new Date(jsonData.resultEnd*1000);

			} else {

				$("#reservationResult").html('<div id="content" style="background:none;margin-left:0px;>'+jsonData.resultLabel+'</div>');

			}

			$(".myown-btn-edit").show();

		},

		error: function(jsonData)

		{

			$("#reservationResult").html('<div id="content" style="background:none;margin-left:0px;"><div class="error">'+queryError+'</div></div>');

			$("#actionWaiting").hide();

			$("#reservationContent .nav-pills").show();

			$(".myown-btn-edit").show();

		}

	});

	return true;

}



function submitNew(temp_id, action) {

	var url = ajaxAdmin + '&action='+action;

	if ($().jquery<'1.6.0') {

		if ($('#cb_notify').attr('checked')) url +='&notify=1';

	    if ($('#cb_refresh').attr('checked')) url +='&refreshOrder=1';

	    if ($('#cb_invoice').attr('checked')) url +='&updateInvoice='+$('#invoiceDoc').val();

	} else {

	    if ($('#cb_notify').prop('checked')) url +='&notify=1';

	    if ($('#cb_refresh').prop('checked')) url +='&refreshOrder=1';

	    if ($('#cb_invoice').prop('checked')) url +='&updateInvoice='+$('#invoiceDoc').val();

    }

    $("#actionWaiting").show();

    $("#adminbox_cancel").attr("disabled", "disabled");

    $("#adminbox_submit").attr("disabled", "disabled");

    $("#reservationActions").hide();

    $(".cc_button").hide();

	$.ajax({

		type: 'POST',

		headers: { "cache-control": "no-cache" },

		url: url,

		async: true,

		cache: false,

		dataType : "json",

		data: resaJSON,

		success: function(jsonData)

		{

			$("#actionWaiting").hide();

			$("#reservationContent .nav-pills").show();

			$(".cc_button").show();

			$("#adminbox_cancel").removeAttr("disabled");

			$("#adminbox_submit").removeAttr("disabled");  

			if (jsonData.resultBool) {

				$("#reservationResult").html('<div id="content" style="background:none;margin-left:0px;">'+jsonData.resultLabel+'</div>');

				scheduler.deleteEvent(temp_id);

			    scheduler.addEvent(new Date(jsonData.resultObj.start_timestamp), new Date(jsonData.resultObj.end_date),"",jsonData.resultObj.id, jsonData.resultObj);

			} else {

				$("#reservationResult").html('<div id="content" style="background:none;margin-left:0px;">'+jsonData.resultLabel+'</div>');

			}

			$(".myown-btn-edit").show();

		},

		error: function(jsonData)

		{

			$("#reservationResult").html('<div id="content" style="background:none;margin-left:0px;"><div class="error">'+queryError+'</div></div>');

			$("#actionWaiting").hide();

			$("#reservationContent .nav-pills").show();

			$(".myown-btn-edit").show();

		}

	});

	return true;

}



function submitDelete(resa_id, status, statusVal, invoiceVal, selectedItems) {

	var url = ajaxAdmin + '&action=delete&resa_id='+resa_id + '&selectedItems=' + selectedItems;

	if ($().jquery<'1.6.0') {

		if ($('#cb_status').attr('checked')) url +='&updateStatus='+$('#orderStatus').val();

		if ($('#cb_refresh').attr('checked')) url +='&refreshOrder=1';

		if ($('#cb_invoice').attr('checked')) url +='&updateInvoice='+$('#invoiceDoc').val();

	} else {

		if ($('#cb_status').prop('checked')) url +='&updateStatus='+$('#orderStatus').val();

		if ($('#cb_refresh').prop('checked')) url +='&refreshOrder=1';

		if ($('#cb_invoice').prop('checked')) url +='&updateInvoice='+$('#invoiceDoc').val();

	}

    $("#actionWaiting").show();

    $("#adminbox_cancel").attr("disabled", "disabled");

    $("#adminbox_submit").attr("disabled", "disabled");

    $("#reservationActions").hide();

    $(".cc_button").hide();

	$.ajax({

		type: 'POST',

		headers: { "cache-control": "no-cache" },

		url: url,

		async: true,

		cache: false,

		dataType : "json",

		success: function(jsonData)

		{

			$("#actionWaiting").hide();

			$("#reservationContent .nav-pills").show();

			$(".cc_button").show();

			$("#adminbox_cancel").removeAttr("disabled");

			$("#adminbox_submit").removeAttr("disabled");  

			if (jsonData.resultBool) {

				$("#reservationResult").html('<div id="content" style="background:none;margin-left:0px;"><div class="conf">'+jsonData.resultLabel+'</div></div>');

				if (typeof(scheduler)!='undefined') scheduler.deleteEvent(resa_id);

			} else {

				$("#reservationResult").html('<div id="content" style="background:none;margin-left:0px;"><div class="error">'+jsonData.resultLabel+'</div></div>');

			}

			$(".myown-btn-edit").show();

		},

		error: function(jsonData)

		{

			$("#reservationResult").html('<div id="content" style="background:none;margin-left:0px;"><div class="error">'+queryError+'</div></div>');

			$("#actionWaiting").hide();

			$("#reservationContent .nav-pills").show();

			$(".myown-btn-edit").show();

		}

	});

	return true;

}



function clean_html(val) {

	var print = $('<b>'+val+'</b>').find('.labelprint');

	if (print!=null && print.length) return print.html();

	return val.replace(newline_regexp, "\n").replace(html_regexp, "");

}



function renderElem(start, end, event, bar) {

	var isTimeslot = (start.getMonth()==end.getMonth() && start.getFullYear()==end.getFullYear() && start.getDate()==end.getDate());

	var style=""; var product=""; var header=""; var order="";

	if (scheduler._mode=="timeline") 

		style = "flat";

	if (scheduler._mode=="month" && isTimeslot) 

		style = "mini";

	if (scheduler._mode=="week" && !isTimeslot)

		style = "width";

	if (scheduler._mode=="month" && !isTimeslot)

		style = "width";

	if (scheduler._mode=="week" && !bar) 

		style = "height";



	var print  = "<span class='labelprint' style='display:none'>"+(isTimeslot ? myOwnConvertTime(start)+"-"+myOwnConvertTime(end) : myOwnConvert(start)+"-"+myOwnConvert(end))+" "+event.product+" #"+event.id_order+"</span>";

	

	//Header

	if (style=="mini") header = '<div class="mini_time"><b>'+myOwnConvertTime( start )+'<br/>'+myOwnConvertTime( end )+'</b></div>';

	if (style=="flat") header = '';

	//if (style=="width") header = '<div class="time_label"><b>'+myOwnConvert( start )+'<br/>'+myOwnConvert( end )+'</b></div>';



	//Product

	if (event.product!=undefined) {

		if (style=="flat") product = '';

		if (style=="mini") product ='<div class="mini_label"><b>'+event.product+'</b></div>';

		if ((style=="width" && event.startDate!=event.endDate) || style=="height") product ='<div class="product_label" style="'+(event.image!=undefined ? 'padding-left:38px;background-image:url('+event.image+')' : '')+'"><div style="float:left"><b>'+event.product+'</b>'+(event.attribute!=undefined ? event.attribute : '')+'</div><div style="clear:both"></div></div>';

		if (style=="width" && event.startDate==event.endDate) product ='<div class="product_label" style=""><div style="float:left"><b>'+event.product+'</b>'+event.attribute+'</div><div style="clear:both"></div></div>';

	}

	

	//Order

	order = '';

	//case of factorized by product

	if (parseInt(event.ref_order)>0) {

		if (style=="flat") order += '<span class="badge" style="">'+translations["label_order_title_short"]+event.id_order+'</span> ';

		else order += '<i class="icon-ticket order_icon"></i> <span class="badge" style="">'+event.quantity+'</span> <i class="icon-credit-card order_icon"></i> <span class="badge" style="">'+event.ref_order+'</span>'+(style=="mini" ? '' : '<br/>&nbsp;');

	} else if (event.id_order) {

		if (style=="flat") order += '<span class="badge" style="background-color:'+event.status_color+'">'+translations["label_order_title_short"]+event.id_order+'</span> '+event.customer+'';

		else order += '<i class="icon-credit-card order_icon"></i> <span class="badge" style="background-color:'+event.status_color+'">'+event.ref_order+'</span>'+(style=="mini" ? '' : '<br/>&nbsp;'+event.customer); //order = '<b>'+translations["label_order_title"]+event.id_order+'</b> ('+event.customer+')';

		if (style=="heightt")

			order += '<div class="status_label" style="background-color:'+event.status_color+'"><span>'+event.status_text+'</span></div>';

	} else if(event.customer!=undefined) {

		if (event.id_source) order = translations["label_order_source_"+event.id_source];

		else if (style=="flat") order = translations["label_order0_short"];

		else order = translations["label_order0"];

		if (event.customer!='') order+= ' ('+event.customer+')';

	}

	if (style=="mini") order = '<div class="mini_label2">'+order+'</div>';

	else order = '<div class="order_label">'+order+'</div>';

	



	// header+product+'<div class="header_label">'+order+'</div><div style="clear:both"></div>';



	if (style=="width" || style=="flat") return '<div class="width_item">'+header+product+order+print+'</div>';

	if (style=="mini") return '<div style="overflow:hidden" class="mini_panel"><div>'+header+product+order+print+'</div></div>';

	else return header+product+order+print;

}



function filterChange(val) {

	stockFilter='';

	//day view

	$("#unit_button").hide();

	if (typeof(unitsObject)!='undefined' && unitsObject=='stock' && typeof(units)!='undefined' && val != 0 && units[val]!=undefined && units[val].length<16) {

		scheduler.createUnitsView({

		    name:"unit",

		    property:"stock", //the mapped data property

		    list:units[val]

		});

		$("#unit_button").show();

	}

	else if (scheduler._mode == 'unit') planningMode('month');

	

	//$(".object_filter").css('display', 'none');

	if ( typeof(unitsIds)!='undefined' && unitsIds[val]!=undefined ) {

		//$("#object_filter_"+unitsIds[val]).css('display', 'inline-block');

	} else {

		//$("#object_filter_"+unitsIds[val]).val(0);

	}

	oldProductFamily=currentProductFamily;

	currentProductFamily=val;

	if (currentProductFamily!=oldProductFamily && oldProductFamily>0) {

		var i = familyMarked[oldProductFamily].length

		while (i--) {

			scheduler.deleteMarkedTimespan(familyMarked[oldProductFamily][i]);

		    familyMarked[oldProductFamily].splice(i, 1);

		}

	}

	if (currentProductFamily>0) {

		if (typeof(productFamilyHours[currentProductFamily])!='undefined') {

			var startTime = productFamilyHours[currentProductFamily][0];

			var endTime = productFamilyHours[currentProductFamily][1];

			if (endTime<startTime) {

				endTime = productFamilyHours[currentProductFamily][0];

				startTime = productFamilyHours[currentProductFamily][1];

			}

			if (startTime<endTime) {

				scheduler.config.first_hour = startTime;

				scheduler.config.last_hour = endTime;

				scale = Math.ceil(24/(endTime-startTime));

				scheduler.config.hour_size_px = (25*scale)+1;

			} else {

				scheduler.config.first_hour = 0;

				scheduler.config.last_hour = 24;

				scheduler.config.hour_size_px = 26;

			}

		}

		if (oldProductFamily==0) {

			var i = globalMarked.length

			while (i--) {

				scheduler.deleteMarkedTimespan(globalMarked[i]);

			    globalMarked.splice(i, 1);

			}

		}

		

		for (familyMarkedIndex in familyMarkedTimeSpans[currentProductFamily]) {

			var familyMaredTimeSpan = familyMarkedTimeSpans[currentProductFamily][familyMarkedIndex];

			if (objectFilter==0 || familyMaredTimeSpan.id_object==0 || objectFilter==familyMaredTimeSpan.id_object)

				familyMarked[currentProductFamily].push(scheduler.addMarkedTimespan(familyMaredTimeSpan));

		}

		

	} else {

		scheduler.config.first_hour = 0;

		scheduler.config.last_hour = 24;

		scheduler.config.hour_size_px = 26;

	}

	createTimeline();

	//scheduler.setCurrentView(currentViewDate);

	scheduler.updateView();

}

function objectFilterChange(val) {

	oldObjectFilter=objectFilter;

	objectFilter=val;

	$("#unit_button").hide();

	if (typeof(units)!='undefined' && val != 0 && units[val]!=undefined) {

		scheduler.createUnitsView({

		    name:"unit",

		    property:"stock", //the mapped data property

		    list:units[val]

		});

		$("#unit_button").show();

	}

	else if (scheduler._mode == 'unit') planningMode('month');

	

	if (typeof(currentProductFamily)!='undefined' && currentProductFamily>0) {

		if (oldObjectFilter!=objectFilter) {

			var i = familyMarked[currentProductFamily].length

			while (i--) {

				scheduler.deleteMarkedTimespan(familyMarked[currentProductFamily][i]);

			    familyMarked[currentProductFamily].splice(i, 1);

			}

			for (familyMarkedIndex in familyMarkedTimeSpans[currentProductFamily]) {

				var familyMaredTimeSpan = familyMarkedTimeSpans[currentProductFamily][familyMarkedIndex];

				if (objectFilter==0 || familyMaredTimeSpan.id_object==0 || objectFilter==familyMaredTimeSpan.id_object)

					familyMarked[currentProductFamily].push(scheduler.addMarkedTimespan(familyMaredTimeSpan));

			}

		}

	}

	

	scheduler.updateView();

}

function assignmtFilterChange(val) {

	assignmtFilter=val;

	scheduler.updateView();

}

function stockFilterChange(val) {

	stockFilter=val;

	//currentProductFamily=val;

	//createTimeline();

	//scheduler.setCurrentView(currentViewDate);

	scheduler.updateView();

}



function assignmtFilterChange(val) {

	assignmtFilter=val;

	scheduler.updateView();

}



function createTimeline() {

	for(prod in allProductsList) {

		product = allProductsList[prod];

		scheduler.deleteSection(product.key);

		productkey = product.key.split("-");;

		for (id_family in productFamilyProducts) {

			if ($.inArray(productkey[0], productFamilyProducts[id_family])>-1) { //productFamilyProducts[id_category].indexOf(id_product_str)

				if (id_family == currentProductFamily ||currentProductFamily==0) scheduler.addSection(product, (productkey[1]=="0" ? null : id_family+"-0"));

			}

		}

	}

}



function createUnits() {

	scheduler.createUnitsView({

		    name:"unit",

		    property:"unit_id", //the mapped data property

		    list:[              //defines the units of the view

		        {key:1, label:"Section A"},

		        {key:2, label:"Section B"},

		        {key:3, label:"Section C"}  

		    ]

		});

}



function refreshAvailability() {

    console.log("A5");

	$('#availability').html('<img src="../img/loader.gif">');

	//case edition

	if ($("#rescheduleProduct").length) {

		var id_product = $("#rescheduleProduct").val();

		var id_product_attribute = $("#rescheduleProductAttribute").val();

		var id_object = $("#rescheduleObject").val();

	//case creation

	} else if ($("#scheduleProduct").length) {

		var id_product = $("#scheduleProduct").val();

		var id_product_attribute = $("#scheduleProductAttribute").val();

		var id_object = $("#myownr_object").val();

	//case creation

	} else if ($("#id_product").length) {

		var id_product = $("#id_product").val();

		var id_product_attribute = $("#id_product_ttribute").val();

		var id_object = $("#myownr_object").val();

	} else if (resaJSON!=undefined) {

		var id_product = resaJSON.id_product;

		var id_product_attribute = resaJSON.id_product_attribute;

		var id_object = resaJSON.id_object;

	}

	$('#availability').load(ajaxAdmin, {

	   action: 'availability', 

       id_product : id_product,

       id_product_attribute : id_product_attribute, 

       id_object : id_object, 

       period: getFormParams()

   });

}



function preInitScheduler(ps16) {

	    if (ps16) {

	    	scheduler.xy.nav_height = 0;

	    	scheduler.xy.month_scale_height = 35;

		} else {

			scheduler.xy.nav_height = 32;

			scheduler.xy.month_scale_height = 20;

		}

        scheduler.xy.bar_height = 35;

        

        scheduler.xy.menu_width = 0;

        

        scheduler.config.hour_size_px = 26;



        scheduler.config.xml_date="%Y-%m-%d %H:%i";

        scheduler.config.wide_form = true;

        scheduler.config.multi_day=false;

        scheduler.config.icons_select = [];

        scheduler.config.icons_edit = [];

        scheduler.config.edit_on_create=false;

        scheduler.config.separate_short_events = true;



        scheduler.templates.event_class = function(start,end,event){

		    if (event.validated=="1") //if the date in the past

		          return "validated_event"; //sets a special css class for it

		    return "unvalidated_event";//default value

		}

		

		scheduler.templates.event_text=function(start,end,event){

			return renderElem(start, end, event, false);

		}

		

		scheduler.templates.event_bar_text=function(start,end,event){

			return renderElem(start, end, event, true);

		}

		

		scheduler.templates.event_bar_date=function(start,end,event){

			return "";

		}

		

		scheduler.filter_day = scheduler.filter_week = scheduler.filter_month = scheduler.filter_timeline = scheduler.filter_grid = function(ev_id, event){

			return ((event.productFamilly == currentProductFamily || currentProductFamily==0) && (event.stock == stockFilter || stockFilter==0 || stockFilter=='') && (event.object == objectFilter || objectFilter=='') && (event.assigned == assignmtFilter || assignmtFilter==''));

		}

		

		scheduler.createTimelineView({

	         name:		"timeline",

	         x_unit:	"day",

	         x_date:	"%D %j",

	         x_step:	1,

	         x_size:	timeLineLen,

	         y_unit:productsList,

	         y_property:"ykey",

	         render: "tree",

			 folder_events_available: false,

			 section_autoheight : false,

			 dy:35,

			 folder_dy:20,

			 event_dy:30,

			 event_min_dy:18

	    });

	    

	    scheduler.createUnitsView({

		    name:"unit",

		    property:"unit_id", //the mapped data property

		    list:[              //defines the units of the view

		        {key:1, label:"Section A"},

		        {key:2, label:"Section B"},

		        {key:3, label:"Section C"}  

		    ]

		});

	    

	    scheduler.createGridView({

		       name:"grid",

		       fields:[

		             {id:"id",       label:translations["col_id"], align:'center', width:30, sort:'int'},

		             {id:"ref",       label:translations["col_ref"], align:'center', width:60, sort:'str'},

		             {id:"start_date",     label:translations["col_start"], sort:'date', width:110},

		             {id:"end_date",     label:translations["col_end"], sort:'date', width:110},

		             {id:"id_order",    label:translations["col_order_id"], align:'center', width:40, sort:'int'},

		             {id:"status_text",    label:translations["col_order_status"], align:'center', width:140, sort:'str', template:function(start,end,ev){return "<div class=\'status_label\' style=\'background-color:"+ev.status_color+"\'><span>"+ev.status_text+"</span></div>"}},

		             {id:"amount",    label:translations["col_amount"], align:'center', width:70, sort:'int'},

		             {id:"customer",     label:translations["col_customer"], sort:'str', width:140, template:function(start,end,ev){return (ev.customer!=null ? ev.customer : '')}},

		             {id:"product",     label:translations["col_product"], sort:'str', width:'*', template:function(start,end,ev){return ev.product+ev.attribute}},

		             {id:"quantity",     label:translations["col_quantity"], sort:'int', width:40},

		             {id:"validated",     label:translations["col_validated"], sort:'str', width:65, template:function(start,end,ev){return (ev.validated=="1" ? translations["label_val"] : translations["label_unval"])}},

		             {id:"comment",     label:translations["col_comment"], sort:'str', width:60},

		       ],

		       paging:true

		});

		

		scheduler.locale.labels.grid_tab = "Grid";

		scheduler.locale.labels.unit_tab = "Unit"

		

	    scheduler.showLightbox = function(id){

		     showResaBox(id);

		     showResaDetails(id,"");

		}

		

		scheduler.attachEvent("onBeforeEventChanged", function(event_object, native_event, is_new, unmodified_event){

		     showResaBox(event_object.id, true);

		     var convert = scheduler.date.date_to_str("%Y-%m-%d %H:%i:%s");

		     unmodifiedEvent = unmodified_event;

		     showResaDetails(event_object.id, convert(event_object.start_date)+"#"+convert(event_object.end_date), event_object.ykey, event_object.stock);

		     return true;

		});



		scheduler.attachEvent("onDblClick", function (event_id, native_event_object){

		       scheduler.showLightbox(event_id, false);

		});

		

		scheduler.attachEvent("onViewChange", function (mode , date){

		       currentViewDate=date;

		       currentViewMode=mode;

		       $("#datepickerDate").val(myOwnConvertDate(currentViewDate));

		       $(".btn-period").each(function(index) {

					if ($(this).attr("view")==currentViewMode) 

						$(this).addClass("btn-primary");

					else $(this).removeClass("btn-primary");

				});

		       if (this._mode == 'grid') {

					var value = function(ev){ return ev["start_date"];};

				    var rule = function (a,b, getVal){ return new Date(getVal(a))< new Date(getVal(b))?1:-1};

				    var params = {dir:"asc", value:value, rule:rule};

				    //$(".dhx_grid_line div").eq(1).addClass("dhx_grid_sort_asc");

				    //$(".dhx_grid_line div").eq(1).append('<div class="dhx_grid_view_sort" style="left:108px">&nbsp;</div>');

				    scheduler.grid.draw_sort_marker($(".dhx_grid_line div").eq(1)[0], 'asc');

				    scheduler.clear_view();

					scheduler.grid._fill_grid_tab(scheduler._gridView, params);

				}

		});

		

		//redefine render data for defaut sort

		scheduler.attachEvent("onTemplatesReady",function(){

			var old = scheduler.render_data;

			scheduler.render_data=function(evs){

				if (this._mode == 'grid') {

					var value = function(ev){ return ev["start_date"];};

				    var rule = function (a,b, getVal){ return new Date(getVal(a))< new Date(getVal(b))?1:-1};

				    var params = {dir:"asc", value:value, rule:rule};

				    $(".dhx_grid_line div").eq(1).addClass("dhx_grid_sort_asc");

				    $(".dhx_grid_line div").eq(1).append('<div class="dhx_grid_view_sort" style="left:108px">&nbsp;</div>');

					scheduler.grid._fill_grid_tab(scheduler._gridView, params);

				} else

					return old.apply(this,arguments);

			};

		});

		if ($("#objectFilter").length) objectFilter = $("#objectFilter").val();

}

function cleanResaBox(id)  {

	if (("" + id).indexOf('#') > -1) {

		id = id.split('#')[0];

	}

    if (typeof(scheduler)!='undefined') {

    	if (typeof(resaSaved)!='undefined' && resaSaved=='1') {

    		var event = scheduler.getEvent(id);

    		if (typeof(event)!= 'undefined' && typeof(unmodifiedStartDate)!= 'undefined') {

		        scheduler.setEventStartDate(id, unmodifiedStartDate);

				scheduler.setEventEndDate(id, unmodifiedEndDate);

				scheduler.getEvent(id).validated = unmodifiedValidated;

				scheduler.updateEvent(id);

			}

		} else scheduler.deleteEvent(id);

	}

}

		



function showResaBox(id, isReschedule) {

	if ($(window).width()<768) {

		var wwidth = $(window).width();

		var wheight = $(window).height();

		$.fancybox({

	        "width": wwidth+"px",

	        "height": wheight+"px",

	        "padding": 0,

	        "autoScale": true,

	        "transitionIn": "fade",

	        "transitionOut": "fade",

	        "hideOnOverlayClick": true,

	        "showCloseButton": false,

	        "closeBtn": false,

		    "margin": 0,

		    "afterClose": function() {

	           cleanResaBox(id);

	        },

	        "content": "<div style=\'width:"+wwidth+"px;height:"+wheight+"px;text-align:center\' id=\'myOwnReservationLoading\'><img style=\'margin-top:140px\' src=\'"+imgDir+"/loader.gif\'></div><div style=\'width:"+wwidth+"px;height:100%;display:none\' id=\'myOwnReservationContent\'></div>"

	    });

	} else {

		$.fancybox({

	        "width": "400px",

	        "height": "600px",

	        "padding": 0,

	        "autoScale": true,

	        "transitionIn": "fade",

	        "transitionOut": "fade",

	        "hideOnOverlayClick": true,

	        "showCloseButton": false,

	        "closeBtn": false,

		    "margin": 0,

		    "afterClose": function() {

	           cleanResaBox(id);

	        },

	        "content": "<div style=\'width:420px;height:500px;text-align:center\' id=\'myOwnReservationLoading\'><img style=\'margin-top:140px\' src=\'"+imgDir+"/loader.gif\'></div><div style=\'width:420px;display:none\' id=\'myOwnReservationContent\'></div>"

	    });

    }

	$(document).bind('keydown.fb', function(e) {

		if (e.keyCode == 13) {

			e.preventDefault();

			$("#adminbox_submit").click();

		}

	});

}