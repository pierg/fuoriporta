/*
* 2010-2012 LaBulle All right reserved
*/


/*--------------------------------- widget selection ---------------------------------*/

function validateSearch() {
	document.myownresWidgetSearch.submit();
}
function enableSearch () {
	$("#searchStartDisabledButton").hide();
	$("#searchStartEnabledButton").show();
}
function validateSearchStartDate () {
	if ($("input[name=startDate]").val()!='') {
		var start = $("#searchStart").datepicker("getDate");
		$("#searchEnd").datepicker('option', 'minDate', start);
	}
}
function initPickerSearch () {
	$("#searchStart").datepicker('option', 'onSelect', validateSearchStartDate);
	$("#searchStart").datepicker('option', 'minDate', minDate);
	$("#searchStart").datepicker('option', 'maxDate', maxDate);
	
	if ($("#searchStart").attr("default")!='')
		var startDate=new Date(parseInt($("#searchStart").attr("default")));
	else var startDate=minDate;
	$("#searchStart").datepicker("setDate", startDate );
	
	$("#searchEnd").datepicker('option', 'onSelect', enableSearch)
	$("#searchEnd").datepicker('option', 'minDate', minDate);
	$("#searchEnd").datepicker('option', 'maxDate', maxDate);
	
	if ($("#searchEnd").attr("default")!='')
		var endDate=new Date(parseInt($("#searchEnd").attr("default")));
	else var endDate=minDate;
	$("#searchEnd").datepicker("setDate", endDate );

	if ($("#searchStart").val()!='' && $("#searchEnd").val()!='') enableSearch();
}

function validatePeriod() {
	var error=false;
	if ($("input[name=startDate]").val()=='') {
		$('#period_start_date').addClass('myownrerror');
		error=true;
	}
	if (!error && $("input[name=startTimeslot]").val()=='') {
		$('#period_start_timeslot').addClass('myownrerror');
		error=true;
	}
	if (!error && $("input[name=endDate]").val()=='') {
		$('#period_end_date').addClass('myownrerror');
		error=true;
	}
	if (!error && $("input[name=endTimeslot]").val()=='') {
		$('#period_end_timeslot').addClass('myownrerror');
		error=true;
	}
	var start = $("#searchStart").datepicker("getDate");
	$("#searchStart").val(start.getFullYear()+'-'+(start.getMonth()+1)+'-'+start.getDate());
	var end = $("#searchEnd").datepicker("getDate");
	$("#searchEnd").val(end.getFullYear()+'-'+(end.getMonth()+1)+'-'+end.getDate());
	validateSearch();
}