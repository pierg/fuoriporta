/*
* 2010-2014 LaBulle
*
* NOTICE OF LICENSE
*
* This source file is subject to Terms and Conditions of Use 
* Available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade module to newer
* versions in the future. If you wish to customize module for your
* needs please refer to http://www.labulle.net for more information.
*
* @author    laBulle.net <info@labulle.net>
* @copyright 2010-2014 laBulle
* @license   http://www.labulle.net/download/CGV-en.pdf
*/
function addClassName(element,add_classe){if(element){classe=element.className;element.className=classe+' '+add_classe;}}
function removeClassName(element,remove_classe){if(element){tab_classe=element.className.split(' ');new_tab_classe=new Array();for(var i in tab_classe){if(tab_classe[i]!=remove_classe){new_tab_classe.push(tab_classe[i]);}}
element.className=new_tab_classe.join(' ');}}
function addEvent(obj,event,fct){if(obj.attachEvent){obj.attachEvent('on'+event,fct);}else{obj.addEventListener(event,fct,true);}}
function removeEvent(obj,event,fct){if(obj.DetachEvent){obj.DetachEvent('on'+event,fct);}else{obj.removeEventListener(event,fct,true);}}

function get_pos_mouse(e){if(!e){var e=window.event;}
if(e.pageX||e.pageY){posx=e.pageX;posy=e.pageY;}else if(e.clientX||e.clientY){posx=e.clientX+document.body.scrollLeft+document.documentElement.scrollLeft;posy=e.clientY+document.body.scrollTop+document.documentElement.scrollTop;}}
function getpos(element){var top=0,left=0;do{top+=element.offsetTop||0;left+=element.offsetLeft||0;element=element.offsetParent;}while(element);return left;};function in_array(v,t){var length=t.length;for(var i=0;i<length;i++){if(t[i]==v){return true;}}
return false;}

function dd(v){return document.getElementById(v);}
function ddv(v){if(document.getElementById(v)){return document.getElementById(v).value;}else{}}
function ddh(v){return document.getElementById(v).innerHTML;}

var posx, posy;
var is_charge_page_ready = true;
var ready = true;
var son;
var oldlocation = location.pathname;
var planning = {};
var planningparam = {
    largeur: 742,
    nbminute: 1440,
    diffcurseur: 10,
    largeurplagemin: 30,
    espaceplagemin: 0,
    pasmin: 5,
    pas: Math.round(5 * 742 / 1440),
}
var deplace = false;
var diffX = 0;
var curseur = {}
var pointeur = {}

var timeslots = Array();


var clickevent = {
    btn_connexion: 'connexion',
    mdpoublie: 'mdpoublie',
    send_mdp: 'send_mdp',
    deconnexion: 'deconnexion',
    burger: 'open_menu',
    menu_configuration: 'open_menu_secondaire',
    menu_boutique: 'open_menu_secondaire',
    item_menu: 'charge_page',
    select: 'open_select',
    ecouter: 'ecouter',
    status_open: 'change_status',
    status_pause: 'change_status',
    edit_profil: 'edit_profil',
    notification: 'open_notification',
    notification_t: 'open_notification',
    carnetadresse: 'open_carnet',
    carnet_t: 'open_carnet',
    callback: 'open_dialer',
    dialer_t: 'open_dialer',
    clavier_touche: 'clavier_touche',
    save_carnet: 'save_carnet',
    killcall: 'killcall',
    make_callback: 'make_callback',
    jour: 'switchjour',
    reinitialiser: 'reinitialiser',
    contextprofil: 'contextprofil',
    btn_planning: 'save_planning',
    openCTZ: 'openCTZ',
    closeCTZ: 'closeCTZ',
    submitCTZ: 'submitCTZ',
    chooseCTZ: 'chooseCTZ',
    duplication: 'duplicate_monday',
    switch_telactif: 'switch_telactif',
    switch_emailactif: 'switch_emailactif',
    confirmappel: 'confirmappel',
    efface_phone: 'delete_redirection',
    remonte_phone: 'change_index_redirection',
    descend_phone: 'change_index_redirection',
    change_parametre: 'change_parametre',
    routage_action: 'routage_action',
    routage_edit: 'routage_edit',
    routage_choice_num: 'routage_choice_num',
    routage_record_num: 'routage_record_num',
    routage_touche: 'routage_touche',
    routage_choice_touch: 'routage_choice_touch',
    routage_sens: 'routage_sens',
    routage_add: 'routage_add',
    routage_add_none: 'routage_add',
    routage_delete: 'routage_delete',
    routage_activer: 'routage_activer',
    routage_desactiver: 'routage_desactiver',
    routage_ecouter: 'routage_ecouter',
    liste_credit: 'select_credit',
    numerofax: 'select_numerofax',
    change_listefax: 'get_faxnum',
    btn_credit: 'get_credit',
    btn_paiement: 'get_paiement',
    btn_valid_credits: 'valid_free_credits',
    btn_fax: 'get_fax',
    btn_active_fax: 'confirm_achat_fax',
    submit_boutique: 'submit_boutique',
    conferencesecure: 'conferencesecure',
    conferencesave: 'conferencesave',
    btnsms: 'send_sms',
    tempspassage: 'tempspassage',
    btn_uploadfax: 'upload_fax',
    btn_sendfax: 'send_fax',
    oa_step: 'oa_step',
    change_genremusic: 'change_genremusic',
    change_speakrine: 'change_speakrine',
    generer_phonecode: 'generer_phonecode',
    switch_annonce: 'switch_annonce',
    checkmusique: 'checkmusique',
    ecouter_synthese: 'ecouter_synthese',
    btn_uploadAnnonce: 'upload_annonce',
    ecouter_annonce: 'ecouter_annonce',
    sauvegarder_annonce: 'sauvegarder_annonce',
    typerec: 'typerecappels',
    gorecappels: 'gorecappels',
    openXLS: 'openXLS',
    monthXLS: 'monthXLS',
    submitXLS: 'submitXLS',
    urgence: 'change_urgence',
    commentaire: 'commentaire_edit',
    callback_button: 'callback_emit',
    vidercorbeilleappels: 'vidercorbeilleappels',
    checkallappels: 'checkallappels',
    checkappels: 'checkappels',
    corbeille: 'corbeille',
    save_coordonnees: 'save_coordonnees',
    save_motdepasse: 'save_motdepasse',
    modify_bank: 'modify_bank',
}


var switchjour = function(elem) {
	if (typeof(elem)!='undefined') {
		var etat = $(elem).hasClass("action-enabled");
		var jour = elem.dataset.jour;
	} else {
		var etat = (this.className == 'jour');
		var jour = this.dataset.jour;
	}
    
   
    var tostatus = '0';
    if (etat) {
		$(elem).find("i.icon-remove").addClass("hidden");
		$(elem).find("i.icon-check").removeClass("hidden");
        tostatus = '0';
    } else {
    	tostatus = '1';
		$(elem).find("i.icon-remove").removeClass("hidden");
		$(elem).find("i.icon-check").addClass("hidden");
    }
    
    setDayStatus(jour, tostatus);
    
    for (j = 0; j < timeslots.length; j++) {
		var timeslot=timeslots[j];
		
		if (timeslot.days[jour] != tostatus) {
			timeslots[j].days[jour] = tostatus;
			construire_planning(jour, timeslot, j+2);
			timeslot.save();
		}
	};
	insertDayTimeslots();
    construire_jour(jour);
}

function setDayStatus(jour, etat) {
	console.log('setDayStatus:'+etat);
	var elem = $("#jour_"+jour);
	if (etat==1) {
		elem.find("a.btn").removeClass("action-disabled");
		elem.find("a.btn").addClass("action-enabled");
		
		elem.find("i.icon-remove").addClass("hidden");
		elem.find("i.icon-check").removeClass("hidden");
		
		$('#clipjour_' + jour).removeClass("of");
        //elem.addClass("jour");
    } else {
    	elem.find("a.btn").addClass("action-disabled");
		elem.find("a.btn").removeClass("action-enabled");
		
		elem.find("i.icon-remove").removeClass("hidden");
		elem.find("i.icon-check").addClass("hidden");
		
		$('#clipjour_' + jour).addClass("of");
        //elem.removeClass("jour");
        /*dd('clipjour_' + jour).className = 'clipjour of';
        dd('jour_' + jour).className = 'jour';*/
    }
}

function insertDayTimeslots() {
	for (var jour = 1; jour <= 7; jour++) {
		var content = '';
		var temp_timeslots = new Array();
		for (j = 0; j < timeslots.length; j++) {
			temp_timeslots[timeslots[j].start] = timeslots[j];
		}
		//for (j = 0; j < timeslots.length; j++) {
		for (var temp_timeslot in temp_timeslots) {
			var timeslot=temp_timeslots[temp_timeslot];
		
			if (timeslot.days[jour] == 1) {
				content += '<li><a onclick="disable_plage('+jour+', '+timeslot.id+');" ><i class="icon-remove"></i> '+trads.disable;
			} else {
				content += '<li><a onclick="enable_plage('+jour+', '+timeslot.id+');" ><i class="icon-check"></i> '+trads.enable;
			}
			content += ' '+trads.timeslot+' #'+Math.abs(timeslot.id)+' <span style="float:right;font-style:italic">'+get_heure(timeslot.start)+'-'+get_heure(timeslot.end)+'</span></a></li>';
		}
		$("#jour_"+jour).find("ul.dropdown-menu").html(content);
	}
}

function getFreeTimeslots() {
	var tuples=new Array();
	for (var jour = 1; jour <= 7; jour++) {
		var day_timeslots=new Array();
    	var timeslot_pos=0;
		for (j = 0; j < timeslots.length; j++) {
			if (timeslots[j].enabled && timeslots[j].days[jour]==1)
				day_timeslots.push(timeslots[j]);
		}
		
		for (j = 0; j < day_timeslots.length; j++) {
			var timeslot=day_timeslots[j];
			//if (timeslot.days[jour]==1) {
				
				if (j==0) {
					var tuple = '0-'+timeslot.start;
					if (tuples[tuple]==undefined)
						tuples[tuple]=1;
					else tuples[tuple]++;
				}
				var tuple = '';
				if (j==day_timeslots.length-1) {
					tuple = timeslot.end+'-'+(24*60);
				} else if( timeslot.end!=day_timeslots[j+1].start) {
					tuple = timeslot.end+'-'+day_timeslots[j+1].start;
				}

				if(tuple!='') {
					if (tuples[tuple]==undefined)
						tuples[tuple]=1;
					else tuples[tuple]++;
				}
				
			//}
		}
	}
		
		
		for (var main_tuple in tuples) {
			if (tuples[main_tuple]>0) {
				main_tuble_tab = main_tuple.split('-');
				for (var sub_tuple in tuples) {
					if (sub_tuple!=main_tuple && tuples[sub_tuple]>0) {
						sub_tuble_tab = sub_tuple.split('-');
						if ((sub_tuble_tab[0]==main_tuble_tab[0] || sub_tuble_tab[1]==main_tuble_tab[1]) && sub_tuble_tab[1] >= main_tuble_tab[0] && sub_tuble_tab[0] <= main_tuble_tab[1]) {
				 			/*tuples[main_tuple] += tuples[sub_tuple];
				 			tuples[sub_tuple]=0;*/
				 			tuples[sub_tuple] += tuples[main_tuple];
				 			tuples[main_tuple]=0;
				 		}
					}
				}
			}
		}
		
		var new_tuples=new Array();
		for (var main_tuple in tuples) {
			if (tuples[main_tuple]>0) {
				main_tuble_tab = main_tuple.split('-');
				new_tuples[main_tuble_tab[0]] = main_tuple;
			}
		}
		
		
	return new_tuples;
}
function insertPositions() {
	var tuples = getFreeTimeslots();

	var contents = "";
	for (var tuple in tuples) {
		var tuple_tab = tuples[tuple].split("-");
		
		var start = tuple_tab[0];//timeslots[j].end;
		var hour = Math.floor(start/60);
		var min = start % 60;
		if (min<10) min = "0" + min;
		var start_str = hour + ":" + min;
		
		var end = tuple_tab[1];
		var hour = Math.floor(end/60);
		var min = end % 60;
		if (min<10) min = "0" + min;
		var end_str = hour + ":" + min;
			
		contents += '<li><a onclick="add_timeslot(\'' + tuples[tuple] + '\');">'+trads.between+' ';
		contents += start_str + ' ' + trads.and + ' ' + end_str + '</a></li>';
	}
	if (tuples.length==0) {
		contents += '<li><a onclick="add_timeslot(\'' + 8*60 + '-'+ 18*60 + '\');">'+trads.between+' ';
		contents += '08:00' + ' ' + trads.and + ' ' + '18:00' + '</a></li>';
	}
	$(".insert_positions").html(contents);
}
		
function lancement_planning() {

	construire_timeslots();
    for (var jour = 1; jour <= 7; jour++) {
        construire_jour(jour);
    }
    addEvent(document, 'mousedown', planning_onmousedown);
    addEvent(document, 'touchstart', planning_onmousedown);
    addEvent(document, 'mousemove', planning_onmousemove);
    addEvent(document, 'touchmove', planning_onmousemove);
    addEvent(document, 'mouseup', planning_onmouseup);
    addEvent(document, 'touchend', planning_onmouseup);
    addEvent(document, 'dblclick', drag_ondblclick);
}
function ajust_planning() {
	var w = $("#timeslots_planing").width();
	var new_largeur = Math.round((w-170) / planningparam.pasmin) * planningparam.pasmin;
	if (planningparam.largeur != new_largeur) {
	  	planningparam.largeur = new_largeur;
	  	$(".barrejour").width(new_largeur);
	  	construire_semaine();
	}
}
function construire_timeslots() {
	//console.log('construire_timeslots');
	$( "input[class=timeslot]" ).each(function( index ) {
		console.log( "timeslot: " + $( this ).val() );
		var timeslot = construire_timeslot($( this ).val());
		/*var timeslot_data = $(this).val().split('|');
		var timeslot_days = timeslot_data[3].split(',');
		var timeslot = {
			id:timeslot_data[0], 
			start:timeslot_data[1], 
			end:timeslot_data[2], 
			days:timeslot_days, 
			enabled:timeslot_data[4], 
			quota:timeslot_data[5], 
			name:timeslot_data[6],
			save : function() {
				
				var sortie = this.id + '|' + this.start + '|' + this.end + '|' + this.days.join(',') + '|' + this.enabled + '|' + this.quota + '|' + this.name;
				console.log('save'+this.id+':'+sortie);
				if (dd('timeslot' + this.id)!=null) {
					dd('timeslot' + this.id).value = sortie;
				} else {
					$( "#timeslots_data").append('<input type="hidden" class="timeslot" name="timeslots[]" id="timeslot'+this.id+'" value="'+sortie+'">');
				}
			},
			setName : function(name) {
				this.name = name;
				this.save();
			},
			setQuota : function(quota) {
				this.quota = quota;
				this.save();
			},
		};*/
		timeslots.push(timeslot);
	});
}
function construire_timeslot(val) {
	var timeslot_data = val.split('|');
	if (timeslot_data.length>1)
		var timeslot_days = timeslot_data[3].split(',');
	else var timeslot_days = new Array();
	var timeslot = {
		id:timeslot_data[0], 
		start:timeslot_data[1], 
		end:timeslot_data[2], 
		days:timeslot_days, 
		enabled:timeslot_data[4], 
		quota:timeslot_data[5], 
		name:timeslot_data[6],
		save : function() {
			
			var sortie = this.id + '|' + this.start + '|' + this.end + '|' + this.days.join(',') + '|' + this.enabled + '|' + this.quota + '|' + this.name;
			console.log('save'+this.id+':'+sortie);
			if (dd('timeslot' + this.id)!=null) {
				dd('timeslot' + this.id).value = sortie;
			} else {
				$( "#timeslots_data").append('<input type="hidden" class="timeslot" name="timeslots[]" id="timeslot'+this.id+'" value="'+sortie+'">');
			}
		},
		setName : function(name) {
			this.name = name;
			this.save();
		},
		setQuota : function(quota) {
			this.quota = quota;
			this.save();
		},
	};
	return timeslot;
}


function construire_jour(jour) {
	//console.log('construire_jour:'+jour);
	k=0;
	dd('barrejour_' + jour).innerHTML = '';
	//planning['jour' + jour] = [];
	for (j = 0; j < timeslots.length; j++) {
		var timeslot=timeslots[j];
		construire_planning(jour, timeslot, j+2);
		/*if (timeslot.days[jour] == '1') {
			planning['jour' + jour][k] = timeslot;
			k++;
		}*/
	};
	
	//if (planning['jour' + jour].length==0) {
		/*dd('clipjour_' + jour).className = 'clipjour of';
        dd('jour_' + jour).className = 'jour';*/
        //dd('jour_' + jour).children[0].className = 'switch';
	//}
}

function construire_planning(jour, timeslot, zindex) {
	if (typeof(zindex)=='undefined') var zindex = 1;
    var numero = timeslot.id;
    var w = Math.round((timeslot.end - timeslot.start) * planningparam.largeur / planningparam.nbminute);
    var mL = Math.round(timeslot.start * planningparam.largeur / planningparam.nbminute);
    var mL_deb = 0;// - planningparam.diffcurseur;
    var mL_fin = w - (planningparam.diffcurseur * 2);
    var heure_deb = get_heure_pos(mL);
    var heure_fin = get_heure_pos(mL + w);
    if (timeslot.days[jour] == '1') {
        //dd('clipjour_' + jour).className = 'clipjour';
        var sortie = '<div class="plage profil' + timeslot.enabled + '" id="plage_' + jour + '_' + numero + '" style="width:' + w + 'px;margin-left:' + mL + 'px;" data-numero="' + numero + '" data-jour="' + jour + '" onmouseover="show_curseur(' + jour + ',' + numero + ');" onmouseout="hide_curseur(' + jour + ',' + numero + ');">';
        sortie += '<p class="curseur deb" id="curseur_deb_' + numero + '_' + jour + '" data-origine="deb" style="margin-left:' + mL_deb + 'px;"><span style="margin-left:-'+planningparam.diffcurseur+'px">' + heure_deb + '</span><i class="icon-bars icon-rotate-90"></i></p>';
        sortie += '<p class="menu" id="menu_' + numero + '_' + jour + '" style="margin-left:' + (mL_deb + (planningparam.diffcurseur * 2)) + 'px;"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown" onclick="dd(\'menu_dropdown_' + numero + '_' + jour+'\').style.display=\'block\';"> <i class="icon-caret-down"></i>&nbsp; </button></p>';
        sortie += '<p class="curseur fin" id="curseur_fin_' + numero + '_' + jour + '" data-origine="fin" style="margin-left:' + mL_fin + 'px;"><span>' + heure_fin + '</span><i class="icon-bars icon-rotate-90"></i></p>';
        sortie += '<ul class="dropdown-menu" id="menu_dropdown_' + numero + '_' + jour + '"><li class="dropdown-header">'+trads.timeslot+' #'+Math.abs(numero)+'</li><li><a onclick="detach_plage(' + jour + ', ' + numero + ');" title="" class="detach"><i class="icon-indent"></i> '+trads.detach+'</a></li><li><a onclick="disable_plage(' + jour + ', ' + numero + ');" title="" class="delete"><i class="icon-remove"></i> '+trads.disable+'</a></li><li><a onclick="if (confirm(\''+trads.delete+' ?\')){ delete_plage(' + numero + '); };" title="" class="delete"><i class="icon-trash"></i> '+trads.delete+'</a></li></ul>';
        sortie += '</div>';
        dd('barrejour_' + jour).innerHTML += sortie;
        dd('jour_' + jour).className = 'jour on';
        //dd('jour_' + jour).children[0].className = 'switch on';
        //$('#jour_' + jour).children(0);
    /*} else {
        dd('clipjour_' + jour).className = 'clipjour of';
        dd('jour_' + jour).className = 'jour';
        dd('jour_' + jour).children[0].className = 'switch';*/
    }
}


var reinitialiser = function() {
    l = '0-540-720-1-1-0-|0-840-1080-1-1-0-|';
    for (i = 1; i <= 7; i++) {
        if (i >= 6) l = '0-540-720-0-1-0-|0-840-1080-0-1-0-|';
        dd('inputPlan' + i).value = l;
    }
    construire_semaine();
}
var duplicate_monday = function() {
    var lundi = ddv('inputPlan1');
    for (i = 2; i <= 7; i++) {
        dd('inputPlan' + i).value = lundi;
        construire_jour(i);
    }
}

function construire_semaine() {
    for (i = 1; i <= 7; i++) {
        construire_jour(i);
    }
}

function get_heure_pos(pos) {
    var v = pos * planningparam.nbminute / planningparam.largeur;
    v = Math.round(v / planningparam.pasmin) * planningparam.pasmin;
    return get_heure(v);
}

function get_heure(val) {
    var heure = Math.floor(val / 60);
    var minute = val % 60;
    if (minute == 0) minute = '00';
    if (minute == 5) minute = '05';
    return heure + 'h' + minute;
}

function show_curseur(j, n) {
    if (!deplace) {
    	$("#insert_positions").parent("div").removeClass("open");
    	
        addClassName(dd('curseur_deb_' + n + '_' + j), 'on');
        addClassName(dd('curseur_fin_' + n + '_' + j), 'on');
        addClassName(dd('menu_' + n + '_' + j), 'on');
        addClassName(dd('menu_dropdown_' + n + '_' + j), 'on');
        for (i = 1; i <= 7; i++) {
        	addClassName(dd('plage_' + i + '_' + n), 'hov');
        }
    }
}

function hide_curseur(j, n) {
	
    if (!deplace) {
        removeClassName(dd('curseur_deb_' + n + '_' + j), 'on');
        removeClassName(dd('curseur_fin_' + n + '_' + j), 'on');
        removeClassName(dd('menu_' + n + '_' + j), 'on');
        removeClassName(dd('menu_dropdown_' + n + '_' + j), 'on');
        for (i = 1; i <= 7; i++) {
        	removeClassName(dd('plage_' + i + '_' + n), 'hov');
        }
        if (!$('#plage_' + j+ '_' + n).is(':hover') && !$('#menu_dropdown_' + n + '_' + j).is(':hover') && !$('#menu_' + n + '_' + j).is(':hover') && !$('#curseur_deb_' + n + '_' + j).is(':hover') && !$('#curseur_fin_' + n + '_' + j).is(':hover'))
        	dd('menu_dropdown_' + n + '_' + j).style.display='none';//!$('#barrejour_' + j).is(':hover')

    }
}

function planning_onmousedown(event) {
    var target = event.target || event.srcElement;
    if (target.className.slice(0, 7) == 'curseur' || target.className.slice(0, 9) == 'icon-bars') {
        event.returnValue = false;
        if (event.preventDefault) {
            event.preventDefault();
        }
        deplace = true;
      if (target.className.slice(0, 9) == 'icon-bars')
      	target = target.parentNode;
        curseur = {
            self: target,
            initx: target.parentNode.offsetLeft,
            initw: target.parentNode.offsetWidth,
            origine: target.getAttribute('data-origine'),
            plage: target.parentNode,
            barre: target.parentNode.parentNode,
            numero: parseInt(target.parentNode.getAttribute('data-numero')),
            jour: target.parentNode.getAttribute('data-jour'),
        }
        curseur.fin = dd('curseur_fin_' + curseur.numero + '_' + curseur.jour);
        /*for (var i = 0; i < planning['jour' + curseur.jour].length; i++) {
            curseur.barre.children[i].style.zIndex = '1';
        }*/
        curseur.plage.style.zIndex = '2';
        curseur.limiteDeb = 0;
        curseur.limiteFin = planningparam.largeur;
        
   var enabled_days=new Array();
   for (j = 0; j < timeslots.length; j++)
    	if (timeslots[j].id==curseur.numero)
    		var enabled_days = timeslots[j].days;
    	console.log('enabled_days '+enabled_days);	
    console.log('curseur.numero '+curseur.numero);
   for (i = 1; i <= 7; i++) {
     if (enabled_days[i]==1) {
     	console.log('day'+i);
        var day_timeslots=new Array();
        var timeslot_pos=0;
    	for (j = 0; j < timeslots.length; j++) {
    		if (timeslots[j].id==curseur.numero)
    			timeslot_pos=day_timeslots.length;
    		if (timeslots[j].days[i]==1) //if (timeslots[j].days[curseur.jour]==1)
    			day_timeslots.push(timeslots[j]);
    		
    		if (timeslots[j].days[i]==1) 
    			console.log(j+'_'+timeslots[j].start);
		}

        if (curseur.origine == 'deb') {
            curseur.limiteFin = Math.round(curseur.initx + curseur.initw - (planningparam.largeurplagemin * planningparam.largeur / planningparam.nbminute));
            if (timeslot_pos > 0) {
                tmplimiteDeb = Math.round(day_timeslots[timeslot_pos - 1].end * planningparam.largeur / planningparam.nbminute); //planning['jour' + curseur.jour][curseur.numero - 1][2]
                if (tmplimiteDeb > curseur.limiteDeb)
                	curseur.limiteDeb = tmplimiteDeb;
                console.log('deb'+tmplimiteDeb+' '+curseur.limiteDeb);
            }
        }
        if (curseur.origine == 'fin') {
            curseur.limiteDeb = Math.round(curseur.initx + (planningparam.largeurplagemin * planningparam.largeur / planningparam.nbminute));
            if (timeslot_pos < day_timeslots.length - 1) {
                tmplimiteFin = Math.round(day_timeslots[timeslot_pos + 1].start * planningparam.largeur / planningparam.nbminute); //planning['jour' + curseur.jour][curseur.numero + 1][1]
                if (tmplimiteFin < curseur.limiteFin && tmplimiteFin > curseur.limiteDeb)
                	curseur.limiteFin = tmplimiteFin;
                console.log('fin'+tmplimiteFin+' '+curseur.limiteFin);
            }
        }
        
      }
    }
        get_pos_mouse(event);
        diffX = posx - getpos(curseur.plage) - 2;
    }
}

function planning_onmousemove(event) {
    if (deplace) {
        var barrex = getpos(curseur.barre) + 2;
        get_pos_mouse(event);
        posx = posx - barrex - diffX;
        if (curseur.origine == 'fin') {
            posx += curseur.initw;
        }
        if (posx <= curseur.limiteDeb) {
            posx = curseur.limiteDeb;
        } else if (posx >= curseur.limiteFin) {
            posx = curseur.limiteFin;
        }
        if (curseur.origine == 'deb') {
            mL_plage = posx;
            w_plage = curseur.initw + curseur.initx - mL_plage;
            mL_cfin = w_plage - planningparam.diffcurseur *2 ;
        }
        if (curseur.origine == 'fin') {
            mL_plage = curseur.initx;
            w_plage = posx - curseur.initx;
            mL_cfin = w_plage - planningparam.diffcurseur * 2;
        }
        heure = get_heure_pos(posx);
        curseur.plage.style.marginLeft = mL_plage + 'px';
        curseur.plage.style.width = w_plage + 'px';
        curseur.fin.style.marginLeft = mL_cfin + 'px';
        curseur.self.childNodes[0].innerHTML = heure;
        
        for (i = 1; i <= 7; i++) {
        	if (curseur.jour != i) {
	        	var plage = dd('plage_' + i + '_' + curseur.numero);
	        	if (plage != null) {
		        	plage.style.marginLeft = mL_plage + 'px';
			        plage.style.width = w_plage + 'px';
		        }
				var fin = dd('curseur_fin_' + curseur.numero + '_' + i);
				if (fin != null)
					fin.style.marginLeft = mL_cfin + 'px'
				
		        var othercurseur = dd('curseur_'+curseur.origine+'_'+curseur.numero+'_'+i);
		        if (othercurseur != null)
		        	othercurseur.childNodes[0].innerHTML = heure;
	        }
        }
        return;
    }
}

function planning_onmouseup(event) {
    if (deplace) {
        deplace = false;
        enregistrement_data();
        console.log('planning_onmouseup');
        hide_curseur(curseur.jour, curseur.numero);
    }
}

function enregistrement_data() {
    //for (i = 0; i < planning['jour' + curseur.jour].length; i++) {
    	//var timeslot = planning['jour' + curseur.jour][i];
		var timeslot_pos=0;
    	for (j = 0; j < timeslots.length; j++) {
    		if (timeslots[j].id==curseur.numero)
    			timeslot_pos = j;
		}
		var timeslot = timeslots[timeslot_pos];
        //if (i == curseur.numero) {
            if (curseur.origine == 'deb') {
                deb = curseur.plage.offsetLeft;
                deb = Math.round(deb * planningparam.nbminute / planningparam.largeur);
                deb = Math.round(deb / planningparam.pasmin) * planningparam.pasmin;
                fin = timeslot.end;
            } else if (curseur.origine == 'fin') {
                fin = curseur.plage.offsetLeft + curseur.plage.offsetWidth;
                fin = Math.round(fin * planningparam.nbminute / planningparam.largeur);
                fin = Math.round(fin / planningparam.pasmin) * planningparam.pasmin;
                deb = timeslot.start;
            }
        /*} else {
            deb = timeslot.start;
            fin = timeslot.end;
        }*/
        timeslots[timeslot_pos].start = deb;
        timeslots[timeslot_pos].end = fin;
        timeslots[timeslot_pos].save();
    //}
    
    construire_jour(curseur.jour);
    insertPositions();
    insertDayTimeslots();
}

function drag_ondblclick(event) {
    var target = event.target || event.srcElement;
    if (target.className.slice(0, 5) == 'plage') {
        var jour = target.getAttribute('data-jour');
        var numero = target.getAttribute('data-numero');
        disable_plage(jour, numero);
    } else if (target.className == 'barrejour' && target.parentNode.className == 'clipjour') {
        var jour = target.getAttribute('data-jour');
        add_plage(event, jour);
    }
}

function add_plage(event, jour) {
    get_pos_mouse(event);
    var barrex = getpos(dd('barrejour_1')) + 2;
    var valeur = Math.round((posx - barrex) * planningparam.nbminute / planningparam.largeur);
    //var t = planning['jour' + jour];

    //var new_tab = new Array();
    var new_timeslots = new Array();
    var effectue = false;
    var v_deb = valeur - 80;
    var v_fin = valeur + 80;
        v_deb = Math.round(v_deb / planningparam.pasmin) * planningparam.pasmin;
        v_fin = Math.round(v_fin / planningparam.pasmin) * planningparam.pasmin;
	var nb_plages = timeslots.length;
    
    var day_timeslots=new Array();
    var timeslot_pos=0;
    var timeslot_maxid=0;
	for (j = 0; j < timeslots.length; j++) {
		if (Math.abs(parseInt(timeslots[j].id))>timeslot_maxid)
			timeslot_maxid = Math.abs(parseInt(timeslots[j].id));
		if (timeslots[j].days[jour]==1)
			day_timeslots.push(timeslots[j]);
	}
		
		
    //for (var i = 0; i < nb_plages; i++) {
    var h=0;
    for (var i = 0; i < timeslots.length; i++) {
        if (timeslots[i].days[jour]==1) {
	        if (valeur < timeslots[i].start) {
	            effectue = true;
	            if (h == 0) {
	                if (v_deb < 0) v_deb = 0;
	            } else {
	                var v_prec = parseInt(day_timeslots[h - 1].end);
	                if (v_deb < v_prec + 10) {
	                    v_deb = v_prec + 10;
	                }
	            }
	            var v_suiv = parseInt(timeslots[i].start);
	            if (v_fin > v_suiv - 10) {
	                v_fin = v_suiv - 10;
	            }
	            //new_tab[i] = [0, v_deb, v_fin, '1', '1', '0', ''];
	            var newTimeslot = jQuery.extend(true, {}, timeslots[i]);
	            newTimeslot.start=v_deb;
	            newTimeslot.end=v_fin;
	            for (var tjour = 1; tjour <= 7; tjour++) {
					//newTimeslot.days[tjour] = $('#switchjour_'+tjour).hasClass("action-enabled");
					newTimeslot.days[tjour] = (jour==tjour);
				}
				newTimeslot.days[jour]=1;
				newTimeslot.name='';
				newTimeslot.quota=0;
				newTimeslot.enabled=1;
	            newTimeslot.id=-(timeslot_maxid+1);
	            new_timeslots[i] = newTimeslot;
	            new_timeslots[i].save();
	            for (k = i+1; k < timeslots.length + 1; k++) {
	                //new_tab[k] = t[k - 1];
	                new_timeslots[k] = timeslots[k-1];
	            }
	            break;
	        } else {
	            //new_tab[i] = t[i];
	            new_timeslots[i] = timeslots[i];
	        }
	        h++;
        } else {
	        new_timeslots[i] = timeslots[i];
        }
    }
    if (!effectue) {
    	var v_prec=0;
    	if (day_timeslots.length)
        	v_prec = parseInt(day_timeslots[day_timeslots.length - 1].end);
        if (v_deb < v_prec + 10) {
            v_deb = v_prec + 10;
        }
        if (v_fin > 1440) {
            v_fin = 1440;
        }
        //new_tab[nb_plages] = [0, v_deb, v_fin, '1', '1', '0', ''];
        if (day_timeslots.length)
        	var newTimeslot = jQuery.extend(true, {}, day_timeslots[0]);
        else {
	        var newTimeslot = construire_timeslot('');
        }
        newTimeslot.start=v_deb;
        newTimeslot.end=v_fin;
        for (var tjour = 1; tjour <= 7; tjour++) {
			//newTimeslot.days[tjour] = $('#switchjour_'+tjour).hasClass("action-enabled");
			newTimeslot.days[tjour] = (jour==tjour);
		}
		newTimeslot.days[jour]=1;
		newTimeslot.name='';
		newTimeslot.quota=0;
		newTimeslot.enabled=1;
        newTimeslot.id=-(timeslot_maxid+1);
        new_timeslots = timeslots;
        new_timeslots.push(newTimeslot);
        newTimeslot.save();
    }
    timeslots = new_timeslots;
    insertPositions();
    insertDayTimeslots();
    //for (var tjour = 1; tjour <= 7; tjour++) {
		construire_jour(jour);
	//}
}

function add_timeslot(tuple) {
	var effectue = false;
	var new_timeslots = new Array();
	var tuple_tab = tuple.split("-");
	var timeslot_maxid=0;
	for (j = 0; j < timeslots.length; j++) {
		if (Math.abs(parseInt(timeslots[j].id))>timeslot_maxid)
			timeslot_maxid = Math.abs(parseInt(timeslots[j].id));
	}
	for (var i = 0; i <= timeslots.length; i++) {
		
		if ( (i < timeslots.length && tuple_tab[1] <= timeslots[i].start) || (!effectue && i==timeslots.length)) {
				if (timeslots.length)
					var newTimeslot = jQuery.extend(true, {}, timeslots[0]);
				else	
					var newTimeslot = construire_timeslot('');
	            newTimeslot.start=tuple_tab[0];
	            newTimeslot.end=tuple_tab[1];
	            if (timeslots.length) {
		            for (var tjour = 1; tjour <= 7; tjour++) {
						newTimeslot.days[tjour] = ($('#switchjour_'+tjour).hasClass("action-enabled") ? 1 : 0);
					}
				} else {
					for (var tjour = 1; tjour <= 7; tjour++) {
						newTimeslot.days[tjour] = 1;
						setDayStatus(tjour, 1);
					}
				}
				newTimeslot.name='';
				newTimeslot.quota=0;
				newTimeslot.enabled=1;
	            newTimeslot.id=-(timeslot_maxid+1);
	            new_timeslots[i] = newTimeslot;
	            new_timeslots[i].save();
	            for (k = i; k < timeslots.length; k++) {
	                //new_tab[k] = t[k - 1];
	                if (timeslots[k]!=undefined) new_timeslots[k+1] = timeslots[k];
	            }
	            var effectue = true;
	            timeslots = new_timeslots;
	            break;
	            
		} else {
	            //new_tab[i] = t[i];
	        new_timeslots[i] = timeslots[i];
	    }
	}

    timeslots = new_timeslots;
    insertPositions();
	insertDayTimeslots();
    construire_semaine();
    //$(".insert_positions").hide();
}

function detach_plage(jour, numero) {
	console.log('detach_plage'+numero);
	var timeslot_maxid = 0;
	for (j = 0; j < timeslots.length; j++) {
		if (Math.abs(parseInt(timeslots[j].id))>timeslot_maxid)
			timeslot_maxid = Math.abs(parseInt(timeslots[j].id));
		if (timeslots[j].id == numero) {
			var newTimeslot = jQuery.extend(true, {}, timeslots[j]);
			timeslots[j].days[jour] = 0;
			timeslots[j].save();
		}
	};
	
	newTimeslot.id = -(timeslot_maxid+1);
	for (var tjour = 1; tjour <= 7; tjour++) {
		newTimeslot.days[tjour]=0;
	}
	newTimeslot.days[jour]=1;
	newTimeslot.save();
	timeslots.push(newTimeslot);

	 construire_semaine();
}

function rename_plage(numero, name) {
	console.log('rename_plage'+numero);
	for (j = 0; j < timeslots.length; j++) {
		if (timeslots[j].id == numero) {
			timeslots[j].setName(name);
		}
	};

	 construire_semaine();
}

function quota_plage(numero, jour, quota) {
	console.log('quota_plage'+quota);
	var quota_int=parseInt(quota);
	if (quota_int<0) dd('option_quota' + numero + '_' + jour).value=0;
	else {
		for (j = 0; j < timeslots.length; j++) {
			if (timeslots[j].id == numero) {
				timeslots[j].setQuota(quota_int);
			}
		};

		 construire_semaine();
	}
}

function delete_plage(numero) {
	for (j = 0; j < timeslots.length; j++) {
		if (timeslots[j].id==numero)
			timeslot_pos = j;
	}
	timeslots.splice(timeslot_pos, 1);
	if (dd('timeslot' + numero)!=null)
		$('#timeslot' + numero).remove();
		
	for (var tjour = 1; tjour <= 7; tjour++) {
		var nb_plage = 0;
	    for (j = 0; j < timeslots.length; j++) {
	    	if (timeslots[j].days[tjour]==1) {
	    		nb_plage++;
			}
		}
		if (nb_plage == 0) {
	        setDayStatus(tjour, 0);
	    }
	}
     construire_semaine();
     insertDayTimeslots();
}

function disable_plage(jour, numero) {
	console.log('disable_plage:'+jour+numero);
    var nb_plage = 0;
    for (j = 0; j < timeslots.length; j++) {
    	if (timeslots[j].days[jour]==1) {
    		nb_plage++;
    		if (timeslots[j].id==numero)
    			timeslot_pos = j;
		}
	}
	
    if (nb_plage == 1) {
        setDayStatus(jour, 0);
    }
    	
		timeslots[timeslot_pos].days[jour] = 0;
		timeslots[timeslot_pos].save();
		
        construire_jour(jour);
    
    insertDayTimeslots();
}

function enable_plage(jour, numero) {
	console.log('enable_plage:'+jour+numero);
    	for (j = 0; j < timeslots.length; j++) {
    		if (timeslots[j].id==numero)
    			timeslot_pos = j;
		}
		timeslots[timeslot_pos].days[jour] = 1;
		timeslots[timeslot_pos].save();
		
        construire_jour(jour);
    
    setDayStatus(jour, 1);
    insertDayTimeslots();
}




