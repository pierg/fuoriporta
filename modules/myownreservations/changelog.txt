v4-2-0
2018-04-15
------------------------------------------------------
- Corrected carrier disabled when reservation dissabled on PRO version.
- Corrected Flter minimal planning days depending on place
- Corrected day counted twice on multiple period discount
- Corrected stay tax not checked in resv rules, and multiply qty always checked
- Corrected mutiple refresh on product qty change
- Corrected Google Calendar not updated for external reservations
- Corrected Upgrade quantity of products with variable occupation 
- Corrected Guest ignore qty option ignored on planning
- Corrected discount that apply on whole order with advance on PS 1.7 
- Corrected Manual reservation creation wasn't saving cart on right shop
- Corrected Cart context is reset on validate order because of some payment modules
- Corrected Round problem that can show advance/deposit when not set.
- Corrected Custom pagination ignored
- Corrected marketplace assignation can't update seller
- Corrected Enable/ Disable on PS 1.7
- Corrected quickview add to cart button when resa in session
- Added Deeper integration with admin menus.
- Added timeslots/object relation added to reservation admin
- Added admin dashboard for checkin/out
- Added Minimum Order amount for deposit application
- Added Automatic stock assignation
- Added Same period product restriciton (in this case reservation period is displayed from orders list)
- Added option on marketplace to disable time slots or availabilities edition from customer account
- Added taking holidays in price rules length count
- Added Day view with 16 stock items column or less
- Added Drag & Drop on reservation for stock item assignation
- Added On Order creation option to send payment link for customer
- Enhanced Admin mobile use

v4-1-1
2017-12-01
------------------------------------------------------
- Corrected Stock view bulk actions was hidden
- Corrected Conflict with length selection and new option to allow resa outside timeslots
- Price rule amount field hidden
- Added Extension activation/deactivation
- Added Time slice generation 
- Added Fixed length front display enhanced
- Added Define to set a or/and on recurring conditions
- Added Week condition to recurring price rule
- Added Option to apply price rule on one product only
- Enhanced Options/occupation of extensions
- Corrected Time slices availability bug on ressources time slots when length not fixed
- Corrected Order status change bug when product image is missing

v4-1-0
2017-11-04
------------------------------------------------------
- Corrected Save and not stay don't save attendees details
- Corrected List view default selection
- Corrected Advance x2 on checque payment confirmation
- Corrected Total invoice now showsorder total and one more line display the advance amount
- Corrected widgetTop init don't remove end sel for unique sel at init
- Corrected MultiShop check on widgetTop
- Corrected Delete only one reservation of cart was deleting all resa products
- Corrected Module doesn't appear in prodcuts admin if disabled on the shop
- Corrected product by length save bug
- Corrected pack with multiple qty and pack with element in another pack
- Corrected Set custom reservation price from order creation
- Corrected Removed length verification on unlimited length
- Added Comment field and select field changed into text if no values
- Added Option to display price rules legend
- Added Reservation details (attendees) edition
- Added 2 columns in grid view reservation amount and ref
- Added Grouped view of reservations by product
- Added Reservation items extraction or deletion on PRO version
- Added Reservations can be assigned to employees
- Added You can reinit user password at creation from admin
- Added Extensions details in resa confirmation email
- Added Option to count combination price once when resa price depends on length
- Added Day view if object filter is used
- Added Min hours and max hours week view display changed of family filter used
- Added Reservation re/schedule date picker start and end auto populate for start < end
- Added Extension place to price rules and timeslots
- Added Extension place edition on reservations admin
- Added Product page layout options with a mini and popup calendar
- Added Product Prices sets legend
- Added Availability edition from reservation window
- Added View of whole family availabilities on planning view if family filter used
- Added Prices by rates added
- Added History price rule
- Added Timeslots with week restrictions
- Added Option to group time slots by day instead of doing it when more than 3
- Added Option to allow selection time slot end after current day
- Enhanced Clearer Reservation rules config

v4-0-4
2017-03-15
------------------------------------------------------
- Corrected Availabilities display when creating reservation
- Corrected Night selection

v4-0-3
2017-02-25
------------------------------------------------------
- Corrected Problem when saving reservation rules on std version
- Corrected Checkout steps modification options ignored
- Corrected Length disabled slot or holidays option not saved in std version

v4-0-2
2017-02-15
------------------------------------------------------
- Compatibility with PS 1.7
- Warning .tpl .js .css file reorganization
- Improved Reservation confirmation sent by email with objects at reservation validation
- Improved Reservation admin popup style
- Added Checkout menu in order details to quickly reschedule reservations or split them
- Added Order creation with reservation add for PRO version
- Added Tab translations.
- Added Column compatibility and rewritten name of my account reservation page
- Added Availability edition
- Added widget home manage availabilities on specific products 
- Added Automatic /cache/class_index delete
- Added WebServices for Reservations
- Added Display price rules legend under planning
- Added Display reduced price also for reservations
- Added Quantity price rule
- Added Option to keep the price rule with best rate.
- Added Option to disable advance on physical store
- Corrected taking quantity into account for specific product price
- Corrected removed reservations head on order conf if no resas
- Corrected Week availabilities not applied on availability start week
- Corrected Start planning view hide period that can't have a end.
- Corrected Search widget using datepicket and not our calendar
- Corrected Duplication of reservation lines for combination
- Corrected Price by week count null for first day when selection by day
- Corrected Discount repartition on advance and balance

v3-5-1
2016-07-15
------------------------------------------------------
- Corrected stock order and family filter
- Corrected extenal reservation created with bad attribute when a product has no attribute
- Corrected order_name empty in msg
- Corrected Wrong cart details with price rules on ajax refresh
- Added Removed out of stock email
- Enhanced floating planning header is only for more than 3 timeslots sel
- Added Disabled products and categories from other families on product selection
- Added Reservation rules filter when shop is selected
- Enhanced removed warning when url is distinct than shop url

v3-5-0
2016-07-15
------------------------------------------------------
- Corrected filter empty months due to unavailabilities
- Added starter tutorial

v3-4-4
2016-07-2
------------------------------------------------------
- Corrected length calculated with on day more
- Corrected case when start time is later that end time for night purpose
- Added minoring pack qties with min prod qty inside pack
- Corrected also checking pack avaialability with pack items resas
- Added Linked product are added under added to cart popup

v3-4-3
2016-05-10
------------------------------------------------------
- Corrected Price display option
- Corrected Price rules multiple product without family
- Added times cut option
- Corrected widget colum selection display when end on next page
- Added Can directly add to cart linked products
- Added Customized error message when reservation can't be added to cart
- Check bounds are not in holidays if resa allow to run on it
- Availabilities created from product page are affected to right familly
- taking new pack option into account to check each pack items

v3-4-2
2016-04-06
------------------------------------------------------
- Corrected Mutiple products in price rule
- Corrected home widget keeping old start sel
- Corrected stay on same calendar page on qty or attribute change
- Display reservation button when order edition disabled
- Enhanced month view

v3-4-1
2016-04-06
------------------------------------------------------
- Added Option to display product per availability instead of reverse
- Corrected including disabled days in table rate bu length
- Corrected Customisation qty on order-details from cust
- Corrected Order update when changing resa line
- Corrected Invoice not updated due to invoice id shift
- corrected Adding product to origninal cart when adding new resa to existing order

v3-4-0
2016-01-06
------------------------------------------------------
- Added Can reschedule cart
- Added Multishop restriction on reservation rules
- Corrected trad labels js error
- Corrected count days instead of length with 00:00
- Enhanced help perf

v3-3-2
2016-01-06
------------------------------------------------------
- Reduction display
- Corrected problem when saving start/end selection on old family
- Corrected problem with availability count on some cases
- Enhanced reseting widget selection on product plannign if unavailable

v3-3-1
2015-11-10
------------------------------------------------------
- Added new responsive widget on home page
- Added Widget can select start and length
- Added Widget can be hidden in categories without rental products
- Added Option to exceed reservation window with resa length
- Added define to get next available day for frequency restrict
- Added Deposit calc per product qty
- Enhanced checkout table elements
- Enhanced Option for selection mode start/end, start/length, multiple, unique
- Enhanced Constraint display between seletion mode and length option and length and price set option
- Enhanced performance when ignoring disabled days or holidays
- Enhanced removed PS 1.5 compatibility mode display
- Enhanced Filter first day even with a lot of time slots
- Enhanced Administration optimisation module data is not loaded anymore
- Corrected order refresh after reservation modification
- Corrected month occupation

v3-3-0
2015-10-15
------------------------------------------------------
- Corrected Hide time unit when reservation price without length
- Categories used in other reservaton rules are diabled
- Enhanced integrated help
- Added New optionnal length, to select start or stop or both, with customized labels

v3-2-2
2015-08-01
------------------------------------------------------
- Corrected reservation unit display on grid/list change
- Corrected january week from the menu
- Corrected period price rule on overall 

v3-2-1
2015-07-01
------------------------------------------------------
- Corrected Compatibility with myCollectionPlaces
- Added Ignore Advance on maintenance IP

v3-2
2015-06-15
------------------------------------------------------
- Corrected Resa Popup Buttins icon
- Improved Back planning design
- Corrected Bad Optimisation in month view
- Corrected Product attribute lost in resa creation
- Corrected Show availabilities qty on home widget

v3-1-3
2015-05-15
------------------------------------------------------
- Corrected Deletion of stock items
- Corrected Barcode print with new pdf library
- Corrected Link on home widget availabilities

v3-1-2
2015-03-01
------------------------------------------------------
- Corrected Reservation in cart not taken into account when checking product list
- Corrected Fixed advance amount not taken into acount by bloccart module
- Corrected Min Price display whan price rules on products only (not category)
- Added Translation for deposit

v3-1-1
2015-01-26
------------------------------------------------------
- Added getProductTaxesBreakdown overrides change for ps 1.6.0.11
- Added Multiple email separated by a ; for prod familly notification
- Added New option for displaying price
- Added Discount displayed with discount label style on cart
- Corrected Reservation start the same day for day selection

v3-1-0
2015-01-04
------------------------------------------------------
- Added Reservation quantity option
- Added Ignore step exlusively for reservations option

v3-0-9
2014-10-23
------------------------------------------------------
- Added Overides backup
- Corrected reservation between shift bug
- Corrected 404 error with home widget
- Corrected wraping fee with paypal

v3-0-8
2014-10-23
------------------------------------------------------
- Corrected Variable product to taken into account
- Corrected week management when end day of week less than start

v3-0-7
2014-08-12
------------------------------------------------------
- Changed reduction that apply on all reservation will be caclulated after other prices rules
- Corrected week display shift on month
- Corrected multiple availability problem

v3-0-6
2014-06-13
------------------------------------------------------
- Corrected Currency display probem in order details
- Corrected 0% advance malfunction
- Corrected Planning day selector not working
- Corrected availabilities delete button 
- Corrected Can't hide column widget
- Added PS1.6 list button

v3-0-5
2014-06-06
------------------------------------------------------
- Corrected Some settings problems
- Availability on 2 days when reservation without length
- Corrected product qty check before add to cart disabled
- Corrected hover removed on home widget

v3-0-4
2014-06-06
------------------------------------------------------
- Corrected Admin popup creation error
- Corrected some admin translations
- Corrected warning on ress and icalendar notification

v3-0-3
2014-05-10
------------------------------------------------------
- Corrected Allow manual order creation on reserved products
- Simplified widget options
- Compatibility with Prestashop 1.6

v3-0-2
2014-04-10
------------------------------------------------------
- Corrected Advance deposit save in std version
- Corrected javascript: replaced by onclick
- Corrected Duplicate line for product in order details
- Corrected New cart problem when login with a extisting cart
- Corrected Taxes calculation on invoice
- Improved No more execute permissions needed on ajax.php
- Corrected checkbox not saved and hide step 2 trouble
- Corrected product order not following config
- Corrected Unlimioted resa length problem

v3-0-1
2014-04-02
------------------------------------------------------
- Corrected Price calculation with new price options
- Corrected Home widget display
- Corrected Period selector in calendar not saving resa start
- Corrected Date in other languages not formated correctly
- Added Price Sets per day of year
- Corrected Planning doesn't filter reservation that is impossible (after another resa)

v3-0-0
2014-03-10
------------------------------------------------------
Features
- Evolution of bookings rules in 4 steps (information , planning, payment, notification) and product selection
- Introduction Product type : real , virtual, variable, mutual and shared
- New notifications : email, iCalendar , RSS, SMS reminder on the end of the command
- New duration rules and time unit dissociation for calculating the rate
- New calculation methods for the deposit and the beginning of the schedule
Appearance
- New templates optimized level of ergonomics and performance but also easily modifiable
- Rules price with customizable and applicable style has a selection of produce
- function and style of widgets are now unified, adding the search mode
- Display Option for the wording of the reservation and schedule boxes
Technical
- Reorganization of configuration objects floating menu and new options
- rewritten booking engine to manage the occupationfaster
- Preparation module extensions for the types of selection and notifications
- Better integration of the help and warning messages on the configuration
Pro Version
- New view to better manage the stock of products even without a serial number
- Now possible to apply a price rule to a complete product family
- Reservation status, advance and deposit are now managed by family
- New product type: shared and mutual

v2-2-5
2014-01-10
------------------------------------------------------
- Corrected Specific price were not taken into account in some cases
- Warning on email send
- Corrected bad week price for week number <10
- Corrected search on home category displaying none active products
- Added style on period selected display
- Prevent enter key pressed on product page
- Corrected default product order on category
- Start date wasn't same for widget

v2-2-4
2013-10-10
------------------------------------------------------
- Corrected Problem on dates view when min & max length = 1
- Corrected Availabilities were ignored on widget period selection
- Corrected Resa price display on unique selection

v2-2-3
2013-09-03
------------------------------------------------------
- Corrected Product was not available for resa when order out of stock allowed
- Corrected multiple selection change quantity reset
- Corrected pagination in categories
- Corrected currency convert for deposit, price sets and rules
- Added Related products "add to cart" buttons are removend if product are in rent.
- Corrected Saving pricesets at each product admin details save

v2-2-2
2013-07-08
------------------------------------------------------
- Corrected Search widget calendar style on PS1.4
- Corrected Category wasn't displaying products non reservable
- Added New fields customer name, carrier, advance amout and type for external orders
- Improved stock management with new details and filters, and table print.
- Improved performances for a huge list of timeslots
- Improved Availbility period selection in product tab

v2-2-1
2012-06-29
------------------------------------------------------
- Corrected Can now select awaiting paiement statuses
- Corrected Reservations display on carts details on admin
- Added planning reload every minute

v2-2-0
2012-06-24
------------------------------------------------------
- Added price list management from product administration
- Added display or remaining palces on planning
- Added multiple selection on planning and saving current period displayed
- Added Renew order compatibility (reservations not avaialbable anymore are not renewed)
- Added Price rule for unit price
- Added New deposit calculation modes (and an option to invoice it or not)
- Added Search widget for all reservable products
- Added Resevation edition popup form planning and order detail
- Added Multiple admin users selection on a product familly
- Corrected multi shop management for same product id
- Corrected week planning with same day of week as start and end
- Improved Advance/Balance display on order detail and checkout
- Improved Front planning start at first available date
- Improved Front planning save current period displayed
- Improved Ajax admin planning with a grid view instead of day view
- Improved Drag and drop reservation reschedule or create on admin

v2-1-7
2012-05-02
------------------------------------------------------
- Corrected recurring price rule wasn't working

v2-1-5
2012-04-15
------------------------------------------------------
- Corrected week count with same days
- Corrected timeslot configuration

v2-1-4
2012-03-20
------------------------------------------------------
- Added Availabilities management from product administration
- Compatibility with Product override and PS1.5.3.1
- Corrected Default timeslot is used when no time slots on admin
- Corrected Cart summary details table display and calculation
- Corrected Refresh on combination change with combo box
- Corrected Stock compensation on id order state greater than 9
- Modified Exclude last timeslot/day option only for price
- Added Stock management with option ingore stock attribute

v2-1-0
2012-03-05
------------------------------------------------------
- Added multiple store management
- Added quick enable/disable for price rules and timeslots
- Added Minimal planning type with only 2 combo box
- Added portugese and corean translations
- Added Could add more than one validation status
- Added Could change quantity of reservations on cart summary
- Added Back-end cart displays reservations
- Added Could change or hide price unit
- Improved Day planning type now display inline timeslots
- Improved Clearer config 
- Improved Cart summary display a line by reservation
- Improved Product list can hide unavailable reservations
- Corrected planning calculation different in product or colum
- Corrected stock management with PS1.5 when canceling an order

v2-0-7
2012-01-05
------------------------------------------------------
- Corrected Bug with deposit when reservation price was equal to the product price
- Improved Caution line forced to display in blockcart when deposit > 0
- Corrected bad product listing in module config
- Added Cannot add reservation product without dates
- Corrected Length price rule reduction calculation

v2-0-5
2012-12-05
------------------------------------------------------
- Improved Javascript cart add and planning refresh

v2-0-4
2012-11-25
------------------------------------------------------
- Added Length and session dates management on search and tags
- Correct cart refresh with reservation dates forced for PS1.5
- Improved Back-office reservation display
- Added Only one date selection with 1 day max length

v2-0-3
2012-11-15
------------------------------------------------------
- Improved of memory usage and sql requests amount
- Improved display of reservation unit
- Added Subcategories check
- Added German translation

v2-0-2
2012-11-05
------------------------------------------------------
- Added Availability to add reservation unit to widget prices
- Corrected Cannot save price settings
- Corrected Bad family mgmt on admin for PRO version
- Corrected bad planning week end selection problem introduced in 2.0

v2-0-1
2012-10-25
------------------------------------------------------
- Corrected ui.calendar javascript calendar included in the header
- Corrected Tab missing can be added after the install
- Corrected Somme overrides for the 1.4 wasn't compatible with the 1.5

v2-0-0
2012-10-23
------------------------------------------------------
- Improved Module architecture has been redesigned
- Improved Planning design
- Compatibility Prestashop 1.5

v1-5-4 
2012-02-12
------------------------------------------------------
- Improved Performances

v1-3-3 
2011-08-16
------------------------------------------------------
- Added Option to show or hide 'add to cart' buttons on widget
- Added Fixed reservation advance
- Added Add a reservation to cart without the Ajax cart widget
- Added Timeslot precision on holiday management
- Added Option to hide Reservation price
- Added Option to manage product family per product and not per category
- Corrected bad display of price rules options in their list on backend
- Corrected Week end wasn't displayed correctly on some cases
- Corrected Day occupation per month wasn't showing percents
- Improved Easier availabilities management 
- Improved Also checking child categories of category selected for reservations
- Improved module category from logistic to checkout

v1-3-2
2011-08-04
------------------------------------------------------
- Corrected Purchase and reservation of same product wasn't displayed properly
- Added a parameter to let a product handle multiple reservation for 1 quantity
- Added Csv export from reservations planning
- Compatibility with CheckoutFields on Export
- Corrected product family filter does not apply on back-end
- Corrected bad language for jquery calendar on widget
- Corrected qty update fix

v1-3-1
2011-07-11
------------------------------------------------------
- Corrected bad week end display on front end

v1-3
2011-06-30
------------------------------------------------------
- Added New prices rules, for fixed and increase price
- Added Registration key system
- Added New reservation units and views per week and per month
- Improved New option for choice of user selection on planning
- Improved Replanification of reservation is easier

v1-2
2011-06-02
------------------------------------------------------
- Corrected planning by month wasn't going through december
- Corrected Allowing reservation without shift between reservations in some cases
- Added Cart verification to remove no longer available reservations

v1-1-2
2011-05-14
------------------------------------------------------
- Corrected CMS page conflict with widget on left side
- Corrected Attribute combinaison reservation not taken into account with "ignore stock attribute" option
- Corrected Bad widget javascript date treatment in comes cases

v1-1-1
2011-05-10
------------------------------------------------------
- Bug with length price rule
- Corrected CSS widget doest not apply in some cases
- Corrected Reservation unavailable for some days after a reservation
- Added Unit price displayed on products display by tags
- Compatibility with layered blocks


v1-1-0
2011-01-15
------------------------------------------------------
- Corrected last time slots of planning wasn't disabled correctly with min reservation period
- Corrected reservation planning was refreshed before removal of reservation in cart
- Added compatibility with smarty 2
- Added Widget for selecting reservation start and end dates
- Added Categories view show product price and availability for reservation selected in widget
- Corrected Categories view showing price per day or per time slot
- Added Availabilities management by product instead of general holidays
- Added A gap between each reservation can be forced
- Improved Estimation of price and discounts can be displayed on start date selection
- Improved planning time slot selection

v1-0-0
2011-08-12
------------------------------------------------------
Initial version