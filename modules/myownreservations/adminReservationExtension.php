<?php
/*
* 2010-2012 LaBulle All right reserved
*/

require_once(_PS_MODULE_DIR_ . "myownreservations/myownreservations.php");
require_once(MYOWNRES_PATH . "controllers/admin.php");
require_once(MYOWNRES_PATH . "controllers/planning.php");
require_once(MYOWNRES_PATH . "controllers/ajax_admin.php");


class adminReservationExtension extends AdminController
{
	var $module = "myownreservations";
	var $name = "myownreservations";
	var $multishop_context_group;
	var $multishop_context;
	var $moduleObj;
	var $ruleId;

    public function __construct()
    {
        global $cookie, $_LANGADM;

        $this->moduleObj = Module::getInstanceByName('myownreservations');
        $this->moduleObj->checkPro();
        //if (!is_file(_PS_MODULE_DIR_ ."myownreservations/pro/products.php")) 
        $this->multishop_context = true;
        myOwnReservationsController::_setInfos ($this->moduleObj);
        myOwnReservationsController::_construire($this->moduleObj);
		$this->display = 'view';
		$this->class_name = 'adminReservationExtension';
		$this->bootstrap=true;
		$this->doActions();
        parent::__construct();
    }
    
    public function displayErrors() {
	    foreach($this->_errors as $err) echo $err;
    }
    
    public function initPageHeaderToolbar()
	{
		global $cookie;
		$tab = Tools::getValue('myOwnResTab');
		$ctrl = strtolower(Tools::getValue('controller'));
		$field='';
		if ($tab=='timeslots')
			$fieldl = 'TimeSlot';
		if ($tab=='pricesrules') 
			$field = 'Pricesrule';
		if ($tab=='availability') 
			$field = 'Availability';
		if ($ctrl=='adminreservationrules') {
			$tab='products';
			$field = 'Product';
		}
		
		if ($this->ruleId > 0) $this->page_header_toolbar_title = ucfirst(myOwnLang::$object[MYOWN_OBJ::RESVRULE]).' #'.$this->ruleId;
		else $this->page_header_toolbar_title = ucfirst(myOwnLang::$object[MYOWN_OBJ::RESVRULE]);

		$this->page_header_toolbar_btn = myOwnReservationsController::getButtonsList( $this->moduleObj, $this->ruleId);

		if (_PS_VERSION_ >= "1.6.0.0") parent::initPageHeaderToolbar();
		$this->context->smarty->assign('help_link', null);
	}
	

	
	public function doActions()
	{
		global $cookie;
		$obj = $this->moduleObj;

		$operation='';
		$insertedId=0;
		$objet = $obj->l('The reservation rules', 'adminreservationrules');
		$male = true;
		$errors=array();$plur=false;
		$isproversion=is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");
		
		$this->ruleId = intval(Tools::getValue('editProduct', -1));
		if (!$isproversion) $this->ruleId=-1;
		if ($this->ruleId==0) $this->ruleId = $insertedId;
		
	} 


    public function renderView()
	{
		global $cookie;
		$content='';
		$obj = $this->moduleObj;
		$cookie = $obj->getContext()->cookie;
		
		if (Tools::getIsset('id_tab')) {
			$sql = 'SELECT * FROM `'. _DB_PREFIX_ .'tab_lang` WHERE `id_lang` = 0 && `id_tab` = '.Tools::getValue('id_tab');
			$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($sql);
		}
		if (array_key_exists(0, $result) && isset($result[0]['name'])) 
			$myOwnResTab = $result[0]['name'];
		else $this->errors[] = Tools::displayError('Menu not found');
		
		foreach ($obj->_extensions->getExtByType(extension_type::OBJECT) as $ext)
				if ($myOwnResTab==$ext->name) 
					$content .= $ext->getContent($obj);
		
					
		
		if (_PS_VERSION_ < "1.6.0.0") {
			$this->initPageHeaderToolbar();
			$buttons = MyOwnReservationsUtils::getButtonsList($this->page_header_toolbar_btn, $this->ruleId);
		} else $buttons='';
		//$buttons = myOwnReservationsController::getButtons($cookie, $obj, $this->ruleId, false, 'Product',  'products');
		
		foreach($this->errors AS $error) {
			$htmlOut .= myOwnUtils::displayError($error);
		}
		foreach($this->confirmations AS $confirmation) {
			$htmlOut .= myOwnUtils::displayConf($confirmation);
		}
		$this->errors=array();
		$this->confirmations=array();
		
		$parent = $obj->l('Catalog', 'adminReservationRules');
		if ($this->ruleId==-1) $title = ucfirst(myOwnLang::$object[MYOWN_OBJ::RESVRULE]);
		if ($this->ruleId==0) $title = ucfirst(myOwnLang::$actions[MYOWN_OPE::FNEW]).' '.ucfirst(myOwnLang::$objects[MYOWN_OBJ::RESVRULE]);
		if ($this->ruleId>0) $title = ucfirst(myOwnLang::$object[MYOWN_OBJ::RESVRULE]).' #'.$this->ruleId;
		
		/*$header = MyOwnReservationsUtils::displayIncludes();
		$header .= myOwnReservationsController::getModuleHeader($obj, $cookie);
		$header .= $htmlOut;;*/
		$header .= MyOwnReservationsUtils::displayStarter($obj);
		if (_PS_VERSION_ < "1.6.0.0") $header .= myOwnReservationsController::getMenuHeader($obj, $parent, $title, $buttons);
		$isproversion=is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");
		//if ($this->ruleId>-1 or !$isproversion) $content = myOwnReservationsProductsController::edit($cookie,$obj,$this->ruleId);
		//else $content = myOwnReservationsProductsController::show($cookie,$obj);
		
		return $header.$content;
  	}
  
}

?>