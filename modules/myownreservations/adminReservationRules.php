<?php
/*
* 2010-2012 LaBulle All right reserved
*/

require_once(_PS_MODULE_DIR_ . "myownreservations/myownreservations.php");
require_once(MYOWNRES_PATH . "controllers/admin.php");
require_once(MYOWNRES_PATH . "controllers/planning.php");
require_once(MYOWNRES_PATH . "controllers/ajax_admin.php");


class adminReservationRules extends AdminController
{
	var $module = "myownreservations";
	var $name = "myownreservations";
	var $multishop_context_group;
	var $multishop_context;
	var $moduleObj;
	var $ruleId;

    public function __construct()
    {
        global $cookie, $_LANGADM;

        $this->moduleObj = Module::getInstanceByName('myownreservations');
        $this->moduleObj->checkPro();
        //if (!is_file(_PS_MODULE_DIR_ ."myownreservations/pro/products.php")) 
        $this->multishop_context = true;
        myOwnReservationsController::_setInfos ($this->moduleObj);
        myOwnReservationsController::_construire($this->moduleObj);
		$this->display = 'view';
		$this->class_name = 'adminReservationRules';
		$this->bootstrap=true;
		$this->doActions();
        parent::__construct();
    }
    
    public function displayErrors() {
	    foreach($this->_errors as $err) echo $err;
    }
    
    public function initPageHeaderToolbar()
	{
		global $cookie;
		$tab = Tools::getValue('myOwnResTab');
		$ctrl = strtolower(Tools::getValue('controller'));
		$field='';
		if ($tab=='timeslots')
			$fieldl = 'TimeSlot';
		if ($tab=='pricesrules') 
			$field = 'Pricesrule';
		if ($tab=='availability') 
			$field = 'Availability';
		if ($ctrl=='adminreservationrules') {
			$tab='products';
			$field = 'Product';
		}
		
		if ($this->ruleId > 0) $this->page_header_toolbar_title = ucfirst(myOwnLang::$object[MYOWN_OBJ::RESVRULE]).' #'.$this->ruleId;
		else $this->page_header_toolbar_title = ucfirst(myOwnLang::$object[MYOWN_OBJ::RESVRULE]);

		$this->page_header_toolbar_btn = myOwnReservationsController::getButtonsList( $this->moduleObj, $this->ruleId);

		if (_PS_VERSION_ >= "1.6.0.0") parent::initPageHeaderToolbar();
		$this->context->smarty->assign('help_link', null);
	}
	

	
	public function doActions()
	{
		global $cookie;
		$obj = $this->moduleObj;

		$operation='';
		$insertedId=0;
		$objet = $obj->l('The reservation rules', 'adminreservationrules');
		$male = true;
		$errors=array();$plur=false;
		$isproversion=is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");
		
		//UPDATE TUTO STEPS
		if (Tools::getIsset('current_step')) {
			$current_step = Tools::getValue('current_step');
			Configuration::updateValue('MYOWNRES_STARTER', (int)$current_step);
		}
			
		if(isset($_POST['idProduct'])){
			$tempProduct = myOwnReservationsProductsController::getFromPost($obj, $cookie);
			$msg='';
			if ($tempProduct->_reservationPriceType==reservation_price::TABLE_RATE) myOwnReservationsPriceSetsController::editAction($obj, $msg, $tempProduct);
			
			//save extensions options
			foreach ($obj->_extensions->getExtByType(extension_type::NOTIFICATION) as $ext)
				if (in_array($ext->id, $tempProduct->ids_notifs))
					if (method_exists($ext, "execConfigNotif"))
						$ext->execConfigNotif($obj, $tempProduct);
			
			foreach ($obj->_extensions->list as $ext)
				if (method_exists($ext, "execSaveProductRule"))
					$ext->execSaveProductRule($obj, $tempProduct);
			
			$male = false;
			$plur=true;

			if ($tempProduct->sqlId==-1 or !$isproversion) {
				$operation = myOwnLang::getOperation($obj, MYOWN_OPE::UPDATE, false, false);
				$result = $tempProduct->save();
				$insertedId = 0;
				$obj->_products->list[0]=$tempProduct;
			} else if ($tempProduct->sqlId==0) {
					$operation = myOwnLang::getOperation($obj, MYOWN_OPE::CREATE, false, false);
					$result = $tempProduct->sqlInsert();
					$inserted = Db::getInstance()->ExecuteS("SELECT LAST_INSERT_ID() AS ID;");
					$insertedId = $inserted[0]['ID'];
					$tempProduct->sqlId = $insertedId;
					$obj->_products->list[$insertedId]=$tempProduct;
			} else {
				$operation = myOwnLang::getOperation($obj, MYOWN_OPE::UPDATE, false, false);
				$result = $tempProduct->sqlUpdate();
				$obj->_products = new myOwnProductsRules();
			}
			$operation = myOwnLang::getOperation($obj, MYOWN_OPE::UPDATE, false, false);
		}

		if(isset($_GET['deleteProduct']) && !empty($_GET['deleteProduct'])) {
			$male = false;
			$tempProduct = new myOwnProductsRule();
			$tempProduct->sqlId = $_GET['deleteProduct'];
			$operation = myOwnLang::getOperation($obj, MYOWN_OPE::DELETE, false);
			$result = $tempProduct->sqlDelete();
			foreach ($obj->_timeSlots->list as $tempTimeslot)
				if ($tempTimeslot->id_family == $tempProduct->sqlId) 
					$tempTimeslot->sqlDelete();
			$obj->_products = new myOwnProductsRules();
		}
		
		if(isset($_GET['copyProduct'])){
			$objet = $obj->l('The reservation rules', 'adminReservationRules');
			$cpy=(int)$_GET['copyProduct'];
			$tempProduct = clone $obj->_products->list[$cpy];
			$tempProduct->name .= ' '.$obj->l('copy', 'adminReservationRules');
			$tempProduct->sqlId=0;
			$tempProduct->ids_categories='';
			$tempProduct->ids_products='';
			$operation = myOwnLang::getOperation($obj, MYOWN_OPE::COPY, false);
			$result = $tempProduct->sqlInsert();
			$inserted = Db::getInstance()->ExecuteS("SELECT LAST_INSERT_ID() AS ID;");
			$insertedId = $inserted[0]['ID'];

			foreach ($obj->_timeSlots->list as $timeslot) {
				if ($timeslot->id_family == $cpy) {
					$tempTimeslot = clone $timeslot;
					$tempTimeslot->sqlId=0;
					$tempTimeslot->id_family = $insertedId;
					$tempTimeslot->sqlInsert();
				}
			}
			$obj->_products = new myOwnProductsRules();
		}
		// ADD, UPDATE & DELETE TIME SLOT
		//-----------------------------------------------------------------------------------------------------------------
		
		if(isset($_POST['idTimeSlot'])){
			$tserrors = myOwnReservationsTimeslotsController::check($obj);
			if ($tserrors==array()) {
				$tempTimeSlot = new myOwnTimeSlot();
				$startTime = MyOwnCalendarTool::getHourPost('startTime');
				$endTime = MyOwnCalendarTool::getHourPost('endTime');
				
				$tempTimeSlot->startTime = $startTime;
				$tempTimeSlot->endTime = $endTime;
				$tempTimeSlot->name = $_POST['timeSlotName'];
				//$tempTimeSlot->quota = $_POST['timeslotQuota'];
				$tempTimeSlot->type = $_POST['timeSlotType'];
				$idTimeSlot=0;
				if (isset($_POST["idTimeSlot"])) $idTimeSlot = intval($_POST["idTimeSlot"]);
				$tempTimeSlot->sqlId = $idTimeSlot;
				$tempTimeSlot->id_family = Tools::getValue('timeSlotFamily', 0);
				$tempTimeSlot->id_product = Tools::getValue('timeSlotProduct', 0);
				
				for($i=1; $i<8; $i++) {
					$tempTimeSlot->days[$i] =  intval(isset($_POST['day'.$i]) && $_POST['day'.$i]==1);
				}
				
				$objet = $obj->l('The time slot', 'controller');
				if ($tempTimeSlot->sqlId==0) {
					$operation = myOwnLang::getOperation($obj, MYOWN_OPE::CREATE);
					$result = $tempTimeSlot->sqlInsert();
					$inserted = Db::getInstance()->ExecuteS("SELECT LAST_INSERT_ID() AS ID;");
					$insertedId = $inserted[0]['ID'];
				} else {
					$operation = myOwnLang::getOperation($obj, MYOWN_OPE::UPDATE);
					$result = $tempTimeSlot->sqlUpdate();
				}
			} else $this->errors += $tserrors;
		}
		if(isset($_GET['deleteTimeSlot']) && !empty($_GET['deleteTimeSlot'])) {
			$objet = $obj->l('The time slot', 'adminReservationRules');
			$tempTimeSlot = new myOwnTimeSlot();
			$tempTimeSlot->sqlId = $_GET['deleteTimeSlot'];
			$operation = myOwnLang::getOperation($obj, MYOWN_OPE::DELETE);
			$result = $tempTimeSlot->sqlDelete();
		}
		if(isset($_GET['disableTimeSlot'])) {
			$objet = $obj->l('The time slot', 'adminReservationRules');
			$tempTimeSlot = $obj->_timeSlots->list[$_GET['disableTimeSlot']];
			$tempTimeSlot->days[$_GET['day']] = 0;
			$days=myOwnLang::getDaysNames($cookie->id_lang);
			$operation = myOwnLang::getOperation($obj, MYOWN_OPE::DISABLE).' '.$obj->l('on', 'adminReservationRules').' '.$days[$_GET['day']];
			$result = $tempTimeSlot->sqlUpdate();
		}
		if(isset($_GET['enableTimeSlot'])) {
			$objet = $obj->l('The time slot', 'adminReservationRules');
			$tempTimeSlot = $obj->_timeSlots->list[$_GET['enableTimeSlot']];
			$tempTimeSlot->days[$_GET['day']] = 1;
			$days=myOwnLang::getDaysNames($cookie->id_lang);
			$operation = myOwnLang::getOperation($obj, MYOWN_OPE::ENABLE).' '.$obj->l('on', 'adminReservationRules').' '.$days[$_GET['day']];
			$result = $tempTimeSlot->sqlUpdate();
		}

		$male=true;
		if ($operation != "") {
			if ($result) $this->confirmations[] = $objet.' '.myOwnLang::getResult($obj, true, !$plur).' '.$operation;
			else $this->errors[] = $objet.' '.myOwnLang::getResult($obj, false, !$plur).' '.$operation.' : '.Db::getInstance()->getMsgError();

			myOwnReservationsController::_construire($obj, true);
		}
		
		$this->ruleId = intval(Tools::getValue('editProduct', -1));
		if (!$isproversion) $this->ruleId=-1;
		if ($this->ruleId==0) $this->ruleId = $insertedId;
		
	} 


    public function renderView()
	{
		global $cookie;
		$htmlOut='';
		$obj = $this->moduleObj;
		if (_PS_VERSION_ < "1.6.0.0") {
			$this->initPageHeaderToolbar();
			$buttons = MyOwnReservationsUtils::getButtonsList($this->page_header_toolbar_btn, $this->ruleId);
		} else $buttons='';
		//$buttons = myOwnReservationsController::getButtons($cookie, $obj, $this->ruleId, false, 'Product',  'products');
		
		foreach($this->errors AS $error) {
			$htmlOut .= myOwnUtils::displayError($error);
		}
		foreach($this->confirmations AS $confirmation) {
			$htmlOut .= myOwnUtils::displayConf($confirmation);
		}
		$this->errors=array();
		$this->confirmations=array();
		
		$parent = $obj->l('Catalog', 'adminReservationRules');
		if ($this->ruleId==-1) $title = ucfirst(myOwnLang::$object[MYOWN_OBJ::RESVRULE]);
		if ($this->ruleId==0) $title = ucfirst(myOwnLang::$actions[MYOWN_OPE::FNEW]).' '.ucfirst(myOwnLang::$objects[MYOWN_OBJ::RESVRULE]);
		if ($this->ruleId>0) $title = ucfirst(myOwnLang::$object[MYOWN_OBJ::RESVRULE]).' #'.$this->ruleId;
		
		$header = MyOwnReservationsUtils::displayIncludes();
		$header .= myOwnReservationsController::getModuleHeader($obj, $cookie);
		$header .= $htmlOut;
		$header .= MyOwnReservationsUtils::displayStarter($obj);
		if (_PS_VERSION_ < "1.6.0.0") $header .= myOwnReservationsController::getMenuHeader($obj, $parent, $title, $buttons);
		$isproversion=is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");
		if ($this->ruleId>-1 or !$isproversion) $content = myOwnReservationsProductsController::edit($cookie,$obj,$this->ruleId);
		else $content = myOwnReservationsProductsController::show($cookie,$obj);
		
		return $header.$content;
  	}
  
}
?>