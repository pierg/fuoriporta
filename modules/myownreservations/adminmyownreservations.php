<?php
/*
* 2010-2012 LaBulle All right reserved
*/

require_once(_PS_MODULE_DIR_ . "myownreservations/myownreservations.php");
require_once(MYOWNRES_PATH . "controllers/admin.php");
require_once(MYOWNRES_PATH . "controllers/planning.php");
require_once(MYOWNRES_PATH . "controllers/ajax_admin.php");


class adminmyownreservations extends AdminController
{
	var $module = 'myownreservations';
	var $name = 'myownreservations';
	var $multishop_context_group;
	var $multishop_context;
	var $moduleObj;
	
	//-----------------------------------------------------------------------
	//     CONSTRUCT
	//-----------------------------------------------------------------------
    public function __construct()
    {
        global $cookie;
        $this->className = 'adminmyownreservations';
        $this->moduleObj = Module::getInstanceByName('myownreservations');
        $this->moduleObj->checkPro();
        myOwnReservationsController::_setInfos ($this->moduleObj);
        myOwnReservationsController::_construire($this->moduleObj);
        $this->multishop_context = true;
		$this->display = 'view';
        parent::__construct();
        $this->bootstrap=true;
    }
    
    public function initPageHeaderToolbar()
	{
		$this->page_header_toolbar_title = $this->l('Reservations administration', null, null, false);
		
		$this->page_header_toolbar_btn = myOwnReservationsAdminController::getButtonsList($this->moduleObj);
		
		if (_PS_VERSION_ >= "1.6.0.0") parent::initPageHeaderToolbar();
		$this->context->smarty->assign('help_link', null);
	}
	
	public function setMedia()
	{
		parent::setMedia();

		$this->addJqueryPlugin(array('fancybox', 'typewatch', 'growl'));
	}
	
	public function ajaxProcess()
	{
		global $cookie;
		$this->display = '';
		echo myOwnReservationsAjaxController::display();
	}
	
	public function ajaxProcessExtension() {
		global $cookie;
		$this->display = '';
		$method=Tools::getValue('method');
		$obj = Module::getInstanceByName('myownreservations');
		foreach ($obj->_extensions->getExtByType(extension_type::AJAX) as $ext) 
				if (strtolower($ext->name)==$method)
					echo $ext->ajax($obj, $cookie);
	}
	
	public function ajaxProcessCreate()
	{	
		$this->display = '';
		echo myOwnReservationsAjaxController::create($this->moduleObj);
	}
	
	public function ajaxProcessDelete()
	{
		$this->display = '';
		echo myOwnReservationsAjaxController::delete($this->moduleObj);
	}
	
	public function ajaxProcessPrintGrid()
	{
		$this->display = '';
		myOwnReservationsPlanningController::printGrid($this->moduleObj, $_GET['date']);
		die ("");
	}
	
	public function ajaxProcessExportConfig()
	{
		$this->display = '';
		myOwnReservationsAjaxController::exportConfig($_GET['myOwnResTab']);
	}
	
	public function ajaxProcessPrintHelp()
	{
		$this->display = '';
		$obj = Module::getInstanceByName('myownreservations');
		$iso = Context::getContext()->language->iso_code;
		if (isset($_GET['chapter'])) $chapter=$_GET['chapter'];
		else $chapter='';
		
		$out .= myOwnHelp::printStyle();
		if ($chapter=='full') 
			$out .=  myOwnHelp::printFull($obj, $iso);
		else $out .= myOwnHelp::printChapter($obj, $iso, $chapter);

		echo $out;
	}
	
	public function ajaxProcessPrintStock()
	{
		$this->display = '';

		if (Tools::getValue('printFormat')=='') {
			$productFamillyFilter = Tools::getValue('productFamillyFilter');
			$dayFilter = Tools::getValue('date', '');
			myOwnReservationsStockController::printStock($this->moduleObj, $dayFilter, $productFamillyFilter);
		} else {
			if (_PS_VERSION_ > "1.6.0.0") 
				myOwnReservationsStockController::printSerialv2();
			else
				myOwnReservationsStockController::printSerial();
		}
	}
	
	public function ajaxProcessAvailability()
	{
		$this->display = '';
		$obj = $this->moduleObj;
		$reschedule=new myOwnCart();
		$reschedule->id_product = $_POST['id_product'];
		$reschedule->id_shop = myOwnUtils::getShop();
		if ($reschedule->id_product > 0)
			$reschedule->_mainProduct = $obj->_products->getResProductFromProduct($reschedule->id_product);
		
		if ($reschedule->id_product==0 or $reschedule->_mainProduct==null) echo '';
		else {
			$reschedule->id_product_attribute = (int)$_POST['id_product_attribute'];
			$reschedule->id_object = $_POST['id_object'];
			if (stripos($_POST['period'], '#')!==false) {
				$period=explode("#", $_POST['period']);
				$startPeriod=explode("_", $period[0]);
				$endPeriod=explode("_", $period[1]);
				$reschedule->startDate = $startPeriod[0];
				$reschedule->startTimeslot = $startPeriod[1];
				$reschedule->endDate = $endPeriod[0];
				$reschedule->endTimeslot = $endPeriod[1];
				echo $reschedule->getAvailableQuantityToString($obj);
			} else echo '<div style="border-radius:5px;padding:5px;background-color:#FFF" >'.$obj->l('To see availability please indicate a period', 'adminmyownreservations').'</div>';
		}
	}
	
	public function ajaxProcessUpdateAvailability()
	{
		$this->display = '';
		$obj = $this->moduleObj;
		$id_availability = Tools::getValue('id_availability');
		$quantity = Tools::getValue('quantity');
		myOwnAvailability::sqlUpdateQty($id_availability, $quantity);
	}
	
	public function ajaxProcessDeleteAvailability()
	{
		$this->display = '';
		$obj = $this->moduleObj;
		$id_availability = Tools::getValue('id_availability');
		myOwnAvailability::sqlIdDelete($id_availability);
	}
	
	public function ajaxProcessCreateAvailability()
	{
		$this->display = '';
		$obj = $this->moduleObj;
		$speriod = Tools::getValue('period', 0);
		if (stripos($speriod, '#')!==false || stripos($speriod, '@')!==false) {
			$tempAvailability = new myOwnAvailability();
			$tempAvailability->id_product = Tools::getValue('id_product', 0);
			$tempAvailability->id_family = Tools::getValue('id_family', 0);
			$tempAvailability->id_product_attribute = Tools::getValue('id_product_attribute', 0);
			$tempAvailability->type = 1;
			
			if (stripos($speriod, '#')!==false) $period=explode("#", $speriod);
			else $period=explode("@", $speriod);
			$startPeriod=explode("_", $period[0]);
			$endPeriod=explode("_", $period[1]);
			$tempAvailability->startDate = $startPeriod[0];
			$tempAvailability->startTimeslot = $startPeriod[1];
			$tempAvailability->endDate = $endPeriod[0];
			$tempAvailability->endTimeslot = $endPeriod[1];
			if ($tempAvailability->startTimeslot==0 && $tempAvailability->endTimeslot==0 && $tempAvailability->startDate==$tempAvailability->endDate)
				$tempAvailability->endDate = date('Y-m-d', strtotime('+1day', strtotime($tempAvailability->startDate)));
			
			$tempAvailability->id_object  = Tools::getValue('id_object', 0);
			$tempAvailability->quantity = Tools::getValue('quantity',0);
			return $tempAvailability->sqlInsert();
		}
				
	}
	
	public function ajaxProcessSearchProducts()
	{
		$this->display = '';
		$obj = $this->moduleObj;
		$cookie = $obj->getContext()->cookie;
		$ret_prods=array();
		Context::getContext()->customer = new Customer((int)Tools::getValue('id_customer'));
		$id_currency = myOwnUtils::getCurrencyObj()->id;
		$currency = new Currency($id_currency);
		$family = (int)Tools::getValue('family', 0);
		$id = (int)Tools::getValue('id', 0);
		if (($id ? $products = array($id => new Product($id, true)) : $products = Product::searchByName((int)$this->context->language->id, pSQL(Tools::getValue('product_search')))))
		{
			foreach ($products as $key => &$product)
			{
				// Formatted price
				$mainProduct = $obj->_products->getResProductFromProduct($product['id_product']);
				$unit = ($mainProduct!=null ? $mainProduct->getUnitLabel($obj, $cookie->id_lang) : '');
				$product['mainProduct'] = (int)$mainProduct->sqlId;
				if ($family==0 || $product['mainProduct']==$family) {
					$product['formatted_price'] = Tools::displayPrice(Tools::convertPrice($product['price_tax_incl'], $currency), $currency).(trim($unit)!='' ? '/'.$unit : '');
					// Concret price
					$product['price_tax_incl'] = Tools::ps_round(Tools::convertPrice($product['price_tax_incl'], $currency), 2);
					$product['price_tax_excl'] = Tools::ps_round(Tools::convertPrice($product['price_tax_excl'], $currency), 2);
					$productObj = new Product((int)$product['id_product'], false, (int)$this->context->language->id);
					$combinations = array();
					$attributes = $productObj->getAttributesGroups((int)$this->context->language->id);
	
					// Tax rate for this customer
					if (Tools::isSubmit('id_address'))
						$product['tax_rate'] = $productObj->getTaxesRate(new Address(Tools::getValue('id_address')));
	
					$product['warehouse_list'] = array();
	
					foreach ($attributes as $attribute)
					{
						if (!isset($combinations[$attribute['id_product_attribute']]['attributes']))
							$combinations[$attribute['id_product_attribute']]['attributes'] = '';
						$combinations[$attribute['id_product_attribute']]['attributes'] .= $attribute['attribute_name'].' - ';
						$combinations[$attribute['id_product_attribute']]['id_product_attribute'] = $attribute['id_product_attribute'];
						$combinations[$attribute['id_product_attribute']]['default_on'] = $attribute['default_on'];
						if (!isset($combinations[$attribute['id_product_attribute']]['price']))
						{
							$price_tax_incl = Product::getPriceStatic((int)$product['id_product'], true, $attribute['id_product_attribute']);
							$price_tax_excl = Product::getPriceStatic((int)$product['id_product'], false, $attribute['id_product_attribute']);
							$combinations[$attribute['id_product_attribute']]['price_tax_incl'] = Tools::ps_round(Tools::convertPrice($price_tax_incl, $currency), 2);
							$combinations[$attribute['id_product_attribute']]['price_tax_excl'] = Tools::ps_round(Tools::convertPrice($price_tax_excl, $currency), 2);
							$combinations[$attribute['id_product_attribute']]['formatted_price'] = Tools::displayPrice(Tools::convertPrice($price_tax_excl, $currency), $currency).(trim($unit)!='' ? '/'.$unit : '');
						}
						if (!isset($combinations[$attribute['id_product_attribute']]['qty_in_stock']))
							$combinations[$attribute['id_product_attribute']]['qty_in_stock'] = StockAvailable::getQuantityAvailableByProduct((int)$product['id_product'], $attribute['id_product_attribute'], (int)$this->context->shop->id);
	
						if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') && (int)$product['advanced_stock_management'] == 1)
							$product['warehouse_list'][$attribute['id_product_attribute']] = Warehouse::getProductWarehouseList($product['id_product'], $attribute['id_product_attribute']);
						else
							$product['warehouse_list'][$attribute['id_product_attribute']] = array();
	
						$product['stock'][$attribute['id_product_attribute']] = Product::getRealQuantity($product['id_product'], $attribute['id_product_attribute']);
	
					}
	
					if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') && (int)$product['advanced_stock_management'] == 1)
						$product['warehouse_list'][0] = Warehouse::getProductWarehouseList($product['id_product']);
					else
						$product['warehouse_list'][0] = array();
	
					$product['stock'][0] = StockAvailable::getQuantityAvailableByProduct((int)$product['id_product'], 0, (int)$this->context->shop->id);
	
					foreach ($combinations as &$combination)
						$combination['attributes'] = rtrim($combination['attributes'], ' - ');
					$product['combinations'] = $combinations;
	
					if ($product['customizable'])
					{
						$product_instance = new Product((int)$product['id_product']);
						$product['customization_fields'] = $product_instance->getCustomizationFields($this->context->language->id);
					}
					$ret_prods[$key]=$product;
				}
			}

			$to_return = array(
				'products' => $ret_prods,
				'found' => count($ret_prods)
			);
		}
		else
			$to_return = array('found' => false);

		echo Tools::jsonEncode($to_return);
	}
	
	public function ajaxProcessExportPlanning() {
		$this->display = '';
		$obj = $this->moduleObj;
		return myOwnReservationsPlanningController::exportPlanning($obj);
	}

    
    public function renderView()
	{	
		global $cookie;
		$obj = $this->moduleObj;
		$header = '';
		$header .= myOwnReservationsController::getModuleHeader($obj, $cookie);
		return $header.myOwnReservationsAdminController::display($this);
  	}

  
}
?>