<?php
/*
* 2010-2012 LaBulle All right reserved
*/

require_once(_PS_MODULE_DIR_ . "myownreservations/myownreservations.php");
require_once(MYOWNRES_PATH . "controllers/admin.php");
require_once(MYOWNRES_PATH . "controllers/planning.php");
require_once(MYOWNRES_PATH . "controllers/ajax_admin.php");


class adminReservationAvailability extends AdminController
{
	var $module = "myownreservations";
	var $name = "myownreservations";
	var $multishop_context_group;
	var $multishop_context;
	var $moduleObj;
	var $ruleId;
	var $insertedId=0;

    public function __construct()
    {
        global $cookie, $_LANGADM;

        $this->moduleObj = Module::getInstanceByName('myownreservations');
        $this->moduleObj->checkPro();
        //if (!is_file(_PS_MODULE_DIR_ ."myownreservations/pro/products.php")) 
        $this->multishop_context = true;
        myOwnReservationsController::_setInfos ($this->moduleObj);
        myOwnReservationsController::_construire($this->moduleObj);
		$this->display = 'view';
		$this->class_name = 'adminReservationAvailability';
		$this->bootstrap=true;
		$this->doActions();
        parent::__construct();
    }
    
        public function setMedia()
	{
		parent::setMedia();

		$this->addJqueryPlugin(array('fancybox', 'typewatch', 'growl'));
		$this->addJS(array(
                _PS_JS_DIR_.'tiny_mce/tiny_mce.js',
                _PS_JS_DIR_.'admin/tinymce.inc.js',
            ));
        $this->addJS(_MODULE_DIR_.'myownreservations/views/js/back.js');
	}
    
    public function displayErrors() {
	    foreach($this->_errors as $err) echo $err;
    }
    
    public function initPageHeaderToolbar()
	{
		global $cookie;
		$tab = Tools::getValue('myOwnResTab');
		$ctrl = strtolower(Tools::getValue('controller'));
		$field='';
		if ($tab=='timeslots')
			$fieldl = 'TimeSlot';
		if ($tab=='pricesrules') 
			$field = 'Pricesrule';
		if ($tab=='availability') 
			$field = 'Availability';
		if ($ctrl=='adminreservationrules') {
			$tab='products';
			$field = 'Product';
		}
		
		if ($this->ruleId > 0) $this->page_header_toolbar_title = ucfirst(myOwnLang::$object[MYOWN_OBJ::AVAILABILITY]).' #'.$this->ruleId;
		else $this->page_header_toolbar_title = ucfirst(myOwnLang::$object[MYOWN_OBJ::AVAILABILITY]);

		$this->page_header_toolbar_btn = myOwnReservationsController::getButtonsList( $this->moduleObj, $this->ruleId);

		if (_PS_VERSION_ >= "1.6.0.0") parent::initPageHeaderToolbar();
		$this->context->smarty->assign('help_link', null);
	}
	

	
	public function doActions()
	{
		global $cookie;
		$obj = $this->moduleObj;

		$operation='';
		$this->insertedId=0;
		$objet = $obj->l('The Availability', 'adminreservationavailabilities');
		$male = true;
		$errors=array();$plur=false;
		$isproversion=is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");
		

		$this->errors =  myOwnReservationsAvailabilitiesController::checkEdit($obj);

		$msg='';
		if (!count($this->errors)) {
			$result = myOwnReservationsAvailabilitiesController::addAction($obj, $msg);
		}
		
		if (isset($_POST["deleteAvailabilities"])) {
			$msg='';
			$plur=true;
			$objet = myOwnLang::$objects[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$objects[MYOWN_OBJ::AVAILABILITY]);
			$result = myOwnReservationsAvailabilitiesController::deleteAction($obj, $msg);
			$operation = myOwnLang::getOperation($obj, MYOWN_OPE::DELETE, false, false);
		}
		
		$male=true;
		if ($operation != "") {
			if ($result) $this->confirmations[] = $objet.' '.myOwnLang::getResult($obj, true, !$plur).' '.$operation;
			else $this->errors[] = $objet.' '.myOwnLang::getResult($obj, false, !$plur).' '.$operation.' : '.Db::getInstance()->getMsgError();

			myOwnReservationsController::_construire($obj, true);
		}
	} 


    public function renderView()
	{
		global $cookie;
		$htmlOut='';$content='';
		$obj = $this->moduleObj;
		if (_PS_VERSION_ < "1.6.0.0") {
			$this->initPageHeaderToolbar();
			$buttons = MyOwnReservationsUtils::getButtonsList($this->page_header_toolbar_btn, $this->ruleId);
		} else $buttons='';
		//$buttons = myOwnReservationsController::getButtons($cookie, $obj, $this->ruleId, false, 'Product',  'products');
		
		if (is_array($this->errors))
		foreach($this->errors AS $error) {
			$htmlOut .= myOwnUtils::displayError($error);
		}
		foreach($this->confirmations AS $confirmation) {
			$htmlOut .= myOwnUtils::displayConf($confirmation);
		}
		$this->errors=array();
		$this->confirmations=array();

		
		$header = MyOwnReservationsUtils::displayIncludes();
		$header .= myOwnReservationsController::getModuleHeader($obj, $cookie);
		$header .= $htmlOut;
		$header .= MyOwnReservationsUtils::displayStarter($obj);
		if (_PS_VERSION_ < "1.6.0.0") $header .= myOwnReservationsController::getMenuHeader($obj, $parent, $title, $buttons);
		$isproversion=is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");

		
		$id = intval(Tools::getValue('editTimeSlot', -1));
			if ($id==0) $id = $insertedId;

			
			if ($id==-1) $title = ucfirst(myOwnLang::$objects[MYOWN_OBJ::AVAILABILITY]);
			if ($id==0) $title = ucfirst(myOwnLang::$actions[MYOWN_OPE::FNEW]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::AVAILABILITY]);
			if ($id>0) $title = ucfirst(myOwnLang::$object[MYOWN_OBJ::AVAILABILITY]).' #'.$id;
			
			//if ($id>-1) 
			$content .= myOwnReservationsAvailabilitiesController::add('global',$obj);
			$content .= myOwnReservationsAvailabilitiesController::show('global',$obj);
		
		return $header.$content;
  	}
  
}
?>