<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnReservationsReservationsController {
	
	public static function getScript($obj, $print=false)
	{
		global $cookie;
		$out='';
		$imgSize = new ImageType(1);
		if (!$print) {
		$out .= (_PS_VERSION_ < "1.5.0.0" ? '<script src="'._PS_JS_DIR_.'jquery/jquery.fancybox-1.3.4.js" type="text/javascript" charset="utf-8"></script><link rel="stylesheet" href="'._PS_CSS_DIR_.'jquery.fancybox-1.3.4.css" type="text/css" charset="utf-8">' :
		'<script src="'._PS_JS_DIR_.'jquery/plugins/fancybox/jquery.fancybox.js" type="text/javascript" charset="utf-8"></script><link rel="stylesheet" href="'._PS_JS_DIR_.'jquery/plugins/fancybox/jquery.fancybox.css" type="text/css" charset="utf-8">').'
		<script type="text/javascript">
		var baseDir = \''.__PS_BASE_URI__.'\';
        var imgDir = \''._PS_IMG_.'\';
        var imageSize = '.intval($imgSize->height).';
        '.(_PS_VERSION_ < "1.5.0.0" ? '
        $(document).ready(function() {
	        $("#container").attr("style", "width:1400px");
        });' :'').'
		</script>';
		}
		
		$out .= '
		<script type="text/javascript">
		var unmodifiedEvent;
        var qtyErrorMsg = "'.$obj->l('Quantity must be greater than 0','reservations').'";
        var customerErrorMsg = "'.$obj->l('Please select a customer','reservations').'";
        var noproductErrorMsg = "'.$obj->l('Please add products','reservations').'";
        var availability_label = "'.ucfirst(myOwnLang::$object[MYOWN_OBJ::AVAILABILITY]).' '.ucfirst(myOwnLang::$operations[MYOWN_OPE::UPDATE]).'";
        var queryError = "'.$obj->l('Internal error','reservations').'";
        var pageAdmin= "'.myOwnReservationsController::getAdminUrl('').'";
        var ajaxAdmin = \''.(Tools::getIsset('fc') ? __PS_BASE_URI__.'index.php?method=mymarketplace&fc=module&module=myownreservations&controller=actions&id_lang='.$cookie->id_lang : 'ajax-tab.php?tab=admin'.$obj->name.'&goupedView='.(int)$cookie->goupedView.'&token='.Tools::getAdminToken('admin'.$obj->name.intval(Tab::getIdFromClassName('admin'.$obj->name)).intval($cookie->id_employee))).'\';
        </script>
        ';
		return $out;
		
	}	
	//==============================================================================================================================================
	//     RESERVATION ACTIONS
	//==============================================================================================================================================

	public static function check($obj, $cookie, &$resa, &$reschedule) {
		$actions=array();
		$warning='';
		$check='';
		$result='';
		$productCreate=Tools::getValue("product");
		$stockCreate=Tools::getValue("stock");
		
		//--------------------------------------------------------- create check ---------------------------------------------------------
		if (isset($_POST['period']) && $_POST['period']!='') {
			$actions[]="new";
			$reschedule = new myOwnReservation($_POST['resa_id']);
			$reschedule->id_shop = myOwnUtils::getShop();
			$reschedule->id_product = $_POST['id_product'];
			if (!$reschedule->id_product) die(myOwnUtils::displayError($obj->l('Please select a product','reservations')));
			$reschedule->id_product_attribute = $_POST['id_product_attribute'];
			$reschedule->_mainProduct = $obj->_products->getResProductFromProduct($reschedule->id_product);
			
			$period=explode("#", $_POST['period']);
			$startPeriod = explode("_", $period[0]);
			$endPeriod = explode("_", $period[1]);
			//$timeslots = $reschedule->_mainProduct->getTimeslots();
			$start=strtotime($startPeriod[0]);
			$end=strtotime($endPeriod[0]);
			//if getting no timeslot selection from form the get nearest
			if (array_key_exists($startPeriod[1], $reschedule->_mainProduct->_timeslotsObj->list))
				$newStartTs = $reschedule->_mainProduct->_timeslotsObj->list[$startPeriod[1]];
			else $newStartTs = $reschedule->_mainProduct->_timeslotsObj->getNearestStart($start);		
			
			if (MYOWNRES_WEEK_TS && $resa->_mainProduct->_reservationUnit==reservation_unit::WEEK) {
				$newEndTs = $newStartTs;
			} else {
				if (array_key_exists($endPeriod[1], $reschedule->_mainProduct->_timeslotsObj->list))
					$newEndTs = $reschedule->_mainProduct->_timeslotsObj->list[$endPeriod[1]];
				else $newEndTs =  $reschedule->_mainProduct->_timeslotsObj->getNearestEnd($end);
			}
		}
		
		//--------------------------------------------------------- rescheduling calc ---------------------------------------------------------
		if (isset($_POST['reschedule']) && $_POST['reschedule']!='') {
			$period=explode("#", $_POST['reschedule']);
			//
			//if data from the fields
			if (strripos($period[0], '_') !== false) {
				$startPeriod=explode("_", $period[0]);
				$endPeriod=explode("_", $period[1]);

				$newStartTs = $resa->_mainProduct->_timeslotsObj->list[$startPeriod[1]];
				/*if (MYOWNRES_WEEK_TS && $resa->_mainProduct->_reservationUnit==reservation_unit::WEEK) {
					$newEndTs = $newStartTs;
					$newEndTs->ignoreCheck=true;
				} else {*/
					$newEndTs = $resa->_mainProduct->_timeslotsObj->list[$endPeriod[1]];
				//}
				$start=strtotime($startPeriod[0]);
				$end=strtotime($endPeriod[0]);
				//if (!$resa->_saved) $actions[]="new";
			//if data from the drag
			} else {
				$start=strtotime($period[0]);
				$end=strtotime($period[1]);
				if ($resa->_mainProduct==null) {
					$newStartTs = $obj->_timeSlots->getNearestStart($start);
					$newEndTs =  $obj->_timeSlots->getNearestEnd($end);
				} else {
					$newStartTs = $resa->_mainProduct->_timeslotsObj->getNearestStart($start);
					$newEndTs = $resa->_mainProduct->_timeslotsObj->getNearestEnd($end);
					if (!$resa->_saved && MYOWNRES_WEEK_TS && $resa->_mainProduct->_reservationUnit==reservation_unit::WEEK) {
						$end = myOwnCalendarWeek::getDayOfWeek($resa->_mainProduct->_reservationSelWeekEnd, date('W',$start), date('o',$start));
					}
				}
			}
		}
		
		if ($reschedule!=null && ((isset($_POST['period']) && $_POST['period']!='') or (isset($_POST['reschedule']) && $_POST['reschedule']!=''))) {
			$reschedule->startDate=date('Y-m-d',$start);
			if ($newStartTs!=null) $reschedule->startTime = $newStartTs->startTime;
			$reschedule->startTimeslot=($newStartTs!=null ? $newStartTs->sqlId : 0);
			$reschedule->endDate=date('Y-m-d',$end);
			if ($newStartTs!=null) $reschedule->endTime = $newStartTs->endTime;
			$reschedule->endTimeslot=($newEndTs!=null ? $newEndTs->sqlId : 0);
		}
		
		//--------------------------------------------------------- action check ---------------------------------------------------------
		if ($reschedule!=null && $reschedule->_saved && isset($_POST['reschedule']) && $_POST['reschedule']!='') {
			//check reschedule difference
			if ($newStartTs!=null && $newEndTs!=null && $reschedule->_saved) {
				$error='';
				//check start diff
				$diff=$reschedule->getStartDateTime()-$resa->getStartDateTime();
				if ($diff>0) {
					$actions[]="forward";
				} if ($diff<0) {
					$actions[]="backward";
				}
				
				//check length diff
				$resadiff=$resa->getEndDateTime()-$resa->getStartDateTime();
				//echo '@'.date('Y-m-d H:i:s', $resa->getEndDateTime()).'-'.date('Y-m-d H:i:s', $resa->getStartDateTime());
				//echo '#'.date('Y-m-d H:i:s', $reschedule->getEndDateTime()).'-'.date('Y-m-d H:i:s', $reschedule->getStartDateTime());
				$reschedulediff=$reschedule->getEndDateTime()-$reschedule->getStartDateTime();
				$diffLength=$reschedulediff-$resadiff;
				if ($diffLength<0) {
					$actions[]="shorten";
				} else if ($diffLength>0) {
					$actions[]="extend";
				}
			} else {
				$warning.=$obj->l('No time slot found', 'reservations');
			}
		}
		
		$stockChanged=false;
		
		//--------------------------------------------------------- rescheduling check ---------------------------------------------------------
		//set resa start and end for reshcedule or new resa.
		if ($reschedule!=null && ((isset($_POST['period']) && $_POST['period']!='') or (isset($_POST['reschedule']) && $_POST['reschedule']!=''))) {
			/*$reschedule->startDate=date('Y-m-d',$start);
			$reschedule->startTimeslot=$newStartTs->sqlId;
			$reschedule->endDate=date('Y-m-d',$end);
			$reschedule->endTimeslot=$newEndTs->sqlId;*/
			
			//rescheduling on xy timeline view
			if ($productCreate!="" && $productCreate!="0") {
				$productTab=explode('-', $productCreate);
				$reschedule->id_product = intval($productTab[0]);
				$reschedule->id_product_attribute = intval($productTab[1]);
				$reschedule->_mainProduct = $obj->_products->getResProductFromProduct($reschedule->id_product);
			}
			/*if ($stockCreate!="" && $stockCreate!="0") {
				$productTab=explode('-', $productCreate);
				$reschedule->stock = $stockCreate;
			}*/
			if ($reschedule->id_product > 0 && $reschedule->_mainProduct!=null) $reschedule->setPrice($obj->_pricerules->list);
			if ($resa->_mainProduct!=null) {
				if ($newStartTs==null or !$newStartTs->isEnableOnDay($start) or $newStartTs->type==2) $warning.=$obj->l('The reservation should not start on that day or time slot', 'reservations').' ';
				if (!MYOWNRES_WEEK_TS && $resa->_mainProduct->_reservationUnit!=reservation_unit::WEEK)
				if ($newEndTs==null or !$newEndTs->isEnableOnDay($end) or $newStartTs->type==1) $warning.=$obj->l('The reservation should not end on that day or time slot', 'reservations').' ';
			}
			if ($stockCreate!=$resa->stock && !isset($_POST['edit'])) {
	 			$actions[] = "assign";
	 			$reschedule->stock = $stockCreate;
	 			$stockChanged = true;
	 			$stock = $stockCreate;
	 		}
		}
		
		//--------------------------------------------------------- stock check ---------------------------------------------------------
		if ($resa->_mainProduct!=null && isset($_POST['edit'])) {
		    $stockassigned = explode(";",$resa->stock);
		    /*if ($resa->_mainProduct->productType == product_type::PLACE)
		    	$stockitems=$obj->_stocks->getStockForProduct(-$resa->id_object);
		    else if($resa->_mainProduct->productType == product_type::RESOURCE)
		    	$stockitems=$obj->_extensions->list[product_type::RESOURCE]->getStockForProduct($resa->id_product, 0);
	 		else */
	 		$obj_exts = $obj->_extensions->getExtByType(extension_type::OBJECT);
			$ext_object = null;
			foreach ($obj_exts as $ext) 
				if (method_exists($ext, "objectSelect"))
					if ($resa->_mainProduct!=null && in_array($ext->id, $resa->_mainProduct->ids_options))
						$ext_object = $ext;
			if ($resa->_mainProduct->productType==product_type::STOCK)
	 			$stockitems=$obj->_stocks->getStockForProduct($resa->id_product, ($resa->_mainProduct->_optionIgnoreAttrStock ? 0 : $resa->id_product_attribute));
	 		elseif ($ext_object != null && $resa->id_object)
				$stockitems=$obj->_stocks->getStockForProduct(-$resa->id_object, 0);

	 		$stock="";$err=false;
	 		for($i=1; $i<($resa->quantity+1); $i++) {
	 			if (isset($_POST['product'.$resa->sqlId.'_'.$i]) && trim($_POST['product'.$resa->sqlId.'_'.$i])!="") {
	 				$prod_stock_item = trim($_POST['product'.$resa->sqlId.'_'.$i]);
	 				if ($stock=="" or strpos($stock, $prod_stock_item) === false or $resa->_mainProduct->productType==product_type::RESOURCE) {
	     				$serialexist=false;
	     				foreach ($stockitems as $stockitem)
	     					if ($stockitem->serial==$prod_stock_item)
	     						$serialexist=true;
	     				if ($serialexist) {
	     					//get reservation count if not already assigned
	     					if (in_array(trim($_POST['product'.$i]), $stockassigned)) $count = 0;
	     					else $count = myOwnResas::getFlatResaLinesCount(null, null, $resa->_mainProduct, $resa->id_product, $resa->id_product_attribute, strtotime($resa->startDate),  strtotime($resa->endDate), $resa->startTimeslot, $resa->endTimeslot, $resa->id_object, $prod_stock_item);
	     					if ($count==0 || $resa->_mainProduct->productType==product_type::RESOURCE) {
			     				if ($stock!="") $stock.=";";
			     				$stock.=$prod_stock_item;
			     			} else if ($resa->_mainProduct->productType!=product_type::RESOURCE) {
			     				$warning.=$obj->l('The stock item with serial number', 'reservations').' '.$prod_stock_item.' '.$obj->l('is already reserved', 'reservations');
			     			}
			     		} else {
			     			$warning.= $obj->l('There is no stock item with serial number', 'reservations').' '.$prod_stock_item;
			     		}
			     	} else {
			     		$warning.= $obj->l('The stock item with serial number', 'reservations').' '.$prod_stock_item.' '.$obj->l('is already assigned', 'reservations');
			     	}
	 			}
	 		}
	 		if ($stock!=$resa->stock || $resa->id_assigned != Tools::getValue("assignmentEmployee".$resa->sqlId, $resa->id_assigned)) {
	 			$actions[] = "assign";
	 			$stockChanged = true;
	 			if ($cookie->goupedView && $warning=='') {
	 				if ($stock!=$resa->stock) {
		 				$resulta = myOwnResas::setReservationStock($resa->sqlId, $stock);
			 			$resa->stock=$stock;
		 			}
		 			if ($resa->id_assigned != Tools::getValue("assignmentEmployee".$resa->sqlId, $resa->id_assigned)) {
		 				$resultb = $resa->assign($obj, Tools::getValue("assignmentEmployee".$resa->sqlId, $resa->id_assigned));
		 				$resa->id_assigned = Tools::getValue("assignmentEmployee".$resa->sqlId, $resa->id_assigned);
		 			}
		 			if ($resulta || $resultb) $result = myOwnUtils::displayConf(ucfirst(myOwnLang::$object[MYOWN_OBJ::RESV]).' #'.$resa->sqlId.' '.$obj->l('successfully assigned', 'reservations'));
	 			}
	 		}
	 		if ($cookie->goupedView && $warning=='' && Tools::getValue("param0")=='attendees') {
	 			if (Tools::getValue("param1")==$resa->sqlId) {
 					$result=true;
	 				if ($result) $result = myOwnUtils::displayConf($obj->l('Item', 'reservations').' '.Tools::getValue("param2").' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::RESV]).' #'.$resa->sqlId.' '.$obj->l('successfully deleted', 'reservations'));
	 			}
 			}
 		}
 		/*
 		debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);*/
 		if ($reschedule!=null
			&& $reschedule->getSelectValue() == $resa->getSelectValue()
			&& $reschedule->_saved
			&& $resa->quantity==Tools::getValue("quantity", $resa->quantity)
			&& !$stockChanged)
			 $reschedule=null;
		$check='';
		
     	
     	//--------------------------------------------------------- update check ---------------------------------------------------------	
		//if no reschedule confirm save resa options
		if ($reschedule==null) {
			$temp_external_fields = substr(Tools::getValue("customer", ''),0,100).';'.Tools::getValue("carrier", 0).';'.Tools::getValue("payment", 0);
			if ( isset($_POST["comment"]) && (
				trim($resa->comment) != trim(Tools::getValue("comment", $resa->comment))
				or $resa->validated != Tools::getValue("validationStatus", $resa->validated) 
				or ($resa->id_assigned != Tools::getValue("assignmentEmployee", $resa->id_assigned))
				or ($resa->id_object != Tools::getValue("object", $resa->id_object))
				or ($resa->id_order==0 && $resa->external_fields != $temp_external_fields) 
				or ($resa->id_order==0 && $resa->advance != Tools::getValue("advance", $resa->advance)) 
				or Tools::getIsset('saveDetails')
				) ) {
				$resa->comment = trim(Tools::getValue("comment"));
				
				if ($resa->validated != Tools::getValue("validationStatus", $resa->validated)) {
					$resa->validate($obj, Tools::getValue("validationStatus"));
				}
				if ($resa->_mainProduct != null && $resa->_mainProduct->assignment > 0) {
					if ($resa->id_assigned != Tools::getValue("assignmentEmployee", $resa->id_assigned)) {
						$resa->assign($obj, Tools::getValue("assignmentEmployee"));
					}
					$resa->id_assigned = Tools::getValue("assignmentEmployee");
				}
				if ($resa->id_object != Tools::getValue("object", $resa->id_object)) {
					$resa->park($obj, Tools::getValue("object", $resa->id_object));
				}
				$resa->id_assigned = Tools::getValue("assignmentEmployee");
				if ($resa->id_order==0) {
					$resa->external_fields = $temp_external_fields;
					$resa->advance = str_ireplace(',', '.', Tools::getValue("advance", $resa->advance));
					$resa->_carrier = Tools::getValue("carrier", $resa->_carrier);
					$resa->_payment = Tools::getValue("payment", $resa->_payment);
				}
				if (Tools::getIsset('saveDetails')) {
					foreach ($obj->_extensions->getExtByType(extension_type::HEADER) as $ext) {
						if (method_exists($ext, "saveAttendeesCartData"))
							$ext->saveAttendeesCartData($obj, myOwnCarts::getResCartFromResa($obj, $resa));
					}
					$res=true;
				} else
					$res = $resa->sqlUpdate();
				
				if (_PS_VERSION_ < "1.6.0.0") {
					if ($res) $result = '<div id="content" style="background:none">'.myOwnUtils::displayConf($obj->l('Reservation successfully saved', 'reservations')).'</div></div>';
					else $result = '<div id="content" style="background:none">'.myOwnUtils::displayError($obj->l('Error during reservation save', 'reservations')).'</div></div>';
				} else {
					if ($res) $result = myOwnUtils::displayConf($obj->l('Reservation successfully saved', 'reservations'));
					else $result = myOwnUtils::displayError($obj->l('Error during reservation save', 'reservations'));
				}
			}
		//if reschedule confirm just set resa options
		} else {
			$reschedule->validated = Tools::getValue("validationStatus", $resa->validated);
			if ($reschedule->_mainProduct != null && $reschedule->_mainProduct->assignment > 0) 
				$reschedule->id_assigned = Tools::getValue("assignmentEmployee", $resa->id_assigned);
			$reschedule->comment = Tools::getValue("comment", $resa->comment);
			$reschedule->quantity = Tools::getValue("quantity", $resa->quantity);
			if ($stockChanged) $reschedule->stock = $stock;
			$reschedule->id_order = Tools::getValue("id_order", $resa->id_order);
			if ($reschedule->id_order >0 ) $reschedule = myOwnResas::populateReservation($reschedule);
			else {
				$reschedule->external_fields = substr(Tools::getValue("customer", ''),0,100).';'.Tools::getValue("carrier", 0).';'.Tools::getValue("payment", 0);
				$reschedule->advance = str_ireplace(',', '.', Tools::getValue("advance", $resa->advance));
				$reschedule->_carrier = Tools::getValue("carrier", $resa->_carrier);
				$reschedule->_payment = Tools::getValue("payment", $resa->_payment);
			}
			if (!$reschedule->_saved) $resa=clone $reschedule;
			$diffQty=$reschedule->quantity - $resa->quantity;
			if ($reschedule->quantity>0) {
				if ($diffQty>0) $actions[]="add";
				if ($diffQty<0) $actions[]="remove";
			} else {
				$actions[]="clear";
			}
		}

		//set warning details
		$actionsCnt=0;
		foreach ($actions as $action) {
			$actionsCnt++;
			if ($actionsCnt>1) {
				if ($actionsCnt<count($actions)) $check .= ', ';
				else if ($actionsCnt==count($actions) && count($actions)>1) $check .= ' '.trim($obj->l('and', 'reservations')).' ';
			}
			if ($action=="forward") $check .= $obj->l('move', 'reservations').' '.MyOwnCalendarTool::getNiceLength($obj, $diff/60).' '.$obj->l('later', 'reservations');
			if ($action=="backward") $check .= $obj->l('move', 'reservations').' '.MyOwnCalendarTool::getNiceLength($obj, $diff/60).' '.$obj->l('earlier', 'reservations');
			if ($action=="extend") $check .= $obj->l('extend', 'reservations').' '.MyOwnCalendarTool::getNiceLength($obj, $diffLength/60).' '.$obj->l('more', 'reservations');
			if ($action=="shorten") $check .= $obj->l('shorten', 'reservations').' '.MyOwnCalendarTool::getNiceLength($obj, $diffLength/60).' '.$obj->l('less', 'reservations');
			if ($action=="add") $check .= myOwnLang::$actions[MYOWN_OPE::ADD].' '.$diffQty.' '.($diffQty >1 ? $obj->l('units', 'reservations') : $obj->l('unit', 'reservations'));
			if ($action=="remove") $check .= $obj->l('remove', 'reservations').' '.$diffQty.' '.($diffQty >1 ? $obj->l('units', 'reservations') : $obj->l('unit', 'reservations'));
			if ($action=="clear") $check .= $obj->l('delete reservation', 'reservations');
			if ($action=="assign") $check .= $obj->l('Change stock', 'reservations').' '.$obj->l('with item', 'reservations').' '.$stockCreate;
			if ($action=="new") $check .= $obj->l('New reservation confirmation', 'reservations');
			if ($actionsCnt==1) $check=ucfirst($check);
		}
		return array('label' => $check, 'list' => $actions, 'warning' => $warning, 'result' => $result);
	}
	
	public static function display($obj) {

		global $cookie, $smarty;
		if (_PS_VERSION_ >= "1.7.0.0") smartyRegisterFunction($smarty, 'function', 'displayPrice', array('Tools', 'displayPriceSmarty'));
		$link=new Link();
		$smarty->assign('myownlink',$link);
		if (empty($link->protocol_content))
			$link->protocol_content = Tools::getCurrentUrlProtocolPrefix();
		$myresafilter = Tools::getValue('filter');
		$resas = array();
		$productsIds = array();
		$productsNames = array();
		$resasAttributes = array();
		$resasExt = $obj->_extensions->getExtByType(extension_type::NOTIFOBJ);
		if ($cookie->id_customer) $resas = myOwnResas::getReservationsForCustomer($obj, $cookie->id_customer, $myresafilter);
		foreach ($resas as $resa) {
			if (! in_array($resa->id_product, $productsIds)) {
				$productsIds[]=$resa->id_product;
				$productsNames[$resa->id_product]=MyOwnReservationsUtils::getProductName($resa->id_product, $cookie->id_lang);
			}
			if ($resa->id_product_attribute>0) $resasAttributes[$resa->sqlId]=MyOwnReservationsUtils::getAttributeCombinaisons($resa->id_product, $resa->id_product_attribute, $cookie->id_lang);
			else $resasAttributes[$resa->sqlId]='';
		}
		$imageTypes=ImageType::getImagesTypes();
		foreach($imageTypes as $imageType)
			if ($imageType['name']=='small_default') $mediumSize=new ImageType($imageType['id_image_type']);
		$statusestmp = myOwnUtils::getStatusList($cookie->id_lang);

		$statuses=array();
		foreach ($statusestmp as $statusetmp)
			$statuses[$statusetmp['id_order_state']]=$statusetmp;
		$smarty->assign('resas', $resas);

		$productsImages=MyOwnReservationsUtils::getProductsImages($productsIds, $cookie->id_lang);
		//attribute_text=MyOwnReservationsUtils::getAttributeCombinaisons($resa->id_product, $resa->id_product_attribute, $cookie->id_lang);
// 			$productStr = '<b>'.($resa->quantity > 1 ? $resa->quantity.'x ' : '').MyOwnReservationsUtils::getProductName($resa->id_product, $cookie->id_lang).'</b>'.
		    $smarty->assign(array(
		    		'myresafilter' => $myresafilter,
					'productsImages' => $productsImages,
					'productsNames' => $productsNames,
					'resasAttributes' => $resasAttributes,
					'resasExt' => $resasExt,
					'mediumSize' => $mediumSize));

		$smarty->assign('myownr', $obj);
		$smarty->assign('id_lang', $cookie->id_lang);
		$smarty->assign('statuses', $statuses);
		$out=$obj->display(MYOWNRES_PATH.$obj->name.'.php','views/templates/front/account/reservations_history.tpl');
		return $out;
	}
	
	public static function editOrderDetails($obj, $carriers, $payment_modules, $resa, $productsFilter=array()) {
		$orders = myOwnUtils::getOrdersList();
		$isproversion=is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");
		$external_fields = explode(';', $resa->external_fields);
		$output="";
		$output .= '
		<script type="text/javascript">
		var ordertypeval=0;
		</script>
		<div style="float:right">
			<select id="orderId" name="orderId" style="width:150px" onchange="ordertypeval=$(this).val();($(this).val()==-1 ? $(\'#editCustomer\').show() && $(\'#displayProductDetails\').hide() && $(\'#reservationDetails\').hide() : $(\'#editCustomer\').hide() && $(\'#displayProductDetails\').show() && $(\'#reservationDetails\').show());($(this).val()==0 ? $(\'.hideOnOrder\').show() : $(\'.hideOnOrder\').hide())">';
			if ($isproversion) $output .= '
				<option value="0">'.$obj->l('No order', 'reservations').'</option>
				<option value="-1">'.ucfirst(myOwnLang::$actions[MYOWN_OPE::FNEW]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::ORDER]).'</option>';
		if ($productsFilter==array())
		foreach($orders AS $orderElem) {
			$customer = new Customer($orderElem['id_customer']);
			$output .= '<option value="' . $orderElem['id_order']. '">N°' . $orderElem['id_order']. ' '.$obj->l('by(cust)', 'reservations').' '.$customer->firstname.' '.$customer->lastname.'</option>';
		}
		$output .= '
			</select>
		</div>';
		
		return $output;
	}
	
	public static function previewOrder($obj, $carriers, $payment_modules, $resa, $productsFilter=array()) {
		global $cookie;
		$id_customer = Tools::getValue('customer');
		$id_cart = Tools::getValue('id_cart');
		if ($id_cart) $cart=new Cart($id_cart);
		
		$customer = new Customer($id_customer);
		echo '
			<div class="bootstrap">
				<div class="panel">
					<div class="panel-heading">
						<i class="icon-credit-card"></i>
						'.ucfirst(myOwnLang::$actions[MYOWN_OPE::FNEW]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::ORDER]).'
					</div>
					<div class="form-group" style="margin-bottom:3px">
						<label class="control-label col-lg-3">'.$obj->l('Customer', 'reservations').'</label>
						<div class="col-lg-9">
							#'.$id_customer.' '.ucfirst($customer->firstname).' '.strtoupper($customer->lastname).'
						</div>
					</div>
					<div style="clear:both"></div>';
		if (Tools::getValue('validate', 'false')!='true') {
			$cust_carts = Cart::getCustomerCarts($id_customer, false);
			echo '
					<div class="form-group" style="margin-bottom:3px">
						<label class="control-label col-lg-3">'.$obj->l('Cart', 'reservations').'</label>
						<div class="col-lg-9">
							<select id="select_cart" name="select_cart" onchange="id_cart=$(this).val();myOwnGetSummary();">
								<option value="0">'.ucfirst(myOwnLang::$actions[MYOWN_OPE::CREATE]).'</option>';
							foreach ($cust_carts as $cart_item)
								echo '
								<option value="'.$cart_item['id_cart'].'" '.($id_cart==$cart_item['id_cart'] ? 'selected' : '').'>#'.$cart_item['id_cart'].'  ('.$cart_item['date_upd'].')</option>';
							echo '
						</select>
						</div>
					</div>';
			foreach ($obj->_extensions->getExtByType(extension_type::CART) as $ext) 
				if (method_exists($ext, "displayShoppingCart"))
					echo $ext->displayShoppingCart($obj);
			echo '
			<script type="text/javascript">
				var id_cart = '.$id_cart.';
				if (id_cart) myOwnGetSummary();
				$("#desc-product-save").click(function (event) {
					newOrder('.$customer->id.', id_cart, "validate");
				});
			</script>';
			
		}
		echo '<div style="clear:both"></div>';
			
		if (Tools::getValue('validate', 'false')=='true') {
			 $currencies = Currency::getCurrencies(false, true, true);
			 $languages = Language::getLanguages();
			echo '
			
				<div class="form-group" style="margin-bottom:3px">
					<label class="control-label col-lg-3" for="id_currency">
						'.$obj->l('Currency', 'reservations').'
					</label>
					<script type="text/javascript">
						var currencies = new Array();';
							foreach ($currencies as $currency)
							echo "currencies['".$currency['id_currency']."'] = '".$currency['sign']."';";
						echo '
					</script>
					<div class="col-lg-9">
						<select id="id_currency" name="id_currency">';
							foreach ($currencies as $currency)
								echo '<option rel="'.$currency['iso_code'].'" value="'.$currency['id_currency'].'">'.$currency['name'].'</option>';
							echo '
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:3px">
					<label class="control-label col-lg-3" for="id_lang">
						'.$obj->l('Language', 'reservations').'
					</label>
					<div class="col-lg-9">
						<select id="id_lang" name="id_lang">';
							foreach ($languages as $lang)
								echo '<option value="'.$lang['id_lang'].'">'.$lang['name'].'</option>';
							echo '
						</select>
					</div>
				</div>
				<div style="clear:both"></div>';
		}
		echo '</div>
		</div>';
	}
	
	public static function displayOrderConfirmation($obj, $cookie, $resa, $order, $productsFilter=array()) {
		global $cookie;
		$link = $obj->getContext()->link;
		$id_customer = Tools::getValue('customer');
		$customer = new Customer($id_customer);
		$addresses = $customer->getAddresses($cookie->id_lang);
		$id_cart = Tools::getValue('id_cart');
		$cart = new Cart($id_cart);
		$id_address = $cart->id_address_delivery;
		if (!$id_address) {
			$id_address = Address::getFirstCustomerAddressId($id_customer);
			if ($id_address) {
				$cart->id_address_delivery=$id_address;
				$cart->id_address_invoice=$id_address;
				$cart->save();
			}
		}
		$deliveryOptions = $cart->getDeliveryOptionList();
		
		$summary = $cart->getSummaryDetails();
		$options_list = $deliveryOptions[$id_address];
		
		$modules = Module::getAuthorizedModules($customer->id_default_group);
		$authorized_modules = array();

		foreach ($modules as $module)
			$authorized_modules[] = (int)$module['id_module'];

		$payment_modules = array();
		foreach (PaymentModule::getInstalledPaymentModules() as $p_module)
			if (in_array((int)$p_module['id_module'], $authorized_modules))
				$payment_modules[] = Module::getInstanceById((int)$p_module['id_module']);
				
		foreach ($obj->_extensions->getExtByType(extension_type::HEADER) as $ext) {
			if (method_exists($ext, "saveAttendeesData"))
				$ext->saveAttendeesData($obj, $cart);
		}

		echo '
		<script type="text/javascript">
			var cart_id='.intval($id_cart).';
			var url = "index.php?controller=AdminCarts&token='.Tools::getAdminToken('AdminCarts'.intval(Tab::getIdFromClassName('AdminCarts')).intval($cookie->id_employee)).'";
			$("#delivery_option,#carrier_recycled_package,#order_gift,#gift_message").change(function() {
				myOwnUpdateDeliveryOption();
			});
			$(".fancybox_address").fancybox({
				"type": "iframe",
				"width": "90%",
				"height": "90%",
				
			});
        </script>
        
        	<input type="hidden" name="id_cart" value="'.intval($id_cart).'">
        	<input type="hidden" name="id_customer" value="'.intval($id_customer).'">
        	<input type="hidden" name="submitAddOrder" value="">
			<div class="bootstrap">
				<div class="panel">
					<div class="panel-heading">
						<i class="icon-envelope"></i>
						'.$obj->l('Address', 'reservations').'
					</div>
					<div id="address_delivery" class="col-lg-6">
						<h4>
							<i class="icon-truck"></i>
							'.$obj->l('Delivery', 'reservations').'
						</h4>
						<div class="row-margin-bottom">
							<select id="id_address_delivery" name="id_address_delivery">';
								foreach ($addresses as $address)
									echo '<option value="'.$address['id_address'].'" '.($id_address == $address['id_address'] ? 'selected="selected"' : '').'>'.$address['alias'].'</option>';
								echo '
							</select>
						</div>
						
					</div>
					<div id="address_invoice" class="col-lg-6">
						<h4>
							<i class="icon-file-text"></i>
							'.$obj->l('Invoice', 'reservations').'
						</h4>
						<div class="row-margin-bottom">
							<select id="id_address_invoice" name="id_address_invoice">';
								foreach ($addresses as $address)
									echo '<option value="'.$address['id_address'].'" '.($cart->id_address_invoice == $address['id_address'] ? 'selected="selected"' : '').'>'.$address['alias'].'</option>';
								echo '
							</select>
						</div>
						
					</div>
					<div class="row">
						<div class="col-lg-12">
							<a class="fancybox fancybox_address btn btn-default" id="new_address" href="?tab=AdminAddresses&token='.Tools::getAdminToken('AdminAddresses'.intval(Tab::getIdFromClassName('AdminAddresses')).intval($cookie->id_employee)).'&addaddress&id_customer='.$id_customer.'&liteDisplaying=1&submitFormAjax=1#">								<i class="icon-plus-sign-alt"></i>
								'.$obj->l('Add a new address', 'reservations').'
							</a>
						</div>
					</div>
					<div style="clear:both"></div>
			
				</div>
				<div class="panel">
					<div class="panel-heading">
						<i class="icon-truck"></i>
						'.$obj->l('Delivery', 'reservations').'
					</div>
					<div class="form-group">
						<label class="control-label col-lg-3">'.$obj->l('Mode', 'reservations').'</label>
						<div class="col-lg-9">
							<select name="delivery_option" id="delivery_option">';
							foreach ($options_list as $key => $option)
								foreach ($option['carrier_list'] as $carrier)
								echo '<option value="'.$key.'" '.($cart->id_carrier == $carrier['instance']->id ? 'selected' : '').'>'.$carrier['instance']->name.' - '.$carrier['instance']->delay[$cookie->id_lang].'</option>';
							echo '
							</select>
						</div>
					</div>
						
					<div style="clear:both"></div>
				</div>
			
				<div class="panel">
					<div class="panel-heading">
						<i class="icon-align-justify"></i>
						'.$obj->l('Summary', 'reservations').'
					</div>
					<div id="cart_summary" class="panel row-margin-bottom text-center" style="padding:2px;margin:0px;font-size:11px">
						<div class="row">
							<div class="col-lg-2">
								<div class="data-focus">
									<span><strong>'.ucfirst(myOwnLang::$objects[MYOWN_OBJ::PRODUCT]).'</strong></span><br>
									<span id="total_products" class="size_m text-success"><strong>'.Tools::displayPrice($summary['total_products']).'</strong></span>
								</div>
							</div>
							<div class="col-lg-2">
								<div class="data-focus">
									<span><strong>'.$obj->l('Discounts', 'reservations').'</strong></span><br>
									<span id="total_vouchers" class="size_m text-danger"><strong>'.Tools::displayPrice($summary['total_discounts']).'</strong></span>
								</div>
							</div>
							<div class="col-lg-2">
								<div class="data-focus">
									<span><strong>'.$obj->l('Delivery', 'reservations').'</strong></span><br>
									<span id="total_shipping" class="size_m"><strong>'.Tools::displayPrice($summary['total_shipping']).'</strong></span>
								</div>
							</div>
							<div class="col-lg-2">
								<div class="data-focus">
									<span><strong>'.$obj->l('Taxes', 'reservations').'</strong></span><br>
									<span id="total_taxes" class="size_m"><strong>'.Tools::displayPrice($summary['total_tax']).'</strong></span>
								</div>
							</div>
							<div class="col-lg-2">
								<div class="data-focus">
									<span><strong>'.$obj->l('Total (exc. taxes)', 'reservations').'</strong></span><br>
									<span id="total_taxes" class="size_m"><strong>'.Tools::displayPrice($summary['total_price_without_tax']).'</strong></span>
								</div>
							</div>
							
							<div class="col-lg-2">
								<div class="data-focus data-focus-primary">
									<span><strong>'.$obj->l('Total (inc. taxes)', 'reservations').'</strong></span><br>
									<span id="total_with_taxes" class="size_m"><strong>'.Tools::displayPrice($summary['total_price']).'</strong></span>
								</div>
							</div>
						</div>
					</div>
					
						<div class="form-group">
							<label class="control-label col-lg-3">'.$obj->l('Payment', 'reservations').'</label>
							<div class="col-lg-9">
								<select name="payment_module_name" id="payment_module_name" style="width: 40%;display: inline-block;">';
									foreach ($payment_modules as $pmodule) 
										echo '<option value="'.$pmodule->name.'">'.$pmodule->displayName.'</option>';
									echo '
								</select>
								<a href="javascript:sendMailToCustomer(\''.$link->getAdminLink('AdminOrders').'\');" class="btn btn-default" style="display: inline-block;width: 58%;overflow-x: hidden;">
									<i class="icon-credit-card"></i> '.$obj->l('Send Payment Email', 'reservations').'</a>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-lg-3">État de la commande</label>
							<div class="col-lg-9">
								<select name="id_order_state" id="id_order_state">';
									foreach(myOwnUtils::getStatusList($cookie->id_lang) AS $orderStatus)
										echo '<option value="' . $orderStatus['id_order_state']. '" >' . $orderStatus['name']. '</option>';
								echo '
								</select>
							</div>
						</div>
					
					<div style="clear:both"></div>
				</div>
			
			</div>
			</form>
			';
	}
		
	//==============================================================================================================================================
	//     ORDER DETAILS
	//==============================================================================================================================================
		
	public static function displayOrderDetails($obj, $resa, $reschedule, $order, $actions=array(), $productsFilter=array()) {
		global $cookie;
		$isfront = Tools::getIsset('fc');
		$carriers = MyOwnReservationsUtils::listCarriers();
		$isproversion=is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");
		$payment_modules = array();
		foreach (PaymentModule::getInstalledPaymentModules() as $p_module)
			$payment_modules[] = Module::getInstanceById((int)$p_module['id_module']);
		
		$carrierId=0;$paymentId=0;$custName='';$orderRef='';$advanceAmt=0;$is_advance=false;
		if ($reschedule!=null) {
			$external_fields = explode(';', $reschedule->external_fields);
			$carrierId = $reschedule->_carrier;
			$paymentId = $reschedule->_payment;
			$advanceAmt = $reschedule->advance;
		} else {
			$external_fields = explode(';', $resa->external_fields);
			$carrierId = $resa->_carrier;
			$paymentId = $resa->_payment;
			$advanceAmt = $resa->advance;
		}
		if ($order!=null) {
			$is_advance=($order->total_paid_real!=$order->total_paid);
			if ($isfront) {
				$custName = $resa->_customerName;
				$orderRef= $order->reference;
			} else {
				$custName = '<a href="?tab=AdminCustomers&id_customer='.$resa->_customerId.'&viewcustomer&token='.Tools::getAdminToken('AdminCustomers'.intval(Tab::getIdFromClassName('AdminCustomers')).intval($cookie->id_employee)).'" target="_blank">'.$resa->_customerName.'</a>';
				$orderRef='<a href="?tab=AdminOrders&id_order='.$resa->id_order.'&vieworder&token='.Tools::getAdminToken('AdminOrders'.intval(Tab::getIdFromClassName('AdminOrders')).intval($cookie->id_employee)).'" target="_blank">'.$order->reference.'</a>';
			}
			$advanceAmt = $order->total_paid_real;
		} else if (trim($external_fields[0])!='') {
			if (trim($external_fields[0])!='') $custName = '<i>'.strtoupper($external_fields[0]).'</i>';
			$orderRef='<i>'.$obj->l('external', 'reservations').'</i>';
		}
		
		$paymentName='';$carrierName='';
		if ($resa->_saved) {
			foreach ($carriers as $carrier) if ($carrier['id_carrier']==$carrierId) $carrierName=$carrier['name'];
			foreach ($payment_modules as $payment) if ($payment->id==$paymentId) $paymentName=$payment->displayName;
		}
		
		$editBtn='';
		if ($order==null && $resa->_saved && $isproversion) $editBtn='<a onclick="$(\'#editOrderDetails\').toggle();$(\'#displayOrderDetails\').toggle();" class="btn btn-default btn-subpos myown-btn-edit"  style="float:right;">'.(_PS_VERSION_ < "1.6.0.0" ? '<img src="../img/admin/edit.gif">' : '<i class="icon-pencil"></i>').' '.ucfirst(myOwnLang::$actions[MYOWN_OPE::EDIT]).'</a>';
		
			if ($order==null && !$resa->_saved && !$isproversion) $editBtn=self::editOrderDetails($obj, $carriers, $payment_modules, $resa, $productsFilter);
// 		} else 
		if (_PS_VERSION_ >= "1.6.0.0") {
			//display order
			echo '
			<div class="">
				<div class="panel">
					<div class="panel-heading">
						<i class="icon-credit-card"></i>
						'.ucfirst(myOwnLang::$object[MYOWN_OBJ::ORDER]).'
						'.($orderRef!='' ? ($isfront ? $orderRef : '<span class="badge">'.$orderRef.'</span>') :'').'
						'.($order!=null ?'
							'.($isfront ? '<span class="badge">'.$obj->l('n°', 'reservations').$order->id.'</span>' : '').'
							<span style="float:right"><i class="icon-time"></i> '.Tools::displayDate($order->date_add, null).'</span>
						' : '').$editBtn.'
					</div>
					<div id="displayOrderDetails">
						'.($custName!='' ? '
						<span class="badge">
						'.$custName.'
						</span>' : '').'
						
						'.($carrierName!="" && $carrierName!="0" ? '<span>'.$obj->l('via', 'reservations').' '.$carrierName.'</span>' :'').'
						'.($order!=null ?'
							<span class="label color_field" style="float:right;background-color:'.myOwnUtils::getStatusColor($resa->_orderStatus).';color:white">
								'.strtoupper(substr(myOwnUtils::getStatusName($resa->_orderStatus,$cookie->id_lang),0,22)).'
								</span>
								<div style="clear:both;height:3px"></div>
						':'');
					
			if ($resa->_saved or ($reschedule!=null && !$reschedule->_saved && $reschedule->id_product > 0))
				echo '
						<span class="badge badge-'.($is_advance? 'warn' : 'success').'">
							'.Tools::ps_round($advanceAmt,_PS_PRICE_DISPLAY_PRECISION_).' '.myOwnUtils::getCurrency().'
						</span>
						<span>
							'.($is_advance ?  '('.$obj->l('advance', 'reservations').')' : '').($paymentName!= '' ? ' '.$obj->l('via', 'reservations').' '.$paymentName : '').' '.($is_advance? $obj->l('on a total of', 'reservations').' '.Tools::ps_round($order->total_paid,_PS_PRICE_DISPLAY_PRECISION_).' '.myOwnUtils::getCurrency() : '').'
						</span>';
			echo '</div>';
			
			//edit order	
			if ($isproversion && $actions==array()) 
				echo '
					<div id="editOrderDetails" class="" style="'.($resa->_saved ? 'display:none' : '').'">
					'.myOwnReservationsProController::editOrderDetails($obj, $carriers, $payment_modules, $resa, $productsFilter).'
					</div>';
					
					echo '
				</div>
			</div>';
		} else {
			echo $editBtn.'
			<div id="displayOrderDetails" class="bloc-command" style="margin-top:6px;">
				<div class="metadata-command" style="padding:5px;color:rgb(88, 90, 105);text-shadow: 0 1px 0 #fff;">
					<div style="float:left">'.ucfirst(myOwnLang::$object[MYOWN_OBJ::ORDER]).' '.$orderRef.($order!=null ? ' '.$obj->l('n°', 'reservations').$resa->id_order.'</span></a></div>
					<div style="float:right"><span>'.$order->date_add.'</span>':'').'</div>
					<div class="clear"></div>
					
					<div style="float:left">
						'.$custName.($carrierName!='' && $carrierName!="0" ?' '.$obj->l('via', 'reservations').' '.$carrierName : '').'
					</div>
					'.($order!=null ?'
					<div class="status_label" style="float:right;width:160px;background-color:'.myOwnUtils::getStatusColor($resa->_orderStatus).'">
						<span>'.strtoupper(substr(myOwnUtils::getStatusName($resa->_orderStatus,$cookie->id_lang),0,22)).'</span>
					</div>
					' : '').'
					<div class="clear"></div>
					
					<div style="float:right">
						'.Tools::ps_round($advanceAmt,_PS_PRICE_DISPLAY_PRECISION_).' '.myOwnUtils::getCurrency().' '.($is_advance?  '('.$obj->l('advanced', 'reservations').')' : '').(trim($paymentName)!='' ? ' '.$obj->l('via', 'reservations').' '.$paymentName : '').($is_advance? ' '.$obj->l('on a total of', 'reservations').' '.Tools::ps_round($order->total_paid,_PS_PRICE_DISPLAY_PRECISION_).' '.myOwnUtils::getCurrency() : '').'
					</div>
					<div class="clear"></div>
				</div>
			</div>';
			if ($isproversion) 
				echo myOwnReservationsProController::editOrderDetails($obj, $carriers, $payment_modules, $resa, $productsFilter);
		}
	}
	
		
	//==============================================================================================================================================
	//     PRODUCT DETAILS
	//==============================================================================================================================================
	public static function displayProductSearch($obj, $cookie, $resa, $order, $productsFilter=array()) {
		global $rights;
		$id_customer = Tools::getValue('customer', 0);
		$id_cart = Tools::getValue('id_cart', 0);
		if ($id_cart) $cart=new Cart($id_cart);
		echo MyOwnReservationsUtils::insertTimeslotPickerScript($obj, 'schedule', 0, $obj->l('All', 'availabilities'));
		$args = '
			dateFormat:"yy-mm-dd",
			onSelect : function(date) {
						refreshAvailability();
					},
			beforeShow: function() {
		        setTimeout(function(){
		            $(".datepicker").css("z-index", 99999999999999);
		        }, 0);
		    }';
		echo myOwnUtils::insertDatepicker($obj, array('scheduleStart', 'scheduleEnd'), false, $args, true);
		if ($id_cart) $products=$cart->getProducts();
		else $products=array();
		
		 //object selection
		$obj_sel=null;
		$obj_exts = $obj->_extensions->getExtByType(extension_type::OBJECT);
		foreach ($obj_exts as $obj_ext)
			if ($obj_ext->id==6)
				$obj_sel = $obj_ext;
		
		
		$rescheduleFamily=0;
		if (Tools::getIsset('rescheduleProduct')) {
			$prodFamily = $obj->_products->getResProductFromProduct(Tools::getValue('rescheduleProduct'));
			if ($prodFamily!=null) $rescheduleFamily = $prodFamily->sqlId;
		}

		echo '
		<script type="text/javascript">
		var addcarturl = "index.php?controller=AdminCarts&token='.Tools::getAdminToken('AdminCarts'.intval(Tab::getIdFromClassName('AdminCarts')).intval($cookie->id_employee)).'";
		var id_customer = '.intval($id_customer).';
		var cart_quantity = new Array();
		var currency_format = 5;
		var currency_sign = "";
		var currency_blank = false;
		var priceDisplayPrecision = 2;
		function getAttendeeQty() {
		
		}
		'.(0 && Tools::getIsset('rescheduleProduct') ? 'myOwnSearchProducts("'.MyOwnReservationsUtils::getProductName(Tools::getValue('rescheduleProduct'), $cookie->id_lang).'", '.(int)Tools::getValue('rescheduleProductAttribute').');' : '').'
		$(document).ready(function() {
			$(".datepicker, .ui-datepicker").css("z-index", 99999999999999);
			$("#scheduleStart").on("change", function(e) {
				if ($("#scheduleEnd").length) {
					var start = $("#scheduleStart").datepicker("getDate");
					var end = $("#scheduleEnd").datepicker("getDate");
					if (end<start || $("#scheduleEnd").val()=="") $("#scheduleEnd").datepicker("setDate", start );
				}
			});
			$("#scheduleEnd").on("change", function(e) {
				if ($("#scheduleStart").length) {
					var start = $("#scheduleStart").datepicker("getDate");
					var end = $("#scheduleEnd").datepicker("getDate");
					if (end<start || $("#scheduleStart").val()=="") $("#scheduleStart").datepicker("setDate", end );
				}
			});
			
			refreshAvailability();
		});
		$("#id_product").change(refreshAvailability);
		function attachButtons() {
			$(".product_unit_price").unbind("click");
			$(".product_unit_price").on("change", function(e) {
				e.preventDefault();
				var product = $(this).attr("rel").split("_");
				myOwnUpdateProductPrice(product[0], product[1], $(this).val());
			});
			$(".increaseqty_product, .decreaseqty_product").unbind("click");
			$(".increaseqty_product, .decreaseqty_product").on("click", function(e) {
				e.preventDefault();
				//var resa = $("#scheduleStart").val()+"_"+$("#scheduleStartTimeslot").val()+"@"+$("#scheduleEnd").val()+"_"+$("#scheduleEndTimeslot").val();
				var product = $(this).attr("rel").split("_");
				var sign = "";
				if ($(this).hasClass("decreaseqty_product"))
					sign = "-";
				myOwnUpdateQty(product[0], product[1], product[2], sign+1, "");
			});
			$(".delete_product").unbind("click");
			$(".delete_product").on("click", function(e) {
				e.preventDefault();
				var to_delete = $(this).attr("rel").split("_");
				//myOwnUpdateQty(to_delete[1], to_delete[2], to_delete[3], 0, "");
				myOwnDeleteProduct(to_delete[1], to_delete[2], to_delete[3]);
			});
			$(".attendee_quantity_up").unbind("click");
			$(".attendee_quantity_up").on("click", function(e) {
				e.preventDefault();
			    fieldName = $(this).data("field-qty");
			    var field = $("input[name="+fieldName+"]");
			    var currentVal = parseInt(field.val());
			    var max = field.attr("max");
		
			    if (!isNaN(currentVal) && (currentVal <= max || max==0))
			        $("input[name="+fieldName+"]").val(currentVal + 1).trigger("keyup");
			    //else
			    //    $("input[name="+fieldName+"]").val(quantityAvailableT);
	        
				var product = $(this).attr("rel").split("_");
				var sign = "";

				var cat_qty="";
				//alert($(this).parents("td").html());
				$(this).parents("td").find("input").each(function(index) {
					if ($(this).val()!="") {
						if (cat_qty != "")
							cat_qty += ",";
						cat_qty += $(this).val()+" "+ $(this).attr("key");
					}
				});
				myOwnUpdateQty(product[0], product[1], product[2], sign+1, "", cat_qty);
			});
			$(".attendee_quantity_down").unbind("click");
			$(".attendee_quantity_down").on("click", function(e) {
				e.preventDefault();
				fieldName = $(this).data("field-qty");
			    var field = $("input[name="+fieldName+"]");
			    var currentVal = parseInt(field.val());
			    var min = field.attr("min");
			    if (!isNaN(currentVal) && currentVal > min)
			        $("input[name="+fieldName+"]").val(currentVal - 1).trigger("keyup");
			    else
			        $("input[name="+fieldName+"]").val(min);
	        
				var product = $(this).attr("rel").split("_");
				sign = "-";

				var cat_qty="";
				//alert($(this).parents("td").html());
				$(this).parents("td").find("input").each(function(index) {
					if ($(this).val()!="") {
						if (cat_qty != "")
							cat_qty += ",";
						cat_qty += $(this).val()+" "+ $(this).attr("key");
					}
				});
				myOwnUpdateQty(product[0], product[1], product[2], sign+1, "", cat_qty);
			});
		}
		</script>
		<style type="text/css">
		#customer_cart label.attendee_type  { width:100%;margin-bottom:0px }
		#customer_cart .attendee_quantity_down, .addQuantity .attendee_quantity_down {padding-left:4px;padding-right:4px}
		#customer_cart .attendee_quantity_up, .addQuantity .attendee_quantity_up {padding-left:4px;padding-right:4px}
		#customer_cart .attendee_qty_block {margin-bottom:3px}
		#customer_cart .attendee_form label {margin-bottom:2px}
		#customer_cart .attendee_form input[type=text] {padding: 2px;height: 25px;}
		#customer_cart .attendee_form input[type=radio] {padding: 2px;height: 15px;margin-left:-10px}
		#customer_cart .attendee_form .radio-inline label {font-size: 10px;font-weight: normal;margin-left:2px;padding-left:2px}
		#customer_cart .attendee_form div.radio-inline {padding-left:2px;}
		#customer_cart .attendee_form {margin-bottom:4px}
		#customer_cart .cart_quantity {height: 27px;}
		</style>
		<div class="bootstrap" id="displayProductDetails">
			<div class="panel">
				<div class="panel-heading">
					<button type="button" id="show_old_carts" class="btn btn-default pull-right collapsed" data-toggle="collapse" data-target="#add_products">
						<i class="icon-caret-down"></i>
					</button>
					<i class="icon-AdminCatalog"></i>
					'.$obj->l('Add Products', 'reservations').'
				</div>
				<div id="add_products" class="collapse '.(!$id_cart ? 'in' : '').'">';
				if (0 && $obj->isProVersion && $productsFilter==array()) {
					echo '
					<div class="form-group" style="margin-bottom:5px">	
						<label class="control-label col-lg-3">'.ucfirst(myOwnLang::$object[MYOWN_OBJ::FAMILY]).'</label>
						<div style="display:inline-block">'.MyOwnReservationsUtils::insertProductFamillyPicker($obj, $cookie->id_lang, "scheduleFamily", "", "font-weight:bold;width:75%", Tools::getValue('scheduleFamily', $rescheduleFamily), "display_product_quantity();", false, "" , 0, '.reservationUnit', false).'</div>
					</div>';
				}
				if ($obj_sel!= null)
				echo '<div class="form-group" style="margin-bottom:0px">
						<label class="control-label col-lg-3">
							'.$obj_sel->label.'
						</label>
						'.$obj_sel->productSelectInput($obj, 0, $value->id_object, 'width:50%;', 'refreshAvailability();').'
					</div>';
				echo '
				<div class="form-group" style="margin-bottom:0px">	
					<label class="control-label col-lg-3">
						<span class="mylabel-tooltip" title="'.$obj->l('Search a product by hitting first later of his name', 'reservations').'">
							'.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRODUCT]).'
						</span>
					</label>
					<div style="display:inline-block;width:75%">'.MyOwnReservationsUtils::insertProductPicker($obj, $cookie->id_lang, "scheduleProduct", Tools::getIsset('rescheduleProduct'), "scheduleProductAttribute", Tools::getIsset('rescheduleProductAttribute'), 'width:100%', 'display_product_attributes();display_product_customizations();display_product_quantity();refreshAvailability();if (typeof(populateTimeslot)!=\'undefined\') {populateTimeslot(\'start\');populateTimeslot(\'end\');}if (typeof(populateObject)!=\'undefined\') {populateObject(\'start\');populateObject(\'end\');}', ($productsFilter == array() ? 0 : $productsFilter), "", false).'</div>
					'.(0 ? '<div class="input-group">
						<input type="text" id="scheduleProduct" value="">
						<span class="input-group-addon" style="cursor: pointer;">
							<i class="icon-search"></i>
						</span>
					</div>' : '').'
				</div>
				<div style="padding:0px;border:none;border-radius:5px;margin-bottom: 5px;text-align: center;margin-top: 5px;" id="availability">
						 '. $resa->getAvailableQuantityToString($obj).'
				</div>
				<div class="form-group" style="margin-bottom:0px">		
					<label class="control-label col-lg-3">'.$obj->l('Start', 'reservations').'</label>
					<div style="display:inline-block">
						'.MyOwnReservationsUtils::getDateField('scheduleStart', Tools::getValue('rescheduleStart'), '', 'float:left;z-index:0', 'populateTimeslot(\'start\');').'
						<div style="float:right">
							<select onChange="refreshAvailability();" class="upsInput" id="scheduleStartTimeslot" name="scheduleStartTimeslot" style="float:left;width:120px;display:none"></select><div style="clear:both"></div>
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:0px">			
					<label class="control-label col-lg-3">'.$obj->l('End', 'reservations').'</label>
					<div style="display:inline-block">
						'.MyOwnReservationsUtils::getDateField('scheduleEnd', Tools::getValue('rescheduleEnd'), '', 'float:left;z-index:0', 'populateTimeslot(\'end\');').'
						<div style="float:right">
							<select onChange="refreshAvailability();" class="upsInput" id="scheduleEndTimeslot" name="scheduleEndTimeslot" style="float:left;width:120px;display:none"></select>
						</div>
					</div>
				</div>
				
				<div id="products_found" style="">
					<div id="product_list" class="form-group" style="margin-bottom:5px"></div>
					<div id="attributes_list" class="form-group" style="margin-bottom:5px"></div>
					<div class="form-group addQuantity" id="addQuantity" style="display:none">
						<label class="control-label col-lg-3" for="qty">'.$obj->l('Quantity', 'reservations').'</label>
						<input type="text" name="qty" id="qty" class="form-control fixed-width-sm" value="1">
					</div>';
					foreach ($obj->_extensions->getExtByType(extension_type::SEL_QTY) as $ext) {
						foreach ($obj->_products->list as $mainProduct)
						if (method_exists($ext, "displayProductAction")) {
							$extraQty=$ext->displayProductAction($obj, $mainProduct, $id_product, false, true);
							if ($extraQty!='') echo '<div class="form-group addQuantity" id="addQuantityExtra'.$mainProduct->sqlId.'" style="display:none;margin-bottom:0px">'.$extraQty.'</div>';
						}
					}
					echo '
				</div>
				
				<div class="col-lg-9 col-lg-offset-3" id="addProduct" style="margin-top:5px;">
					<button type="button" class="btn btn-default" onclick="myOwnAddProduct()">
						<i class="icon-ok text-success"></i>
						'.$obj->l('Add to cart', 'reservations').'
					</button>
				</div>
				<div style="clear:both"></div>
			  </div>
			</div>
			
			<div id="products_err" class="alert alert-danger" style="display:none"></div>
			
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-shopping-cart"></i>
					'.$obj->l('Cart', 'reservations').'
					<span id="badge_id_cart" class="badge" style="'.(!$id_cart ? 'display:none' : '').'">N°'.$id_cart.'</span>
				</div>
				<div class="row">
						<table class="table" id="customer_cart">
							<thead>
								<tr>
									<th style="padding:0px"><span class="title_box">'.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRODUCT]).'</span></th>
									<th style="padding:0px"><span class="title_box"></span></th>
									<th style="padding:0px"><span class="title_box">'.$obj->l('Price', 'reservations').'</span></th>
									<th style="padding:0px"><span class="title_box">'.$obj->l('Qty', 'reservations').'</span></th>
									<th style="padding:0px"><span class="title_box">'.$obj->l('Total', 'reservations').'</span></th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
				</div>
			</div>
		</div>
		<script type="text/javascript">
		attachButtons();
		</script>';
	}
	public static function displayProductDetails($obj, $cookie, $resa, $order=null, $productsFilter=array(), $factorized=false) {
		global $rights;
		if (_PS_VERSION_ >= "1.5.0.0")
			$link = $obj->getContext()->link;
		else $link = new Link();
		$id_customer = Tools::getValue('customer', 0);
		$isproversion=is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");
		if ($resa->id_product) {
			$productsImages=MyOwnReservationsUtils::getProductsImages(array($resa->id_product), $cookie->id_lang);
			$viewedProduct=null;
			if (array_key_exists($resa->id_product, $productsImages)) $viewedProduct=$productsImages[$resa->id_product];
			$imageTypes=ImageType::getImagesTypes();
			foreach($imageTypes as $imageType) {
				if ($imageType['name']=='cart_default') $mediumSize=new ImageType($imageType['id_image_type']);
			}
			$image='';
			if ($viewedProduct!=null) $image=$link->getImageLink($viewedProduct->link_rewrite, $viewedProduct->cover, $mediumSize->name);
			$attribute_text=MyOwnReservationsUtils::getAttributeCombinaisons($resa->id_product, $resa->id_product_attribute, $cookie->id_lang);
			$productStr = '<b>'.($resa->quantity > 1 ? $resa->quantity.'x ' : '').MyOwnReservationsUtils::getProductName($resa->id_product, $cookie->id_lang).'</b>'.(trim($attribute_text)!='<br />&nbsp;' ? '<br />'.$attribute_text : '');
		} else {	
			$image='';
		}
		$extraQty='';
		foreach ($obj->_extensions->getExtByType(extension_type::SEL_QTY) as $ext) {
			if (method_exists($ext, "displayProductEdition")) {
				$extraQty = $ext->displayProductEdition($obj, $resa);
			}
		}
		echo '
		<script type="text/javascript">
			$("#rescheduleProduct").change(refreshAvailability);
			refreshAvailability();
		</script>';
		
		if (_PS_VERSION_ >= "1.6.0.0")
			echo '
			<div class="" id="displayProductDetails" style="'.($factorized ? 'float:left;width:320px' :'' ).'">
				<div class="panel" style="margin-top:0px">
					<div class="panel-heading" style="padding:0px">
						<i class="icon-shopping-cart"></i>
						'.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRODUCT]).'
					</div>';
					
			echo '
		<table class="table" width="100%;" cellspacing="0" cellpadding="0" id="documents_table" style="background-color:#FFF">
			<tbody>
				<tr>';
				if ($resa->id_product) echo '
					<input type="hidden" id="rescheduleProduct" name="rescheduleProduct" value="'.$resa->id_product.'">
					<input type="hidden" id="rescheduleProductAttribute" name="rescheduleProductAttribute" value="'.$resa->id_product_attribute.'">
					<input type="hidden" id="rescheduleObject" name="rescheduleObject" value="'.$resa->id_object.'">
					<td style="width:10%;border-bottom:none"><img width="'.$mediumSize->width.'" height="'.$mediumSize->height.'" src="'.$image.'"></td>
					<td colspan="2" align="left" class="" style="border-bottom:none">'.$productStr.'</td>';
				else echo '
					<td colspan="3"  style="border-bottom:none" class="bootstrap">
						'.MyOwnReservationsUtils::insertProductPicker($obj, $cookie->id_lang, "rescheduleProduct", $resa->id_product, "rescheduleProductAttribute", $resa->id_product_attribute, 'width:100%', 'if (typeof(populateTimeslot)!=\'undefined\') {populateTimeslot(\'start\');populateTimeslot(\'end\');}if (typeof(populateObject)!=\'undefined\') {populateObject(\'start\');populateObject(\'end\');}', ($productsFilter == array() ? 0 : $productsFilter), "", false).'
					</td>';
				echo '
					<td class="bootstrap" id="customer_cart" style="text-align:center;border-bottom:none;'.($extraQty!='' && $rights['edit'] && !$factorized ?'padding:0px;width: 75px;':'').'">';
				if ($rights['edit'] && !$factorized ) {
			
					if ($extraQty!='')
						echo '
		<style type="text/css">
		#customer_cart label.attendee_type  { width:100%;margin-bottom:0px }
		#customer_cart .attendee_quantity_down {padding-left:4px;padding-right:4px}
		#customer_cart .attendee_quantity_up {padding-left:4px;padding-right:4px}
		#customer_cart .attendee_qty_block {margin-bottom:3px}
		#customer_cart .attendee_form label {margin-bottom:2px}
		#customer_cart .attendee_form input[type=text] {padding: 2px;height: 25px;}
		#customer_cart .attendee_form input[type=radio] {padding: 2px;height: 15px;margin-left:-10px}
		#customer_cart .attendee_form .radio-inline label {font-size: 10px;font-weight: normal;margin-left:2px;padding-left:2px}
		#customer_cart .attendee_form div.radio-inline {padding-left:2px;}
		#customer_cart .attendee_form {margin-bottom:4px}
		#customer_cart .cart_quantity {height: 27px;}
		</style>
		<script type="text/javascript">
		var addcarturl = "index.php?controller=AdminCarts&token='.Tools::getAdminToken('AdminCarts'.intval(Tab::getIdFromClassName('AdminCarts')).intval($cookie->id_employee)).'";
		var id_customer = '.intval($id_customer).';
		var cart_quantity = new Array();
		var currency_format = 5;
		var currency_sign = "";
		var currency_blank = false;
		var priceDisplayPrecision = 2;
		function getAttendeeQty() {
		
		}
		
		var cat_qty="";
		var cat_qty_total='.$resa->quantity.';
		function attachButtons() {
			$(".attendee_quantity_up").unbind("click");
			$(".attendee_quantity_up").on("click", function(e) {
				e.preventDefault();
			    fieldName = $(this).data("field-qty");
			    var field = $("input[name="+fieldName+"]");
			    var currentVal = parseInt(field.val());
			    var max = field.attr("max");
		
			    if (!isNaN(currentVal) && (currentVal <= max || max==0))
			        $("input[name="+fieldName+"]").val(currentVal + 1).trigger("keyup");
			    //else
			    //    $("input[name="+fieldName+"]").val(quantityAvailableT);

				var sign = "";
				cat_qty="";
				cat_qty_total_tmp=0;
				//alert($(this).parents("td").html());
				$(this).parents("td").find("input").each(function(index) {
					
					if ($(this).val()!="") {
						if (cat_qty != "")
							cat_qty += ",";
						cat_qty += $(this).val()+" "+ $(this).attr("key");
						cat_qty_total_tmp+=parseInt(""+$(this).val());
					}
				});
				cat_qty_total = cat_qty_total_tmp;
				
			});
			$(".attendee_quantity_down").unbind("click");
			$(".attendee_quantity_down").on("click", function(e) {
				e.preventDefault();
				fieldName = $(this).data("field-qty");
			    var field = $("input[name="+fieldName+"]");
			    var currentVal = parseInt(field.val());
			    var min = field.attr("min");
			    if (!isNaN(currentVal) && currentVal > min)
			        $("input[name="+fieldName+"]").val(currentVal - 1).trigger("keyup");
			    else
			        $("input[name="+fieldName+"]").val(min);

				sign = "-";
				cat_qty="";
				cat_qty_total_tmp=0;
				//alert($(this).parents("td").html());
				$(this).parents("td").find("input").each(function(index) {
					if ($(this).val()!="") {
						if (cat_qty != "")
							cat_qty += ",";
						cat_qty += $(this).val()+" "+ $(this).attr("key");
						cat_qty_total_tmp+=parseInt(""+$(this).val());
					}
				});
				cat_qty_total = cat_qty_total_tmp;
			});
		}
		</script>
		'.$extraQty.'
		<script type="text/javascript">
		attachButtons();
		refreshAvailability();
		$("#rescheduleProduct").change(refreshAvailability);
		</script>';
					else echo '<input type="text" size="2" id="quantity" name="quantity" value="'.$resa->quantity.'">';
				} else echo $resa->quantity.'<input type="hidden" id="quantity" name="quantity" value="'.$resa->quantity.'">';
				echo '</td>
					<td style="border-bottom:none;padding:0px"></td>
				</tr>
				<tr>
					<td colspan="5" align="center" style="padding:0px;border:none;border-radius:5px" id="availability">
						 '. $resa->getAvailableQuantityToString($obj).'
					</td>
				</tr>';
			
			//--------------------------------------------------------- product cusctom ---------------------------------------------------------
			
			if ($order!=null) {
				$customized_datas = ProductCore::getAllCustomizedDatas($order->id_cart);
				if (is_array($customized_datas) && array_key_exists($resa->id_product, $customized_datas))
					if (array_key_exists($resa->id_product_attribute, $customized_datas[$resa->id_product])) 
				
						foreach ($customized_datas[$resa->id_product][$resa->id_product_attribute] as $customizationPerAddress) {
							foreach ($customizationPerAddress as $customizationId => $customization) {
								foreach ($customization['datas'] as $type => $datas) {
									if ($type == Product::CUSTOMIZE_FILE) {
										$cnt=0;
										foreach ($datas as $data) 
										{
											$cnt++;
											echo '
											<tr class="hov">
												<td colspan="2">'.($data['name'] ? $data['name'] : 'Picture #'.$cnt).' :</td>
												<td colspan="3">
													<a href="displayImage.php?img='.$data['value'].'&name='.$order->id.'-file'.$cnt.'" target="_blank"><img src="'._THEME_PROD_PIC_DIR_.$data['value'].'_small" alt="" /></a>
												</td>
											</tr>';
										}
									} elseif ($type == Product::CUSTOMIZE_TEXTFIELD) {
										if (count($datas)>1 or $datas[0]["id_customization"]!=99) {
										foreach ($datas as $data)
											if ($data["id_customization"]!=99) 
												echo '
											<tr class="hov">
												<td colspan="2" style="font-size: 11px;padding:2px;font-weight:bold">'.($data['name'] ? ucfirst($data['name']) : 'Text #'.$cnt).' :</td>
												<td colspan="3" style="font-size: 11px;padding:2px" align="right">'.$data['value'].'</td>
											</tr>';
										}
									}
								}
							}
						}
			}
			echo '
			</tbody>
		</table>';
		if (_PS_VERSION_ >= "1.6.0.0") echo '
		</div>
	</div>';
		else echo '
	<div style="height:8px"></div>';
	}
	

	
	public static function editReservationDetails($obj, $cookie, $resa, $reschedule, $actions) {
		global $rights;
		$legend="";
		$editBtn='<a onclick="$(\'#editReservationDetails\').toggle();$(\'#displayReservationDetails\').toggle();$(\'#displayReservationSubDetails\').toggle();" class="btn btn-default btn-subpos myown-btn-edit"  style="float:right;">'.(_PS_VERSION_ < "1.6.0.0" ? '<img src="../img/admin/edit.gif">' : '<i class="icon-pencil"></i>').' '.myOwnLang::$actions[MYOWN_OPE::EDIT].'</a>';
		echo '
		<script type="text/javascript">
		$(document).ready(function() {
			$(".datepicker, .ui-datepicker").css("z-index", 99999999999999);
		});
		</script>';
		if (_PS_VERSION_ >= "1.6.0.0")
			echo '
		<div class="" id="reservationDetails">
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-calendar-o"></i>
					'.ucfirst(myOwnLang::$object[MYOWN_OBJ::RESV]).($resa->_saved ? $editBtn : '').'
				</div>';
		else echo $editBtn;
				
		if ($resa->_saved) {
			echo '
			<div id="displayReservationDetails" class="resa_details_period">
				'.self::displayPeriod($obj, $resa, $cookie->id_lang, false).'
			</div>';
			if ($reschedule!=null && $reschedule->getSelectValue()!=$resa->getSelectValue())
				$legend='<span class="legend" style="text-decoration: line-through">'.$resa->toString($obj, $cookie->id_lang, false).'</span>&nbsp;
						<span class="legend" style="color:rgb(158, 96, 20);">'.$reschedule->toString($obj, $cookie->id_lang, false).'</span>';
		}

		if (_PS_VERSION_ >= "1.6.0.0")
			echo '<div class="well form-horizontal bootstrap" id="editReservationDetails" style="margin: 0px -10px;padding-left: 15px;'.(($resa->_saved or in_array("new", $actions)) ? 'display:none' : '').'">';
		else 
			echo '
		<fieldset id="editReservationDetails" style="'.(($resa->_saved or in_array("new", $actions)) ? 'display:none' : '').'" >
			<legend><img src="../img/admin/date.png">'.ucfirst(myOwnLang::$object[MYOWN_OBJ::RESV]).'</legend>';
			
		echo '

				<div class="form-group" style="clear:both;margin-bottom:5px;">		
					<label style="width: 20%;float:left;padding-top: 7px;">'.$obj->l('Start', 'reservations').'</label>
					<div style="float:left">
						'.MyOwnReservationsUtils::getDateField('rescheduleStart', Tools::getValue('rescheduleStart', ($reschedule!=null ? $reschedule->startDate : $resa->startDate)), '', 'z-index:10000;float:left', 'populateTimeslot(\'start\');').'
						<select class="upsInput" id="rescheduleStartTimeslot" name="rescheduleStartTimeslot" onChange="populateObject(\'start\');" style="float:left;width:120px;height:32px;display:none"></select><div style="clear:both"></div>
					</div>
				</div>
				<div class="form-group" style="clear:both;margin-bottom:5px">	
					<label style="width: 20%;float:left;padding-top: 7px;">'.$obj->l('End', 'reservations').'</label>
					<div style="float:left">
						'.MyOwnReservationsUtils::getDateField('rescheduleEnd', Tools::getValue('rescheduleEnd', ($reschedule!=null ? $reschedule->endDate : $resa->endDate)), '', 'z-index:10000;float:left', 'populateTimeslot(\'end\');').'
						<select class="upsInput" id="rescheduleEndTimeslot" name="rescheduleEndTimeslot" onChange="populateObject(\'end\');" style="float:left;width:120px;height:32px;display:none"></select>
					</div>
				</div>
				<div class="form-group" style="clear:both;margin-bottom:5px">	
					<label style="width: 25%;float:left;padding-top: 7px;">'.$obj->l('Status', 'reservations').'</label>
					<select style="float:left;width:70%;height:32px" id="validationStatus" name="validationStatus">
						<option value="1" '.($resa->validated ? 'selected' : '').' >'.ucfirst(myOwnLang::getOperation($obj, MYOWN_OPE::VALIDATE, false)).'</option>
						<option value="0" '.($resa->validated ? '' : 'selected').' >'.$obj->l('Validation pending', 'reservations').'</option>
					</select>
				</div>';
			//assignation selection
			$assign='';
			$assign_values=array();
			if ($resa->_mainProduct != null && $resa->_mainProduct->productType==product_type::RESOURCE) {
				$assign_obj = $obj->_extensions->list[product_type::RESOURCE];
				$assign = $assign_obj->label;
				foreach($assign_obj->objects->list as $obj_key => $obj_val) {
					$assign_values[$obj_key] = $obj_val->name;
				}
			} elseif ($resa->_mainProduct != null && $resa->_mainProduct->assignment > 0) {
				$assign=$obj->l('Assignment', 'reservations');
				foreach (myOwnUtils::getEmployees($resa->_mainProduct->assignment) AS $employee) {
          			$assign_values[$employee['id_employee']]==$employee['firstname'] . ' ' . $employee['lastname'];
        		}
			}
			if ($assign!='') {
				echo '
				<div class="form-group" style="clear:both;margin-bottom:5px">	
					<label style="width: 25%;float:left;padding-top: 7px;">'.$assign.'</label>
					<select style="float:left;width:70%;height:32px;" id="assignmentEmployee" name="assignmentEmployee">
							<option value="0">'.$obj->l('None', 'reservations').'</option>';
		        		foreach ($assign_values AS $assign_key => $assign_val) {
		          			echo '<option value="' . $assign_key. '"';
							if ($assign_key==$resa->id_assigned) echo ' selected';
							echo '>' . $assign_val . '</option>';
		        		}
		          		echo '
          				</select>';
					echo '
				</div>';
			}
			//object selection
			$obj_sel=null;
			$obj_exts = $obj->_extensions->getExtByType(extension_type::OBJECT);
			//in case of 
			if ($resa->_mainProduct != null) {
				foreach ($resa->_mainProduct->ids_options as $id_option)
					if (array_key_exists($id_option, $obj_exts) && $resa->_mainProduct->productType!=$id_option)
						$obj_sel = $obj_exts[$id_option];
				
				/*if ($obj_sel!=null) {
					echo '
					<div class="form-group" style="clear:both;margin-bottom:5px">	
						<label style="width: 25%;float:left;padding-top: 7px;">'.$obj_sel->label.'</label>
						'.$obj_sel->productSelectInput($obj, $resa->id_product, $resa->id_object, 'float:left;width:70%;height:32px;').'
					</div>';
				}*/
			}
			foreach ($obj_exts as $obj_ext) {
				echo '
					<div id="obj_sel_'.$obj_ext->id.'" class="form-group obj_sel" style="clear:both;margin-bottom:5px;'.($obj_sel!=null && $obj_ext->id==$obj_sel->id ? '' : 'display:none').'">	
						<label style="width: 25%;float:left;padding-top: 7px;">'.$obj_ext->label.'</label>
						'.$obj_ext->productSelectInput($obj, $resa->id_product, $resa->id_object, 'float:left;width:70%;height:32px;').'
					</div>';
			}
			echo '
				<div class="form-group" style="clear:both;margin-bottom:5px">		
					<label style="width: 25%;float:left;padding-top: 7px;">'.$obj->l('Comment', 'reservations').'</label>
					<input id="comment" name="comment" type="text" style="width:95%" value="'.$resa->comment.'">
				</div>
';
		
		if (_PS_VERSION_ >= "1.6.0.0") echo '
				</div>
				'.($resa->id_product ? self::displayReservationSubDetails($obj, $cookie, $resa, $reschedule, $legend) : '').'
			</div>
		</div>';
		else 
			echo '
		</fieldset>
		'.($resa->id_product ? self::displayReservationSubDetails($obj, $cookie, $resa, $reschedule, $legend) : '');

	}
	
	public static function editCustomer($obj, $cookie, $resa, $reschedule, $actions) {
		global $rights;
		$editBtn='';
		$legend='';
		echo '
		<script type="text/javascript">
		var prod_label = "'.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRODUCT]).'";
		var comb_label = "'.$obj->l('Combination', 'reservations').'";
		var detail_lalbel = "'.$obj->l('Detail', 'reservations').'";
		var choose_label = "'.$obj->l('Choose', 'reservations').'";
		var not_found_label = "'.$obj->l('None found', 'reservations').'";
		var searchcustomerurl = "index.php?controller=AdminCustomers&token='.Tools::getAdminToken('AdminCustomers'.intval(Tab::getIdFromClassName('AdminCustomers')).intval($cookie->id_employee)).'";
		if (typeof($(document).typeWatch)!="undefined") {
			$("#scheduleCustomer").typeWatch({
				captureLength: 3,
				highlight: true,
				wait: 100,
				callback: function(){ myOwnSearchCustomers(searchcustomerurl, $("#scheduleCustomer").val()); }
			});
			$("#scheduleProduct").typeWatch({
				captureLength: 3,
				highlight: true,
				wait: 100,
				callback: function(){ myOwnSearchProducts($("#scheduleProduct").val()); }
			});
		}
		$(".fancybox_customer").fancybox({
			"type": "iframe",
			"width": "90%",
			"height": "90%",
			"afterClose" : function () {
				myOwnSearchCustomers(searchcustomerurl, $("#customer").val());
			}
		});
		/*$(".fancybox").fancybox({
			"type": "iframe",
			"width": "90%",
			"height": "90%",
		});*/
		</script>';

		if (_PS_VERSION_ >= "1.6.0.0")
			echo '
		<div id="editCustomer" class="" style="display:none">
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-user"></i>
					'.$obj->l('Customers', 'reservations').$editBtn.'
				</div>';

		if (_PS_VERSION_ >= "1.6.0.0")
			echo '<div style="">';
		else 
			echo '
		<fieldset id="editCustomer" style="display:none" >
			<legend><img src="../img/admin/tab-customers.gif">'.$obj->l('Customers', 'reservations').'</legend>';
			
		/*echo '
		<div class="bootstrap">
			<div id="search-customer-form-group" class="form-group" style="display: block;">
				<label class="control-label col-lg-5">
					<span class="mylabel-tooltip" title="'.$obj->l('Search a customer by hitting first later of his name', 'reservations').'">
						'.$obj->l('Search a customer', 'reservations').'
					</span>
				</label>
					<div class="row">
						<div class="col-lg-6">
							<div class="input-group">
								<input type="text" id="scheduleCustomer" value="">
								<span class="input-group-addon">
									<i class="icon-search"></i>
								</span>
							</div>
						</div>
						<div class="col-lg-7" style="float: right;padding-top: 5px;">
							<span class="form-control-static">Ou&nbsp;</span>
							<a class="fancybox_customer btn btn-default" href="index.php?controller=AdminCustomers&token='.Tools::getAdminToken('AdminCustomers'.intval(Tab::getIdFromClassName('AdminCustomers')).intval($cookie->id_employee)).'&addcustomer&liteDisplaying=1&submitFormAjax=1#">
								<i class="icon-plus-sign-alt"></i>
								'.$obj->l('Add a New Customer', 'reservations').'
							</a>
						</div>
					</div>
			</div>
			<div style="clear:both"></div>
		</div>';*/
		
		echo '<div class="row">
			<div id="customers" class=""><div class="bootstrap"><div class="alert alert-info">'.$obj->l('Search a customer by hitting first later of his name', 'reservations').'</div></div></div>
		</div>';
			
		
		if (_PS_VERSION_ >= "1.6.0.0") echo '
				</div>
			</div>
		</div>';
		else 
			echo '
		</fieldset>';

	}
	
	public static function displayReservationSubDetails($obj, $cookie, $resa, $reschedule,$legend) {
		global $rights;
		$isproversion=is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");
		$adjustPrice = ($reschedule!=null && ($reschedule->getSelectValue() != $resa->getSelectValue() or $reschedule->quantity != $resa->quantity));
		$productPrice_with_taxes = MyOwnReservationsUtils::getProductPriceTaxes($resa->id_product, $resa->id_product_attribute, true, ($resa->_mainProduct!=null ? $resa->_mainProduct->_optionAlsoSell : false), $desiredQty);
		if (trim($resa->discounts)!='') $discounts = explode(";",$resa->discounts);
		else $discounts=array();
		
		$output =''; 
		$output .= '
		<table class="table" width="100%;" cellspacing="0" cellpadding="0" id="displayReservationSubDetails" style="background-color:#FFF">
			'.($legend !='' ? '
			<thead>
				<tr>
					<th colspan="2" style="text-align:right;font-style:italic">'.$legend.'</th>
				</tr>
			</thead>
			' : '').'
			<tbody>';
			if ($reschedule!=null)
			$output .=  '
				<tr class="hov">
					<td align="left">'.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRODUCT]).'</td>
					<td align="right">'.($resa->quantity != $reschedule->quantity ? '<span style="text-decoration: line-through">'.$resa->quantity.'x</span> <span style="color:rgb(158, 96, 20);font-weight:bold">'.$reschedule->quantity.'x</span>' : $resa->quantity.'x').' <b>'.MyOwnReservationsUtils::getProductName($resa->id_product, $cookie->id_lang).'</b><br/>'.(strlen($attribute_text)>40 ? substr(str_replace ("<br />", ', ', $attribute_text), 0, 40).'...' : $attribute_text).'</td>
				</tr>
				<tr class="hov">
					<td align="left">'.$obj->l('Status', 'reservations').'</td>
					<td align="right">'.(trim($resa->validated) != trim($reschedule->validated) ? '<span style="text-decoration: line-through">'.$resa->statusToString($obj).'</span> <span style="color:rgb(158, 96, 20);font-weight:bold">'.$reschedule->statusToString($obj).'</span>' : $resa->statusToString($obj, true)).'</td>
				</tr>';
			if ($isproversion && $reschedule!=null && ($reschedule->stock!="" or trim($resa->stock) != trim($reschedule->stock) ) ) { 
				if (stripos($resa->stock, '#')!==false || stripos($reschedule->stock, '#')!==false) {
					$reschedulestockitem=$obj->_extensions->list[product_type::RESOURCE]->objects->list[(int)str_ireplace('#', '', $reschedule->stock)];
					$resastockitem=$obj->_extensions->list[product_type::RESOURCE]->objects->list[(int)str_ireplace('#', '', $resa->stock)];
				$output .=  '
				<tr class="hov">
					<td align="left">'.$obj->l('Resource', 'reservations').'</td>
					<td align="right">'.(trim($resa->stock) != trim($reschedule->stock) ? '<span style="text-decoration: line-through">'.str_ireplace(';', ', ', $resa->stock).' '.$resastockitem->name.'</span> <span style="color:rgb(158, 96, 20);font-weight:bold">'.str_ireplace(';', ', ', $reschedule->stock).' '.$reschedulestockitem->name.'</span>' : str_ireplace(';', ', ', $resa->stock)).$stockitem->name.'</td>
				</tr>';
				} else $output .=  '
				<tr class="hov">
					<td align="left">'.ucfirst(myOwnLang::$objects[MYOWN_OBJ::STOCKITEM]).'</td>
					<td align="right">'.(trim($resa->stock) != trim($reschedule->stock) ? '<span style="text-decoration: line-through">'.str_ireplace(';', ', ', $resa->stock).'</span> <span style="color:rgb(158, 96, 20);font-weight:bold">'.str_ireplace(';', ', ', $reschedule->stock).'</span>' : str_ireplace(';', ', ', $resa->stock)).'</td>
				</tr>';
			}
			
			if ($reschedule!=null && trim($reschedule->comment)!='')
				$output .=  '
				<tr class="hov">
					<td align="left">'.$obj->l('Comment', 'reservations').'</td>
					<td align="right">'.(trim($resa->comment) != trim($reschedule->comment) ? '<span style="text-decoration: line-through">'.$resa->comment.'</span> <span style="color:rgb(158, 96, 20);font-weight:bold">'.$reschedule->comment.'</span>' : $resa->comment).'</td>
				</tr>';

				
			if (!$rights['edit'] or $reschedule==null)
			$output .=  '
				<tr class="hov"">
					<td align="left">'.$obj->l('Status', 'reservations').'</td>
					<td align="right">'.$resa->statusToString($obj, true).'<input type="hidden" id="validationStatus" name="validationStatus" value="'.intval($resa->validated).'" ></td>
				</tr>';

/*-----Length----*/
			$output .=  '
				<tr class="hov">
					<td align="left">'.$obj->l('Length', 'reservations').'</td>
					<td align="right">
					<span style="'.($reschedule!=null && $reschedule->getLength(true) != $resa->getLength(true) ? 'text-decoration: line-through' : '').'">'.$resa->lengthToString($obj, $cookie->id_lang).'</span>
					'.($reschedule!=null && $reschedule->getLength(true) != $resa->getLength(true) ? '<span style="color:rgb(158, 96, 20);font-weight:bold">'.$reschedule->lengthToString($obj, $cookie->id_lang).'</span>' : '').'
					</td>
				</tr>';
				
/*-----Unit price----*/
			if ($resa->_mainProduct != null) $output .=  '
				<tr class="hov">
					<td align="left">'.$obj->l('Unit price', 'reservations').'</td>
					<td align="right">'.Tools::ps_round($productPrice_with_taxes).' '.myOwnUtils::getCurrency().(($resa->_mainProduct->_reservationLengthControl == reservation_length::NONE or $resa->_mainProduct->_reservationPriceType==reservation_price::PRODUCT_DATE) ? ' ' : '/'.$resa->unitToString($obj, $cookie->id_lang)).'</td>
				</tr>
				';

/*-----Discounts----*/
			foreach ($discounts as $discount) {
				$discountdetails = explode(":",$discount);
				if ($display_price_wt) $reduc = $discountdetails[1]*(1+($resa->tax_rate/100));
				else $reduc = $discountdetails[1];
				$output .=  '
				<tr style="'.($reschedule!=null && $reschedule->getPrice() != $resa->getPrice() && $adjustPrice ? 'text-decoration: line-through' : '').'">
					<td align="left">'.$obj->l('Discount', 'reservations').' "'.$discountdetails[0].'"</td>
					<td align="right">'.Tools::ps_round(-$reduc,_PS_PRICE_DISPLAY_PRECISION_).' '.myOwnUtils::getCurrency().'</td>
				</tr>';
			}
			if ($reschedule!=null && $reschedule->getPrice() != $resa->getPrice() && $adjustPrice) {
				if (trim($reschedule->discounts)!='') $discounts = explode(";",$reschedule->discounts);
				else $discounts=array();
				foreach ($discounts as $discount) {
					$discountdetails = explode(":",$discount);
					$output .=  '
					<tr>
						<td align="left" style="color:rgb(158, 96, 20);font-weight:bold">'.$obj->l('Discount', 'reservations').' "'.$discountdetails[0].'"</td>
						<td align="right" style="color:rgb(158, 96, 20);font-weight:bold">'.Tools::ps_round($discountdetails[1]*(1+($resa->tax_rate/100)),_PS_PRICE_DISPLAY_PRECISION_).' '.myOwnUtils::getCurrency().'</td>
					</tr>';
				}
			}
/*-----Price----*/
			if ($resa->_mainProduct != null) $output .=  '
				<tr class="hov">
					<td align="left">'.ucfirst(myOwnLang::$object[MYOWN_OBJ::RESV]).'</td>
					<td align="right"><span style="'.($reschedule!=null && $reschedule->getPriceTaxes(true) != $resa->getTotalPriceSaved(true) && $adjustPrice ? 'text-decoration: line-through' : '').'">'.($resa->_saved ? $resa->getTotalPriceSaved(true) : $resa->getPriceTaxes(true)).' '.myOwnUtils::getCurrency().'</span>
					'.($reschedule!=null && $reschedule->getPriceTaxes(true) != $resa->getTotalPriceSaved(true) && $adjustPrice ? '<span style="color:rgb(158, 96, 20);font-weight:bold">'.$reschedule->getPriceTaxes(true).' '.myOwnUtils::getCurrency().'</span>' : '').'</td>
				</tr>';
				
/*-----Assignment----*/

			if ($resa->_mainProduct != null && $resa->_mainProduct->assignment > 0 && ($resa->id_assigned || ($reschedule!=null && $reschedule->id_assigned))) {
				$assign='';
				$assign_values=array();
				if ($resa->_mainProduct->productType==product_type::RESOURCE) {
					$assign_obj = $obj->_extensions->list[product_type::RESOURCE];
					$assign = $assign_obj->label;
					foreach($assign_obj->objects->list as $obj_key => $obj_val) {
						$assign_values[$obj_key] = $obj_val->name;
					}
				} elseif ($resa->_mainProduct->assignment > 0) {
					$assign=$obj->l('Assignment', 'reservations');
					foreach (myOwnUtils::getEmployees($resa->_mainProduct->assignment) AS $employee) {
	          			$assign_values[$employee['id_employee']]==$employee['firstname'] . ' ' . $employee['lastname'];
	        		}
				}
			
				$output .=  '
				<tr class="hov">
					<td align="left">'.$assign.'</td>
					<td align="right"><span style="'.($reschedule!=null && $reschedule->id_assigned != $resa->id_assigned ? 'text-decoration: line-through' : '').'">'.$assign_values[$resa->id_assigned].'</span>
					'.($reschedule!=null && $reschedule->id_assigned != $resa->id_assigned ? '<span style="color:rgb(158, 96, 20);font-weight:bold">'.$assign_values[$reschedule->id_assigned].'</span>' : '').'</td>
				</tr>';
			}

/*-----Ext object----*/
			$ext_object = null;
			foreach ($obj->_extensions->getExtByType(extension_type::OBJECT) as $ext) 
				if (method_exists($ext, "objectSelect"))
					if ($resa->_mainProduct!=null && in_array($ext->id, $resa->_mainProduct->ids_options))
						$ext_object = $ext;
			if ($ext_object!=null && intval($resa->id_object)!=0  && array_key_exists($resa->id_object, $ext_object->objects->list))
			$output .=  '
				<tr class="hov">
					<td align="left">'.$ext_object->label.'</td>
					<td align="right">'.$ext_object->objects->list[$resa->id_object]->name.'</td>
				</tr>';

/*-----Comment----*/	
			if ($reschedule==null && trim($resa->comment)!='')
				$output .=  '
				<tr class="hov">
					<td align="left">'.$obj->l('Comment', 'reservations').'</td>
					<td align="right">'.$resa->comment.'</td>
				</tr>';
				
			//Add extenssion display
			foreach ($obj->_extensions->getExtByType(extension_type::RESADETAILS) as $ext) 
				if ($ext->enabled)
					if (in_array($ext->id, $resa->_mainProduct->ids_notifs) || $resa->_mainProduct->_qtyType==$ext->id)
						if (method_exists($ext, "displayReservationDetails"))
							$output .=  $ext->displayReservationDetails($obj, $resa);
				
			$output .=  '
			</tbody>
		</table><div style="height:8px"></div>';
		return $output;
	}
	
	public static function displayPeriod($obj, $resa, $langue, $small) {
		global $smarty, $cookie;
		//one ts
		if ($resa->_mainProduct!= null)
			$_timeSlots = $resa->_mainProduct->_timeslotsObj;
		else $_timeSlots = $obj->_timeSlots;
		
		if ($resa->_mainProduct!= null && $resa->_mainProduct->_reservationSelExt!=null) return $resa->_mainProduct->_reservationSelExt->toString($obj, $resa, $langue, $small);

		$resaStr='';
		if (($resa->_mainProduct!=null && $resa->_mainProduct->_reservationLengthControl == reservation_length::NONE)
			or ($resa->startDate==$resa->endDate && $resa->startTimeslot==$resa->endTimeslot)) {
			$resaStr .= $obj->l('On(date)', 'cart').' '.MyOwnCalendarTool::formatDateCompact(strtotime($resa->startDate), $langue);
			if ($resa->getStartTimeSlot()!=null)
				if ($resa->_mainProduct->_reservationShowTime) 
					$resaStr .= ' '.MyOwnCalendarTool::formatTime($resa->getStartHour($obj), $langue);
				else if ($resa->_mainProduct->reservationSelType==reservation_interval::TIMESLOT)
					$resaStr .= ' '.$resa->getStartTimeSlot()->name;
			
			//same day different ts
		} else if ($resa->startDate==$resa->endDate) {
			$resaStr .= $obj->l('On(date)', 'cart').' '.MyOwnCalendarTool::formatDate(strtotime($resa->startDate), $langue);
			if ($resa->startTimeslot && $resa->getStartHour($obj)!=$resa->getEndHour($obj)) 
				$resaStr .= ', '.$obj->l('from(time)', 'cart').' '.MyOwnCalendarTool::formatTime($resa->getStartHour($obj), $langue).' '.$obj->l('to(time)', 'cart').' '.MyOwnCalendarTool::formatTime($resa->getEndHour($obj), $langue);
			
		//different days
		} else {
			$displayMonth=(date('Y-m', strtotime($resa->startDate)) != date('Y-m', strtotime($resa->endDate)));
			$displayYear=(date('Y', strtotime($resa->startDate)) != date('Y', strtotime($resa->endDate)));

			$resaStr .= $obj->l('From(date)', 'cart').' '.MyOwnCalendarTool::formatDate(strtotime($resa->startDate), $langue, $displayMonth, $displayYear).' ';
			if ($resa->startTimeslot) 
				$resaStr .= MyOwnCalendarTool::formatTime($resa->getStartHour(), $langue).($small ? '<br>' : ', ');
			$resaStr .= $obj->l('To(date)', 'cart').' '.MyOwnCalendarTool::formatDate(strtotime($resa->endDate), $langue).' ';
			if ($resa->startTimeslot) 
				$resaStr .= MyOwnCalendarTool::formatTime($resa->getEndHour(), $langue);
		}
		$startKeys = array();
		$startKeys['hour']=$resa->getStartHour($obj);
		$startKeys['day']=date('Y-m-d', strtotime($resa->startDate));
		$startKeys['week']=date('o-W', strtotime($resa->startDate));
		$startKeys['month']=date('Y-m', strtotime($resa->startDate));
		$startKeys['year']=date('Y', strtotime($resa->startDate));
		$endKeys = array();
		$endKeys['hour']=$resa->getEndHour($obj);
		$endKeys['day']=date('Y-m-d', strtotime($resa->endDate));
		$endKeys['week']=date('o-W', strtotime($resa->endDate));
		$endKeys['month']=date('Y-m', strtotime($resa->endDate));
		$endKeys['year']=date('Y', strtotime($resa->endDate));
		//getDayString($date,$langue,$short=false)
		$calendar = new MyOwnCalendarTool();
		$smarty->assign(array(
			'showTime' => ($resa->_mainProduct->_reservationShowTime or $resa->_mainProduct->isTimeSelect()),
			'weekTs' => MYOWNRES_WEEK_TS,
			'id_lang' => $cookie->id_lang,
			'startDate' => strtotime($resa->startDate),
			'endDate' => strtotime($resa->endDate),
			'resa' => $resa,
			'myownreservations' => $obj,
			'startKeys' => $startKeys,
			'endKeys' => $endKeys,
			'calendar' => $calendar,
			'resaStr' => $resaStr,
			'arrowCount' => min(10, $resa->getLength())
		));
		$resaDisplay=$obj->display(MYOWNRES_PATH.$obj->name.'.php','views/templates/admin/reservation_display.tpl');
		if (_PS_VERSION_ < "1.6.0.0") return '<div style="margin-top:12px">'.$resaDisplay.'</div>';
		else return $resaDisplay;
	}
	
	//==============================================================================================================================================
	//     SHOW RESERVATION
	//==============================================================================================================================================

	public static function showFactorize($obj, $cookie, $resas, $actionsArrays) {
		global $rights;
		global $ids;
		$qty=0;
		$ids=array();
		$check='';
		$warning='';
		$isresasaved=false;
		foreach ($resas as $resa) {
			$check .= $actionsArrays[$resa->sqlId]['label'];
			$warning .= $actionsArrays[$resa->sqlId]['warning'];
			$result .= $actionsArrays[$resa->sqlId]['result'];
			$qty+=$resa->quantity;
			$ids[]=$resa->sqlId;
			if ($resa->_saved) $isresasaved=true;
		}
		$resa = $resas[0];
		if ((int)Tools::getValue('productFamilyFilter', 0)) {
			$productFamilyFilter = (int)Tools::getValue('productFamilyFilter', 0);
			if (array_key_exists($productFamilyFilter, $obj->_products->list))
				$resa->_mainProduct = $obj->_products->list[$productFamilyFilter];
		}
		if ((int)Tools::getValue('objectFilter', 0))
			$resa->id_object = (int)Tools::getValue('objectFilter', 0);
	
		$page_header_toolbar_btn=array();
		$page_header_toolbar_btn['save'] = array(
			'desc' => ucfirst(myOwnLang::$actions[MYOWN_OPE::SAVE]),
			'js' => 'editFactorised(\''.implode(',', $ids).'\');',
			'icon' => ($isfront ? 'icon-save' : 'process-icon-save-and-stay')
		);
		$page_header_toolbar_btn['add'] = array(
			'desc' => ucfirst(myOwnLang::$actions[MYOWN_OPE::ADD]),
			'js' => 'showResaBox(0, true);showResaDetails(9999999999, \''.$resa->startDate.'_'.(int)$resa->startTimeslot.'#'.$resa->endDate.'_'.$resa->endTimeslot.'\', \''.$resa->id_product.'-'.$resa->id_product_attribute.'\');',
			'icon' => ($isfront ? 'icon-add' : 'process-icon-new')
		);
		$page_header_toolbar_btn['close'] = array(
			'desc' => $obj->l('Close', 'reservations'),
			'js' => 'cleanResaBox('.$resa->sqlId.');$.fancybox.close();',
			'icon' => ($isfront ? 'icon-remove' : 'process-icon-cancel')
		);
		$buttons = MyOwnReservationsUtils::getButtonsList($page_header_toolbar_btn);
		
		echo '
		<script type="text/javascript">
	        $(document).ready(function() {
		        $("#myOwnReservationContent").parent().attr("style", "width:640px");
		        $("#myOwnReservationContent").parent().parent().attr("style", "width:640px");
	        });
		</script>
		
		<div id="reservationContent" class="reservationContent reservationContent16 '.($isfront ? 'frontContent' : '').'" style="width:640px;min-height:400px;padding:0px;padding-bottom:1px;background-color:rgb(204, 204, 204)">
			
			<div class="page-head page-'.($isfront ? 'front-' : '').'head-myown bootstrap">
				<h2 class="page-title" stye="float: left;">
					'.ucfirst(myOwnLang::$objects[MYOWN_OBJ::RESV]).intval($resa->_mainProduct!=null).'
				</h2>
				<center><div id="actionWaiting" style="padding-top:20px;display:none"><img src="../img/loader.gif"></div></center>
				<div class="page-bar toolbarBox" id="content">
					'.$buttons.'
					<div style="clear:both"></div>
				</div>
				<div style="clear:both"></div>
			</div>
			<div style="padding-left: 10px;padding-right: 10px;padding-top: 10px;">
			'.$result;
			
		if ($warning!='') echo '<div id="warning" class="bootstrap">
				'.(_PS_VERSION_ >= "1.6.0.0" ? '
				<div class="alert alert-warning" style="margin-bottom:0px;">
				' : '
				<div id="content" style="background:none">
				').'<div class="error">'.$warning.'</div></div></div>';

		$origqty = $resa->quantity;
		foreach($resas as $key => $tmpresa)
			if ($tmpresa->sqlId!=$resa->sqlId)
				$resa->quantity += $tmpresa->quantity;
		echo self::displayProductDetails($obj, $cookie, $resa, null, array(), true);
		$resa->quantity = $origqty;

		$ext_object = null;
			foreach ($obj->_extensions->getExtByType(extension_type::OBJECT) as $ext) 
				if (method_exists($ext, "objectSelect"))
					if ($resa->_mainProduct!=null && in_array($ext->id, $resa->_mainProduct->ids_options))
						$ext_object = $ext;
			if ($ext_object!=null && intval($resa->id_object)!=0  && array_key_exists($resa->id_object, $ext_object->objects->list))
			echo  '
		<div class="" id="objectDetails" style="float:right;width:280px">
			<div class="panel bootstrap" style="margin-top:0px">
				<div class="panel-heading">
					<i class="icon-'.$ext_object->icon.'"></i>
					 '.$ext_object->label.'
				</div>
				<p style="margin:0px;text-align:right;margin-top:-20px;">'.$ext_object->objects->list[$resa->id_object]->name.'</p>
				'.($resa->id_object ? '<input type="hidden" id="rescheduleObject" value="'.$resa->id_object.'">' : '').'
			</div>
		</div>';
		
		
		echo '
		<div class="" id="reservationDetails" style="float:right;width:280px">
			<div class="panel bootstrap" style="margin-top:0px">
				<div class="panel-heading">
					<i class="icon-calendar-o"></i>
					'.$obj->l('Period', 'reservations').'
				</div>';
				
		echo '
			<div id="displayReservationDetails" class="resa_details_period">
				'.self::displayPeriod($obj, $resa, $cookie->id_lang, false).'
				<input type="hidden" id="rescheduleStartTimeslot" value="'.$resa->startTimeslot.'">
				<input type="hidden" id="rescheduleStart" value="'.$resa->startDate.'">
				<input type="hidden" id="rescheduleEndTimeslot" value="'.$resa->endTimeslot.'">
				<input type="hidden" id="rescheduleEnd" value="'.$resa->endDate.'">
			</div>';
			
			
		echo '</div>
		</div>
		
		<div style="clear:both"></div>';
		$isproversion=is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");
		$detailsExt = null;
		foreach ($obj->_extensions->getExtByType(extension_type::RESADETAILS) as $ext) 
			if ($ext->enabled)
				if ($resa->_mainProduct->_qtyType==$ext->id)
					if (method_exists($ext, "displayReservationDetails"))
						$detailsExt = $ext;

		echo '
		<div class="" id="subDetails">
			<div class="panel bootstrap" style="padding:10px;margin-top:0px">
				<div class="panel-heading" style="padding:10px">
					<i class="icon-ticket"></i>
					'.$obj->l('Details', 'reservations').'
				</div>';
				
		echo '
		<table class="table" width="100%;" cellspacing="0" cellpadding="0" id="displayReservationSubDetails" style="background-color:#FFF">
			<thead>
				<tr>
					'.(0 && $rights['delete'] && $detailsExt!=null ? '<th></th>' :'' ).'
					<th>'.($detailsExt != null ? $detailsExt->displayReservationDetails($obj, $resa, 0) : $obj->l('Item', 'reservations')).'
					</th>
					'.($isproversion ? '<th width="110px" ><i class="icon-bookmark"></i> '.$obj->l('Assign', 'reservations').'</th>' : '').'
					<th width="15%" ><i class="icon-credit-card"></i> '.ucfirst(myOwnLang::$object[MYOWN_OBJ::ORDER]).'</th>
					<th width="20%" style="text-align:right"><i class="icon-calendar-o"></i> '.ucfirst(myOwnLang::$object[MYOWN_OBJ::RESV]).'</th>
				</tr>
			</thead>
			<tbody>';
			//for ($i=1; $i<$qty+1; $i++)
			foreach($resas as $resa) {
				if ($resa->id_order>0) {
					$order=new Order($resa->id_order);
				} else {
					$order=null;
				}
				
				if ($order!=null) {
					$orderDetails = '
					<td rowspan="'.$resa->quantity.'" align="center">
						<span style="float:right"><i class="icon-time"></i> '.Tools::displayDate($order->date_add, null).'</span>
						<span class="badge" style="background-color:'.myOwnUtils::getStatusColor($resa->_orderStatus).'"><a style="cursor:pointer;color:#444;cursor:pointer" href="?tab=AdminOrders&id_order='.$resa->id_order.'&vieworder&token='.Tools::getAdminToken('AdminOrders'.intval(Tab::getIdFromClassName('AdminOrders')).intval($cookie->id_employee)).'" target="_blank">'.$resa->ref_order.'</a></span>
						
					</td>
					<td rowspan="'.$resa->quantity.'" align="right">
						<span class="badge" style="background-color:'.($resa->validated ? '#72C279' : '#FFF3D7').'"><a style="color:#444;cursor:pointer" onclick="showResaBox('.$resa->sqlId.');showResaDetails('.$resa->sqlId.');">'.$resa->reference.'</a></span>';
					if ($resa->_mainProduct != null && $resa->_mainProduct->assignment > 0) {
						$assign='';
						$assign_values=array();
						if ($resa->_mainProduct->productType==product_type::RESOURCE) {
							$assign_obj = $obj->_extensions->list[product_type::RESOURCE];
							$assign = $assign_obj->label;
							foreach($assign_obj->objects->list as $obj_key => $obj_val) {
								$assign_values[$obj_key] = $obj_val->name;
							}
						} elseif ($resa->_mainProduct->assignment > 0) {
							$assign=$obj->l('Assignment', 'reservations');
							foreach (myOwnUtils::getEmployees($resa->_mainProduct->assignment) AS $employee) {
			          			$assign_values[$employee['id_employee']]==$employee['firstname'] . ' ' . $employee['lastname'];
			        		}
						}
					
						$orderDetails .= '
						<select  style="width:100%; height:22px;margin-top:2px" id="assignmentEmployee" class="assignmentEmployee" name="assignmentEmployee'.$resa->sqlId.'">
							<option value="0">'.$obj->l('None', 'reservations').'</option>';
		        		foreach ($assign_values AS $assign_key => $assign_val) {
		          			$orderDetails .= '<option value="' . $assign_key. '"';
							if ($assign_key==$resa->id_assigned) $orderDetails .= ' selected';
							$orderDetails .= '>' . $assign_val . '</option>';
		        		}
		          		$orderDetails .= '
          				</select>';
					}
					$orderDetails .= '
					</td>';
					//<a href="?tab=AdminCustomers&id_customer='.$resa->_customerId.'&viewcustomer&token='.Tools::getAdminToken('AdminCustomers'.intval(Tab::getIdFromClassName('AdminCustomers')).intval($cookie->id_employee)).'" target="_blank">'.$resa->_customerName.'</a>
				} else $orderDetails = 'new';
				
				if ($resa->_saved) {
					if ($isproversion) {
						echo myOwnReservationsProController::editStockDetails($obj, $resa, $rights, $orderDetails);
						
						
					} else 
						for ($qty=1; $qty<$resa->quantity+1; $qty++) {
							echo '
					<tr class="hov" >
						<td align="left">';
				
						if ($detailsExt != null)
							echo $detailsExt->displayReservationDetails($obj, $resa, $qty);
						else echo $obj->l('Item', 'reservations').' #'.$qty;
				
						echo '
						</td>';
						if ($qty==1) echo $orderDetails;
						echo '
					</tr>';
					}
				}
			}
		echo '
			</tbody>
		</table>';
		
		if ($isresasaved) echo self::editItemsMenu($obj, $rights, $detailsExt);
		
		echo '</div>
		</div>';
		
		echo '</div>
		</div>';
	}
	
	public static function showReservation($obj, $cookie, $resa, $reschedule, $actions, $check, $productsFilter=array()) {
		global $rights;
		global $cookie;
		
		$isfront = Tools::getIsset('fc');
		$saveAction='';
		
		if ($resa->_saved && $resa->_mainProduct == null) $check = '<div id="" style="background:none">'.myOwnUtils::displayError($obj->l('No reservation rules found', 'reservations')).'</div></div>';

		$isproversion=is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");
		if ($resa->id_order>0) 
			$order=new Order($resa->id_order);
		//--------------------------------------------------------- head ---------------------------------------------------------
		if ($saveAction=="") {
			if ($reschedule==null) {
				if ($resa->sqlId > 0 && $resa->_saved) $saveAction = 'editForm('.$resa->sqlId.');';
				else {
					$customer = Tools::getValue('customer');
					if ($customer!='')
						$saveAction = "newOrder(".$customer.", id_cart);";
					else $saveAction = "newForm();";
				}
			} else {
				if ($resa->_saved) $saveAction = 'showResaDetails('.$resa->sqlId.');';
				else $saveAction = 'newForm('.$resa->sqlId.');';
			}
		}
		$page_header_toolbar_btn=array();
		if (!in_array("clear", $actions) ) {
			if (!in_array("new", $actions) && Tools::getValue('create')=='') { //&& !in_array("backward", $actions) && !in_array("forward", $actions) && !in_array("shorten", $actions) && !in_array("extend", $actions) && !in_array("reschedule", $actions)
				if ($resa->_saved && $rights['delete'])
					$page_header_toolbar_btn['delete'] = array(
						'desc' => ucfirst(myOwnLang::$actions[MYOWN_OPE::DELETE]),
						'js' => 'editForm('.$resa->sqlId.', true);',
						'icon' => ($isfront ? 'icon-trash' : 'process-icon-delete')
					);
				if ($rights['edit'])
					$page_header_toolbar_btn['save'] = array(
						'desc' => ucfirst(myOwnLang::$actions[MYOWN_OPE::SAVE]),
						'js' => $saveAction,
						'icon' => ($isfront ? 'icon-save' : 'process-icon-save-and-stay')
					);
			}
			if (Tools::getValue('create')!='' && !$isfront) {
				$customer = Tools::getValue('customer');
				$validate = (Tools::getValue('validate', 'false')=='true');

				$page_header_toolbar_btn['prev'] = array(
					'desc' => $obj->l('Previous', 'reservations'),
					'js' => ($validate ? 'newOrder('.$customer.', id_cart);' : 'newOrder(0, 0);'),
					'icon' => 'icon-arrow-left'
				);
				
				$page_header_toolbar_btn['save'] = array(
					'desc' => (!$validate ? $obj->l('Next', 'reservations') : ucfirst(myOwnLang::$operations[MYOWN_OPE::SAVE])),
					'js' => (!$validate ? '' : "($('#id_address_invoice').val() ? $('#myOwnAddOrder').submit() : alert('".$obj->l('Please select an address', 'reservations')."'));"),
					'icon' => (!$validate ? 'icon-arrow-right' : 'process-icon-save-and-stay ')
				);
			}
			$page_header_toolbar_btn['close'] = array(
				'desc' => $obj->l('Close', 'reservations'),
				'js' => 'cleanResaBox('.$resa->sqlId.');$.fancybox.close();',
				'icon' => ($isfront ? 'icon-remove' : 'process-icon-cancel')
			);
		}
		$buttons = MyOwnReservationsUtils::getButtonsList($page_header_toolbar_btn);


		$title=($resa->_saved>0 ? ucfirst(myOwnLang::$object[MYOWN_OBJ::RESV]).' #'.$resa->sqlId : ucfirst(myOwnLang::$actions[MYOWN_OPE::FNEW]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::RESV]));
		
		if (_PS_VERSION_ < "1.6.0.0") 
			echo '
		<div id="reservationContent" class="reservationContent reservationContent15 '.($isfront ? 'frontContent' : '').'" style="padding:10px;padding-bottom:1px;background-color:rgb(204, 204, 204)">
			<div class="toolbarBox toolbarHead">
				'.$buttons.'
				<div class="pageTitle">
					<center><div id="actionWaiting" style="padding-top:20px;display:none"><img src="../img/loader.gif"></div></center>
					<h3 style="line-height:40px">
						'.$title.'
					</h3>
				</div>
			</div>
		<div>';
		else echo '
		<div id="reservationContent" class="reservationContent reservationContent16 '.($isfront ? 'frontContent' : '').'" style="padding:0px;padding-bottom:1px;background-color:rgb(204, 204, 204)">
			<div class="page-head page-'.($isfront ? 'front-' : '').'head-myown bootstrap">
				<h2 class="page-title" stye="float: left;">
					'.$title.'
				</h2>
				<center><div id="actionWaiting" style="padding-top:20px;display:none"><img src="../img/loader.gif"></div></center>
				<div class="page-bar toolbarBox" id="content">
					'.$buttons.'
					<div style="clear:both"></div>
				</div>
				<div style="clear:both"></div>
			</div>
			<div style="padding-left: 10px;padding-right: 10px;padding-top: 10px;">';

			
			//--------------------------------------------------------- confirm ---------------------------------------------------------
			
			echo '
			<div class="bootstrap"><div id="reservationResult">'.$check.'</div></div>';
			
			//--------------------------------------------------------- order ---------------------------------------------------------
			if (Tools::getValue('validate', 'false')!='false')
				echo '<form class="form-horizontal" id="myOwnAddOrder" target="_blank" action="index.php?controller=AdminOrders&token='.Tools::getAdminToken('AdminOrders'.intval(Tab::getIdFromClassName('AdminOrders')).intval($cookie->id_employee)).'&submitAddorder=1" method="post" autocomplete="off">';
			
			if (!in_array("clear", $actions)) {
				if (Tools::getValue('create')=='')
					echo self::displayOrderDetails($obj, $resa, $reschedule, $order, $actions, $productsFilter);
				else echo self::previewOrder($obj, $resa, $reschedule, $order, $actions, $productsFilter);
			}
			
			echo self::editCustomer($obj, $cookie, $resa, $reschedule, $actions);
			
			//--------------------------------------------------------- product details ---------------------------------------------------------
			
			if (empty($actions)) {
				if (Tools::getValue('create')=='') {
					echo self::displayProductDetails($obj, $cookie, $resa, $order, $productsFilter);
					if ($isproversion && $resa->_saved && !$factorized) {
						echo myOwnReservationsProController::editStockDetails($obj, $resa, $rights);
					}
				}
				else if (Tools::getValue('validate')=='true')
					echo self::displayOrderConfirmation($obj, $cookie, $resa, $order, $productsFilter);
				else echo self::displayProductSearch($obj, $cookie, $resa, $order, $productsFilter);
			}
			
			if (Tools::getValue('validate', 'false')!='false')
				echo '</form>';
		
		//--------------------------------------------------------- edit resa ---------------------------------------------------------
			if ($rights['edit'] && !in_array("clear", $actions) && Tools::getValue('create')=='')
				echo self::editReservationDetails($obj, $cookie, $resa, $reschedule, $actions);
		
			echo '
			</div>
		</div>';
	}
	
	public static function editItemsMenu($obj, $rights, $detailsExt=null) {
		if ($rights['delete'] && $detailsExt!=null)
			return '<div><a onclick="$(\'#editProductsDetails\').toggle();" class="btn btn-default btn-subpos myown-btn-edit" style="float:right;"><i class="icon-pencil"></i> '.$obj->l('Reschedule', 'reservations').'</a>
				<a onclick="$(\'#editProductsDetails\').toggle();" class="btn btn-default btn-subpos myown-btn-edit" style="float:right;"><i class="icon-trash"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::DELETE]).'</a></div>
				<div style="clear:both"></div>';
	}
	
	public static function show($obj, $cookie, $resa, $productsFilter=array()) {
		global $rights;
		global $cookie;
		/*if (Tools::getIsset('fc')) {
			$rights_tab=array();
			foreach ($obj->_extensions->list as $ext) 
				if (get_class($ext)=='marketplace')
						if (method_exists($ext, "getRightsCustomer"))
							$rights = $ext->getRightsCustomer($resa->id_product, $cookie->id_customer);
		}*/
		if ($cookie->goupedView && !is_array($resa) && $resa->sqlId!=0 && $resa->sqlId!=9999999999) {
			if (!$resa->_saved) 
				$resa = array($resa);
		}
		if (is_array($resa)) {
			$reschedule=null;
			$actionsArrays=array();
			$resas=array();
			foreach ($resa as $resatmp) {
				$reschedule = clone $resatmp;
				$actionsArrays[$resatmp->sqlId] = self::check($obj, $cookie, $resatmp, $reschedule);
				$resas[]=$resatmp;//if (!$resatmp->_saved) $resatmp = $reschedule;
			}

			return self::showFactorize($obj, $cookie, $resas, $actionsArrays);
			
		}
		
		if (_PS_VERSION_ >= "1.5.0.0")
			$link = $obj->getContext()->link;
		else $link = new Link();
		
		if ($productsFilter!=array()) {
			$rights['edit']=1;
			$rights['delete']=0;
		}
		$isfront = Tools::getIsset('fc');

		//check actions
		if (!is_array($resa) && $resa->sqlId) $reschedule = new myOwnReservation($resa->sqlId);
		else $reschedule=null;
		if ($reschedule!=null) $reschedule->_mainProduct = $obj->_products->getResProductFromProduct($reschedule->id_product);
		$actionsArray = self::check($obj, $cookie, $resa, $reschedule);
		$payment_modules = array();
		foreach (PaymentModule::getInstalledPaymentModules() as $p_module)
			$payment_modules[] = Module::getInstanceById((int)$p_module['id_module']);
		$check = $actionsArray['label'];
		$actions = $actionsArray['list'];
		//$saveAction="";

		if ($resa->id_product) {
			$productsImages=MyOwnReservationsUtils::getProductsImages(array($resa->id_product), $cookie->id_lang);
			$resaData = $resa->toArray($cookie->id_lang, $link, $productsImages);

			$attribute_text=MyOwnReservationsUtils::getAttributeCombinaisons($resa->id_product, $resa->id_product_attribute, $cookie->id_lang);
			$productStr = '<b>'.($resa->quantity > 1 ? $resa->quantity.'x ' : '').MyOwnReservationsUtils::getProductName($resa->id_product, $cookie->id_lang).'</b>'.(trim($attribute_text)!='<br />&nbsp;' ? '<br />'.$attribute_text : '');
			$resa->_mainProduct = $obj->_products->getResProductFromProduct($resa->id_product);
			if ($resa->id_order) $resa = myOwnResas::populateReservation($resa);
		} else {	
			$resaData = $resa->toArray($cookie->id_lang);
		}
		
		if ($resa->id_order>0) {
			$order=new Order($resa->id_order);
			$display_price_wt = ($order->getTaxCalculationMethod() != PS_TAX_EXC);
		} else {
			$order=null;
			$display_price_wt = true;
		}

		echo myOwnUtils::insertDatepicker($obj, array('rescheduleStart', 'rescheduleEnd'), false, '', true);
		echo MyOwnReservationsUtils::insertTimeslotPickerScript($obj, 'reschedule', ($resa->id_product==0 ? 0 : ($resa->_mainProduct!=null ? $resa->_mainProduct->sqlId : 0)));
		
		echo '
		<script type="text/javascript">
			var resaSaved = '.intval($resa->_saved).';
			var resaJSON = '.Tools::jsonEncode($resaData).';
			//availability refresh
			$(document).ready(function() {
        		$("#rescheduleProduct").change(refreshAvailability);
        		$("#rescheduleProductAttribute").change(refreshAvailability);
        		$("#quantity").change(refreshAvailability);
        		$("#rescheduleStart").change(refreshAvailability);
        		$("#rescheduleStartTimeslot").change(refreshAvailability);
        		$("#rescheduleEnd").change(refreshAvailability);
        		$("#rescheduleEndTimeslot").change(refreshAvailability);
        		$("#myownr_object").change(refreshAvailability);
        		$("#rescheduleStart").on("change", function(e) {
					if ($("#rescheduleEnd").length) {
						var start = $("#rescheduleStart").datepicker("getDate");
						var end = $("#rescheduleEnd").datepicker("getDate");
						if (end<start || $("#rescheduleEnd").val()=="") $("#rescheduleEnd").datepicker("setDate", start );
					}
				});
				$("#rescheduleEnd").on("change", function(e) {
					if ($("#rescheduleStart").length) {
						var start = $("#rescheduleStart").datepicker("getDate");
						var end = $("#rescheduleEnd").datepicker("getDate");
						if (end<start || $("#rescheduleStart").val()=="") $("#rescheduleStart").datepicker("setDate", end );
					}
				});

        	if ($("#rescheduleProduct").length) refreshAvailability();
			';
			if (count($actions)==0 && $resa->id_product>0) echo '
				populateTimeslot("start");
				populateTimeslot("end");
				$("#rescheduleStartTimeslot").val('.$resa->startTimeslot.');
				$("#rescheduleEndTimeslot").val('.$resa->endTimeslot.');';
			echo '
			});';
			if ($resa->id_product>0 && $resa->sqlId >0 && $resa->_saved) { 
				echo '
				if (typeof(scheduler)!="undefined") {
					var unmodifiedStartDate=new Date('.($resa->getStartDateTime()*1000).');
					var unmodifiedEndDate=new Date('.($resa->getEndDateTime()*1000).');
					var unmodifiedValidated='.intval($resa->validated).';';
					if ($reschedule!=null) echo '
					scheduler.setEventStartDate('.$resa->sqlId.', new Date('.($reschedule->getStartDateTime()*1000).'));
					scheduler.setEventEndDate('.$resa->sqlId.', new Date('.($reschedule->getEndDateTime()*1000).'));';
					echo '
					var schedEvent = scheduler.getEvent('.$resa->sqlId.');
					if (schedEvent!=undefined) {
						schedEvent.validated = '.intval(Tools::getValue("validationStatus", $resa->validated)).';
						schedEvent.assigned = '.intval(Tools::getValue("assignmentEmployee", $resa->id_assigned)).';
						if (resaJSON.id_product) {
							schedEvent.image = resaJSON.image;
							schedEvent.product = resaJSON.product;
						}
						if (resaJSON.id_order) {
							schedEvent.id_order = resaJSON.id_order;
							schedEvent.ref_order = resaJSON.ref_order;
							schedEvent.status_text = resaJSON.status_text;
							schedEvent.status_color = resaJSON.status_color;
							schedEvent.customer = resaJSON.customer;
						}
						scheduler.updateEvent('.$resa->sqlId.');
					}
				}';
			}
			echo '
		</script>';
		
		if ($isfront) {
			echo "
			<style type=\"text/css\">

				
			</style>";
		}

		//--------------------------------------------------------- rescheduling check ---------------------------------------------------------
		if (!in_array("clear", $actions)) $questionClass = "warn";
		else $questionClass = "error";
		if (in_array("new", $actions)) $questionClass = "hint";

		if (count($actions)) {
			$check = '
			<script type="text/javascript">
			$("#reservationContent .nav-pills").hide();
			$(".myown-btn-edit").hide();
			</script>
			<div id="warning" class="bootstrap">
				'.(_PS_VERSION_ >= "1.6.0.0" ? '
				<div class="alert alert-warning">
				' : '
				<div id="content" style="background:none">
				').' 
					'.($actionsArray['warning']!="" && !in_array("clear", $actions) ? '<div class="error">'.$actionsArray['warning'].'</div>' : '');
			if (count($actions)) {
				$splited=false;
				if ($resa->quantity>1) {
					$selected=false;
					$selItemsStr='';
					for ($i=1; $i < $resa->quantity+1; $i++) {
						$sel[$i]=Tools::getIsset('resa_'.$resa->sqlId.'_'.$i, '');
						if (!$sel[$i]) {
							$splited=true;
						} else {
							if ($selItemsStr!='') $selItemsStr .= ', #';
							$selItemsStr .= '#'.$i;
						}
					}
					if ($selItemsStr=='') $splited=false;
				}
				$check .= '
						<div class="'.$questionClass.'" style="display:block">
						<h2 style="font-size:17px;">' . ($splited ? $obj->l('Item', 'reservations').(count(array_filter($sel))>1 ? 's' : '').' '.$selItemsStr.' : ' : '') . $actionsArray['label'] . '</h2>
				<ul style="margin-bottom:6px;list-style-type:none;padding-left:15px;">';
				
				//Change order status to
				if ($resa->id_order
					&& (in_array("clear", $actions) or in_array("assign", $actions))) {
					$check .= '
					<li>
						<input type="checkbox" style="display:inline-block;" id="cb_status" name="orderStatus" value="1" onclick="">
						&nbsp;'.$obj->l('Change order status to ', 'reservations').' <select style="font-size:10px;display:inline-block;width:100px" id="orderStatus" name="orderStatus">';
							if (in_array("clear", $actions)) $toselect = Configuration::get('PS_OS_CANCELED');
							if (in_array("assign", $actions)) $toselect = Configuration::get('PS_OS_PREPARATION');
							foreach(myOwnUtils::getStatusList($cookie->id_lang) AS $orderStatus) {
								$check .= '<option style="font-size:10px;" value="' . $orderStatus['id_order_state']. '" '.($toselect == $orderStatus['id_order_state'] ? 'selected' : '').'>' . substr($orderStatus['name'],0,22) . '</option>';
							}
							$check .= '
							</select>
					</li>';
				}
				
				//Refresh order item
				if ( $reschedule!= null 
					&& $resa->id_order
					&& $reschedule->id_product > 0 
					&& ($reschedule->getPriceTaxes(true) != $resa->getTotalPriceSaved(true) or $reschedule->quantity != $resa->quantity or !$reschedule->_saved))
					$check .= '
					<li>
						<input type="checkbox" id="cb_refresh" name="refreshOrder" value="1" onclick="" checked>
						&nbsp;'.$obj->l('Update order', 'reservations').'
					</li>';
					if ($order!=null) {
						$docs = $order->getDocuments();
						$invoices=array();
						foreach ($docs as $dockey=>$doc)
							if (get_class($doc)=='OrderInvoice')
								$invoices[$doc->id] = $doc->date_add;
						if (count($invoices)) {
						$check .= '
						<li>
							<input type="checkbox" id="cb_invoice" name="refreshInvoice" value="1" onclick="" checked>
							&nbsp;'.$obj->l('Refresh invoice ', 'reservations').' <select style="font-size:10px;width:100px;display:inline-block" id="invoiceDoc" name="invoiceDoc">';
								foreach($invoices as $invoiceKey => $invoice) {
									$check .= '<option style="font-size:10px;" value="' . $invoiceKey. '">#' . sprintf('%06d', $invoiceKey) . '</option>';
								}
								$check .= '
								</select>
						</li>';
						}
					}
				//Notify customer
				if (!in_array("clear", $actions) 
					&& $reschedule->id_order > 0 
					&& $reschedule->getSelectValue() != $resa->getSelectValue())
					$check .= '
					<li>
						<input type="checkbox" id="cb_notify" name="notify" value="1" onclick="" checked>
						&nbsp;'.$obj->l('Notify customer', 'reservations').'
					</li>';

				$check .= '
				</ul>
				<div style="padding-top:10px;text-align:right;">';

				$check .= '
					<button type="submit" name="cancel" name="adminbox_cancel" id="adminbox_cancel" class="btn btn-default" onclick="cleanResaBox('.$resa->sqlId.');$.fancybox.close();">
						<i class="icon-remove"></i>
						'.ucfirst(myOwnLang::$actions[MYOWN_OPE::CANCEL]).'
					</button>
				&nbsp;';
				if (in_array("clear", $actions)) $check .= '
					<button type="submit" name="adminbox_submit" class="btn btn-default" onclick="submitDelete('.$resa->sqlId.', $(\'cb_status\').attr(\'checked\'), $(\'orderStatus\').val(), $(\'invoiceDoc\').val(), \''.($splited ? implode(',', $sel) : '').'\');">
						<i class="icon-trash text-danger"></i>
						'.$obj->l('Delete reservation', 'reservations').'
					</button>
					';
				//reschedule save 

				if ( $reschedule != null && !in_array("clear", $actions) && $reschedule->_saved) {
					$saveSubAction = 'submitReschedule('.$resa->sqlId.', \''.$reschedule->startDate.'\', '.$reschedule->startTimeslot.', \''.$reschedule->endDate.'\', '.$reschedule->endTimeslot.', \''.($reschedule->quantity - $resa->quantity).'\', '.intval($reschedule->validated).', \''.addslashes($reschedule->comment).'\', \''.$reschedule->stock.'\', \''.Tools::getValue('qty_field', '').'\' , \''.($splited ? implode(',', $sel) : '').'\');';
					$check .= '
					<button type="submit" id="adminbox_submit" name="adminbox_submit" class="btn btn-default" onclick="'.$saveSubAction.'">
						'.(in_array('forward', $actions) || in_array('shorten', $actions) || in_array('backward', $actions) || in_array('extend', $actions) ? '
							<i class="icon-random text-danger"></i>
						'.$obj->l('Reschedule', 'reservations') : '
							<i class="icon-save text-danger"></i>
						'.$obj->l('Save', 'reservations').'').'
					</button>
					';
				}
				//create save
				if ( $reschedule != null && !in_array("clear", $actions) && !$reschedule->_saved) {
					$saveSubAction = 'submitNew('.$resa->sqlId.', \'create\' );';
					$check .= '
					<button type="submit" id="adminbox_submit" name="adminbox_submit" class="btn btn-default" onclick="'.$saveSubAction.'">
						<i class="icon-save text-danger"></i>
						'.ucfirst(myOwnLang::$actions[MYOWN_OPE::CREATE]).'
					</button>
					';
				}
				$check .= '
				</div></div>';
			}
			$check .= '</div></div>';
		} else {
			$check = '';
			if ($actionsArray['warning']!="")
				$check = '<div id="warning" class="bootstrap"><div class="alert alert-warning"><div class="error">'.$actionsArray['warning'].'</div></div></div>';
			if ($actionsArray['result']!="")
				$check .= $actionsArray['result'];
		};
		
		self::showReservation($obj, $cookie, $resa, $reschedule, $actions, $check, $productsFilter);
// 		if ($resa->_saved && $resa->_mainProduct == null) $check='<div id="warning"><div id="content" style="background:none"><div class="error alert">'.$obj->l('No reservation rules found', 'reservations').'</div></div></div>';
		/*if ($resa->_saved && $resa->_mainProduct == null) $check = '<div id="" style="background:none">'.myOwnUtils::displayError($obj->l('No reservation rules found', 'reservations')).'</div></div>';
		
		$isproversion=is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");
		
		//--------------------------------------------------------- head ---------------------------------------------------------
		if ($saveAction=="") {
			if ($reschedule==null) {
				if ($resa->sqlId > 0) $saveAction = 'editForm('.$resa->sqlId.');';
				else {
					$customer = Tools::getValue('customer');
					if ($customer!='')
						$saveAction = "newOrder(".$customer.", id_cart);";
					else $saveAction = "newForm();";
				}
			} else {
				if ($resa->_saved) $saveAction = 'showResaDetails('.$resa->sqlId.');';
				else $saveAction = 'newForm('.$resa->sqlId.');';
			}
		}
		$page_header_toolbar_btn=array();
		if (!in_array("clear", $actions) ) {
			if (!in_array("new", $actions) && Tools::getValue('create')=='') { //&& !in_array("backward", $actions) && !in_array("forward", $actions) && !in_array("shorten", $actions) && !in_array("extend", $actions) && !in_array("reschedule", $actions)
				if ($resa->_saved && $rights['delete'])
					$page_header_toolbar_btn['delete'] = array(
						'desc' => ucfirst(myOwnLang::$actions[MYOWN_OPE::DELETE]),
						'js' => 'editForm('.$resa->sqlId.', true);',
						'icon' => ($isfront ? 'icon-trash' : 'process-icon-delete')
					);
				if ($rights['edit'])
					$page_header_toolbar_btn['save'] = array(
						'desc' => ucfirst(myOwnLang::$actions[MYOWN_OPE::SAVE]),
						'js' => $saveAction,
						'icon' => ($isfront ? 'icon-save' : 'process-icon-save-and-stay')
					);
			}
			if (Tools::getValue('create')!='' && !$isfront) {
				$customer = Tools::getValue('customer');
				$validate = (Tools::getValue('validate', 'false')=='true');

				$page_header_toolbar_btn['prev'] = array(
					'desc' => $obj->l('Previous', 'reservations'),
					'js' => ($validate ? 'newOrder('.$customer.', id_cart);' : 'newOrder(0, 0);'),
					'icon' => 'icon-arrow-left'
				);
				
				$page_header_toolbar_btn['save'] = array(
					'desc' => (!$validate ? $obj->l('Next', 'reservations') : $obj->l('Save', 'reservations')),
					'js' => (!$validate ? '' : "($('#id_address_invoice').val() ? $('#myOwnAddOrder').submit() : alert('".$obj->l('Please select an address', 'reservations')."'));"),
					'icon' => (!$validate ? 'icon-arrow-right' : 'process-icon-save-and-stay ')
				);
			}
			$page_header_toolbar_btn['close'] = array(
				'desc' => $obj->l('Close', 'reservations'),
				'js' => 'cleanResaBox('.$resa->sqlId.');$.fancybox.close();',
				'icon' => ($isfront ? 'icon-remove' : 'process-icon-cancel')
			);
		}
		$buttons = MyOwnReservationsUtils::getButtonsList($page_header_toolbar_btn);


		$title=($resa->_saved>0 ? ucfirst(myOwnLang::$object[MYOWN_OBJ::RESV]).' #'.$resa->sqlId : $obj->l('New', 'reservations').' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::RESV]));
		
		if (_PS_VERSION_ < "1.6.0.0") 
			echo '
		<div id="reservationContent" class="reservationContent reservationContent15 '.($isfront ? 'frontContent' : '').'" style="padding:10px;padding-bottom:1px;background-color:rgb(204, 204, 204)">
			<div class="toolbarBox toolbarHead">
				'.$buttons.'
				<div class="pageTitle">
					<center><div id="actionWaiting" style="padding-top:20px;display:none"><img src="../img/loader.gif"></div></center>
					<h3 style="line-height:40px">
						'.$title.'
					</h3>
				</div>
			</div>
		<div>';
		else echo '
		<div id="reservationContent" class="reservationContent reservationContent16 '.($isfront ? 'frontContent' : '').'" style="padding:0px;padding-bottom:1px;background-color:rgb(204, 204, 204)">
			<div class="page-head page-'.($isfront ? 'front-' : '').'head-myown bootstrap">
				<h2 class="page-title" stye="float: left;">
					'.$title.'
				</h2>
				<center><div id="actionWaiting" style="padding-top:20px;display:none"><img src="../img/loader.gif"></div></center>
				<div class="page-bar toolbarBox" id="content">
					'.$buttons.'
					<div style="clear:both"></div>
				</div>
				<div style="clear:both"></div>
			</div>
			<div style="padding-left: 10px;padding-right: 10px;padding-top: 10px;">';

			
			//--------------------------------------------------------- confirm ---------------------------------------------------------
			
			echo '
			<div id="reservationResult">'.$check.'</div>';
			
			//--------------------------------------------------------- order ---------------------------------------------------------
			if (Tools::getValue('validate', 'false')!='false')
				echo '<form class="form-horizontal" id="myOwnAddOrder" target="_blank" action="index.php?controller=AdminOrders&token='.Tools::getAdminToken('AdminOrders'.intval(Tab::getIdFromClassName('AdminOrders')).intval($cookie->id_employee)).'&submitAddorder=1" method="post" autocomplete="off">';
			
			if (!in_array("clear", $actions)) {
				if (Tools::getValue('create')=='')
					echo self::displayOrderDetails($obj, $resa, $reschedule, $order, $actions, $productsFilter);
				else echo self::previewOrder($obj, $resa, $reschedule, $order, $actions, $productsFilter);
			}
			
			echo self::editCustomer($obj, $cookie, $resa, $reschedule, $actions);
			
			//--------------------------------------------------------- product details ---------------------------------------------------------
			
			if (empty($actions)) {
				if (Tools::getValue('create')=='') {
					echo self::displayProductDetails($obj, $cookie, $resa, $order, $productsFilter);
					if ($isproversion && $resa->_saved && !$factorized) {
						echo myOwnReservationsProController::editStockDetails($obj, $resa, $rights);
					}
				}
				else if (Tools::getValue('validate')=='true')
					echo self::displayOrderConfirmation($obj, $cookie, $resa, $order, $productsFilter);
				else echo self::displayProductSearch($obj, $cookie, $resa, $order, $productsFilter);
			}
			
			if (Tools::getValue('validate', 'false')!='false')
				echo '</form>';
		
		//--------------------------------------------------------- edit resa ---------------------------------------------------------
			if ($rights['edit'] && !in_array("clear", $actions) && Tools::getValue('create')=='')
				echo self::editReservationDetails($obj, $cookie, $resa, $reschedule, $actions);
		
			echo '
			</div>
		</div>';*/
	}
}



?>