<?php
/*
* 2010-2012 LaBulle All right reserved
*/
use PrestaShop\PrestaShop\Core\Product\ProductExtraContent;

class myOwnReservationsHooksController {

	//==============================================================================================================================================
	//     ACTIONS
	//==============================================================================================================================================

	public static function actionBeforeAjaxDie($obj, &$params) {
		if(session_id() == '') session_start();

		if (array_key_exists('myownresFrontErr', $_SESSION))
			$myownresFrontErr = $_SESSION['myownresFrontErr'];
		else $myownresFrontErr = 0;

		if ($params['controller'] == 'CartController' && $myownresFrontErr) {
			switch ($myownresFrontErr) { 
				case reservation_error::NO_PRODUCT_AVAILABLE: 
					$errors[] = $obj->l('No product available', 'hooks');
					break;
				case reservation_error::ALL_PRODUCTS_BOOOKED: 
					$errors[] = $obj->l('all product are booked', 'hooks');
					break;
				case reservation_error::PERIOD_IN_HOLIDAYS: 
					$error = $obj->l('Your selection is over an holiday period', 'hooks');
					break;
				case reservation_error::INVALID_LENGTH: 
					$error = $obj->l('Your selection is too short or too long', 'hooks');
					break;
				case reservation_error::INVALID_PERIOD: 
					$errors[] = $obj->l('Your selection doesn\'t match reservation rule', 'hooks');
					break;
				case reservation_error::DISTINCT_PERIOD_FORBIDDEN: 
					$error = $obj->l('you can\'t reserve products with distinct periods', 'hooks');
					break;
				case reservation_error::NO_PERIOD_SELECTED: 
					$error = $obj->l('you need to select a period to add this product to your cart', 'hooks');
					break;
				case reservation_error::PRODUCT_ISNT_RENTED:
					$error = $obj->l('you need to select a period to add this product to your cart', 'hooks');
					break;
				case reservation_error::NOT_ENOUGH_AVAILABLE:
					$error = $obj->l('there isn\'t enough products available for this quantity', 'hooks');
					break;
				default:
					$error = $obj->l('unknown error', 'hooks');
					break;
			}
			$errors = array($obj->l('Sorry', 'hooks').', '.$error);
			die(Tools::jsonEncode([
	            'hasError' => true,
	            'errors' => $errors
	        ]));
			/*$id_product = Tools::getValue('id_product');
			$reservations = Tools::getValue('reservations');
			$id_customization = Tools::getValue('id_customization');
			$delete = Tools::getValue('delete');
			$mainProduct = $obj->_products->getResProductFromProduct($id_product);
			$errors=array();
			if ($mainProduct!=null && !$delete) {
				//check no resa arg
				if (!$mainProduct->_optionAlsoSell && (trim($reservations)=='' || $reservations=='undefined') && $id_customization==0) {
					$errors[] = $obj->l('Please select a reservation period', 'hooks').$test;
					die(Tools::jsonEncode(array('hasError' => true, 'errors' => $errors)));
				} else $errors[] = $obj->l('Reservation period unavailable', 'hooks').$test;
				//check qty
			}*/
		}


	}
	
	public static function actionCartListOverride($obj, &$params)
	{
		global $cart, $cookie, $smarty;
		
		if (!Configuration::get('PS_BLOCK_CART_AJAX'))
			return;
		
		$mainProduct=null;
		$id_product = Tools::getValue('id_product');
		if ($id_product) $mainProduct = $obj->_products->getResProductFromProduct($id_product);
		//if the product handle reservation
		if ($mainProduct!=null) {
			$mainProducts = explode(',', $mainProduct->ids_products);
			$sessionPotentialResa=myOwnPotentialResa::getSessionPotentialResa($obj);
			
			$product = new Product($id_product);
			
			$accessories = $product->getAccessories($cookie->id_lang);
			$potentialresas = array();
			if ($accessories != array()) {
				$carts = $cart->getMyOwnReservationCart();
				$resa=null;
				foreach ($carts->getResas($id_product) as $resa);
					$temp_prod_resa = $resa;
				if ($temp_prod_resa != null) {
					foreach ($accessories as $accessorie) {
						if (!in_array($accessorie['id_product'], $mainProducts)) $accMainProduct = $obj->_products->getResProductFromProduct($accessorie['id_product']);
						if (in_array($accessorie['id_product'], $mainProducts) or ($accMainProduct!=null && $accMainProduct->sqlId==$mainProduct->sqlId)) {
							$temp_acc_resa = clone $temp_prod_resa;
							$temp_acc_resa->id_product = $accessorie['id_product'];
							$attribute = MyOwnReservationsUtils::getDefaultAttribute($temp_acc_resa->id_product);
							$temp_acc_resa->id_product_attribute = $attribute;
							
							$accAvailabilities = new myOwnAvailabilities($temp_acc_resa->getStartDateTime(), $temp_acc_resa->getEndDateTime(), $mainProduct, $temp_acc_resa->id_product, $attribute, false, true);
							$temp_acc_resa->setPrice($obj->_pricerules->list);
							$temp_acc_resa->_productQty = $mainProduct->getProductQuantity($temp_acc_resa->id_product, $attribute, $obj->_stocks, $accAvailabilities);
							$temp_acc_resa->_reservations = null;
							$availableQty = $temp_acc_resa->getAvailableQuantity($cart, null, $obj->_stocks);
							if ($availableQty>0) {
								$potentialresas[$temp_acc_resa->id_product]= $temp_acc_resa;
							} else 
								$potentialresas[$temp_acc_resa->id_product]=null;
						} else 
							$potentialresas[$accessorie['id_product']]=null;
					}
				}
			}

			$smarty->assign('accessories',$accessories);
			$smarty->assign('accessories_potentialresas',$potentialresas);
			$link=new Link();
			if (empty($link->protocol_content))
				$link->protocol_content = Tools::getCurrentUrlProtocolPrefix();
			$smarty->assign(array(
				'myownlink' => $link,
				'homeSize' => Image::getSize(ImageType::getFormatedName('home'))
			));
			
			if ($cart->getMyOwnReservationCart()->total_wt - $cart->getMyOwnReservationCart()->advance_wt >0  ) { //&& Configuration::get('MYOWNRES_DEPOSIT_SHOW')!=1
				$advanceMsg = Configuration::get('MYOWNRES_MSG_ADVANCE', $cookie->id_lang);
				//self::forceTranslation('blockcart', '', 'Wrapping', $depositMsg);
				self::forceTranslation('blockcart', '', 'Total products', $advanceMsg);
			}
		
			$accessories_list = $obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/hook/accessories.tpl');
			
			if (trim($accessories_list)!='') {
				$json = Tools::jsonDecode($params['json'], true);
				$json['crossSelling'] = '<div class="crossseling-content">'.$accessories_list.'</div>';
				$params['json'] = Tools::jsonEncode($json);
			}
		
		} else return;
		
	}

	//-----------------------------------------------------------------------------------------------------------------
    //   CRON
    //-----------------------------------------------------------------------------------------------------------------

	public static function actionCronJob($obj, &$params) 
    {
    	myOwnResas::sync($obj);
    	PrestaShopLogger::addLog(date('Y-m-d H:i:s').'actionCronJob');
    }
    
	//-----------------------------------------------------------------------------------------------------------------
	// ADMIN Product Action
	//-----------------------------------------------------------------------------------------------------------------
	
	public static function actionProductUpdate($obj, $params) {
		global $cookie;
		$mainProduct=null;
		if(session_id() == '') session_start();
		$_SESSION['myownresAdminMsg'] = array();
		$id_product = Tools::getValue("id_product", (array_key_exists('id_product', $params) ? $params['id_product'] : 0));
		PrestaShopLogger::addLog(date('Y-m-d H:i:s').'actionProductUpdate id_product '.$id_product);
		
		
		if ($id_product>0) $mainProduct = $obj->_products->getResProductFromProduct($id_product);
		
		//priceset
		//-----------------------------------------------------------------------------------------------------------------
		if ($mainProduct!=null && $mainProduct->_reservationPriceType > reservation_price::PRODUCT_DATE) {
			$msg='';
			$res = myOwnReservationsPriceSetsController::editAction($obj, $msg);
			if (trim($msg)!='') {
				if (!$res)
					$_SESSION['myownresAdminMsg'][] = self::displayAdminError($msg);
				else $_SESSION['myownresAdminMsg'][] = self::displayAdminConf($msg);
			}
		}

		//availabilities
		//-----------------------------------------------------------------------------------------------------------------
		$availabilitieserrors=myOwnReservationsAvailabilitiesController::checkEdit($obj);
		if ($availabilitieserrors!==false) {
			$msg='';
			if (!count($availabilitieserrors)) {
				$res = myOwnReservationsAvailabilitiesController::addAction($obj, $msg);
				if (!$res)
					$_SESSION['myownresAdminMsg'][] = self::displayAdminError($msg);
				else $_SESSION['myownresAdminMsg'][] = self::displayAdminConf($msg);
			}
			else
				$_SESSION['myownresAdminMsg'] = array_merge($_SESSION['myownresAdminMsg'], array_map(array('myOwnReservationsHooksController', 'displayAdminError'), $availabilitieserrors));
		}

		//ts
		//-----------------------------------------------------------------------------------------------------------------
		$tserrors = myOwnReservationsTimeslotsController::check($obj);
		if ($tserrors!==false) {
			$msg='';
			if (!count($tserrors)) {
				$res = myOwnReservationsTimeslotsController::editAction($obj, $msg);
				if (!$res)
					$_SESSION['myownresAdminMsg'][] = self::displayAdminError($msg);
				else $_SESSION['myownresAdminMsg'][] = self::displayAdminConf($msg);
			} else 
				$_SESSION['myownresAdminMsg'] = array_merge($_SESSION['myownresAdminMsg'], array_map(array('myOwnReservationsHooksController', 'displayAdminError'), $tserrors));
		}

		//ext
		//-----------------------------------------------------------------------------------------------------------------
    	foreach ($obj->_extensions->getExtByType(extension_type::UPD_PROD) as $ext) 
			if (method_exists($ext, "actionProductUpdate"))
				$ext->actionProductUpdate($obj, $params);
		
		
		

		if (isset($_POST["deleteAvailabilities"])) {
			$msg='';
			$res = myOwnReservationsAvailabilitiesController::deleteAction($obj, $msg);
			if (!$res)
				$_SESSION['myownresAdminMsg'][] = self::displayAdminError($msg);
			else $_SESSION['myownresAdminMsg'][] = self::displayAdminConf($msg);
		}
		if (isset($_POST["deleteTimeslots"])) {
			$msg='';
			$res = myOwnReservationsTimeslotsController::deleteAction($obj, $msg);
			if (!$res)
				$_SESSION['myownresAdminMsg'][] = self::displayAdminError($msg);
			else $_SESSION['myownresAdminMsg'][] = self::displayAdminConf($msg);
		}
		if ($id_product>0 && $mainProduct!=null)
		if ($mainProduct->_reservationLengthControl == reservation_length::PRODUCT or $mainProduct->_reservationLengthControl == reservation_length::COMBINATION) {
			if ($mainProduct->_reservationLengthControl == reservation_length::PRODUCT) $attributes=array(0);
			else $attributes = MyOwnReservationsUtils::getProductAttributes($id_product);
			foreach ($attributes as $attribute) {
				if (!$attribute)
					if (isset($_POST["productLength"]))
						Configuration::updateValue('MYOWNRES_RES_LENGTH_'.$id_product, $_POST["productLength"]);
				if ($attribute)
					if (isset($_POST["productLength_".$attribute]))
						Configuration::updateValue('MYOWNRES_RES_LENGTH_'.$id_product.'-'.$attribute, $_POST["productLength_".$attribute]);
			}
		}
		//sync
		//-----------------------------------------------------------------------------------------------------------------
		foreach (myOwnLang::$sync_platforms AS $platform_key => $platform_label)
			if (Configuration::get('MYOWNRES_SYNC_'.$platform_key)) {
				Configuration::updateValue('MYOWNRES_SYNCREF_'.$platform_key, array($id_product => Tools::getValue('sync_ref_'.$platform_key)));
			}
					
		
		if ($id_product>0 && $mainProduct!=null)
		if ($mainProduct->_depositByProduct) {
			$prod_amount = Configuration::updateValue('MYOWNRES_RES_DEPOSIT_'.$id_product, $_POST["productDeposit"]);
		}
	}
	
	public static function actionSearch($obj, $params) {
		global $cart, $smarty, $cookie;
		
		$selectedStart = Tools::getValue('selectedStart','');
		if ($selectedStart!="") {
			
		}
	}
	
	public static function actionAdminOrdersListingFieldsModifier($obj, &$params)
    {
    	if (Configuration::get('MYOWNRES_SAME_PERIOD')) {
	    	$select = ', a.reference as reference';
	    	$join = ' LEFT JOIN ' . _DB_PREFIX_ . 'myownreservations_reservation as resv ON (resv.id_order=a.id_order) ';
	    	if (isset($params['select']))
	    		$params['select'].= $select;
	    	else $params['select'] = $select;
			if (isset($params['join']))
				$params['join'].= $join;
			else $params['join']= $join;
			$params['fields']['resv_start'] = Array (
				'title' => $obj->l('Reservation Start'),
				'filter_key' => 'resv!start_date',
	            'havingFilter' => true,
				'type' => 'date',
	            'search' => true
	        );
	        $params['fields']['resv_end'] = Array (
				'title' => $obj->l('Reservation End'),
				'filter_key' => 'resv!end_date',
	            'havingFilter' => true,
				'type' => 'date',
	            'search' => true
	        );
        }
    }
    
    public static function listingFieldsModifierCallback($value, $line)
    {
		
        $return = '';

        return $return;
    }
	
	//==============================================================================================================================================
	//     DISPLAY FRONT
	//==============================================================================================================================================

	public static function displayHeader($obj, $params, $smarty, $cookie)
	{	
		global $cart, $sessionPotentialResa;
		if (!method_exists ($cart , 'getMyOwnReservationCart' )) return '';
		if (Configuration::get('MYOWNRES_WIDGET_PROD', 0)==widget_position::POPUP)
			$obj->getContext()->controller->addJqueryPlugin('fancybox');
		
		$out="";
		if(session_id() == '') session_start();
		myOwnReservationsController::_construire($obj);
		if (_PS_VERSION_ >= "1.5.0.0") {
      		$obj->getContext()->controller->addJS(_MODULE_DIR_.$obj->name.'/views/js/myownreservations.js');
      	} else {
      		Tools::addJS(_MODULE_DIR_.$obj->name.'/views/js/myownreservations.js');
		}
		
		//Include date picker
		if (Configuration::get('MYOWNRES_WIDGET_TYPE')==2) {
			if (_PS_VERSION_ >= "1.5.0.0") {
	      		$obj->getContext()->controller->addJqueryUI('ui.datepicker');
	      		$obj->getContext()->controller->addJS(_MODULE_DIR_.$obj->name.'/views/js/widget.js');
	      	} else {
	      		Tools::addJS(_PS_JS_DIR_.'jquery/jquery-ui-1.8.10.custom.min.js');
	      		Tools::addJS(_PS_JS_DIR_.'jquery/datepicker/ui/i18n/ui.datepicker-'.Language::getIsoById($cookie->id_lang).'.js');
	      		Tools::addJS(_MODULE_DIR_.$obj->name.'/views/js/widget.js');
			}
		}
		
		//Assigning javascript vars for all site
		//-----------------------------------------------------------------------------------------------------------------
		$link=new Link();
		$smarty->assign('cartLink', $link->getPageLink('cart.php'));
		$smarty->assign('ajaxLink', MyOwnReservationsUtils::getModuleLink($link, 'calendar'));
		$smarty->assign('reservationLabel',Configuration::get('MYOWNRES_MSG_RESV', $cookie->id_lang));
	  	$out .= $obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/hook/script_cart.tpl');
		
		$resas = myOwnCarts::getProducts($cookie->id_cart, $obj->_products, $obj->_pricerules);
		$unavailable='';
		$link = new Link();
		$ajax = Tools::getValue('ajax',0);
		if (!$ajax)
		foreach ($resas AS $resa) {
			$resa->_mainProduct = $obj->_products->getResProductFromProduct($resa->id_product);
			if ($resa->_mainProduct!=null) {
				$reservationStart = $resa->_mainProduct->getReservationStart();
				//Cart is null when checking reservation after added  it to cart (instead it's checking cart qty + resea qty)

				if ($resa->isAvailable(null, null, $obj->_stocks)<1 or $resa->getStartDateTime() < $reservationStart or !$resa->isValidLength() ) {
					$unavailable.= '<b>'.($resa->quantity > 1 ? $resa->quantity.'x ' :'').'<a '.(_PS_VERSION_ >= "1.7.0.0" ? 'class="h3"' : '').' href="'.$link->getProductLink($resa->id_product).'">'.MyOwnReservationsUtils::getProductName($resa->id_product, $cookie->id_lang).'</a></b></br>';
					
					$attr='';
					$attr=MyOwnReservationsUtils::getAttributeCombinaisons($resa->id_product, $resa->id_product_attribute, $cookie->id_lang);
					if ($attr!='') $unavailable.= $attr.'</br>';
					$unavailable.=$resa->toString($obj, $cookie->id_lang, false).'<br/><br/>';
					$cart->deleteProduct($resa->id_product, $resa->id_product_attribute);
				}
			} else $cart->deleteProduct($resa->id_product, $resa->id_product_attribute);
		}
		$products = $cart->getProducts(true);
		
		$cart->getMyOwnReservationCart()->checkProducts($obj, $cart, $products);
		
		if ($unavailable!='') {
			$unavailable=(_PS_VERSION_ >= "1.7.0.0" ? '<h1 class="h1">' : '<h3>').htmlentities(myOwnLang::getMessage('NOTAVAILABLE', $cookie->id_lang)).(_PS_VERSION_ >= "1.7.0.0" ? '</h1><br />' : '</h3>').$unavailable;
			if (_PS_VERSION_ < "1.5.0.0") {
				Tools::addCSS(_PS_CSS_DIR_.'jquery.fancybox-1.3.4.css', 'screen');
				Tools::addJS(_PS_JS_DIR_.'jquery/jquery.fancybox-1.3.4.js');
			} else {
				$obj->getContext()->controller->addCSS(_PS_CSS_DIR_.'jquery.fancybox-1.3.4.css', 'screen');
				$obj->getContext()->controller->addJS(_PS_JS_DIR_.'jquery/jquery.fancybox-1.3.4.js');
			}
			$out.="
			<script>
			document.addEventListener('DOMContentLoaded', function(event) {
					".(_PS_VERSION_ < "1.7.0.0" ? "
			        $.fancybox({
			            'width': '40%',
			            'height': '40%',
			            'autoScale': true,
			            'transitionIn': 'fade',
			            'transitionOut': 'fade',
			            'content': '".addslashes($unavailable)."'
			        });" : 
			        '$("body").append(\'<div id="quickview-unavailable" class="modal fade quickview" tabindex="-1" role="dialog" aria-hidden="true"><div class="modal-dialog" role="document"><div class="modal-content" style="text-align:center"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div><div class="modal-body">'.addslashes($unavailable).'</div></div></div></div>\');
			        $("#quickview-unavailable").modal("show");')."
			});
			</script>";
		}
				
		//translations hack
		//-----------------------------------------------------------------------------------------------------------------
		if ($cart->getMyOwnReservationCart()->deposit >0  ) { //&& Configuration::get('MYOWNRES_DEPOSIT_SHOW')!=1
			$depositMsg = Configuration::get('MYOWNRES_MSG_DEPOSIT', $cookie->id_lang);
			if (_PS_VERSION_ >= "1.7.0.0")
				$obj->getContext()->getTranslator()->getCatalogue(null)->set('Gift wrapping', $depositMsg, 'ShopThemeCheckout');
			else {
				self::forceTranslation('blockcart', '', 'Wrapping', $depositMsg);
				self::forceTranslation('', 'shopping-cart', 'Total gift-wrapping:', $depositMsg.':');
				self::forceTranslation('', 'shopping-cart', 'Total gift-wrapping (tax incl.):', $depositMsg.':');
				self::forceTranslation('', 'shopping-cart', 'Total gift wrapping (tax incl.):', $depositMsg.':');
				self::forceTranslation('', 'shopping-cart', 'Total gift wrapping (tax incl.)', $depositMsg.':');
				self::forceTranslation('', 'shopping-cart', 'Total gift-wrapping', $depositMsg);
				self::forceTranslation('', 'shopping-cart', 'Total gift-wrapping cost', $depositMsg);
				self::forceTranslation('', 'shopping-cart', 'Total gift-wrapping cost:', $depositMsg);
				self::forceTranslation('', 'order-payment', 'Total gift-wrapping:', $depositMsg.':');
				self::forceTranslation('', 'order-payment', 'Total gift-wrapping (tax incl.):', $depositMsg.':');
				self::forceTranslation('', 'order-payment', 'Total gift wrapping (tax incl.)', $depositMsg.':');
				self::forceTranslation('', 'order-payment', 'Total gift-wrapping', $depositMsg);
			}
		}
		
		//Hide checkout steps
		//-----------------------------------------------------------------------------------------------------------------
		$ctrl=Tools::getValue('controller');

		$link=new Link();
		$orderLink = $link->getPageLink('order.php');
		if (stripos($orderLink, "?")===false) $orderLink.='?myownredir=1&';
		$smarty->assign('orderLink', $orderLink);
		$stdCheckout=(Configuration::get('PS_ORDER_PROCESS_TYPE')==0);
		$smarty->assign('stdCheckout',$stdCheckout);
		$smarty->assign('defaultCarrier',Configuration::get('PS_CARRIER_DEFAULT'));
		
		$hideStep1=(Configuration::get('MYOWNRES_IGNORE_STEP1')==1);
		$hideStep2=((!$cart->id_customer or $cart->id_address_delivery or $ctrl=='address' or !$hideStep1) && Configuration::get('MYOWNRES_IGNORE_STEP2')==1);
		$ignoreOnPurchase=(Configuration::get('MYOWNRES_IGNORE_PURCHA')==1);

		if ($ignoreOnPurchase && (!$cart->getMyOwnReservationCart()->isReservation || $cart->getMyOwnReservationCart()->isPurchase)) {
			$hideStep1=false;
			$hideStep2=false;
		}
		$smarty->assign('hideStep1', $hideStep1);
		$smarty->assign('hideStep2', $hideStep2);
		$smarty->assign('islogged', $obj->getContext()->customer->isLogged());
		$smarty->assign('cust_id_address_delivery', $cart->id_address_delivery);
		
		$step=Tools::getValue('step');
		$smarty->assign('currentStep',intval($step));
		$previousStep=2;
		if ($hideStep2) $previousStep=1;
		if ($hideStep1) $previousStep=0;
		$smarty->assign('previousStep',$previousStep);
		$nextStep=2;
		if (($hideStep1 or $hideStep2) && $stdCheckout) {
			if ($hideStep1) $nextStep=2;
			if ($hideStep2) $nextStep=3;
		}
		$smarty->assign('nextStep',$nextStep);
		if ($hideStep1 || $hideStep2) {
			$both = ($hideStep1 && $hideStep2);
			$out .= '
			<style type="text/css">
				ul.step li {
				    float: left;
				    width: '.($both ? '33%' : '25%').';
				}
			</style>';
		}
		
		//get current page
		//-----------------------------------------------------------------------------------------------------------------
		$page='';
		if (strpos($_SERVER['PHP_SELF'], "order.php") !== false or $ctrl=='order') $page='order';
		if (strpos($_SERVER['PHP_SELF'], "order-opc.php") !== false or $ctrl=='orderopc') $page='order-opc';
		if (strpos($_SERVER['PHP_SELF'], "product.php") !== false or $ctrl=='product') $page='product';
		if (strpos($_SERVER['PHP_SELF'], "category.php") !== false or $ctrl=='category') $page='category';
		if (strpos($_SERVER['PHP_SELF'], "search.php") !== false or $ctrl=='search') $page='search';
		if (strpos($_SERVER['PHP_SELF'], "authentication.php") !== false or $ctrl=='authentication') $page='authentication';
		if (strpos($_SERVER['PHP_SELF'], "address.php") !== false or $ctrl=='address') $page='address';
		$smarty->assign('myownpage',$page);

		foreach ($obj->_extensions->getExtByType(extension_type::HEADER) as $ext) {
				if (method_exists($ext, "displayHeader"))
					$out.=$ext->displayHeader($obj, $cart, $ctrl);
		}
		
		//get current session dates
		//-----------------------------------------------------------------------------------------------------------------
		$sessionPotentialResa = myOwnPotentialResa::getSessionPotentialResa($obj);
		
		$smarty->assign('isResaSession',($sessionPotentialResa!=null));

		if ($sessionPotentialResa!=null) {
	       	$smarty->assign('potentialResa',$sessionPotentialResa);
		}
		

		//reservation unit
		//-----------------------------------------------------------------------------------------------------------------
		$out .= '
			<script type="text/javascript">
			var productFamilyProducts=new Array();
			var productFamilyUnits=new Array();';
		foreach ($obj->_products->list as $prodFamily) {
			$unitLabel=$prodFamily->getUnitLabel($obj, $cookie->id_lang);
			$productsIds=$prodFamily->getProductsId();
			$productsIdList = implode(',',$productsIds);
			$out .= '
			productFamilyProducts['.$prodFamily->sqlId.']="'.$productsIdList.'".split(\',\');
			productFamilyUnits['.$prodFamily->sqlId.']="'.$unitLabel.'";
			';
		}
		$out .= '
			</script>';

		//with PS 1.5 we have to calc whole in header because hook
		if (_PS_VERSION_ >= "1.6.0.0" && !Configuration::get('MYOWNRES_PS15_LIST')) {
			$smarty->assign('isCatOfSession',false);
		} else {
       		//set product prices for resa
       		//-----------------------------------------------------------------------------------------------------------------
			if ($sessionPotentialResa!=null) {
				$mainProduct=null;
				$id_mainProduct = Tools::getValue("res_familly",-1);
				if (array_key_exists($id_mainProduct, $obj->_products->list))
					$mainProduct = $obj->_products->list[$id_mainProduct];
				else if (isset($_GET['id_category']))
					$mainProduct = $obj->_products->getResProductFromCategory($_GET['id_category']);
				else if (isset($_GET['id_product']))
					$mainProduct = $obj->_products->getResProductFromProduct($_GET['id_product']);
				
				$sessionPotentialAvailabilities = new myOwnAvailabilities($sessionPotentialResa->getStartDateTime(), $sessionPotentialResa->getEndDateTime(), $mainProduct, 0, 0, false, true);
				
				$id_category=Tools::getValue('id_category', 0);
				$categoryMainProduct = $obj->_products->getResProductFromCategory($id_category);

				$isCatOfSession=(intval($_SESSION['categorySelection'])==0 or 
								($sessionPotentialResa->_mainProduct!= null && $categoryMainProduct!=null && $sessionPotentialResa->_mainProduct->sqlId==$categoryMainProduct->sqlId));
		       	$smarty->assign('isCatOfSession',$isCatOfSession);

	       		//display a category
	       		//-----------------------------------------------------------------------------------------------------------------
	       		if ($page!='search' && ($isCatOfSession or $page!='category') && $sessionPotentialResa->_mainProduct!=null && (Configuration::get('MYOWNRES_WIDGET_TYPE')==1 || Configuration::get('MYOWNRES_WIDGET_TYPE')==3)) {
	       			if ($page=='category') {
	       				if (Configuration::get('PS_LAYERED_FULL_TREE')==1) $products=MyOwnReservationsUtils::getProductsIdFromCategoryAndSub($id_category);
	       				else $products=MyOwnReservationsUtils::getProductsIdFromCategory($id_category);
	       			}
	       			else $products=$sessionPotentialResa->_mainProduct->getProductsId();
     			
	       			//$availabilities = new myOwnAvailabilities($sessionPotentialResa->getStartDateTime(), $sessionPotentialResa->getEndDateTime());

	       			$out .= '
	       			<script type="text/javascript">
	       			var productResaPrice=new Array();
	       			var productDefaultAttr=new Array();';
	       			foreach($products as $productId) {
           				$productAttributeId = MyOwnReservationsUtils::getDefaultAttribute($productId);
           				$sessionPotentialResa->id_product = $productId;
           				$sessionPotentialResa->id_product_attribute = $productAttributeId;
           				$sessionPotentialResa->_productQty = -9;
           				$sessionPotentialResa->setPrice($obj->_pricerules->list, null, $sessionPotentialAvailabilities);
		   				
		   				//get available qty without availabilities because they must be set for each product
           				$isAvailable = $sessionPotentialResa->isAvailable($cart, $sessionPotentialAvailabilities, $obj->_stocks) > 0;

           				$out .= '
           					productDefaultAttr['.$productId.']="'.$productAttributeId.'";';
           				if (!$isAvailable) $out .= '
           					productResaPrice['.$productId.']="";';
           				else $out .= '
           					productResaPrice['.$productId.']="'.Tools::displayPrice($sessionPotentialResa->getPrice()).'";';
	       			}
	       			$out .= '
	       			</script>';
	       			
   	       		//display search
   		   		//-----------------------------------------------------------------------------------------------------------------
	       		} else if ($page=='search' && Configuration::get('MYOWNRES_WIDGET_TYPE')==2) {

			   		$search_query = Tools::getValue('search_query');
					$n = abs((int)(Tools::getValue('n', Configuration::get('PS_PRODUCTS_PER_PAGE'))));
					$p = abs((int)(Tools::getValue('p', 1)));
					$search = Search::find($cookie->id_lang, $search_query, 1, 10, 'position', 'desc');			   		

	       			/*if ($page=='category') {
	       				if (Configuration::get('PS_LAYERED_FULL_TREE')==1) $products=MyOwnReservationsUtils::getProductsIdFromCategoryAndSub($id_category);
	       				else $products=MyOwnReservationsUtils::getProductsIdFromCategory($id_category);
	       			}
	       			else $products=$sessionPotentialResa->_mainProduct->getProductsId();*/
	       			
	       			//$availabilities = new myOwnAvailabilities($sessionPotentialResa->getStartDateTime(), $sessionPotentialResa->getEndDateTime());
	       			$out .= '
	       			<script type="text/javascript">
	       			var productResaPrice=new Array();
	       			var productDefaultAttr=new Array();';
	       			foreach($search['result'] as $searchItem) {
           				$sessionPotentialResa->id_product = $searchItem["id_product"];
           				$sessionPotentialResa->id_product_attribute = $searchItem["id_product_attribute"];
           				$sessionPotentialResa->_mainProduct=$obj->_products->getResProductFromProduct($sessionPotentialResa->id_product);
           				
           				if ($sessionPotentialResa->_mainProduct!=null) {
	           				$sessionPotentialResa->setPrice($obj->_pricerules->list, null, $sessionPotentialAvailabilities);
			   				
			   				//get available qty without availabilities because they must be set for each product
	           				$isAvailable = $sessionPotentialResa->isAvailable($cart, $sessionPotentialAvailabilities, $obj->_stocks) > 0;
           				
	           				$out .= '
	           					productDefaultAttr['.$sessionPotentialResa->id_product.']="'.$sessionPotentialResa->id_product_attribute.'";';
	           				if (!$sessionPotentialResa->isValidLength($sessionPotentialAvailabilities)) $productResaPrice="-1";
	           				elseif (!$isAvailable) $productResaPrice="";
	           				else $productResaPrice = Tools::displayPrice($sessionPotentialResa->getPrice());
			   				$out .= '
	           					productResaPrice['.$sessionPotentialResa->id_product.']="'.$productResaPrice.'";';
           				}
	       			}
	       			$out .= '
	       			</script>';
	       		} else if ($page=='category' && Configuration::get('MYOWNRES_WIDGET_TYPE')==2) {
			   		
				
			   		$id_category=Tools::getValue('id_category', 0);
			   		$mainProduct = $obj->_products->getResProductFromCategory($id_category);			   		
			   		$sessionPotentialResa->_mainProduct = $mainProduct;
			   		
			   		$sessionPotentialAvailabilities = new myOwnAvailabilities($sessionPotentialResa->getStartDateTime(), $sessionPotentialResa->getEndDateTime(), $mainProduct, 0, 0, false, true);
			   		
			   		$products==array();
			   		if ($mainProduct!=null) {
       					if (Configuration::get('PS_LAYERED_FULL_TREE')==1) $products=MyOwnReservationsUtils::getProductsIdFromCategoryAndSub($id_category);
	   					else $products=MyOwnReservationsUtils::getProductsIdFromCategory($id_category);
	   				}
	   				$smarty->assign('isCatOfSession',true);
	   				
	       			$out .= '
	       			<script type="text/javascript">
	       			var productResaPrice=new Array();
	       			var productDefaultAttr=new Array();';
	       			foreach($products as $productId) {
           				$productAttributeId = MyOwnReservationsUtils::getDefaultAttribute($productId);
           				$sessionPotentialResa->id_product = $productId;
           				$sessionPotentialResa->id_product_attribute = $productAttributeId;
           				
           				$sessionPotentialResa->_productQty = $mainProduct->getProductQuantity($productId, $productAttributeId, $obj->_stocks, $sessionPotentialAvailabilities);
		   				$sessionPotentialResa->_reservations = null;
           				
           				$sessionPotentialResa->setPrice($obj->_pricerules->list, null, $sessionPotentialAvailabilities);
		   				//echo '@'.$sessionPotentialResa->_productQty;
		   				//get available qty without availabilities because they must be set for each product
           				$isAvailable = $sessionPotentialResa->isAvailable($cart, $sessionPotentialAvailabilities, $obj->_stocks) > 0;
       				
           				$out .= '
           					productDefaultAttr['.$sessionPotentialResa->id_product.']="'.$sessionPotentialResa->id_product_attribute.'";';
           				if (!$sessionPotentialResa->isValidLength($sessionPotentialAvailabilities)) $productResaPrice="-1";
           				elseif (!$isAvailable) $productResaPrice="";
           				else $productResaPrice = Tools::displayPrice($sessionPotentialResa->getPrice());
		   				$out .= '
           					productResaPrice['.$sessionPotentialResa->id_product.']="'.$productResaPrice.'";';
	       			}
	       			$out .= '
	       			</script>';
	       		}
	       			       		
   	       		//display min price
   		   		//-----------------------------------------------------------------------------------------------------------------
			} else if (Configuration::get('MYOWNRES_PRICE_TYPE')==reservation_price_display::MIN) {
				$defaultResa = new myOwnPotentialResa();
				$defaultResa->quantity = 1;
				$defaultResa->startDate = 0;
				$defaultResa->startTimeslot = 0;
				$defaultResa->endDate = 0;
				$defaultResa->endTimeslot = 0;
				$defaultResa->_fixedLength = 1;
				
				if ($page=='category') {
					$id_category = Tools::getValue('id_category', 0);
					$categoryMainProduct = $obj->_products->getResProductFromCategory($id_category);
					
					if (Configuration::get('PS_LAYERED_FULL_TREE')==1) $products=MyOwnReservationsUtils::getProductsIdFromCategoryAndSub($id_category);
		       		else $products=MyOwnReservationsUtils::getProductsIdFromCategory($id_category);
		       			
					if ($categoryMainProduct!=null) {
						$defaultResa->_mainProduct = $categoryMainProduct;
						if ($categoryMainProduct->_reservationMinLength>0) 
							$defaultResa->_fixedLength = $categoryMainProduct->_reservationMinLength;
		       			
		       			$out .= '
		       			<script type="text/javascript">
		       			var productMinPrice=new Array();';
		       			foreach($products as $productId) {
	           				$productAttributeId = MyOwnReservationsUtils::getDefaultAttribute($productId);
	           				$defaultResa->id_product = $productId;
	           				$defaultResa->id_product_attribute = $productAttributeId;
		           			$defaultResa->setPrice($obj->_pricerules->list);
	
		           			$out .= '
		           					productMinPrice['.$productId.']="'.Tools::displayPrice($defaultResa->getPrice()).'";';
		       			}
		       			$out .= '
		       			</script>';
		       		} else {
			       		$out .= '
		       			<script type="text/javascript">
		       			var productMinPrice=new Array();';
		       			foreach($products as $productId) {
		       				$productMainProduct = $obj->_products->getResProductFromProduct($productId);
		       				if ($productMainProduct !=null ) {
	           					$productAttributeId = MyOwnReservationsUtils::getDefaultAttribute($productId);
			   					$defaultResa->id_product = $productId;
			   					$defaultResa->id_product_attribute = $productAttributeId;
			   					$defaultResa->_mainProduct = $productMainProduct;

		           				$defaultResa->setPrice($obj->_pricerules->list);
	
		           				$out .= '
		           					productMinPrice['.$productId.']="'.Tools::displayPrice($defaultResa->getPrice()).'";';
	           				
	           				}
		       			}
		       			$out .= '
		       			</script>';
		       		}
		       		

	       			
				} else if ($page=='search') {
					$search_query = Tools::getValue('search_query');
					$n = abs((int)(Tools::getValue('n', Configuration::get('PS_PRODUCTS_PER_PAGE'))));
					$p = abs((int)(Tools::getValue('p', 1)));
					$search = Search::find($cookie->id_lang, $search_query, 1, 10, 'position', 'desc');	
					
					$out .= '
	       			<script type="text/javascript">
	       			var productMinPrice=new Array();';
	       			foreach($search['result'] as $searchItem) {
           				$defaultResa->id_product = $searchItem["id_product"];
           				$defaultResa->id_product_attribute = $searchItem["id_product_attribute"];
           				$defaultResa->_mainProduct = $obj->_products->getResProductFromProduct($defaultResa->id_product);
           				
           				if ($defaultResa->_mainProduct!=null) {
           					if ($defaultResa->_mainProduct->_reservationMinLength>0) 
								$defaultResa->_fixedLength = $defaultResa->_mainProduct->_reservationMinLength;
	           				$defaultResa->setPrice($obj->_pricerules->list);
			   				$out .= '
           					productMinPrice['.$defaultResa->id_product.']="'.Tools::displayPrice($defaultResa->getPrice()).'";';
           				}
	       			}
	       			$out .= '
	       			</script>';
	       		} else if ($page=='') {
	       			$products=array();
	       			$productsFull = $obj->_products->getProducts($obj->getContext()->language->id);
	       			foreach($productsFull as $product)
	       				if (!array_key_exists($product['id_product'], $products))
		   					$products[] = (int)$product['id_product'];

	       			//Home featured
	       			/*$id_category = Configuration::get('HOME_FEATURED_CAT');
	       			if ($id_category)
	       				$products = MyOwnReservationsUtils::getProductsIdFromCategory($id_category);
	       			
	       			//New products
	       			if (Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) {
		   				$newProducts = Product::getNewProducts((int) $obj->getContext()->language->id, 0, (int)Configuration::get('NEW_PRODUCTS_NBR'));
		   				foreach ($newProducts as $newProduct)
		   					if (!array_key_exists($newProduct['id_product'], $products))
		   						$products[] = (int)$newProduct['id_product'];
		   			}
		   			
		   			//Best sellers
		   			if (Configuration::get('PS_BLOCK_BESTSELLERS_DISPLAY')) {
			   			$bestProducts = ProductSale::getBestSalesLight((int)$obj->getContext()->language->id, 0, (int)Configuration::get('PS_BLOCK_BESTSELLERS_TO_DISPLAY'));
		   			}*/

	       			$out .= '
	       			<script type="text/javascript">
	       			var productMinPrice=new Array();';
	       			foreach($products as $productId) {
           				$defaultResa->id_product = $productId;
           				$productAttributeId = MyOwnReservationsUtils::getDefaultAttribute($productId);
           				$defaultResa->id_product_attribute = $productAttributeId;
           				$defaultResa->_mainProduct = $obj->_products->getResProductFromProduct($defaultResa->id_product);
           				
           				if ($defaultResa->_mainProduct!=null) {
           					if ($defaultResa->_mainProduct->_reservationMinLength>0) 
								$defaultResa->_fixedLength = $defaultResa->_mainProduct->_reservationMinLength;
	           				$defaultResa->setPrice($obj->_pricerules->list);
			   				$out .= '
           					productMinPrice['.$defaultResa->id_product.']="'.Tools::displayPrice($defaultResa->getPrice()).'";';
           				}
	       			}
	       			$out .= '
	       			</script>';
				}
			}
		}
		
		//patch for all products search
		//-----------------------------------------------------------------------------------------------------------------
		if (isset($_GET['id_category']) && $_GET['id_category']==2 && Tools::getIsset('searchStartDay')) {
			$category = new Category(2);
			$out .= '
			<script type="text/javascript">
			$(document).ready(function() {
				$("h1").html($("h1").html().replace("'.$category->name[$cookie->id_lang].'","'.$obj->l('Available products','hooks').'"));
			});
			</script>';
		}
		
		$smarty->assign('myOwnPriceDisplay',(Configuration::get('MYOWNRES_PRICE_TYPE')!=reservation_price_display::HIDE));
		$smarty->assign('myOwnPriceType',intval(Configuration::get('MYOWNRES_PRICE_TYPE')));
		$smarty->assign('_PS_VERSION_',_PS_VERSION_);
		$smarty->assign('changeWidgets',Configuration::get('MYOWNRES_WIDGET_SHOW')!=1);
		$smarty->assign('addToCartOnList',intval(Configuration::get('MYOWNRES_ADDTOCART_ONLIST')!=1));
		$link=new Link();
		$smarty->assign('cartLink', $link->getPageLink('cart.php'));
		$out .= $obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/hook/script_header.tpl');
 
	    //patch cart displayed on step 3
	    //-----------------------------------------------------------------------------------------------------------------
	    if ($ctrl=='order' && $step==3) {
	    	$summary = $cart->getSummaryDetails();
	    	$out.=self::execShoppingCartExtra($obj, $smarty, $cookie, array('summmary' => $summary));
	    }
	    
		if ($ctrl=='orderdetail' && _PS_VERSION_ >= "1.7.0.0") {
			$id_order=Tools::getValue('id_order');
			$order=new Order($id_order);
			$out .= self::displayOrderDetailDisplayed($obj, $order, $smarty, $cookie);
		}

		if ($ctrl=='cart' && _PS_VERSION_ >= "1.7.0.0" && $hideStep1) {
			$rawData = Db::getInstance()->getValue(
	            'SELECT checkout_session_data FROM '._DB_PREFIX_.'cart WHERE id_cart = '.(int) $obj->getContext()->cart->id
		        );
	        $data = json_decode($rawData, true);
	        
	        if ((empty($data) || ($hideStep2 && $data['checkout-payment-step']['step_is_reachable']==0)) && $obj->getContext()->customer->id) {
	        	$cartChecksum = new CartChecksum(new AddressChecksum());
				$data['checkout-personal-information-step'] = array ( 'step_is_reachable' => 1, 'step_is_complete' => 1 );
				$data['checkout-addresses-step'] = array ( 'step_is_reachable' => 1, 'step_is_complete' => 1 );
				$data['checkout-payment-step'] = array ( 'step_is_reachable' => 1, 'step_is_complete' => 0 );
		        $data['checksum'] = $cartChecksum->generateChecksum($obj->getContext()->cart);
		
		        Db::getInstance()->execute(
		            'UPDATE '._DB_PREFIX_.'cart SET checkout_session_data = "'.pSQL(json_encode($data)).'"
		                WHERE id_cart = '.(int) $obj->getContext()->cart->id
		        );
	        } 
		}
		
	    return $out;
	}
	
	public static function displayShoppingCartFooter($obj, $smarty, $cookie, $params) {
		global $cart;
		$out='';

		//Add extenssion attachements
		foreach ($obj->_extensions->getExtByType(extension_type::CART) as $ext) 
				if (method_exists($ext, "displayShoppingCart"))
					$out.=$ext->displayShoppingCart($obj, $cart);
					
		return $out;
	}
	
	public static function displayAfterCarrier($obj, $params) {
		global $cart;
		$out='';

		//Add extenssion attachements
		foreach ($obj->_extensions->getExtByType(extension_type::CART) as $ext) 
				if (method_exists($ext, "displayCarrier"))
					$out.=$ext->displayCarrier($obj, $cart);
					
		return $out;
	}
	
	public static function displayCarrierList($obj, $params) {
		global $cart;
		$out='';

		//Add extenssion attachements
		foreach ($obj->_extensions->getExtByType(extension_type::CART) as $ext) 
				if (method_exists($ext, "displayCarrier"))
					$out.=$ext->displayCarrier($obj, $cart);
					
		return $out;
	}
	
	public static function displayCheckoutDetails($obj, $params) {
		global $cart, $smarty, $cookie;
		if ($cart->resaCarts == null) $cart->getMyOwnReservationCart();
		if ($cart->resaCarts->suppl_wt == 0) $products = $cart->getProducts(true);

		
		if (_PS_VERSION_ >= "1.7.0.0") smartyRegisterFunction($smarty, 'function', 'displayPrice', array('Tools', 'displayPriceSmarty'));
		$smarty->assign('resv_title',Configuration::get('MYOWNRES_MSG_RESV', $cookie->id_lang));
		$smarty->assign('balance_title',Configuration::get('MYOWNRES_MSG_BALANCE', $cookie->id_lang));
		$smarty->assign('advance_title',Configuration::get('MYOWNRES_MSG_ADVANCE', $cookie->id_lang));
		$smarty->assign('deposit_title',Configuration::get('MYOWNRES_MSG_DEPOSIT', $cookie->id_lang));
		$smarty->assign('show_deposit',(Configuration::get('MYOWNRES_DEPOSIT_SHOW')==1 ? $cart->getMyOwnReservationCart()->deposit : 0));
		if (is_array($params) && isset($params['summary'])) {
			$smarty->assign('myowncart',$params['summary']);
		} else {
			$summary = $cart->getSummaryDetails();
			$smarty->assign('myowncart',$summary);
		}

		$isAjaxLoaded = Tools::getValue('ajax');
		$smarty->assign('isAjaxLoaded',$isAjaxLoaded);
		
		$fee_details=$cart->getMyOwnReservationCart()->fee_details;
		$fee_titles=array();
		foreach ($obj->_extensions->getExtByType(extension_type::GLOBFEE) as $ext) 
			$fee_titles[get_class($ext)]=$ext->feeTitle;
		
		$smarty->assign('fee_details', $fee_details);
		$smarty->assign('fee_titles', $fee_titles);

		return $obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/hook/script_summary.tpl');
	}

	public static function displayCheckoutSummary($obj, $params) {
		return self::displayCheckoutDetails($obj, $params);
	}
	
	public static function displayCustomization($obj, $params) {
		
	}
    
    public static function displayProductPrice($obj, $params) {
    	if (array_key_exists('type', $params) && $params['type']=='unit_price')
	    return self::displayProductListFunctionalButtons($obj, $params);
    }

	public static function displayProductListFunctionalButtons($obj, $params) {
		global $cookie, $cart, $sessionPotentialResa, $sessionPotentialAvailabilities;
		if(session_id() == '') session_start();
		$mainProduct=null;
		$objectSelection = 0;
		if (array_key_exists('objectSelection', $_SESSION))
			$objectSelection = (int)$_SESSION['objectSelection'];
		//$params['id_product_attribute']
		if (isset($params['product']['id_product'])) $mainProduct = $obj->_products->getResProductFromProduct($params['product']['id_product']);
		$isCatOfSession=false;
		if ($mainProduct!=null) {
			if ($sessionPotentialResa==null || $sessionPotentialAvailabilities==null) {
				$sessionPotentialResa = myOwnPotentialResa::getSessionPotentialResa($obj);

				if ($sessionPotentialResa!=null)
					$sessionPotentialAvailabilities = new myOwnAvailabilities($sessionPotentialResa->getStartDateTime(), $sessionPotentialResa->getEndDateTime(), $mainProduct, 0, 0, false, true); //need strict prod = true to set corect availabilities id prod
				else $sessionPotentialAvailabilities = null;
			}
			
			if ($sessionPotentialResa!=null && $sessionPotentialResa->_mainProduct!=null) {
				
				$isCatOfSession=((intval($_SESSION['categorySelection'])==0 && array_key_exists(0, $obj->_products->list))  or 
								($sessionPotentialResa->_mainProduct!= null && $sessionPotentialResa->_mainProduct->sqlId==$mainProduct->sqlId) or 
								(isset($_GET['id_category']) && array_key_exists(intval($_GET['id_category']), $sessionPotentialResa->ids_categories)));

				if ($isCatOfSession) {
					$sessionPotentialResa->id_product = $params['product']['id_product'];
					$sessionPotentialResa->id_product_attribute = $params['product']['id_product_attribute'];
					$sessionPotentialResa->_productPrice=-1;
					$sessionPotentialResa->setPrice($obj->_pricerules->list, null, $sessionPotentialAvailabilities);
					$sessionPotentialResa->_productQty = $mainProduct->getProductQuantity($sessionPotentialResa->id_product, $sessionPotentialResa->id_product_attribute, $obj->_stocks, $sessionPotentialAvailabilities);
					
					$sessionPotentialResa->_reservations = null;
					/*
					//get available qty without availabilities because they must be set for each product
					$isAvailable = $sessionPotentialResa->isAvailable($cart, null, $obj->_stocks);*/
					
					$isAvailable = $sessionPotentialResa->isAvailable($cart, $sessionPotentialAvailabilities, $obj->_stocks) > 0;

					$isValid = $sessionPotentialResa->isValidLength($sessionPotentialAvailabilities);
					if (!$isValid)
						$comment = $obj->l('Period not valid for this product', 'hooks');
					else if (!$isAvailable)
						$comment = $obj->l('Not available for selected period', 'hooks');
					else 
						$comment = $obj->l('Available for selected period', 'hooks');
					
					foreach ($obj->_extensions->getExtByType(extension_type::SEL_WIDGET) as $ext) {
						if (in_array($ext->id, $mainProduct->ids_options))
							if (method_exists($ext, "widgetSelect") && $objectSelection && array_key_exists($objectSelection, $ext->objects->list))
									$comment.=' '.$obj->l('for', 'hooks').' '.strtolower($ext->label).' '.$ext->objects->list[$objectSelection]->name;
					}
			
					return '<div style="display:none" class="myOwnAvailability" id_product="'.$params['product']['id_product'].'" available="'.($isAvailable ? 'true' : 'false').'" valid="'.($isValid ? 'true' : 'false').'"  oldprice="'.Tools::displayPrice($sessionPotentialResa->getOldPrice()).'" price="'.Tools::displayPrice($sessionPotentialResa->getPrice()).'" link="myownreservationCallCart('.$params['product']['id_product'].', '.$params['product']['id_product_attribute'].', 1, \''.$sessionPotentialResa->startDate.'_'.$sessionPotentialResa->startTimeslot.'@'.$sessionPotentialResa->endDate.'_'.$sessionPotentialResa->endTimeslot.'\', this);">'.$comment.'</div>';
				}
			}
			if (!$isCatOfSession) { 
				$unitLabel = $mainProduct->getUnitLabel($obj, $cookie->id_lang);
				return '<div style="display:none" class="myOwnUnit" id_product="'.$params['product']['id_product'].'">'.$unitLabel.'</div>';
			}
		}
	}
	
	
	public static function displayFooter($obj, $params) {
		global $cookie;
		$lang_iso=Language::getIsoById($cookie->id_lang);
		if ($lang_iso=='fr') $url='http://www.labulle.net/myownreservations';
		else if ($lang_iso=='es') $url='http://www.labulle.es/myownreservations-es';
		else $url='http://www.labulle.co.uk/myownreservations-en';
		
		return '';
		return $obj->l('booked with', 'hooks').' <a href="'.$url.'">myOwnReservations</a>';
	}
	
	//==============================================================================================================================================
	//    DISPLAY PRODUCT
	//==============================================================================================================================================
	
	public static function displayProductExtraContent($obj, $params) {
		global $cookie, $smarty;
		$product=$params['product'];
		$extras =  array();
		$extra = new ProductExtraContent();
		$extra->setTitle(Configuration::get('MYOWNRES_MSG_RESV', $cookie->id_lang));
		//$extra->setAttr(array('class' => 'active'));
		$planning='';
		$smarty->assign(array(
			'my_img_dir' => _PS_IMG_,
			'_PS_VERSION_' => _PS_VERSION_,
			'module_dir' => _MODULE_DIR_,
			'planning' => $planning,
			'planningType' => 'product',
			'planningStyle' => 'product',
			'reservation_title' => Configuration::get('MYOWNRES_MSG_RESV', $cookie->id_lang)
		));
				
		$mainProduct = $obj->_products->getResProductFromProduct($product->id);
		
		if ($mainProduct!=null && Configuration::get('MYOWNRES_WIDGET_PROD', 0)==widget_position::TABS) {
			$styles=$obj->_pricerules->getStyles($mainProduct->sqlId, $product->id);
			$planning = $obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/hook/reservation_tab.tpl');
			$extra->setContent($styles.$planning);
	//$extra->setContent('test');
			$extras[]=$extra;
		}
		if (Configuration::get('MYOWNRES_PRICERULE_LEGEND')==widget_position::TABS) {
			$legend = self::displayLegend($obj, $mainProduct, $product->id);
			if ($legend!='') {
				$extra = new ProductExtraContent();
				$extra->setTitle($obj->l('Special offers', 'hooks'));
				$extra->setContent($legend);
				$extras[]=$extra;
			}
		}

		if (Configuration::get('MYOWNRES_PRICESET_LEGEND')==widget_position::TABS) {
			$legend = self::displayPricesList($obj, $product);
			if ($legend!='') {
				$extra = new ProductExtraContent();
				$extra->setTitle($obj->l('Prices list', 'hooks'));
				$extra->setContent($legend);
				$extras[]=$extra;
			}
		}
		
		return $extras;
	}
	
	public static function displayLegend($obj, $mainProduct, $id_product) {
		global $smarty;

		if (Tools::getIsSet('ipa')) $productAttributeId = intval(Tools::getValue('ipa'));
		else $productAttributeId=MyOwnReservationsUtils::getDefaultAttribute($id_product);
		$potentialRules=array();
		$potentialRulesConditions=array();
		foreach ($obj->_pricerules->list as $key => $pricerule)
			if ($pricerule->enable==1 && $pricerule->visible==1 && $pricerule->doesApplyToProduct($mainProduct->sqlId, $id_product, $productAttributeId)) {
				$potentialRules[$key]=$pricerule;
				$potentialRulesConditions[$key]=$obj->l('For a', 'hooks').' '.myOwnLang::$priceruleTypes[$pricerule->type].' '.$pricerule->getConditionToString($obj, $mainProduct);
			}
		$smarty->assign('potentialRules', $potentialRules);
		$smarty->assign('potentialRulesConditions', $potentialRulesConditions);
		$smarty->assign('mainProduct', $mainProduct);
		if (count($potentialRules)) return $obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/front/planning/legend.tpl');
		return '';
	}
	
	public static function displayPricesList($obj, $product) {
		global $cookie, $smarty;
		$lastRaiseWT=array();
		$out='';
		
		$mainProduct = $obj->_products->getResProductFromProduct($product->id);
		if ($mainProduct==null) return '';
		if ($mainProduct->_reservationPriceType!=reservation_price::TABLE_TIME && $mainProduct->_reservationPriceType!=reservation_price::TABLE_DATE) return '';
		$with_taxes = (Configuration::get('MYOWNRES_PRICE_TAXEXCL')!=1);
		if ($with_taxes) $tax_rate = $product->getTaxesRate();
		else $tax_rate = 0;
		$sattributes = MyOwnReservationsUtils::getProductAttributes($product->id);
		$productPrices = array();
		$productAttributes=array();
		$productAttributesPrices=array();
		$productAttributesPricesFiltred=array();
		$productAttributesPricesFinal=array();
		$productPricesPeriod=array();
		$maxlength=1;
		foreach ($sattributes as $attribute) {
			if ($attribute>0) {
				$productPrices[$attribute] = MyOwnReservationsUtils::getProductPriceTaxes($product->id, $attribute, false, false, 1);
				$productPrices_wt[$attribute] = MyOwnReservationsUtils::getProductPriceTaxes($product->id, $attribute, true, false, 1);
				$productAttr = MyOwnReservationsUtils::getAttributeCombinaisons($product->id, $attribute, $cookie->id_lang);
				$productAttributes[$attribute] = str_replace("<br />", " - ", $productAttr);
				$productAttributesPrices[$attribute] = myOwnPricesSets::getPricesSetForProduct($product->id, $attribute);
				if (count($productAttributesPrices[$attribute])) {
					if (max(array_keys($productAttributesPrices[$attribute]))>$maxlength)
						$maxlength = max(array_keys($productAttributesPrices[$attribute]));
					if ($mainProduct->_reservationPriceType==reservation_price::TABLE_TIME && array_key_exists(1, $productAttributesPrices[$attribute]))
						$productAttributesPricesFiltred[$attribute]=array (1 => $productAttributesPrices[$attribute][1]);
				}
			}
		}

if ($mainProduct->_reservationPriceType==reservation_price::TABLE_TIME) {
		//filter price to eject prices = price of length -1 + prodcut price
		if ($maxlength==1) return '';
		$lastRaise=array();
		$lastRaiseTemp=array();
		
		foreach ($sattributes as $attribute)
			$lastRaise[$attribute] = 0;
			
		for ($length=2;$length<$maxlength;$length++) {
			$filtered=true;
			foreach ($sattributes as $attribute)
				if (array_key_exists($length-1, $productAttributesPrices[$attribute])) {
					$lastRaiseTemp[$attribute] = ceil($productAttributesPrices[$attribute][$length+1]-$productAttributesPrices[$attribute][$length]);
					$filtered = ($filtered && $lastRaiseTemp[$attribute]==$lastRaise[$attribute]);
				}
			
			foreach ($sattributes as $attribute) {
				if ($mainProduct->_reservationPriceType==reservation_price::TABLE_TIME) {
					$raisewithprodprice = ceil($productAttributesPrices[$attribute][$length+1])==ceil($productAttributesPrices[$attribute][$length]+$productPrices[$attribute]);
					if (array_key_exists($length+1, $productAttributesPrices[$attribute]))
						$lastRaise[$attribute] = ceil($productAttributesPrices[$attribute][$length+1]-$productAttributesPrices[$attribute][$length]);
					if (array_key_exists($length+1, $productAttributesPrices[$attribute]))
						$lastRaiseWT[$attribute] = round($productAttributesPrices[$attribute][$length+1]*(1+($tax_rate/100))-$productAttributesPrices[$attribute][$length]*(1+($tax_rate/100)));
						
					if (!$raisewithprodprice && !$filtered) {
						$productAttributesPricesFiltred[$attribute][$length] = $productAttributesPrices[$attribute][$length];
					} else if ($filtered) {
						$productAttributesPricesFinal[$attribute][$length] = $productAttributesPrices[$attribute][$length]*(1+($tax_rate/100));
					}
				}
			}
			
		}
}

		if ($mainProduct->_reservationPriceType==reservation_price::TABLE_TIME) {
			$maxpricelength=1;
			foreach ($sattributes as $attribute)
				if (max(array_keys($productAttributesPricesFiltred[$attribute]))>$maxpricelength)
					$maxpricelength = max(array_keys($productAttributesPricesFiltred[$attribute]));
			if ($maxpricelength==1) return '';
		
			for ($length=1;$length<=$maxpricelength;$length++)
				foreach ($sattributes as $attribute) {
					if (!array_key_exists($length, $productAttributesPricesFiltred[$attribute]) && !array_key_exists($length, $productAttributesPricesFinal[$attribute]))
						$productAttributesPricesFinal[$attribute][$length] = ($productAttributesPricesFinal[$attribute][$length-1] + $productPrices[$attribute])*(1+($tax_rate/100));
					else if (array_key_exists($length, $productAttributesPricesFiltred[$attribute])) $productAttributesPricesFinal[$attribute][$length] = $productAttributesPricesFiltred[$attribute][$length]*(1+($tax_rate/100));
				}

		} else {
			$periods = $mainProduct->getPeriodUnits($cookie, $obj);

			$maxpricelength=365;
			
			foreach ($sattributes as $attribute) {
				$keys = array_keys($productAttributesPrices[$attribute]);
				$lastprice=0;$lastpricekey=0;
				foreach ($keys as $yearday) {
					if ($lastprice==0) {
						$lastprice = $productAttributesPrices[$attribute][$yearday];
						$lastpricekey = $yearday;
						$productPricesPeriod[$yearday]=$periods[$yearday];
					} else {
						if ($productAttributesPrices[$attribute][$yearday] != $lastprice) {
							if ($yearday==$lastpricekey+1 || $lastpricekey==$previouskey)
								$productPricesPeriod[$lastpricekey]=$periods[$lastpricekey];
							else $productPricesPeriod[$lastpricekey]=$periods[$lastpricekey].' - '.$periods[$previouskey];
							$lastprice = $productAttributesPrices[$attribute][$yearday];
							$lastpricekey = $yearday;
						}
					}
					$previouskey=$yearday;
				}
				if (count($keys) && $lastpricekey != $yearday) {
					if ($previouskey==$lastpricekey) $productPricesPeriod[$lastpricekey]=$periods[$lastpricekey];
					else $productPricesPeriod[$lastpricekey]=$periods[$lastpricekey].' - '.$periods[$previouskey];
				}
			}
			$productAttributesPricesFinal = $productAttributesPrices;
		}		
				
				
		$accessoriesPrices = array();
		$accessories = $product->getAccessories($cookie->id_lang);
		if ($accessories != array())
			foreach ($accessories as $accessorie) {
				$accessoriesPrices[$accessorie['name']]=$accessorie['price'];
			}
		
		$smarty->assign(array(
			'my_img_dir' => _PS_IMG_,
			'_PS_VERSION_' => _PS_VERSION_,
			'module_dir' => _MODULE_DIR_,
			'istimeprice' => $mainProduct->_reservationPriceType==reservation_price::TABLE_TIME,
			'maxlength' => $maxlength,
			'maxpricelength' => $maxpricelength,
			'productAttributes' => $productAttributes,
			'productPrices' => $productPrices_wt,
			'productAttributesPrices' => $productAttributesPricesFinal,
			'productPricesPeriod' => $productPricesPeriod,
			'accessoriesPrices' => $accessoriesPrices,
			'unit' => $mainProduct->getUnitLabel($obj, $cookie->id_lang, false),
			'units' => $mainProduct->getUnitLabel($obj, $cookie->id_lang, true),
			'lastRaise' => $lastRaiseWT
		));

		$out = $obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/hook/price_list.tpl');
		return $out;
	}
	
	public static function displayProductDeliveryTime($obj, $params) {
		global $cookie;
		/*$out='';
		$mainProduct = null;

		if (isset($params['product']['id_product'])) $id_product=$params['product']['id_product'];
		$mainProduct = $obj->_products->getResProductFromProduct($id_product);
		if ($mainProduct==null) return '';
		
		$start = $mainProduct->getReservationStart();
		$length = $mainProduct->reservationPeriod; 
		$end = strtotime("+".$length." day",$start);
		
		$availabilities = new myOwnAvailabilities($start, $end, $mainProduct, intval($id_product), 0, false, true);
		$days = myOwnCalendarDay::getFilteredOnPeriod($start, $end, $mainProduct->_timeslotsObj, 0, $availabilities);
		$timeslots = $mainProduct->_timeslotsObj->list;
		foreach($days as $day) {
			foreach($timeslots as $timeslot) {
				if ($availabilities->isAvailable($day->date, $timeslot, $id_product)) {
					$out .= $obj->l('Available from', 'hooks').' '.MyOwnCalendar::formatDateWithDay($day->date, $cookie->id_lang);
					if ($mainProduct->reservationSelType==reservation_unit::TIMESLOT)
						$out .= ' '.$obj->l('on', 'hooks').' '.$timeslot->getStartHourTimeStr($cookie->id_lang);
					return $out;
				}
			}
		}

		if (array_key_exists(0, $days))
			return $obj->l('Available from', 'hooks').' '.MyOwnCalendar::formatDateWithDay($days[0]->date, $cookie->id_lang);
		else return $obj->l('Not available', 'hooks');*/
	}

	public static function displayRightColumnProduct($obj, $params) {
		global $smarty, $cookie;
		$out='';
		$product=null;
		if (get_class($obj->getContext()->controller)=='ProductController')
			$product = $obj->getContext()->controller->getProduct();
		else if ($_GET['id_product'])
			$product = new Product($_GET['id_product']);
		if ($product==null) return '';
		$mainProduct = $obj->_products->getResProductFromProduct($product->id);
		if ($mainProduct==null) return '';
		if ($mainProduct!=null) {
			if (Configuration::get('MYOWNRES_WIDGET_PROD', 0)==widget_position::RIGHT) {
				$styles = $obj->_pricerules->getStyles($mainProduct->sqlId, $product->id);
				$smarty->assign(array(
					'my_img_dir' => _PS_IMG_,
					'_PS_VERSION_' => _PS_VERSION_,
					'module_dir' => _MODULE_DIR_,
					'planning' => '',
					'planningType' => 'product',
					'planningStyle' => 'topproduct',
					'reservation_title' => Configuration::get('MYOWNRES_MSG_RESV', $cookie->id_lang)
				));
				$out .= $styles.$obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/hook/reservation_tab.tpl');
			}
			if (Configuration::get('MYOWNRES_PRICESET_LEGEND')==widget_position::RIGHT)
				$out .= self::displayPricesList($obj, $product);
			if (Configuration::get('MYOWNRES_PRICERULE_LEGEND')==widget_position::RIGHT)
				$out .= self::displayLegend($obj, $mainProduct, $product->id);
		}
		return $out;
	}
	
	public static function displayProductTab($obj, $smarty, $cookie)
	{
		$out='';
		$product=null;
		if (get_class($obj->getContext()->controller)=='ProductController')
			$product = $obj->getContext()->controller->getProduct();
		else if ($_GET['id_product'])
			$product = new Product($_GET['id_product']);
		if ($product==null) return '';
		$mainProduct = $obj->_products->getResProductFromProduct($product->id);
		if ($mainProduct!=null && Configuration::get('MYOWNRES_WIDGET_PROD', 0)==widget_position::TABS) {
			if (_PS_VERSION_ >= "1.6.0.0" && !MYOWNRES_PS15_THEME) {
				$styles=$obj->_pricerules->getStyles($mainProduct->sqlId, $product->id);
				$planning='';
				$smarty->assign(array(
					'my_img_dir' => _PS_IMG_,
					'_PS_VERSION_' => _PS_VERSION_,
					'module_dir' => _MODULE_DIR_,
					'planning' => $planning,
					'planningType' => 'product',
					'planningStyle' => 'product',
					'reservation_title' => Configuration::get('MYOWNRES_MSG_RESV', $cookie->id_lang)
				));
				//<section class="page-product-box">

				$out .= $obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/hook/reservation_tab_head.tpl').'
					'.$styles.$obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/hook/reservation_tab.tpl');
			//</section>
			}
	     	else $out .= '<li><a id="resa_tab_resa" href="#idTabResa" class="selected"  data-toggle="tab">'.Configuration::get('MYOWNRES_MSG_RESV', $cookie->id_lang).'</a></li>';
		}
		if (_PS_VERSION_ >= "1.6.0.0" && !MYOWNRES_PS15_THEME) {
	        if (Configuration::get('MYOWNRES_PRICERULE_LEGEND')==widget_position::TABS)
	        	$out .= self::displayLegend($obj, $mainProduct, $product->id);
	        if (Configuration::get('MYOWNRES_PRICESET_LEGEND')==widget_position::TABS)
	        	$out .= self::displayPricesList($obj, $product);
        }
        return $out;
	}
	      
    public static function displayProductTabContent($obj, $smarty, $cookie)
	{
		global $_LANG;
		$out = '';
		$product=null;
		if (get_class($obj->getContext()->controller)=='ProductController')
			$product = $obj->getContext()->controller->getProduct();
		else if ($_GET['id_product'])
			$product = new Product($_GET['id_product']);
		if ($product==null) return '';
		
		$mainProduct = $obj->_products->getResProductFromProduct($product->id);
		if ($mainProduct!=null && Configuration::get('MYOWNRES_WIDGET_PROD', 0)==widget_position::TABS) {
			$styles=$obj->_pricerules->getStyles($mainProduct->sqlId, $product->id);
			if (_PS_VERSION_ < "1.6.0.0" || MYOWNRES_PS15_THEME) {
				$planning = '';//myOwnReservationsCalendarController::displayProduct($obj, $cookie, $smarty);
				$smarty->assign(array(
					'my_img_dir' => _PS_IMG_,
					'_PS_VERSION_' => _PS_VERSION_,
					'module_dir' => _MODULE_DIR_,
					'planning' => $planning,
					'planningType' => 'product',
					'planningStyle' => 'product',
					'reservation_title' => Configuration::get('MYOWNRES_MSG_RESV', $cookie->id_lang)
				));
				$out .= $styles.$obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/hook/reservation_tab.tpl');
			} else {
				//need to output something to get displayProductTab displayed
				$out .= '<div style="display:none"></div>';
			}
        }
        if (_PS_VERSION_ < "1.6.0.0" || MYOWNRES_PS15_THEME) {
	        if (Configuration::get('MYOWNRES_PRICERULE_LEGEND')==widget_position::TABS)
	        	$out .= self::displayLegend($obj, $mainProduct, $product->id);
	        if (Configuration::get('MYOWNRES_PRICESET_LEGEND')==widget_position::TABS)
	        	$out .= self::displayPricesList($obj, $product);
        }
        return $out;
	}
	
	public static function displayProductActions($obj, $smarty, $cookie)
	{
		global $cart;
		if(session_id() == '') session_start();
		$params=array();
		$out = '';
		
		if (_PS_VERSION_ >= "1.7.0.0")
			$out .= '<div class="block-categories">'.self::displayRightColumnProduct($obj, $params).'</div>';
		
		$link=new link();
		$mainProduct=null;
		$id_product = Tools::getValue('id_product');
		if (Tools::getIsset('id_product')) $mainProduct = $obj->_products->getResProductFromProduct($id_product);
		//if the product handle reservation
		if ($mainProduct!=null) {
			myOwnReservationsController::_construire($obj);
			$mainProducts = explode(',', $mainProduct->ids_products);
			$sessionPotentialResa=myOwnPotentialResa::getSessionPotentialResa($obj);
			
			$product = new Product($id_product);
			
			$carts = $cart->getMyOwnReservationCart();
			$temp_prod_resa=null;
			foreach ($carts->getResas($id_product) as $resa)
				$temp_prod_resa=clone $resa;
			if ($temp_prod_resa != null) {
				$out .= '
				<script type="text/javascript">
					var productLinkedAction=new Array();
					var productLinkedPrice=new Array();
					var productLinkedAvailable=new Array();';
				$accessories = $product->getAccessories($cookie->id_lang);
				if ($accessories != array())
				foreach ($accessories as $accessorie) {
					if (!in_array($accessorie['id_product'], $mainProducts)) $accMainProduct = $obj->_products->getResProductFromProduct($accessorie['id_product']);
					if (in_array($accessorie['id_product'], $mainProducts) or ($accMainProduct!=null && $accMainProduct->sqlId==$mainProduct->sqlId)) {
						$temp_prod_resa->id_product = $accessorie['id_product'];
						$attribute = MyOwnReservationsUtils::getDefaultAttribute($temp_prod_resa->id_product);
						$temp_prod_resa->id_product_attribute = $attribute;
						
						$accAvailabilities = new myOwnAvailabilities($temp_prod_resa->getStartDateTime(), $temp_prod_resa->getEndDateTime(), $mainProduct, $temp_prod_resa->id_product, $attribute, false, true);
						$temp_prod_resa->setPrice($obj->_pricerules->list);
						$temp_prod_resa->_productQty = $mainProduct->getProductQuantity($temp_prod_resa->id_product, $attribute, $obj->_stocks, $accAvailabilities);
						$temp_prod_resa->_reservations = null;
						$availableQty = $temp_prod_resa->getAvailableQuantity($cart, null, $obj->_stocks);
						$out .= '
	   					productLinkedAvailable['.$temp_prod_resa->id_product.']='.intval(($availableQty > 0 ? 1 : 0)).';
	   					productLinkedAction['.$temp_prod_resa->id_product.']="myownreservationCallCart('.$temp_prod_resa->id_product.', '.$attribute.', '.$temp_prod_resa->quantity.', \''.$temp_prod_resa->startDate.'_'.$temp_prod_resa->startTimeslot.'@'.$temp_prod_resa->endDate.'_'.$temp_prod_resa->endTimeslot.'\', this)";
	   					productLinkedPrice['.$temp_prod_resa->id_product.']="'.Tools::displayPrice($temp_prod_resa->getPrice()).'";';
   					}
				}
				$out .= '
				</script>
				';
			}
			
			$alsoSell=($mainProduct->_optionAlsoSell==1); //&& MyOwnReservationsUtils::getProductPriceRatio($id_product)>0
			$hidePrice=(Configuration::get('MYOWNRES_PRICE_TYPE')==reservation_price_display::HIDE);
			$attributes = MyOwnReservationsUtils::getProductAttributes($id_product);
			$out .= '
			<script type="text/javascript">
				var productAttrPrice=new Array();
				var productAttrOldPrice=new Array();
				var productAttrMinPrice=new Array();
				var productAttrQty=new Array();
				var productAttrAvailable=new Array();
				var productAttrLabel=new Array();';

			//calculating price for each conbination
			if ($sessionPotentialResa!=null && $mainProduct->sqlId == $sessionPotentialResa->_mainProduct->sqlId) {
				foreach($attributes as $attribute) {
					$sessionPotentialAvailabilities = new myOwnAvailabilities($sessionPotentialResa->getStartDateTime(), $sessionPotentialResa->getEndDateTime(), $mainProduct, $id_product, $attribute, false, true);
					$sessionPotentialResa->id_product = $id_product;
					$sessionPotentialResa->id_product_attribute = $attribute;
					$desiredShift = $mainProduct->getDesiredShift($sessionPotentialResa);

					if ($desiredShift>1 && $desiredShift>$sessionPotentialResa->getLength(true)) {
						$realShif = $mainProduct->getShiftTimeSlot(strtotime($sessionPotentialResa->startDate), $sessionPotentialResa->startTimeslot, $desiredShift, true, null, $endDate, $sessionPotentialResa->endTimeslot, $id_product, $attribute, $sessionPotentialResa->_reservationEnd);
					}
					$sessionPotentialResa->setPrice($obj->_pricerules->list);
					$sessionPotentialResa->_productQty = $mainProduct->getProductQuantity($id_product, $attribute, $obj->_stocks, $sessionPotentialAvailabilities);
					$sessionPotentialResa->_reservations = null;
					$availableQty = $sessionPotentialResa->getAvailableQuantity($cart, null, $obj->_stocks);

					$out .= '
   					productAttrQty['.$attribute.']='.intval(($availableQty > -1 ? $availableQty : 0)).';
   					productAttrAvailable['.$attribute.']='.intval(($availableQty != 0 ? 1 : 0)).';
   					productAttrPrice['.$attribute.']="'.Tools::displayPrice($sessionPotentialResa->getPrice()).'";
   					productAttrOldPrice['.$attribute.']="'.Tools::displayPrice($sessionPotentialResa->getOldPrice()).'";
   					productAttrLabel['.$attribute.']="'.str_replace("<br>"," ",$sessionPotentialResa->toString($obj, $cookie->id_lang, true)).'";';
				}
			} else $sessionPotentialResa=null;
			
			if (Configuration::get('MYOWNRES_PRICE_TYPE')==reservation_price_display::MIN) {
				$defaultResa = new myOwnPotentialResa();
				$defaultResa->id_product = $id_product;
				$defaultResa->quantity = 1;
				$defaultResa->startDate = 0;
				$defaultResa->startTimeslot = 0;
				$defaultResa->endDate = 0;
				$defaultResa->endTimeslot = 0;
				$defaultResa->_fixedLength = 1;
				$defaultResa->_mainProduct = $mainProduct;
				if ($mainProduct->_reservationMinLength>0) 
					$defaultResa->_fixedLength = $mainProduct->_reservationMinLength;
				
				foreach($attributes as $attribute) {
					$defaultResa->id_product_attribute = $attribute;
					$defaultResa->setPrice($obj->_pricerules->list);
					$out .= '
	   					productAttrMinPrice['.$attribute.']="'.Tools::displayPrice($defaultResa->getPrice()).'";';
   				}
			}
			$out .= '
			</script>
			';
			$isResaSession=($sessionPotentialResa != null && $_SESSION['startSelection']!="" && $mainProduct->sqlId == $sessionPotentialResa->_mainProduct->sqlId);
			$homeselection=Tools::getValue('selection');
			$homesel=Tools::getValue('sel');
			$smarty->assign(array(
				'productLink' => $link->getProductLink($id_product),
				'isResaSession' => $isResaSession,
				'id_product' => $id_product,
				'_PS_VERSION_' => _PS_VERSION_,
				'isquickview' => (Tools::getValue('action')=='quickview'),
				'defaultcombinaison' => MyOwnReservationsUtils::getDefaultAttribute($id_product),
				'myHomeSelection' => $homeselection,
				'myHomePage' => $homesel,
				'myPlanningBox' => Configuration::get('MYOWNRES_WIDGET_PROD', 0)==widget_position::POPUP,
				'imgDir' => _PS_IMG_,
				'myOwnResPriceUnit' => $mainProduct->getUnitLabel($obj, $cookie->id_lang),
				'myOwnResAlsoSell' => intval($alsoSell),
				'myOwnResWay' => ($mainProduct->_reservationLengthControl == reservation_length::OPTIONAL),
				'myOwnResLblStart' => $mainProduct->getStartLabel($cookie->id_lang),
				'myOwnResLblEnd' => $mainProduct->getStopLabel($cookie->id_lang),
				'myOwnResIgnoreProdQty' => ($mainProduct->_qtyType == reservation_qty::SEARCH || $mainProduct->_qtyType == reservation_qty::FIXED),
				'myOwnPriceDisplay' => intval(Configuration::get('MYOWNRES_PRICE_TYPE')!=reservation_price_display::HIDE),
				'myOwnPriceType' => intval(Configuration::get('MYOWNRES_PRICE_TYPE'))
			));

			if ($isResaSession) {
				$smarty->assign('potentialResa',$sessionPotentialResa);
				$smarty->assign('resaSessionString',str_replace("<br>"," ",$sessionPotentialResa->toString($obj, $cookie->id_lang, true)));
				$smarty->assign('resaSessionPrice',$sessionPotentialResa->getPrice().' '.myOwnUtils::getCurrency());
			}
			$out .= $obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/hook/script_product.tpl');
			
			
			$obj_exts = $obj->_extensions->getExtByType(extension_type::OBJECT);
			foreach ($mainProduct->ids_options as $id_option)
				if (array_key_exists($id_option, $obj_exts)) {
					$obj_sel = $obj_exts[$id_option]->productSelect($obj);
					if ($obj_sel != '') {
						$out.= $obj_sel;
						$out.= '<script type="text/javascript">document.addEventListener("DOMContentLoaded", function(event) {$("#quantity_wanted_p, .product-add-to-cart").before($("#myownr_'.$obj_exts[$id_option]->name.'_fieldset"));});</script>';
					}
				} 
			
			foreach ($obj->_extensions->getExtByType(extension_type::SEL_QTY) as $ext) {
		// 				if (in_array($ext->id, $resa->_mainProduct->ids_notifs)) {
					if (method_exists($ext, "displayProductAction"))
						$out .= $ext->displayProductAction($obj, $mainProduct, $id_product);
			}
			
		  	return $out;
		}
		
	}
	
	public static function forceTranslation($mod, $comp, $string, $new) {
		global $_MODULES;
		global $_LANG;
		if ($mod!='') {
			$currentKey = '<{'.strtolower($mod).'}'.strtolower(_THEME_NAME_).'>'.strtolower($mod).'_'.md5($string);
			$defaultKey = '<{'.strtolower($mod).'}prestashop>'.strtolower($mod).'_'.md5($string);
			
			$_MODULES[$currentKey] = $new;
			$_MODULES[Tools::strtolower($currentKey)] = $new;
			$_MODULES[$defaultKey] = $new;
			$_MODULES[Tools::strtolower($defaultKey)] = $new;
		}
		if ($comp!='') {
			$defaultKey = strtolower($comp).'_'.md5($string);

			$_LANG[$defaultKey] = $new;
			$_LANG[Tools::strtolower($defaultKey)] = $new;
		}
	}

	//==============================================================================================================================================
	//     DISPLAY COLUMN
	//==============================================================================================================================================	
	 
    public static function displayColumn($obj, $smarty, $cookie, $params, $column) {
	    //echo 'zasdasdasd';

        return myOwnReservationsCalendarController::displayWidget($obj);

		//check ===false mean no value set so we consider true if no value becaues option has beed added after
    	if (Configuration::get('MYOWNRES_WIDGET_TYPE')>0 && ((_PS_VERSION_ >= "1.6.0.0" && (Configuration::get('MYOWNRES_WIDGET_COL')===false || Configuration::get('MYOWNRES_WIDGET_COL')==1)) or (_PS_VERSION_ < "1.6.0.0" && $column=='right' && Configuration::get('MYOWNRES_WIDGET_POS')==1) or (_PS_VERSION_ < "1.6.0.0" && $column=='left' && Configuration::get('MYOWNRES_WIDGET_POS')==0))) {
    		//return myOwnReservationsCalendarController::displayWidget($obj);
    	}
    }
    
	public static function displayTopColumn($obj) {
		$ctrl = Tools::getValue('controller');
		if ($ctrl=='index' && Configuration::get('MYOWNRES_WIDGET_TOP')) return myOwnReservationsCalendarController::displayTopWidget($obj);
	}
	
	public static function displayNav($obj, $params) {
		$ctrl = Tools::getValue('controller');
		if ($ctrl=='index' && Configuration::get('MYOWNRES_WIDGET_TOP')) return '</div>'.myOwnReservationsCalendarController::displayTopWidget($obj).'<div>';
	}
	
	//==============================================================================================================================================
	//     DISPLAY HOME PAGE
	//==============================================================================================================================================	

		
	public static function displayHomeTab($obj) {
		$param=Configuration::get('MYOWNRES_WIDGET_HOME');
		if (trim($param)!='' && $param>0 && count($obj->_products->list)>0) {
			return '<li><a data-toggle="tab" href="#blockmyownreservations" class="blockmyownreservations">'.(Configuration::get('MYOWNRES_WIDGET_HOME') <3 ? ucfirst(myOwnLang::$objects[MYOWN_OBJ::AVAILABILITY]) : Configuration::get('MYOWNRES_MSG_RESV', $cookie->id_lang)).'</a></li>';
		}
	}
	
	public static function displayHomeTabContent($obj) {
		$param=Configuration::get('MYOWNRES_WIDGET_HOME');
		if (trim($param)!='' && $param>0 && count($obj->_products->list)>0) {
			return '<div id="blockmyownreservations" class="product_list grid row blockmyownreservations tab-pane">'.(Configuration::get('MYOWNRES_WIDGET_HOME') <3 ? myOwnReservationsCalendarController::displayHome($obj) : myOwnReservationsCalendarController::displayTopWidget($obj)).'</div>';
		}
	}
		
	public static function displayHome($obj, $params) {
		$param=Configuration::get('MYOWNRES_WIDGET_HOME');
		if (_PS_VERSION_ < "1.6.0.0") {
	     	$ajax=false;
	     	if (isset($params['ajax'])) $ajax = $params['ajax'];
	
	     	if (trim($param)!='' && $param>0) {
				myOwnReservationsController::_construire($obj);
				return myOwnReservationsCalendarController::displayHome($obj);
			}
		} else if (_PS_VERSION_ >= "1.7.0.0") {
			if (trim($param)!='' && $param>0 && count($obj->_products->list)>0)
			return '<div id="blockmyownreservations" class="card card-block blockmyownreservations">'.(Configuration::get('MYOWNRES_WIDGET_HOME') <3 ? myOwnReservationsCalendarController::displayHome($obj) : (Configuration::get('MYOWNRES_WIDGET_TYPE')==2 ?  myOwnReservationsCalendarController::displayWidget($obj) : myOwnReservationsCalendarController::displayTopWidget($obj))).'</div>';
		}
	}

	
	//==============================================================================================================================================
	//     DISPLAY ORDER 
	//==============================================================================================================================================
		
	public static function displayOrderConfirmation($obj, $params) {
		if (_PS_VERSION_ < "1.7.0.0") {
			$order = $params['objOrder'];
			$total_to_pay = $params['total_to_pay'];
			$currency = $params['currencyObj'];
		} else {
			$order = $params['order'];
			$currency = new Currency($order->id_currency);
			$total_to_pay = $order->getOrdersTotalPaid();
		}
		
		$out = self::displayReservationsConfirmation($obj, $order, true);
		
		//return '<link href="'._MODULE_DIR_.$obj->name.'/css/orderConf.css" rel="stylesheet" type="text/css" media="all" />'.$out; //moved in template
		return $out;
	}
	
	public static function displayReservationsConfirmation($obj, $order, $inemail=true) {
		global $smarty, $cookie;
		$currency = new Currency($order->id_currency);
		$c_decimals = (int)$currency->decimals;
		$resas = myOwnResas::getReservations($order->id);
		$resasPrice_wt=0;
		$advancePrice_wt=0;
		$attributes=array();
		foreach($resas as $resa) {
			$resasPrice_wt+=$resa->getTotalPriceSaved(true);
			$advancePrice_wt+=$resa->getAdvancePriceSaved(true);
			$attributes[$resa->id_product]=str_ireplace("<br />", ", ", MyOwnReservationsUtils::getAttributeCombinaisons($resa->id_product, $resa->id_product_attribute, $cookie->id_lang));
		}
		$order_balance_wt = $resasPrice_wt - $advancePrice_wt;
		$purchasePrice_wt = $order->total_paid_tax_incl-$resasPrice_wt;
		$order_advance = $order->total_paid-$order_balance_wt;
		$order_total_paid=Tools::displayPrice($order->total_paid, $currency, false);
		
		$smarty->assign(array(
			'order_total_paid' => $order_total_paid,
			'order_total_paid_num' => Tools::ps_round($order_total_paid,$c_decimals),
			'order_total_paid_real' => Tools::displayPrice($order_advance, $currency, false),
			'order_total_paid_real_num' => Tools::ps_round($order_advance,$c_decimals),
			'order_total_purchases' => Tools::displayPrice($purchasePrice_wt, $currency, false),
			'resas_advance' => Tools::displayPrice($advancePrice_wt, $currency, false),
			'resas_attributes' => $attributes,
			'advance_title' => Configuration::get('MYOWNRES_MSG_ADVANCE', $cookie->id_lang),
			'reservations' => $resas
		));
		
		$link=new Link();
		if (empty($link->protocol_content))
			$link->protocol_content = Tools::getCurrentUrlProtocolPrefix();
		
		$productsIds=array();$width=1000;
		foreach (ImageType::getImagesTypes() AS $img) { 
			if ($img['width']<$width) {
				$width = $img['width'];
				$id_image_type = $img['id_image_type'];
		 	}
		}
		
		$resaExtention=array();$resaDescription=array();$globalExtentions=array();
	  	foreach($resas as $resa) {
			$productsIds[]=$resa->id_product;
			if ($resa->_mainProduct==null) $resa->_mainProduct = $obj->_products->getResProductFromProduct($resa->id_product);

	  		$resaExtention[$resa->sqlId]='';
	  		$resaDescription[$resa->sqlId]='';
	  		foreach ($obj->_extensions->list as $ext) {
	  			if (method_exists($ext, "displayReservation"))
					$resaDescription[$resa->sqlId].=$ext->displayReservation($obj, $resa);
	  		}
			foreach ($obj->_extensions->getExtByType(extension_type::NOTIFICATION) as $ext) {
				if (in_array($ext->id, $resa->_mainProduct->ids_notifs)) {
					
					if (method_exists($ext, "displayOrderConfGlobal"))
						if (!isset($globalExtentions[$ext->id])) 
							$globalExtentions[$ext->id]=$ext->displayOrderConfGlobal($obj, $order);
					else if (method_exists($ext, "displayOrderConfResa"))
							$resaExtention[$resa->sqlId].=$ext->displayOrderConfResa($obj, $resa);
				}
			}
		}
		
		$style='';
		if (!$inemail) $style=file_get_contents(MYOWNRES_PATH.'/views/css/orderConf.css');
		
		$productsImages=MyOwnReservationsUtils::getProductsImages($productsIds, $cookie->id_lang);
		if (_PS_VERSION_ >= '1.5.0.0')
        	$order_name = $order->getUniqReference();
        else $order_name = '#'.$order->id; 	
		$smarty->assign(array(
			'id_lang' => $cookie->id_lang,
			'myownreservations' => $obj,
			'myownlink' => $link,
			'productsImages' => $productsImages,
			'showExt' => $inemail,
			'module_dir' => _MODULE_DIR_,
			'myownstyle' => $style,
			'resaExtention' => $resaExtention,
			'globalExtentions' => $globalExtentions,
			'resaDescription' => $resaDescription,
			'imageTypeR' => new ImageType($id_image_type),
			'order_name' => $order_name,
			'_PS_VERSION_' => _PS_VERSION_
		));

		if (count($resas))
			return $obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/hook/orderconfirmation.tpl');
		else return '';
	}
	
	//Order details
	public static function displayOrderDetailDisplayed($obj, $order, $smarty, $cookie)
	{
		if (Configuration::get('PS_FORCE_SMARTY_2')==0) {
			$products = $smarty->getTemplateVars('products');
		} else {
			$products = $smarty->get_template_vars('products');
		}
		if (_PS_VERSION_ >= "1.7.0.0") smartyRegisterFunction($smarty, 'function', 'displayPrice', array('Tools', 'displayPriceSmarty'));
		
		if (Configuration::get('MYOWNRES_ORDER_DEPOSIT')>0 && Configuration::get('MYOWNRES_DEPOSIT_SHOW')!=1 ) {
			$depositMsg = Configuration::get('MYOWNRES_MSG_DEPOSIT', $cookie->id_lang);
			self::forceTranslation('', 'order-detail', 'Total gift-wrapping:', $depositMsg);
		}
		$totalResaPrice=0;
		$totalResaPrice_wt=0;
		$advancePrice_wt=0;
		$existingReservations = myOwnResas::getReservations($order->id);
		if(count($existingReservations)>0) {
      		$customizedDatas=Product::getAllCustomizedDatas($order->id_cart, $cookie->id_lang, true, $order);
			
      		$smarty->assign('customizedDatas',$customizedDatas);
      		$smarty->assign('hidePrice',(Configuration::get('MYOWNRES_PRICE_TYPE')==reservation_price_display::HIDE));

			//calculating price of all resa
  			$totalResaPrice=0;$totalResaPrice_wt=0;
  			foreach($existingReservations as $resa) {
  				$totalResaPrice+= $resa->getTotalPriceSaved(false);
  				$totalResaPrice_wt+= $resa->getTotalPriceSaved(true);
  				$advancePrice_wt+=$resa->getAdvancePriceSaved(true);
  			}

      		//calculating price of all products
      		$totalProductPrice=0;
      		$totalProductPrice_wt=0;
      		$filteredProducts = array();
      		$resaQty=0;$resasPrice=0;$resasPrice_wt=0;
      		if (is_array($products))
      		foreach ($products as $key => $product) {
				$resas = myOwnResas::getReservations($order->id, $product['product_id'], $product['product_attribute_id']);
				foreach($resas as $resa) {
					$resaQty+=$resa->quantity;
					$resasPrice+=$resa->getTotalPriceSaved(false);
					$resasPrice_wt+=$resa->getTotalPriceSaved(true);
				}
				//purchased products
      			if ($resaQty<$product['product_quantity']) {
	      			$totalProductPrice+=$product['product_price']*$product['product_quantity'];
	      			$totalProductPrice_wt+=$product['product_price_wt']*$product['product_quantity'];//* (1 + $product['tax_rate'] * 0.01);
	      		}
	      		if (count($resas)>0) {
		      		$product['id_customization'] = 99;
		      		$product['customizationQuantityTotal'] = $resaQty;
		      		$product['total_customization'] = $resasPrice;
		      		$product['total_customization_wt'] = $resasPrice_wt;
	      		}
			    //added to 1.5.3.1
		      	$product['customizedDatas'] = null;
				if (isset($customizedDatas[$product['product_id']][$product['product_attribute_id']]))
					$product['customizedDatas'] = $customizedDatas[$product['product_id']][$product['product_attribute_id']];
				else
					$product['customizationQuantityTotal'] = 0;
					
	      		$filteredProducts[$key] = $product;
	      	}

      		//managing advance
      		$order_balance_wt = $totalResaPrice_wt - $advancePrice_wt;
      		if ($order_balance_wt>0) {
      		
		  		$order_advance = $order->total_paid-$order_balance_wt;

      			$smarty->assign('order_total_advance',$order_advance);
      			$smarty->assign('order_total_balance',$order_balance_wt);

      			$smarty->assign('advance_label',Configuration::get('MYOWNRES_MSG_ADVANCE', $cookie->id_lang));
      			$smarty->assign('balance_label',Configuration::get('MYOWNRES_MSG_BALANCE', $cookie->id_lang));
      		} else $smarty->assign('order_total_balance',0);
      		$smarty->assign('products',$filteredProducts);
      		
        		
        	return $obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/hook/orderdetail.tpl');
		}
	}
		
	public static function displayPDFInvoice($obj, $smarty, $cookie, $params) {
		$out='';
		foreach ($params as $param)
			if (is_object($param) && get_class($param)=='OrderInvoice')
				$orderInvoice = $param;
		
		if (!isset($orderInvoice->total_advance_wt)) $orderInvoice->getProducts();

		$balance_wt = round($orderInvoice->total_resas_wt) - round($orderInvoice->total_advance_wt);
		$advance_wt = round($orderInvoice->total_advance_wt) - round($orderInvoice->total_discount_tax_incl)+round($orderInvoice->total_balance_discounts_wt);
		
		if ($balance_wt>0 && $advance_wt>0) {
			$smarty->assign(array(
				'_PS_VERSION_' => _PS_VERSION_,
				'resv_label' => Configuration::get('MYOWNRES_MSG_RESV', $cookie->id_lang),
				'balance_label' => Configuration::get('MYOWNRES_MSG_BALANCE', $cookie->id_lang),
				'advance_label' => Configuration::get('MYOWNRES_MSG_ADVANCE', $cookie->id_lang),
				'myown_advance' => Tools::displayPrice($advance_wt),
				'myown_balance' => Tools::displayPrice($balance_wt-$orderInvoice->total_balance_discounts_wt)
			));
			$out = $obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/admin/pdf.tpl');
		}

		return $out;
	}

	//==============================================================================================================================================
	//    DISPLAY BACK
	//==============================================================================================================================================
	
	public static function dashboardZoneTwo($obj, $params) {
		global $cookie, $smarty;
		$day = date('Y-m-d');
		$resas = myOwnResas::getReservationsStartingOrEnding(strtotime($day));
		$out = MyOwnReservationsUtils::displayIncludes();
		$out .= (_PS_VERSION_ < "1.5.0.0" ? '<script src="'._PS_JS_DIR_.'jquery/jquery.fancybox-1.3.4.js" type="text/javascript" charset="utf-8"></script><link rel="stylesheet" href="'._PS_CSS_DIR_.'jquery.fancybox-1.3.4.css" type="text/css" charset="utf-8">' :
		'<script src="'._PS_JS_DIR_.'jquery/plugins/fancybox/jquery.fancybox.js" type="text/javascript" charset="utf-8"></script><link rel="stylesheet" href="'._PS_JS_DIR_.'jquery/plugins/fancybox/jquery.fancybox.css" type="text/css" charset="utf-8">');
		$obj->getContext()->controller->addJqueryPlugin(array('fancybox', 'typewatch'));
		$out .= myOwnReservationsReservationsController::getScript($obj);
		$ourtheme = file_exists(_PS_BO_ALL_THEMES_DIR_.'labulle');
		$smarty->assign(array(
			'id_lang' => $cookie->id_lang,
			'myownreservations' => $obj,
			'reservations' => $resas,
			'day' => $day,
			'ourtheme' => $ourtheme
		));

		return $out.$obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/hook/dashboardZoneTwo.tpl');
			
		return 'toto';
	}
	
	public static function displayAdminCustomers($obj, $params) {
		return '<div class="form-group">
					'.myOwnUtils::getBoolField($obj, 'reinit_password', true, $obj->l('Reinit password', 'settings'), 'width:25%;font-size: 13px;font-weight: normal;color: #666;text-align:right;padding-left: 5px;padding-right: 5px;').'<div style="clear:both"></div>
				</div>
				<script type="text/javascript">
					$("#passwd").val("totototo").parent().parent().parent().hide();
					$("#reinit_password_on").change(function(e){
						$("#passwd").val("totototo").parent().parent().parent().hide();
					});
					$("#reinit_password_off").change(function(e){
						$("#passwd").val("").parent().parent().parent().show();
					});
				</script>';
	}
	
	public static function actionAdminSaveAfter($obj, $params) {
		PrestaShopLogger::addLog(date('Y-m-d H:i:s').'actionAdminSaveAfter');
		if (Tools::getIsset('submitAddcustomer') && Tools::getIsset('reinit_password') && Tools::getValue('reinit_password')) {
			$customer = new Customer(Tools::getValue('id_customer'));
			if (Validate::isLoadedObject($customer)) {
				$customer->passwd = Tools::encrypt($password = Tools::passwdGen(MIN_PASSWD_LENGTH));
				$customer->last_passwd_gen = date('Y-m-d H:i:s', time());
				if ($customer->update())
				{
					Hook::exec('actionPasswordRenew', array('customer' => $customer, 'password' => $password));
					$mail_params = array(
						'{email}' => $customer->email,
						'{lastname}' => $customer->lastname,
						'{firstname}' => $customer->firstname,
						'{passwd}' => $password
					);
					Mail::Send($obj->getContext()->language->id, 'password', Mail::l('Your new password'), $mail_params, $customer->email, $customer->firstname.' '.$customer->lastname);
				}
			}
		}
	}
	
	public static function displayBackOfficeHeader($obj, $params) {
		global $cookie;
		$postform = Tools::getValue('form');
		
		if (Tools::getIsset('form') && array_key_exists('id_product', $postform))
			self::actionProductUpdate($obj, array('id_product' => $postform['id_product']));
		$obj->getContext()->controller->addJquery();
		$obj->getContext()->controller->addJqueryPlugin(array('fancybox', 'typewatch'));
	    $submitReScheduleEnd = Tools::getIsset('submitReScheduleEnd');
		$reScheduleEnd = Tools::getValue('reScheduleEnd', '');
		$reScheduleEndTimeslot = Tools::getValue('reScheduleEndTimeslot', 0);
		
		$id_order=Tools::getValue('id_order');
		if (Tools::getIsset('submitReScheduleEndConf') && $reScheduleEnd!='') {
			$existingReservations = myOwnResas::getReservations($id_order);

			$rescheduleerror='';
			$diff_wt=0;
			$diff_ht=0;
			$total_advance_wt=0;
			$total_advance_ht=0;
			foreach($existingReservations as $resa) {
				//for reservation with multipe qties each qty can be selected alone
				$splited=false;
				
				$sel=array(1 => 1);
				if ($resa->quantity>1) {
					$selected=false;
					
					for ($i=1; $i < $resa->quantity+1; $i++) {
						$sel[$i]=Tools::getIsset('resa_'.$resa->sqlId.'_'.$i, '');
						if (Tools::getIsset('resa_'.$resa->sqlId.'_'.$i, ''))
							$selected=true;
						else $splited=true;
					}
				} else $selected = Tools::getIsset('resa_'.$resa->sqlId, '');

				if ($selected) {
					$originalresa = clone $resa;
					$oldResa = myOwnResas::extractReservationItem($obj, $resa, $sel, false, '', 0, $reScheduleEnd, $reScheduleEndTimeslot);
					$addResa = $resa->sqlId;
			 		
			 		
			 		//update order
					if ($resa->id_order>0 && isset($_POST['refreshOrder'])) {
						$diff = MyOwnReservationsUtils::updateOrderDetail($obj, $resa, $cookie->id_lang, false, intval(Tools::getValue('updateInvoice')), $oldResa, $addResa);
						$diff_wt+=$diff['diff_wt'];
						$diff_ht+=$diff['diff_ht'];
						$total_advance_wt+=$diff['total_advance_wt'];
						$total_advance_ht+=$diff['total_advance_ht'];
					}
					
					if (!MyOwnReservationsUtils::sendProdMsg($obj, $resa, strtotime($resa->startDate), $resa->getStartTimeSlot(), strtotime($resa->endDate), $resa->getEndTimeSlot(), intval($cookie->id_employee), $resa->_mainProduct, true))
			 			$rescheduleerror .= myOwnUtils::displayError($obj->l('but an error occurred while sending e-mail to', 'ajax_admin').' '.$obj->l('the product family owner', 'ajax_admin'));
			 		
			 		if (isset($_POST["notify"]) && $_POST["notify"]) {
			 			if (!MyOwnReservationsUtils::sendMsg($obj, $resa, strtotime($resa->startDate), $resa->getStartTimeSlot(), strtotime($resa->endDate), $resa->getEndTimeSlot(), intval($cookie->id_employee)))
			 				$rescheduleerror .= myOwnUtils::displayError($obj->l('but an error occurred while sending e-mail to', 'ajax_admin').' '.$obj->l('the customer', 'ajax_admin'));
			 		}
			 		if (!$splited && $resa->id_order>0) {
			 			MyOwnReservationsUtils::updateCartDetail($obj, $originalresa, 0, $resa->startDate, $resa->startTimeslot, $resa->endDate, $resa->endTimeslot);
			 		}
			 		
				}
			}
			if (isset($_POST["updateStatus"]) && $_POST["updateStatus"]) {
				$history = new OrderHistory();
				$history->id_order = (int)$id_order;
				$history->id_employee = (int)($cookie->id_employee);
				$history->changeIdOrderState($_POST["orderStatus"], (int)$id_order);
				$templateVars = array();
				$history->addWithemail(true, $templateVars);
			}
					
			if (Tools::getValue('refreshOrder')) {
				$order = new Order($id_order);
				if (_PS_VERSION_ < "1.5.0.0") {
					$query = "UPDATE `"._DB_PREFIX_."orders` SET `total_products_wt` =  '".($order->total_products_wt+$diff_wt)."', `total_products` =  '".($order->total_products+$diff_ht)."', `total_paid` =  '".($order->total_paid+$diff_wt)."' WHERE `id_order` =".$id_order.";";
				} else {
					$query = "UPDATE `"._DB_PREFIX_."orders` SET `total_products_wt` =  '".($total_advance_wt)."', `total_products` =  '".($total_advance_ht)."', `total_paid` =  '".($order->total_paid+$diff_wt)."', `total_paid_tax_incl` =  '".($order->total_paid_tax_incl+$diff_wt)."', `total_paid_tax_excl` =  '".($order->total_paid_tax_excl+$diff_ht)."' WHERE `id_order` =".$id_order.";";
			}
			Db::getInstance()->Execute($query);
			}
			if ($rescheduleerror!='')
				 $out .= self::displayAdminError($rescheduleerror);
				
			$existingReservations = myOwnResas::getReservations($id_order);
		}
	}


	// ADMIN Products Extra
	//-----------------------------------------------------------------------------------------------------------------
		
	public static function displayAdminProductsExtra($obj, $params) {
		global $cookie;
		global $rights;
		if (!MyOwnReservationsUtils::isModuleActive('myownreservations')) return false;
		if(session_id() == '') session_start();
		$out = ' ';
		$out .= MyOwnReservationsUtils::displayIncludes();
		
		$employee = new Employee($cookie->id_employee);
		$rights = Profile::getProfileAccess($employee->id_profile, Tab::getIdFromClassName('adminmyownreservations'));
		$id_product = Tools::getValue('id_product', (isset($params['id_product']) ? $params['id_product'] : 0));
		$class = 'hint';
		if (_PS_VERSION_ >= "1.6.0.0")
			$class = 'alert alert-info';
		if ($id_product>0) $mainProduct = $obj->_products->getResProductFromProduct($id_product);
		if ($id_product==0 or $mainProduct==null)
			return '
			<div class="'.$class.'" style="display:block; position:\'auto\';">
			<p>'.$obj->l('This product is not reservable, please check','hooks').' '.'
			<a href="'.myOwnReservationsController::getConfUrl('products').'">'.$obj->l('myOwnReservation Configuration','hooks').'</a></p>
			</div>';
		if (!$rights['view'])
			return '
			<div class="'.$class.'" style="display:block; position:\'auto\';">
			<p>'.$obj->l('You don\'t have view permissions on myOwnReservations tab','hooks').'</p>
			</div>';

		if (isset($_SESSION['myownresAdminMsg']) && is_array($_SESSION['myownresAdminMsg']))
			foreach ($_SESSION['myownresAdminMsg'] as $error) 
				if ($error!= array()) $out .= $error;

		$_SESSION['myownresAdminMsg'] = array();
				
		//ext
		//-----------------------------------------------------------------------------------------------------------------
    	foreach ($obj->_extensions->getExtByType(extension_type::ADM_PROD) as $ext) 
			if (method_exists($ext, "displayAdminProduct"))
				$out.=$ext->displayAdminProduct($obj, $params);
		
		$mymarketplace = $obj->_extensions->getExtByName('mymarketplace');
		if ($mymarketplace!=null && $mainProduct->reservationSelType==reservation_interval::TIMESLOT) {
			//timeslots
			//-----------------------------------------------------------------------------------------------------------------
			if (_PS_VERSION_ < "1.6.0.0") 
				$out .= '
			<h4>'.ucfirst(myOwnLang::$objects[MYOWN_OBJ::TIMESLOT]).'</h4>
			<div class="separation"></div>';
	
			if ($rights['add']) $out .= myOwnReservationsTimeslotsController::edit('product', $obj, 0, $id_product);
			$out .= myOwnReservationsTimeslotsController::show('product', $obj, $mainProduct->sqlId, $id_product, array());
		}
		
		//availabilities
		//-----------------------------------------------------------------------------------------------------------------
		if (_PS_VERSION_ < "1.6.0.0") 
			$out .= '
		<h4>'.ucfirst(myOwnLang::$objects[MYOWN_OBJ::AVAILABILITY]).'</h4>
		<div class="separation"></div>';

		
		if ($rights['add']) $out .= myOwnReservationsAvailabilitiesController::add('product', $obj, $id_product);
		$out .= myOwnReservationsAvailabilitiesController::show('product', $obj, $id_product);
		
		//deposit per product
		//-----------------------------------------------------------------------------------------------------------------
		if ($mainProduct->_depositByProduct) {
			$prod_amount = Configuration::get('MYOWNRES_RES_DEPOSIT_'.$id_product, 0);
			if (_PS_VERSION_ >= "1.6.0.0") {
				$out .= MyOwnReservationsUtils::table16wrap($obj, '', '', $obj->l('Product deposit', 'hooks'), -1);
			} else
				$out.='
			<h4>'.$obj->l('Product deposit','hooks').'</h4>
			<div class="separation"></div>';

				$out.='
				<li style="list-style-type: none;">
					<label style="text-align: left;width:150px;">'.$obj->l('Amount','hooks').' :</label>
					'.myOwnUtils::getInputField('productDeposit', Tools::getValue('productDeposit', intval($prod_amount)), '', 'width:25%;display:inline-block', 'upsInput', true).'
				</li>';
			if (_PS_VERSION_ >= "1.6.0.0")  $out .= '<div class="panel-footer">
				<a href="index.php?controller=AdminProducts&token='.Tools::getAdminToken('AdminProducts'.(int)Tab::getIdFromClassName('AdminOrders').(int)$obj->context->cookie->id_employee).'" class="btn btn-default"><i class="process-icon-cancel"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::CANCEL]).'</a>
				<button type="submit" name="submitAddproduct"  onclick="$(\'#priceSetBtn\').attr(\'checked\',\'checked\')" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::SAVE]).'</button>
				<button type="submit" name="submitAddproductAndStay"  onclick="$(\'#priceSetBtn\').attr(\'checked\',\'checked\')" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::SAVESTAY]).'</button>
			</div>
			';
			if (_PS_VERSION_ >= "1.6.0.0") $out.='</div>';
		}
		
		//length per product
		//-----------------------------------------------------------------------------------------------------------------
		if ($mainProduct->_reservationLengthControl == reservation_length::PRODUCT or $mainProduct->_reservationLengthControl == reservation_length::COMBINATION ) {
			if ($mainProduct->_reservationLengthControl == reservation_length::PRODUCT) $attributes=array(0);
			else $attributes = MyOwnReservationsUtils::getProductAttributes($id_product);
			if (_PS_VERSION_ >= "1.6.0.0") {
				$out .= MyOwnReservationsUtils::table16wrap($obj, '', '', $obj->l('Reservation options', 'hooks'), -1);
			} else
				$out.='
			<br/>
			<h4>'.$obj->l('Reservation options','hooks').'</h4>
			<div class="separation"></div>';
			foreach($attributes as $attribute) {
				if ($attribute>0) $length = Configuration::get('MYOWNRES_RES_LENGTH_'.$id_product.'-'.$attribute);
				else $length = Configuration::get('MYOWNRES_RES_LENGTH_'.$id_product);
				if (intval($length)==0) $length=intval($mainProduct->_reservationMinLength);
				if (intval($length)==0) $length=1;
				$out.='
				<li style="list-style-type: none;">
					<label style="text-align: left;width:150px;">'.($attribute>0 ? MyOwnReservationsUtils::getAttributeCombinaisons($id_product,$attribute,$cookie->id_lang) : ucfirst(myOwnLang::$object[MYOWN_OBJ::PRODUCT])).' '.$obj->l('length','hooks').' :</label>
					'.myOwnUtils::getInputField('productLength'.($attribute>0 ? '_'.$attribute : ''), Tools::getValue('productLength'.($attribute>0 ? '_'.$attribute : ''), $length), $mainProduct->getUnitLabel($obj, $cookie->id_lang, true), 'width:50px').'
					
				</li>';
			}
			if (_PS_VERSION_ >= "1.6.0.0")  $out .= '<div class="panel-footer">
				<a href="index.php?controller=AdminProducts&token='.Tools::getAdminToken('AdminProducts'.(int)Tab::getIdFromClassName('AdminOrders').(int)$obj->context->cookie->id_employee).'" class="btn btn-default"><i class="process-icon-cancel"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::CANCEL]).'</a>
				<button type="submit" name="submitAddproduct"  onclick="$(\'#priceSetBtn\').attr(\'checked\',\'checked\')" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::SAVE]).'</button>
				<button type="submit" name="submitAddproductAndStay"  onclick="$(\'#priceSetBtn\').attr(\'checked\',\'checked\')" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::SAVESTAY]).'</button>
			</div>';
			if (_PS_VERSION_ >= "1.6.0.0") $out.='</div>';
		}
		
		
		//price tab
		//-----------------------------------------------------------------------------------------------------------------
		if ($rights['edit'] 
			&& 	($mainProduct->_reservationPriceType==reservation_price::TABLE_TIME 
				or $mainProduct->_reservationPriceType==reservation_price::TABLE_DATE
				or ($mainProduct->_reservationPriceType==reservation_price::TABLE_RATE && $mainProduct->_coefPerProduct)))  {
			if (_PS_VERSION_ >= "1.6.0.0") {
				$out .= MyOwnReservationsUtils::table16wrap($obj, '', '', $obj->l('Reservation price', 'hooks'), -1);
			} else
			 $out.='
			<br/>
			<h4>'.$obj->l('Reservation price','hooks').'</h4>
			<div class="separation"></div>';
			
			$out .= myOwnReservationsPriceSetsController::edit($cookie, $obj, $id_product);
			if (_PS_VERSION_ >= "1.6.0.0") $out.='</div>';
		}
		
		
		//sync
		//-----------------------------------------------------------------------------------------------------------------
		$issync=false;
		foreach (myOwnLang::$sync_platforms AS $platform_key => $platform_label)
			if (Configuration::get('MYOWNRES_SYNC_'.$platform_key)) $issync = true;
			
		if ($issync) {
			if (_PS_VERSION_ >= "1.6.0.0") {
				$out .= MyOwnReservationsUtils::table16wrap($obj, '', '', $obj->l('Product sync', 'hooks'), -1);
			} else
				$out.='
			<h4>'.$obj->l('Product sync','hooks').'</h4>
			<div class="separation"></div>';
			$out.=myOwnUtils::displayInfo($obj->l('Indicate product reference on theses platforms', 'hooks'));
			foreach (myOwnLang::$sync_platforms AS $platform_key => $platform_label)
				if (Configuration::get('MYOWNRES_SYNC_'.$platform_key))
			$out.='
				<li style="list-style-type: none;">
					<label style="text-align: right;width:150px;">'.$platform_label.' :</label>
					<input name="sync_ref_'.$platform_key.'" class="form-control" style="    width: 150px;display: inline-block;" value="'.Tools::getValue('sync_ref_'.$platform_key, Configuration::get('MYOWNRES_SYNCREF_'.$platform_key, $id_product)).'">
					
				</li>';
				$link = new Link();
				$out .=  myOwnUtils::displayInfo($obj->l('Product reservations can be exported with this link', 'settings').':  "'.MyOwnReservationsUtils::getModuleLink($link, 'icalendar').'&id='.$id_product.'"');


			if (_PS_VERSION_ >= "1.6.0.0")  $out .= '<div class="panel-footer">
				<a href="index.php?controller=AdminProducts&token='.Tools::getAdminToken('AdminProducts'.(int)Tab::getIdFromClassName('AdminOrders').(int)$obj->context->cookie->id_employee).'" class="btn btn-default"><i class="process-icon-cancel"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::CANCEL]).'</a>
				<button type="submit" name="submitAddproduct"  onclick="$(\'#priceSetBtn\').attr(\'checked\',\'checked\')" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::SAVE]).'</button>
				<button type="submit" name="submitAddproductAndStay"  onclick="$(\'#priceSetBtn\').attr(\'checked\',\'checked\')" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::SAVESTAY]).'</button>
			</div>';
			if (_PS_VERSION_ >= "1.6.0.0") $out.='</div>';
			
			
		}
		
		
		return $out;
	}
	
	// ADMIN Order
	//-----------------------------------------------------------------------------------------------------------------

	
	public static function displayAdminOrder($obj, $id_order, $cookie) {
		global $currentIndex;
		global $cart;
		
		$out = MyOwnReservationsUtils::displayIncludes();
		$out .= (_PS_VERSION_ < "1.5.0.0" ? '<script src="'._PS_JS_DIR_.'jquery/jquery.fancybox-1.3.4.js" type="text/javascript" charset="utf-8"></script><link rel="stylesheet" href="'._PS_CSS_DIR_.'jquery.fancybox-1.3.4.css" type="text/css" charset="utf-8">' :
		'<script src="'._PS_JS_DIR_.'jquery/plugins/fancybox/jquery.fancybox.js" type="text/javascript" charset="utf-8"></script><link rel="stylesheet" href="'._PS_JS_DIR_.'jquery/plugins/fancybox/jquery.fancybox.css" type="text/css" charset="utf-8">');
		$obj->getContext()->controller->addJqueryPlugin(array('fancybox', 'typewatch'));
		$out .= myOwnReservationsReservationsController::getScript($obj);
		$script = "";
		$prePS15=(_PS_VERSION_ < "1.5.0.0");
		$prePS16=(_PS_VERSION_ < "1.6.0.0");
		$existingReservations = myOwnResas::getReservations($id_order);
		$order = new Order($id_order);
		$orderStatuss = Db::getInstance()->ExecuteS("SELECT * FROM  `" . _DB_PREFIX_ . "order_history` WHERE  `id_order` =".$id_order." ORDER BY date_add DESC LIMIT 1;");
		$orderDetails = $order->getProducts();

		$orderStatus = $order->getCurrentState();
		$orderCurrency = new Currency($order->id_currency);
		$display_price_wt = ($order->getTaxCalculationMethod() != PS_TAX_EXC);
		$productPrices=array();
		
		//for reschedule end
		$addResa=0;
		$orderResaFamily = -1;
		$submitReScheduleEnd = Tools::getIsset('submitReScheduleEnd');
		$reScheduleEnd = Tools::getValue('reScheduleEnd', '');
		$reScheduleEndTimeslot = Tools::getValue('reScheduleEndTimeslot', 0);
		
		//set each resa price
		//----------------------------
		myOwnReservationsController::_construire($obj);
		$totalProductPrice=0;
		$totalProductPrice_wt=0;
		$totalProductCount=0;
		$totalResaPrice=0;$totalResaPrice_wt=0;
		$tabOffset=0;
		if ($order->hasBeenPaid()) $tabOffset++;
		if ($order->hasBeenDelivered()) $tabOffset++;
		if ((int)Configuration::get('PS_STOCK_MANAGEMENT')) $tabOffset++;
		
		//set customoization resa price and action
		//----------------------------
		foreach($existingReservations as $resa) {
			$totalResaPrice+= $resa->getTotalPriceSaved(false);
  			$totalResaPrice_wt+= $resa->getTotalPriceSaved(true);
  			$resa->_mainProduct = $obj->_products->getResProductFromProduct($resa->id_product);
  			if ($orderResaFamily==-1) $orderResaFamily=$resa->_mainProduct->sqlId;
  			if ($orderResaFamily!=-1 && $resa->_mainProduct->sqlId!=$orderResaFamily) $orderResaFamily=0; 
			$price=Tools::displayPrice($resa->getTotalPriceSaved($display_price_wt), $orderCurrency);
			$onclick='showResaBox('.$resa->sqlId.');showResaDetails('.$resa->sqlId.');';
			if ($prePS16) $script .= '
			var editButton = \'<a onclick="'.$onclick.'" class="edit_product_change_link"><img src="../img/admin/edit.gif"></a>\';';
			else $script .= '
			var editButton = \'<a onclick="'.$onclick.'" class="btn btn-default"><i class="icon-calendar-o"></i> '.ucfirst(myOwnLang::$object[MYOWN_OBJ::RESV]).'</a>\';';
			$script .= '
			var editCheckbox = \'<input type="checkbox">\';';
			
			//changing edit reservation on PS1.4
			if (_PS_VERSION_ < "1.5.0.0") $script .= '
				$("#resa_'.$resa->sqlId.'").closest("tr").find("td:eq('.(4+$tabOffset).')").html("'.$price.'");
				$("#resa_'.$resa->sqlId.'").closest("tr").find(".cancelCheck").html(editButton);';
			//changing edit reservation on PS1.5
			else if (_PS_VERSION_ < "1.7.0.0") {
				$script .= '
				//$("#resa_'.$resa->sqlId.'").closest("tr").find(".total_product").html("'.$price.'");';
				if (!$order->hasBeenDelivered()) $script .= '
				$("#resa_'.$resa->sqlId.'").closest("tr").find(".product_action").html(editButton);';
				else $script .= '
				$("#resa_'.$resa->sqlId.'").closest("tr").find("td:eq(1)").html(editButton);';
			} else {
				if (Tools::getValue('reScheduleEnd', '')!='') $checked = Tools::getIsset('resa_'.$this->sqlId, ''); 
				else $checked=true;
				$ext_qty='';
				foreach ($obj->_extensions->getExtByType(extension_type::SEL_QTY) as $ext)
					if (method_exists($ext, "displayQtyField"))
						$ext_qty = $ext->displayQtyField($obj, $resa->_mainProduct, $this);
				if ($ext_qty=='') {
					if ($resa->quantity > 1) {
						$opt_head='';
						for ($i=1; $i<($resa->quantity+1); $i++) {
							$opt_head .= '<input type="checkbox" name="resa_'.$resa->sqlId.'_'.$i.'" value="1" class="check-out-checkbox" '.(Tools::getValue('resa_'.$resa->sqlId.'_'.$i, !Tools::getIsset("reScheduleEnd")) ? 'checked="checked' : '').'" product="'.$resa->id_product.'" style="display:none"><span>#'.$i.' </span>'.($i!=$resa->quantity ? '<br/>' : '').'';
						}
						$script .= '
						$("tr.customized td:contains(\'#'.$resa->sqlId.'\')").closest("tr").find("td:eq(0)").append(\''.$opt_head.'\');';
					} else {
						$resv_title = '<input type="checkbox" name="resa_'.$resa->sqlId.'" value="1" class="check-out-checkbox" '.($checked ? 'checked="checked' : '').'" product="'.$resa->id_product.'" style="display:none"> ';
						$script .= '
						$("tr.customized td:contains(\'#'.$resa->sqlId.'\')").closest("tr").find("td:eq(0) div.form-group span").prepend(\''.$resv_title.'\');';
					}
				}
				$script .= '
				$("tr.customized td:contains(\'#'.$resa->sqlId.'\')").closest("tr").find("td.product_action").html(editButton);';
			}
			if ($obj->isProVersion && $resa->_mainProduct->productType==product_type::STOCK) {
						$stockassigned = explode(";",$resa->stock);
						$stockitems=$obj->_stocks->getStockForProduct($resa->id_product, ($resa->_mainProduct!=null && $resa->_mainProduct->_optionIgnoreAttrStock ? 0 : $resa->id_product_attribute));
						if ($resa->stock!='')
							$script .= '$("#myownr_attendees_'.$resa->sqlId.'").find("thead th:last").after(\'<th style="font-size:10px"><b>Stock</b></th>\');';

						for ($i=1; $i<($resa->quantity+1); $i++) {
							$stockitemval='';
							foreach($stockitems as $stockitem) {
								if (count($stockassigned)>=$i && trim($stockassigned[($i-1)])==trim($stockitem->serial))
									$stockitemval=trim($stockitem->serial);
							}
							if ($resa->quantity==1 && $stockitemval!='') {
								$script .= 'if (!$("#myownr_attendees_'.$resa->sqlId.'").length) $("#resa_'.$resa->sqlId.'").closest("div").find("div").append("'.$obj->l('Serial N°', 'hooks').' '.$stockitemval.'");';
							} else {
								if ($stockitemval!='') $script .= '$("input[name=\'resa_'.$resa->sqlId.'_'.$i.'\']").next().append("('.$stockitemval.')");';
							}
							if ($resa->stock!='')
								$script .= '$("#myownr_attendee_'.$resa->sqlId.'_'.$i.'").find("td:last").after("<td>'.$stockitemval.'</td>");';
						}
			}
			$key=$resa->id_product.'-'.$resa->id_product_attribute;
			if (array_key_exists($key, $productPrices))
				$productPrices[$key]+=$price;
			else $productPrices[$key]=$price;
		}
		
		//set product total resa price
		//----------------------------
		foreach($productPrices as $key => $price) {
			$keyTab=explode('-', $key);
			$resas = myOwnResas::getReservations($id_order, $keyTab[0], $keyTab[1]);
			foreach($resas as $resa) {
				$resaCustomizeKey = ($resa->id_customization>0 ? $resa->id_customization : '-'.$resa->sqlId);
				//set price unit
				$resa->_mainProduct = $obj->_products->getResProductFromProduct($resa->id_product);
				if (_PS_VERSION_ >= "1.6.0.0") {
					$unitprice = MyOwnReservationsUtils::getProductPriceTaxes($resa->id_product, $resa->id_product_attribute, $display_price_wt, $sell=false, $resa->quantity);
					$script .= '$(\'input[name="product_quantity['.$resaCustomizeKey.']"]\').parent().parent().parent().prev("tr").find("td:eq(2)").html("'.Tools::displayPrice($unitprice, $orderCurrency).' /'.($resa->_mainProduct!=null ? $resa->_mainProduct->getUnitLabel($obj, $cookie->id_lang) : '').'");';
				} elseif ($prePS15) $script .= '$("#cancelQuantity_'.$resaCustomizeKey.'").parent().parent().prev("tr").find("td:eq(2)").append(" /'.($resa->_mainProduct!=null ? $resa->_mainProduct->getUnitLabel($obj, $cookie->id_lang) : '').'");';
				else $script .= '$(\'input[name="product_quantity['.$resaCustomizeKey.']"]\').parent().parent().parent().prev("tr").find("td:eq(2)").append(" /'.($resa->_mainProduct!=null ? $resa->_mainProduct->getUnitLabel($obj, $cookie->id_lang) : '').'");';
				//set total price
				$edited = Tools::getIsset('resa_'.$resa->sqlId, '') && $submitReScheduleEnd;
				if ($prePS15) $script .= '$("#cancelQuantity_'.$resaCustomizeKey.'").parent().parent().prev("tr").find("td:eq('.(5+$tabOffset).')").html("'.Tools::displayPrice($price, $orderCurrency).'");';
				else $script .= '$(\'input[name="product_quantity['.$resaCustomizeKey.']"]\').parent().parent().parent().prev("tr").find("td:eq('.(5+$tabOffset).')").html("'.($edited ? '<s>' :'').Tools::displayPrice($price, $orderCurrency).($edited ? '</s>' :'').'");';
				break;
			}
		}
		
		$hidePrice = (Configuration::get('MYOWNRES_PRICE_TYPE')==reservation_price_display::HIDE);
		
		//set price and warning by order detail
		//----------------------------
		if (count($existingReservations)>0) {
			$resaUnavailable=false;
			$resaUnvalidated=false;
			$resaUnavailableStr="";
			foreach($orderDetails as $orderDetail) {
				$tempReservations = myOwnResas::getReservations($id_order, $orderDetail['product_id'], $orderDetail['product_attribute_id']);
				if (count($tempReservations)>0) {
					$script .= '
					$(\'.customized-'.$orderDetail["id_order_detail"].'\').first().find(".product_action").html("");';
					foreach($tempReservations as $resa) {
						$resa->_mainProduct = $obj->_products->getResProductFromProduct($resa->id_product);
						if ($resa->_mainProduct!=null && !$resa->validated && $resa->isAvailable($cart, null, $obj->_stocks)<1) {
							$resaUnavailable=true;
							if (in_array($orderStatus, explode(',',$resa->_mainProduct->_optionValidationStatus))) $resaUnvalidated=true;
							$resaUnavailableStr.="- ".$orderDetail['product_name']." ".$resa->toString($obj, $order->id_lang, false)."";
						}
					}
				} else {
					$totalProductPrice_wt+=(($orderDetail['product_price']*(1 + ($orderDetail['tax_rate']/100))) + ($orderDetail['ecotax'] * (1 + $orderDetail['ecotax_tax_rate']/100)) )*$orderDetail['product_quantity'];
					$totalProductPrice+=$orderDetail['product_price']*$orderDetail['product_quantity'];
					$totalProductCount+=1;
				}
			}
			
			//display warning if resa unavailable
			//----------------------------
			if ($resaUnavailable) {
				$btn = '<input style="float:right" type="submit" name="cancelReservation" value="'.$obj->l('Notify unavailable reservations', 'hooks').'" class="button" style="margin-top:8px;" onclick="$(\'#txt_msg\').html(\''.stripslashes(myOwnLang::getMessage('NOTAVAILABLE', $order->id_lang)).": ".addslashes($resaUnavailableStr).'\');$.scrollTo(\'#message_m\');$.scrollTo(\'#message\');$(\'#message\').slideDown();$(\'#message_m\').slideDown();return false">';
			}
			if ($resaUnvalidated) $out .= self::displayAdminError($obj->l('Some reservations are no more available and were not validated', 'hooks').$btn);
			else if ($resaUnavailable) $out .= self::displayAdminWarn($obj->l('Some reservations are no more available', 'hooks').$btn);
				
			//display total prices
			//----------------------------
		$out .= MyOwnReservationsUtils::insertTimeslotPickerScript($obj, 'reSchedule', $orderResaFamily, $obj->l('All', 'availabilities'));
		$args = '';
		//$out .=  str_ireplace('datepicker', 'datetimepicker', myOwnUtils::insertDatepicker($obj, 'reScheduleEnd', false, $args));
			$out .= '
		<div id="panel-check-out">
			<div class="panel panel-check-out" style="display: none;">
				<div class="panel-heading">
					<i class="icon-check-square"></i> '.$obj->l('Check Out', 'hooks').' <span class="badge" id="check-out-count">'.count($existingReservations).'</span>
				</div>
				<div class="current-edit" id="check_out_form" style="display: block;">
					<div class="form-horizontal well">';
					if ($submitReScheduleEnd) {
						$out .= 
						'<input type="hidden" name="reScheduleEnd" value="'.Tools::getValue('reScheduleEnd', '').'">
						<input type="hidden" name="reScheduleEndTimeslot" value="'.Tools::getValue('reScheduleEndTimeslot', 0).'">
						<div class="form-group" style="margin-bottom:2px">
							'.myOwnUtils::getBoolField($obj, 'updateStatus', false, $obj->l('Change order status', 'hooks'), 'width:40%').'
						</div>
						<div class="form-group" style="margin-bottom:2px">
							<label style="width:40%"></label>
							<select style="font-size:10px;display:inline-block;width:40%" id="orderStatus" name="orderStatus">';
						foreach(myOwnUtils::getStatusList($cookie->id_lang) AS $orderStatus) {
							$out .= '<option style="font-size:10px;" value="' . $orderStatus['id_order_state']. '">' . $orderStatus['name'] . '</option>';
						}
						$out .= '
							</select>
						</div>
						';
							
						$out .= myOwnUtils::getBoolField($obj, 'refreshOrder', true, $obj->l('Update order', 'hooks'), 'width:40%');
						$out .= '<div style="clear:both"></div>';
						if ($order!=null) {
							$docs = $order->getDocuments();
							$invoices=array();
							foreach ($docs as $dockey=>$doc)
								if (get_class($doc)=='OrderInvoice')
									$invoices[$doc->id] = $doc->date_add;
							if (count($invoices)) {
								$out .= '
								<div class="form-group" style="margin-bottom:2px">
									'.myOwnUtils::getBoolField($obj, 'refreshInvoice', true, $obj->l('Refresh invoice', 'hooks'), 'width:40%').'
								</div>
								<div class="form-group" style="margin-bottom:2px">
									<label style="width:40%"></label>
									<select style="font-size:10px;width:100px;display:inline-block" id="invoiceDoc" name="invoiceDoc">';
									foreach($invoices as $invoiceKey => $invoice) {
										$out .= '<option style="font-size:10px;" value="' . $invoiceKey. '">#' . sprintf('%06d', $invoiceKey) . '</option>';
									}
									$out .= '
									</select>
								</div>
							';
							}
						}
	
						$out .= myOwnUtils::getBoolField($obj, 'notify', true, $obj->l('Notify customer', 'hooks'), 'width:40%');
						$out .= '<div style="height:5px;clear:both"></div>';
						//$out .= myOwnUtils::getBoolField($obj, 'cart_reschedule', true, $obj->l('Also reschedule the cart', 'hooks'), 'width:40%');
						
						$out .= '
						<div class="row">
							<div class="col-lg-9 col-lg-offset-3" style="text-align:right">
								<button class="btn btn-default" type="button" id="cancel_check_out">
									<i class="icon-remove text-danger"></i>
									'.ucfirst(myOwnLang::$actions[MYOWN_OPE::CANCEL]).'
								</button>
								<button class="btn btn-default" type="submit" name="submitReScheduleEndConf">
									<i class="icon-ok text-success"></i>
									'.$obj->l('Checkout', 'hooks').'
								</button>
							</div>
						</div>';
						
					} else $out .= '
					
						<div class="form-group">
							<label class="control-label col-lg-3">
								'.$obj->l('Reschedule End', 'hooks').'
							</label>
							'.str_ireplace('datepicker', '', MyOwnReservationsUtils::getDateField('reScheduleEnd', date('Y-m-d'), '', 'float:left', '')).'
							<div style="float:right">
								<select class="upsInput" id="reScheduleEndTimeslot" name="reScheduleEndTimeslot" style="float:left;width:120px;display:none"></select>
							</div>
						</div>
					
						
						<div class="row">
							<div class="col-lg-9 col-lg-offset-3">
								<button class="btn btn-default" type="button" id="cancel_check_out">
									<i class="icon-remove text-danger"></i>
									'.ucfirst(myOwnLang::$actions[MYOWN_OPE::CANCEL]).'
								</button>
								<button class="btn btn-default" type="submit" name="submitReScheduleEnd" onclick="$(this).parents(\'form\').attr(\'action\', $(this).parents(\'form\').attr(\'action\')+\'#orderProductsPanel\')">
									<i class="icon-ok text-success"></i>
									'.$obj->l('Reschedule', 'hooks').'
								</button>
							</div>
						</div>
					';
				$out .= '
					</div>
				</div>
			</div>
		</div>
			<script type="text/javascript">
				var baseDir = "'.__PS_BASE_URI__.'";
				var imgDir = "'._PS_IMG_.'";
				
				$(document).ready(function() {
					'.$script.'
					var resaAdvance="";';
				
				//managing advance
	      		$order_total = $totalProductPrice_wt + $totalResaPrice_wt + $order->total_shipping_tax_incl + $order->total_wrapping_tax_incl - $order->total_discounts_tax_incl;
	      		if ($order->total_products_wt < ($totalProductPrice_wt + $totalResaPrice_wt)) { //$order->total_paid_real>0
	      			$balance_wt = ($totalProductPrice_wt + $totalResaPrice_wt) - $order->total_products_wt;//$order_total - $order->total_paid_real;
	      			$advance_wt = $order->total_paid-$balance_wt; //$order->total_paid_real;
	      			
	      			$out .= '
	      			resaAdvance="<tr><td width=\"150px;\">'.Configuration::get('MYOWNRES_MSG_ADVANCE', $cookie->id_lang).'</td><td align=\"right\">'.Tools::displayPrice($advance_wt, $orderCurrency).'</td></tr>";
	      			resaAdvance+="<tr><td width=\"150px;\">'.Configuration::get('MYOWNRES_MSG_BALANCE', $cookie->id_lang).'</td><td align=\"right\">'.Tools::displayPrice($balance_wt, $orderCurrency).'</td></tr>";';
	      		}
	      		
	      		//display subtotals
	      		$out .= '
	      			var resaSum="<tr><td width=\"150px;\">'.ucfirst(myOwnLang::$objects[MYOWN_OBJ::RESV]).'</td><td align=\"right\">'.Tools::displayPrice(($display_price_wt ? $totalResaPrice_wt : $totalResaPrice), $orderCurrency).'</td></tr>";
	      			var prodSum="<tr><td width=\"150px;\">'.$obj->l('Purchases', 'hooks').'</td><td align=\"right\">'.Tools::displayPrice(($display_price_wt ? $totalProductPrice_wt : $display_price), $orderCurrency).'</td></tr>";';
	      			
				if ($prePS15)
					$out .= '
						$(".table:eq(1) tr:eq(1) td:eq(0)").html("'.Configuration::get('MYOWNRES_MSG_DEPOSIT', $cookie->id_lang).'");
						$(".table:eq(1) tr:eq(0)").remove();
						$(".table:eq(1) tr:eq(2)").after(resaAdvance);
						$(".table:eq(1) tr:eq(0)").before(prodSum);
						$(".table:eq(1) tr:eq(0)").before(resaSum);';
				else {
					$out .= '
						$("#total_wrapping td:eq(0)").html("<b>'.Configuration::get('MYOWNRES_MSG_DEPOSIT', $cookie->id_lang).'</b>");
						$("#total_products").remove();
						if (resaAdvance!="") $("#total_order .amount").html("'.Tools::displayPrice($order_total, $orderCurrency).'");
						var sum = $("#total_shipping").parent().html();
						$("#total_shipping").parent().html(resaSum+prodSum+sum+resaAdvance);
						$(".order_action").append(\'<button type="button" id="check_out" class="btn btn-default"><i class="icon-check-square"></i> '.$obj->l('Check Out', 'hooks').'</button>\');
						var temp = $("#panel-check-out").html();
						//$(temp).find("#panel-check-out").addClass("panel-check-out");
						$(".panel-vouchers").after(temp);
						$("#orderProducts").parent().parent().attr("id", "orderProductsPanel");
						if ($(".myownr_attendees").length) {
							$(".myownr_attendees").each(function(e) {
								var attendess_tab = $(this).clone();
								$(attendess_tab).css("margin-left", "15px");
								var  attendess_cont = $(this).parent().parent();
								$(this).remove();
								attendess_cont.append(attendess_tab);
							});
							
							$(".check-out-checkbox").each(function(e) {
								var tmp_name = $(this).attr("name");
								tmp_name = tmp_name.replace("resa_", "myownr_attendee_");
								$("#"+tmp_name).find("td:first").prepend($(this));
							});
							$(".panel-checkbox").remove();
							
						}
						$("#panel-check-out").remove();
						$("#check_out").unbind("click").click(function(e) {
							$(".order_action").hide();
							$(".panel-check-out").show();
							$(".check-out-checkbox").show();
							$(".panel-checkbox").show();
							e.preventDefault();
						});
						$("#cancel_check_out").unbind("click").click(function(e) {
							$(".panel-check-out").hide();
							$(".order_action").show();
							$(".check-out-checkbox").hide();
							$(".panel-checkbox").hide();
							e.preventDefault();
						});
						$(".check-out-checkbox").unbind("click").click(function(e) {
							var res_cnt = parseInt($("#check-out-count").text());
							if ($(this).parent().hasClass("panel-checkbox") || $(this).parent().is( "td" ) ) {
								if ($(this).attr("checked")) {
									if ($(this).parent().parent().parent().find(".check-out-checkbox:checked").length==1) res_cnt++;
								} else {
									if ($(this).parent().parent().parent().find(".check-out-checkbox:checked").length==0) res_cnt--;
								}
							} else {
								if ($(this).attr("checked")=="checked") res_cnt++;
								else res_cnt--;
							}
							$("#check-out-count").html(res_cnt);
						});
						$("#reScheduleEnd").datepicker({
								prevText:"",
								nextText:"",
								pickTime:false,
								disabledHours:true,
								format:"yy-mm-dd",
								dateFormat:"yy-mm-dd"
						});
						$("#reScheduleEnd").change(function(e){
							var family=-1;
							$(".check-out-checkbox:checked").each(function() {
								if (family==-1) family=productFamilly[$(this).attr("product")];
								if (family!=-1 && family!=productFamilly[$(this).attr("product")]) family=0;
							});
							populateTimeslot("end", family);
						});
						$("#orderStatus_on").change(function(e){
							$("#orderStatus").parent().show();
						});
						$("#orderStatus_off").change(function(e){
							$("#orderStatus").parent().hide();
						});
						';
						if ($submitReScheduleEnd) {
							$out .= '
							$(".order_action").hide();
							$(".panel-check-out").show();
							$(".check-out-checkbox").show();';
							foreach($existingReservations as $resa) {
								if ($resa->quantity==1) {
									$selected = Tools::getIsset('resa_'.$resa->sqlId, '');
									
								} else {
									$selected=0;
									for ($i=1; $i < $resa->quantity+1; $i++) {
										//$out .= '//toto resa_'.$resa->sqlId.'_'.$i.':'.Tools::getIsset('resa_'.$resa->sqlId.'_'.$i, '');
										if (Tools::getIsset('resa_'.$resa->sqlId.'_'.$i, '')) {
											$selected++;
											$out.='
											$("#myownr_attendee_'.$resa->sqlId.'_'.$i.'").children().css("background-color","#F4C179");';
										}
										else {
											$out.='
											$("#myownr_attendee_'.$resa->sqlId.'_'.$i.'").children().css("color","#DDD");';
										}
									}
									
								}
								if ($selected) {
										$rescheduled_resa = clone $resa;
										$rescheduled_resa->quantity=$selected;
										$rescheduled_resa->endDate=$reScheduleEnd;
										if ($reScheduleEndTimeslot)
											$rescheduled_resa->endTimeslot = $reScheduleEndTimeslot;
										$rescheduled_resa->setPrice($obj->_pricerules->list);
										$out .= '
										$("#resa_'.$resa->sqlId.'").closest("tr").find(".product_action").html(\'<input type="text" name="resa_'.$resa->sqlId.'_price" style="width:80%;display: inline-block;" class="edit_product_price_tax_excl edit_product_price" value="'.Tools::ps_round($rescheduled_resa->getPriceTaxes($display_price_wt),_PS_PRICE_DISPLAY_PRECISION_).'"> '.myOwnUtils::getCurrency().'\');
										$("#resa_'.$resa->sqlId.'").closest("tr").find(".product_quantity_show").html(\''.$selected.'\');
										$("#resa_'.$resa->sqlId.'").closest("tr").children().css("background-color","#FFF3D7");
										$("#resa_'.$resa->sqlId.'").closest("tr").prev("tr").find(".total_product").css("text-decoration", "line-through");';
									}
							}
						}
				}
			$out .= '
				});
			</script>';
		}
		return $out;
	}
	
	public static function displayAdminError($msg) {
		if (_PS_VERSION_ >= "1.5.0.0") $sel='$(".pageTitle, #ajax_confirmation")';
		else $sel='$(".path_bar")';
		return '<script type="text/javascript">
				$(document).ready(function() {
					'.$sel.'.after(\''.MyOwnUtils::displayError(addslashes($msg)).'\');
				});
			</script>';
	}
	
	public static function displayAdminWarn($msg) {
		if (_PS_VERSION_ >= "1.5.0.0") $sel='$(".pageTitle, #ajax_confirmation")';
		else $sel='$(".path_bar")';
		return '<script type="text/javascript">
				$(document).ready(function() {
					'.$sel.'.after(\''.MyOwnUtils::displayWarn(addslashes($msg)).'\');
				});
			</script>';
	}

	public static function displayAdminConf($msg) {
		if (_PS_VERSION_ >= "1.5.0.0") $sel='$(".pageTitle, #ajax_confirmation")'; //$(".pageTitle").parent()
		else $sel='$(".path_bar")';
		return '<script type="text/javascript">
				$(document).ready(function() {
					'.$sel.'.after(\''.MyOwnUtils::displayConf(addslashes($msg)).'\');
				});
			</script>';
	}

	//==============================================================================================================================================
	//    EXEC FRONT
	//==============================================================================================================================================
	
    public static function execShoppingCartExtra($obj, $smarty, $cookie, $params)
	{
		global $cart;
		require_once(_PS_ROOT_DIR_.'/config/smarty.config.inc.php');

		if ($cart->resaCarts == null) $cart->getMyOwnReservationCart();
		if ($cart->resaCarts->suppl_wt == 0) $products = $cart->getProducts(true);
		
		if (_PS_VERSION_ >= "1.7.0.0") return '';
		
		//1.5 hide discounted price on summary
		if (Configuration::get('PS_FORCE_SMARTY_2')==0) {
			$products = $smarty->getTemplateVars('products');
		} else {
			$products = $smarty->get_template_vars('products');
		}
		
		if (is_array($products))
			foreach ($products as &$product)
				if (array_key_exists('is_reservation', $product) && $product['is_reservation'] && array_key_exists('is_discounted', $product) && !$product['is_discounted']) {
					$product['price_without_specific_price']=false;
					$product['customizationQuantityTotal']=$product['cart_quantity'];
				}
		$smarty->assign('products', $products);
		
		$resv_title=Configuration::get('MYOWNRES_MSG_RESV', $cookie->id_lang);
		$smarty->assign('resv_title',$resv_title);
		$smarty->assign('balance_title',Configuration::get('MYOWNRES_MSG_BALANCE', $cookie->id_lang));
		$smarty->assign('advance_title',Configuration::get('MYOWNRES_MSG_ADVANCE', $cookie->id_lang));
		$smarty->assign('deposit_title',Configuration::get('MYOWNRES_MSG_DEPOSIT', $cookie->id_lang));
		
		$smarty->assign('show_deposit',(Configuration::get('MYOWNRES_DEPOSIT_SHOW')==1 ? $cart->getMyOwnReservationCart()->deposit : 0));
		if (is_array($params) && isset($params['summary'])) {
			$smarty->assign('myowncart',$params['summary']);
		} else {
			$summary = $cart->getSummaryDetails();
			$smarty->assign('myowncart',$summary);
		}

		$smarty->assign('hidePrice',(Configuration::get('MYOWNRES_PRICE_TYPE')==reservation_price_display::HIDE));
		$smarty->assign('totalResaTaxInc',(Configuration::get('MYOWNRES_TOTAL_TAXINC')==1));
		$smarty->assign('prePS15',(_PS_VERSION_ < "1.5.0.0"));
		$smarty->assign('prePS16',(_PS_VERSION_ < "1.6.0.0"));
		$step=Tools::getValue('step');
		$isAjaxLoaded = Tools::getValue('ajax');
		$smarty->assign('currentStep',intval($step));
		$smarty->assign('isAjaxLoaded',$isAjaxLoaded);
		$stdCheckout=(Configuration::get('PS_ORDER_PROCESS_TYPE')==0);
		$smarty->assign('stdCheckout',$stdCheckout);
		
		$fee_details=$cart->getMyOwnReservationCart()->fee_details;
		$fee_titles=array();
		foreach ($obj->_extensions->getExtByType(extension_type::GLOBFEE) as $ext) 
			$fee_titles[get_class($ext)]=$ext->feeTitle;

		$smarty->assign('is_stock_management', Configuration::get('PS_STOCK_MANAGEMENT'));
		$smarty->assign('fee_details', $fee_details);
		$smarty->assign('fee_titles', $fee_titles);
		$smarty->assign('myOwnCartList', Tools::jsonEncode($cart->getMyOwnReservationCart()->getCartResasPrice()));

		return $obj->display(dirname(__FILE__).'/'.$obj->name.'.php','views/templates/hook/script_shoppingcart.tpl');
	}

	// Account
	//-----------------------------------------------------------------------------------------------------------------
		
	public static function execDisplayAccount($obj, $params)
    {
    	$out='';
    	foreach ($obj->_extensions->getExtByType(extension_type::ACCOUNT) as $ext) 
			if (method_exists($ext, "execDisplayAccount"))
				$out.=$ext->execDisplayAccount($obj, $params);
		$link = new Link();
		$resa_link = $link->getPageLink('module-myownreservations-reservations', true);		
		if (_PS_VERSION_ >= '1.7.0.0') {
            $out .= '<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="myownreservations-link" href="'.$resa_link.'?admin=reservations" title="'.$obj->l('My Reservations', 'hooks').'"><span class="link-item"><i class="material-icons">schedule</i><span>'.$obj->l('My Reservations', 'hooks').'</span></span></a>';
        } else if (_PS_VERSION_ >= '1.6.0.0') {
            $out .= '<li><a href="'.$resa_link.'?admin=reservations" title="'.$obj->l('My Reservations', 'hooks').'"><i class="icon-calendar"></i><span>'.$obj->l('My Reservations', 'hooks').'</span></a></li>';
        } else {
            $out .= '<li><a href="'.$resa_link.'?admin=reservations" title="Informations"><img src="'._MODULE_DIR_.$obj->name.'/views/img/marker-maps.png" alt="'.$obj->l('My Reservations', 'hooks').'" class="icon"> '.$obj->l('My Reservations', 'hooks').'</a></li>';
        }
        
        return $out;
    }
	
	public static function execCreateAccount($obj, $cookie, $params)
	{
		global $smarty, $cart;
		if (Configuration::get('MYOWNRES_IGNORE_STEP2')==1 && Configuration::get('PS_CARRIER_DEFAULT')>0) {
			$cart->id_carrier = (int)Configuration::get('PS_CARRIER_DEFAULT');
			if (_PS_VERSION_ >= "1.5.0.0") $obj->getContext()->cart->id_carrier = (int)Configuration::get('PS_CARRIER_DEFAULT');
			$cart->save();
		}
	}
    
	//Order validation
	//-----------------------------------------------------------------------------------------------------------------
	public static function execPostUpdateOrderStatus($obj, $orderID, $newOrderStatus)
	{
		global $cookie;
		$currentOrderStatusID = $newOrderStatus->id;
		$existingReservations = myOwnResas::getReservations($orderID);
		$total_reservations = 0;
		$id_order_states = Db::getInstance()->executeS('
		SELECT `id_order_state`
		FROM `'._DB_PREFIX_.'order_history`
		WHERE `id_order` = '.(int)($orderID).'
		ORDER BY `date_add` DESC');
		
		
		if(count($existingReservations)>0) {

			$_products = $obj->_products;//new myOwnProductsRules();
			$_stocks = $obj->_stocks;//new myOwnStocks();
			foreach ($existingReservations as &$resa) {
				$total_reservations+=$resa->getTotalPriceSaved(true);
				$resa->_mainProduct = $_products->getResProductFromProduct($resa->id_product);
				//Validate delivery on resv status
				if (in_array($currentOrderStatusID, explode(',',$resa->_mainProduct->_optionValidationStatus))) {
					if (!$resa->validated) {
						if ($resa->_mainProduct != null && $resa->isAvailable(null, null, $_stocks)>0)
							$resa->validate($obj, 1);
					}
				//Unvalidate delivery on different resv status
	  			} else {
	  				$resa->validate($obj, 0);
	  			}
  			}

  			//exec extenssion action
			foreach ($obj->_extensions->getExtByType(extension_type::NOTIFICATION) as $ext) 
				if (method_exists($ext, "postUpdateOrderStatus"))
					$ext->postUpdateOrderStatus($obj, $orderID, $newOrderStatus, $existingReservations);


			$alreadyValidated=false;
			$newValidated=false;
			foreach ($existingReservations as $resa) 
				foreach ($id_order_states as $id_order_state)
					if (in_array($id_order_state['id_order_state'], explode(',',$resa->_mainProduct->_optionValidationStatus))) {
						$alreadyValidated=true;
						break 2;
					}
			foreach ($existingReservations as $resa)
				if (in_array($currentOrderStatusID, explode(',',$resa->_mainProduct->_optionValidationStatus))) {
					$newValidated=true;
					break;
				}
					
			//PrestaShopLogger::addLog('alreadyValidated '.$alreadyValidated.', newValidated '.$newValidated.', status'.$id_order_states[0]['id_order_state'].'->'.$currentOrderStatusID);
			//Add extenssion attachements	

			if (!$alreadyValidated && $newValidated) {
				$attachements=array();
				$order = new Order($orderID);
				
				if ($resa->_mainProduct->productType==product_type::STOCK && $resa->_mainProduct->_occupyStockAssign==stock_assignation::AUTO) {
	 				$stockitems=$obj->_stocks->getStockForProduct($resa->id_product, ($resa->_mainProduct->_optionIgnoreAttrStock ? 0 : $resa->id_product_attribute));
	 				$qty=0;$stockitemassign='';

	 				foreach ($stockitems as $stockitem) {
	 					$scount = myOwnResas::getFlatResaLinesCount(null, null, $resa->_mainProduct, $resa->id_product, $resa->id_product_attribute, strtotime($resa->startDate),  strtotime($resa->endDate), $resa->startTimeslot, $resa->endTimeslot, $resa->id_object, $stockitem->serial);
	 					PrestaShopLogger::addLog(date('Y-m-d H:i:s').'stock:'.$stockitem->serial.'='.$scount);
	 					if ($scount==0) {
		 					if ($qty < $resa->quantity) {
		 						if ($stockitemassign=='') $stockitemassign.=$stockitem->serial;
		 						else $stockitemassign.=';'.$stockitem->serial;
		 						$qty++;
		 					} else break;
		 				}
	 				}
	 				PrestaShopLogger::addLog(date('Y-m-d H:i:s').'autoassign>'.$stockitemassign);
	 				if ($stockitemassign!='') myOwnResas::setReservationStock($resa->sqlId, $stockitemassign);
	 			}
				
				foreach ($obj->_extensions->getExtByType(extension_type::NOTIFICATION) as $ext) 
					if (method_exists($ext, "getAttachements"))
						$attachements = array_merge($attachements , $ext->getAttachements($obj, $order, $existingReservations));
				

				if ($attachements!=array()) {	
					if (Validate::isLoadedObject($order)) {
			  			$customer = new Customer($order->id_customer);
			  			if (Validate::isLoadedObject($customer)) {
			  			
			  			$message = self::displayReservationsConfirmation($obj, $order, false);

			  			$order_name=Configuration::get('PS_INVOICE_PREFIX', (int)($order->id_lang)).sprintf('%06d', $order->invoice_number);
						if (_PS_VERSION_ >= "1.5.0.0") $order_name=$order->getUniqReference();
						$varsTpl = array(
							'{lastname}' => $customer->lastname,
							'{firstname}' => $customer->firstname,
							'{id_order}' => $order->id,
							'{order_name}' => $order_name,
							'{message}' => $message
						);
			
						if (!empty($attachements))
							Mail::Send((int)$order->id_lang, 'newsletter',
							Mail::l('Reservations confirmation', (int)$order->id_lang), $varsTpl, $customer->email,
							$customer->firstname.' '.$customer->lastname, null, null, $attachements, null, _PS_MAIL_DIR_, true, (int)$order->id_shop);
											
						}
					}
				}
			}
  			
  			//compensation if creating order from canceled one
			//or $id_order_states[0]['id_order_state']== _PS_OS_OUTOFSTOCK_
			if (count($id_order_states)>0
				&& ( $id_order_states[0]['id_order_state']== _PS_OS_CANCELED_ 
				or $id_order_states[0]['id_order_state']== _PS_OS_ERROR_ )
				&& $currentOrderStatusID!= _PS_OS_CANCELED_ 
				&& $currentOrderStatusID!= _PS_OS_ERROR_ ) {
					foreach($existingReservations as $resa) {
						if (_PS_VERSION_ < "1.5.0.0") {
							$stockMvt = new StockMvt();
							$stockMvt->id_product = $resa->id_product;
							$stockMvt->id_product_attribute = $resa->id_product_attribute;
							$stockMvt->id_order = $orderID;
							$stockMvt->id_employee = 0;
							$stockMvt->quantity = $resa->quantity;
							$stockMvt->id_stock_mvt_reason = 1;
							$result = $stockMvt->add();
						} /*else 
							StockAvailable::updateQuantity($resa->id_product, $resa->id_product_attribute, $resa->quantity);*/
					}
			 }
			 
			 //new PS 1.5 
			 if (_PS_VERSION_ >= "1.5.0.0") {
			 	$new_os = new OrderState($currentOrderStatusID, $cookie->id_lang);
			 	
			 	// PRODUCT COMPENSATION
				$id_order_states = Db::getInstance()->executeS('
				SELECT `id_order_state`
				FROM `'._DB_PREFIX_.'order_history`
				WHERE `id_order` = '.(int)($orderID).'
				ORDER BY `date_add` DESC');
				if (count($id_order_states)>0) {
					$old_os = new OrderState($id_order_states[0]['id_order_state'], $cookie->id_lang);
					
					foreach($existingReservations as $resa) {
						// if becoming logable => adding sale so we remove it to balance
						if ($new_os->logable && !$old_os->logable)
						{
							if (!Pack::isPack($resa->id_product) &&
								($old_os->id == Configuration::get('PS_OS_ERROR') || $old_os->id == Configuration::get('PS_OS_CANCELED')) &&
								!StockAvailable::dependsOnStock($resa->id_product)) {
										StockAvailable::updateQuantity($resa->id_product, $resa->id_product_attribute, $resa->quantity);
							}
						}
						// if becoming unlogable => removes sale so we add it to balance
						elseif (!$new_os->logable && $old_os->logable)
						{
							if (!Pack::isPack($resa->id_product) &&
								($new_os->id == Configuration::get('PS_OS_ERROR') || $new_os->id == Configuration::get('PS_OS_CANCELED')) &&
								!StockAvailable::dependsOnStock($resa->id_product)) {
										StockAvailable::updateQuantity($resa->id_product, $resa->id_product_attribute, -$resa->quantity);
							}
							
						}
						// if waiting for payment => payment error/canceled so we add it to balance
						elseif (!$new_os->logable && !$old_os->logable &&
								 ($new_os->id == Configuration::get('PS_OS_ERROR') || $new_os->id == Configuration::get('PS_OS_CANCELED')) &&
								 !StockAvailable::dependsOnStock($resa->id_product)) {
										StockAvailable::updateQuantity($resa->id_product, $resa->id_product_attribute, -$resa->quantity);
						}
					}
				}
			}
		}
	}
	
	
	// Create ORder
	//-----------------------------------------------------------------------------------------------------------------
    public static function execNewOrder($obj, $params) {
 		$order = $params['order'];
 		$orderStatus = $params['orderStatus'];
 		myOwnReservationsController::_construire($obj);
 		$cartProducts = myOwnCarts::getProducts($order->id_cart, $obj->_products, $obj->_pricerules);
 		$existingReservations = myOwnResas::getReservations($order->id);
 		
 		//Create delivery on order creation
 		if(count($cartProducts)!=count($existingReservations)) {
 			//get shop id
 			$id_shop = myOwnUtils::getShop();
		
 			//save reservations lines
 			myOwnResas::addFromCart($order->id, $cartProducts, $id_shop);

 			$existingReservations = myOwnResas::getReservations($order->id);
 		}
 		MyOwnReservationsUtils::notificationMsg($obj, $order);
 		
 		if (count($existingReservations)>0 && Configuration::get('PS_STOCK_MANAGEMENT')!=0)
 		  if ($orderStatus->id != _PS_OS_CANCELED_ AND $orderStatus->id != _PS_OS_ERROR_)// && $orderStatus->id!= _PS_OS_OUTOFSTOCK_
 			foreach($existingReservations as $resa) {
     			 //$mainProduct = $obj->_products->getResProductFromProduct($resa->id_product);
     			 if (_PS_VERSION_ < "1.5.0.0") {
	     			 $stockMvt = new StockMvt();
	     			 $stockMvt->id_product = $resa->id_product;
	     			 $stockMvt->id_product_attribute = $resa->id_product_attribute;
	     			 $stockMvt->id_order = $order->id;
	     			 $stockMvt->id_employee = 0;
	     			 $stockMvt->quantity = $resa->quantity;
	     			 $stockMvt->id_stock_mvt_reason = 1;
	     			 $result = $stockMvt->add();
     			 } else 
     			 	StockAvailable::updateQuantity($resa->id_product, $resa->id_product_attribute, $resa->quantity);
			 	}
 			 	
     }

	// Product list
	//-----------------------------------------------------------------------------------------------------------------
		
	public static function execFilterProductSearch($obj, &$params) {
		$result = self::filterProducts($obj, $params['searchVariables']['products']);
		if (!$result) return false;

		$params['searchVariables']['products'] = $result['products'];
		$params['searchVariables']['label'] .= $result['period'];
	}
	
	public static function execProductListAssign($obj, &$params, $type)
	{
		global $smarty;
		global $cookie;
		global $cart;

		$context = $obj->getContext();
		$default_products_per_page = max(1, (int)Configuration::get('PS_PRODUCTS_PER_PAGE'));
        $n_array = array($default_products_per_page, $default_products_per_page * 2, $default_products_per_page * 5);
		$page = (int)Tools::getValue('p');
		$order_by_values = array(0 => 'name', 1 => 'price', 2 => 'date_add', 3 => 'date_upd', 4 => 'position', 5 => 'manufacturer_name', 6 => 'quantity');
		$order_way_values = array(0 => 'asc', 1 => 'desc');
		$orderby = Tools::strtolower(Tools::getValue('orderby', $order_by_values[(int)Configuration::get('PS_PRODUCTS_ORDER_BY')]));
		$orderway = Tools::strtolower(Tools::getValue('orderway', $order_way_values[(int)Configuration::get('PS_PRODUCTS_ORDER_WAY')]));
		$nbp = (int)Tools::getValue('n', Configuration::get('PS_PRODUCTS_PER_PAGE'));
		if (isset($context->cookie->nb_item_per_page) && in_array($context->cookie->nb_item_per_page, $n_array)) {
            $nbp = (int)$context->cookie->nb_item_per_page;
        }
		
		if ($_GET['id_category']==2 && Tools::getIsset('searchStartDay')) {
			$cat_products = $obj->_products->getProducts($cookie->id_lang, true);
			$cat_products_total = $cat_products;
			$cat_products = Product::getProductsProperties($cookie->id_lang, $cat_products);
			$smarty->clearAssign('subcategories');
		} else {
			$category=new Category($_GET['id_category']);
			$cat_products = $category->getProducts((int)($cookie->id_lang), (int)($page), (int)($nbp), $orderby, $orderway);
			$cat_products_total = $category->getProducts((int)($cookie->id_lang), 0, 1000, $orderby, $orderway);
		} 
		$resulttot = self::filterProducts($obj, $cat_products_total);
		$result = self::filterProducts($obj, $cat_products);

		if (!$result) return false;
		$smarty->assign('categoryNameComplement', ' '.$result['period']);

		$params['catProducts'] = $result['products'];
		$params['cat_products'] = $result['products'];
		$params['hookExecuted'] = true;
		$params['nbProducts']=count($resulttot['products']);
		$params['nb_products']=count($resulttot['products']);
	}
	
	public static function filterProducts($obj, $cat_products)
	{
		global $cookie;
		global $cart;
		$id_customer = $cookie->id_customer;
		$period = '';
		$mainProduct = null;
		if(session_id() == '') session_start();
		$objectSelection = 0;
		if (array_key_exists('objectSelection', $_SESSION))
			$objectSelection = (int)$_SESSION['objectSelection'];
		$out_products = array();
		if (isset($_GET['id_category'])) $mainProduct = $obj->_products->getResProductFromCategory($_GET['id_category']);
			
		if (Tools::getIsset('searchStartDay')) {
			$startDay = strtotime(Tools::getValue('searchStartDay'));
			$endDay = strtotime(Tools::getValue('searchEndDay'));
			
			$selLength = ($endDay - $startDay)/3600/24;

			$period = $obj->l('from', 'hooks').' '.MyOwnCalendarTool::formatDate($startDay, $cookie->id_lang).' '.$obj->l('to', 'hooks').' '.MyOwnCalendarTool::formatDate($endDay, $cookie->id_lang);
				
			foreach ($cat_products as $key => $cat_product) {
				$productId = $cat_product['id_product'];
				$mainProduct = $obj->_products->getResProductFromProduct($productId);

				if ($mainProduct!=null && (intval(Configuration::get('MYOWNRES_WIDGET_CHECKLEN'))==0 or round($selLength)>=$mainProduct->getMinDayLength())) {
					$productAttributeId = MyOwnReservationsUtils::getDefaultAttribute($productId);
           			$productQty = $mainProduct->getProductQuantity($productId, $productAttributeId, $obj->_stocks);

           			//check start and end
           			$availabilities = new myOwnAvailabilities($startDay, $endDay, $mainProduct, $productId, $productAttributeId);
           			$selectable=true;
           			if (!$availabilities->isAvailable($startDay, Tools::getValue('searchStartTimeslot',null), $productId, $productAttributeId)) $selectable=false;
           			if (!$availabilities->isAvailable($endDay, Tools::getValue('searchEndTimeslot',null), $productId, $productAttributeId)) $selectable=false;
           			//check availability (holiday if not allowed + resa)
           			$count = myOwnResas::getFlatResaLinesCount($cart, $availabilities, $mainProduct, $productId, $productAttributeId, $startDay, $endDay, Tools::getValue('searchStartTimeslot',0), Tools::getValue('searchEndTimeslot',0), $objectSelection);

           			if (($productQty<0 or $count<$productQty) && $count>-1 && $selectable) {
	           			if ($cat_product['quantity']>0)
	           				$cat_product['customizable']=0;
						$out_products[$key] = $cat_product;
					}
				}
			}
		} else if ($mainProduct!=null && array_key_exists('startSelection', $_SESSION) && $_SESSION['startSelection']!="" && Configuration::get('MYOWNRES_CAT_HIDE')==1 && (Configuration::get('MYOWNRES_WIDGET_TYPE')==1  || Configuration::get('MYOWNRES_WIDGET_TYPE')==3)) {

			$potentialResa = myOwnPotentialResa::getSessionPotentialResa($obj);
			if (class_exists('mymarketplace'))
				$filtermarket = myOwnExtensions::getConfig(8, 0, mymarketplace::CONFIG_RESTRICT_PRODS, 0);
			else $filtermarket=false;

			if ($potentialResa!=null) {
				$sessionPotentialAvailabilities = new myOwnAvailabilities($potentialResa->getStartDateTime(), $potentialResa->getEndDateTime(), $mainProduct, 0, 0, false, true);
				
				$period = $obj->l('for', 'hooks').' '.$potentialResa->toString($obj, $cookie->id_lang, false);
				foreach ($obj->_extensions->getExtByType(extension_type::SEL_WIDGET) as $ext) {
		 				if (in_array($ext->id, $mainProduct->ids_options))
							if (method_exists($ext, "widgetSelect") && $objectSelection && array_key_exists($objectSelection, $ext->objects->list))
									$complement.=' '.$obj->l('for', 'hooks').' '.strtolower($ext->label).' '.$ext->objects->list[$objectSelection]->name;
					}
				
				foreach ($cat_products as $key => $cat_product) {
					$productId = $cat_product['id_product'];
					$prod_customer_id=0;
					if ($filtermarket) {
						$customers = myOwnExtensions::getConfig(8, $productId, mymarketplace::CONFIG_MARKET, -1);
				    	foreach ($customers as $customer)
				    		$prod_customer_id = $customer['id_internal'];
				    		
					}
					$productAttributeId = MyOwnReservationsUtils::getDefaultAttribute($productId);
           			$potentialResa->id_product = $productId;
           			$potentialResa->id_product_attribute = $productAttributeId;
           			$potentialResa->_productQty = $mainProduct->getProductQuantity($productId, $productAttributeId, $obj->_stocks, $sessionPotentialAvailabilities);
           			$potentialResa->_reservations = null;
           			//$potentialResa->setPrice($obj->_pricerules->list);
           			$isAvailable = $potentialResa->isAvailable($cart, $sessionPotentialAvailabilities, $obj->_stocks) > 0;

           			if (($isAvailable || !Configuration::get('MYOWNRES_CAT_HIDE')) && (!$prod_customer_id || $prod_customer_id==$id_customer)) {
	           			if ($cat_product['quantity']>0)
	           				$cat_product['customizable']=0;
						$out_products[$key] = $cat_product;
					}
				}
			}
		} else if ($mainProduct!=null) {
			foreach ($cat_products as $key => $cat_product)
				if ($cat_product['quantity']>0) {
					$cat_product['customizable']=0;
					$out_products[$key] = $cat_product;
				}
		} else return false;

		return array('products' => $out_products, 'period' => $period);
	}
     
	public static function execAddWebserviceResources($obj, $params) {
		return  array('reservations' => array('description' => 'Reservations', 'class' => 'myOwnResas'));
	}

	public static function rutime($ru, $rus, $index) {
	    return ($ru["ru_$index.tv_sec"]*1000 + intval($ru["ru_$index.tv_usec"]/1000))
	     -  ($rus["ru_$index.tv_sec"]*1000 + intval($rus["ru_$index.tv_usec"]/1000));
	}
	
}



?>