<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnReservationsSettingsController {
	
	//==============================================================================================================================================
	//     PRICE SETTINGS
	//==============================================================================================================================================

	public static function priceFields($obj) {

		$iso = Context::getContext()->language->iso_code;
		myOwnHelp::translate($obj, $iso);
		return array (
			'MYOWNRES_DEPOSIT_SHOW' => array (
					'validation' => 'isBool',
                    'cast' => 'intval',
                    'type' => 'bool',
					'title' => $obj->l('Display Deposit', 'settings'),
					'hint' => $obj->l('Display but do not invoice the Deposit', 'settings')
				),
			'MYOWNRES_DEPOSIT_GLOBAL' => array (
					'validation' => 'isBool',
                    'cast' => 'intval',
                    'type' => 'bool',
					'title' => $obj->l('Higher Deposit', 'settings'),
					'hint' => $obj->l('Keep only the greater family deposit and not cumulate them', 'settings')
				),
			'MYOWNRES_NO_LOC_ADVANCE' => array (
					'validation' => 'isBool',
                    'cast' => 'intval',
                    'type' => 'bool',
                    'title' => $obj->l('No advance on the spot', 'settings'),
					'hint' => $obj->l('If true use maintenance IP to disable advance on physical store', 'settings')
			),
		);
	}

	public static function advdeposit($cookie,$obj) {
		$langue=$cookie->id_lang;
		$fields = self::priceFields($obj);
		$productPrice = array(reservation_price_display::UNIT => $obj->l('Unit price', 'settings'), reservation_price_display::MIN => $obj->l('Minimum price', 'settings'));
		if (!array_key_exists(0, $obj->_products->list))
			$productPrice[reservation_price_display::HIDE] = $obj->l('Hide', 'settings');
		$output = '';
		$output .= '
			<script type="text/javascript">
			function fixedAdvanceChanged() {
				var fixed=0;
				if ($("#resaAdvanceAmount")!=null)
					fixed=parseInt($("#resaAdvanceAmount").val());
				if (isNaN(fixed)) fixed=0;
				if (fixed>0) $("#resaAdvanceRate").val(0);
				
				var percent=parseInt($("#resaAdvanceRate").val());
				if (isNaN(percent)) percent=0;
				if (percent>0) $("#resaAdvanceAmount").val(0);
				
				$("#paymentOption_advance").prop("checked", (percent<100 || fixed>0));
				
			}
				
			</script>';
			
		//Price global					
		//-----------------------------------------------------------------------------------------------------------------
				
		if (_PS_VERSION_ < "1.6.0.0") $output .= '
			<fieldset class="myOwnConfigPanel">
				<legend><img src="../modules/myownreservations/img/AdminCurrencies.gif" />'.$obj->l('Deposit & Advance', 'settings').'</legend>';
		else $output .= '
			<div class="panel">
				<h3><i class="icon-credit-card"></i> '.$obj->l('Deposit & Advance', 'settings').'</h3>';
		$output .= '
				<ol style="padding-right:0px;padding-left:0px;margin:0px">';
			foreach ($fields as $key => $field) {
				$output .= '
				<li class="myowndlist">
					'.myOwnUtils::getBoolField($obj, $key, Tools::getValue($key, Configuration::get($key))==1, $field['title'], 'width:25%',  $field['hint']).'
				</li>';
			}
			$output .= '
				<li class="myowndlist">
					<label class="li-label" style="width:25%;padding-top:7px;padding-left:0px">' . $obj->l('Min Amount For Advance', 'products') .'</label>
					<div style="display:inline-block;width:80px">
						'.myOwnUtils::getInputField('minAdvanceAmount', (int)Tools::getValue('minAdvanceAmount', Configuration::get('MYOWNRES_MIN_FOR_ADVANCE')), '', 'width:50px;height: 28px;display:inline-block', 'upsInput', true).'
					</div>
					<span style="display:inline:block;color: #959595;vertical-align: super;">('.$obj->l('with taxes', 'settings').')</span>
				</li>
			</ol>
			<div style="clear:both"></div>
			'.(_PS_VERSION_ < "1.6.0.0" ? '</fieldset>' : '</div>');
			

		return $output;	
	}
	
	public static function price($cookie,$obj) {
		$langue=$cookie->id_lang;
		$fields = self::priceFields($obj);
		$productPrice = array(reservation_price_display::UNIT => $obj->l('Unit price', 'settings'), reservation_price_display::MIN => $obj->l('Minimum price', 'settings'));
		if (!array_key_exists(0, $obj->_products->list))
			$productPrice[reservation_price_display::HIDE] = $obj->l('Hide', 'settings');
		$output = '';


		//Price display					
				//-----------------------------------------------------------------------------------------------------------------
		
		if (_PS_VERSION_ < "1.6.0.0") $output .= '
			<fieldset class="myOwnConfigPanel">
				<legend><img src="../modules/myownreservations/img/AdminCurrencies.gif" />'.$obj->l('Price display', 'settings').'</legend>';
		else $output .= '
			<div class="panel">
				<h3><i class="icon-tags"></i> '.ucfirst(myOwnLang::$objects[MYOWN_OBJ::PRICERULE]).'</h3>';
		$output .= '
				<ol style="padding-right:0px;padding-left:0px;margin:0px">';
					
					
		$output .= '<li class="myowndlist">
						'.myOwnUtils::getBoolField($obj, 'globalPricerule', Tools::getValue('globalPricerule', Configuration::get('MYOWNRES_PRICERULE_BEST'))==1, $obj->l('Best Price Rule', 'settings'), 'width:25%', $obj->l('Keep only the best percentage price rule and not cumulate them', 'settings')).'
					</li>
					<li class="myowndlist">
						'.myOwnUtils::getBoolField($obj, 'priceTaxes', Tools::getValue('priceTaxes', Configuration::get('MYOWNRES_PRICE_TAXEXCL'))==1, $obj->l('Price Sets without taxes', 'settings'), 'width:25%').'
					</li>
					';
					
		$output .= '
				</ol>
			'.(_PS_VERSION_ < "1.6.0.0" ? '</fieldset>' : '</div>');
		
		//Price display					
		//-----------------------------------------------------------------------------------------------------------------
			
		if (_PS_VERSION_ < "1.6.0.0") $output .= '
			<fieldset class="myOwnConfigPanel">
				<legend><img src="../modules/myownreservations/img/AdminCurrencies.gif" />'.$obj->l('Price display', 'settings').'</legend>';
		else $output .= '
			<div class="panel">
				<h3><i class="icon-money"></i> '.$obj->l('Price display', 'settings').'</h3>';
		$output .= '
				<ol style="padding-right:0px;padding-left:0px;margin:0px">';
					
					
		$output .= '
					<li class="myowndlist">
						'.myOwnUtils::getBoolField($obj, 'totalTaxes', Tools::getValue('totalTaxes', Configuration::get('MYOWNRES_TOTAL_TAXINC'))==1, $obj->l('Total resas with taxes', 'settings'), 'width:25%', $obj->l('On cart summary display total reservations with taxes', 'settings')).'
					</li>
					<li class="myowndlist">
						'.myOwnUtils::getBoolField($obj, 'rawPrice', Tools::getValue('rawPrice', Configuration::get('MYOWNRES_PRICE_RAW'))==1, $obj->l('Display raw price', 'settings'), 'width:25%', $obj->l('Display reservation raw price (without discounts and quantity) after period', 'settings')).'
					</li>
					<li class="myowndlist">
						<label class="li-label" style="width:25%">
							<span class="mylabel-tooltip" title="'.myOwnHelp::$objects['settings']['priceDisplay'].'" >'.$obj->l('Product price display', 'settings').'</span>
						</label>
						'.'<select style="width:25%" name="priceType" id="priceType">';
							foreach ($productPrice AS $productPriceKey => $productPriceValue){
								$output .= '<option value="' . $productPriceKey. '"';
								if (intval(Configuration::get('MYOWNRES_PRICE_TYPE'))==$productPriceKey) $output .= ' selected';
								$output .= '>' . $productPriceValue . '</option>';
							}
							$output .= '
						</select>
					</li>';
					
		$output .= '
				</ol>
			'.(_PS_VERSION_ < "1.6.0.0" ? '</fieldset>' : '</div>');
		return $output;	
	}
	
	//==============================================================================================================================================
	//     CHECKOUT SETTINGS
	//==============================================================================================================================================

	public static function checkoutFields($obj) {

		$iso = Context::getContext()->language->iso_code;
		myOwnHelp::translate($obj, $iso);
		return array (
			'MYOWNRES_IGNORE_STEP1' => array (
					'validation' => 'isBool',
                    'cast' => 'intval',
                    'type' => 'bool',
					'title' => $obj->l('Ignore Address Step', 'settings'),
					'hint' => myOwnHelp::$objects['settings']['ignoreAddress']
				),
			'MYOWNRES_IGNORE_STEP2' => array (
					'validation' => 'isBool',
                    'cast' => 'intval',
                    'type' => 'bool',
					'title' => $obj->l('Ignore Carrier Step', 'settings'),
					'hint' => myOwnHelp::$objects['settings']['ignoreCarrier']
				),
			'MYOWNRES_IGNORE_PURCHA' => array (
					'validation' => 'isBool',
                    'cast' => 'intval',
                    'type' => 'bool',
                    'title' => $obj->l('Only for reservations', 'settings'),
					'hint' => $obj->l('Steps will remain if there is product in cart without reservations', 'settings')
			),
			'MYOWNRES_SAME_PERIOD' => array (
					'validation' => 'isBool',
                    'cast' => 'intval',
                    'type' => 'bool',
                    'title' => $obj->l('Same period restrict', 'settings'),
					'hint' => $obj->l('All products in cart must have the same reservation period', 'settings')
			),
		);
	}
	
	public static function checkout($cookie,$obj) {
		$langue=$cookie->id_lang;
		$fields = self::checkoutFields($obj);
		$output = '';
		if (_PS_VERSION_ < "1.6.0.0") $output .= '
		<fieldset class="myOwnConfigPanel">
			<legend><img src="../modules/myownreservations/img/AdminCarts.gif" />'.$obj->l('Order process', 'settings').'</legend>';
		else $output .= '
		<div class="panel">
			<h3><i class="icon-shopping-cart"></i> '.$obj->l('Order process', 'settings').'</h3>';
		$output .= '
			<ol style="padding-right:0px;padding-left:0px;margin:0px">';
			foreach ($fields as $key => $field) {
				$output .= '
				<li class="myowndlist">
					'.myOwnUtils::getBoolField($obj, $key, Tools::getValue($key, Configuration::get($key))==1, $field['title'], 'width:25%',  $field['hint']).'
				</li>';
			}
			$output .= '
			</ol>
			<div style="clear:both"></div>
			';

				$output .= '
		'.(_PS_VERSION_ < "1.6.0.0" ? '</fieldset>' : '</div>');
		return $output;
	}
	
	//==============================================================================================================================================
	//     WIDGET COLUMN
	//==============================================================================================================================================

	public static function widgetColumn($cookie,$obj) {
		$langue=$cookie->id_lang;
		$widgetType = array(
		0 => $obj->l('None', 'settings'),
		1 => $obj->l('Select start and end', 'settings'),
		3 => $obj->l('Select start and length', 'settings'),
		2 => $obj->l('Search products available', 'settings')
		);
		$positions = array(
		widget_position::COL => $obj->l('On Column', 'settings'),
		widget_position::TOP => $obj->l('On Home Top', 'settings'),
		);
		$widgetPos = array(0 => $obj->l('Left column', 'settings'), 1 => $obj->l('Right column', 'settings'));
		$output = '';
		if (_PS_VERSION_ < "1.6.0.0") $output .= '
		<fieldset class="myOwnConfigPanel">
			<legend><img src="../img/admin/date.png" />'.$obj->l('Reservation widget', 'settings').'</legend>';
		else $output .= '
		<div class="panel">
				<h3><i class="icon-search"></i> '.$obj->l('Reservation widget', 'settings').'</h3>';
		$output .= '
			<ol style="float:left;padding-right:0px;padding-left:0px;margin:0px;width:100%">
				<li class="myowndlist">
					<label class="li-label" style="width:25%">
						<span class="mylabel-tooltip" title="'.myOwnHelp::$objects['settings']['columnWidget'].'">'.$obj->l('Type', 'settings').'</span>
					</label>
					<select style="width:50%" name="widgetType" id="widgetType" onChange="columWidget()">';
						foreach ($widgetType AS $widgetKey => $widgetValue){
							$output .= '<option value="' . $widgetKey. '"';
							if (intval(Configuration::get('MYOWNRES_WIDGET_TYPE'))==$widgetKey) $output .= ' selected';
							$output .= '>' . $widgetValue . '</option>';
						}
						$output .= '
					</select>
				</li>';
				if (_PS_VERSION_ < "1.6.0.0") {
					$output .= '
				<li class="myowndlist widgetPosition" style="height:22px">
					<label class="li-label" style="width:25%">'.$obj->l('Position', 'settings').'</label>';
						foreach ($widgetPos AS $widgetKey => $widgetValue)
							$output .= '<input type="radio" name="widgetPosition" value="' . $widgetKey. '" '.(Configuration::get('MYOWNRES_WIDGET_POS')==$widgetKey ? 'checked' : '').'> '.$widgetValue.'&nbsp;&nbsp;&nbsp;';
						$output .= '
				</li>';
				} else {
					$output .= '
				<li class="myowndlist">
				<label for="timeSlotEnable" class="li-label" style="width:25%;padding-top:0px">'.$obj->l('Widget Position', 'settings').'</label>
				';
					foreach ($positions AS $posKey => $posValue){
						$output .= '&nbsp;<input type="checkbox" id="modeControl" onclick="" class="listElem" name="widget'.($posKey == widget_position::TOP ? 'Top' : 'Column').'" value="1" '.(($posKey == widget_position::COL && Configuration::get('MYOWNRES_WIDGET_COL')==1) || ($posKey == widget_position::TOP && Configuration::get('MYOWNRES_WIDGET_TOP')==1) ? 'checked' : '').' >&nbsp;&nbsp;<img src="../modules/'.$obj->name.'/img/layout'.$posKey.'.png">&nbsp;' . $posValue.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					}
					$output .= '
				
				</li>
					';
				}
				$output .= '
				
				<li class="myowndlist" id="widgetSearchCheckLen">
					'.myOwnUtils::getBoolField($obj, 'checkLength', Tools::getValue('checkLength',Configuration::get('MYOWNRES_WIDGET_CHECKLEN'))==1, $obj->l('Check length', 'settings'), 'width:25%', $obj->l('Dismiss products that have shorter length than selection', 'settings')).'
				</li>
				<li class="myowndlist" id="widgetColumnStrict">
					'.myOwnUtils::getBoolField($obj, 'widgetColumnStrict', Tools::getValue('widgetColumnStrict',Configuration::get('MYOWNRES_WIDGET_COLSTRICT'))==1, $obj->l('Limited display', 'settings'), 'width:25%', $obj->l('Display only on reservation categories', 'settings')).'
				</li>

			</ol>
			<div style="clear:both"></div>
		'.(_PS_VERSION_ < "1.6.0.0" ? '</fieldset>' : '</div>');
		return $output;
	}
	
	public static function widgetProduct($cookie,$obj) {
		$langue=$cookie->id_lang;
		$positions = array(
		widget_position::TABS => $obj->l('Bottom tabs', 'settings'),
		widget_position::RIGHT => $obj->l('Right side (mini size)', 'settings'),
		widget_position::POPUP => $obj->l('Popup', 'settings'),
		);
		$legendpositions = array(
		widget_position::NONE => $obj->l('None', 'settings'),
		widget_position::TABS => $obj->l('Bottom tabs', 'settings'),
		widget_position::RIGHT => $obj->l('Right side', 'settings'),
		);
		$widgetPos = array(0 => $obj->l('Left column', 'settings'), 1 => $obj->l('Right column', 'settings'));
		$output = '';
		if (_PS_VERSION_ < "1.6.0.0") $output .= '
		<fieldset class="myOwnConfigPanel">
			<legend><img src="../img/admin/date.png" />'.$obj->l('Product Page', 'settings').'</legend>';
		else $output .= '
		<div class="panel">
				<h3><i class="icon-columns"></i> '.$obj->l('Product Page', 'settings').'</h3>';
		$output .= '
			<ol style="float:left;padding-right:0px;padding-left:0px;margin:0px;width:100%">
				<li class="myowndlist">
					<label for="timeSlotEnable" class="li-label" style="width:25%;padding-top:0px">'.$obj->l('Planning Position', 'settings').'</label>
					';
					foreach ($positions AS $posKey => $posValue){
						$output .= '&nbsp;<input type="radio" id="modeControl" onclick="" class="listElem" name="productLayout" value="'.$posKey.'" '.(Configuration::get('MYOWNRES_WIDGET_PROD') == $posKey ? 'checked' : '').' >&nbsp;&nbsp;<img src="../modules/'.$obj->name.'/img/layout'.$posKey.'.png">&nbsp;' . $posValue.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					}
					$output .= '
				
				</li>
				<li class="myowndlist">
					<label for="timeSlotEnable" class="li-label" style="width:25%;padding-top:0px">'.$obj->l('Price rules Legend', 'settings').'</label>
					';
					foreach ($legendpositions AS $posKey => $posValue){
						$output .= '&nbsp;<input type="radio" id="modeControl" onclick="" class="listElem" name="priceruleLegend" value="'.$posKey.'" '.(Configuration::get('MYOWNRES_PRICERULE_LEGEND') == $posKey ? 'checked' : '').' >&nbsp;&nbsp;<img src="../modules/'.$obj->name.'/img/layout'.$posKey.'.png">&nbsp;' . $posValue.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					}
					$output .= '
				
				</li>
				<li class="myowndlist">
					<label for="timeSlotEnable" class="li-label" style="width:25%;padding-top:0px">'.$obj->l('Price Sets Legend', 'settings').'</label>
					';
					foreach ($legendpositions AS $posKey => $posValue){
						$output .= '&nbsp;<input type="radio" id="modeControl" onclick="" class="listElem" name="pricesetLegend" value="'.$posKey.'" '.(Configuration::get('MYOWNRES_PRICESET_LEGEND') == $posKey ? 'checked' : '').' >&nbsp;&nbsp;<img src="../modules/'.$obj->name.'/img/layout'.$posKey.'.png">&nbsp;' . $posValue.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					}
					$output .= '
				
				</li>
			</ol>
			<div style="clear:both"></div>
		'.(_PS_VERSION_ < "1.6.0.0" ? '</fieldset>' : '</div>');
		return $output;
	}
	
	//==============================================================================================================================================
	//     PROD LIST
	//==============================================================================================================================================

	public static function listPref($cookie,$obj) {
		$langue=$cookie->id_lang;

		$output = '';
		if (_PS_VERSION_ < "1.6.0.0") $output .= '
		<fieldset class="myOwnConfigPanel"  id="prodlist">
			<legend><img src="../img/admin/tab-categories.gif" />'.$obj->l('Product list', 'settings').'</legend>';
		else $output .= '
		<div class="panel" id="prodlist">
				<h3><i class="icon-list"></i> '.$obj->l('Product list', 'settings').'</h3>';
		$output .= '
			<ol style="float:left;padding-right:0px;padding-left:0px;margin:0px;width:100%">
				<li class="myowndlist">
						'.myOwnUtils::getBoolField($obj, 'addToCartOnList', Tools::getValue('addToCartOnList',!Configuration::get('MYOWNRES_ADDTOCART_ONLIST')), $obj->l('Add to cart button', 'settings'), 'width:25%', $obj->l('Display add to cart button if available on period', 'settings')).'
				</li>
				<li class="myowndlist">
						'.myOwnUtils::getBoolField($obj, 'catHide', Tools::getValue('catHide',Configuration::get('MYOWNRES_CAT_HIDE')), $obj->l('Hide unavailable', 'settings'), 'width:25%', $obj->l('Hide products that are not available on period', 'settings')).'
				</li>
			</ol>
			<div style="clear:both"></div>
		'.(_PS_VERSION_ < "1.6.0.0" ? '</fieldset>' : '</div>');
		return $output;
	}
		
	//==============================================================================================================================================
	//     WIDGET HOME
	//==============================================================================================================================================

	public static function widgetHome($cookie,$obj) {
		$langue=$cookie->id_lang;
		$widgetType = array(
		0 => $obj->l('None', 'settings'),
		1 => $obj->l('Availabilities per product', 'settings'),
		2 => $obj->l('Product per availabilities', 'settings'),
		3 => $obj->l('Reservation Widget', 'settings'),
		);
		$output = '';
		if (_PS_VERSION_ < "1.6.0.0") $output .= '
		<fieldset class="myOwnConfigPanel">
			<legend><img src="../img/admin/home.gif" />'.$obj->l('Home availability tab', 'settings').'</legend>';
		else $output .= '
		<div class="panel">
			<h3><i class="icon-home"></i> '.$obj->l('Home availability tab', 'settings').'</h3>';
		$output .= '
			<ol style="padding-left:0px;margin:0px;width:100%">';

				//Widget home						
				//-----------------------------------------------------------------------------------------------------------------
				$output .= '
				<li class="myowndlist">
					<label class="li-label" style="width:25%">'.$obj->l('Type', 'settings').'</label>
					<select style="width:30%" name="widgetHome" id="widgetHome" onChange="columWidget();$(\'#widgetHomeImg-li\').toggle(($(this).val()!=3));">';
						foreach ($widgetType AS $widgetKey => $widgetValue){
							$output .= '<option value="' . $widgetKey. '"';
							if ((int)Configuration::get('MYOWNRES_WIDGET_HOME')==$widgetKey) $output .= ' selected';
							$output .= '>' . $widgetValue . '</option>';
						}
						$output .= '
					</select>
				</li>
				<li class="myowndlist" id="widgetHomeImg-li" style="'.(Configuration::get('MYOWNRES_WIDGET_HOME')==3 ? 'display:none':'').'">
					<label for="Category" class="li-label" style="width:25%">
						<span class="mylabel-tooltip" title="'.myOwnHelp::$objects['settings']['homeWidget'].'" >'.$obj->l('Image size', 'settings').'</span>
					</label>
					<select style="width:25%" name="widgetHomeImg" >
						<option value="-1" '.(Configuration::get('MYOWNRES_WIDGET_IMG')=='' ? 'selected' : '').'>'.$obj->l('None', 'settings').'</option>';
						if (count($obj->_products->list))
						foreach (ImageType::getImagesTypes() AS $img){
							$output .= '<option value="' . $img['id_image_type']. '"';
							if (Configuration::get('MYOWNRES_WIDGET_IMG')==$img['id_image_type']) $output .= ' selected';
							$output .= '>' .$img['name'] .' ('. $img['width'] . ')</option>';
						}
						$output .= '
					</select>
				</li>

			</ol>
		'.(_PS_VERSION_ < "1.6.0.0" ? '</fieldset>' : '</div>');
		return $output;
	}
		
	//==============================================================================================================================================
	//     IMPORT
	//==============================================================================================================================================

	public static function import($cookie,$obj) {
		$langue=$cookie->id_lang;
		$output = '';

		if (_PS_VERSION_ < "1.6.0.0") $output .= '
		<fieldset class="myOwnConfigPanel">
			<legend><img src="../img/admin/import.gif">'.$obj->l('Import file', 'settings').'</legend>';
		else $output .= '
		<div class="panel">
			<h3><i class="icon-upload"></i> '.$obj->l('Import file', 'settings').'</h3>';
		$output .= '
			
				<li class="myowndlist">
					<label class="li-label" style="width:25%">
						<span class="mylabel-tooltip" title="'.myOwnHelp::$objects['settings']['import'].'" >'.$obj->l('File', 'settings').'</span>
					</label>
					'.MyOwnReservationsUtils::getFileField().'
				
				</li>
				<li class="myowndlist">
					<label class="li-label" style="width:25%">'.$obj->l('Data type', 'settings').'</label>
						<select style="width:25%" name="datatable" id="importType">
							<option value="" selected="selected">'.$obj->l('Select..', 'settings').'</option>
							<option value="reservation">'.ucfirst(myOwnLang::$objects[MYOWN_OBJ::RESV]).'</option>
							<option value="timeslot">'.ucfirst(myOwnLang::$objects[MYOWN_OBJ::TIMESLOT]).'</option>
							<option value="pricesrule">'.ucfirst(myOwnLang::$objects[MYOWN_OBJ::PRICERULE]).'</option>
							<option value="pricesset">'.$obj->l('Price sets', 'settings').'</option>
							<option value="products">'.ucfirst(myOwnLang::$objects[MYOWN_OBJ::PRODFAMILY]).'</option>
							<option value="availability">'.ucfirst(myOwnLang::$objects[MYOWN_OBJ::AVAILABILITY]).'</option>
							<option value="stock">'.ucfirst(myOwnLang::$objects[MYOWN_OBJ::STOCK]).'</option>
						</select>
				</li>

				<li class="myowndlist">
						'.myOwnUtils::getBoolField($obj, 'emptyData', Tools::getValue('emptyData')==1, $obj->l('Empty data', 'settings'), 'width:25%', myOwnHelp::$objects['settings']['importempty'], $obj->l('Warning this will delete all data type from database', 'settings')).'<div style="clear:both"></div>
					</li>

		'.(_PS_VERSION_ < "1.6.0.0" ? '</fieldset>' : '</div>');
		return $output;
	}
	
	//==============================================================================================================================================
	//     IMPORT
	//==============================================================================================================================================

	public static function synchronize($cookie,$obj) {
		$langue=$cookie->id_lang;
		$output = '';
		$link = new Link();
		if (_PS_VERSION_ < "1.6.0.0") $output .= '
		<fieldset class="myOwnConfigPanel">
			<legend><img src="../img/admin/transfer_stock.png">'.$obj->l('Synchronize', 'settings').'</legend>';
		else $output .= '
		<div class="panel">
			<h3><i class="icon-refresh"></i> '.$obj->l('Synchronize', 'settings').'</h3>';

		$iscron = MyOwnReservationsUtils::isModuleActive('cronjobs');
		if ($iscron || _PS_VERSION_ < "1.6.0.0") {
			$id_module = $obj->id;
			$id_shop = myOwnUtils::getShop();
			$cronjob = Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'cronjobs`
                WHERE `id_module` = \''.$id_module.'\' AND `id_shop` = \''.$id_shop.'\'');
            $date='';
            if (array_key_exists(0, $cronjob))
            	$date=$cronjob[0]['updated_at'];
                
			foreach (myOwnLang::$sync_platforms AS $platform_key => $platform_label){
				$output .= '
				<li class="myowndlist">
						'.myOwnUtils::getBoolField($obj, 'synchro_'.$platform_key, Configuration::get('MYOWNRES_SYNC_'.$platform_key)==1, $platform_label, 'width:25%').'<div style="clear:both"></div>
				</li>';
			}
			if (Configuration::get('MYOWNRES_SYNC_'.sync_platforms::EXPEDIA)==1) {
				$output .= '
				<div class="well">
					<div style="display:table-row">
						<label class="li-label" style="display:table-cell;width:15%">'.$obj->l('EQC Login', 'settings').'</label>
						<input class="upsInput" style="display:table-cell;width:15%" size="20" type="text" name="expediaLogin" value="'.Configuration::get('MYOWNRES_SYNC_EQC_LOGIN').'" autocomplete="off">
						<label class="li-label" style="display:table-cell;width:15%">'.$obj->l('Password', 'settings').'</label>
						<input class="upsInput" style="display:table-cell;width:15%" size="20" type="password" name="expediaPassword" value="'.Configuration::get('MYOWNRES_SYNC_EQC_PASS').'" autocomplete="off">
						<label class="li-label" style="display:table-cell;width:20%">'.$obj->l('Property Id', 'settings').'</label>
						<input class="upsInput" style="display:table-cell;width:15%" size="20" type="text" name="expediaHotelId" value="'.Configuration::get('MYOWNRES_SYNC_EQC_HOTELID').'" autocomplete="off">
					</div>
					<div style="clear:both"></div>
				</div>';
			}
			$output .= '
				<li class="myowndlist"></li>
				<li class="myowndlist">
						'.myOwnUtils::getBoolField($obj, 'synchro_external', Configuration::get('MYOWNRES_SYNC_EXT')==1, $obj->l('Sync external', 'settings'), 'width:25%').'<div style="clear:both"></div>
				</li>
				<li class="myowndlist">
						'.myOwnUtils::getBoolField($obj, 'synchro_between', Configuration::get('MYOWNRES_SYNC_BETW')==1, $obj->l('Sync between platfroms', 'settings'), 'width:25%').'<div style="clear:both"></div>
				</li>';
			if ($date!='') $output .= '
				<li class="myowndlist"></li>
				<li class="myowndlist"><label class="li-label" style="width:25%;padding:0px;">'.$obj->l('Last sync', 'settings').'</label><span style="padding-left:10px">'.$date.'</span></li>';
			if (_PS_VERSION_ < "1.6.0.0") $output .=  myOwnUtils::displayInfo($obj->l('To enable synchronisation you have to add this line in your corn task manager', 'settings').':  0 * * * * curl "'.MyOwnReservationsUtils::getModuleLink($link, 'tasks').'"');
		
		} else {
			$output .=  myOwnUtils::displayWarn($obj->l('To enable synchronisation you have to enable cronjobs module', 'settings'));
		}
		
						$output .= '


		'.(_PS_VERSION_ < "1.6.0.0" ? '</fieldset>' : '</div>');
		return $output;
	}
	
	//==============================================================================================================================================
	//     LABEL SETTINGS
	//==============================================================================================================================================

	public static function labels($cookie,$obj) {
		$output='';
		$defaultLanguage = intval(Configuration::get('PS_LANG_DEFAULT'));
		$languages = Language::getLanguages();
		//Messages					
		//-----------------------------------------------------------------------------------------------------------------
		$output .= '
			<script type="text/javascript">
				var id_languagew = Number('.$defaultLanguage.');

				function	changeSettingsLanguage(field, fieldsString, id_language_new, iso_code)
				{	
							document.getElementById(field+"B_" + id_languagew).style.display = "none";
							document.getElementById(field+"B_" + id_language_new).style.display = "block";
							
							document.getElementById(field+"C_" + id_languagew).style.display = "none";
							document.getElementById(field+"C_" + id_language_new).style.display = "block";
							
							document.getElementById(field+"D_" + id_languagew).style.display = "none";
							document.getElementById(field+"D_" + id_language_new).style.display = "block";
							
							document.getElementById(field+"E_" + id_languagew).style.display = "none";
							document.getElementById(field+"E_" + id_language_new).style.display = "block";
							
							document.getElementById(field+"F_" + id_languagew).style.display = "none";
							document.getElementById(field+"F_" + id_language_new).style.display = "block";
							
							document.getElementById(field+"G_" + id_languagew).style.display = "none";
							document.getElementById(field+"G_" + id_language_new).style.display = "block";
							
							document.getElementById(field+"H_" + id_languagew).style.display = "none";
							document.getElementById(field+"H_" + id_language_new).style.display = "block";
							
							document.getElementById(field+"I_" + id_languagew).style.display = "none";
							document.getElementById(field+"I_" + id_language_new).style.display = "block";
							
							document.getElementById(field+"L_" + id_languagew).style.display = "none";
							document.getElementById(field+"L_" + id_language_new).style.display = "block";
							
					'.(_PS_VERSION_ < "1.6.0.0" ? '
					document.getElementById("language_current_"+field).src = "../img/l/" + id_language_new + ".jpg";
					document.getElementById("languages_"+field).style.display = "none";' : '$(".languageMyOwn").html(iso_code+\' <span class=\"caret\"></span>\');').'
					
					id_languagew = id_language_new;
				}
				function columWidget() {
					var type= $("#widgetType").val();
					
					if (type==0) $(".widgetPosition").hide();
					else $(".widgetPosition").show();
					
					if (type==0) $("#widgetColumnStrict").hide();
					else $("#widgetColumnStrict").show();
					
					if (type!=2) $("#widgetSearchCheckLen").hide();
					else $("#widgetSearchCheckLen").show();
					
					if (type==1) $("#widgetDefaultCat").show();
					else $("#widgetDefaultCat").hide();
					
					if (type==0 && $("#widgetHome").val()!=3) $("#prodlist").hide();
					else  $("#prodlist").show();
				}
				$(document).ready(function () {
					columWidget();
				});
			</script>';
			if (_PS_VERSION_ < "1.6.0.0") $output .= '
			<fieldset class="myOwnConfigPanel">
				<legend><img src="../img/admin/contact.gif" />'.$obj->l('Messages and labels', 'settings').'</legend>';
			else $output .= '
			<div class="panel">
					<h3><i class="icon-envelope-o"></i> '.$obj->l('Messages and labels', 'settings').'</h3>';
			$output .= '
				<ol style="padding-right:0px;padding-left:0px;margin:0px">
					<li class="myowndlist">
						<label class="li-label" style="width:200px">
							<span class="mylabel-tooltip" title="'.myOwnHelp::$objects['settings']['messages'].'">' . $obj->l('Reservation Message', 'settings') . '</span></label>
						'.myOwnUtils::getLanguageFlags($obj, 'changeSettingsLanguage', 'ReservationMsg').'
					</li>
					<br/>
					<li class="myowndlist">
						<label class="myowndeliveriesSettingsLabel" style="height:18px;">' . $obj->l('Deactivate', 'settings') . '&nbsp;:</label>';
						foreach ($languages as $language)
							$output .= '
							<div id="ReservationMsgB_'.$language['id_lang'].'" style="width:100%;display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
								<input style="width:98%" type="text" name="disabledReservationMsg_'.$language['id_lang'].'" id="ReservationBInput_'.$language['id_lang'].'" value="'.stripslashes(myOwnLang::getMessage('DESACTIVATE_RES', $language['id_lang'], true)).'" />
	
							</div>';
					$output .= '
					</li>
					<li class="myowndlist">
						<label class="myowndeliveriesSettingsLabel style="height:18px;">' . $obj->l('Notification', 'settings') . '&nbsp;:</label>';
						foreach ($languages as $language)
							$output .= '
							<div id="ReservationMsgC_'.$language['id_lang'].'" style="width:100%;display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
								<input style="width:98%" type="text" name="notifyReservationMsg_'.$language['id_lang'].'" id="ReservationCInput_'.$language['id_lang'].'" value="'.stripslashes(myOwnLang::getMessage('NOTIFY_RES', $language['id_lang'], true)).'" />
	
							</div>';
					$output .= '
					</li>
					<li class="myowndlist">
					<label class="myowndeliveriesSettingsLabel" style="height:18px;">' . $obj->l('Not available', 'settings'). '&nbsp;:</label>';
						foreach ($languages as $language)
							$output .= '
							<div id="ReservationMsgL_'.$language['id_lang'].'" style="width:100%;display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
								<input style="width:98%" type="text" name="notAvailableReservationMsg_'.$language['id_lang'].'" id="ReservationLInput_'.$language['id_lang'].'" value="'.stripslashes(myOwnLang::getMessage('NOTAVAILABLE', $language['id_lang'], true)).'" />
	
							</div>';
					$output .= '
					</li>
					<li class="myowndlist">
						<label class="myowndeliveriesSettingsLabel" style="height:18px;">' . $obj->l('Owner notify', 'settings') . '&nbsp;:</label>';
						foreach ($languages as $language)
							$output .= '
							<div id="ReservationMsgH_'.$language['id_lang'].'" style="width:100%;display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
								<input style="width:98%" size="100%" type="text" name="notifyProductMsg_'.$language['id_lang'].'" id="ReservationHInput_'.$language['id_lang'].'" value="'.stripslashes(myOwnLang::getMessage('NOTIFY_PROD', $language['id_lang'], true)).'" />
	
							</div>';
					$output .= '
					</li>
					<li class="myowndlist">
						<label class="myowndeliveriesSettingsLabel" style="height:18px;">' . $obj->l('Customer notify', 'settings') . '&nbsp;:</label>';
						foreach ($languages as $language)
							$output .= '
							<div id="ReservationMsgI_'.$language['id_lang'].'" style="width:100%;display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
								<input style="width:98%" size="100%" type="text" name="notifyCustMsg_'.$language['id_lang'].'" id="ReservationIInput_'.$language['id_lang'].'" value="'.stripslashes(myOwnLang::getMessage('NOTIFY_CUST', $language['id_lang'], true)).'" />
	
							</div>';
					$output .= '
					</li>
					<div style="clear:both"></div>
					
					<li class="myowndlist">
						<label class="myowndeliveriesSettingsLabel" style="width:100px;height:18px;">' . ucfirst(myOwnLang::$object[MYOWN_OBJ::RESV]) . '</label>';
						foreach ($languages as $language)
							$output .= '
							<div id="ReservationMsgD_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
								<input size="30" type="text" name="resvMsg_'.$language['id_lang'].'" id="ReservationDInput_'.$language['id_lang'].'" value="'.stripslashes(Configuration::get('MYOWNRES_MSG_RESV', $language['id_lang'])).'" />
	
							</div>';
						$output .= '
						<div style="clear:both"></div>
					</li>
					
					<li class="myowndlist">
						<label class="myowndeliveriesSettingsLabel" style="width:100px;height:18px;">' . $obj->l('Deposit', 'settings') . '</label>';
						foreach ($languages as $language)
							$output .= '
							<div id="ReservationMsgF_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
								<input size="30" type="text" name="depositMsg_'.$language['id_lang'].'" id="ReservationFInput_'.$language['id_lang'].'" value="'.stripslashes(Configuration::get('MYOWNRES_MSG_DEPOSIT', $language['id_lang'])).'" />
							</div>';
						$output .= '
						<div style="clear:both"></div>
					</li>
					
					<li class="myowndlist">
						<label class="myowndeliveriesSettingsLabel" style="width:100px;height:18px;">' . $obj->l('Advance', 'settings') . '</label>';
					foreach ($languages as $language)
						$output .= '
						<div id="ReservationMsgE_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
							<input size="30" type="text" name="advanceMsg_'.$language['id_lang'].'" id="ReservationEInput_'.$language['id_lang'].'" value="'.stripslashes(Configuration::get('MYOWNRES_MSG_ADVANCE', $language['id_lang'])).'" />
						</div>';
					$output .= '
						<div style="clear:both"></div>
					</li>
					
					<li class="myowndlist">
						<label class="myowndeliveriesSettingsLabel" style="width:100px;height:18px;">' . $obj->l('Balance', 'settings') . '</label>';
					foreach ($languages as $language)
						$output .= '
						<div id="ReservationMsgG_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
							<input size="30" type="text" name="balanceMsg_'.$language['id_lang'].'" id="ReservationGInput_'.$language['id_lang'].'" value="'.stripslashes(Configuration::get('MYOWNRES_MSG_BALANCE', $language['id_lang'])).'" />
						</div>';
					$output .= '
						<div style="clear:both"></div>
					</li>
				</ol>
				<div style="clear:both"></div>
			'.(_PS_VERSION_ < "1.6.0.0" ? '</fieldset>' : '</div>');
		return $output;	
	}

	//==============================================================================================================================================
	//     SETTINGS
	//==============================================================================================================================================

	public static function settings($cookie,$obj) {
		$langue=$cookie->id_lang;
		$ctrl=Tools::getValue('controller');
		$output = '';

		if (_PS_VERSION_ < "1.5.0.0") $output .= '
	<div style="padding:20px 10px 0px 10px;">';
		$output .= '
		<form id="editConfigForm" name="editConfigForm" action="'.myOwnReservationsController::getConfUrl('advanced').'" method="post" enctype="multipart/form-data">';
		
		if ($ctrl!='adminReservationConfig') {
			if (_PS_VERSION_ < "1.6.0.0") $output .= '
			<fieldset class="settingsList">
				<legend><img src="../img/admin/htaccess.gif" />' . $obj->l('Module license key', 'settings') .'</legend>';
				else $output .= '
			<div class="panel">
				<h3><i class="icon-key"></i> ' . $obj->l('Module license key', 'settings') .'</h3>';
				$output .= '
				<ol style="padding-right:0px;padding-left:0px;margin:0px">
					<li class="myowndlist">
						<label class="li-label" style="width:200px">
							<span class="mylabel-tooltip" title="'.myOwnHelp::$tuto['registration'].'">' . $obj->l('License key', 'settings') . '</span></label>
						<input style="width:50%" class="upsInput" size="42" type="text" name="myownrkey" value="' . Configuration::get('MYOWNR_KEY') . '" />
					</li>
				</ol>
				<div style="clear:both"></div>
			'.(_PS_VERSION_ < "1.6.0.0" ? '</fieldset>' : '</div>');
		}
		$output .= '
			<table style="width:100%">
				<tr>
					<td width="50%" style="padding-right:20px">';
			$outputext='';
			$extcnt=0;
			//Add extenssion config
			foreach ($obj->_extensions->getExtByType(extension_type::MODULE) as $ext) 
				if (method_exists($ext, "displayConfigGlobal")) {
					$extcnt++;
					$name = 'ext_enabled_'.$ext->id;
					if (_PS_VERSION_ < "1.6.0.0") $outputext .= '
					<fieldset class="myOwnConfigPanel">
						<legend><img src="../modules/myownreservations/extensions/'.$ext->name.'.png" width="16px" height="16px" />'.$ext->title.'</legend>';
					else $outputext .= '
					<div class="panel" '.(!$ext->enabled? 'style="padding-bottom: 0px;"' : '').'>
						<div class="panel-heading" '.(!$ext->enabled? 'style="margin-bottom: 0px;"' : '').'>
							<i class="icon-'.$ext->icon.'"></i> '.$ext->title.'
							<span class="panel-heading-action">
								<span style="display:inline-flex;margin-top: 0px;margin-right: 2px;" class="switch prestashop-switch fixed-width-lg">
									<input '.($ext->needconfig ? 'disabled' : '').' type="radio" name="'.$name.'" id="'.$name.'_on" value="1" '.($ext->enabled ? 'checked="checked"' : '').'>
									<label for="'.$name.'_on" >'.$obj->l('Enabled', 'myownutils').'</label>
									<input '.($ext->needconfig ? 'disabled' : '').' type="radio" name="'.$name.'" id="'.$name.'_off" value="0" '.(!$ext->enabled ? 'checked="checked"' : '').'>
									<label for="'.$name.'_off">'.$obj->l('Disabled', 'myownutils').'</label>
									<a style="position: absolute;top: 0;" class="slide-button btn"></a>
								</span>
							</span>
					</div>';
					if ($ext->enabled) $outputext .= $ext->displayConfigGlobal($cookie,$obj);
					$outputext .= '</fieldset>';
					$outputext .= (_PS_VERSION_ < "1.6.0.0" ? '</fieldset>' : '</div>');
				}
				
			
			
			$output .= self::price($cookie,$obj);
			if ($ctrl!='adminReservationConfig') {
				$output .= self::advdeposit($cookie,$obj);
				$output .= self::checkout($cookie,$obj);
			}
			$output .= self::labels($cookie,$obj);
			$obj->checkIsProVersion();
			if ($obj->isProVersion && $extcnt>2) $output .= self::import($cookie,$obj).self::synchronize($cookie,$obj);
			$output .= '
					</td>
					<td width="50%" style="vertical-align:top">';
			$output .= self::widgetHome($cookie,$obj);
			$output .= self::widgetColumn($cookie,$obj);
			$output .= self::listPref($cookie,$obj);
			$output .= self::widgetProduct($cookie,$obj);
			if ($obj->isProVersion && $extcnt<3) $output .= self::import($cookie,$obj).self::synchronize($cookie,$obj);
			$output .= $outputext;
					
					
			$output .= '
					</td>
				</tr>
			</table>';
			
		if (_PS_VERSION_ < "1.5.0.0") $output .= '
				<div style="text-align:center;width:100%"><input type="submit" name="editConfig" value="' . $obj->l('Update Settings', 'settings') . '" class="button" /></div>';
		else $output .= '
				<input type="hidden" name="editConfig" value="1">';
		$output .= '
			</form>';
		if (_PS_VERSION_ < "1.5.0.0")$output .= '
		</div>';
		return $output;
	}
}

?>