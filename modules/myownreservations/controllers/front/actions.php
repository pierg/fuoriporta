<?php
/*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @since 1.5.0
 */
class myownreservationsactionsModuleFrontController extends ModuleFrontController
{
	/**
	 * @var int
	 */
	public $id_product;
	public $obj;

	public function init()
	{
		parent::init();
		$this->obj = Module::getInstanceByName('myownreservations');
		$this->addJquery();
		$method = strtolower(Tools::getValue('method', 'calendar'));
		if ($method == 'external')
			$this->processExternal();
	}

	public function postProcess()
	{
		global $cookie;
		$method = strtolower(Tools::getValue('method', 'calendar'));
		if ($method == 'add')
			$this->processAdd();
		else if ($method == 'tasks')
			$this->processTasks();
		else if ($method == 'icalendar')
			$this->processICal();
		else if ($method == 'calendar')
			$this->processCalendar();
		else if ($method == 'eexternal')
			$this->processExternal();
		else if ($method == 'updatekey')
			$this->processUpdateKey();
		//extenssion ajax in else for perf
		else 
			foreach ($this->obj->_extensions->getExtByType(extension_type::AJAX) as $ext) 
				if (strtolower($ext->name)==$method)
					echo $ext->ajax($this->obj, $cookie);
		exit;
	}
	
	public function processTasks()
	{
		myOwnResas::sync($this->obj);
		exit;
	}
	
		public function processICal()
	{
		$mainProduct=null;
		$id_product=Tools::getValue('id');
		$id_source = Tools::getValue('id_source', 0);
		$id_product_attribute = 0;
		$extsync = (bool)Configuration::get('MYOWNRES_SYNC_EXT');
		$betweensync = (bool)Configuration::get('MYOWNRES_SYNC_BETW');

		if ($id_product) $mainProduct = $this->obj->_products->getResProductFromProduct($id_product);
		if ($mainProduct!=null) {
			$startDate = strtotime('-1months');//time();
			$endDate = strtotime('+'.$mainProduct->reservationPeriod.'days');
			$resas = myOwnResas::getReservationsOnPeriod($this->obj, $mainProduct, $id_product, $id_product_attribute, $startDate, $endDate);

			$data="BEGIN:VCALENDAR\nVERSION:2.0\nCALSCALE:GREGORIAN\nPRODID:-//myOwnReservation, ".Configuration::get('PS_SHOP_DOMAIN')."//EN\n";
			foreach ($resas as $resa) 
				if ($resa->validated && ($resa->id_source==0 || ($betweensync && $resa->id_source!=$id_source)) && ($resa->id_order || $extsync)){
				$resa = myOwnResas::populateReservation($resa);
				$enddate=strtotime($resa->endDate);
				if ($resa->startTime == $resa->endTime)
					$enddate=strtotime('+1day', $enddate);
				$data.="BEGIN:VEVENT\nUID:".$resa->reference."\nDTSTAMP:".date('Ymd')."T".date('His')."Z\nDTSTART:".date('Ymd', strtotime($resa->startDate))."T".str_ireplace(':', '', $resa->startTime)."Z\nDTEND:".date('Ymd', $enddate)."T".str_ireplace(':', '', $resa->endTime)."Z\nSUMMARY:".$resa->_customerName."\nEND:VEVENT\n";
			}
			

			$data.="END:VCALENDAR\n";
			
			header("Content-type:text/calendar");
	        header('Content-Disposition: attachment; filename="product_'.$id_product.'.ics"');
	        Header('Content-Length: '.strlen($data));
	        Header('Connection: close');
	        echo $data;
        
		}
		
		

		exit;
	}
	
	/**
	 * Remove a favorite product
	 */
	public function processCalendar()
	{
		global $cookie;
		global $smarty;
		$type = strtolower(Tools::getValue('type', 'product'));
		if (!Tools::getIsSet('id_product') && $type=="product") die('Indicate product id');

		

				if ($type=="product") {
					echo '<div id="primary_block" test="'.$type.'">'.myOwnReservationsCalendarController::displayProduct($this->obj, $cookie, $smarty, $type).'</div>';
				} else if ($type=="home") echo myOwnReservationsCalendarController::displayHome($this->obj, true);
				else if ($type=="column") echo myOwnReservationsCalendarController::displayWidget($this->obj, true); //displayWidget($this->obj, $cookie, $smarty);
				else if ($type=="topcolumn") echo myOwnReservationsCalendarController::displayTopWidget($this->obj, true);
		    exit;
	}
	
	public function processUpdateKey()
	{
		$old_key = trim(strtolower(Tools::getValue('old', '')));
		$new_key = trim(strtolower(Tools::getValue('new', '')));
		$saved_key = trim(strtolower(Configuration::get('MYOWNR_KEY')));

		if ($old_key==$saved_key) {
			Configuration::updateValue('MYOWNR_KEY', $new_key);
			echo 'key set to :'.$new_key;
		} else echo 'bad old key:'.$old_key.' '.$saved_key;
		exit;
	}
	
	
	/**
	 * Add a favorite product
	 */
	public function processExternal()
	{
		global $cookie;
		$isproversion=is_file(_PS_MODULE_DIR_ .$this->obj->name."/pro/products.php");
		$employee = new Employee($cookie->id_employee);
			$rights = Profile::getProfileAccess($employee->id_profile, Tab::getCurrentTabId());
			$reschedule=null;
			$actions=array();
			$productsFilter=array();
		$carriers = MyOwnReservationsUtils::listCarriers();
		$payment_modules = array();
		foreach (PaymentModule::getInstalledPaymentModules() as $p_module)
			$payment_modules[] = Module::getInstanceById((int)$p_module['id_module']);
			$order=null;
		$resa = new myOwnReservation();
		
		$this->display_column_left = false;
		$this->display_column_right = false;
		$this->display_header_javascript = true;
		$this->display_header = true;
		$this->display_footer = false;
		$this->addJquery();
		$this->setMedia();
		//require_once(_PS_ADMIN_DIR_.'/header.inc.php');
		
		/*echo '<link href="'._MODULE_DIR_.'myownreservations/views/css/back.css" rel="stylesheet" type="text/css" media="all" />
		<script src="'._MODULE_DIR_.'myownreservations/views/js/back.js"></script>';*/
        $html = myOwnReservationsProController::editOrderDetails($this->obj, $carriers, $payment_modules, $resa, $productsFilter);
        $html += myOwnReservationsReservationsController::displayProductDetails($this->obj, $cookie, $resa, $order, $productsFilter);
		if ($isproversion && $resa->_saved && !$factorized) {
			$html += myOwnReservationsProController::editStockDetails($this->obj, $resa, $rights);
		}
		$html+= myOwnReservationsReservationsController::editReservationDetails($this->obj, $cookie, $resa, $reschedule, $actions);

Context::getContext()->smarty->assign(array(
    'content_only' => true,
    'HOOK_HEADER' => '',
    'template' => ''
));
return $this->display();
			
	}

	/**
	 * Add a favorite product
	 */
	public function processAdd()
	{
		$product = new Product($this->id_product);
		// check if product exists
		if (!Validate::isLoadedObject($product) || FavoriteProduct::isCustomerFavoriteProduct((int)Context::getContext()->cookie->id_customer, (int)$product->id))
			die('1');
		$favorite_product = new FavoriteProduct();
		$favorite_product->id_product = $product->id;
		$favorite_product->id_customer = (int)Context::getContext()->cookie->id_customer;
		$favorite_product->id_shop = (int)Context::getContext()->shop->id;
		if ($favorite_product->add())
			die('0');
		die(1);
	}
}