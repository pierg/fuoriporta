<?php
/*
* 2010-2012 LaBulle All right reserved
*/
/*if (!class_exists('TCPDF'))
	require_once(MYOWNRES_PATH.'library/tcpdf/tcpdf.php');*/
	
class myOwnReservationsPlanningController
{
	
	//-----------------------------------------------------------------------
	//    DISPALYcurrentViewDate
	//-----------------------------------------------------------------------
	public static function displayPeriodView($obj, $productFamillyFilter, $view='month', $producrs=array())
	{
		global $cookie;
		$obj->checkIsProVersion();
		$dateStr='';
		$out = '';
		$unitsIds = $obj->_products->getUnitsFamiliesIds($obj);
		
		if ($view!='') {
			$prev='scheduler._click.dhx_cal_prev_button();';
			$next='scheduler._click.dhx_cal_next_button();';
			$btns = array('month' => $obj->l('Month', 'planning'), 'timeline' => $obj->l('Timeline', 'planning'), 'week' => $obj->l('Week', 'planning'), 'grid' => $obj->l('Grid', 'planning'));
			//if ($productFamillyFilter!='' && array_key_exists($productFamillyFilter, $obj->_products->list) && $obj->_products->list[$productFamillyFilter]->productType == product_type::PLACE) {
				$btns['unit'] = $obj->l('Day', 'planning');
			//}
			$out = '
					<div class="btn-group" style="margin-left:20px;margin-top:-4px">';
			foreach ($btns as $btnKey => $btnVal)
			{
				$out .= '
					<a id="'.$btnKey.'_button" onclick="planningMode(\''.$btnKey.'\');" class="btn-period btn btn-default '.($view == $btnKey ? 'btn-primary' : '').'" view="'.strtolower($btnKey).'" '.($btnKey== 'unit' && ($productFamillyFilter=='' || !array_key_exists($productFamillyFilter, $unitsIds)) ? 'style="display:none"' : '').'>
						'.$btnVal.'
					</a>';
			}
			
			$out .= '</div>';
		}
		else {
			$prev='$(\'#submitDateDayPrev\').click();';
			$next='$(\'#submitDateDayNext\').click()';
			$out .= myOwnUtils::insertDatepicker($obj, array('datepickerDate'), false, 'dateFormat: "yy-mm-dd", onSelect: submitDatePickerStock');
			$day = myOwnCalendarWeek::getDayOfWeek($cookie->myown_dayOfWeek, $cookie->myown_weekOfYear, $cookie->myown_year);
			$dateStr=date('Y-m-d', $day);
			$out .= '
			<form action="'.myOwnReservationsController::getConfUrl('stock').'" method="post" style="display:inline-block">
			<input style="width:25px;background:#FFF6D3 3px 3px no-repeat url(\'../img/admin/previous.gif\');display:none" type="submit" name="submitDateDayPrev" id="submitDateDayPrev" class="button" value="">
	    	<input style="display:none" type="submit" name="submitDateDayNext" id="submitDateDayNext" class="button" value=""></form>';
		}
		$links = '';//MyOwnUtils::insertDatepicker(array('datepickerDate'));
		$out .= '
		<script type="text/javascript"> 
		var unitsIds = '. Tools::jsonEncode($unitsIds).';
		/*function submitDatePickerPlanning(dateText) {
			document.location.href=\''.myOwnReservationsController::getAdminUrl('view='.$view).'&day=\'+dateText;
		}*/
		function submitDatePickerStock(dateText) {
			document.location.href=\''.myOwnReservationsController::getConfUrl('stock').'&day=\'+dateText;
		}
		function planningMode(btn_view) {
			planningDisplayed = '.(($view=="month" or $view=="week" or $view=="day") ? 'true' : 'false' ).';
			if (planningDisplayed) scheduler.setCurrentView(currentViewDate, btn_view);
			else document.location.href="'.myOwnReservationsController::getAdminUrl('').'#date='.date("Y-m-d").',mode="+btn_view;
			$(".btn-period").each(function(index) {
				if ($(this).attr("view")==btn_view) 
					$(this).addClass("btn-primary");
				else $(this).removeClass("btn-primary");
			});
		}
		</script>';

		$links .= '
		<div style="display:inline-block">
			<h4 style="margin-left:20px;display:inline-block;text-transform: none;">
				<i class="icon-calendar"></i>
				'.$obj->l('Period', 'planning').'
			</h4>
			<input style="display:inline-block;width:100px;height:26px;" size="10" type="text" name="datepickerFrom" id="datepickerDate" value="'.$dateStr.'">
			<span class="btn-group" style="margin-top:-4px">
				<a onclick="'.$prev.'" class="btn btn-default "><i class="icon-backward"></i></a>
				<a onclick="'.$next.'" class="btn btn-default "><i class="icon-forward"></i></a>
			</span>
		</div>';
			
		if ($producrs!=array()) { 
			/*$productVal=Tools::getValue('product',0);
			$links .= '<h4 style="margin-left:20px;display:inline-block;text-transform: none;">
				<i class="icon-tags"></i>
				'.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRODUCT]).'
			</h4> ';
	            $links .= '<div style="display:inline-block;width:100px;">'.MyOwnReservationsUtils::insertProductPicker($obj, $cookie->id_lang, "product", $productVal, 0, 0, "height:24px;width:100px;font-size:12px;background-color:#FFF;", "filterChange($(this).val());", 0, "", true, '').'</div>';*/
		} elseif ( $obj->isProVersion) { 
			$links .= '<h4 style="margin-left:20px;display:inline-block;text-transform: none;">
				<i class="icon-tags"></i>
				'.ucfirst(myOwnLang::$object[MYOWN_OBJ::FAMILY]).'
			</h4> ';
	            $links .= '<div style="display:inline-block;width:80px;">'.MyOwnReservationsUtils::insertProductFamillyPicker($obj, $cookie->id_lang, "productFamillyFilter", "dhx_cal_extra", "height:24px;width:80px;font-size:12px;background-color:#FFF;", $productFamillyFilter, ($view != '' ? "filterChange($(this).val());" : "submitProdFamilyStock();"), false).'</div>';
	        if ($view!='') {
	        $links .= '<h4 style="margin-left:20px;display:inline-block;text-transform: none;">
				<i class="icon-barcode"></i>
				'.ucfirst(myOwnLang::$object[MYOWN_OBJ::STOCK]).'
			</h4> ';
	            $links .= '<div style="display:inline-block;width:80px;">'.myOwnReservationsProController::stockItemSelect($obj).'</div>';
	        }
		}
		$ext_object=null;
		if ($view!='')
		foreach ($obj->_extensions->getExtByType(extension_type::OBJECT) as $ext_object) 
			if (method_exists($ext_object, "objectSelect")) {
				$links .= '<div style="display:inline-block" class="object_filter" id="object_filter_'.$ext_object->id.'"><h4 style="margin-left:20px;display:inline-block;text-transform: none;">
					<i class="icon-'.$ext_object->icon.'"></i>
					'.$ext_object->label.'
				</h4> ';
		            $links .= '<div style="display:inline-block;width:80px;">'.$ext_object->objectSelect($obj, $cookie->id_lang, 'objectFilter', '', false).'</div></div>';
			}
		return $out.$links;
	}
	
	public static function display($obj, $admin, $productFamillyFilter, $productsFilter=array())
	{
		global $cookie, $rights;
		if(!isset($_SESSION)) session_start();
		if (isset($_GET["goupedView"])) $cookie->goupedView = $_GET["goupedView"];
		$isfront=($productsFilter!=array());
		$obj->checkIsProVersion();
		$link= new Link();
		$iso = strtolower(Language::getIsoById($cookie->id_lang));
		$employee = new Employee($cookie->id_employee);
		$_products = $obj->_products;
		if ($employee->id_profile>1) {
			$_products->list = $_products->getListForEmployee($employee->id);
		}
		$obj->_products = $_products;
		
		$output= '';
		$output .= '
		<script src="'._MODULE_DIR_.$obj->name.'/views/js/dhtmlxscheduler.js" type="text/javascript" charset="utf-8"></script>
		<script src="'._MODULE_DIR_.$obj->name.'/views/js/dhtmlxscheduler_limit.js"></script>
		<script src="'._MODULE_DIR_.$obj->name.'/views/js/dhtmlxscheduler_timeline.js" type="text/javascript" charset="utf-8"></script>
		<script src="'._MODULE_DIR_.$obj->name.'/views/js/dhtmlxscheduler_treetimeline.js" type="text/javascript" charset="utf-8"></script>
		<script src="'._MODULE_DIR_.$obj->name.'/views/js/dhtmlxscheduler_units.js" type="text/javascript" charset="utf-8"></script>
		<script src="'._MODULE_DIR_.$obj->name.'/views/js/dhtmlxscheduler_year_view.js" type="text/javascript" charset="utf-8"></script>
		<script src="'._MODULE_DIR_.$obj->name.'/views/js/dhtmlxscheduler_grid_view.js" type="text/javascript" charset="utf-8"></script>
		<script src="'._MODULE_DIR_.$obj->name.'/views/js/dhtmlxscheduler_url.js" type="text/javascript" charset="utf-8"></script>
		<script src="'._MODULE_DIR_.$obj->name.'/views/js/dhtmlxscheduler_serialize.js" type="text/javascript" charset="utf-8"></script>
		<script src="'._MODULE_DIR_.$obj->name.'/views/js/dhtmlxscheduler_pdf.js" type="text/javascript" charset="utf-8"></script>
		
		'.(_PS_VERSION_ < "1.5.0.0" ? '<script src="'._PS_JS_DIR_.'jquery/jquery.fancybox-1.3.4.js" type="text/javascript" charset="utf-8"></script><link rel="stylesheet" href="'._PS_CSS_DIR_.'jquery.fancybox-1.3.4.css" type="text/css" charset="utf-8">' :
		'<script src="'._PS_JS_DIR_.'jquery/plugins/fancybox/jquery.fancybox.js" type="text/javascript" charset="utf-8"></script><link rel="stylesheet" href="'._PS_JS_DIR_.'jquery/plugins/fancybox/jquery.fancybox.css" type="text/css" charset="utf-8">').'
		
		<link rel="stylesheet" href="'._MODULE_DIR_.$obj->name.'/views/css/dhtmlxscheduler_dhx_terrace.css" type="text/css" charset="utf-8">
		<link rel="stylesheet" href="'._MODULE_DIR_.$obj->name.'/views/css/dhtmlxscheduler_'.(!$isfront ? 'labulle': 'front').'.css" type="text/css" charset="utf-8">
		';
		if ($iso!='en') $output .= '
		<script src="'._MODULE_DIR_.$obj->name.'/views/js/locale/locale_'.$iso.'.js" type="text/javascript" charset="utf-8"></script>';
		
		$output .= myOwnUtils::insertDatepicker($obj, array('datepickerDate'), false, 'dateFormat: "yy-mm-dd", onSelect: submitDatePickerPlanning'); //
		$imgSize = new ImageType(1);
		if ($productFamillyFilter!="" && $productsFilter==array()) {
			$prodcutFamilly= $obj->_products->list[$productFamillyFilter];
			$reservationStart = $prodcutFamilly->getReservationStart();
			$reservationEnd = strtotime("+".$prodcutFamilly->reservationPeriod." day",$reservationStart);
			$products=$prodcutFamilly->getProducts($cookie->id_lang);
		} else {
			$prodcutFamilly=null;
			$reservationStart=time();
			$reservationEnd=time();
			$products= $obj->_products->getProducts($cookie->id_lang, false, $productsFilter);
		}

		//---------product list on product view------------
		$yItems=array();
		$productsIds=array();
		foreach ($products as $product)
			$productsIds[$product['id_product']]=$product['id_product'];
		$productsImages=MyOwnReservationsUtils::getProductsImages($productsIds, $cookie->id_lang);
		$mediumSize=new ImageType(1);
		foreach($products as $product) {
			$tempArray=array();
			if (array_key_exists($product['id_product'], $productsImages)) {
				$viewedProduct=$productsImages[$product['id_product']];
				$image = $link->getImageLink($viewedProduct->link_rewrite, $viewedProduct->cover, $mediumSize->name);
			} else $image='';
			$tempMainProduct = $obj->_products->getResProductFromProduct($product['id_product']);
			$attributes = MyOwnReservationsUtils::getProductAttributes($product['id_product']);
			if ($tempMainProduct!=null && $tempMainProduct->productType!=product_type::VIRTUAL && count($attributes)>0 && $attributes[0]>0) {
				$tempSubArray=array();
				foreach($attributes as $attribute) {
					$tempElem = array();
					$tempElem['key']=$product['id_product'].'-'.$attribute;
					$tempElem['label'] = '<img style="float:left;padding-right:3px" width="30" height="30" src="'.(_PS_VERSION_ < "1.5.0.0" ? '' : 'http://').$image.'"><div style="text-align:left;height:100%">'.MyOwnReservationsUtils::getAttributeCombinaisons($product['id_product'],$attribute,$cookie->id_lang).'</div><div style="clear:both"></div>';
					$tempSubArray[] = $tempElem;
				}
				$tempArray['label']=MyOwnReservationsUtils::getProductName($product['id_product'], $cookie->id_lang);
				$tempArray['open'] = true;
				$tempArray['children'] = $tempSubArray;
			} else {
				$tempArray['label'] = '<img style="float:left;padding-right:3px" width="30" height="30" src="'.(_PS_VERSION_ < "1.5.0.0" ? '' : 'http://').$image.'"><div style="text-align:left"><br><b>'.MyOwnReservationsUtils::getProductName($product['id_product'], $cookie->id_lang).'</b></div><div style="clear:both"></div>';
			}
			$tempArray['key']=$product['id_product'].'-0';
			$yItems[]=$tempArray;
		}
		$tempArray=array();
		$tempArray['label'] = "";
		$tempArray['key'] = "0";
		$yItems[]=$tempArray;

		$availabilities = new myOwnAvailabilities();
		$availabilitiesAdded=array();
		$markeds=array();
		
		//--------full unavailabilities-----------
		if (count($availabilities->list)<50)
		foreach ($availabilities->list  as $availability)
			if ($availability->id_family==0 && $availability->type==1 && $availability->id_product >= 0) {
				$marked=array();
				
				if ($availability->id_product > 0) {
					$sections=array();
					$sections['timeline']=array();
					$attributes = MyOwnReservationsUtils::getProductAttributes($availability->id_product);
					if (count($attributes)<2) $sections['timeline'][] = $availability->id_product.'-0';
					else {
						foreach($attributes as $attribute)
							$sections['timeline'][]=$availability->id_product.'-'.$attribute;
					}
					$marked['sections'] = $sections; 
				}
				$day=strtotime("-20 years");
				$marked['start_date']='new Date('.date('Y',$day).', '.(date('m',$day)-1).', '.date('d',$day).')';
				$day=strtotime("+20 years");
				$marked['end_date']='new Date('.date('Y',$day).', '.(date('m',$day)-1).', '.date('d',$day).')';
				$marked['css']='unavailable';
				$marked['invert_zones']=true;
				$markeds[]=$marked;
			}
			
		//partial unavailabilities or availabilities
		if (count($availabilities->list)<50)
		foreach ($availabilities->list  as $availability) 
			if ($availability->id_family==0 && $availability->id_product>=0) {
			$days=MyOwnCalendarTool::getDaysBetween(strtotime($availability->startDate), strtotime($availability->endDate));
			foreach ($days  as $day) {
				$marked=array();
				$marked['days']='new Date('.date('Y',$day).', '.(date('m',$day)-1).', '.date('d',$day).')';
				
				if ($availability->type==1) $marked['css']='available_color';
				else $marked['css']='unavailable';
				$startDelay = ($day == strtotime($availability->startDate) && $availability->startTimeslot);
				$endDelay =  ($day == strtotime($availability->endDate) && $availability->endTimeslot);
				if ($startDelay or $endDelay) {
					$zones=array();
					if ($startDelay)
						$zones[]=($availability->getStart($obj->_timeSlots->list)-$day)/60;
					else $zones[]=0;
					
					if ($endDelay)
						$zones[]=($availability->getEnd($obj->_timeSlots->list)-$day)/60;
					else $zones[]=24*60;

				} else $zones='fullday';
				
				$marked['zones']=$zones;
				if ($availability->id_product > 0) {
					$sections=array();
					$sections['timeline']=array();
					$attributes = MyOwnReservationsUtils::getProductAttributes($availability->id_product);
					if (count($attributes)<2) $sections['timeline'][] = $availability->id_product.'-0';
					else {
						foreach($attributes as $attribute)
							$sections['timeline'][]=$availability->id_product.'-'.$attribute;
					}
					$marked['sections']=$sections; //$timeline;
				}
				$markeds[]=$marked;
			}
		}
		//foreach ($obj->_products->list as $prodFamily) echo '@'.$prodFamily->sqlId.'='.$prodFamily->getProductsCount();
		//partial unavailabilities or availabilities
		$markeds_family = array();
		foreach ($obj->_products->list as $prodFamily)
			foreach ($availabilities->list  as $availability) 
				if ($availability->id_family==$prodFamily->sqlId && ($availability->id_product==0 || $prodFamily->getProductsCount()==1)) {
				$days=MyOwnCalendarTool::getDaysBetween(strtotime($availability->startDate), strtotime($availability->endDate));
				foreach ($days  as $day) {
					$marked = array();
					$marked['days']='new Date('.date('Y',$day).', '.(date('m',$day)-1).', '.date('d',$day).')';
					$marked['id_object']=$availability->id_object;
					if ($availability->type==1) $marked['css']='available_color';
					else $marked['css']='unavailable';
					$startDelay = ($day == strtotime($availability->startDate) && $availability->startTimeslot);
					$endDelay =  ($day == strtotime($availability->endDate) && $availability->endTimeslot);
					if (MYOWNRES_WEEK_TS
						&& $prodFamily->_reservationUnit==reservation_unit::WEEK
						&& $availability->startTimeslot
						&& array_key_exists($availability->startTimeslot, $obj->_timeSlots->list)) {
							$startDelay = true;$endDelay = true;
							$startZone = strtotime(date('Y-m-d', $day).' '.$obj->_timeSlots->list[$availability->startTimeslot]->startTime);
							$endZone = strtotime(date('Y-m-d', $day).' '.$obj->_timeSlots->list[$availability->startTimeslot]->endTime);
					} else {
						$startDelay = ($day == strtotime($availability->startDate) && $availability->startTimeslot);
						$startZone = $availability->getStart($obj->_timeSlots->list);
						$endDelay = ($day == strtotime($availability->endDate) && $availability->endTimeslot);
						$endZone = $availability->getEnd($obj->_timeSlots->list);
					}
					if ($startDelay or $endDelay) {
						$zones = array();
						if ($startDelay)
							$zones[] = ($startZone-$day)/60;
						else $zones[] = 0;
						
						if ($endDelay)
							$zones[] = ($endZone-$day)/60;
						else $zones[] = 24*60;
	
					} else $zones='fullday';
					
					$marked['zones']=$zones;
				
					$markeds_family[$prodFamily->sqlId][] = $marked;
					
				}
			}
		
		//------------ext objects-------------
		$ext_object=null;
		$units=array();
		foreach ($obj->_extensions->getExtByType(extension_type::OBJECT) as $ext) 
			if (method_exists($ext, "objectSelect"))
				$ext_object = $ext;
		if ($ext_object!=null) {
			foreach ($ext_object->objects->list as $key => $ext_obj_item) {
				$name = $ext_obj_item->name;
				$units[$key] = array();
				$stockelems = $obj->_stocks->getStockForProduct(-$key);
				foreach ($stockelems as $stockelem)
					$units[$key][] = array('key' => $stockelem->serial, 'label' => $stockelem->name);
			}
		} else {
			$prodsIds=array();
			foreach ($obj->_products->list as $family)
				$prodsIds[$family->sqlId] = $family->getProductsId();

			foreach ($obj->_stocks->list as $key => $ext_obj_item) {
				//print_r($ext_obj_item);
				$name = $ext_obj_item->name;
				$prodFamily=0;
				if ($ext_obj_item->id_product)
					foreach($prodsIds as $family_id => $family_prods)
						if (in_array($ext_obj_item->id_product, $family_prods)) 
							$prodFamily = $family->sqlId;
				if (!isset($units[$prodFamily]))
					$units[$prodFamily] = array();
				
				//$stockelems = $obj->_stocks->getStockForProduct(-$key);
				//foreach ($stockelems as $stockelem)
					$units[$prodFamily][] = array('key' => $ext_obj_item->serial, 'label' => ($ext_obj_item->name != '' ? $ext_obj_item->name : $ext_obj_item->serial ));
			}
		}
		
		$views='';
		$btns=self::displayPeriodView($obj, $productFamillyFilter, 'month', $productsFilter);
		if (_PS_VERSION_ >= '1.6.0.0' && !$isfront)
			$output .= MyOwnReservationsUtils::table16wrap($obj, 'reservations', "$('#addDelivery').toggle();showNewResa();", ucfirst(myOwnLang::$objects[MYOWN_OBJ::RESV]), -1, $btns);
		if ($isfront) {
			$output .= '<div class="box">'.ucfirst(myOwnLang::$objects[MYOWN_OBJ::RESV]).' '.$btns.'</div>';
		}
		if (_PS_VERSION_ >= "1.6.0.0" && $productsFilter==array())
			$views = myOwnReservationsAdminController::showPlanningMenu($obj, 'month', 'left:340px;margin:-8px');
		$output .= '
		<div id="scheduler_here" class="dhx_cal_container" style="width:100%; height:725px;">';
        if (_PS_VERSION_ < '1.6.0.0') {
        	$output .= '
	        <div class="dhx_cal_navline">
	            <div class="dhx_cal_prev_button" style="left:100px">&nbsp;</div>
	            <div class="dhx_cal_next_button" style="left:147px">&nbsp;</div>
	            <input style="float:left;margin:4px;height:18px;padding:2px;width:90px;-webkit-box-sizing:initial;-moz-box-sizing:initial;
	box-sizing: initial;" size="10" type="text" name="datepickerFrom" id="datepickerDate" class="datepicker admin_extra_button" value="">';
	            if ($obj->isProVersion) $output .= '
	            <div style="left:220px;width:200px;">'.MyOwnReservationsUtils::insertProductFamillyPicker($obj, $cookie->id_lang, "productFamillyFilter", "dhx_cal_extra", "height:24px;width:100px;font-size:12px;background-color:#FFF;", $productFamillyFilter, "filterChange($(this).val());", false).'</div>';
	            $output .= $views.'
	            <div class="dhx_cal_date" style="'.(_PS_VERSION_ >= "1.6.0.0" ? 'margin-left:100px' : '').'"></div>
	        </div>';
        } else $output .= '<div class="dhx_cal_navline"><div class="dhx_cal_date" style="display:none"></div></div>';
        $output .= '
        <div class="dhx_cal_header">
        </div>
        <div class="dhx_cal_data">
        </div>       
        </div>
        <div style="float:right">
        	<div>
        '.myOwnUtils::getBoolField($obj, 'goupedView', Tools::getValue('goupedView', $cookie->goupedView)==1, $obj->l('Group reservations by product', 'planning'), 'width:250px').'
        	</div>
        </div>
        <div style="clear:both"></div>
        '.myOwnReservationsReservationsController::getScript($obj).'
        <script type="text/javascript">
        	function print() {
        		if (scheduler._mode=="grid") 
        			window.window.open(ajaxAdmin+\'&action=printGrid&date=\'+myOwnConvertDate(currentViewDate), "_blank");
        		else scheduler.toPDF(ajaxAdmin);
        	}
        	function exportPeriod() {
        		document.location.href = ajaxAdmin+\'&action=exportPlanning&date=\'+myOwnConvertDate(currentViewDate)+\'&mode=\'+currentViewMode;
        	}
        	var currentViewDate;
        	var currentViewMode="month";
        	var currentProductFamily=0;
        	var stockFilter="";
        	var objectFilter="";
        	var assignmtFilter="";
        	var html_regexp = new RegExp("<[^>]*>", "g");
	        var myOwnConvert = scheduler.date.date_to_str(scheduler.config.default_date);
	        var myOwnConvertTime = scheduler.date.date_to_str("%H:%i");
	        var myOwnConvertDate = scheduler.date.date_to_str("%Y-%m-%d");
	        
	        var baseDir = \''.__PS_BASE_URI__.'\';
	       
	        var imgDir = \''._PS_IMG_.'\';
	        var imageSize = '.intval($imgSize->height).';
	        var timeLineLen='.(_PS_VERSION_ < "1.5.0.0" ? 7 : 14).';
	        var translations = new Array();
	        translations["col_id"] = "'.$obj->l('Id','planning').'";
	        translations["col_ref"] = "'.$obj->l('Ref','planning').'";
	        translations["col_start"] = "'.$obj->l('Start','planning').'";
	        translations["col_end"] = "'.$obj->l('End','planning').'";
	        translations["col_order_id"] = "'.ucfirst(myOwnLang::$object[MYOWN_OBJ::ORDER]).'";
	        translations["col_order_status"] = "'.$obj->l('Order status','planning').'";
	        translations["col_amount"] = "'.$obj->l('Amount','planning').'";
	        translations["col_customer"] = "'.$obj->l('Customer','planning').'";
	        translations["col_product"] = "'.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRODUCT]).'";
	        translations["col_quantity"] = "'.$obj->l('Qty','planning').'";
	        translations["col_validated"] = "'.myOwnLang::getOperation($obj, MYOWN_OPE::VALIDATE, false).'";
	        translations["col_comment"] = "'.$obj->l('Comment','planning').'";
	        translations["label_val"] = "'.myOwnLang::getOperation($obj, MYOWN_OPE::VALIDATE, false).'";
	        translations["label_unval"] = "'.$obj->l('Awaiting','planning').'";
	        translations["label_order0_short"] = "'.$obj->l('Ext','planning').'";
	        translations["label_order0"] = "'.$obj->l('External','planning').'";
	        translations["label_order_title"] = "'.$obj->l('Order N°','planning').'";
	        translations["label_order_title_short"] = "'.$obj->l('N°','planning').'";';
	        foreach (myOwnLang::$sync_platforms AS $platform_key => $platform_label)
	        	$output .= 'translations["label_order_source_'.$platform_key.'"] = "'.$platform_label.'";';
	        
	        $output .= '
	        var units = '.Tools::jsonEncode($units).';
	        var unitsObject = "'.($ext_object != null ? $ext_object->title : 'stock').'";
	        var allProductsList = '.Tools::jsonEncode($yItems).';
	        var productsList = '.Tools::jsonEncode($yItems).';
	        var familyMarked = new Array();
	        var productFamilyHours=new Array();
	        var productFamilyProducts=new Array();';
			foreach ($obj->_products->list as $prodFamily) {
				$prodFamily->_timeslotsObj->list = $prodFamily->getTimeslots(true);
				$productsIds=$prodFamily->getProductsId();
				$productsIdList = implode(',',$productsIds);
				$first = $prodFamily->_timeslotsObj->getFirst();
				$last = $prodFamily->_timeslotsObj->getLast();
				if ($prodFamily->reservationSelType==reservation_unit::TIMESLOT || ($prodFamily->reservationSelType==reservation_unit::WEEK && MYOWNRES_WEEK_TS)) {
					if ($first!=null) $firstStartTimeTab = explode(':', $first->startTime);
					if ($last!=null) $lastStartTimeTab = explode(':', $prodFamily->_timeslotsObj->getLast()->endTime);
				} else {
					$firstStartTimeTab = explode(':', $prodFamily->_reservationStartTime);
					$lastStartTimeTab = explode(':', $prodFamily->_reservationEndTime);
				}
				
				$output .= "
				familyMarked[".$prodFamily->sqlId.'] = new Array();
				productFamilyProducts['.$prodFamily->sqlId.']="'.$productsIdList.'".split(\',\');
				productFamilyHours['.$prodFamily->sqlId.']=new Array();';
				if ($prodFamily->reservationSelType==reservation_unit::TIMESLOT || ($prodFamily->reservationSelType==reservation_unit::WEEK && MYOWNRES_WEEK_TS))
					$output .= "
				productFamilyHours[".$prodFamily->sqlId.'][0]='.($first!=null ? (int)$firstStartTimeTab[0] : '0').";
				productFamilyHours[".$prodFamily->sqlId.'][1]='.($last!=null ? (int)$lastStartTimeTab[0] : '0').';';
				else $output .= "
				productFamilyHours[".$prodFamily->sqlId.'][0]='.(int)$firstStartTimeTab[0].";
				productFamilyHours[".$prodFamily->sqlId.'][1]='.(int)$lastStartTimeTab[0].';';
			}
			$output .= '
			
		    var globalMarked = new Array();
		    var familyMarkedTimeSpans = new Array();
		    if ($(window).width()<768) {
				$("#nav-sidebar").css({
				    transition: "0s",
				    width: 50
				});
									$("#content").css({
				    transition: "0s",
				    "margin-left": 55
				});
			}
			$(document).ready(function() {
				if ($(window).width()<768)
					$("#nav-sidebar").css("width", "50px");
		        preInitScheduler('.(_PS_VERSION_ < "1.6.0.0" ? 'false' : 'true').');
		        familyMarkedTimeSpans = '.str_replace(')",', '),', str_replace('"new Date(', 'new Date(', Tools::jsonEncode($markeds_family))).';
		        
			    ';
				foreach ($markeds as $marked)
				$output .= '
				globalMarked.push(scheduler.addMarkedTimespan('.str_replace(')",', '),', str_replace('"new Date(', 'new Date(', Tools::jsonEncode($marked))).'));';
				$output .= '
				scheduler.config.drag_create = '.($rights['add'] && (!$isfront || $obj->isProVersion) ? 'true' : 'false').';
				scheduler.config.drag_resize = scheduler.config.drag_move = '.($rights['edit'] ? 'true' : 'false').';
				scheduler.init("scheduler_here",null,"month");
				setInterval(function(){scheduler.load(ajaxAdmin,"json");},60000);
				scheduler.load(ajaxAdmin,"json");
				if (scheduler._mode=="unit" && ($("#productFamillyFilter").val()==0 || $("#objectFilter").val()==0))
					planningMode("month");
	
				$("#goupedView_on").change(function(e){
					window.location = pageAdmin+"&goupedView=1";
				});
				$("#goupedView_off").change(function(e){
					window.location = pageAdmin+"&goupedView=0";
				});
			});

		</script>
        ';

		return $output;
	}
	
	static function printGrid($obj, $date, $productFamillyFilter='') {
    	global $cookie;
    	if (!class_exists('TCPDF'))
			require_once(MYOWNRES_PATH.'library/tcpdf/tcpdf.php');
    	$start = $end = strtotime($date);
    	
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('laBulle');
		$pdf->SetTitle(MyOwnCalendarTool::formatDate($start, $cookie->id_lang));
		
		// set default header data
		$pdf->SetHeaderData('', '', MyOwnCalendarTool::formatDate($start, $cookie->id_lang), '');
		
		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		
		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(10);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		
		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		
		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		    require_once(dirname(__FILE__).'/lang/eng.php');
		    $pdf->setLanguageArray($l);
		}
		
		// set font
		$pdf->SetFont('helvetica', '', 8);
		
		// add a page
		$pdf->AddPage();

		// data loading
		$obj = Module::getInstanceByName('myownreservations');
		
		$tbl='
		<table border="0" cellpadding="0" cellspacing="0">
			<thead>
				<tr style="background-color:#C2D5FC;color:#000000;">
					<td width="5%">'.$obj->l('Id', 'planning').'</td>
					<td width="20%">'.$obj->l('Period', 'planning').'</td>
					<td width="20%">'.$obj->l('Order Status', 'planning').'</td>
					<td width="17%">'.$obj->l('Customer', 'planning').'</td>
					<td width="25%">'.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRODUCT]).'</td>
					<td width="3%">'.$obj->l('Qty', 'planning').'</td>
					<td width="10%">'.$obj->l('Comment', 'planning').'</td>
				</tr>
			</thead>
			<tbody>';
			$odd=false;
		foreach ($obj->_products->list as $mainProduct) {
			if ($productFamillyFilter== $mainProduct->sqlId or $productFamillyFilter==0) {
				$resas = myOwnResas::getReservationsForProductFamilly($mainProduct, $start, $end, $debug=false);
				foreach ($resas as $resa) {
					myOwnResas::populateReservation($resa);
					$attribute_text=MyOwnReservationsUtils::getAttributeCombinaisons($resa->id_product, $resa->id_product_attribute, $cookie->id_lang);
					$odd=!$odd;
		$tbl.='
				<tr style="background-color:'.($odd ? '#DCE6F4' : 'FCFEFC').'">
					<td width="5%">'.$resa->sqlId.'</td>
					<td width="20%">'.str_ireplace('<br>', ' ', $resa->toString($obj, $cookie->id_lang, true)).'</td>
					<td width="20%">'.strtoupper(substr(myOwnUtils::getStatusName($resa->_orderStatus,$cookie->id_lang),0,22)).'</td>
					<td width="17%">'.$resa->_customerName.'</td>
					<td width="25%">'.MyOwnReservationsUtils::getProductName($resa->id_product, $cookie->id_lang).(trim($attribute_text)!='' ? '<br />'.$attribute_text : '').'</td>
					<td width="3%">'.$resa->quantity.'</td>
					<td width="10%">'.$resa->comment.'</td>
				</tr>';
				}
			}
		}
		$tbl.='
			</tbody>
		</table>';
		
		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output($date.'.pdf', 'I');
	}

	static function exportPlanning($obj) {
		global $cookie;
		$mode = $_GET['mode'];
		$start = strtotime($_GET['date']);
		switch ($mode) {
			case 'grid': 
				$end = $start;
				break;
			case 'week': 
				$end = strtotime('+1week -1day', $start);
				break;
			case 'timeline': 
				$end = strtotime('+2weeks -1day', $start);
				break;
			case 'month': 
				$start = strtotime(date('Y-m', $start).'-1');
				$end = strtotime('+1month -1day', $start);
				break;
		}
		
		$formatFile="csv";
		header('Content-type: text/csv; charset=iso-8859-1');
		if ($start==$end) {
			header('Content-Disposition: attachment; filename="'.date('Y-m-d',$start).'.csv"');
		} else {
			header('Content-Disposition: attachment; filename="'.date('Y-m-d',$start).'_'.date('Y-m-d',$end).'.csv"');
		}
			
		//$cart->id_lang = intval($cookie->id_lang);
		
		$_products = new myOwnProductsRules();
		
		$timeSlotFilter = Tools::getValue('deliveryTimeSlotFilter');
		$orderStatusFilter = Tools::getValue('orderStatusFilter');
		$deliverySubareaFilter = Tools::getValue('deliverySubareaFilter');
		$productFamillyFilter = Tools::getValue('productFamillyFilter',0);
		
		echo "Id;id_product;id_product_attribute;quantity;startDate;startTime;startTimeslot;endDate;endTime;endTimeslot;unit_price;tax_rate;discounts;id_order;orderStatus;deliveryId;customerId;customerName;customerPhone;customerAddressId;customerAddressStreet;customerAddressPostCode;customerAddressCity;validated;stock;message;id_shop;comment;".self::getOrderCheckoutFieldsTitle(intval($cookie->id_lang))."\r\n";
		foreach ($_products->list as $mainProduct) {
			if ($productFamillyFilter== $mainProduct->sqlId or $productFamillyFilter==0) {
				$resas = myOwnResas::getReservationsForProductFamilly($mainProduct, $start, $end, $debug=false);
				foreach ($resas as $resa) {
					myOwnResas::populateReservation($resa);
					echo $resa->sqlId.";".$resa->id_product.";".$resa->id_product_attribute.";".$resa->quantity.";".$resa->startDate.";".$mainProduct->_timeslotsObj->list[$resa->startTimeslot]->startTime.";".$resa->startTimeslot.";".$resa->endDate.";".$mainProduct->_timeslotsObj->list[$resa->endTimeslot]->endTime.";".$resa->endTimeslot.";".$resa->price.";".$resa->tax_rate.";".$resa->discounts.";".$resa->id_order.";".$resa->_orderStatus.";".$resa->_deliveryId.";".$resa->_customerId.";".$resa->_customerName.";".$resa->_customerPhone.";".$resa->_customerAddressId.";".$resa->_customerAddressStreet.";".$resa->_customerAddressPostCode.";".$resa->_customerAddressCity.";".$resa->validated.";".$resa->stock.";".self::getOrderMsg($resa->id_order).";".$resa->id_shop.";".$resa->comment.";".self::getOrderCheckoutFieldsValue($resa->id_order)."\r\n";
				}
			}
		}
	}
	
	static function getOrderMsg($orderId) {
		$msgs = Message::getMessagesByOrderId(intval($orderId));
		$comment = "";
		foreach($msgs as $msg)
			if (intval($msg['id_customer'])>0) $comment.=$msg['message'];
		return substr(str_replace("\n"," ",html_entity_decode($comment, ENT_QUOTES, 'UTF-8')),0,70);
	}
	
	static function getOrderCheckoutFieldsValue($orderId) {
		$query  = "SHOW TABLES LIKE '"._DB_PREFIX_."checkout_fields%';";
		$tables = Db::getInstance()->ExecuteS($query);
		if (count($tables)==0) return "";
		$query  = "SELECT * FROM `"._DB_PREFIX_."checkout_values` WHERE `id_order` = ".intval($orderId);
		$fields = Db::getInstance()->ExecuteS($query);
		$out="";
		foreach ($fields as $field) {
			if ($out!="") $out.=";";
			$out.=$field['field_value'];
		}
		return $out;
	}
	static function getOrderCheckoutFieldsTitle($id_lang) {
		$query  = "SHOW TABLES LIKE '"._DB_PREFIX_."checkout_fields%';";
		$tables = Db::getInstance()->ExecuteS($query);
		if (count($tables)==0) return "";
		$query  = "SELECT * FROM `"._DB_PREFIX_."checkout_fields_lang` WHERE `id_lang` = ".$id_lang;
		$fields = Db::getInstance()->ExecuteS($query);
		$out="";
		foreach ($fields as $field) {
			if ($out!="") $out.=";";
			$out.=$field['field_title'];
		}
		return $out;
	}
	
	static function supprimerAccent($cc)
	{
		$cc = str_replace(	array(
								'à', 'â', 'ä', 'á', 'ã', 'å',
								'î', 'ï', 'ì', 'í', 
								'ô', 'ö', 'ò', 'ó', 'õ', 'ø', 
								'ù', 'û', 'ü', 'ú', 
								'é', 'è', 'ê', 'ë', 
								'ç', 'ÿ', 'ñ', 'ý',
							),
							array(
								'a', 'a', 'a', 'a', 'a', 'a', 
								'i', 'i', 'i', 'i', 
								'o', 'o', 'o', 'o', 'o', 'o', 
								'u', 'u', 'u', 'u', 
								'e', 'e', 'e', 'e', 
								'c', 'y', 'n', 'y',
							),
							$cc
						);
		return $cc;				
	}
}
?>