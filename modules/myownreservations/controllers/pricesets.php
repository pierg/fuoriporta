<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnReservationsPriceSetsController {
	
	//==============================================================================================================================================
	//     SHOW PRICE SETS
	//==============================================================================================================================================

	public static function edit($cookie, $obj, $id_product, $productFamilly=null) {
		global $rights;
		$output='';
		$periods2=array();
		
		if ($productFamilly==null) $productFamilly = $obj->_products->getResProductFromProduct($id_product);
		if ($productFamilly->_reservationPriceType!=reservation_price::TABLE_TIME && $productFamilly->_reservationPriceType!=reservation_price::TABLE_DATE) {
			if ($id_product)
				$periodKind = array(reservation_price::TABLE_TIME => $productFamilly->getTimeUnits($cookie, $obj, $id_product));
			else {
				$periods2 = $productFamilly->getPeriodUnits($cookie, $obj);
				$periods = $productFamilly->getTimeUnits($cookie, $obj, $id_product);
				$periodKind = array(reservation_price::TABLE_TIME => $periods, reservation_price::TABLE_DATE => $periods2);
			}
		} else {
			if ($productFamilly->_reservationPriceType==reservation_price::TABLE_DATE)
				$periods = $productFamilly->getPeriodUnits($cookie, $obj);
			else $periods = $productFamilly->getTimeUnits($cookie, $obj, $id_product);
			$periodKind=array($productFamilly->_reservationPriceType => $periods);
		}
		
		$attributesPrice=array(0 => 0);
		
		$product = new Product($id_product, true);
		if ($id_product) $attributes = MyOwnReservationsUtils::getProductAttributes($id_product);
		else $attributes = array(0 => 0);

		$with_taxes = (Configuration::get('MYOWNRES_PRICE_TAXEXCL')!=1);
		if ($with_taxes) $tax_rate=$product->tax_rate;
		else $tax_rate=0;
		$isPricesSet = (myOwnPricesSets::isPricesSetForProduct($id_product));
		$output .= '
			<input type="checkBox" style="'.(_PS_VERSION_ >= "1.6.0.0" ? 'display:none' : '').'" id="priceSetBtn" onchange="if ($(this).attr(\'checked\')==\'checked\') $(\'#priceSetGrid\').show(); else $(\'#priceSetGrid\').hide()" name="pricesetProductId" value="'.$id_product.'" '.($isPricesSet && _PS_VERSION_ < "1.6.0.0" ?'checked' : '').'> <label style="float:none;font-weight:normal" for="advanced_stock_management">';
		if ($id_product>0) {
			$output .= '
			<b>'.($productFamilly->_reservationPriceType==reservation_price::PRODUCT_DATE ? $obj->l('Set unit price depending on period', 'pricesets') : $obj->l('Edit the', 'pricesets')).' '.myOwnLang::$reservPriceType[$productFamilly->_reservationPriceType].'</b>  ('.myOwnUtils::getCurrency().' '.($with_taxes ? $obj->l('with taxes', 'pricesets') : $obj->l('without taxes', 'pricesets')).')
				</label>
				<br /><br />
				<input type="hidden" name="pricesetProductPrice" value="'.Tools::ps_round(Product::getPriceStatic($id_product, $with_taxes),_PS_PRICE_DISPLAY_PRECISION_).'">
				<input type="hidden" name="pricesetProductTax" value="'.$product->tax_rate.'">';
				foreach($attributes as $attribute){
					$attributesPrice[$attribute] = Tools::ps_round(MyOwnReservationsUtils::getProductPriceTaxes($id_product, $attribute, $with_taxes, $productFamilly->_optionAlsoSell),_PS_PRICE_DISPLAY_PRECISION_);
					$output .= '
					<input type="hidden" name="o'.$attribute.'" value="'.$attributesPrice[$attribute].'">';
				}
		}
		foreach ($periodKind as $periodKindKey => $periodKindValues) {
				$output .= '
				<div style="overflow-x:scroll">
					<table style="width:100%;background-color:#FFFFFF;table-layout:fixed;'.(!$isPricesSet && _PS_VERSION_ < "1.6.0.0" ? 'display:none':'').'" id="priceSetGrid" class="table">
						<thead>
							<tr>
								<th width="100px">'.($periodKindKey==reservation_price::TABLE_DATE ? $obj->l('Period', 'pricesets') : $obj->l('Length', 'pricesets')).'</th>';
							foreach($periodKindValues AS $periodKey => $periodLabel) {
								$output .= '
								<th style="text-align:center;white-space:nowrap;" width="'.(count($periodKindValues)>12 ? '50px' : '').'">'.$periodLabel.'</th>';
							}
						$output .= '
							</tr>
						</thead>';
					foreach($attributes as $attribute){
						$output .= '
						<tr>
							<td style="text-align:left;">'.($attribute>0 ? MyOwnReservationsUtils::getAttributeCombinaisons($id_product,$attribute,$cookie->id_lang) : ($productFamilly->_reservationPriceType==reservation_price::TABLE_RATE ? $obj->l('Ratio', 'pricesets') : $obj->l('Price', 'pricesets'))).'</td>';
							if ($id_product) $setprices = myOwnPricesSets::getPricesSetForProduct($id_product, $attribute, $tax_rate, $with_taxes);
							else $setprices = myOwnPricesSets::getPricesSetForFamily($productFamilly, $periodKindKey);
							foreach($periodKindValues AS $periodKey => $periodLabel)
								$output .= '
							<td style="padding:2px;text-align:right;">
								<input type="text" class="" name="'.$periodKindKey.'_'.$attribute.'_'.$periodKey.'" value="'.(array_key_exists($periodKey, $setprices) ? Tools::ps_round($setprices[$periodKey],_PS_PRICE_DISPLAY_PRECISION_) : (!$id_product ? 1 : $attributesPrice[$attribute])).'" style="float:left;width:100%" onChange="$(this).attr(\'value\', $(this).val())">
								</td>';
						$output .= '
						</tr>';	
					}	
				$output .= '
					</table>
				</div>';
		}
		if (_PS_VERSION_ >= "1.6.0.0" && $id_product)  $output .= '<div class="panel-footer">
				<a href="index.php?controller=AdminProducts&token='.Tools::getAdminToken('AdminProducts'.(int)Tab::getIdFromClassName('AdminOrders').(int)$obj->context->cookie->id_employee).'" class="btn btn-default"><i class="process-icon-cancel"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::CANCEL]).'</a>
				<button type="submit" name="submitAddproduct"  onclick="$(\'#priceSetBtn\').attr(\'checked\',\'checked\')" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::SAVE]).'</button>
				<button type="submit" name="submitAddproductAndStay"  onclick="$(\'#priceSetBtn\').attr(\'checked\',\'checked\')" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> '.ucfirst(myOwnLang::$actions[MYOWN_OPE::SAVESTAY]).'</button>
			</div>';
		return $output;
				
	}
	
	// ADD AVAILABILITY
	//-----------------------------------------------------------------------------------------------------------------
	public static function editAction($obj, &$msg, $productFamilly=null) {
		global $cookie;
		$id_product = Tools::getValue('pricesetProductId', 0);
		$priceset = new myOwnPricesSet();
		$with_taxes = false;
		$res=true;

		if ($productFamilly==null && $id_product) $productFamilly = $obj->_products->getResProductFromProduct($id_product);

		if ($id_product) {
			$product = new Product($id_product, true);
			$priceset->id_product = $id_product;
			$with_taxes = (Configuration::get('MYOWNRES_PRICE_TAXEXCL')!=1);
		} else if ($productFamilly!=null) 
			$priceset->id_product = -$productFamilly->sqlId;
//echo '@'.	$priceset->id_product;	
		if ($id_product && $productFamilly->_reservationPriceType!=reservation_price::TABLE_RATE) $tax_rate = Tools::getValue('pricesetProductTax', $product->tax_rate);
		else $tax_rate=1;
		
		if ($id_product) $attributes = MyOwnReservationsUtils::getProductAttributes($id_product);
		else $attributes = array(0 => 0);

		if ($productFamilly != null && (isset($_POST['pricesetProductPrice']) || $id_product==0)) {
		//echo '#'.intval($productFamilly->_reservationPriceType==reservation_price::TABLE_RATE);
			if ($productFamilly->_reservationPriceType==reservation_price::TABLE_RATE) {
				if ($id_product)
					$periodKind = array(reservation_price::TABLE_TIME => $productFamilly->getTimeUnits($cookie, $obj, $id_product));
				else {
					$periods2 = $productFamilly->getPeriodUnits($cookie, $obj);
					$periods = $productFamilly->getTimeUnits($cookie, $obj, $id_product);
					$periodKind = array(reservation_price::TABLE_TIME => $periods, reservation_price::TABLE_DATE => $periods2);
				}
			} else {
				if ($productFamilly->_reservationPriceType==reservation_price::TABLE_DATE)
					$periods = $productFamilly->getPeriodUnits($cookie, $obj);
				else $periods = $productFamilly->getTimeUnits($cookie, $obj, $id_product);
				$periodKind=array($productFamilly->_reservationPriceType => $periods);
			}
			
			if ($id_product>0 || $productFamilly!=null) {
				$attributes = MyOwnReservationsUtils::getProductAttributes($id_product);

				foreach ($periodKind as $periodKindKey => $periodKindValues) {

					foreach($attributes as $attribute) {

						if ($id_product) {
							$priceset->id_product_attribute = $attribute;
							myOwnPricesSets::deletePricesSetForProduct($id_product, $attribute);
						} else {
							$priceset->id_product_attribute = $periodKindKey;
							myOwnPricesSets::deletePricesSetForProduct(-$productFamilly->sqlId, $periodKindKey);
						}
						foreach($periodKindValues AS $periodKey => $periodLabel) {
							if (Tools::getIsset($periodKindKey.'_'.$attribute.'_'.$periodKey)) {
								$editPriceField=Tools::getValue($periodKindKey.'_'.$attribute.'_'.$periodKey,0);

								if (preg_match( "@^\d*(\.\d*)?$@i" , $editPriceField)== 0 )
									$msg .= $obj->l('A amount must be formatted with a dot', 'pricessets');
								if (intval($editPriceField)>=100000000)
									$msg .= $obj->l('A amount must be less than 100000000', 'pricessets');
								//echo '@'.$attribute;
								$editPrice = Tools::ps_round($editPriceField,_PS_PRICE_DISPLAY_PRECISION_);
								$origPrice = Tools::ps_round(Tools::getValue('o'.$attribute,0),_PS_PRICE_DISPLAY_PRECISION_);
									
								if (($productFamilly->_reservationPriceType==reservation_price::TABLE_DATE && $editPrice != $origPrice)
									|| ($productFamilly->_reservationPriceType==reservation_price::TABLE_RATE && $editPrice != 1)
									|| $productFamilly->_reservationPriceType==reservation_price::TABLE_TIME) {

									$temppriceset = clone $priceset;
									$temppriceset->period = $periodKey;
									$temppriceset->amount = ($with_taxes ? $editPrice/(1+($tax_rate/100)) : $editPrice);
									$res &= $temppriceset->sqlInsert();
								}
							}
						}
					}
				}
				if ($res) $msg .= myOwnLang::$object[MYOWN_OBJ::PREFIX].' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRICESET]).' '.myOwnLang::getResult($obj, true, true).' '.myOwnLang::$operations[MYOWN_OPE::UPDATE];
			}
		}
		return $res;
	}
	
}

?>