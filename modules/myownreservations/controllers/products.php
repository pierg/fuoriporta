<?php
/*
* 2010-2012 LaBulle All right reserved
*/

class myOwnReservationsProductsController {

	//==============================================================================================================================================
	//     PRODUCTS LIST
	//==============================================================================================================================================
	
	public static function show($cookie, $obj) {
		$langue=$cookie->id_lang;
		$id_shop = myOwnUtils::getShop();
		$exts = $obj->_extensions->getExtByType(extension_type::PROD_UNIT);
		$occ_exts = $obj->_extensions->getExtByType(extension_type::PROD_OCC);
		$unitexts=array();
		foreach ($exts as $ext)
			$unitexts[$ext->unit] = $ext;
		$output = '';
		$odd=false;
		if (_PS_VERSION_ < "1.5.0.0") {
		$output .= '
		<div style="padding:20px 10px 0px 10px;">
			<fieldset class="settingsList">
				<legend>
					<img src="../img/admin/tab-categories.gif" />'.$obj->l('Product family management', 'products').'
				</legend>
				<br />
				<br />';
		}
		
		if (_PS_VERSION_ >= "1.6.0.0") {
			$output .= MyOwnReservationsUtils::table16wrap($obj, 'products', 'Product', ucfirst(myOwnLang::$object[MYOWN_OBJ::RESVRULE]), count($obj->_products->list));
		}
		
		$output .= '
				<table style="width:100%;background-color:#FFFFFF" cellpadding="0" cellspacing="0" class="table">
					<thead>
					<tr>
						<th width="10px">'.$obj->l('ID', 'products').'</th>
						<th width="22px"></th>
						<th>'.ucfirst(myOwnLang::$objects[MYOWN_OBJ::PRODUCT]).'</th>
						<th width="22px"></th>
						<th>'.$obj->l('Customer selection', 'products').'</th>
						<th>'.$obj->l('Reservation window', 'products').'</th>
						<th width="70px">'.$obj->l('Actions', 'products').'</th>
					</tr>
					</thead>';
					
				if (count($obj->_products->list)==0) {
					if (_PS_VERSION_ < "1.6.0.0") $output .= '
					<tr>
						<td colspan="7">'.ucfirst(myOwnLang::$operations[MYOWN_OPE::NORES]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRODFAMILY]).'</td>
					</tr>';
					else $output .= '
					<tr class="odd">
						<td class="list-empty" colspan="7">
							<div class="list-empty-msg">
								<i class="icon-warning-sign list-empty-icon"></i>
								'.ucfirst(myOwnLang::$operations[MYOWN_OPE::NORES]).' '.ucfirst(myOwnLang::$object[MYOWN_OBJ::PRODFAMILY]).'
							</div>
						</td>
					</tr>';
				} else {
					foreach($obj->_products->list AS $key => $value)
						if ($id_shop==0 or $value->id_shop==0 or $value->id_shop==$id_shop) {
						$cattxt='';
						foreach(explode(',',$value->ids_categories) as $cat) {
							$category = new Category($cat,intval($cookie->id_lang));
							if ($cattxt!='') $cattxt.=', ';
							$cattxt .= $category->name;
						}// else $cattxt = $obj->l('All', 'products');
						if ($cattxt!='') $cattxt=$obj->l('Categories', 'products').' : '.$cattxt;
						
						$prodtxt='';
						foreach(explode(',',$value->ids_products) as $prod) {
							if ($prodtxt!='') $prodtxt.=', ';
							$prodtxt.=MyOwnReservationsUtils::getProductName($prod,intval($cookie->id_lang), $value->id_shop);
						}
						if ($prodtxt!='') $prodtxt = ($cattxt!='' ? '<br/>' :'').ucfirst(myOwnLang::$objects[MYOWN_OBJ::PRODUCT]).' : '.$prodtxt;
						$odd=!$odd;
						$output .= '
					<tr class="row_hover'.($odd==0 ? ' alt_row odd' : '').'">
						<td>'.$key.'</td>
						<td><img src="../modules/'.$obj->name.(array_key_exists($value->productType, $occ_exts) ? '/extensions/'.$occ_exts[$value->productType]->name : '/img/occupy'.$value->productType).'.png"></td>
						<td style="text-align:left;"><b>'.(trim($value->name) == '' ? ucfirst(myOwnLang::$object[MYOWN_OBJ::FAMILY]).' #'.$value->sqlId : $value->name).'</b><br />'.$cattxt.$prodtxt.'</td>
						<td><img src="../modules/'.$obj->name.'/img/calendarsel'.$value->reservationSelType.'.png"></td>
						<td style="text-align:left;">';
						$productSelect="";$start='';$end='';
						if (!array_key_exists($value->reservationSelType, myOwnLang::$reservationInterval) && array_key_exists($value->reservationSelType, $unitexts))
							$productSelect=$unitexts[$value->reservationSelType]->showSelectionType($obj, $value);
						elseif ($value->reservationSelType==reservation_interval::TIMESLICE) {
							$productSelect = '<b>'.ucfirst(myOwnLang::$objects[MYOWN_OBJ::TIMESLICE]).'</b> : '.$obj->l('Each', 'products').' '.$value->_timeSliceLength.' '.$obj->l('minutes', 'products').' '.$obj->l('from', 'products').' '.$value->_reservationStartTime.' '.$obj->l('to', 'products').' '.$value->_reservationEndTime;
						} elseif ($value->isTimeSelect()) {
							foreach($value->getTimeslots(true) AS $timeslot) {
								if ($start=='') $start=$timeslot->startTime;
								$end=$timeslot->endTime;
							}
							$tscount = count($value->getTimeslots(true));
							$productSelect = '<b>'.ucfirst(myOwnLang::$objects[MYOWN_OBJ::TIMESLOT]).'</b> : ';
							if (!$tscount) $productSelect .= $obj->l('None', 'products');
							else $productSelect .= $tscount.' '.$obj->l('from', 'products').' '.$start.' '.$obj->l('to', 'products').' '.$end;
						} elseif ($value->reservationSelType==reservation_interval::DAY) {
							$defaultDays=explode(",",$value->_reservationSelDays);
							foreach(myOwnLang::getDaysNames($cookie->id_lang) as $dayId=>$dayName) {
								if ($defaultDays[(int)$dayId]==1) {
									if ($productSelect!='') $productSelect.=', ';
									$productSelect .= $dayName;
								}
							}
							$productSelect = '<b>'.$obj->l('Days', 'products').'</b> : '.$productSelect;
						} else if ($productSelect=='') {
							
							$productSelect='<b>'.ucfirst(myOwnLang::$reservationInterval[$value->reservationSelType]).'</b>';
						}
						$productSelect .= '<br/>'.$obj->l('Per', 'products').' '.myOwnLang::$reservationViews[$value->reservationSelPlanning];

						$output .= $productSelect.'</td>
						<td style="text-align:left;">
							<b>'.myOwnLang::$reservTypes[$value->reservationStartType] .'</b>
							<br/>'. $obj->l('During', 'products').' '.$value->reservationPeriod.' '. $obj->l('days', 'products').'
						</td>
						<td style="text-align:center;">
							'.(_PS_VERSION_ < "1.6.0.0" ? '
							<a href="'.myOwnReservationsController::getConfUrl('products','&editProduct='.$key).'"><img src="../img/admin/edit.gif" alt="'.ucfirst(myOwnLang::$actions[MYOWN_OPE::EDIT]).'" /></a>
							<a href="'.myOwnReservationsController::getConfUrl('products','&copyProduct='.$key).'"><img src="../img/admin/duplicate.png" alt="'.$obj->l('Copy', 'products').'"></a>
							<a href="'.myOwnReservationsController::getConfUrl('products','&deleteProduct='.$key).'" onclick="return confirm(\''.ucfirst(myOwnLang::$actions[MYOWN_OPE::DELETE]).' '.myOwnLang::$object[MYOWN_OBJ::PRODUCT].' '.$key.' ?\');"><img src="../img/admin/delete.gif" alt="" title="" /></a>' : MyOwnReservationsUtils::getListButton($obj, $key, 'products', 'Product')).'
						</td>
					</tr>';
					}
				}
		$output .= '
				</table>';	
		if (_PS_VERSION_ < "1.5.0.0") {
		$output .= '<br />
				<a href="'.myOwnReservationsController::getConfUrl('products','&editProduct=0').'"><img src="../img/admin/add.gif" border="0">&nbsp;'.$obj->l('Add a new product family', 'products').'</a>
				<br />
			</fieldset>
		</div>';
		}
		if (_PS_VERSION_ >= "1.6.0.0") $output .= '</div>';
		return $output;
	}
	
	//==============================================================================================================================================
	//     ADD/EDIT SAVE
	//==============================================================================================================================================
	public static function getFromPost($obj, $cookie) {
		$idProduct=0;
		if (isset($_POST['idProduct'])) $idProduct=$_POST['idProduct'];
		if ($idProduct==-1) $idProduct=0;
		if ($idProduct>-1 && array_key_exists($idProduct, $obj->_products->list))
			$tempProduct = $obj->_products->list[$idProduct];
		else $tempProduct = new myOwnProductsRule();
		
		//main params
		$tempProduct->sqlId = $idProduct;
		if (isset($_POST['productName'])) 
			$tempProduct->name = $_POST['productName'];
		if (isset($_POST['productShop'])) 
			$tempProduct->id_shop = $_POST['productShop'];
		if (isset($_POST['productEmail'])) 
			$tempProduct->email = $_POST['productEmail'];
		if (isset($_POST['notifications'])) 	
			$tempProduct->ids_notifs = $_POST['notifications'];
		else $tempProduct->ids_notifs = array();
		if (isset($_POST['options'])) 	
			$tempProduct->ids_options = $_POST['options'];
		else $tempProduct->ids_options = array();
		if (isset($_POST['productAssignment']) && isset($_POST['productAssignmentCheck'])) 	
			$tempProduct->assignment = $_POST['productAssignment'];
		else $tempProduct->assignment = 0;
		
		if (isset($_POST['options'])) 	
			$tempProduct->ids_options = $_POST['options'];
		else $tempProduct->ids_options = array();
			
		if (isset($_POST['carrierEmployees'])) 
			$tempProduct->ids_users = $_POST['carrierEmployees'];
		if (isset($_POST['categoryBox']) && is_array($_POST['categoryBox'])) 
			$tempProduct->ids_categories = implode(',', $_POST['categoryBox']);
		else $tempProduct->ids_categories = "";
		if (array_key_exists('productsBox', $_POST) && is_array($_POST['productsBox'])) 
			$tempProduct->ids_products = implode(',', $_POST['productsBox']);
		else $tempProduct->ids_products="";
	
		//reservation occupation
		$tempProduct->productType = Tools::getValue('productType', product_occupation::STOCK);
		$tempProduct->_occupyQty = Tools::getValue('occupyQty', '');
		$tempProduct->_qtyDisplay = (isset($_POST['productQuantityDisplay']) && $_POST['productQuantityDisplay']==1);
		$tempProduct->_optionAlsoSell= (Tools::getValue('productAlsoSell', 0));
		$tempProduct->_qtyCapacity = Tools::getValue('qtyCapacity', 1);
		$tempProduct->_occupyCombinations = Tools::getValue('occupyCombinations', 0);
		if ($tempProduct->productType==product_type::STOCK) 
			$tempProduct->_occupyCombinations = (isset($_POST['occupyStockCombinations']) && $_POST['occupyStockCombinations']==1);
		$tempProduct->_optionIgnoreAttrStock=($tempProduct->productType==product_type::VIRTUAL && ($tempProduct->_occupyCombinations==occupy_combinations::SUM or $tempProduct->_occupyCombinations==occupy_combinations::DEF));
		$tempProduct->_occupyMaxOrder = (isset($_POST['productOccupyMaxOrder']) && $_POST['productOccupyMaxOrder']==1);
		$tempProduct->_occupyStockAssign= (Tools::getValue('stockAssignation', 0));
		
		$tempProduct->_qtyType = Tools::getValue('reservationQtyControl', reservation_qty::PRODUCT);
		if ($tempProduct->_qtyType == reservation_qty::FIXED)
			$tempProduct->_qtyFixed = Tools::getValue('reservationQtyFixed', 1);
		if ($tempProduct->_qtyType == reservation_qty::SEARCH) 
			$tempProduct->_qtySearch = Tools::getValue('reservationQtySearch', '');
		
		//reservation selection type
		$tempProduct->reservationSelType = $_POST['reservationSelType'];
		//$tempProduct->_reservationSelMultiple = isset($_POST['reservationSelMulti']);
		$tempProduct->_reservationSelMode = Tools::getValue('reservationMode');
		$tempProduct->_reservationSelMultiple = ($tempProduct->_reservationSelMode == reservation_mode::MULTIPLE);
		$tempProduct->_reservationShowTime = isset($_POST['reservationShowTime']);

		$tempProduct->_reservationStartTime =  substr(MyOwnCalendar::getHourPost('productStartTime'), 0, 5);
		$tempProduct->_reservationEndTime = substr(MyOwnCalendar::getHourPost('productEndTime'), 0, 5);
		$tempProduct->_reservationCutTime = (isset($_POST['reservationCutTime']) && $_POST['reservationCutTime']==1);
		$tempProduct->_reservationTimeByDay = (isset($_POST['reservationTimeByDay']) && $_POST['reservationTimeByDay']==1);
		$tempProduct->_timeSliceLength = (int)Tools::getValue('productTimeSliceLength', 15);

		$tempProduct->reservationSelPlanning = $_POST['reservationPlanning'];
		$reservationSelDays='';
		for($i=1; $i<8; $i++)
			$reservationSelDays .= ",".intval(isset($_POST['reservationWeekDay'.$i]) && $_POST['reservationWeekDay'.$i]==1);
		$tempProduct->_reservationSelDays=$reservationSelDays;
		$tempProduct->_reservationSelWeekStart=intval(Tools::getValue('reservationWeekStart'));
		$tempProduct->_reservationSelWeekEnd=intval(Tools::getValue('reservationWeekEnd'));
		$tempProduct->_reservationSelMonthStart=Tools::getValue('reservationMonthStart');
		$tempProduct->_reservationSelMonthEnd=Tools::getValue('reservationMonthEnd');
		
		//reservation start and planning length
		$tempProduct->reservationStartType = $_POST['productReservationStartType'];
		if ($tempProduct->reservationStartType==reservation_start::AFTER_DAYS
			 or $tempProduct->reservationStartType==reservation_start::SOME_WEEKS)
			$reservationStartParams = $_POST['productReservationStartNb'];
		if ($tempProduct->reservationStartType==reservation_start::AFTER_HOURS
			 or $tempProduct->reservationStartType==reservation_start::AFTER_MINUT)
			 $reservationStartParams = $_POST['productReservationStartNb'].';'.(int)isset($_POST['productReservationStartCurrent']);
		if ($tempProduct->reservationStartType==reservation_start::SAME_DAY)
			$reservationStartParams = isset($_POST['productReservationStartCurrent']);
		if ($tempProduct->reservationStartType==reservation_start::NEXT_WEEK)
			$reservationStartParams = $_POST['productReservationStartDay'].';'.MyOwnCalendarTool::getHourPost('ressourceReservationStartTime');
		
		$tempProduct->reservationStartParams = $reservationStartParams;
		$tempProduct->reservationPeriod = $_POST['productReservationPeriod'];
		
		
		//reservation length
		$tempProduct->_reservationUnit = Tools::getValue('productReservationUnit',0);

		$tempProduct->_reservationLengthControl = $_POST['reservationLengthControl'];
		if (isset($_POST['productLengthCombination']) && $_POST['productLengthCombination']==1
			&& $tempProduct->_reservationLengthControl==reservation_length::PRODUCT)
			$tempProduct->_reservationLengthControl=reservation_length::COMBINATION;
	
		$tempProduct->_lengthHolidays = !(isset($_POST['lengthHolidays']) && $_POST['lengthHolidays']==1);
		$tempProduct->_lengthDisabledTimeSlots = !(isset($_POST['lengthDisabledTimeSlots']) && $_POST['lengthDisabledTimeSlots']==1);
		
		if ($tempProduct->_reservationLengthControl == reservation_length::FIXED) {
			$tempProduct->_reservationMinLength = Tools::getValue('productReservationFixedLength', 1);
			if ($tempProduct->_reservationMinLength==0) $tempProduct->_reservationMinLength=1;
			$tempProduct->_reservationMaxLength =$tempProduct->_reservationMinLength;
		} else if ($tempProduct->_reservationLengthControl == reservation_length::LIMIT)	{
			$tempProduct->_reservationMinLength = Tools::getValue('productReservationMinLength', 1);
			if ($tempProduct->_reservationMinLength==0) $tempProduct->_reservationMinLength=1;
			$tempProduct->_reservationMaxLength = Tools::getValue('productReservationMaxLength', 1);
		}
		if ($tempProduct->_reservationLengthControl == reservation_length::LIMIT
			or $tempProduct->_reservationLengthControl == reservation_length::FREE) 
				$tempProduct->_reservationLengthFreq = $_POST['reservationLengthFreq'];
		else $tempProduct->_reservationLengthFreq = 0;
		$tempProduct->_reservationLengthStrict = isset($_POST['reservationLengthStrict']);
		$tempProduct->_reservationLengthCanExceed = isset($_POST['productLengthCanExceed']);
		$tempProduct->_reservationLengthQuantity = isset($_POST['productLengthQuantity']) && $_POST['productLengthQuantity']==1;
		
		//reservation length
		$tempProduct->_reservationBetweenShift = $_POST['productReservationBetweenShift'];
		$tempProduct->_allowResOnHolidays = (isset($_POST['resaOnHolidays']) && $_POST['resaOnHolidays']==1);
		$tempProduct->_allowResOnRestDays = (isset($_POST['resaOnRestdays']) && $_POST['resaOnRestdays']==1);
		$tempProduct->_allowResBetweenDays = isset($_POST['productLengthBetweenDays']);
		
		//price length params
		if (isset($_POST['productPriceUnit'])) $tempProduct->_priceUnit = $_POST['productPriceUnit'];
		$tempProduct->_priceStrict = (isset($_POST['strictPrice']) && $_POST['strictPrice']==1);
		$tempProduct->_priceDisabledTimeSlots = !(isset($_POST['priceDisabledTimeSlots']) && $_POST['priceDisabledTimeSlots']==1);
		$tempProduct->_priceHolidays = !(isset($_POST['priceHolidays']) && $_POST['priceHolidays']==1);

		$tempProduct->_priceDoubleIfLength = (isset($_POST['priceDoubleIfLength']) && $_POST['priceDoubleIfLength']==1);
		$tempProduct->_priceCombination = (isset($_POST['priceCombination']) && $_POST['priceCombination']==1);
		$tempProduct->_coefPerProduct = (isset($_POST['coefPerProduct']) && $_POST['coefPerProduct']==1);
		$tempProduct->_reservationPriceType = $_POST['reservationPriceType'];
		if (isset($_POST['resaDepositCalc'])) {
			$tempProduct->_depositCalc=$_POST['resaDepositCalc'];
			$tempProduct->_depositAmount=$_POST['resaDepositAmount'];
		}
		$tempProduct->_advanceRate=$_POST['resaAdvanceRate'];
		$tempProduct->_advanceAmount=$_POST['resaAdvanceAmount'];
		$tempProduct->_depositByProduct=(isset($_POST['depositByProduct']) && $_POST['depositByProduct']);

		//price param
		$languages = Language::getLanguages();
		$priceLabel = array();
		$startLabel = array();
		$stopLabel = array();
		foreach ($languages AS $language) {
			if (isset($_POST['priceLabel_'.$language['id_lang']]))
				$priceLabel[$language['id_lang']] = $_POST['priceLabel_'.$language['id_lang']];
			if (isset($_POST['startLabel_'.$language['id_lang']]))
				$startLabel[$language['id_lang']] = $_POST['startLabel_'.$language['id_lang']];
			if (isset($_POST['stopLabel_'.$language['id_lang']]))
				$stopLabel[$language['id_lang']] = $_POST['stopLabel_'.$language['id_lang']];
		}
		$confid = 0;
		if ($tempProduct->sqlId>0) $confid = $tempProduct->sqlId;
		Configuration::updateValue('MYOWNRES_PRICELABEL_'.$confid, $priceLabel);
		Configuration::updateValue('MYOWNRES_STARTLABEL_'.$confid, $startLabel);
		Configuration::updateValue('MYOWNRES_STOPLABEL_'.$confid, $stopLabel);
		$tempProduct->_priceEndTimeSlot = (isset($_POST['priceEndTimeSlot']) && $_POST['priceEndTimeSlot']==1);
		$tempProduct->_pricePreview = (isset($_POST['previewPrice']) && $_POST['previewPrice']==1);
		
		if (isset($_POST['reservationStatus'])) $tempProduct->_optionValidationStatus=implode(',',$_POST['reservationStatus']);

		//timeslots
		$timeslots="";
		if(count($obj->_timeSlots->list)>0)
		foreach($obj->_timeSlots->list AS $timeSlot)
				if (isset($_POST['productTimeslot'.$timeSlot->sqlId]) && $_POST['productTimeslot'.$timeSlot->sqlId]==1) $timeslots.=$timeSlot->sqlId.";";
		$tempProduct->timeslots = $timeslots;
		//$obj->_products->list[$tempProduct->sqlId]=$tempProduct;
		return $tempProduct;
	}
	
	//==============================================================================================================================================
	//     ADD/EDIT PRODUCTS
	//==============================================================================================================================================
	public static function gettitle($title, $top=false)
	{
		if (_PS_VERSION_ >= "1.6.0.0") return '<h3 style="'.(!$top ? 'margin-top:20px' : '').'">'.$title.'</h3>';
		else return '<h4>'.$title.'</h4>';
	}
	
    public static function recurseCategory($categories, $current, $id_category = 1, $id_selected = 1)
	{
		global $currentIndex;
		$output='';
		$output.= '<option value="'.$id_category.'"'.(($id_selected == $id_category) ? ' selected="selected"' : '').'>'.
		str_repeat('&nbsp;', $current['infos']['level_depth'] * 5).self::hideCategoryPosition(stripslashes($current['infos']['name'])).'</option>';
		if (isset($categories[$id_category]))
			foreach ($categories[$id_category] AS $key => $row)
				$output.= self::recurseCategory($categories, $categories[$id_category][$key], $key, $id_selected);
     	return $output;
	}


	static public function hideCategoryPosition($name)
	{
		return preg_replace('/^[0-9]+\./', '', $name);
	}
	
	public static function getUnitLabelJS($obj) {
		if (!is_array(myOwnReservationsController::$reservationUnitKeys))
			myOwnReservationsController::_construire ($obj);
		$output = '
		function getUnitLabel(resvUnit) {
			var unitStr = "";';
			foreach (myOwnReservationsController::$reservationUnitKeys as $reservationUnitKey)
				$output .= '
			if (resvUnit=="'.$reservationUnitKey.'") unitStr = "'.myOwnLang::getUnit($reservationUnitKey, false, true).'";';
			$output .= '
			return unitStr;
		}';
		return $output;
	}
	
	public static function editInfoTab($cookie,$obj,$value) {
		$output = '
		<script language=\'javascript\'> 
			function occupyOption() {
				var occupyType = $(".productType:checked").val();
				$("#panelOption .option").each(function() {
					if (!$(this).attr("checked") || $(this).val()==occupyType) {
						$(this).parent().parent().removeClass("myownr_options");
						$(this).parent().parent().find(".optionItems").attr("disabled", true);
						$(this).parent().parent().find("td:eq(3)").children().hide();
						//$(this).attr("disabled", ($(this).val()==occupyType) );
						//if ($(this).val()==occupyType) $(this).attr("checked", false );
					} else {
						$(this).parent().parent().addClass("myownr_options");
						$(this).parent().parent().find(".optionItems").attr("disabled", false);
						$(this).parent().parent().find("td:eq(3)").children().show();
						$(this).attr("disabled", false);
					}
				});
				//$("#panelOption .option").attr("disabled", false);
				//$("#panelOption #option"+occupyType).attr("checked", true).attr("disabled", true);
			}
			function qtyOption() {
				var qtyType = $("#reservationQtyType:checked").val();
				if (qtyType=="'.reservation_qty::PARTIAL.'") {
					$("#qtyCapacity").show();
				} else {
					$("#qtyCapacity").hide();
				}
			}

		</script>';
		
		
		//Product information				
		//-----------------------------------------------------------------------------------------------------------------
		$output .= myOwnUtils::displayInfo(myOwnHelp::$tabs['prodFamily']['editInfoTab'], false);
		$obj->checkIsProVersion();
		if ($obj->isProVersion) {
			$output .= '
		<div class="panel">
			<h3><i class="icon-info"></i> '.$obj->l('General', 'settings').'</h3>
			
			<ol style="float:left;padding-left:0px;margin:0px;width:400px">
				<li class="myowndlist">
					<label for="name" class="li-label" style="width:100px;padding-top:7px;">
						<span class="mylabel-tooltip" title="'.myOwnHelp::$options['editInfoTab']['products']['name'].'">'.$obj->l('Name', 'products').'</span>
					</label>
					<input style="width:50%" type="text" size="16" id="name" name="productName" value="'.Tools::getValue('productName', $value->name).'" />
				</li>
				';
		        if (Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE')) {
		        	if (!defined('MYOWNRES_SEPARATE_SHOP') || !MYOWNRES_SEPARATE_SHOP) {
			            $shops = Shop::getShops(true);
			            $output .= '
							<li class="myowndlist">
								<label for="name" class="li-label" style="width:100px">'.$obj->l('Assigned Shop', 'products').'</label>
								<select style="width:30%" name="productShop">';
			            $output .= '<option value="0" >'.$obj->l('All', 'products').'</option>';
			            foreach ($shops as $shop) {
			                $output .= '<option value="'.$shop['id_shop'].'" '.($value->id_shop == $shop['id_shop'] ? 'selected' : '').'>'.$shop['name'].'</option>';
			            }
			            $output .= '
								</select>
							</li>';
					} else {
						$output .= '<input type="hidden" name="productShop" value="'.$value->id_shop.'">';
					}
		        }
				$output .= '
			</ol>
			<div style="clear:both"></div>
		</div>';
		}
		
		//Product selection				
		//-----------------------------------------------------------------------------------------------------------------	
		$output.='
		<div class="panel">
			<h3><i class="icon-AdminCatalog"></i> '.$obj->l('Products selection', 'products').'</h3>
			'.myOwnUtils::displayInfo(myOwnHelp::$sections['editInfoTab']['products'], false).'
			
			<table cellspacing="0" cellpadding="0" width="100%" style=" margin-bottom:0px;" align="left" class="">
				<tr>
					<td style="vertical-align: top;width:48%">
						<ol style="float:left;padding-left:0px;margin:0px;width:100%">
							<li class="myowndlist">		
								<label for="name" class="li-label" style="width:100px">
									<span class="mylabel-tooltip" title="'.myOwnHelp::$options['editInfoTab']['products']['products'].'">'.ucfirst(myOwnLang::$objects[MYOWN_OBJ::PRODUCT]).' </span>
								</label>
						';
					$products = MyOwnReservationsUtils::getAllProducts($cookie->id_lang, false);
					$disabledProds = $obj->_products->getDisabledProducts($value->sqlId);
	
					$output .= '<select multiple size="8" name="productsBox[]" style="width:350px;margin-top:20px">';
					foreach($products as $product) {
						$output .= '<option value="' . $product['id_product']. '" '.( in_array($product['id_product'], explode(',',$value->ids_products)) ? 'selected' : '').' '.(in_array($product['id_product'],$disabledProds) ? 'disabled' : '').'>'.$product['name'].'</option>';
					}
					$output .= '</select>';
	
					$output .= '
							</li>
						</ol>
					</td>
					<td style="vertical-align: top;width:48%">
						<span style="margin-left:40px;margin-bottom:3px" class="mylabel-tooltip" title="'.myOwnHelp::$options['editInfoTab']['products']['category'].'">'.$obj->l('Categories', 'products').'</span>';
				$selectedCat=array();
				$rowSelectedCat=explode(',',$value->ids_categories);
				foreach ($rowSelectedCat as $selectedCatItem)
					if (Category::categoryExists($selectedCatItem))
						$selectedCat[] = $selectedCatItem;
					
				$root = Category::getRootCategory();
				if (_PS_VERSION_ >= "1.6.0.0") {
					$disabledCats = $obj->_products->getDisabledCategories($value->sqlId);
					$tree = new HelperTreeCategories('associated-categories-tree', $obj->l('Reservation categories', 'products'));
					//setAttribute('is_category_filter', (bool)$this->id_current_category)
					if (method_exists($tree, 'setFullTree'))
						$tree->setFullTree(false);
						
					$tree->setRootCategory($root->id)
						->setUseCheckBox(true)
						->setUseSearch(true)
						->setSelectedCategories($selectedCat)
						->setDisabledCategories($disabledCats);
				
					$output .= $tree->render();
				} else {
				// Translations are not automatic for the moment ;)
					$trads = array(
						 'Home' => $obj->l('Home', 'products'),
						 'selected' => $obj->l('selected', 'products'),
						 'Collapse All' => $obj->l('Collapse All', 'products'),
						 'Expand All' => $obj->l('Expand All', 'products'),
						 'Check All' => $obj->l('Check All', 'products'),
						 'Uncheck All'  => $obj->l('Uncheck All', 'products')
					);
					$root = array('name' => $obj->l('Home', 'products'), 'id_category' => 1);
					if (_PS_VERSION_ < "1.5.0.0") $output .= Helper::renderAdminCategorieTree($trads, $selectedCat);
					else {
						$helper = new Helper();
						$use_shop_context = !is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");
						$output .= $helper->renderCategoryTree(null, $selectedCat, 'categoryBox', false, false, array(), false, $use_shop_context);
					}
				}
					$output .= '
					</td>
				</tr>
			</table>
			<div  style="clear:both"></div>
		</div>';
		
		//Product occupation				
		//-----------------------------------------------------------------------------------------------------------------	
		$output.='
		<div class="panel">
			<h3><i class="icon-bar-chart"></i> '.$obj->l('Product occupation', 'products').'</h3>
			'.myOwnUtils::displayInfo(myOwnHelp::$sections['editInfoTab']['occupation'], false).'';
			
			$output .= '
			<table cellspacing="0" cellpadding="0" width="100%" style=" margin-bottom:0px;" align="left" class="table" id="panelOccupation" type="single">
				<thead>
					<tr class="nodrag nodrop">
						<th class="center" width="20">
						</th><th colspan="2">'.$obj->l('Product kind', 'products').'</th>
						<th width="60%">'.$obj->l('Occupation options', 'products').'</th>
					</tr>			
				</thead><tbody>
				';
				$occ_exts = $obj->_extensions->getExtByType(extension_type::PROD_OCC);
				
				foreach (myOwnLang::$productType AS $occupationTypeKey => $occupationTypeVal)
					if ($occupationTypeKey < product_type::PLACE || array_key_exists($occupationTypeKey, $occ_exts)) {
					$output .= '
				<tr>
					<td width="20">
						<input type="radio" class="productType listElem" name="productType" onClick="occupyOption();listElem(\'panelOccupation\');" id="'.$occupationTypeKey.'" value="' . $occupationTypeKey. '"'.(Tools::getValue('productType', $value->productType)==$occupationTypeKey ? ' checked' : '').'>
					</td>
					<td width="22px">
						<img src="../modules/'.$obj->name.(array_key_exists($occupationTypeKey, $occ_exts) ? '/extensions/'.$occ_exts[$occupationTypeKey]->name : '/img/occupy'.$occupationTypeKey).'.png">
					</td>
					<td id="occupyType">
						<div class="listElemDescription">
							' . MyOwnReservationsUtils::getsubtitle($occupationTypeVal) . '
							<p class="desc">' . myOwnLang::$productTypeDescription[$occupationTypeKey] . '</p>
							'.(array_key_exists($occupationTypeKey, myOwnHelp::$options['editInfoTab']['occupation']) ? myOwnUtils::displaySubInfo(myOwnHelp::$options['editInfoTab']['occupation'][$occupationTypeKey]) : '').' 
						</div>
					</td>
					<td>
						<ol idd="rule_dates" style="float:left;padding-left:0px;margin:0px;width:100%">';		
					//Product occupation options
					if ($occupationTypeKey==product_type::PRODUCT) {
						$output .= '
						<li class="myowndlist" style="padding:2px;">
							'.myOwnUtils::getBoolField($obj, 'productAlsoSell', Tools::getValue('productAlsoSell',$value->_optionAlsoSell)==1, '<span class="mylabel-tooltip" title="'.myOwnHelp::$extras['editInfoTab']['occupation'][product_type::PRODUCT].'">'.$obj->l('Purchase', 'products').'</span>', 'width:140px;text-align:left', $obj->l('Also enable purchase of theses products', 'products')).'
						</li>
						<li class="myowndlist" style="padding:2px;">';
						if (Configuration::get('PS_ORDER_OUT_OF_STOCK')==1 or Configuration::get('PS_STOCK_MANAGEMENT')==0)
							$output .= '<p class="preference_description">'.$obj->l('Out of stock order is allowed, so product quantity is ignored', 'products').'</p>';
						$output .= '</li>';
					}
					if ($occupationTypeKey==product_type::STOCK) {
						$output .= '
						<p class="help-block">'.$obj->l('Enter each stock elements in myOwnReservation administration', 'products').'</p>';
						foreach (myOwnLang::$stockAssignation AS $stockAssignationKey => $stockAssignationVal) {
							$output .= '
							<li class="myowndlist" style="padding:2px;">
								<label class="sub-li-label"  style="">
									'.($stockAssignationKey==stock_assignation::NONE ? '<span class="mylabel-tooltip" title="'.myOwnHelp::$extras['editInfoTab']['occupation'][product_type::STOCK].'">'.$obj->l('Stock Assignation', 'products').'</span>' : '').'
								</label>
								<input type="radio" name="stockAssignation" value="' . $stockAssignationKey. '"'.(Tools::getValue('stockAssignation', $value->_occupyStockAssign)==$stockAssignationKey? ' checked' : '').'>&nbsp; ' . $stockAssignationVal . '
							</li><div style="clear:both"></div>';
						}
						$output .= '
						<li class="myowndlist" style="padding:2px;">
							'.myOwnUtils::getBoolField($obj, 'occupyStockCombinations', Tools::getValue('occupyStockCombinations',$value->_occupyCombinations)==1, $obj->l('Ignore attributes', 'products'), 'width:140px;text-align:left', $obj->l('The attribute is not used on stock items', 'products')).'
						</li>';
					}
					if ($occupationTypeKey==product_type::AVAILABLE) {
						$output .= '
						<p class="help-block">'.$obj->l('Enter available quantity in availabilities', 'products').'</p>';
					}
					if ($occupationTypeKey==product_type::VIRTUAL) {
						$output .= '
						';
						foreach (myOwnLang::$virtualQuantity AS $combinationTypeKey => $combinationTypeVal) {
							$output .= '
							<li class="myowndlist" style="padding:2px;">
								<label class="sub-li-label"  style="">
									'.($combinationTypeKey==occupy_combinations::IGNORE ? '<span class="mylabel-tooltip" title="'.myOwnHelp::$extras['editInfoTab']['occupation'][product_type::VIRTUAL].'">'.$obj->l('Product quantity', 'products').'</span>' : '').'
								</label>
								<input type="radio" name="occupyCombinations" value="' . $combinationTypeKey. '"'.(Tools::getValue('occupyCombinations', $value->_occupyCombinations)==$combinationTypeKey? ' checked' : '').'>&nbsp; ' . $combinationTypeVal . '</li><div style="clear:both"></div>';
						}
					}
					if ($occupationTypeKey==product_type::MUTUEL) {
						$output .= '
						<li class="myowndlist" style="padding:2px;">
							<label class="sub-li-label" style="padding-top:7px;">
								<span class="mylabel-tooltip" title="'.myOwnHelp::$extras['editInfoTab']['occupation'][product_type::MUTUEL].'">'. $obj->l('Occupation', 'products').'</span>
							</label>
							'.myOwnUtils::getQtyField($obj, 'occupyQty', Tools::getValue('occupyQty',$value->_occupyQty), ucfirst(myOwnLang::$objects[MYOWN_OBJ::RESV])).'
						</li>
						<li class="myowndlist" style="padding:2px;">
							'.myOwnUtils::getBoolField($obj, 'productOccupyMaxOrder', Tools::getValue('productOccupyMaxOrder',$value->_occupyMaxOrder)==1, $obj->l('Same order', 'products'), 'width:140px;text-align:left', $obj->l('If distinct order a new product is occupied', 'products')).'
						</li>
						<p class="help-block"><i class="icon-warning-sign"></i>&nbsp;'.$obj->l('In this case you\'ve to allow out of stock orders for these products', 'products').'</p>
						';
					}
					if ($occupationTypeKey==product_type::SHARED) {
						$output .= '
						<li class="myowndlist" style="padding:2px;">
							<label class="sub-li-label"  style="padding-top:7px;">
								<span class="mylabel-tooltip" title="'.myOwnHelp::$extras['editInfoTab']['occupation'][product_type::SHARED].'">'. $obj->l('Capacity', 'products').'</span>
							</label>
							'.myOwnUtils::getQtyField($obj, 'qtyCapacity', Tools::getValue('qtyCapacity',$value->_qtyCapacity), ucfirst(myOwnLang::$objects[MYOWN_OBJ::RESV])).'
							<p class="help-block">
								'.$obj->l('Amount of reservations that a product can handle for the same order', 'products').'
							</p>
						';
					}
					if ($occupationTypeKey >= product_type::PLACE && array_key_exists($occupationTypeKey, $occ_exts)) {
						$output .= $occ_exts[$occupationTypeKey]->displayConfigRuleOccupation($obj, $value);
					}
					
				$output .= '
						</ol>
					</td>
				</tr>';
			}
			$output .= '
			</tbody>
			</table>
			<div style="clear:both"></div>
		</div>';


		//Qty Control
		//-----------------------------------------------------------------------------------------------------------------	
		$exts = $obj->_extensions->getExtByType(extension_type::PROD_QTY);
		$qtyOptions=myOwnLang::$qtyControl;
		$qtyExts=array();
		foreach ($exts as $ext) 
			if (method_exists($ext, "getQties")) {
				$qtyOptions[$ext->id] = $ext->label;
				$qtyExts[$ext->id] = $ext;
			}
		$output.='
		<div class="panel">
			<h3><i class="icon-plus-square"></i> '.$obj->l('Quantity', 'products').'</h3>
			'.myOwnUtils::displayInfo(myOwnHelp::$sections['editInfoTab']['quantity'], false).'';
			$output .= '
			<table cellspacing="0" cellpadding="0" style="float:right;width:100%; margin-bottom:0px;" align="left" class="table" id="panelQty" type="single">
				<thead>
					<tr class="nodrag nodrop">
						<th class="center" width="20"></th>
						<th colspan="2">'.$obj->l('Reservation quantity', 'products').'</th>
						<th width="60%"></th>
					</tr>			
				</thead><tbody>
				';
				foreach ($qtyOptions AS $qtyControlKey => $qtyControlVal) {
					$output .= '
				<tr height="20px">
					<td width="20px">
						<input type="radio" onchange="listElem(\'panelQty\');" id="qtyControl" class="listElem" name="reservationQtyControl" value="' . $qtyControlKey. '" '.(Tools::getValue('reservationQtyControl', $value->_qtyType)==$qtyControlKey ? 'checked' : '').'>
					</td>
					<td width="20px">
						<img style="top:-8px" width="16px" height="16px" src="../modules/'.$obj->name.'/'.(array_key_exists($qtyControlKey, $qtyExts) ? $qtyExts[$qtyControlKey]->getImg() : '/img/qty'.$qtyControlKey.'.png').'">
					</td>
					<td style="padding:8px;">
						<b>' . $qtyControlVal . '</b>'.(array_key_exists($qtyControlKey, $qtyExts) ? '' : myOwnUtils::displaySubInfo(myOwnHelp::$options['editInfoTab']['quantity'][$qtyControlKey])).' 
					</td>
					<td width=""><ol style="padding-left:0px;margin:0px">';
					if ($qtyControlKey==reservation_qty::FIXED) {
						$output .= '
						<li class="myowndlist" style="padding: 0px;">
							<label class="sub-li-label" style="width:80px;margin-top: 5px;">'.$obj->l('Quantity', 'products').'</label>
							'.myOwnUtils::getQtyField($obj, 'reservationQtyFixed', Tools::getValue('reservationQtyFixed', $value->_qtyFixed), $obj->l('Quantity', 'products'), 1).'
						</li>';
					}
					if ($qtyControlKey==reservation_qty::SEARCH) {
						$attributes = AttributeGroup::getAttributesGroups($cookie->id_lang);
						$output .= '
						<li class="myowndlist" style="padding: 0px;">
							<label class="sub-li-label" style="width:90px;margin-top: 5px;">'.$obj->l('Search', 'products').'</label>
							';
							$output .= '<select name="reservationQtySearch" style="width:80px;">';
							foreach($attributes as $attributeKey => $attributeVal) {
								$output .= '<option value="'.$attributeVal['id_attribute_group'].'" '.( $attributeVal['id_attribute_group']==$value->_qtySearch ? 'selected' : '').'>'.$attributeVal['name'].'</option>';
							}
							$output .= '</select>';
					$output .= '
						</li>';
					}
					if (array_key_exists($qtyControlKey, $qtyExts)) {
						$output .= $qtyExts[$qtyControlKey]->displayConfigRuleQty($cookie, $obj, $value);
					}
						$output .= '</ol>
					</td>';
				}
					$output .= '
				</tr>
				</tbody>
				</table>';
	
			//Quantity display				
			//-----------------------------------------------------------------------------------------------------------------	
			$output.='
			<ol style="display:inline-block;margin-top:10px;width:100%">
				<li class="myowndlist">
					
					'.myOwnUtils::getBoolField($obj, 'productQuantityDisplay', Tools::getValue('productQuantityDisplay', $value->_qtyDisplay)==1, $obj->l('Show available quantity on planning', 'products'), 'width:260px').'
					
				</li>
			</ol>
			<div style="clear:both"></div>
		</div>';


		//Admin Control
		//-----------------------------------------------------------------------------------------------------------------	
		$output.='
		<div class="panel">
			<h3><i class="icon-group"></i> '.$obj->l('Administration', 'products').'</h3>
			'.myOwnUtils::displayInfo(myOwnHelp::$sections['editInfoTab']['admin'], false).'';

			$output.='
			<ol style="float:left;padding-left:0px;margin:0px;width:400px">
				<li class="myowndlist">		
					<label for="name" class="li-label" style="width:100px">
						<span class="mylabel-tooltip" title="'.myOwnHelp::$options['editInfoTab']['products']['admin'].'">'.$obj->l('Admins', 'products').'</span>
					</label>
					<select name="carrierEmployees[]" size="4" style="display:inline-block;width:240px" multiple="yes">';
	        		foreach (myOwnUtils::getEmployees() AS $employee) {
	          			$output .= '<option value="' . $employee['id_employee']. '"';
						if (in_array($employee['id_employee'], Tools::getValue('carrierEmployees', $value->ids_users))) $output .= ' selected';
						$output .= '>' . $employee['firstname'] . ' ' . $employee['lastname'] . '</option>';
	        		}
	          		$output .= '</select>
			  		<p class="preference_description" style="display:inline-block;margin-left:100px">('.$obj->l('Select users that will be able to manage these products reservations', 'products').')</p>
				</li>
				
			</ol>
			<div  style="clear:both"></div>
			
			<ol style="display:inline-block;margin-top:10px;width:500px;float:left;">
				<li class="myowndlist">
					
					'.myOwnUtils::getBoolField($obj, 'productAssignmentCheck', Tools::getValue('productAssignmentCheck', $value->assignment)>0, $obj->l('Assign a employee to the reservation', 'products'), 'width:260px').'
				</li>
			</ol>
			<script type="text/javascript">
			$("#productAssignmentCheck_on").change(function(e){
				$("#productAssignment").parent().show();
			});
			$("#productAssignmentCheck_off").change(function(e){
				$("#productAssignment").parent().hide();
			});
			</script>
			<ol style="display:inline-block;margin-top:10px;width:50%;float:left;margin-left:0px;-webkit-padding-start:0px">
				<li class="myowndlist" style="'.(Tools::getValue('productAssignmentCheck', $value->assignment)>0 ? '' :'display:none').'">
					<label for="name" class="li-label" style="width:100px">
						'.$obj->l('Profile', 'products').'
					</label>
					<select id="productAssignment" name="productAssignment" style="display:inline-block;width:100px">
						<option value="1">'.$obj->l('All', 'products').'</option>';
						foreach (Profile::getProfiles($obj->getContext()->language->id) as $profil)
							if ($profil['id_profile']>1)
								$output .= '<option value="'.$profil['id_profile'].'" '.(Tools::getValue('productAssignmentCheck', $value->assignment)==$profil['id_profile'] ? 'selected' : '').'>'.$profil['name'].'</option>';
						$output .= '
					</select>
				</li>
			</ol>
			<div  style="clear:both"></div>
		</div>';
	
		return $output;
	}
	
	public static function editPlanningTab($cookie,$obj,$value) {
		$days = myOwnLang::getDaysNames($cookie->id_lang);
		$obj->checkIsProVersion();
		$exts = $obj->_extensions->getExtByType(extension_type::PROD_UNIT);
		$reservationStartParamsTab = explode(';', $value->reservationStartParams);
		$output = '';
		if (!isset($_POST['productName'])) {
			$timeslotsArray= $value->_timeslotsTab;
			$reservationStartType = $value->reservationStartType;
			if ($reservationStartType==reservation_start::AFTER_DAYS or $reservationStartType==reservation_start::SOME_WEEKS) $resaStartNb = $value->reservationStartParams;
			else if ($reservationStartType==reservation_start::AFTER_HOURS or $reservationStartType==reservation_start::AFTER_MINUT) $resaStartNb = $reservationStartParamsTab[0];
			else $resaStartNb = 1;

			if ($reservationStartType==reservation_start::SAME_DAY) $resaStartCurrent = $value->reservationStartParams;
			else if ($reservationStartType==reservation_start::AFTER_HOURS or $reservationStartType==reservation_start::AFTER_MINUT) $resaStartCurrent = $reservationStartParamsTab[1];
			else $resaStartCurrent = 0;
			if ($reservationStartType==reservation_start::NEXT_WEEK) {
	      		$resaParams = explode(';',$value->reservationStartParams);
	      		$resaStartDay = $resaParams[0];
	      		$resaStartTime = $resaParams[1];
	      	} else {
	      		$resaStartDay = 0;
	      		$resaStartTime = '00:00:00';
	        }
        } else {
        	$timeslotsArray = array();
        	if (count($obj->_timeSlots->list))
        	foreach($obj->_timeSlots->list AS $timeSlot)
        		if (isset($_POST['productTimeslot'.$timeSlot->sqlId]) && $_POST['productTimeslot'.$timeSlot->sqlId]==1) $timeslotsArray[] = $timeSlot->sqlId;

        	$reservationStartType = $_POST['productReservationStartType'];
        	$resaStartNb = $_POST['productReservationStartNb'];
        	$resaStartCurrent = isset($_POST['productReservationStartCurrent']);
        	$resaStartDay = $_POST['productReservationStartDay'];
        	$resaStartTime = MyOwnCalendarTool::getHourPost('ressourceReservationStartTime');
        }
        
		$fixedLength=Tools::getValue('productReservationLengthType', ($value->_reservationMinLength==$value->_reservationMaxLength));
		
		$output .= '
		<script language=\'javascript\'> 
			var planningLength='.Tools::getValue('reservationlength', intval($value->reservationSelPlanning)).';
			var lengthUnit="'.Tools::getValue('lengthUnit', $value->_priceUnit).'";
			
			var reservationUnitViews = new Array();
			var reservationLengthUnits = new Array();';
						
						
			$output .= '
				var defaultUnits=new Array();
				defaultUnits['.reservation_interval::TIMESLICE.'] 		= '.reservation_unit::TIMESLOT.'; 
				defaultUnits['.reservation_interval::TIMESLOT.'] 	= '.reservation_unit::TIMESLOT.'; 
				defaultUnits['.reservation_interval::DAY.'] 		= '.reservation_unit::DAY.'; 
				defaultUnits['.reservation_interval::WEEK.'] 		= '.reservation_unit::WEEK.'; 
				defaultUnits['.reservation_interval::MONTH.']		= '.reservation_unit::MONTH.'; 
				defaultUnits['.reservation_interval::PERIOD.'] 		= '.reservation_unit::DAY.';
				var minUnitLength=new Array();
				minUnitLength['.reservation_interval::TIMESLICE.'] 		= '.reservation_unit::TIMESLOT.'; 
				minUnitLength['.reservation_interval::TIMESLOT.'] 	= '.reservation_unit::TIMESLOT.'; 
				minUnitLength['.reservation_interval::DAY.'] 		= '.reservation_unit::DAY.'; 
				minUnitLength['.reservation_interval::WEEK.'] 		= '.reservation_unit::WEEK.'; 
				minUnitLength['.reservation_interval::MONTH.']		= '.reservation_unit::MONTH.'; 
				minUnitLength['.reservation_interval::PERIOD.'] 		= '.reservation_unit::TIMESLOT.';
			';
			
			foreach (myOwnReservationsController::$reservationUnitViews AS $reservationUnitViewKey => $reservationUnitViewValue) {
				$output .= '
				reservationUnitViews['.$reservationUnitViewKey.']=\'';
					foreach ($reservationUnitViewValue AS $reservationView) {
						$output .= '<option value="' . $reservationView. '">'.$obj->l('Per', 'products').' ' . myOwnLang::$reservationViews[$reservationView]  . '</option>';
					}
				$output .= '\';';
			}
			foreach (myOwnReservationsController::$reservationLengthUnits AS $reservationSelUnitKey => $reservationSelUnitValue) {
				$output .= '
				reservationLengthUnits['.$reservationSelUnitKey.']=\'';
					foreach ($reservationSelUnitValue AS $reservationUnit) {
						$output .= '<option value="' . $reservationUnit. '">'.ucfirst(myOwnLang::getUnit($reservationUnit)). '</option>';
					}
				$output .= '\';
				';
			}
			foreach ($exts AS $ext) {
				myOwnReservationsController::$reservationUnitViews[$ext->unit] = myOwnReservationsController::$reservationUnitViews[$ext->parentUnit];
				$output .= '
				reservationUnitViews['.$ext->unit.']=\'';
					foreach (myOwnReservationsController::$reservationUnitViews[$ext->parentUnit] AS $reservationView) {
						$output .= '<option value="' . $reservationView. '">'.$obj->l('Per', 'products').' ' . myOwnLang::$reservationViews[$reservationView]  . '</option>';
					}
				$output .= '\';';
				$output .= '
				reservationLengthUnits['.$ext->unit.']=\'';
					foreach (myOwnReservationsController::$reservationLengthUnits[$ext->parentUnit] AS $reservationUnit) {
						$output .= '<option value="' . ($reservationUnit==$ext->parentUnit ? $ext->unit : $reservationUnit). '">'.($reservationUnit!=$ext->parentUnit ? ucfirst(myOwnLang::getUnit($reservationUnit)) : ucfirst($ext->getUnitLabel($obj))). '</option>';
					}
				$output .= '\';
				';
				$output .= '
				minUnitLength['.$ext->unit.'] 		= '.$ext->parentUnit.';
				defaultUnits['.$ext->unit.'] 		= '.$ext->parentUnit.'; ';
			}
			

			$output .= self::getUnitLabelJS($obj);
			$output .= '
			function startTypeOption() {
				var resvType = $("#reservationStartType").val();
				if (resvType=="'.reservation_start::SAME_DAY.'") {
					$("#reservationStartDay").hide();
					$("#reservationStartNb").hide();
					$("#reservationStartCurrent").show();
				}
			if (resvType=="'.reservation_start::AFTER_MINUT.'" || resvType=="'.reservation_start::AFTER_HOURS.'") {
					$("#reservationStartDay").hide();
					$("#reservationStartNb").show();
					$("#reservationStartCurrent").show();
					if (resvType=="'.reservation_start::AFTER_MINUT.'")
						$("#reservationStartNbLabel").html("'.myOwnLang::getUnit(reservation_unit::HOUR, true).'");
					if (resvType=="'.reservation_start::AFTER_HOURS.'")
						$("#reservationStartNbLabel").html("'.myOwnLang::getUnit(reservation_unit::HOUR, true).'");
				}
				if (resvType=="'.reservation_start::AFTER_DAYS.'" || resvType=="'.reservation_start::SOME_WEEKS.'") {
					$("#reservationStartDay").hide();
					$("#reservationStartNb").show();
					$("#reservationStartCurrent").hide();
					if (resvType=="'.reservation_start::AFTER_DAYS.'")
						$("#reservationStartNbLabel").html("'.myOwnLang::getUnit(reservation_unit::DAY, true).'");
					if (resvType=="'.reservation_start::SOME_WEEKS.'")
						$("#reservationStartNbLabel").html("'.myOwnLang::getUnit(reservation_unit::WEEK, true).'");
				}
				if (resvType=="'.reservation_start::NEXT_WEEK.'") {
					$("#reservationStartDay").show();
					$("#reservationStartNb").hide();
					$("#reservationStartCurrent").hide();
				}
			}
			function intervalTypeOption(init) {
				var resvSelType = $("input[name=\'reservationSelType\']:checked").val();
				var preview=false;
				planningLength=$("#planningLength").val();
				
				$(".runOverDisabled").hide();
				$(".runOverBetween").hide();
				$("#reservationSelTimeslot").hide();
				$("#reservationSelWeek").hide();
				$("#reservationSelMonth").hide();
				$("#editTimeslotsTab-pane").parent().hide();
				$("#link-editTimeslotsTab").hide();
				$("#timeByDay").hide();
				
				if (resvSelType=="'.reservation_interval::TIMESLICE.'") {
					$("#reservationSelTimeslot").show();
					$("#resaOnRestdays").hide();
					$(".runOverDisabled").hide();
					preview=true;
				}
				if (resvSelType=="'.reservation_interval::TIMESLOT.'" '.(MYOWNRES_WEEK_TS ? '|| resvSelType=="'.reservation_interval::WEEK.'"' : '').') {
					$("#reservationSelTimeslot").show();
					$("#link-editTimeslotsTab").show();
					'.($obj->isProVersion ? '$("#editTimeslotsTab-pane").parent().show();$("#link-editTimeslotsTab").show();' : '').'
					$("#resaOnRestdays").show();
					$(".runOverBetween").show();
					$(".runOverDisabled").show();
					$(".selectLbl").html("' . $obj->l('Disabled time slots', 'products') . '");
					$("#startTimeMinute, #startTimeHour, #endTimeMinute, #endTimeHour").attr("disabled");
					preview=true;
					if (planningLength>0 && planningLength<4) $("#timeByDay").show();
				} else {
					$("#startTimeMinute, #startTimeHour, #endTimeMinute, #endTimeHour").removeAttr("disabled");
					'.($obj->isProVersion ? '$("#editTimeslotsTab-pane").parent().hide();$("#link-editTimeslotsTab").hide();' : '').'
				}
				if (resvSelType=="'.reservation_interval::DAY.'") {
					$("#resaOnRestdays").show();
					$(".runOverDisabled").show();
					$(".selectLbl").html("' . $obj->l('Disabled days', 'products') . '");
					preview=true;
				}
				if (resvSelType=="'.reservation_interval::WEEK.'") {
					$("#reservationSelWeek").show();
					$("#resaOnRestdays").hide();
					preview=true;
				}
				';
				foreach ($exts as $ext) {
					$output .= '
					if (resvSelType=="'.$ext->unit.'") {
						'.$ext->selUnitScript($obj).'
					}';
				}
				$output .= '
				if (resvSelType=="'.reservation_interval::MONTH.'") {
					$("#reservationSelMonth").show();
					$("#resaOnRestdays").hide();
					preview=true;
				}
				if (resvSelType=="'.reservation_interval::PERIOD.'") {
					$("#resaOnRestdays").hide();
				}
				$("#subDayConfig").find("select").each(function( index ) {
					/*if (resvSelType!="'.reservation_interval::TIMESLOT.'") $(this).removeAttr("disabled","disabled");
					else $(this).attr("disabled","disabled");*/
				});
				
				
				//minUnitLength
				
				lengthUnit=$("#lengthUnit").val();
				document.getElementById("planningLength").innerHTML = reservationUnitViews[resvSelType];
				document.getElementById("lengthUnit").innerHTML = reservationLengthUnits[resvSelType];
				if (reservationUnitViews[resvSelType].indexOf(\'"\'+planningLength+\'"\')>0) document.getElementById("planningLength").value=planningLength;
				else $("#planningLength option:first-child").attr("selected", "selected");
				if (init) {
					document.getElementById("lengthUnit").value="'.intval($value->_reservationUnit).'";
					$(\'#lengthUnit option[value="'.intval($value->_reservationUnit).'"]\').attr("selected", "selected");
				} else {
					document.getElementById("lengthUnit").value=defaultUnits[resvSelType];
					$(\'#lengthUnit option[value="\'+defaultUnits[resvSelType]+\'"]\').attr("selected", "selected");
				}
				tempplanningLength=$("#planningLength").val(); 
				if (preview) {
					document.getElementById("planning-preview").src = "../modules/'.$obj->name.'/img/"+resvSelType+"-"+tempplanningLength+".gif";
					document.getElementById("planning-preview").style.display = "block";
				} else 
					document.getElementById("planning-preview").style.display = "none";
				
				
				lengthUnitOption();
			}
			function lengthUnitOption() {
				var resvUnit = $("#lengthUnit").val();
				var unitStr = "";
				$(".lengthDisabled").hide();
				if (resvUnit=="'.reservation_unit::MINUTE.'" || resvUnit=="'.reservation_unit::HOUR.'") {
					$(".lengthDisabled").hide();
				}
				if (resvUnit=="'.reservation_unit::TIMESLOT.'") {
					$("span.lengthDisabled").html("' . $obj->l('Disabled time slots', 'products') . '");
					$(".lengthDisabled").show();
				}
				if (resvUnit=="'.reservation_unit::DAY.'") {
					$("span.lengthDisabled").html("' . $obj->l('Disabled days', 'products') . '");
					$(".lengthDisabled").show();
				}
				';
				foreach ($exts as $ext) {
					$output .= '
					if (resvUnit=="'.$ext->unit.'") {
						'.$ext->lengthUnitScript($obj).'
					}';
				}
				$output .= '
				unitStr = getUnitLabel(resvUnit);
				$(".lengthUnitLbl").each(function() {
					$(this).html(unitStr);
				});
				
				//refresh in case of price unit is same
				priceUnitOption();
			}		  

			function getView() {
				var resvSelType = $("input[name=\'reservationSelType\']:checked").val();
				planningLength=$("#planningLength option:selected").val();
				if (resvSelType=="'.reservation_interval::TIMESLOT.'" '.(MYOWNRES_WEEK_TS ? '|| resvSelType=="'.reservation_interval::WEEK.'"' : '').') {
					if (planningLength>0 && planningLength<4) $("#timeByDay").show();
					else $("#timeByDay").hide();
				} else $("#timeByDay").hide();
				
				document.getElementById(\'planning-preview\').src = "../modules/'.$obj->name.'/img/"+resvSelType+"-"+planningLength+".gif";
			}
			function modeControlOption() {
				var mode = $("#modeControl:checked").val();
				var removed=false;
				if (mode=="'.reservation_mode::MULTIPLE.'" || mode=="'.reservation_mode::UNIQUE.'")
				{
					$(".reservationLengthControl").each(function( index ) {
						if ($(this).val()=="'.reservation_length::FREE.'" || $(this).val()=="'.reservation_length::LIMIT.'") {
							$(this).attr("disabled", "disabled");
							if ($(this).attr("checked")) {
								$(this).removeAttr("checked");
								removed=true;
								$(this).parent().parent().children("td:eq(2)").css("color", "#959595");
							}
						} else {
							$(this).removeAttr("disabled");
							if (removed) {
								removed=false;
								$(this).attr("checked", "checked");
							}
						}
					});
					$("#lengthoptionhelp").show();
					$("#pricePreview").hide();
				} else
				{
					$(".reservationLengthControl").each(function( index ) {
						$(this).removeAttr("disabled");
						$(this).parent().parent().children("td:eq(2)").css("color", "");
					});
					$("#lengthoptionhelp").hide();
					$("#pricePreview").show();
				}
				//listElem("panelLength"); removed because reseting length field
			}
			function lengthControlOption() {
				var lengthControl = $("#lengthControl:checked").val();
				$("#productLength").hide();
				$("#fixedLength").hide();
				$("#variableLength").hide();
				$("#multipleSel").hide();
				
				$("#unlimitedLength").hide();
				if (lengthControl=="'.reservation_length::FREE.'") {
					$("#unlimitedLength").show();
					$("#lengthFreq").show();
				}
				if (lengthControl=="'.reservation_length::LIMIT.'" ) {
					$("#variableLength").show();
					$("#lengthFreq").show();
				}
				if (lengthControl=="'.reservation_length::FIXED.'") {
					$("#fixedLength").show();
					$("#multipleSel").show();
					$("#lengthFreq").hide();
				}
				if (lengthControl=="'.reservation_length::PRODUCT.'" || lengthControl=="'.reservation_length::COMBINATION.'") {
					$("#productLength").show();
					$("#multipleSel").show();
					$("#lengthFreq").hide();
				}
				if (lengthControl!=="'.reservation_length::FREE.'" && lengthControl!="'.reservation_length::NONE.'") {
					var resaLengthOption = $("#resaLengthOption");
					/*resaLengthOption.find("input").each(function( index ) {
						if (!$(this).hasClass("listElem")) $(this).removeAttr("disabled");
					});
					resaLengthOption.find("select").each(function( index ) {
						if (!$(this).hasClass("listElem")) $(this).removeAttr("disabled");
					});*/
				}
				if (lengthControl=="'.reservation_length::NONE.'") {
					$("#runOver").hide();
					$("#lengthFreq").hide();
					$("#multipleSel").show();
				}
				else $("#runOver").show();
				if (lengthControl=="'.reservation_length::OPTIONAL.'") 
					$("#lengthFreq").show();
				if (lengthControl=="'.reservation_length::LIMIT.'" || lengthControl=="'.reservation_length::FIXED.'" || lengthControl=="'.reservation_length::PRODUCT.'" || lengthControl=="'.reservation_length::COMBINATION.'") {
					$("#priceControl'.reservation_price::TABLE_TIME.'").removeAttr("disabled");
					$("#priceControl'.reservation_price::TABLE_DATE.'").removeAttr("disabled");
					$("#priceControl'.reservation_price::TABLE_RATE.'").removeAttr("disabled");
					$("#priceControl'.reservation_price::TABLE_TIME.'").parent().parent().children("td:eq(2)").css("color", "");
					$("#priceControl'.reservation_price::TABLE_DATE.'").parent().parent().children("td:eq(2)").css("color", "");
					$("#priceControl'.reservation_price::TABLE_RATE.'").parent().parent().children("td:eq(2)").css("color", "");
					$("#tablerateshelp").hide();
				} else {
					$("#priceControl'.reservation_price::TABLE_TIME.'").attr("disabled", "disabled");
					$("#priceControl'.reservation_price::TABLE_DATE.'").attr("disabled", "disabled");
					$("#priceControl'.reservation_price::TABLE_RATE.'").attr("disabled", "disabled");
					if ($("#priceControl'.reservation_price::TABLE_TIME.'").attr("checked") || $("#priceControl'.reservation_price::TABLE_DATE.'").attr("checked") || $("#priceControl'.reservation_price::TABLE_RATE.'").attr("checked")) {
						$("#priceControl'.reservation_price::PRODUCT_TIME.'").attr("checked", "checked");
						priceControlOption();
					}
					$("#priceControl'.reservation_price::TABLE_TIME.'").parent().parent().children("td:eq(2)").css("color", "#959595");
					$("#priceControl'.reservation_price::TABLE_RATE.'").parent().parent().children("td:eq(2)").css("color", "#959595");
					$("#priceControl'.reservation_price::TABLE_DATE.'").parent().parent().children("td:eq(2)").css("color", "#959595");
					$("#tablerateshelp").show();
				}
				listElem(\'panelPrice\');
			}

		</script> 
		';
		
		//Interval type
		//-----------------------------------------------------------------------------------------------------------------

	$output .= myOwnUtils::displayInfo(myOwnHelp::$tabs['prodFamily']['editPlanningTab'], false).'
	<div class="panel">
		<h3><i class="icon-calendar"></i> '.$obj->l('Customer choice', 'products').'</h3>
		

		'.myOwnUtils::displayInfo(myOwnHelp::$sections['editPlanningTab']['unit'], false).'
		
		<table cellspacing="0" cellpadding="0" style="width: 100%; margin-bottom:10px;" class="table" id="panelSelection" type="single">
			<thead>
				<tr class="nodrag nodrop">
					<th class="center" width="20">
					<th colspan="2"> '.$obj->l('Selection unit', 'products').'</th>
					<th width="40%">'.$obj->l('Selection options', 'products').'</th>
					<th width="20%"><input type="checkbox" name="reservationShowTime" value="1" '.($value->_reservationShowTime ? 'checked' : '').'> '.$obj->l('Display Time', 'products').'</th>
				</tr>			
			</thead><tbody>
			';
			
			//Extensions selection
			$selChoices=count(myOwnLang::$reservationInterval);
			$extTimeConfig=0;
			$extDaysConfig=0;
			foreach ($exts AS $extKey => $extValue) {
				if ($extValue->isTimeConfig) $extTimeConfig++;
				if ($extValue->isDaysConfig) $extDaysConfig++;
			}

			//sub day config
			$subDayConfig='
				<td width="15%" rowspan="'.($extTimeConfig+4).'" style="background-color:rgb(248, 248, 248);" id="subDayConfig">
					'.$obj->l('Start time', 'products').'<br/>
					'.myOwnReservationsTimeslotsController::timeSelect("productStartTime", $value->_reservationStartTime.':00', $cookie->id_lang, '').'<br/>
					<br/>
					<span class="mylabel-tooltip" title="'.myOwnHelp::$extras['editPlanningTab']['unit'][reservation_interval::MONTH].'">'.$obj->l('End time', 'products').'</span><br/>
					'.myOwnReservationsTimeslotsController::timeSelect("productEndTime", $value->_reservationEndTime.':00', $cookie->id_lang, '').'
					</li>
				</td>';
				
			$daysConfig='
					<ol style="padding-left: 0px;">
						<li class="myowndlist">
							<label class="sub-li-label">'. $obj->l('Available days', 'products') . '</label><div style="clear:both"></div>
							';
						if ($value->_reservationSelDays=='') $defaultDays=array(1,1,1,1,1,1,0,0);
							else $defaultDays=explode(",",$value->_reservationSelDays);
							foreach(myOwnLang::getDaysNames($cookie->id_lang) as $dayId=>$dayName) {
								$daysConfig .= myOwnUtils::getBoolField($obj, 'reservationWeekDay'.$dayId, Tools::getValue('reservationWeekDay'. $dayId, $defaultDays[$dayId])==1, $dayName, 'width:30%').'<div style="clear:both"></div>';
								/*$daysConfig .= '
									<span style="display:inline-block;margin-right:6px"><input style="margin-right:3px;" class="selDay" name="reservationWeekDay'. $dayId .'" type="checkbox" value="1" '.(Tools::getValue('reservationWeekDay'. $dayId, $defaultDays[$dayId])==1 ? 'checked' : '').'/><span style="cursor:default" onclick="$(this).parent().find(\'input\').attr(\'checked\', !$(this).parent().find(\'input\').attr(\'checked\'))">'.$dayName.'</span></span>';*/
							}
							$daysConfig .= '
							
						</li>
					</ol>';
				
			//Extensions selection
			$cnt=0;
			$extsChoice='';
			$extsChoiceCnt=0;
			$extTimeConfigDisplayed=false;
			foreach ($exts AS $extKey => $extValue) {
				$extsChoice .= '
			<tr>
				<td width="20">
					<input style="" class="listElem" title="" type="radio" onChange="listElem(\'panelSelection\');intervalTypeOption(false);" id="reservationSelType" name="reservationSelType" value="' . $extValue->unit. '"'.(Tools::getValue('reservationSelType', $value->reservationSelType)==$extValue->unit ? ' checked' : '').'>
				</td>
				<td width="30px"><img src="../modules/myownreservations/'.$extValue->getImg().'"></td>
				<td>
					<div class="listElemDescription" >
						' . MyOwnReservationsUtils::getsubtitle($extValue->title) . '
						<p class="desc">'.$extValue->description.'</p>
					</div>
				</td>
				<td width="50%" rowspan="'.($extDaysConfig+1).'">' . $daysConfig. '</td>
				'.(!$extValue->isTimeConfig ? '<td width="15%">'.$extValue->displayConfigRuleQty($obj, $value->sqlId).'</td>' : (!$extTimeConfigDisplayed ? $subDayConfig : '')).'
			</tr>';
				$cnt++;
				if ($extValue->isTimeConfig) $extTimeConfigDisplayed=true;
			}
			$extsChoiceCnt=$cnt;
			
			//classic selection
				foreach (myOwnLang::$reservationInterval AS $reservIntervalKey => $reservIntervalValue) {
						$output .= '
			<tr>
				<td width="20">
					<input style="" class="listElem" title="" type="radio" onChange="listElem(\'panelSelection\');intervalTypeOption(false);" id="reservationSelType" name="reservationSelType" value="' . $reservIntervalKey. '"'.(Tools::getValue('reservationSelType', $value->reservationSelType)==$reservIntervalKey ? ' checked' : '').'>
				</td>
				<td width="30px"><img src="../modules/'.$obj->name.'/img/calendarsel'.$reservIntervalKey.'.png"></td>
				<td>
					<div class="listElemDescription" >
						' . MyOwnReservationsUtils::getsubtitle($reservIntervalValue) . '
						<p class="desc">'.myOwnLang::$reservationIntervalDescription[$reservIntervalKey].'</p>
						'.myOwnUtils::displaySubInfo(myOwnHelp::$options['editPlanningTab']['unit'][$reservIntervalKey]).' 
					</div>
				</td>
					';
					if ($reservIntervalKey==reservation_interval::TIMESLOT) {
						$output .= '
					<td width="50%"><ol><i>' . $obj->l('Edit time slots in dedicated tab', 'products'). '</i></ol></td>
					';
					}
					if ($reservIntervalKey==reservation_interval::TIMESLICE) {
						$output .= '
					<td width="50%">
						<ol>
							<li class="myowndlist">
								<label class="li-label" style="width:100px">' . $obj->l('Slice Length', 'products') .'</label>
								'.myOwnUtils::getInputField('productTimeSliceLength', Tools::getValue('productTimeSliceLength', $value->_timeSliceLength), $obj->l('minutes', 'products'), 'width:50px').'
							</li>' . $daysConfig. '
						</ol>
					</td>
					';
					}
					
					if ($reservIntervalKey==reservation_interval::DAY && !$extDaysConfig) {
						$output .= '<td width="50%">'.$daysConfig.'</td>';
					}
					if ($reservIntervalKey==reservation_interval::WEEK) {
						$output .= '
					<td width="50%">
						<ol style="margin-bottom:0px;padding-left: 0px;">
						<li class="myowndlist" style="padding: 2px;">
							<label class="sub-li-label">'.$obj->l('Week start day', 'products').'</label>
							<select id="reservationWeekStart" name="reservationWeekStart" style="width:25%;height:28px">';
							foreach(myOwnLang::getDaysNames($cookie->id_lang) as $dayId=>$dayName) {
								$output .= '<option value="' . $dayId. '"';
								if (Tools::getValue('reservationWeekStart', $value->_reservationSelWeekStart)==$dayId) $output .= ' selected';
								$output .= '>' . $dayName . '</option>';
							}
							$output .= '
							</select>
						</li>
						<li class="myowndlist" style="padding: 2px;">
							<label class="sub-li-label">'.$obj->l('Week end day', 'products').'</label>
							<select title="" id="reservationWeekEnd" name="reservationWeekEnd" style="width:25%;height:28px">';
							foreach(myOwnLang::getDaysNames($cookie->id_lang) as $dayId=>$dayName) {
								$output .= '<option value="' . $dayId. '"';
								if (Tools::getValue('reservationWeekEnd', $value->_reservationSelWeekEnd)==$dayId) $output .= ' selected';
								$output .= '>' . $dayName . '</option>';
							}
							$output .= '
							</select>
						</li>
					</ol></td>';
					}
					if ($reservIntervalKey==reservation_interval::MONTH) {
						$reservationMonthStart = Tools::getValue('reservationMonthStart', $value->_reservationSelMonthStart);
						$reservationMonthEnd = Tools::getValue('reservationMonthEnd', $value->_reservationSelMonthEnd);
						$output .= '
					<td width="50%">
						<ol style="margin-bottom:0px;padding-left: 0px;">
						<li class="myowndlist" style="padding: 2px;">
							<label class="sub-li-label">'.$obj->l('Month start day', 'products').'</label>
								<select id="reservationMonthStart" name="reservationMonthStart" style="width:25%;height:28px">';
								$output .= '<option value="0"';
								if ($reservationMonthStart=="0") $output .= ' selected';
								$output .= '>' . $obj->l('First month day', 'products') . '</option>';
								foreach(myOwnLang::getDaysNames($cookie->id_lang) as $dayId=>$dayName) {
									$output .= '<option value="w' . $dayId. '"';
									if ($reservationMonthStart=='w'.$dayId) $output .= ' selected';
									$output .= '>' . $obj->l('First', 'products'). ' '. $dayName . '</option>';
								}
								for($i=2; $i<29; $i++) {
									$output .= '<option value="d' . $i. '"';
									if ($reservationMonthStart=='d'.$i) $output .= ' selected';
									$output .= '>' . $i . '</option>';
								}
								$output .= '
								</select>
						</li>
						<li class="myowndlist" style="padding: 2px;">
							<label class="sub-li-label">'.$obj->l('Month end day', 'products').'</label>
								<select id="reservationMonthEnd" name="reservationMonthEnd"  style="width:25%;height:28px">';
								foreach(myOwnLang::getDaysNames($cookie->id_lang) as $dayId=>$dayName) {
									$output .= '<option value="w' . $dayId. '"';
									if ($reservationMonthEnd=='w'.$dayId) $output .= ' selected';
									$output .= '>' . $obj->l('Last', 'products'). ' '. $dayName . '</option>';
								}
								for($i=1; $i<29; $i++) {
									$output .= '<option value="d' . $i. '"';
									if ($reservationMonthEnd=='d'.$i) $output .= ' selected';
									$output .= '>' . $i . '</option>';
								}
								$output .= '<option value="0"';
								if ($reservationMonthEnd=="0") $output .= ' selected';
								$output .= '>' . $obj->l('Last month day', 'products') . '</option>';
								$output .= '
								</select>
						</li>
					</ol></td>';
					}
					if ($reservIntervalKey==reservation_interval::TIMESLOT) {
						$output .= '
					<td><input type="checkbox" name="reservationCutTime" value="1" '.($value->_reservationCutTime ? 'checked' : '').'> <span class="mylabel-tooltip" title="'.myOwnHelp::$extras['editPlanningTab']['unit'][reservation_interval::TIMESLOT].'">'.$obj->l('Cut Times', 'products').'</span></td>
					';
					}
					if (!$extTimeConfig && $reservIntervalKey==reservation_interval::TIMESLICE) $output .= $subDayConfig;
					$output .= '
			</tr>
			';
			if ($reservIntervalKey==reservation_interval::TIMESLICE) $output .= $extsChoice;
			
				}
				
				$output .= '
		</table>
		<div style="clear:both"></div>
	</div>
	<div class="panel">
		<h3><i class="icon-hand-o-up"></i> '.$obj->l('Selection mode', 'products').'</h3>
	
		<ol style="padding-left:0px;margin:0px;width:60%;padding: 0px;float:left;">';
			//Reservation mode
			//-----------------------------------------------------------------------------------------------------------------
			$output .= '
			<li class="myowndlist">';
			foreach (myOwnLang::$reservationMode AS $reservModeKey => $reservModeValue){
				$output .= '
					&nbsp;<input type="radio" id="modeControl" onclick="modeControlOption();" class="listElem" name="reservationMode" value="'.$reservModeKey.'" '.($value->_reservationSelMode == $reservModeKey ? 'checked' : '').' >&nbsp;&nbsp;<img src="../modules/'.$obj->name.'/img/mode'.$reservModeKey.'.png">&nbsp;<b>' . $reservModeValue.'</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			}
			$output .= '
					
			</li>
		</ol>
		<div class="" id="pricePreview" style="padding-top:8px;display:inline-block">
				<input class="addTimeSlotsDay" name="previewPrice" type="checkbox" value="1" '.(Tools::getValue('previewPrice', $value->_pricePreview)==1 ? 'checked' : '').' /> '.$obj->l('Show "from" price on reservation start selection', 'products').'
		</div>
		<div style="clear:both"></div>
	</div>
	<div class="panel">
		<h3><i class="icon-calendar-o"></i> '.$obj->l('Front planning', 'products').'</h3>

		'.myOwnUtils::displayInfo(myOwnHelp::$sections['editPlanningTab']['planning'], false).'
		
		<ol style="float:left;padding-left:0px;margin:0px;width:40%;padding: 0px;">';
			//Reservation start type
			//-----------------------------------------------------------------------------------------------------------------
			$output .= '
			<li class="myowndlist">
				<label class="li-label" style="width:100px">
					<span class="mylabel-tooltip" title="'.myOwnHelp::$options['editPlanningTab']['planning']['start'].'">' . $obj->l('Start', 'products') . '</span>
				</label>
				<select id="reservationStartType" name="productReservationStartType" onChange="startTypeOption()" style="width:50%">';
					foreach (myOwnLang::$reservTypes AS $reservTypeKey => $reservTypeValue){
						$output .= '<option value="' . $reservTypeKey. '"';
						if ($reservationStartType==$reservTypeKey)
							$output .= ' selected';
						$output .= '>' . $reservTypeValue . '</option>';
					}
					$output .= '
				</select>
			</li>';

			//After some days or weeks
			//-----------------------------------------------------------------------------------------------------------------
			$output .= '
			<li class="myowndlist" id="reservationStartNb" style="display:';
			if ($reservationStartType!=reservation_start::AFTER_DAYS 
				&& $reservationStartType!=reservation_start::SOME_WEEKS
				&& $reservationStartType!=reservation_start::AFTER_HOURS
				&& $reservationStartType!=reservation_start::AFTER_MINUT)
					$output .= 'none';
			$output .= '">';
			
			$delayUnit = '
					<span  style="display:inline-block;" id="reservationStartNbLabel">';
			if ($reservationStartType==reservation_start::AFTER_DAYS) $delayUnit .= myOwnLang::getUnit(reservation_unit::DAY, true);
			if ($reservationStartType==reservation_start::SOME_WEEKS) $delayUnit .= myOwnLang::getUnit(reservation_unit::WEEK, true);
			if ($reservationStartType==reservation_start::AFTER_HOURS) $delayUnit .= myOwnLang::getUnit(reservation_unit::HOUR, true);
			if ($reservationStartType==reservation_start::AFTER_MINUT) $delayUnit .= myOwnLang::getUnit(reservation_unit::HOUR, true);
			$delayUnit .= '</span>';
				$output .= '
					<label class="myowndeliveriesSettingsLabel" style="display:inline-block;width:100px"></label>
					'.myOwnUtils::getInputField('productReservationStartNb', Tools::getValue('productReservationStartNb', $resaStartNb), $delayUnit, 'width:50px');
				$output .= '
					
					
			</li>';

			//Next week
			//-----------------------------------------------------------------------------------------------------------------
			$output .= '
			<li class="myowndlist" id="reservationStartDay" style="display:'.($reservationStartType!=reservation_start::NEXT_WEEK ? 'none' : '').'">
				<label class="myowndeliveriesSettingsLabel" style="width:100px"></label>
				'.$obj->l('Week start', 'products').' : <select  style="display:inline-block;width:80px;margin-right:5px" name="productReservationStartDay">';
			  			for($i=1; $i<8; $i++) {
						if ($i != $resaStartDay) $output .= '<option value="' . $i . '">' . $days[$i] . '</option>';
						if ($i == $resaStartDay) $output .= '<option value="' . $i . '" selected>' . $days[$i] . '</option>';
					}
				$output .= '				
				</select>'.myOwnReservationsTimeslotsController::timeSelect("ressourceReservationStartTime", $resaStartTime, $cookie->id_lang).'
			</li>';

			//Current day
			//-----------------------------------------------------------------------------------------------------------------
			$output .= '				
			<li class="myowndlist" id="reservationStartCurrent" style="display:'.($reservationStartType!=reservation_start::SAME_DAY && $reservationStartType!=reservation_start::AFTER_HOURS && $reservationStartType!=reservation_start::AFTER_MINUT ? 'none' : '').'">
					<label class="myowndeliveriesSettingsLabel" style="width:100px"></label>
					<input style="" class="upsInput" size="3" type="checkbox" name="productReservationStartCurrent" value="1"'.($resaStartCurrent==1 ? ' checked' : '').' />&nbsp;'.$obj->l('Allow reservation on a current time slot', 'products').'
			</li>';
				
			//Period						
			//-----------------------------------------------------------------------------------------------------------------
			$output .= '
			
			<li class="myowndlist">
				<label class="li-label" style="width:100px">
					<span class="mylabel-tooltip"  title="'.myOwnHelp::$options['editPlanningTab']['planning']['period'].'" >' . $obj->l('Length', 'products') .'</span>
				</label>
				'.myOwnUtils::getInputField('productReservationPeriod', Tools::getValue('productReservationPeriod', $value->reservationPeriod), $obj->l('days', 'products'), 'width:50px').'
			</li>
		</ol>
		<ol style="float:left;padding-left:0px;margin:0px;width:30%;margin-bottom:10px">
			<li class="myowndlist">
				<label class="li-label" style="width:100px;height: 22px;">
					<span class="mylabel-tooltip" title="'.myOwnHelp::$options['editPlanningTab']['planning']['length'].'" >' . $obj->l('View type', 'products') . '</span>
				</label>
				<select id="planningLength" multiple size="3" name="reservationPlanning" onChange="getView();" style="width:150px;font-size:13px;">';
				foreach(myOwnReservationsController::$reservationUnitViews[intval($value->reservationSelType)] AS $reservationView){
					$output .= '<option value="' . $reservationView. '"';
					if (Tools::getValue('reservationlength', $value->reservationSelPlanning)==$reservationView)
						$output .= ' selected';
					$output .= '>' . myOwnLang::$reservationViews[$reservationView] . '</option>';
				}
				$output .= '
				</select>
			</li>
			<li class="myowndlist" id="timeByDay">
				<label class="li-label" style="width:100px;height: 22px;">
				</label>
				<input type="checkbox" name="reservationTimeByDay" value="1" '.($value->_reservationTimeByDay ? 'checked' : '').'> '.$obj->l('Group time slots by day', 'products').'
			</li>
		</ol>
		<div class="well" style="display:inline:block;width:290px;margin:0px;height:150px;overflow:hidden">
			<p style="color: #7F7F7F;font-size: 0.85em;position: relative;top:-15px">' . $obj->l('Sample', 'products') . '</p><img id="planning-preview" width="250px" style="padding: 6px;background: #fff;border: 1px solid #DFD5C3;position: relative;top:-15px" src="../modules/'.$obj->name.'/img/'.intval($value->reservationSelType).'-'.intval($value->reservationSelPlanning).'.gif">
		</div>
		<div  style="clear:both"></div>
	</div>';
		
			$output .= '
	<div class="panel">
		<h3><i class="icon-clock-o"></i> '.$obj->l('Selection length', 'products').'</h3>
		'.myOwnUtils::displayInfo(myOwnHelp::$sections['editPlanningTab']['length'], false).'';
		
		//Length Control
		//-----------------------------------------------------------------------------------------------------------------
		$output .= '
		<table cellspacing="0" cellpadding="0" style="float:right;width:100%; margin-bottom:0px;" align="left" class="table" id="panelLength" type="single">
			<thead>
				<tr class="nodrag nodrop">
					<th class="center" width="20"></th>
					<th colspan="2">'.$obj->l('Length kind', 'products').'</th>
					<th width="32%">'.$obj->l('Length options', 'products').'</th>
					<th width="28%">'.$obj->l('Length Unit', 'products').'</th>
				</tr>			
			</thead><tbody>
			';
			foreach (myOwnLang::$lengthControl AS $lengthControlKey => $lengthControlVal) {
				$output .= '
			<tr height="20px">
				<td width="20px">
					<input type="radio" onchange="listElem(\'panelLength\');lengthControlOption();" id="lengthControl" class="listElem reservationLengthControl" name="reservationLengthControl" value="' . $lengthControlKey. '" '.(Tools::getValue('reservationLengthControl', $value->_reservationLengthControl)==$lengthControlKey || ($lengthControlKey==reservation_length::PRODUCT && Tools::getValue('reservationLengthControl', $value->_reservationLengthControl)==reservation_length::COMBINATION) ? 'checked' : '').'>
				</td>
				<td width="20px">
					<img style="top:-8px" src="../modules/'.$obj->name.'/img/length'.$lengthControlKey.'.png">
				</td>
				<td style="padding:8px;">
					<b>' . $lengthControlVal . '</b>
					'.myOwnUtils::displaySubInfo(myOwnHelp::$options['editPlanningTab']['length'][$lengthControlKey]).' 
				</td>
				<td width="35%">
					<ol style="margin-bottom:0px;padding: 0px;">';
				if ($lengthControlKey==reservation_length::FIXED) {
					$minlength=Tools::getValue('productReservationFixedLength', $value->_reservationMinLength);
					if (!$minlength) $minlength=1;
					$output .= '
					<li class="myowndlist" style="padding: 2px;">
						<label class="sub-li-label" style="width:50%;padding-top:7px">'.$obj->l('Length', 'products').'</label>
						'.myOwnUtils::getInputField('productReservationFixedLength', $minlength, '<span class="lengthUnitLbl">'.myOwnLang::getUnit($value->_reservationUnit, true).'</span>', 'width:40px').'
					</li>';
				}
				if ($lengthControlKey==reservation_length::LIMIT) {
					$output .= '
					<li class="myowndlist" style="padding: 2px;">
						<label class="sub-li-label" style="width:60px;padding-top:7px">' . $obj->l('Length', 'products') .'</label>
						<div style="margin-right:5px">
							'.myOwnUtils::getInputField('productReservationMinLength', Tools::getValue('productReservationMinLength', $value->_reservationMinLength), '<span class="lengthUnitLbl">'.myOwnLang::getUnit($value->_reservationUnit, true).'</span>', 'width:40px', '', false, '', '', 0, $obj->l('From', 'products')).'
						</div>
					<li class="myowndlist" style="padding: 2px;">
						<label class="sub-li-label" style="width:60px"></label>
						<div style="margin-right:5px">
							'.myOwnUtils::getInputField('productReservationMaxLength', Tools::getValue('productReservationMaxLength', $value->_reservationMaxLength), '<span class="lengthUnitLbl">'.myOwnLang::getUnit($value->_reservationUnit, true).'</span>', 'width:40px', '', false, '', '', 0, $obj->l('To', 'products')).'
						<div  style="clear:both"></div>
					</li>';
				}
				if ($lengthControlKey==reservation_length::PRODUCT or $lengthControlKey==reservation_length::COMBINATION) {
					$output .= '
					<li class="myowndlist" style="padding: 2px;">'.myOwnUtils::getBoolField($obj, 'productLengthCombination', $value->_reservationLengthControl==reservation_length::COMBINATION, '<span class="mylabel-tooltip" title="'.myOwnHelp::$extras['editPlanningTab']['length']['combination'].'">'.$obj->l('By combination', 'products').'</span>', 'width:50%;text-align:left').'</li>';
				}
				if ($lengthControlKey==reservation_length::FIXED or $lengthControlKey==reservation_length::PRODUCT or $lengthControlKey==reservation_length::COMBINATION) {
					$output .= '
					<li class="myowndlist" style="padding: 2px;">'.myOwnUtils::getBoolField($obj, 'productLengthQuantity', $value->_reservationLengthQuantity, '<span class="mylabel-tooltip" title="'.myOwnHelp::$extras['editPlanningTab']['length']['quantity'].'">'.$obj->l('Multiply by quantity', 'products').'</span>', 'width:50%;text-align:left').'</li>';
				}
					$output .= '</ol>
				</td>';
			if($lengthControlKey==reservation_length::NONE || $lengthControlKey==reservation_length::OPTIONAL) 
				$output .= '
				<td width="35%"></td>';
				
			//Length unit						
			//-----------------------------------------------------------------------------------------------------------------
			if($lengthControlKey==reservation_length::FREE) {
				$_reservationUnit = $value->_reservationUnit;
				/*if (Tools::getValue('reservationUnit', 0)>=$value->reservationSelType)
					$_reservationUnit = Tools::getValue('reservationUnit');*/
				$output .= '
				<td rowspan="4" width="35%" id="resaLengthOption" style="background-color:rgb(248, 248, 248);color:rgb(102, 102, 102);padding: 0px;">
					<ol style="float:left;padding-left:0px;margin:0px;width:100%">
							<li class="myowndlist">
							<label class="sub-li-label" style="width:80px;padding-top:7px;">'.$obj->l('Unit', 'products').'</label>
							<select onChange="lengthUnitOption();" id="lengthUnit" name="productReservationUnit" style="width:50%">';
								foreach (myOwnReservationsController::$reservationUnitKeys as $unitTypeKey) {
									$output .= '
									<option value="' . $unitTypeKey. '" '.($_reservationUnit==$unitTypeKey ? ' selected' : '').'>' . ucfirst(myOwnLang::getUnit($unitTypeKey)). '</option>';
								}
								$output .= '
							</select>
						</li>
						<li class="myowndlist">
							<label class="sub-li-label" style="width:80px;padding-top:7px;">
								<span>' . $obj->l('Exclude', 'products') .'</span>
							</label>
							<table class="panel" style="margin-bottom: 0px">
								<tbody>
								<tr>
									<td>
										<input class="addTimeSlotsDay" name="lengthHolidays" type="checkbox" value="1" '.(Tools::getValue('lengthHolidays',!$value->_lengthHolidays)==1 ? 'checked' : '').' />
									</td>
									<td><img src="../modules/'.$obj->name.'/img/calendar-holiday-small.png"> '. $obj->l('Holidays periods', 'products').'</td>
								</tr>
								<tr class="lengthDisabled">
									<td>
										<input class="addTimeSlotsDay" name="lengthDisabledTimeSlots" type="checkbox" value="1" '.(Tools::getValue('lengthDisabledTimeSlots',!$value->_lengthDisabledTimeSlots)==1 ? 'checked' : '').' />
									</td>
									<td><img class="lengthDisabled" src="../modules/'.$obj->name.'/img/calendar-delete-small.png"> <span class="lengthDisabled">'.($value->_reservationUnit == reservation_interval::TIMESLOT ? $obj->l('Disabled time slots', 'products') : $obj->l('Disabled days', 'products') ).'</span></td>
								</tr>
								</tbody>
							</table>
							
						</li>
					</ol>
				</td>';
			}
				$output .= '
			</tr>';
			}
			$output .= '		
		</tbody></table>
		<p class="help-block" id="lengthoptionhelp" style="padding-left:32px"><i class="icon-warning-sign"></i>&nbsp;' . $obj->l('Unlimited and limited length can\'t work with unique or multiple selection', 'products'). '</p>';
			
		
		//Advanced options
		//-----------------------------------------------------------------------------------------------------------------
		$defaultLanguage = intval(Configuration::get('PS_LANG_DEFAULT'));
		$languages = Language::getLanguages();
		
		$output .= '
		<script type="text/javascript">
			id_languagew = Number('.$defaultLanguage.');

			function	changelanguageMyOwn(field, fieldsString, id_language_new, iso_code)
			{	
						document.getElementById("ReservationMsgB_" + id_languagew).style.display = "none";
						document.getElementById("ReservationMsgB_" + id_language_new).style.display = "block";
						document.getElementById("ReservationMsgC_" + id_languagew).style.display = "none";
						document.getElementById("ReservationMsgC_" + id_language_new).style.display = "block";
						
				'.(_PS_VERSION_ < "1.6.0.0" ? '
					document.getElementById("language_current_"+field).src = "../img/l/" + id_language_new + ".jpg";
					document.getElementById("languages_"+field).style.display = "none";' : '$(".languageMyOwn").html(iso_code+\' <span class=\"caret\"></span>\');').'
				id_languagew = id_language_new;
			}
		</script>';
		
		$output .= '
		<div style="clear:both"></div>
	</div>
		
	<div class="panel">
		<h3><i class="icon-AdminTools"></i> '.$obj->l('Advanced options', 'products').'</h3>
		<ol style="float:left;padding-left:0px;margin:0px;width:100%">
			<li class="myowndlist" id="runOver">
				<label class="li-label" style="width:250px">
					<span class="mylabel-tooltip" title="'.myOwnHelp::$extras['editPlanningTab']['length']['run-over'].'">'.$obj->l('Allow to run over', 'products').'</span>
				</label>
				<div class="table">
				<table class="panel" style="margin-bottom: 0px">
					<tr>
						<td><input class="addTimeSlotsDay" name="resaOnHolidays" type="checkbox" value="1" '.(Tools::getValue('resaOnHolidays',$value->_allowResOnHolidays)==1 ? 'checked' : '').'/></td>
						<td><img src="../modules/'.$obj->name.'/img/calendar-holiday-small.png"> '. $obj->l('Holidays periods', 'products').'</td>
					</tr>
					<tr class="runOverDisabled">
						<td><input name="resaOnRestdays" type="checkbox" value="1" '.(Tools::getValue('resaOnRestdays',$value->_allowResOnRestDays)==1 ? 'checked' : '').'/></td>
						<td><img class="runOverDisabled" src="../modules/'.$obj->name.'/img/calendar-delete-small.png"> <span class="selectLbl runOverDisabled">'.($value->reservationSelType == reservation_interval::TIMESLOT ? $obj->l('Disabled time slots', 'products') : $obj->l('Disabled days', 'products') ).'</span></td>
					</tr>
					<tr class="runOverBetween">
						<td><input class="productLengthBetweenDays" name="productLengthBetweenDays" type="checkbox" value="1" '.($value->_allowResBetweenDays ? 'checked' : '').'></td>
						<td><img src="../modules/'.$obj->name.'/img/calendar-between-small.png"> ' . $obj->l('Outside Time slots', 'products') .'</td>
					</tr>
					<tr>
						<td><input class="productLengthCanExceed" name="productLengthCanExceed" type="checkbox" value="1" '.($value->_reservationLengthCanExceed ? 'checked' : '').'></td>
						<td><img src="../modules/'.$obj->name.'/img/calendar-exceed-small.png"> ' . $obj->l('Reservation window', 'products') .'</td>
					</tr>
				</table>
				</div>
			</li>
			<li class="myowndlist">
				<label class="li-label" style="width:250px;padding-top:7px">
					<span class="mylabel-tooltip" style="" title="'.myOwnHelp::$extras['editPlanningTab']['length']['between'].'">'.$obj->l('Length between reservations', 'products').'</span>
				</label>
				'.myOwnUtils::getInputField('productReservationBetweenShift', Tools::getValue('productReservationBetweenShift', $value->_reservationBetweenShift), '<span class="lengthUnitLbl">'.myOwnLang::getUnit($value->_reservationUnit, true).'</span>', 'width:50px').'
				
			</li>
			<li class="myowndlist" id="lengthFreq" style="padding-top:8px">
				<label class="li-label"  style="width:250px;padding-top:7px">
					<span class="mylabel-tooltip" title="'.myOwnHelp::$extras['editPlanningTab']['length']['frequency'].'">' . $obj->l('Frequency', 'products') .'</span>
				</label>
				<div style="float:left;margin-right:5px">
					'.myOwnUtils::getInputField('reservationLengthFreq', Tools::getValue('reservationLengthFreq', $value->_reservationLengthFreq), '<span class="lengthUnitLbl">'.myOwnLang::getUnit($value->_reservationUnit, true).'</span>', 'width:50px').'
				</div>
				<div style="float:left;padding-top:7px">
				<input class="addTimeSlotsDay" name="reservationLengthStrict" type="checkbox" value="1" '.(Tools::getValue('reservationLengthStrict', intval($value->_reservationLengthStrict))==1 ? 'checked' : '').' /> '.$obj->l('Strict length', 'products').'</div><div  style="clear:both"></div>
				
			</li>
			<li class="myowndlist" id="labels" style="padding-top:8px">
				<label class="li-label" style="width:250px;padding-top:7px">' . $obj->l('Labels', 'products') .'</label>';
					
					$languages = Language::getLanguages();
					$defaultLanguage = intval(Configuration::get('PS_LANG_DEFAULT'));
					$output .= '
						<div style="float:left">';
							foreach ($languages as $language)
								$output .= '
								<div id="ReservationMsgB_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';">
									<div class="input-group">
										<span class="input-group-addon">'.$obj->l('Start', 'products').'</span>
										<input size="10" type="text" name="startLabel_'.$language['id_lang'].'" id="ReservationBInput_'.$language['id_lang'].'" value="'.stripslashes(Configuration::get('MYOWNRES_STARTLABEL_'.$value->sqlId, $language['id_lang'])).'" /> 
									</div>
								</div>';
						$output .= '
						</div>
						<div style="float:left;padding-right:10px;;padding-left:10px;">';
							foreach ($languages as $language)
								$output .= '
								<div id="ReservationMsgC_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
									<div class="input-group">
										<span class="input-group-addon">'.$obj->l('Stop', 'products').'</span>
										<input size="10" type="text" name="stopLabel_'.$language['id_lang'].'" id="ReservationCInput_'.$language['id_lang'].'" value="'.stripslashes(Configuration::get('MYOWNRES_STOPLABEL_'.$value->sqlId, $language['id_lang'])).'" /> 
									</div>
								</div>';
							$output .= '
						'.myOwnUtils::getLanguageFlags($obj, 'changelanguageMyOwn', 'ReservationMsg').'
							</div>
						<div  style="clear:both"></div>';
							
			$output .= '
			</li>
		</ol>
		<div  style="clear:both"></div>
	</div>';
		
		$output .= '
		</fieldset>';
		return $output;	
	}
	
	public static function editPriceTab($cookie,$obj,$value) {
		$output = '';
		$defaultLanguage = intval(Configuration::get('PS_LANG_DEFAULT'));
		$languages = Language::getLanguages();
		
		$output .= '
		<script type="text/javascript">
			id_languagew = Number('.$defaultLanguage.');

			function	changePriceLanguage(field, fieldsString, id_language_new, iso_code)
			{	
						document.getElementById(field+"A_" + id_languagew).style.display = "none";
						document.getElementById(field+"A_" + id_language_new).style.display = "block";
						
				'.(_PS_VERSION_ < "1.6.0.0" ? '
					document.getElementById("language_current_"+field).src = "../img/l/" + id_language_new + ".jpg";
					document.getElementById("languages_"+field).style.display = "none";' : '$(".languageMyOwn").html(iso_code+\' <span class=\"caret\"></span>\');').'
				id_languagew = id_language_new;
			}
			function priceUnitOption() {
				var priceUnit = $("#priceUnit").val();
				if (priceUnit=="") priceUnit = $("#lengthUnit").val();
				var resvSelType = $("input[name=\'reservationSelType\']:checked").val();
				var unitStr = "";
				$(".priceDisabled").hide();
				$(".priceStrict").hide();
				$(".priceEnd").hide();
				if (priceUnit=="'.reservation_unit::HOUR.'") {
					$("#priceEndTimeSlot").hide();
				}
				if (resvSelType=="'.reservation_interval::TIMESLICE.'") {
					$(".priceEnd span").html("' . $obj->l('End slice', 'products') . '");
					$(".priceDisabled").hide();
					$(".priceEnd").show();
				}
				if (priceUnit=="'.reservation_unit::TIMESLOT.'") {
					$(".priceDisabled span").html("' . $obj->l('Disabled time slots', 'products') . '");
					$(".priceEnd span").html("' . $obj->l('End time slot', 'products') . '");
					$(".priceDisabled").show();
					$(".priceEnd").show();
				}
				if (priceUnit=="'.reservation_unit::DAY.'") {
					if (priceUnit=="'.reservation_unit::DAY.'") $(".priceStrict").show();
					$(".priceDisabled span").html("' . $obj->l('Disabled days', 'products') . '");
					$(".priceEnd span").html("' . $obj->l('End day', 'products') . '");
					$(".priceDisabled").show();
					$(".priceEnd").show();
				}
				if (priceUnit=="'.reservation_unit::WEEK.'") {
					$(".priceStrict").show();
				}
				if (priceUnit=="'.reservation_unit::MONTH.'") {
					$(".priceStrict").show();
				}
				unitStr = getUnitLabel(priceUnit);
				$(".priceUnitLbl").each(function() {
					$(this).html(unitStr);
				});
			}
			function priceControlOption() {
				var priceControl = $("input[name=\'reservationPriceType\']:checked").val();
				if (priceControl=="'.reservation_price::PRODUCT_TIME.'") {
					$(".priceLengthExtraOptions").show();
					$("#priceTableExtraOptions").css("visibility", "hidden");
				}
				if (priceControl=="'.reservation_price::PRODUCT_DATE.'") {
					$(".priceLengthExtraOptions").show();
					$("#priceTableExtraOptions").css("visibility", "hidden");
				}
				if (priceControl=="'.reservation_price::TABLE_TIME.'") {
					$(".priceLengthExtraOptions").show();
					$("#priceTableExtraOptions").css("visibility", "visible");
				}
				if (priceControl=="'.reservation_price::TABLE_DATE.'") {
					$(".priceLengthExtraOptions").hide();
					$("#priceTableExtraOptions").css("visibility", "visible");
				}
				if (priceControl=="'.reservation_price::TABLE_RATE.'") {
					$(".priceLengthExtraOptions").show();
					$("#priceTableExtraOptions").css("visibility", "visible");
					$("#editRatesTab-pane").parent().show();
					$("#link-editRatesTab").show();
				} else {
					$("#editRatesTab-pane").parent().hide();
					$("#link-editRatesTab").hide();
				}
			}
			function fixedAdvanceChanged() {
				var fixed=0;
				if ($("#resaAdvanceAmount")!=null)
					fixed=parseInt($("#resaAdvanceAmount").val());
				if (isNaN(fixed)) fixed=0;
				if (fixed>0) $("#resaAdvanceRate").val(0);
				
				var percent=parseInt($("#resaAdvanceRate").val());
				if (isNaN(percent)) percent=0;
				if (percent>0) $("#resaAdvanceAmount").val(0);
				var enabled = (percent<100 || fixed>0);
				$("#paymentOption_advance").prop("checked", enabled);
				if (enabled)
					$("#resaAdvanceAmount").parents("tr").addClass("myownr_options");
				else $("#resaAdvanceAmount").parents("tr").removeClass("myownr_options");
			}
			function depositChanged() {
				var deposit=0;
				if ($("#resaDepositAmount")!=null)
					deposit=parseInt($("#resaDepositAmount").val());
				if (isNaN(deposit)) deposit=0;
				var depositByProduct = $("#depositByProduct").prop("checked");
				var enabled = (deposit>0 || depositByProduct);
				$("#paymentOption_deposit").prop("checked", enabled);
				if (enabled)
					$("#paymentOption_deposit").parents("tr").addClass("myownr_options");
				else $("#paymentOption_deposit").parents("tr").removeClass("myownr_options");
			}

		</script>';
		
		//Reservation Price
		//-----------------------------------------------------------------------------------------------------------------
		$output .= '
		'.myOwnUtils::displayInfo(myOwnHelp::$tabs['prodFamily']['editPriceTab'], false).'
		
	<div class="panel">
		<h3><i class="icon-money"></i> '.$obj->l('Reservation price', 'products').'</h3>
		'.myOwnUtils::displayInfo(myOwnHelp::$sections['editPriceTab']['price'], false).'
		
			';
			
			//Calculation						
			//-----------------------------------------------------------------------------------------------------------------
			$fixedLength=Tools::getValue('productReservationLengthType', ($value->_reservationMinLength==$value->_reservationMaxLength));

			$output .= '
			<table cellspacing="0" cellpadding="0" style="float:right;width:100%; margin-bottom:0px;" align="left" class="table" id="panelPrice" type="single">
			<thead>
				<tr class="nodrag nodrop">
					<th class="center" width="20"></th>
					<th colspan="2">'.$obj->l('Length kind', 'products').'</th>
					<th width="30%">'.$obj->l('Price options', 'products').'</th>
					<th width="30%">'.$obj->l('Price Unit', 'products').'</th>
				</tr>			
			</thead><tbody>';
				foreach (myOwnLang::$reservPriceType AS $reservPriceTypeKey => $reservPriceTypeVal) {
					$output .= '
				<tr height="20px">
					<td width="20px" height="40px">
						<input type="radio" onchange="listElem(\'panelPrice\');priceControlOption()" id="priceControl'.$reservPriceTypeKey.'" class="listElem" name="reservationPriceType" value="' . $reservPriceTypeKey. '" '.(Tools::getValue('reservationPriceType', $value->_reservationPriceType)==$reservPriceTypeKey ? 'checked' : '').'>
					</td>
					<td width="20px">
						<img style="top:-8px" src="../modules/'.$obj->name.'/img/price'.$reservPriceTypeKey.'.png">
					</td>
					<td style="padding:8px;">
						<b>' . $reservPriceTypeVal . '</b>
						'.myOwnUtils::displaySubInfo(myOwnHelp::$options['editPriceTab']['price'][$reservPriceTypeKey]).' 
					</td>';
					//Table Option						
					//-----------------------------------------------------------------------------------------------------------------
					if ($reservPriceTypeKey==reservation_price::PRODUCT_TIME) $output .= '<td>'.myOwnUtils::getBoolField($obj, 'priceCombination', Tools::getValue('priceCombination', $value->_priceCombination)==1, $obj->l('Price combination once', 'products'), 'width:100%;text-align:left').'</td>';
					elseif ($reservPriceTypeKey==reservation_price::TABLE_RATE) $output .= '<td>'.myOwnUtils::getBoolField($obj, 'coefPerProduct', Tools::getValue('coefPerProduct', $value->_coefPerProduct)==1, $obj->l('Per product', 'products'), 'width:100%;text-align:left').'</td>';
					else if ($reservPriceTypeKey==reservation_price::PRODUCT_DATE) $output .= '<td>'.myOwnUtils::getBoolField($obj, 'priceDoubleIfLength', Tools::getValue('priceDoubleIfLength', $value->_priceDoubleIfLength)==1, $obj->l('Price double if length', 'products'), 'width:100%;text-align:left').'</td>';
					else if ($reservPriceTypeKey==reservation_price::TABLE_TIME) 
						$output .= '
					<td rowspan="2" style="background-color:rgb(248, 248, 248);color:rgb(102, 102, 102);">	
						'.myOwnUtils::getBoolField($obj, 'priceTaxes', Tools::getValue('priceTaxes',Configuration::get('MYOWNRES_PRICE_TAXEXCL'))==1, $obj->l('Set price without taxes', 'products'), 'width:100%;text-align:left').'
					</td>';
					
					//Price Length						
					//-----------------------------------------------------------------------------------------------------------------
					if ($reservPriceTypeKey==reservation_price::PRODUCT_DATE) $output .= '<td></td>';
					else if ($reservPriceTypeKey==reservation_price::PRODUCT_TIME) {
						$output .= '
					<td rowspan="4" style="background-color:rgb(248, 248, 248);padding: 3px;" id="priceLengthOptions">
						<ol style="margin-bottom:0px;padding:0px;">
							<li class="myowndlist" style="padding: 2px;">
								<label class="sub-li-label" style="width:80px;padding-top:7px;">' . $obj->l('Unit', 'products') .'</label>
								<select style="display:inline-block;width:50%" onChange="priceUnitOption();" id="priceUnit" name="productPriceUnit" >
									<option value="">'.$obj->l('Same as length', 'products').'</option>';
									foreach (myOwnReservationsController::$reservationUnitKeys as $priceUnitTypeKey) {
										$output .= '
										<option value="' . $priceUnitTypeKey. '" '.(!$value->_priceUnitSameAsLength && $value->_priceUnit==$priceUnitTypeKey ? ' selected' : '').'>' . ucfirst(myOwnLang::getUnit($priceUnitTypeKey)). '</option>';
									}
									$output .= '
								</select>
							</li>
							<li class="myowndlist" style="padding: 2px;">
								<label class="sub-li-label" style="width:80px;padding-top:7px;">
									<span class="mylabel-tooltip" style="text-align: right;" title="'.myOwnHelp::$extras['editPlanningTab']['price']['label'].'">' . $obj->l('Label', 'products'). '</span>
								</label>
								<div style="display:inline-block;height: 28px;">
								';
								foreach ($languages as $language)
									$output .= '
									<div id="ReservationMsgA_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
										<input size="10" type="text" name="priceLabel_'.$language['id_lang'].'" id="ReservationAInput_'.$language['id_lang'].'" value="'.stripslashes(Configuration::get('MYOWNRES_PRICELABEL_'.$value->sqlId, $language['id_lang'])).'" /> 
									</div>';
								$output .= '
									'.myOwnUtils::getLanguageFlags($obj, 'changePriceLanguage', 'ReservationMsg').'
									<div style="clear:both"></div>
								</div>
								
							</li>
						';

										
					//Price Length Extra options				
					//-----------------------------------------------------------------------------------------------------------------
						$output .= '
						<li class="myowndlist" style="margin-bottom:0px;color: rgb(102, 102, 102);padding: 2px;">
							<label class="sub-li-label" style="width:80px">' . $obj->l('Exclude', 'products') .'</label>
							<table class="panel" style="margin-bottom: 0px">
								<tbody>
								<tr>
									<td>
										<input name="priceHolidays" type="checkbox" value="1" '.((int)$value->_priceHolidays==0 ? 'checked' : '').' />
									</td>
									<td>
										<img src="../modules/'.$obj->name.'/img/calendar-holiday-small.png"> '. $obj->l('Holidays periods', 'products').'
									</td>
								</tr>
								<tr class="priceDisabled">
									<td>
										<input name="priceDisabledTimeSlots" type="checkbox" value="1" '.($value->_priceDisabledTimeSlots!=1 ? 'checked' : '').' />
									</td>
									<td><img class="priceDisabled" src="../modules/'.$obj->name.'/img/calendar-delete-small.png"> <span>'.($value->_priceUnit == reservation_interval::TIMESLOT ? $obj->l('Disabled time slots', 'products') : $obj->l('Disabled days', 'products') ).'</span>
									</td>
								</tr>
								<tr class="priceEnd priceLengthExtraOptions">
									<td>
										<input class="addTimeSlotsDay" name="priceEndTimeSlot" type="checkbox" value="1" '.(Tools::getValue('priceEndTimeSlot', $value->_priceEndTimeSlot) ? 'checked' : '').'/>
									</td>
									<td>
										<img src="../modules/'.$obj->name.'//img/calendar-exceed-small.png"> <span class="mylabel-tooltip" title="'.myOwnHelp::$extras['editPlanningTab']['price']['end'].'">'.($value->_priceUnit == reservation_interval::TIMESLOT ? $obj->l('End time slot', 'products') : $obj->l('End day', 'products') ).'</span>
									</td>
								</tr>
								</tbody>
							</table>
						</li>
						<div class="priceLengthExtraOptions" '.($value->_reservationPriceType!=reservation_price::TABLE_DATE ? '' : 'style="display:none"').'>
							<li class="myowndlist priceStrict" style="padding-bottom:0px;margin-bottom:0px;color: rgb(102, 102, 102);'.($value->_priceUnit==0 ? 'display:none ' : '').'">
								<label class="sub-li-label" style="width:80px;height:18px">
									<span class="mylabel-tooltip" title="'.myOwnHelp::$extras['editPlanningTab']['price']['strict'].'">' . $obj->l('Calculation', 'products'). '</span>
								</label>
								<input class="" name="strictPrice" type="checkbox" value="1" '.(Tools::getValue('strictPrice', $value->_priceStrict)==1 ? 'checked' : '').'/>&nbsp;'. $obj->l('Count number of', 'products').' <span class="priceUnitLbl">'.myOwnLang::getUnit($value->_priceUnit, true).'</span> '.$obj->l('used instead of the length', 'products').'
							</li>
						</div>
						';
						$output .= '
						</ol>
					</td>';
						}
					$output .= '
				</tr>';
				}
				$output .= '
			</tbody></table>
			
			<p class="help-block" id="tablerateshelp" style="padding-left:32px"><i class="icon-warning-sign"></i>&nbsp;' . $obj->l('Tables rates can only work with a limited or fixed length.', 'products'). '</p>
				';


			$output .= '

		</ol>';

		$output .= '
		<div style="clear:both"></div>
	</div>
	<div class="panel">
		<h3><i class="icon-credit-card"></i> '.$obj->l('Payment', 'products').'</h3>
		'.myOwnUtils::displayInfo(myOwnHelp::$sections['editPriceTab']['payment'], false).'';

		//price options				
		//-----------------------------------------------------------------------------------------------------------------
		$output .= '
		<table cellspacing="0" cellpadding="0" style="width: 100%; margin-bottom:10px;" class="table" id="panelFee" type="multiple">
			<tbody>
			<tr>
				<td width="20">
					<input type="checkbox" name="paymentOptions[]" id="paymentOption_advance" class="listElem" value="1" '.((intval($value->_advanceRate)<100 || intval($value->_advanceAmount)>0) ? 'checked' : '').' onclick="listElem(\'panelFee\');" rel="false" class="noborder">
				</td>
				<td>
					<div class="listElemDescription" >
						'.MyOwnReservationsUtils::getsubtitle($obj->l('Advance', 'products')).'
						<p class="desc">'.$obj->l('The customer will pay only a part of reservations amount', 'products').'</p>
						'.myOwnUtils::displaySubInfo(myOwnHelp::$extras['editPlanningTab']['price']['advance']).'
					</div>
				</td>
				<td width="45%"><ol style="padding:0px;margin-bottom:0px;">
						<li class="myowndlist">
							<label class="sub-li-label" style="width: 100px;">' . $obj->l('Advance', 'products') .'</span></label>
							'.myOwnUtils::getInputField('resaAdvanceRate', Tools::getValue('resaAdvanceRate', intval($value->_advanceRate)), '%', 'height: 28px;width:50px', 'upsInput', true, 'fixedAdvanceChanged();').'
						</li>
						<li class="myowndlist">
							<label class="sub-li-label" style="width: 100px;"></label>
							'.myOwnUtils::getInputField('resaAdvanceAmount', Tools::getValue('resaAdvanceAmount', intval($value->_advanceAmount)), '', 'height: 28px;width:50px', 'upsInput', true, 'fixedAdvanceChanged();').'
						</li>
				</ol></td>
			</tr>
			<tr>
				<td width="20">
					<input type="checkbox" name="paymentOptions[]" id="paymentOption_deposit" class="listElem" value="1" '.((intval($value->_depositAmount)>0 || $value->_depositByProduct>0) ? 'checked' : '').' onclick="listElem(\'panelFee\');" rel="false" class="noborder">
				</td>
				<td>
					<div class="listElemDescription" >
						'.MyOwnReservationsUtils::getsubtitle($obj->l('Deposit', 'products')).'
						<p class="desc">'.$obj->l('A deposit amount is asked when reservations are ordered', 'products').'</p>
						'.myOwnUtils::displaySubInfo(myOwnHelp::$extras['editPlanningTab']['price']['deposit']).'
					</div>
				</td>
				<td width="45%"><ol style="padding:0px;margin-bottom:0px;">
					<li class="myowndlist" style="height:38px">
						<label class="sub-li-label" style="width: 100px;">' . $obj->l('Amount', 'products') .'</label>
						<div style="display:inline-block;width:80px">
							'.myOwnUtils::getInputField('resaDepositAmount', Tools::getValue('resaDepositAmount', intval($value->_depositAmount)), '', 'width:50px;height: 28px;display:inline-block', 'upsInput', true, 'depositChanged();').' </div>
						<div style="float:right">
							<input style="display:inline-block" onChange="depositChanged();" id="depositByProduct" name="depositByProduct" type="checkbox" value="1" '.(Tools::getValue('depositByProduct', $value->_depositByProduct) ? 'checked' : '').'/>&nbsp;'.$obj->l('Set by product', 'products').'
						</div>
						
					</li>
					<li class="myowndlist">
						<label class="sub-li-label" style="width: 100px;">'.$obj->l('Calculation', 'products').'</label>
						<select style="width:50%;height: 28px;" name="resaDepositCalc">';
							foreach (myOwnLang::$depositTypes AS $depositKey => $depositType){
								$output .= '
								<option value="' . $depositKey. '" '.(Tools::getValue('resaDepositCalc', $value->_depositCalc)==$depositKey ? 'selected' : '').'>' . $depositType . '</option>';
							}
				$output .= '
						</select>
					</li>
				</ol></td>
			</tr>
			';
			$exts = $obj->_extensions->getExtByType(extension_type::FEE);
			foreach ($exts as $ext) {
				$output .= '
				<tr style="">
					<td width="20">
						<input type="checkbox" name="paymentOptions[]" class="noborder listElem" id="paymentOption_'.$ext->name.'" value="'.$ext->id.'" onclick="listElem(\'panelFee\');" '.($ext->isConfigFee($obj, $value) ? 'checked' : '').' rel="false" >
					</td>
					<td>
						<div class="listElemDescription" >
							'.MyOwnReservationsUtils::getsubtitle($ext->feeTitle).'
							<p class="desc">'.$ext->feeDescr.'</p>
						</div>
					</td>
					<td width="45%">'.(method_exists($ext, 'displayConfigFee') ? $ext->displayConfigFee($obj, $value) : '').'</td>
				</tr>
				';
				}
			
			$output .= '
		</tbody>
		</table>
	</div>';
		

		//Validation						
		//-----------------------------------------------------------------------------------------------------------------
		$output .= '
	<div class="panel">
		<h3><i class="icon-check-square-o"></i> '.$obj->l('Validation', 'products').'</h3>
		'.myOwnUtils::displayInfo(myOwnHelp::$sections['editPriceTab']['validation'], false).'

		<ol style="clear:both;padding-left:0px;margin:0px">
			<li class="myowndlist">
				<label class="li-label">' . $obj->l('Statuses', 'products') . '</label>
				<select style="width:50%;float:left" id="reservationStatus" name="reservationStatus[]" multiple>';
		  			$orderStatuss = Db::getInstance()->ExecuteS("SELECT * FROM `" . _DB_PREFIX_ . "order_state_lang` WHERE `id_lang` = ".$cookie->id_lang);
				foreach($orderStatuss AS $orderStatus) {
					$output .= '<option value="' . $orderStatus['id_order_state']. '"';
					if (in_array($orderStatus['id_order_state'], explode(',',$value->_optionValidationStatus)))
						$output .= ' selected';
					$output .= '>' . $orderStatus['name'] . '</option>';
				}
				$output .= '
				</select>
			</li>
			<p class="preference_description" style="clear:both;width:100%">('.$obj->l('Select multiple order statuses (by keeping ctrl key pushed) that will validate the reservation, others will be unvalidate', 'products').')</p>
		</ol>
	</div>';

		return $output;
	}
	
	public static function editRatesTab($cookie,$obj,$value) {
		$output = '';
		$out = '<div class="panel">
		<h3><i class="icon-table"></i> '.$obj->l('Global rates', 'products').'</h3>';
		if ($value->_saved)
			$out .= ''.myOwnReservationsPriceSetsController::edit($cookie, $obj, 0, $value);
		else $out .= $obj->l('Save the product family to edit rates', 'products');
		return $out.'</div>';
	}
	public static function editTimeslotsTab($cookie,$obj,$value) {
		$timeslotsArray = $value->_timeslotsTab;
		$out = '';//self::gettitle(ucfirst(myOwnLang::$objects[MYOWN_OBJ::TIMESLOT]), true).'<div class="separation"></div>';
		if ($value->_saved)
			$out .= '<div class="separation"></div>'.myOwnReservationsTimeslotsController::show('family', $obj, $value->sqlId, null, $timeslotsArray);
		else $out .= $obj->l('Save the product family to add a new timeslot', 'products');
		return $out;
	}
	
	public static function editNotifTab($cookie,$obj,$value) {
		$output='';
		//Product information				
		//-----------------------------------------------------------------------------------------------------------------	
		$output .= '
		
			
	<div class="panel">
		<h3><i class="icon-bell"></i> '.$obj->l('Notifications', 'products').'</h3>
		'.myOwnUtils::displayInfo(myOwnHelp::$tabs['prodFamily']['editNotifTab'], false).'

		<table cellspacing="0" cellpadding="0" style="width: 100%; margin-bottom:10px;" class="table" id="panelNotif" type="multiple">
			<thead>
				<tr class="nodrag nodrop">
					<th class="center" width="20">
					</th>
					<th colspan="2"></th>
					<th width="45%"></th>
				</tr>			
			</thead>
			<tbody>
			<tr>
				<td width="20">
					<input type="checkbox" name="notifications[]" class="listElem" value="1" '.(is_array($value->ids_notifs) && in_array(1, $value->ids_notifs) ? 'checked' : '').' onclick="listElem(\'panelNotif\');" rel="false" class="noborder">
				</td>
				<td width="30px"><img class="imgm" alt="" src="../modules/myownreservations/img/email.png"></td>
				<td>
					<div class="listElemDescription" >
						'.MyOwnReservationsUtils::getsubtitle($obj->l('Order details e-mail', 'products')).'
						<p class="desc">'.$obj->l('Send an email with order details', 'products').'</p>
					</div>
				</td>
				<td><ol style="padding:0px;margin-bottom:0px;">
					<li class="myowndlist">
						<label for="name" class="myowndeliveriesSettingsLabel" style="width:130px">
							<span class="mylabel-tooltip" title="'.myOwnHelp::$sections['editNotifTab']['email'].'">'.$obj->l('Email', 'products').'</span>
						</label>
						
						'.myOwnUtils::getInputField('productEmail', Tools::getValue('productEmail',$value->email), '<i class="icon-envelope-o"></i>', 'width:300px').'
					</li>
				</ol></td>
			</tr>
			';
			$exts = $obj->_extensions->getExtByType(extension_type::NOTIFICATION);
			foreach ($exts as $ext) {
				$output .= '
				<tr style="'.(!$ext->enabled || $ext->needconfig ? 'background-color:#EEE' : '').'">
					<td width="20">
						<input type="checkbox" name="notifications[]" class="noborder listElem" '.(!$ext->enabled || $ext->needconfig ? 'disabled' : '').' value="'.$ext->id.'" onclick="listElem(\'panelNotif\');" '.(is_array($value->ids_notifs) && in_array($ext->id, $value->ids_notifs) ? 'checked' : '').' rel="false" >
					</td>
					<td><img class="imgm" alt="" src="../modules/myownreservations/'.$ext->getImg().'"></td>
					<td>
						<div class="listElemDescription" >
							'.MyOwnReservationsUtils::getsubtitle($ext->title).'
							<p class="desc">'.$ext->description.'</p>
						</div>
					</td>
					<td>'.(method_exists($ext, 'displayConfigNotif') ? $ext->displayConfigNotif($obj, $value->sqlId) : '').'</td>
				</tr>
				';
				}
			
			$output .= '
			</tbody>
		</table>
	</div>';
	
		return $output;
	}

	public static function editOptionsTab($cookie,$obj,$value) {
		$output='';
		//Product information				
		//-----------------------------------------------------------------------------------------------------------------	
		$output .= '
		

	<div class="panel">
		<h3><i class="icon-puzzle-piece"></i> '.$obj->l('Options', 'products').'</h3>
		'.myOwnUtils::displayInfo(myOwnHelp::$tabs['prodFamily']['editOptionsTab'], false).'

		<table cellspacing="0" cellpadding="0" style="width: 100%; margin-bottom:10px;" class="table" id="panelOption" type="multiple">
			<thead>
				<tr class="nodrag nodrop">
					<th class="center" width="20">
					</th>
					<th colspan="2"></th>
					<th width="45%"></th>
				</tr>			
			</thead>
			<tbody>
			';
			$exts = $obj->_extensions->getExtByType(extension_type::OBJECT);
			foreach ($exts as $ext) {
				$output .= '
				<tr style="'.(!$ext->enabled ? 'background-color:#EEE' : '').'">
					<td width="20">
						<input onclick="listElem(\'panelOption\');;occupyOption()" id="option'.$ext->id.'" type="checkbox" name="options[]" class="option noborder listElem" '.(!$ext->enabled ? 'disabled' : '').' value="'.$ext->id.'" '.(is_array($value->ids_options) && in_array($ext->id, $value->ids_options) ? 'checked' : '').' rel="false" >
					</td>
					<td width="30"><img class="imgm" alt="" src="../modules/myownreservations/'.$ext->getImg().'"></td>
					<td>
						<div class="listElemDescription" >
							'.MyOwnReservationsUtils::getsubtitle($ext->title).'
							<p class="desc">'.$ext->description.'</p>
						</div>
					</td>
					<td>'.(method_exists($ext, 'displayConfigOptions') ? $ext->displayConfigOptions($obj, $value) : '').'</td>
				</tr>
				';
				}
			
			$output .= '
			</tbody>
		</table>
	</div>';
	
		return $output;
	}

	
	public static function edit($cookie,$obj,$id) {
		$output = '';
		$days = myOwnLang::getDaysNames($cookie->id_lang);
// 		$obj->checkIsProVersion();
		if (!$obj->isProVersion) $id=0;
		if (array_key_exists($id, $obj->_products->list)) {	
			$value = $obj->_products->list[$id];
			$value->_saved=true;
      		if ($value->_qtyCapacity>1) $output.=myOwnUtils::displayWarn($obj->l('Your product capacity is greater than 1 you\'ve to allow order of product out of stock', 'products'));
      		if ($value->reservationPeriod!='' && $value->reservationPeriod==0) $output.=myOwnUtils::displayWarn($obj->l('Reservation are disabled with a reservation period at 0', 'products'));
      		if ($value->reservationPeriod=='') $value->reservationPeriod=100;
      		if ($value->_reservationMaxLength==0) $value->_reservationMaxLength=50;
      		
        } else {
        	$value = new myOwnProductsRule();
        	$value->_saved=false;
        	$value->reservationPeriod=100;
        	$value->_qtyCapacity=1;
        	$value->_lengthHolidays=1;
        	$value->_lengthDisabledTimeSlots=1;
        	$value->_allowResOnHolidays=1;
        	$value->_allowResOnRestDays=1;
        	$value->_reservationMinLength=1;
        	$value->_reservationMaxLength=50;
        	$value->_priceUnit='';
        	$value->_priceUnitSameAsLength=true;
        	$value->_optionValidationStatus='2, 3, 4, 5, 12';
        }
					
		//$exts .= $obj->_extensions->getConfigProduct($id);
		
		$subtabs = array(
			'editInfoTab'=>$obj->l('Informations', 'products'),
			'editPlanningTab'=>$obj->l('Planification', 'products'),
			'editTimeslotsTab'=>ucfirst(myOwnLang::$objects[MYOWN_OBJ::TIMESLOT]),
			'editPriceTab'=>$obj->l('Payment', 'products'),
			'editRatesTab'=>$obj->l('Global Rates', 'products')
		);
		if (count($obj->_extensions->getExtByType(extension_type::OBJECT))>0)
			$subtabs['editOptionsTab']=$obj->l('Options', 'products');
		$subtabs['editNotifTab']=$obj->l('Notification', 'products');
		if (!MYOWNRES_PRE_RELEASE) $subtabs += $obj->_extensions->getConfigProduct($id);
		
		$key_tab = Tools::getValue('key_tab' , 'editInfoTab');
		$output .= '
		<script language="javascript"> 

			
		$(document).ready(function() {
			$(".panel").each(function( index ) {
				listElem($(this).attr("id"));
			});
			$(".tab-pane").hide();
			if (window.location.href.indexOf("#")===-1) {
				$("#'.$key_tab.'").show();
				$("#'.$key_tab.'-pane").addClass("selected");
				$("#link-'.$key_tab.'").addClass("active");
			} else {
				var tab = window.location.href.split("#");
				if (tab[1].indexOf("=")!==-1) {
					var tabb = tab[1].split("=");
					$("#"+tabb[1]).show();
					$("#"+tabb[1]+"-pane").addClass("selected");
					$("#link-"+tabb[1]).addClass("active");
				}
			}
			
			listElem("panelLength");
		    listElem("panelOccupation");
		    listElem("panelSelection");
		    listElem("panelPrice");
		    listElem("panelFee");
		    listElem("panelQty");
		    listElem("panelNotif");
		    listElem("panelOption");
		    
			lengthControlOption();
			intervalTypeOption(true);
			
			lengthControlOption();
			modeControlOption();
			lengthUnitOption();
			
			occupyOption();
		    qtyOption();
		    
		    priceUnitOption();
			depositChanged();
			fixedAdvanceChanged();
			priceControlOption();

		    $("#productAlsoSell").removeAttr("disabled");
		});
		</script>';
		
		if (_PS_VERSION_ < "1.6.0.0") {
			$output .= '
			<div class="leadin" sttyle="clear:both"></div>
			<div>
				<div class="productTabs" style="">
					<ul class="tab">';
						foreach ($subtabs as $key => $subtab) $output .= '
						<li class="tab-row" style="cursor:pointer">
							<a class="tab-page" id="'.$key.'-pane" onclick="tabSel(\''.$key.'\');">'.$subtab.'</a>
						</li>';
						$output .= '
					</ul>
				</div>
			</div>';
		} else {
			$output .= '
			<div class="row bootstrap">
				<div class="productTabs col-lg-2">
					<div class="list-group">';
				foreach ($subtabs as $key => $subtab) $output .= '
						<a class="list-group-item " id="link-'.$key.'" onclick="tabSel(\''.$key.'\');">'.$subtab.'</a>';
				$output .= '
					</div>
				</div>';
		}
		$output .= '
		<div id="product-tab-content-wait" style="display: none;"><div id="loading">Chargement...</div></div>
		<div id="modules_tab_list">
			<form class="'.(_PS_VERSION_ > "1.6.0.0" ? 'form-horizontal col-lg-10' : '' ).'" id="'.(_PS_VERSION_ > "1.5.3.0" ? 'editProductForm' : 'product_form' ).'" action="'.myOwnReservationsController::getConfUrl('products') . '" method="post" name="editProductForm" style="">
				<input type="hidden" name="key_tab" id="key_tab" value="'.$key_tab.'">';
				//if ($id > 0 or $id==-1) 
				$output .= '<input type="hidden" name="idProduct" value="'.$id.'" />';
				
				foreach ($subtabs as $key => $subtab) {
					if (_PS_VERSION_ >= "1.6.0.0") $output .= '
					<div id="'.$key.'"  class="tab-pane" >
						<div class="product-tab" style="display: block;">';
					else $output .= '	
					<div class="tab-pane" id="'.$key.'">
						<div class=" product-tab-content" style="display: block;">';
						
						if (stripos($key, 'edit')!==false) 
							$output .= self::$key($cookie,$obj,$value);
						else $output .= $obj->_extensions->getExtByName($key)->displayConfigProduct($cookie,$obj,$value);
						$output .= '
					</div>
				</div>';
				}
				
				$output .= '
			</form>
		</div>
		';
		if (_PS_VERSION_ >= "1.6.0.0") $output .= '</div>';

		
		return $output;
	}
	
}



?>