<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{myownreservations}prestashop>calendarview_a4c5f923f4d204f7111c606557bcb67c'] = 'from';
$_MODULE['<{myownreservations}prestashop>cart_d3c6679ddfc12c848374e3cfe8f682bc'] = 'On';
$_MODULE['<{myownreservations}prestashop>cart_5e097e832e842584f43e7cdc4c3d3db6'] = 'to';
$_MODULE['<{myownreservations}prestashop>cart_fd3ce4d85e2561f15a053cd3466df8ee'] = 'from';
$_MODULE['<{myownreservations}prestashop>cart_ae56f7acda8dbdff0d779c8a3191e7fd'] = 'From';
$_MODULE['<{myownreservations}prestashop>cart_b762a663d1ed778e76650808c118206c'] = 'To';
$_MODULE['<{myownreservations}prestashop>cart_ae83f1113814e4b9c31259fb4e29d403'] = 'Until';
$_MODULE['<{myownreservations}prestashop>pricerule_ae56f7acda8dbdff0d779c8a3191e7fd'] = 'From';
$_MODULE['<{myownreservations}prestashop>pricerule_48989d6051749c4d92e246360e66f041'] = 'to';
$_MODULE['<{myownreservations}prestashop>pricerules_ae56f7acda8dbdff0d779c8a3191e7fd'] = 'From';
$_MODULE['<{myownreservations}prestashop>pricerules_475852b18caeae6e8dbccb0adb1d43e5'] = 'to';
$_MODULE['<{myownreservations}prestashop>pricerules_48989d6051749c4d92e246360e66f041'] = 'to';
$_MODULE['<{myownreservations}prestashop>reservations_1b1ded4593ffec17c18eeadbc64f5e23'] = 'by';
$_MODULE['<{myownreservations}prestashop>reservations_d3c6679ddfc12c848374e3cfe8f682bc'] = 'On';
$_MODULE['<{myownreservations}prestashop>reservations_fd3ce4d85e2561f15a053cd3466df8ee'] = 'from';
$_MODULE['<{myownreservations}prestashop>reservations_5e097e832e842584f43e7cdc4c3d3db6'] = 'to';
$_MODULE['<{myownreservations}prestashop>reservations_ae56f7acda8dbdff0d779c8a3191e7fd'] = 'From';
$_MODULE['<{myownreservations}prestashop>reservations_b762a663d1ed778e76650808c118206c'] = 'To';
$_MODULE['<{myownreservations}prestashop>script_cart_3e0312480da710c5e7334f6fe4307bda'] = 'please select a end timeslot';
$_MODULE['<{myownreservations}prestashop>script_cart_0014bf6a95ce140481692b8a8364a115'] = 'please select a timeslot';
$_MODULE['<{myownreservations}prestashop>script_cart_42deef424ca18554b8bc6b15ed731876'] = 'please select a start timeslot';
$_MODULE['<{myownreservations}prestashop>lang_210a679dd3a6df4e9505ec2f6af0e8d2'] = 'new';
$_MODULE['<{myownreservations}prestashop>lang_9b75199f8594bf2d7c2faab00d5b4eb3'] = 'The';