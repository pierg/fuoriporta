<?php
/*
* 2010-2012 LaBulle All right reserved
*/

require_once(_PS_MODULE_DIR_ . "myownreservations/myownreservations.php");
require_once(MYOWNRES_PATH . "controllers/admin.php");
require_once(MYOWNRES_PATH . "controllers/planning.php");
require_once(MYOWNRES_PATH . "controllers/ajax_admin.php");


class adminReservationStock extends AdminController
{
	var $module = "myownreservations";
	var $name = "myownreservations";
	var $multishop_context_group;
	var $multishop_context;
	var $moduleObj;
	var $ruleId;
	var $insertedId=0;

    public function __construct()
    {
        global $cookie, $_LANGADM;

        $this->moduleObj = Module::getInstanceByName('myownreservations');
        $this->moduleObj->checkPro();
        //if (!is_file(_PS_MODULE_DIR_ ."myownreservations/pro/products.php")) 
        $this->multishop_context = true;
        myOwnReservationsController::_setInfos ($this->moduleObj);
        myOwnReservationsController::_construire($this->moduleObj);
		$this->display = 'view';
		$this->class_name = 'adminReservationStock';
		$this->bootstrap=true;
		$this->doActions();
        parent::__construct();
    }
    
    public function displayErrors() {
	    foreach($this->_errors as $err) echo $err;
    }
    
    public function initPageHeaderToolbar()
	{
		global $cookie;
		$tab = Tools::getValue('myOwnResTab');
		$ctrl = strtolower(Tools::getValue('controller'));
		$field='';
		if ($tab=='timeslots')
			$fieldl = 'TimeSlot';
		if ($tab=='pricesrules') 
			$field = 'Pricesrule';
		if ($tab=='availability') 
			$field = 'Availability';
		if ($ctrl=='adminreservationrules') {
			$tab='products';
			$field = 'Product';
		}
		
		if ($this->ruleId > 0) $this->page_header_toolbar_title = ucfirst(myOwnLang::$object[MYOWN_OBJ::RESVRULE]).' #'.$this->ruleId;
		else $this->page_header_toolbar_title = ucfirst(myOwnLang::$object[MYOWN_OBJ::RESVRULE]);

		$this->page_header_toolbar_btn = myOwnReservationsController::getButtonsList( $this->moduleObj, $this->ruleId);

		if (_PS_VERSION_ >= "1.6.0.0") parent::initPageHeaderToolbar();
		$this->context->smarty->assign('help_link', null);
	}
	

	
	public function doActions()
	{
		global $cookie;
		$obj = $this->moduleObj;

		$operation='';
		$this->insertedId=0;
		$objet = $obj->l('The reservation rules', 'adminreservationrules');
		$male = true;
		$errors=array();$plur=false;
		$isproversion=is_file(_PS_MODULE_DIR_ .$obj->name."/pro/products.php");
		




		$male=true;
		if ($operation != "") {
			if ($result) $this->confirmations[] = $objet.' '.myOwnLang::getResult($obj, true, !$plur).' '.$operation;
			else $this->errors[] = $objet.' '.myOwnLang::getResult($obj, false, !$plur).' '.$operation.' : '.Db::getInstance()->getMsgError();

			myOwnReservationsController::_construire($obj, true);
		}
		
		$this->ruleId = intval(Tools::getValue('editPricesrule', -1));
		if (!$isproversion) $this->ruleId=-1;
		if ($this->ruleId==0) $this->ruleId = $this->insertedId;
		
	} 


    public function renderView()
	{
		global $cookie;
		$htmlOut='';
		$obj = $this->moduleObj;
		if (_PS_VERSION_ < "1.6.0.0") {
			$this->initPageHeaderToolbar();
			$buttons = MyOwnReservationsUtils::getButtonsList($this->page_header_toolbar_btn, $this->ruleId);
		} else $buttons='';
		//$buttons = myOwnReservationsController::getButtons($cookie, $obj, $this->ruleId, false, 'Product',  'products');
		
		foreach($this->errors AS $error) {
			$htmlOut .= myOwnUtils::displayError($error);
		}
		foreach($this->confirmations AS $confirmation) {
			$htmlOut .= myOwnUtils::displayConf($confirmation);
		}
		$this->errors=array();
		$this->confirmations=array();

		
		$header = MyOwnReservationsUtils::displayIncludes();
		if (!file_exists(_PS_ROOT_DIR_."/override/classes/Tab.php")) $header .= myOwnReservationsController::getModuleHeader($obj, $cookie);
		
			require_once(MYOWNRES_PATH . "controllers/admin.php");
			$id = intval(Tools::getValue('editStock', 0));
			
			$content = '';
			if ($id==-1) $title = ucfirst(myOwnLang::$objects[MYOWN_OBJ::STOCKITEM]);
			if ($id>0) $title = ucfirst(myOwnLang::$object[MYOWN_OBJ::STOCKITEM]).' #'.$id;
			$_products = $obj->_products;
			
			//get allowed product family
			$employee = new Employee($cookie->id_employee);
			$rights = Profile::getProfileAccess($employee->id_profile, Tab::getCurrentTabId());
			if ($employee->id_profile>1) {
				$_products->list = $_products->getListForEmployee($employee->id);
			}
		
				//Date selection
				//-----------------------------------------------------------------------------------------------------------------
				$opt=""; $dayStr="";$view="";

				$req_day = '';
				if ((int)$cookie->myown_month == 0 || (int)$cookie->myown_year == 0)
				{
					$cookie->myown_dayOfWeek = date('N');
					$cookie->myown_weekOfYear = date('W');
					$cookie->myown_month = date('m');
					$cookie->myown_year = date('Y');
				}
				$day = myOwnCalendarWeek::getDayOfWeek($cookie->myown_dayOfWeek, $cookie->myown_weekOfYear, $cookie->myown_year);
				
				if (Tools::getIsset('day')) $req_day = Tools::getValue('day');
				if (Tools::getIsset('datepickerFrom')) $req_day = Tools::getValue('datepickerFrom');
				if (Tools::getIsset('submitDateDayPrev')) $opt = ' -1 day';
				if (Tools::getIsset('submitDateDayNext')) $opt = ' +1 day';
				if ($req_day != '') $day = strtotime($req_day);
				if ($opt != '') $day = strtotime($opt, $day);
				
				if (date('Y-m-d') != date('Y-m-d', $day))
				{
					$cookie->myown_dayOfWeek = date('N', $day);
					$cookie->myown_weekOfYear = date('W', $day);
					$cookie->myown_month = date('m', $day);
					$cookie->myown_year = date('Y', $day);
				}

				$productFamillyFilter = "";
				if (Tools::getValue('products','')!='') $_SESSION["myown_familyFilter"] = Tools::getValue('products','');
				if (isset($_POST['productFamillyFilter'])) $_SESSION["myown_familyFilter"] = Tools::getValue('productFamillyFilter','');
				if (!isset($_SESSION["myown_familyFilter"])) $_SESSION["myown_familyFilter"] = "";
				$productFamillyFilter = intval($_SESSION["myown_familyFilter"]);
			
				if (!array_key_exists($productFamillyFilter, $_products->list) && intval($productFamillyFilter)>0) {
					if (array_key_exists(0, $_products->list)) $productFamillyFilter=0;
					else $productFamillyFilter=array_shift(array_values($_products->list))->sqlId;
				}
				if ($productFamillyFilter>0) $_timeSlots = $_products->list[$productFamillyFilter]->_timeslotsObj;
		

			$content .= myOwnReservationsStockController::executeActions($obj);
			if ($id) {
				$content .= myOwnReservationsStockController::displayEdit($cookie, $obj, $_GET["editStock"], $dayStr, $productFamillyFilter);
			} else {
				$dayStr = date('Y-m-d',$day);
				//$content .= myOwnReservationsAdminController::displayPageHeader($obj, $day, 'stock', $productFamillyFilter);
				$content .= myOwnReservationsProController::displayAddStock($obj, $dayStr, $productFamillyFilter);

				$btns=myOwnReservationsPlanningController::displayPeriodView($obj, $productFamillyFilter, '');
				if (_PS_VERSION_ >= '1.6.0.0')
					$content .= MyOwnReservationsUtils::table16wrap($obj, 'stocks', "$('#addStock').toggle('slow');", ucfirst(myOwnLang::$object[MYOWN_OBJ::STOCK]), -1, $btns);
				$content .= myOwnReservationsStockController::bulkActionsDisplay($obj, $day, $cookie);
				$content .= myOwnReservationsStockController::displayList($obj, $dayStr, $productFamillyFilter);
				if (_PS_VERSION_ >= '1.6.0.0')
					$content .= '</div>';
			}
		
		return $header.$content;
  	}
  
}
?>