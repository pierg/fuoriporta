<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{megaattributes}prestashop>megaattributes_3d38631fc60fdfdd78ecb1ff8316ac64'] = 'Mega Atributos';
$_MODULE['<{megaattributes}prestashop>megaattributes_bad3e7539bfc888051df8a5a04a7202c'] = 'Muestra los atributos como botones';
$_MODULE['<{megaattributes}prestashop>megaattributes_8cf04a9734132302f96da8e113e80ce5'] = 'Inicio';
$_MODULE['<{megaattributes}prestashop>megaattributes_fa535ffb25e1fd20341652f9be21e06e'] = 'Configuración';
$_MODULE['<{megaattributes}prestashop>megaattributes_6a26f548831e6a8c26bfbbd9f6ec61e0'] = 'Ayuda';
$_MODULE['<{megaattributes}prestashop>megaattributes_c888438d14855d7d96a2724ee9c306bd'] = 'Opciones Actualizadas';
$_MODULE['<{megaattributes}prestashop>megaattributes_f4f70727dc34561dfde1a3c529b6205c'] = 'Opciones';
$_MODULE['<{megaattributes}prestashop>megaattributes_976a6c8a9b1a03e1487c464b5f9224fa'] = 'Activar Tooltip Atributos';
$_MODULE['<{megaattributes}prestashop>megaattributes_bafd7322c6e97d25b6299b5d6fe8920b'] = 'No';
$_MODULE['<{megaattributes}prestashop>megaattributes_93cba07454f06a4a960172bbd6e2a435'] = 'Si';
$_MODULE['<{megaattributes}prestashop>megaattributes_be6344e71509d07a92feee183c6bba39'] = 'Filtrar Combinaciones';
$_MODULE['<{megaattributes}prestashop>megaattributes_bca5e0b5a6fb41fc9907ab3920a366f0'] = 'Como Mostrar Atributos ';
$_MODULE['<{megaattributes}prestashop>megaattributes_e0626222614bdee31951d84c64e5e9ff'] = 'Select';
$_MODULE['<{megaattributes}prestashop>megaattributes_5b3ec15499a125805b5bbf8e4afcec8c'] = 'Botones';
$_MODULE['<{megaattributes}prestashop>megaattributes_2d12a4113cd614393f82aa7025e62d62'] = 'Muestra Atributos Deshabilitados';
$_MODULE['<{megaattributes}prestashop>megaattributes_7acdf85c69cc3c5305456a293524386e'] = 'Ocultos';
$_MODULE['<{megaattributes}prestashop>megaattributes_3d3f860693971389a045c3580c0e9be5'] = 'Fondo Css';
$_MODULE['<{megaattributes}prestashop>megaattributes_27cace7fe560722b0b6350bd9d5a02ef'] = 'Desabilitar select con solo una opcion';
$_MODULE['<{megaattributes}prestashop>megaattributes_4510ddf668ac7edb6b14c252d9b86a49'] = 'Grupo Imagen Color';
$_MODULE['<{megaattributes}prestashop>megaattributes_3efc7b1eafd3e77ab7db5d112c56085a'] = 'No usar imagenes';
$_MODULE['<{megaattributes}prestashop>megaattributes_93e7c964cc06cd83b24a48aa05fdd9d0'] = 'Categoria Aplica Filtro';
$_MODULE['<{megaattributes}prestashop>megaattributes_28258e7a927711d405af65fbb6d13614'] = 'Categorias Seleccionadas';
$_MODULE['<{megaattributes}prestashop>megaattributes_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
