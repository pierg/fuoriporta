<script type="text/javascript">
// <![CDATA[
var megaattributesimagecolor = {$MA_IMAGE_COLOR};
var megagroupArray = new Array(); 
{if isset($matgroups)}
	{foreach from=$matgroups  item=magroup}
		var matarray = new Array();
		matarray['id_group'] = {$magroup.id_group};
		matarray['is_color'] = {$magroup.is_color};
		megagroupArray.push(matarray);
	{/foreach}
{/if}
{if $MA_FILTER eq 1}

var megaattributesshow = {$MA_SHOW_ATTRIBUTE};
var megaattributesdisable = {$MA_DISABLE_SELECT};
{/if}
//]]>
{if $MA_TOOLTIP eq 1}
{literal}
$(document).ready(function(){
	$('.attribute_list .color_pick').each(function(){
 			if($(this).find('img').length)
 			{
	 			var img = $(this).find('img');
	 			$(this).tooltip({ 
	 				tooltipClass:'colorpicker-tooltip',
					content:'<p><img src="'+$(img).attr('src')+'" alt="'+$(img).attr('alt')+'" /></p><p>'+$(img).attr('alt')+'</p>'
				}); 
			}
			else
			{
				$(this).tooltip();
			}
	});
});
{/literal}
{/if}
</script> 