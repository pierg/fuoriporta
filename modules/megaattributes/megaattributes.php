<?php

if (!defined('_PS_VERSION_'))
	exit;
if (!class_exists('MegaAttributesCore', false)) {
	include _PS_MODULE_DIR_ . 'megaattributes' . DIRECTORY_SEPARATOR . 'MegaattributesCore.php';
}
class MegaAttributes extends MegaAttributesCore
{
	private $_html = '';
	private $_postErrors = array();
	public $id_lang = 1;
	public $aw_key ="megaattributes16";
	function __construct()
	{
		
		$this->name = 'megaattributes';
		$this->tab = 'front_office_features';
		$this->version = '16.0.1.1';
		$this->author = 'Alabazweb Pro';
		$this->need_instance = 0;
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => '1.6');

		parent::__construct();
		
		$this->displayName = $this->l('Mega Attributes');
		$this->description = $this->l('Show attributes with button and disable not posible');
		
		if(_PS_VERSION_<1.5)
		{
			global $cookie;
			$this->_id_lang = $cookie->id_lang;
		}
		else
			$this->_id_lang = Context::getContext()->cookie->id_lang;
		$this->setAwKey('megaattributes16');
			
	}

	function install()
	{
		if (
		!Configuration::updateValue('MA_SHOW_ATTRIBUTE_BTN', '2') OR
		!Configuration::updateValue('MA_DISABLE_SELECT', '0') OR 
		!Configuration::updateValue('MA_IMAGE_COLOR', '0') OR
		!parent::install() OR 
		!$this->registerHook('productFooter') OR 
		!$this->registerHook('productActions') OR
		!$this->registerHook('header'))
			return false;	
		return true;
	}
	
		private function getBaseUrl()
	{
		global $currentIndex;
		return $currentIndex.'&configure=megaattributes&token='.Tools::getValue('token');
	}
	public function _displayLeftMenu()
	{
		$url = $this->getBaseUrl();
		$this->_html .= '
		<div id="mp-menuleft">
		<div class="mp-menu">
	
    	<a class="list-group-item start-group" href="'.$url.'">'.$this->l('Home').'</a>
	
		<a class="list-group-item global-group" href="'.$url.'&action=configuration">'.$this->l('Config').'</a>';
		
		$this->_html .='<a class="list-group-item youtube edit-help"  href="http://www.youtube.com/watch?v='.$this->getVideoHelp().'" onclick="return false;" class="">'.$this->l('Help').'</a>';
		
		
		$this->_html .='</div></div>';
	}
	public function _displayContent()
	{
		if(Tools::getValue('action')=='configuration')
			$this->_displayForm();
		else
			$this->_html .= $this->_displayHome();
	}
	public function _displayHomeContent()
	{
		$this->_html = '<h2>' . $this->displayName . '</h2>';
		 
		$this->_postProcess();
		$this->_displayHeaderContent();
	
		$this->_html .= '<div id="mp-admin" class="bootstrap">';
	
		$this->_displayLeftMenu();
		 
		 
		$this->_html .= '<div id="mp-content">';
		$this->_displayContent();
		$this->_html .= '</div></div>';
	
	}
	public function getVideoHelp()
	{
		return 'HQgIEfD7gGk';
	}
	public function _displayHeaderContent()
	{
		$this->_html .= '<script type="text/javascript" src="'.__PS_BASE_URI__.'modules/'.$this->name.'/js/admin.js"></script>';
		$this->_html .= '<link type="text/css" rel="stylesheet" href="'.__PS_BASE_URI__.'modules/'.$this->name.'/css/admin.css" />';
	}
	public function getContent()
	{
			$this->_html .= $this->checkCurrency(true,true,true);
	
		return $this->_html;

	}
	private function _postProcess()
	{
		if (Tools::isSubmit('submitmegaattributes'))
		{
			
			Configuration::updateValue('MA_SHOW_ATTRIBUTE_BTN', Tools::getValue('id_show_attribute_btn'));
			Configuration::updateValue('MA_DISABLE_SELECT', Tools::getValue('id_disable_select'));
			Configuration::updateValue('MA_IMAGE_COLOR', Tools::getValue('id_image_color'));
			Configuration::updateValue('MA_TOOLTIP', Tools::getValue('id_tooltip'));
			Configuration::updateValue('MA_FILTER', Tools::getValue('id_filter'));
			$show = Tools::getValue('id_show');
			Configuration::updateValue('MA_SHOW', $show);
			Configuration::updateValue('PS_DISP_UNAVAILABLE_ATTR', $show);
			
			
			$arrayCategories = Tools::getValue('categoryBox');
			if(is_array($arrayCategories) && sizeof($arrayCategories)>0)
				Configuration::updateValue('MA_CATEGORIES', implode('-', Tools::getValue('categoryBox')));
			else
				Configuration::updateValue('MA_CATEGORIES', '');
					
			$this->_html .= $this->displayConfirmation($this->l('Settings updated'));
		}
		
	}
	public function _displayForm()
	{	
		$this->_html.= $this->displayForm();
		return $this->_html;
	}

	public function displayForm()
	{
		$ags = AttributeGroup::getAttributesGroups($this->_id_lang);
		$this->_html .= '
		<div class="panel">
		<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
			<fieldset><legend><img src="'.$this->_path.'logo.png" alt="" title="" />'.$this->l('Settings').'</legend>

				
				<label>'.$this->l('Active Tooltip Attributes').'</label>
				<div class="margin-form">
					<select name="id_tooltip" id="id_tooltip">	
					<option value="0" '.((Configuration::get('MA_TOOLTIP') == 0) ? 'selected="selected"' : '').'>'.$this->l('No').'</option>
					<option value="1" '.((Configuration::get('MA_TOOLTIP') == 1) ? 'selected="selected"' : '').'>'.$this->l('Yes').'</option>	
				</select>					
				</div>
				
				<label>'.$this->l('Filter Combinations').'</label>
				<div class="margin-form">
					<select name="id_filter" id="id_filter">	
					<option value="0" '.((Configuration::get('MA_FILTER') == 0) ? 'selected="selected"' : '').'>'.$this->l('No').'</option>
					<option value="1" '.((Configuration::get('MA_FILTER') == 1) ? 'selected="selected"' : '').'>'.$this->l('Yes').'</option>	
				</select>					
				</div>
							
				<label>'.$this->l('Show attributes with').'</label>
				<div class="margin-form">
					<select name="id_show_attribute_btn" id="id_show_attribute_btn">	
					<option value="0" '.((Configuration::get('MA_SHOW_ATTRIBUTE_BTN') == 0) ? 'selected="selected"' : '').'>'.$this->l('Select').'</option>
					<option value="1" '.((Configuration::get('MA_SHOW_ATTRIBUTE_BTN') == 1) ? 'selected="selected"' : '').'>'.$this->l('Buttons').'</option>	
				</select>					
				</div>
							
				<label>'.$this->l('Show Disable Attribute').'</label>
				<div class="margin-form">
					<select name="id_show" id="id_show">	
					<option value="0" '.((Configuration::get('MA_SHOW') == 0) ? 'selected="selected"' : '').'>'.$this->l('Hidden').'</option>
					<option value="1" '.((Configuration::get('MA_SHOW') == 1) ? 'selected="selected"' : '').'>'.$this->l('Css Background').'</option>	
					</select>					
				</div>
				<!-- label>'.$this->l('Display unavailable product attributes on the product page').'</label>
				<div class="margin-form">
					<select name="PS_DISP_UNAVAILABLE_ATTR" id="PS_DISP_UNAVAILABLE_ATTR">	
					<option value="0" '.((Configuration::get('PS_DISP_UNAVAILABLE_ATTR') == 0) ? 'selected="selected"' : '').'>'.$this->l('No').'</option>
					<option value="1" '.((Configuration::get('PS_DISP_UNAVAILABLE_ATTR') == 1) ? 'selected="selected"' : '').'>'.$this->l('Yes').'</option>	
				</select>					
				</div -->
				
				<label style="display:none">'.$this->l('Disable select only one item').'</label>
				<div class="margin-form" style="display:none">
					<select name="id_disable_select" id="id_disable_select">	
					<option value="0" '.((Configuration::get('MA_DISABLE_SELECT') == 0) ? 'selected="selected"' : '').'>'.$this->l('No').'</option>
					<option value="1" '.((Configuration::get('MA_DISABLE_SELECT') == 1) ? 'selected="selected"' : '').'>'.$this->l('Yes').'</option>	
				</select>					
				</div>
				<hr style="visibility:hidden"/>
				
				<label>'.$this->l('Image Color Group').'</label>
				<div class="margin-form">
					<select name="id_image_color" id="id_image_color">	
					<option value="0" '.((Configuration::get('MA_IMAGE_COLOR') == 0) ? 'selected="selected"' : '').'>'.$this->l('Dont use images').'</option>';
				if($ags)
				{
					foreach($ags as $ag)
					{
						if($ag['is_color_group']==1)
							$this->_html .= '<option value="'.$ag['id_attribute_group'].'" '.((Configuration::get('MA_IMAGE_COLOR') == $ag['id_attribute_group']) ? 'selected="selected"' : '').'>'.$ag['name'].'</option>';
					}	
				}
			$this->_html .= '	</select>					
				</div>';	

			$this->_html .= '<fieldset><legend>'.$this->l('Categories Apply Filter').'</legend>';
			// Generate category selection tree
			$helper = new Helper();
			if(Configuration::get('MA_CATEGORIES')!='')
				$categories = explode('-',Configuration::get('MA_CATEGORIES'));
			else
				$categories = array();
			if(_PS_VERSION_<1.5)
			{
				$this->_html.= $helper->renderAdminCategorieTree(null, $categories, 'categoryBox');
			}
			else if(_PS_VERSION_<1.6)
			{
				$this->_html.= $helper->renderCategoryTree(null, $categories, 'categoryBox', false, false, array(), false, true);
			}
			else
			{
				$tree_categories_helper = new HelperTreeCategories('categoryBox', $this->l('Select Categories'));
				$tree_categories_helper->setRootCategory((Shop::getContext() == Shop::CONTEXT_SHOP ? Category::getRootCategory()->id_category : 0))
				->setUseCheckBox(true)->setSelectedCategories($categories);
				$this->_html.= $tree_categories_helper->render();
			
			}
			
			$this->_html .= '</fieldset><br/>';
			
		$this->_html .= '<center><input type="submit" name="submitmegaattributes" value="'.$this->l('Save').'" class="button" /></center>
			</fieldset>
		</form>
		</div>';
		//return $output;
	
	}

	
	
	
	
	public static function getIdGroups($id_product)
	{
		if (!Combination::isFeatureActive())
			return array();
		$sql = 'SELECT DISTINCT ag.`id_attribute_group` as id_group, ag.`is_color_group` as is_color
				FROM `'._DB_PREFIX_.'product_attribute` pa
				INNER JOIN `'._DB_PREFIX_.'product_attribute_shop` pas ON (pas.`id_product_attribute` = pa.`id_product_attribute`)
				LEFT JOIN `'._DB_PREFIX_.'product_attribute_combination` pac ON (pac.`id_product_attribute` = pa.`id_product_attribute`)
				LEFT JOIN `'._DB_PREFIX_.'attribute` a ON (a.`id_attribute` = pac.`id_attribute`)
				LEFT JOIN `'._DB_PREFIX_.'attribute_group` ag ON (ag.`id_attribute_group` = a.`id_attribute_group`)
				'.Shop::addSqlAssociation('attribute', 'a').'
				WHERE pa.`id_product` = '.(int)$id_product.' AND pas.id_shop='.Context::getContext()->shop->id.'
				ORDER BY ag.`position` ASC';
			return Db::getInstance()->executeS($sql);
	}
	
	public function hookProductActions($params){
		$retorno = "";	
			if($this->context->controller->php_self == 'product' && Tools::getValue('content_only')==1){
				$retorno = $this->hookProductFooter($params);
			}
		return $retorno;
	}
	
	public function hookProductFooter($params)
	{
		global $smarty, $cookie;
		
		
		$arrayGroups = self::getIdGroups(Tools::getValue('id_product'));
		$html = '';
		
		$id_lang = intval($this->_id_lang);
		$show_btn = Configuration::get('MA_SHOW_ATTRIBUTE_BTN');
		
		$smarty->assign(array(
				'MA_SHOW_ATTRIBUTE' => Configuration::get('MA_SHOW_ATTRIBUTE_BTN'),
				'MA_TOOLTIP' => Configuration::get('MA_TOOLTIP'),
				'MA_DISABLE_SELECT' => Configuration::get('MA_DISABLE_SELECT'),
				'MA_IMAGE_COLOR' => Configuration::get('MA_IMAGE_COLOR'),
				'MA_FILTER' => Configuration::get('MA_FILTER'),
				'matgroups' => $arrayGroups,
		));
		
		$html = 'megaattributes.tpl';
		
		return $this->display(__FILE__, $html);
		
	}
	protected function hookDisplayAwCustomView()
	{
		return $this->_displayHomeContent();
	}
	function hookHeader($params)
	{
		if(Tools::getValue('id_product'))
		{
			$cats = Configuration::get('MA_CATEGORIES');
			if($cats!='')
			{
				$categories = ProductCore::getProductCategories(Tools::getValue('id_product'));
				$show = false;
				$arrayCats = explode('-',$cats);
				foreach($categories as $id)
				{
					foreach($arrayCats as $cat)
					{
						if($cat==$id)
							$show = true;
					}
				}
				if(!$show)
					return;
			}
				
		}
		else
		{
			return;
		}
		
		if(_PS_VERSION_<1.5)
		{
			Tools::addCSS(($this->_path).'megaattributes.css', 'all');
			Tools::addJS(($this->_path).'megaattributes14.js');
			if(Tools::getValue('id_product'))
			{
				Tools::addJS(($this->_path).'js/jquery-ui.tooltip.min.js');
				Tools::addCSS(($this->_path).'css/jquery-ui.tooltip.css', 'all');
			}
		}
		else
		{
			$this->context->controller->addCSS(($this->_path).'megaattributes.css', 'all');
			$this->context->controller->addJS(($this->_path).'megaattributes.js');
			if(Tools::getValue('id_product'))
			{
				$this->context->controller->addJS(($this->_path).'js/jquery-ui.tooltip.min.js');
				$this->context->controller->addCSS(($this->_path).'css/jquery-ui.tooltip.css');
			}
		}
		if(Configuration::get('MA_SHOW')==0)
		{
			return '<style type="text/css">.ma-disabled{display:none !important;} </style>';
		}
		
	}

}
?>