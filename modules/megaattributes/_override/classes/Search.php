<?php

class Search extends SearchCore
{

 	public static function find($id_lang, $expr, $page_number = 1, $page_size = 1, $order_by = 'position',
        $order_way = 'desc', $ajax = false, $use_cookie = true, Context $context = null)
    {
        $resultado = parent::find($id_lang, $expr, $page_number, $page_size, $order_by, $order_way, $ajax, $use_cookie, $context);
        $resultado['result'] = Product::getProductsProperties((int)$id_lang,  $resultado['result'],  true);

        return $resultado;
    }

    public static function searchTag($id_lang, $tag, $count = false, $pageNumber = 0, $pageSize = 10, $orderBy = false, $orderWay = false,
            $useCookie = true, Context $context = null)
    {
        $resultado = parent::searchTag($id_lang, $tag, $count, $pageNumber, $pageSize, $orderBy, $orderWay, $useCookie, $context);
        if($resultado && is_array($resultado)){
        	$resultado = Product::getProductsProperties((int)$id_lang, $resultado,  true);  
        }
        return $resultado; 
    }
}
