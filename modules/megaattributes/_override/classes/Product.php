<?php

class Product extends ProductCore
{

 public static function getProductProperties($id_lang, $row, Context $context = null, $check_unavailable=false)
    {
        if (!$row['id_product']) {
            return false;
        }

        if ($context == null) {
            $context = Context::getContext();
        }

        $id_product_attribute = $row['id_product_attribute'] = (!empty($row['id_product_attribute']) ? (int)$row['id_product_attribute'] : null);	
        
        // Product::getDefaultAttribute is only called if id_product_attribute is missing from the SQL query at the origin of it:
        // consider adding it in order to avoid unnecessary queries
        $row['allow_oosp'] = Product::isAvailableWhenOutOfStock($row['out_of_stock']);
        if (Combination::isFeatureActive() && $id_product_attribute === null
            && ((isset($row['cache_default_attribute']) && ($ipa_default = $row['cache_default_attribute']) !== null)
                || ($ipa_default = Product::getDefaultAttribute($row['id_product'], !$row['allow_oosp'])))) {
            $id_product_attribute = $row['id_product_attribute'] = $ipa_default;
        }
        if (!Combination::isFeatureActive() || !isset($row['id_product_attribute'])) {
            $id_product_attribute = $row['id_product_attribute'] = 0;
        }
        
        $quantity =0;
		//se ha quitado el check_unavailable para forzar que esta comprobacion se realice siempre
		//ya que en los modulos (blocknewproducts, blockbestsellers, blockspecials) siempre llega false
        if(!$row['allow_oosp'] && Configuration::get('PS_DISP_UNAVAILABLE_ATTR') == 0){
	        if ($id_product_attribute) {
	             $quantity = Product::getQuantity(
	                (int)$row['id_product'],
	                $id_product_attribute,
	                isset($row['cache_is_pack']) ? $row['cache_is_pack'] : null
	            );
	            $quantity_all_versions = Product::getQuantity(
		            (int)$row['id_product'],
		            0,
		            isset($row['cache_is_pack']) ? $row['cache_is_pack'] : null
		        );
		        if($quantity==0 && $quantity_all_versions>0){
		        	$sql = 'select id_product_attribute from '._DB_PREFIX_.'stock_available sa where quantity>0 and id_product_attribute<>0 and id_product = '.(int) $row['id_product'].' and id_shop = '.$context->shop->id;
		        	$row['id_product_attribute'] = $id_product_attribute = Db::getInstance()->getValue($sql);
		        }
	        }
        }

 		$id_image = $row['id_image'];
 		$row = parent::getProductProperties($id_lang, $row, $context);
		$arrexp = explode("-",$id_image);
		if(count($arrexp)<2){
			$id_image = $row['id_product'].'-'.$id_image;
		}
		$row['id_image'] = $id_image;
		return $row;
    }
    
	public static function getProductsProperties($id_lang, $query_result, $check_unavailable=false)
    {
        $results_array = array();

        if (is_array($query_result)) {
            foreach ($query_result as $row) {
                if ($row2 = Product::getProductProperties($id_lang, $row, null, $check_unavailable)) {
                    $results_array[] = $row2;
                }
            }
        }

        return $results_array;
    }

}
