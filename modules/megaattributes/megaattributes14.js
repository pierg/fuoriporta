var groupsMA = new Array();
var groupsMASelects = new Array();
var groupsMAValue = new Array();
var isLoadMegaAttrs = false;
var groupMAButton = false;


$(document).ready(function(){
	
	if(typeof productHasAttributes!='undefined' && productHasAttributes)
	{
		setMAGroups();
		if(typeof megaattributesimagecolor!='undefined' || megaattributesimagecolor!=0)
			asignMSCColorImages(megaattributesimagecolor);

		if(typeof megaattributesshow=='undefined' || megaattributesshow==0)
		{
			setSelectOptions();
			if(groupsMA.length>0)
			{
				setSelectCombinationExist(groupsMA[0]);
			}
		}
		else if(megaattributesshow==1)
		{
			groupMAButton = true;
			setBtnOptions();
			setBtnDefaultAttributes();
		}
	}
	
});
function setMAGroups()
{
	if($('#attributes .attribute_label').length)
	{
		$('#attributes .attribute_label').each(function(){
			var id_group = $(this).attr('for').substring(6);
			groupsMA.push(id_group);
			optionswitch(id_group);
		});
	}
	else
	{
		$('#attributes select').each(function(){
			var id_group = $(this).attr('id').substring(6);
			groupsMA.push(id_group);
			optionswitch(id_group);
		});
	}
	
}
function setSelectOptions()
{
	/*$('#buy_block #attributes select').each(function(i, select){

		if(typeof megaattributesdisable!='undefined' && megaattributesdisable==1)
		{
			if($select.find('option').length<2)
				$select.attr('disabled','disabled').addClass('ma-disabled');
		}
	});*/
	$('#buy_block #attributes select').each(function(){	
		var id_group = $(this).attr('name').substring(6);
	//	var text = "findCombination();setSelectCombinationExist("+id_group+");getProductAttribute();$('#wrapResetImages').show('slow');";
			var text = "setSelectCombinationExist("+id_group+");";
	
		$(this).attr('onchange',text);
	});
	$('#buy_block #color_to_pick_list a').each(function(){	
		var id_attribute = $(this).attr('id').substring(6);
		id_group = getAttributeGroup(id_attribute);
		
		//var text = "setColorCombinationExist(this,"+id_group+");setSelectCombinationExist("+id_group+");colorPickerClick(this);$('#wrapResetImages').show('slow');";
		var text = "setColorCombinationExist(this,"+id_group+");setSelectCombinationExist("+id_group+");";
		$(this).attr('onclick',text);
	});
	$('#buy_block .attribute_radio').each(function(){	
		var id_attribute = $(this).val();
		id_group = getAttributeGroup(id_attribute);
	//	var text = "setSelectCombinationExist("+id_group+");findCombination();$('#wrapResetImages').show('slow');";
			var text = "setSelectCombinationExist("+id_group+");";
		$(this).attr('onclick',text);
	});
		
}
function setBtnOptions()
{
	//Get Exising Select Options    
	$('#buy_block #attributes select').each(function(i, select){
		
	    var $select = $(select);
	    var html = '';
	    $select.before("<br/>");
	
	    var arrayButtons = new Array();
	    $select.find('option').each(function(j, option){
	        var $option = $(option);
	        
	        // Create a button:
	        var $button = $('<input type="button" />');
	        // Set name and value:
	       // $radio.attr('name', $select.attr('name')).attr('value', $option.val());
	        $button.attr('id', $select.attr('name')+'_'+$option.val()).attr('value', $option.attr('title')).addClass('megabtn'); 
	        $button.attr('name', 'box_'+$select.attr('name'));
	       // Set checked if the option was selected
	        if ($option.attr('selected')) $button.addClass('ma-selected');
	        // Insert button before select box:
	       /* if(typeof id_color_default!='undefined')
			{
				if($select.attr('name')=='group_'+id_color_default)
			 		$button.hide();
	      	}*/
	     
			$select.before($button);
	    });
	 
	    $select.hide();
	});
	$('#buy_block .megabtn').click(function(){
		var idname = $(this).attr('id').substring(6).split('_');
		$("[name='box_group_"+idname[0]+"']").removeClass('ma-selected');
		$(this).addClass('ma-selected');
		$('select#group_'+idname[0]+' option[value='+idname[1]+']').attr("selected", "selected");
		//findCombination();
		
		setSelectCombinationExist(idname[0]);
		$(this).attr('onclick','');
	});
	
	
	$('#color_to_pick_list a').click(function(){
		var id_attribute = $(this).attr('id').substring(6);
		id_group = getAttributeGroup(id_attribute);
		$("[name='box_group_"+id_group+"']").removeClass('ma-selected');
		$('#group_'+id_group+'_'+id_attribute).addClass('ma-selected');
		setColorCombinationExist(this,id_group);
		setSelectCombinationExist(id_group);
		displayImage($('#thumb_'+$(this).attr('alt')).parent());
		$(this).attr('onclick','');
	});
	$('#buy_block .attribute_radio').each(function(){	
		var id_attribute = $(this).val();
		id_group = getAttributeGroup(id_attribute);
		var text = "setSelectCombinationExist("+id_group+");findCombination();$('#wrapResetImages').show('slow');";
		$(this).attr('onclick',text);
	});
	$('#color_to_pick_list img').attr('width',50).attr('height',50);	
}
function setColorCombinationExist(elt,idgroup){
	id_attribute = $(elt).attr('id').replace('color_', '');
	$(elt).parent().parent().children().removeClass('selected');
	$(elt).parent().addClass('selected');
	$(elt).parent().parent().parent().children('.color_pick_hidden,#color_pick_hidden').val(id_attribute);
	
}
function getAttributeGroup(id_attribute)
{
	for(var i=0;i<attributesCombinations.length;i++)
	{
		if(attributesCombinations[i]['id_attribute']==id_attribute)
			return attributesCombinations[i]['id_attribute_group'];
	}
	return 0;
}
function checkCombination(comb)
{
	 for (j=0;j<comb.length;j++)
	 { 
		 if(comb[j]!=groupsMAValue[j] && groupsMAValue[j]!=0)
		 {
			 return false;
		 }
	  } 
	 return true;
}
function optionswitch(id_group) {
    
	groupsMASelects[id_group] = new Array();
	groupsMASelects[id_group] =  $('#group_'+ id_group +' option').clone();
}

function setSelectCombinationExist(idgroup)
{
	var existcomb = false;
    if (groupsMA.length > 1)
    {       	
    	for (i=0;i<groupsMA.length;i++)
		{
    		if(($('.color_pick_hidden[name="group_'+groupsMA[i]+'"]').length>0 || $('#color_pick_hidden[name="group_'+groupsMA[i]+'"]').length>0) && i==0)
    		{
    			groupsMAValue[i] = getIdAttribute(groupsMA[i]);
    		}
    		else
    		{
    			groupsMAValue[i] = 0;
    		}
    		if(typeof idgroup!='undefined' && groupsMA[i]==idgroup)
    		{
    			groupsMAValue[i] = getIdAttribute(groupsMA[i]);
    		}
		}
    	if(typeof idgroup=='undefined')
    	{
    		idgroup = groupsMA[0];
    	}
    	var applyRules = false;
    	for (i=0;i<groupsMA.length;i++)
    	{
        	id_attr = $('#group_'+groupsMA[i]).val();
        	id_group = groupsMA[i]; 
         	
            // Disable all options next group
            if(idgroup!=id_group && applyRules && i!=0)
            {
            	if(!groupMAButton)
            	{
	            	$('#group_'+id_group+' option').remove();
	            	$(groupsMASelects[id_group]).appendTo($('#group_'+id_group));
	             	$('#group_'+id_group+' option').attr('disabled','disabled').addClass('ma-disabled');
	              	$("[name='group_"+id_group+"']").parent('.attribute_list').find('a').removeClass('ma-noexist').attr('disabled','disabled').addClass('ma-disabled');
               	}
            	else
            	{
            		$("[name='box_group_"+id_group+"']").removeClass('ma-noexist').attr('disabled','disabled').addClass('ma-disabled');
            	}
	      
                var activeAttr = 0;
                var selectAttr = false;
                if(typeof id_color_default!='undefined' && id_color_default==id_group)
            			$('#color_to_pick_list a').attr('disabled','disabled').addClass('ma-disabled');
               
                for (x=0;x<combinations.length;x++)
                {
                	var id_attribute_combination = combinations[x]['idsAttributes'][i];
                	   
                    if (checkCombination(combinations[x]['idsAttributes']))
                    {
                    	if((!allowBuyWhenOutOfStock && combinations[x]['quantity'] > 0) || allowBuyWhenOutOfStock)
                    	{
                    		existcomb = true;
                    		if(groupMAButton)
                    		{
					            		$('#group_'+id_group+'_'+id_attribute_combination).removeAttr('disabled').removeClass('ma-disabled');      
                    		}
                    		else
                    		{
														$("#group_"+id_group+" option[value='"+id_attribute_combination+"']").removeAttr('disabled').removeClass('ma-disabled'); 
                    		}
	                     	$("#color_"+id_attribute_combination).removeAttr('disabled').removeClass('ma-disabled');
	                     	if(id_attribute_combination==id_attr)
	                     	{
	                     		selectAttr = true;
	                     		activeAttr = id_attr;
	                     	}
	                     	if(!selectAttr && activeAttr==0)
	                     		activeAttr = id_attribute_combination;
                    	}
                    }
                }
                if(!selectAttr)
                {
                	$('#group_'+id_group).val(activeAttr);
                	if(groupMAButton)
                		$('#group_'+id_group+'_'+activeAttr).addClass('ma-selected');
                }
                else
                {
                	$("[name='box_group_"+id_group+"']").removeClass('ma-selected')
                	if(groupMAButton)
                		$('#group_'+id_group+'_'+activeAttr).addClass('ma-selected');
                }
                if(!groupMAButton)
                	$("#group_"+id_group+" option.ma-disabled").remove();
            }
			if(idgroup==id_group)
			{
				applyRules =true;
			}   
              
        	id_attribute = getIdAttribute(groupsMA[i]); 
      		groupsMAValue[i] = id_attribute;
        }   
    }
    else
    {       
    	id_group = groupsMA[0]; 
    	if(!groupMAButton)
    		$('#group_'+idgroup+' option').attr('disabled','disabled').addClass('ma-disabled');
        else
        	$("[name='box_group_"+id_group+"']").removeClass('ma-noexist').attr('disabled','disabled').addClass('ma-disabled');
        
        for (x=0;x<combinations.length;x++){
            var id_attribute_combination = combinations[x]['idsAttributes'][0];
            $('#color_'+id_attribute_combination).attr('disabled','disabled').addClass('ma-disabled');
            if((!allowBuyWhenOutOfStock && combinations[x]['quantity'] > 0) || allowBuyWhenOutOfStock)
            { 
            	existcomb = true;
            	if(!groupMAButton)
            		$("#group_"+idgroup+" option[value='"+id_attribute_combination+"']").removeAttr('disabled').removeClass('ma-disabled');   
            	else
					$('#group_'+id_group+'_'+id_attribute_combination).addClass('ma_available').removeAttr('disabled').removeClass('ma-disabled');                	                
            	  $('#color_'+id_attribute_combination).removeAttr('disabled').removeClass('ma-disabled');                	                
            }
        }
    }
    if(!isLoadMegaAttrs && existcomb)
    {
    	$('#add_to_cart').fadeIn(600);
    }
    findCombination();
  //  getProductAttribute();
    $('#wrapResetImages').show('slow');
    
    isLoadMegaAttrs = true;
}

function setBtnDefaultAttributes(){
	
	if (groupsMA.length > 0)
	{
		setSelectCombinationExist(groupsMA[0]);
	}
	
}
function getIdAttribute(id_group)
{
	if($('#group_'+id_group).length>0)
	{
		return $('#group_'+id_group).val();
	}
	else if($('.color_pick_hidden[name="group_'+id_group+'"]').length>0)
	{
		return $('.color_pick_hidden[name="group_'+id_group+'"]').val();
	}
	else if($('.attribute_radio:checked[name="group_'+id_group+'"]').length>0)
	{
		return $('.attribute_radio:checked[name="group_'+id_group+'"]').val();
	}
	return 0;
}
function asingColorImage(combination)
{
	$('#color_to_pick_list a').each(function(){
		var id_color = $(this).attr('id').substring(6);
		if(combination['idsAttributes'].indexOf(id_color)>=0)
		{
			var imgthumb = $('#thumb_' + combination['image']).clone();
			var newid = 'msc'+ $(imgthumb).attr('id');
			$(imgthumb).attr('id',newid);
			$(this).html($(imgthumb));
			$(this).css('width',$(imgthumb).attr('width'));
			$(this).css('height',$(imgthumb).attr('height'));
			$(this).attr('alt',combination['image']);
				
		}
	});
}
function asignMSCColorImages(id_group)
{
	if($('.color_pick_hidden[name="group_'+id_group+'"]').length>0)
	{
		if(typeof combinationImages!='undefined')
		$.each(combinationImages, function(index, value) {
			if(index!=0)
			{
				if(typeof combinations!='undefined')
					for(x=0;x<combinations.length;x++)
					{
					   	if(combinations[x]['idCombination']==index)
					   	{
					   		asingColorImage(combinations[x]);
					   	}
					}
			}
		});
		$('#color_to_pick_list a').each(function(){
			var text = "getProductAttribute();$('#wrapResetImages').show('slow');";
			$(this).attr('onclick',text);
		});
	
		$('#thumbs_list').hide();
	}
}






