# Payment Methods Product Restrictions


## Description

This module allows you to restrict payment methods by specific products.
For each product you can choose allowed payment methods.


## Setup

To install the "Payment Methods Product Restrictions" module:
1) Install the module's archive in the Back office: go to the Modules page and click the "Add a new module" button. Then follow instructions.
	OR
   Unzip (decompress) the module archive file and, using your FTP software, place the folder in your PrestaShop /modules folder.
2) Go to Back Office >> Modules.
3) Locate the "Payment Methods Product Restrictions" module in the list, scrolling down if necessary.
4) In the row for the "Payment Methods Product Restrictions" module, click Install.
5) Locate the module again. Click >> Configure.


## Usage

At the module configuration page you can see  additional installation instructions and instructions for using this module.
For changing restrictions, open some product in your Back Office and choose the tab "Payment Methods Product Restrictions".
There you'll see list of available payment methods, which you can enable or disable.


## License

This module is licenced under the Software License Agreement.
With the purchase or the installation of the software in your application you accept the licence agreement.
For more details see LICENSE.txt