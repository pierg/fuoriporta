{**
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Presta.Site
* @copyright 2016 Presta.Site
* @license   LICENSE.txt
*}
{if $psv == 1.5}
    <br/><fieldset class="warning-wrp"><legend>{l s='How to use this module' mod='pmrestrictions'}</legend>
{else}
    <div class="panel warning-wrp">
        <div class="panel-heading">
            <i class="icon-cogs"></i> {l s='How to use this module' mod='pmrestrictions'}
        </div>
{/if}
        <div class="form-wrapper">
            <ol>
                <li>{l s='Open some product in your Back Office.' mod='pmrestrictions'}</li>
                {if $psv <= 1.6}
                    <li>{l s='In the product tabs list you will see a new tab: Payment Methods Product Restrictions' mod='pmrestrictions'}</li>
                    <li>{l s='Click that tab and you will be able to enable and disable payment methods for that specific product.' mod='pmrestrictions'}</li>
                {else} {* 1.7+ *}
                    <li>{l s='In the product tabs list click the tab "Modules options".' mod='pmrestrictions'}</b></li>
                    <li>{l s='In the dropdown choose the module Pmrestrictions (Payment Methods Product Restrictions).' mod='pmrestrictions'}</li>
                    <li>{l s='There you will be able to enable and disable payment methods for that specific product.' mod='pmrestrictions'}</li>
                {/if}
            </ol>
            {if $psv == 1.5}
                <p>{l s='See this image for reference:' mod='pmrestrictions'} <a target="_blank" href="{$module_path|escape:'html':'UTF-8'}views/img/example.png">{l s='image' mod='pmrestrictions'}</a></p>
            {else}
                <p>{l s='See this [1]image[/1] for reference.' mod='pmrestrictions' tags=['<a target="_blank" href="'|cat:{$module_path|escape:'html':'UTF-8'}|cat:'views/img/example.png">']}</p>
            {/if}
        </div>

{if $psv == 1.5}
    </fieldset><br/>
{else}
    </div>
{/if}