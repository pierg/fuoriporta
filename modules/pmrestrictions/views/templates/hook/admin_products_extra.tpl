{**
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Presta.Site
* @copyright 2016 Presta.Site
* @license   LICENSE.txt
*}
<div id="pmrestrictions" class="panel product-tab">
    <input type="hidden" name="submitted_tabs[]" value="{$module_name|escape:'html':'UTF-8'}" />
    <input type="hidden" name="{$module_name|escape:'html':'UTF-8'}-submit" value="1" />
    <h3>{l s='Payment Methods Restrictions' mod='pmrestrictions'}</h3>

    <div class="form-group">
        <div class="col-lg-1"><span class="pull-right"></span></div>
        <label class="control-label col-lg-2" for="pmrestrictions">
            {l s='Allowed payment methods' mod='pmrestrictions'}
        </label>
        <div class="col-lg-2">
            <div class="input-group">
                {foreach from=$payment_methods item=payment_method}
                    <div class="checkbox">
                        <label for="pmr-pmethod-{$payment_method.id_module|escape:'html':'UTF-8'}" class="t">
                            <input type="checkbox"
                               name="{$module_name|escape:'html':'UTF-8'}[]"
                               id="pmr-pmethod-{$payment_method.id_module|escape:'html':'UTF-8'}"
                               value="{$payment_method.id_module|escape:'html':'UTF-8'}"
                               {if !$payment_method.disabled}checked="checked"{/if} />&nbsp;
                            {$payment_method.displayName|escape:'html':'UTF-8'}
                        </label>
                    </div>
                {/foreach}
            </div>
        </div>
    </div>

    {if $psv > 1.5}
        <div class="panel-footer">
            <a href="{$link->getAdminLink('AdminProducts')|escape:'html':'UTF-8'}" class="btn btn-default"><i class="process-icon-cancel"></i> {l s='Cancel' mod='pmrestrictions'}</a>
            <button type="submit" name="submitAddproduct" class="btn btn-default pull-right"><i class="process-icon-save"></i> {l s='Save' mod='pmrestrictions'}</button>
            <button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right"><i class="process-icon-save"></i> {l s='Save and stay' mod='pmrestrictions'}</button>
        </div>
    {/if}
</div>
