{**
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Presta.Site
* @copyright 2016 Presta.Site
* @license   LICENSE.txt
*}
<div id="pmrestrictions" class="">
    <input type="hidden" name="submitted_tabs[]" value="{$module_name|escape:'html':'UTF-8'}" />
    <input type="hidden" name="{$module_name|escape:'html':'UTF-8'}-submit" value="1" />

    <div class="row">
        <div class="col-lg-12 col-xl-4">
            <fieldset class="form-group">
                <label class="form-control-label">{l s='Allowed payment methods' mod='pmrestrictions'}:</label>
                <div>
                    {foreach from=$payment_methods item=payment_method}
                        <div class="checkbox">
                            <label for="pmr-pmethod-{$payment_method.id_module|escape:'html':'UTF-8'}" class="t">
                                <input type="checkbox"
                                       name="{$module_name|escape:'html':'UTF-8'}[]"
                                       id="pmr-pmethod-{$payment_method.id_module|escape:'html':'UTF-8'}"
                                       value="{$payment_method.id_module|escape:'html':'UTF-8'}"
                                       {if !$payment_method.disabled}checked="checked"{/if} />&nbsp;
                                {$payment_method.displayName|escape:'html':'UTF-8'}
                            </label>
                        </div>
                    {/foreach}
                </div>
            </fieldset>
        </div>
    </div>
</div>
