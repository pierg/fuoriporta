{**
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* @author    Presta.Site
* @copyright 2016 Presta.Site
* @license   LICENSE.txt
*}
<div id="pmr-restrictions-list">
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-cogs"></i> {l s='Payment methods restrictions:' mod='pmrestrictions'}
        </div>
        <div class="form-wrapper">
            <div class="form-group">
                <div class="table-responsive-row">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>{l s='Product' mod='pmrestrictions'}</th>
                            <th>{l s='Disabled payment methods' mod='pmrestrictions'}</th>
                            <th>{l s='Actions' mod='pmrestrictions'}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach from=$restrictions item=restriction}
                            <tr>
                                <td>#{$restriction.id_product|intval} {$restriction.product_name|escape:'html':'UTF-8'}</td>
                                <td>
                                    {', '|implode:$restriction.disabled_modules}
                                </td>
                                <td>
                                    {* Edit btn *}
                                    {if $psv <= 1.6}
                                        <a href="{$product_link|escape:'html':'UTF-8'}&id_product={$restriction.id_product|intval}&updateproduct&key_tab={$key_tab|escape:'html':'UTF-8'}"
                                           title="{l s='Edit' mod='pmrestrictions'}" class="edit btn btn-default" target="_blank">
                                            <i class="icon-pencil"></i>
                                        </a>
                                    {else}
                                        <a href="{$link->getAdminLink('AdminProducts', true, ['id_product' => $restriction.id_product])}#tab-hooks"
                                           title="{l s='Edit' mod='pmrestrictions'}" class="edit btn btn-default" target="_blank">
                                            <i class="icon-pencil"></i>
                                        </a>
                                    {/if}
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    {* Remove btn *}
                                    <a href="#" title="{l s='Remove all restrictions' mod='pmrestrictions'}" class="edit btn btn-default remove-restriction" data-id-product="{$restriction.id_product|intval}">
                                        <i class="icon-remove"></i>
                                    </a>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var pmr_ajax_url = "{$ajax_url|escape:'quotes':'UTF-8'}";
        var pmr_remove_confirm_txt = "{l s='Are you sure you want to delete these restrictions?' mod='pmrestrictions'}";
    </script>
</div>
