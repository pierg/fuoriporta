/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * @author    Presta.Site
 * @copyright 2016 Presta.Site
 * @license   LICENSE.txt
 */
$(function () {
    $(document).on('click', '.remove-restriction', function(e) {
        if (!confirm(pmr_remove_confirm_txt)) {
            return false;
        }

        var id_product = $(this).data('id-product');
        var $this = $(this);

        $.ajax({
            url: pmr_ajax_url,
            data: {ajax: true, action: 'removeRestrictions', id_product: id_product},
            method: 'post',
            success: function () {
                $this.parents('tr').fadeOut(500, function(){
                    $(this).remove();
                });
            }
        });
        
        e.preventDefault();
    });
});