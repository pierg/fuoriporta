<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * @author    Presta.Site
 * @copyright 2016 Presta.Site
 * @license   LICENSE.txt
 */

class Hook extends HookCore
{
    /**
     * Get list of modules we can execute per hook
     *
     * @since 1.5.0
     * @param string $hook_name Get list of modules for this hook if given
     * @return array
     */
    public static function getHookModuleExecList($hook_name = null)
    {
        $hookModuleExecList = parent::getHookModuleExecList($hook_name);

        if (Module::isEnabled('pmrestrictions')) {
            $ps_version = _PS_VERSION_;
            $ps_version = Tools::substr($ps_version, 0, 3);
            $pmethods_hook_name = ($ps_version <= 1.6 ? 'displayPayment' : 'paymentOptions');

            if ($hook_name == $pmethods_hook_name) {
                $pmrestrictions = Module::getInstanceByName('pmrestrictions');
                if ($pmrestrictions && $pmrestrictions->active) {
                    return $pmrestrictions->filterPaymentModules($hookModuleExecList);
                } else {
                    return $hookModuleExecList;
                }
            }
        }

        return $hookModuleExecList;
    }
}
