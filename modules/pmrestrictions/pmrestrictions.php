<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * @author    Presta.Site
 * @copyright 2016 Presta.Site
 * @license   LICENSE.txt
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class PMRestrictions extends Module
{
    private $html;

    public $settings_prefix = 'PMR_';
    public $mode;

    public function __construct()
    {
        $this->name = 'pmrestrictions';
        $this->tab = 'front_office_features';
        $this->version = '1.0.5';
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => '1.7.99.99');
        $this->author = 'Presta.Site';
        $this->bootstrap = true;
        $this->module_key = '8a8c9679b4eebf44889c5c5d6c1ab690';

        parent::__construct();
        $this->loadSettings();

        $this->displayName = $this->l('Payment Methods Product Restrictions');
        $this->description = $this->l('This module allows you to restrict payment methods for specific products.');
    }

    public function install()
    {
        if (!parent::install() ||
            !$this->registerHook('actionProductSave') ||
            !$this->registerHook('displayAdminProductsExtra') ||
            !$this->registerHook('displayBackOfficeHeader')

        ) {
            return false;
        }

        $db_result = Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pmrestrictions_product` (
			`id_module` INT(6) NOT NULL,
			`id_product` INT(6) NOT NULL,
			`id_shop` INTEGER UNSIGNED NOT NULL,
			`disabled` TINYINT,
			INDEX `id_module_id_shop` (`id_module`, `id_shop`),
			INDEX `id_product_id_shop` (`id_product`, `id_shop`),
			INDEX `disabled_id_shop` (`disabled`, `id_shop`),
			UNIQUE `id_module_id_product_id_shop` (`id_module`, `id_product`, `id_shop`)
		) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8');
        if (!$db_result) {
            return false;
        }

        //default values:
        foreach ($this->getSettings() as $item) {
            if ($item['type'] == 'html') {
                continue;
            }
            if (isset($item['default']) && Configuration::get($this->settings_prefix . $item['name']) === false) {
                Configuration::updateValue($this->settings_prefix . $item['name'], $item['default']);
            }
        }

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall()) {
            return false;
        }
        return true;
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        $this->html = '';
        $this->html .= $this->postProcess();
        $this->html .= $this->renderForm();
        $this->html .= $this->renderRestrictionsList();
        $this->html .= $this->renderInstallationInfo();

        return $this->html;
    }

    /**
     * Save form data.
     * @return string, html
     */
    protected function postProcess()
    {
        $html = '';
        $settings_updated = false;

        if (Tools::isSubmit('submitModule')) {
            //saving settings:
            $settings = $this->getSettings();
            foreach ($settings as $item) {
                if ($item['type'] == 'html' || (isset($item['lang']) && $item['lang'] == true)) {
                    continue;
                }
                if (Tools::isSubmit($item['name'])) {
                    Configuration::updateValue(
                        $this->settings_prefix . $item['name'],
                        Tools::getValue($item['name']),
                        true
                    );
                    $settings_updated = true;
                }
            }

            //update lang fields:
            $languages = Language::getLanguages();
            foreach ($settings as $item) {
                if ($item['type'] == 'html') {
                    continue;
                }
                $lang_value = array();
                foreach ($languages as $lang) {
                    if (Tools::isSubmit($item['name'] . '_' . $lang['id_lang'])) {
                        $lang_value[$lang['id_lang']] = Tools::getValue($item['name'] . '_' . $lang['id_lang']);
                        $settings_updated = true;
                    }
                }
                if (sizeof($lang_value)) {
                    Configuration::updateValue($this->settings_prefix . $item['name'], $lang_value, true);
                }
            }
        }

        $this->loadSettings();

        if ($settings_updated) {
            $html .= $this->displayConfirmation($this->l('Settings updated.'));
        }

        return $html;
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     * @return string, generated html
     */
    protected function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ),
                'input' => $this->getSettings(),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );

        if ($this->getPSVersion() == 1.5) {
            $fields_form['form']['submit']['class'] = 'button';
        }

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang =
            Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ?
                Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') :
                0;
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) .
            '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => array(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        foreach ($this->getSettings() as $item) {
            if ($item['type'] == 'html') {
                continue;
            }
            $helper->tpl_vars['fields_value'][$item['name']] = Configuration::get(
                $this->settings_prefix .
                $item['name']
            );
        }

        return $helper->generateForm(array($fields_form));
    }

    /**
     * @return string, generated html
     */
    protected function renderInstallationInfo()
    {
        $this->context->smarty->assign(array(
            'psv' => $this->getPSVersion(),
            'module_path' => $this->_path,
        ));

        return $this->display(__FILE__, 'installation.tpl');
    }

    /**
     * Get module settings
     * @return array
     */
    public function getSettings()
    {
        $settings = array(
            array(
                'type' => 'select',
                'name' => 'MODE',
                'label' => $this->l('Mode'),
                'hint' => $this->l('Change the mode of restriction'),
                'desc' => $this->l('Default: display only the payment methods that are allowed for all products in the cart.').'<br/>'.
                    $this->l('All: display all available payment methods that are allowed at least for one product in the cart.'),
                'class' => 't',
                'options' => array(
                    'query' => array(
                        array(
                            'id_option' => 'default',
                            'name' => $this->l('Default'),
                        ),
                        array(
                            'id_option' => 'all',
                            'name' => $this->l('All'),
                        ),
                    ),
                    'id' => 'id_option',
                    'name' => 'name',
                ),
                'default' => 'default',
            ),
        );

        if ($this->getPSVersion() < 1.6) {
            foreach ($settings as &$item) {
                $desc = isset($item['desc']) ? $item['desc'] : '';
                $hint = isset($item['hint']) ? $item['hint'] . '<br/>' : '';
                $item['desc'] = $hint . $desc;
                $item['hint'] = '';
            }

        }

        return $settings;
    }

    /**
     * Load settings from database
     */
    protected function loadSettings()
    {
        foreach ($this->getSettings() as $item) {
            if ($item['type'] == 'html') {
                continue;
            }
            $name = Tools::strtolower($item['name']);
            $this->$name = Configuration::get($this->settings_prefix . $item['name']);
        }
    }

    /**
     * Returns PrestaShop version, i.e. 1.6
     * @param bool|false $without_dots
     * @return float
     */
    public function getPSVersion($without_dots = false)
    {
        $ps_version = _PS_VERSION_;
        $ps_version = Tools::substr($ps_version, 0, 3);

        if ($without_dots) {
            $ps_version = str_replace('.', '', $ps_version);
        }

        return (float)$ps_version;
    }

    /**
     * @param array $params
     * @return bool|string
     */
    public function hookDisplayAdminProductsExtra($params)
    {
        if (isset($params['id_product']) && $params['id_product']) {
            $id_product = $params['id_product'];
        } else {
            $id_product = (int)Tools::getValue('id_product');
        }

        if (Validate::isLoadedObject($product = new Product((int)$id_product))) {
            $this->context->smarty->assign(array(
                'psv' => $this->getPSVersion(),
                'module_name' => $this->name,
                'payment_methods' => $this->getPaymentModulesByIdProduct($product->id),
            ));

            if ($this->getPSVersion() <= 1.6) {
                // PS 1.5, 1.6
                return $this->display(__FILE__, 'admin_products_extra.tpl');
            } else {
                // PS 1.7+
                return $this->display(__FILE__, 'admin_products_extra'.$this->getPSVersion(true).'.tpl');
            }
        }

        return false;
    }

    public function hookDisplayBackOfficeHeader($params)
    {
        // check whether it's a module page
        if (Tools::getValue('configure') == $this->name) {
            $this->context->controller->addJquery();
            $this->context->controller->addJS(array(
                $this->_path . 'views/js/admin.js',
            ));
        }
    }

    /**
     * @param int $id_product
     * @return array
     */
    protected function getPaymentModulesByIdProduct($id_product)
    {
        $modules = PaymentModule::getInstalledPaymentModules();

        $disabled_modules_result = Db::getInstance()->executeS('
            SELECT `id_module`
            FROM `'._DB_PREFIX_.'pmrestrictions_product`
            WHERE `id_product` = '.(int)$id_product.'
            AND `id_shop` = '.(int)$this->context->shop->id);
        $disabled_modules = array();
        foreach ($disabled_modules_result as $dmr) {
            $disabled_modules[] = $dmr['id_module'];
        }

        foreach ($modules as &$module) {
            $module['displayName'] = Module::getModuleName($module['name']);
            if (in_array($module['id_module'], $disabled_modules)) {
                $module['disabled'] = true;
            } else {
                $module['disabled'] = false;
            }
        }

        return $modules;
    }

    /**
     * @param array $params
     */
    public function hookActionProductSave($params)
    {
        $id_product = null;
        if (isset($params['id_product'])) {
            $id_product = $params['id_product'];
            $product = new Product($id_product);
        } elseif (isset($params['product']) && is_object($params['product'])) {
            $product = $params['product'];
            $id_product = $product->id;
        } elseif (isset($params['product']) && is_array($params['product'])) {
            $product = $params['product'];
            $id_product = (isset($product['id_product']) ? $product['id_product'] : null);
            $product = new Product($id_product);
        }
        if (!$id_product) {
            return false;
        }

        //process only if it's a product page
        if (Tools::isSubmit($this->name.'-submit')) {
            $modules = PaymentModule::getInstalledPaymentModules();
            $enabled_pmethods = Tools::getValue($this->name);
            $disabled_pmethods = array();

            foreach ($modules as $module) {
                if (is_array($enabled_pmethods) &&
                    !in_array($module['id_module'], $enabled_pmethods)
                    || !$enabled_pmethods
                    || !sizeof($enabled_pmethods)
                ) {
                    $disabled_pmethods[] = '('.(int)$module['id_module'].', '.
                        (int)$product->id.', '.(int)$this->context->shop->id.', 1)';
                }
            }

            // insert disabled payment methods to db table
            if (sizeof($disabled_pmethods)) {
                $query = '
                    INSERT IGNORE INTO `'._DB_PREFIX_.'pmrestrictions_product`
                    (`id_module`, `id_product`, `id_shop`, `disabled`)
                    VALUES
                    '.pSQL(implode(',', $disabled_pmethods)).'
                ';
                Db::getInstance()->execute($query);
            }

            // delete enabled payment methods from db table
            if (is_array($enabled_pmethods)) {
                $query = '
                    DELETE FROM `'._DB_PREFIX_.'pmrestrictions_product`
                    WHERE `id_module` IN ('.pSQL(implode(',', $enabled_pmethods)).')
                    AND `id_product` = '.(int)$product->id.'
                    AND `id_shop` = '.(int)$this->context->shop->id.'
                ';
                Db::getInstance()->execute($query);
            }
        }
    }

    /**
     * @param array $module_list
     * @return mixed
     */
    public function filterPaymentModules($module_list)
    {
        if (!is_array($module_list) || !$module_list || !is_object($this->context->cart)) {
            return $module_list;
        }
        $cart_products = $this->context->cart->getProducts();
        $cart_products_ids = array();

        // mode: all or default
        if ($this->mode == 'all') {
            $disabled_modules_result = array();
            foreach ($cart_products as $product) {
                $disabled_modules_tmp = Db::getInstance()->executeS(
                    'SELECT `id_module`
                    FROM `' . _DB_PREFIX_ . 'pmrestrictions_product`
                    WHERE `id_product` = ' . (int)$product['id_product'] . '
                    AND `disabled` = 1
                    AND `id_shop` = ' . (int)$this->context->shop->id
                );

                $ids = array();
                foreach ($disabled_modules_tmp as $disabled_module) {
                    $ids[] = (int)$disabled_module['id_module'];
                }
                $disabled_modules_result[] = $ids;
            }

            if (sizeof($disabled_modules_result) > 1) {
                $disabled_modules = call_user_func_array('array_intersect', $disabled_modules_result);
            } else {
                $disabled_modules = array_shift($disabled_modules_result);
            }

            foreach ($module_list as $key => $module) {
                if (in_array($module['id_module'], $disabled_modules)) {
                    unset($module_list[$key]);
                }
            }
        } else {
            foreach ($cart_products as $product) {
                $cart_products_ids[] = (int)$product['id_product'];
            }

            $disabled_modules_result = Db::getInstance()->executeS(
                'SELECT `id_module`
                FROM `' . _DB_PREFIX_ . 'pmrestrictions_product`
                WHERE `id_product` IN (' . pSQL(implode(',', $cart_products_ids)) . ')
                AND `disabled` = 1
                AND `id_shop` = ' . (int)$this->context->shop->id
            );
            $disabled_modules = array();
            foreach ($disabled_modules_result as $dmr) {
                $disabled_modules[] = $dmr['id_module'];
            }

            foreach ($module_list as $key => $module) {
                if (in_array($module['id_module'], $disabled_modules)) {
                    unset($module_list[$key]);
                }
            }
        }

        return $module_list;
    }

    protected function renderRestrictionsList()
    {
        $restrictions = $this->getRestrictionsList();

        if (!sizeof($restrictions)) {
            return null;
        }

        $token = Tools::getAdminTokenLite('AdminModules');
        $ajax_url = 'index.php?tab=AdminModules&configure=' . $this->name . '&token=' . $token;

        $this->context->smarty->assign([
            'restrictions' => $restrictions,
            'product_link' => $this->context->link->getAdminLink('AdminProducts'),
            'link' => $this->context->link,
            'key_tab' => 'Module'.Tools::ucfirst(Tools::strtolower($this->name)),
            'ajax_url' => $ajax_url,
            'psv' => $this->getPSVersion(),
        ]);

        return $this->context->smarty->fetch($this->local_path . 'views/templates/admin/restrictions_list.tpl');
    }

    protected function getRestrictionsList()
    {
        $restrictions = Db::getInstance()->executeS(
            'SELECT pp.*, pl.`name` as `product_name`, m.`name` as `module_name`
            FROM `' . _DB_PREFIX_ . 'pmrestrictions_product` pp
            LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON pp.`id_product` = pl.`id_product`
            LEFT JOIN `' . _DB_PREFIX_ . 'module` m ON pp.`id_module` = m.`id_module`
            WHERE pp.`disabled` = 1
            AND pp.`id_shop` = ' . (int)$this->context->shop->id
        );

        $result_restrictions = [];
        foreach ($restrictions as $restriction) {
            $module_name = $restriction['module_name'];
            $module_fullname = Module::getModuleName($restriction['module_name']);
            if (!isset($result_restrictions[$restriction['id_product']]) && !is_array($result_restrictions[$restriction['id_product']])) {
                $result_restrictions[$restriction['id_product']] = $restriction;
            }
            $result_restrictions[$restriction['id_product']]['disabled_modules'][$module_name] = $module_fullname;
        }

        return $result_restrictions;
    }

    public function ajaxProcessRemoveRestrictions()
    {
        $id_product = Tools::getValue('id_product');

        if ($id_product) {
            Db::getInstance()->execute('DELETE FROM `' . _DB_PREFIX_ . 'pmrestrictions_product` WHERE `id_product` = '.(int)$id_product.' AND `id_shop` = '.(int)$this->context->shop->id);
            die('1');
        }
    }
}
