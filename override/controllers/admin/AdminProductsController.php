<?php
class AdminProductsController extends AdminProductsControllerCore
{
	

	public function __construct()
	{
		
		parent::__construct();
		

		
		//Add new field supplier reference in field list
		$this->fields_list['posti'] = array(
			'title' => $this->l('Posti disponibili'),
			'align' => 'center',
			'filter_key' => 'a!posti',
			'width' => 80
		);
	} 

	/*
	protected function initProductList()
		{
			parent::initProductList();
			//Add new field to display
			$this->fields_list['products']['posti'] = array('title' => $this->l('Supplier reference'), 'width' => 'auto');
		}
	*/
}