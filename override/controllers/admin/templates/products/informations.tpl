{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if $check_product_association_ajax}
	{assign var=class_input_ajax value='check_product_name '}
{else}
	{assign var=class_input_ajax value=''}
{/if}

<style>

/*
	#link-Shipping, #link-link-Pack, #link-Customization, #link-Suppliers, #link-Attachments{
		display: none;
	}
*/

#link-Customization, #link-Suppliers, #link-Attachments, #link-Shipping{
	display: none;
}
</style>


{assign var="hide" value="0"}

<div id="product-informations" class="panel product-tab">
	<input type="hidden" name="submitted_tabs[]" value="Informations" />
	<h3 class="tab"> <i class="icon-info"></i> {l s='Information'}2</h3>
	<script type="text/javascript">

		var msg_select_one = "{l s='Please select at least one product.' js=1}";
		var msg_set_quantity = "{l s='Please set a quantity to add a product.' js=1}";

		{if isset($ps_force_friendly_product) && $ps_force_friendly_product}
			var ps_force_friendly_product = 1;
		{else}
			var ps_force_friendly_product = 0;
		{/if}
		{if isset($PS_ALLOW_ACCENTED_CHARS_URL) && $PS_ALLOW_ACCENTED_CHARS_URL}
			var PS_ALLOW_ACCENTED_CHARS_URL = 1;
		{else}
			var PS_ALLOW_ACCENTED_CHARS_URL = 0;
		{/if}
		{$combinationImagesJs}
		{if $check_product_association_ajax}
				var search_term = '';
				$('document').ready( function() {
					$(".check_product_name")
						.autocomplete(
							'{$link->getAdminLink('AdminProducts', true)|addslashes}', {
								minChars: 3,
								max: 10,
								width: $(".check_product_name").width(),
								selectFirst: false,
								scroll: false,
								dataType: "json",
								formatItem: function(data, i, max, value, term) {
									search_term = term;
									// adding the little
									if ($('.ac_results').find('.separation').length == 0)
										$('.ac_results').css('background-color', '#EFEFEF')
											.prepend('<div style="color:#585A69; padding:2px 5px">{l s='Use a product from the list'}<div class="separation"></div></div>');
									return value;
								},
								parse: function(data) {
									var mytab = new Array();
									for (var i = 0; i < data.length; i++)
										mytab[mytab.length] = { data: data[i], value: data[i].name };
									return mytab;
								},
								extraParams: {
									ajax: 1,
									action: 'checkProductName',
									id_lang: {$id_lang}
								}
							}
						)
						.result(function(event, data, formatted) {
							// keep the searched term in the input
							$('#name_{$id_lang}').val(search_term);
							jConfirm('{l s='Do you want to use this product?'}&nbsp;<strong>'+data.name+'</strong>', '{l s='Confirmation'}', function(confirm){
								if (confirm == true)
									document.location.href = '{$link->getAdminLink('AdminProducts', true)}&updateproduct&id_product='+data.id_product;
								else
									return false;
							});
						});
				});
		{/if}
	</script>

	{if isset($display_common_field) && $display_common_field}
	<div class="alert alert-warning" style="display: block">{l s='Warning, if you change the value of fields with an orange bullet %s, the value will be changed for all other shops for this product' sprintf=$bullet_common_field}</div>
	{/if}

	{include file="controllers/products/multishop/check_fields.tpl" product_tab="Informations"}

	<div class="form-group">
		<label class="control-label col-lg-3" for="simple_product">
			{$bullet_common_field} {l s='Type'}
		</label>
		<div class="col-lg-9">
			<div class="radio">
				<label for="simple_product">
					<input type="radio" name="type_product" id="simple_product" value="{Product::PTYPE_SIMPLE}" {if $product_type == Product::PTYPE_SIMPLE}checked="checked"{/if} >
					{l s='Standard product'}</label>
			</div>
			<div class="radio">
				<label for="pack_product">
					<input type="radio" name="type_product" {if $is_in_pack}disabled="disabled"{/if} id="pack_product" value="{Product::PTYPE_PACK}" {if $product_type == Product::PTYPE_PACK}checked="checked"{/if} > {l s='Pack of existing products'}</label>
			</div>
			<div class="radio">
				<label for="virtual_product">
					<input type="radio" name="type_product" id="virtual_product" {if $is_in_pack}disabled="disabled"{/if} value="{Product::PTYPE_VIRTUAL}" {if $product_type == Product::PTYPE_VIRTUAL}checked="checked"{/if} >
					{l s='Virtual product (services, booking, downloadable products, etc.)'}</label>
			</div>
			<div class="row row-padding-top">
				<div id="warn_virtual_combinations" class="alert alert-warning" style="display:none">{l s='You cannot use combinations with a virtual product.'}</div>
				<div id="warn_pack_combinations" class="alert alert-warning" style="display:none">{l s='You cannot use combinations with a pack.'}</div>
			</div>
		</div>
	</div>

	<div id="product-pack-container" {if $product_type != Product::PTYPE_PACK}style="display:none"{/if}></div>

	<hr />

	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="name" type="default" multilang="true"}</span></div>
		<label class="control-label col-lg-2 required" for="name_{$id_lang}">
			<span class="label-tooltip" data-toggle="tooltip" title="{l s='The public name for this product.'} {l s='Invalid characters:'} &lt;&gt;;=#{}">
				{l s='Name'}
			</span>
		</label>
		<div class="col-lg-5">
			{include file="controllers/products/input_text_lang.tpl"
				languages=$languages
				input_class="{$class_input_ajax}{if !$product->id || Configuration::get('PS_FORCE_FRIENDLY_PRODUCT')}copy2friendlyUrl{/if} updateCurrentText"
				input_value=$product->name
				input_name="name"
				required=true
			}
		</div>
	</div>

{*
	<div class="form-group">
    <label class="control-label col-lg-3" for="posti">
        <span class="label-tooltip" data-toggle="tooltip" title="{l s='Numero massimo posti'}">
            {$bullet_common_field} {l s='Numero posti'}
        </span>
    </label>
    <div class="col-lg-5">
        <input type="text" id="posti" name="posti" value="{$product->posti|htmlentitiesUTF8}" />
    </div>
	</div>
*}

	<div class="form-group">
		<label class="control-label col-lg-3" for="reference">
			<span class="label-tooltip" data-toggle="tooltip"
			title="{l s='Your internal reference code for this product.'} {l s='Allowed special characters:'} .-_#\">
				{$bullet_common_field} {l s='Reference code'}
			</span>
		</label>
		<div class="col-lg-5">
			<input type="text" id="reference" name="reference" value="{$product->reference|htmlentitiesUTF8}" />
		</div>
	</div>


	<hr/>


	{literal}

		<script>
            // This example displays an address form, using the autocomplete feature
            // of the Google Places API to help users fill in the information.

            // This example requires the Places library. Include the libraries=places
            // parameter when you first load the API. For example:
            // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

            var placeSearch, autocomplete;
            var componentForm = {
                street_number: 'short_name',
                route: 'long_name',
                locality: 'long_name',
                administrative_area_level_1: 'short_name',
                country: 'long_name',
                postal_code: 'short_name'
            };

            function initAutocomplete() {
                // Create the autocomplete object, restricting the search to geographical
                // location types.
                autocomplete = new google.maps.places.Autocomplete(
                    /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                    {types: ['geocode']});

                // When the user selects an address from the dropdown, populate the address
                // fields in the form.
                autocomplete.addListener('place_changed', fillInAddress);
            }

            function fillInAddress() {
                // Get the place details from the autocomplete object.
                var place = autocomplete.getPlace();

                for (var component in componentForm) {
                    document.getElementById(component).value = '';
                    document.getElementById(component).disabled = false;
                }



                // Get each component of the address from the place details
                // and fill the corresponding field on the form.
                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];

                    if (componentForm[addressType]) {
                        var val = place.address_components[i][componentForm[addressType]];
                        document.getElementById(addressType).value = val;
                    }
                }

                // url encode the address

                var address = "Via Emilia est,7 - 42049 Parma";
                address = $('#autocomplete').val();
                console.log($('#autocomplete').val());

                // google map geocode api url
                var url = "https://maps.google.com/maps/api/geocode/json?address="+address+"&key=AIzaSyAsKwBsOor4HSLPx9FqBIV8yzUmGr15i8M";

                $.getJSON(url, function (data) {
                    for(var i=0;i<data.results.length;i++) {
                        var lat = data.results[i].geometry.location.lat;
                        var lng = data.results[i].geometry.location.lng;

                    }

                    var lat_n = lat.toString().substring(0, 10);
                    var lng_n = lng.toString().substring(0, 10);

                    $('#lat').val(lat_n);
                    $('#lng').val(lng_n);


                });


            }

            // Bias the autocomplete object to the user's geographical location,
            // as supplied by the browser's 'navigator.geolocation' object.
            function geolocate() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        var geolocation = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };

                        //console.log("-->"+position.coords.latitude);

                        var circle = new google.maps.Circle({
                            center: geolocation,
                            radius: position.coords.accuracy
                        });
                        autocomplete.setBounds(circle.getBounds());
                    });
                }
            }
		</script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAsKwBsOor4HSLPx9FqBIV8yzUmGr15i8M&libraries=places&callback=initAutocomplete"
				async defer></script>


	{/literal}

	<style>
		/* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
		#map {
			height: 100%;
		}
		/* Optional: Makes the sample page fill the window. */
		html, body {
			height: 100%;
			margin: 0;
			padding: 0;
		}
	</style>
	<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
	<style>

		.fff.not2edit:before {
			content: '';
			position: absolute;
			top: -5px;
			left: 0;
			background-color: rgba(0,0,0,.35);
			width: 100%;
			height: 100%;
			display: block;
			padding: 10px 0;
			-webkit-box-sizing: content-box;
			-moz-box-sizing: content-box;
			box-sizing: content-box;
		}


		.fff.not2edit {
			position: relative;
		}

		#locationField, #controls {
			position: relative;
			width: 480px;
		}
		#autocomplete {
			position: absolute;
			top: 0px;
			left: 0px;
			width: 99%;
		}
		.label {
			text-align: right;
			font-weight: bold;
			width: 100px;
			color: #303030;
		}
		#address {
			border: 1px solid #dedede;
			/*background-color: #f0f0ff;*/
			width: 480px;
			padding-right: 2px;
			margin-top: 20px;
			margin-bottom: 20px;
		}
		#address td {
			font-size: 10pt;
			color: #000000;
		}
		.field {
			width: 99%;
		}
		.slimField {
			width: 80px;
		}
		.wideField {
			width: 200px;
		}
		#locationField {
			height: 20px;
			margin-bottom: 2px;
		}
	</style>




	<div class="form-group">
		<label class="control-label col-lg-3 required" for="to">
			<span class="label-tooltip" data-toggle="tooltip"
				  title="Indirizzo sede per la Google Maps">
				Sede
			</span>
		</label>
		<div class="col-lg-5">


			<div id="locationField">

				<input id="autocomplete" placeholder="Inserisci indirizzo" onFocus="geolocate()" type="text"></input>

			</div>





			<table id="address" class="fff not2edit">

				<tr>
					<td class="label" colspan="3">Indirizzo</td>

					<td class="wideField" colspan="3">
						<input class="field" id="route"  name="route" value="{$product->route}"></input>
					</td>
				</tr>


				<tr>
					<td class="label">n.</td>
					<td class="wideField" colspan="3">
						<input class="field" id="street_number" name="street_number" value="{$product->street_number}" ></input>
					</td>
				</tr>




				<tr>
					<td class="label">Città</td>

					<td class="wideField" colspan="3">
						<input class="field" id="locality" name="locality"  value="{$product->locality}"></input>
					</td>
				</tr>


				<tr>
					<td class="label">Regione</td>

					<td class="wideField" colspan="3">
						<input class="field" id="administrative_area_level_1"  name="administrative_area_level_1" value="{$product->administrative_area_level_1}" ></input>
					</td>

				</tr>


				<tr>
					<td class="label">CAP</td>

					<td class="wideField" colspan="3">
						<input class="field" id="postal_code" name="postal_code"  value="{$product->postal_code}"></input>
					</td>
				</tr>

				<tr>
					<td class="label">Nazione</td>
					<td class="wideField" colspan="3">
						<input class="field" id="country" name="country" value="{$product->country}"></input>
					</td>
				</tr>

				<tr>
					<td class="label">Latitudine</td>
					<td class="wideField" colspan="3">
						<input class="field" id="lat" name="lat" value="{$product->lat}"></input>
					</td>
				</tr>


				<tr>
					<td class="label">Longitudine</td>
					<td class="wideField" colspan="3">
						<input class="field" id="lng" name="lng" value="{$product->lng}"></input>
					</td>
				</tr>


			</table>




		</div>

		<div class="col-lg-6 col-lg-offset-3">
			<div class="help-block">
				Inserisci il tuo indirizzo nel formato "Indirizzo, Citta". Es: "Via Roma 10, Firenze". <br><b>Utile per la visualizzaione nella google MAPS</b>
			</div>
		</div>

	</div>


	{if (0) }
	<div class="form-group">
		<label class="control-label col-lg-3" for="ean13">
			<span class="label-tooltip" data-toggle="tooltip"
				title="{l s='This type of product code is specific to Europe and Japan, but is widely used internationally. It is a superset of the UPC code: all products marked with an EAN will be accepted in North America.'}">
				{$bullet_common_field} {l s='EAN-13 or JAN barcode'}
			</span>
		</label>
		<div class="col-lg-3">
			<input maxlength="13" type="text" id="ean13" name="ean13" value="{$product->ean13|htmlentitiesUTF8}" />
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-lg-3" for="upc">
			<span class="label-tooltip" data-toggle="tooltip"
				title="{l s='This type of product code is widely used in the United States, Canada, the United Kingdom, Australia, New Zealand and in other countries.'}">
				{$bullet_common_field} {l s='UPC barcode'}
			</span>
		</label>
		<div class="col-lg-3">
			<input maxlength="12" type="text" id="upc" name="upc" value="{$product->upc|escape:'html':'UTF-8'}" />
		</div>
	</div>
	{/if}

	<hr/>

	{* status informations *}
	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="active" type="radio" onclick=""}</span></div>
		<label class="control-label col-lg-2">
			{l s='Enabled'}
		</label>
		<div class="col-lg-9">
			<span class="switch prestashop-switch fixed-width-lg">
				<input onclick="toggleDraftWarning(false);showOptions(true);showRedirectProductOptions(false);" type="radio" name="active" id="active_on" value="1" {if $product->active || !$product->isAssociatedToShop()}checked="checked" {/if} />
				<label for="active_on" class="radioCheck">
					{l s='Yes'}
				</label>
				<input onclick="toggleDraftWarning(true);showOptions(false);showRedirectProductOptions(true);"  type="radio" name="active" id="active_off" value="0" {if !$product->active && $product->isAssociatedToShop()}checked="checked"{/if} />
				<label for="active_off" class="radioCheck">
					{l s='No'}
				</label>
				<a class="slide-button btn"></a>
			</span>
		</div>
	</div>


	{* Prenotazione immediata *}
	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="immediate" type="radio" onclick=""}</span></div>
		<label class="control-label col-lg-2">
			{l s='Prenotazione immediata'}


		</label>

		<div class="col-lg-9">
			<span class="switch prestashop-switch fixed-width-lg">
				<input onclick="toggleDraftWarning(false);showOptions(true);showRedirectProductOptions(false);" type="radio" name="immediate" id="immediate_on" value="1" {if $product->immediate }checked="checked" {/if} />
				<label for="immediate_on" class="radioCheck">
					{l s='Yes'}
				</label>
				<input onclick="toggleDraftWarning(true);showOptions(false);showRedirectProductOptions(true);"  type="radio" name="immediate" id="immediate_off" value="0" {if !$product->immediate}checked="checked"{/if} />
				<label for="immediate_off" class="radioCheck">
					{l s='No'}
				</label>
				<a class="slide-button btn"></a>
			</span>
			<br>
			{l s='Annunci prenotabili senza dover aspettare l\'approvazione dell\'host'}
		</div>
	</div>


	<div class="form-group redirect_product_options" style="display:none">
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="redirect_type" type="radio" onclick=""}</span></div>
		<label class="control-label col-lg-2" for="redirect_type">
			{l s='Redirect when disabled'}
		</label>
		<div class="col-lg-5">
			<select name="redirect_type" id="redirect_type">
				<option value="404" {if $product->redirect_type == '404'} selected="selected" {/if}>{l s='No redirect (404)'}</option>
				<option value="301" {if $product->redirect_type == '301'} selected="selected" {/if}>{l s='Redirected permanently (301)'}</option>
				<option value="302" {if $product->redirect_type == '302'} selected="selected" {/if}>{l s='Redirected temporarily (302)'}</option>
			</select>
		</div>
	</div>
	<div class="form-group redirect_product_options" style="display:none">
		<div class="col-lg-9 col-lg-offset-3">
			<div class="alert alert-info">
				{l s='404 Not Found = Do not redirect and display a 404 page.'}<br/>
				{l s='301 Moved Permanently = Permanently display another product instead.'}<br/>
				{l s='302 Moved Temporarily = Temporarily display another product instead.'}
			</div>
		</div>
	</div>

	<div class="form-group redirect_product_options redirect_product_options_product_choise" style="display:none">
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="id_product_redirected" type="radio" onclick=""}</span></div>
		<label class="control-label col-lg-2" for="related_product_autocomplete_input">
			{l s='Related product:'}
		</label>
		<div class="col-lg-7">
			<input type="hidden" value="" name="id_product_redirected" />

			<div class="input-group">
				<input type="text" id="related_product_autocomplete_input" name="related_product_autocomplete_input" autocomplete="off" class="ac_input" />
				<span class="input-group-addon"><i class="icon-search"></i></span>
			</div>

			<div class="form-control-static">
				<span id="related_product_name"><i class="icon-warning-sign"></i>&nbsp;{l s='No related product.'}</span>
				<span id="related_product_remove" style="display:none">
					<a class="btn btn-default" href="#" onclick="removeRelatedProduct(); return false" id="related_product_remove_link">
						<i class="icon-remove text-danger"></i>
					</a>
				</span>
			</div>

		</div>
		<script>
			var no_related_product = '{l s='No related product'}';
			var id_product_redirected = {$product->id_product_redirected|intval};
			var product_name_redirected = '{$product_name_redirected|escape:'html':'UTF-8'}';
		</script>
	</div>

	<input type="hidden" name="visibility" value="both">
	<input type="hidden" name="available_for_order" value="1">
	<input type="hidden" name="show_price" value="1">
	<input type="hidden" name="condition" value="new">


	{if (0)}
	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="visibility" type="default"}</span></div>
		<label class="control-label col-lg-2" for="visibility">
			{l s='Visibility'}
		</label>
		<div class="col-lg-3">
			<select name="visibility" id="visibility">
				<option value="both" {if $product->visibility == 'both'}selected="selected"{/if} >{l s='Everywhere'}</option>
				<option value="catalog" {if $product->visibility == 'catalog'}selected="selected"{/if} >{l s='Catalog only'}</option>
				<option value="search" {if $product->visibility == 'search'}selected="selected"{/if} >{l s='Search only'}</option>
				<option value="none" {if $product->visibility == 'none'}selected="selected"{/if}>{l s='Nowhere'}</option>
			</select>
		</div>
	</div>

	<div id="product_options" class="form-group">
		<div class="col-lg-12">
			<div class="form-group">
				<div class="col-lg-1">
					<span class="pull-right">
						{if isset($display_multishop_checkboxes) && $display_multishop_checkboxes}
							{include file="controllers/products/multishop/checkbox.tpl" only_checkbox="true" field="available_for_order" type="default"}
							{include file="controllers/products/multishop/checkbox.tpl" only_checkbox="true" field="show_price" type="show_price"}
							{include file="controllers/products/multishop/checkbox.tpl" only_checkbox="true" field="online_only" type="default"}
						{/if}
					</span>
				</div>
				<label class="control-label col-lg-2" for="available_for_order">
					{l s='Options'}
				</label>
				<div class="col-lg-9">
					<div class="checkbox">
						<label for="available_for_order">
							<input type="checkbox" name="available_for_order" id="available_for_order" value="1" {if $product->available_for_order}checked="checked"{/if} >
							{l s='Available for order'}</label>
					</div>
					<div class="checkbox">
						<label for="show_price">
							<input type="checkbox" name="show_price" id="show_price" value="1" {if $product->show_price}checked="checked"{/if} {if $product->available_for_order}disabled="disabled"{/if} >
							{l s='Show price'}</label>
					</div>
					<div class="checkbox">
						<label for="online_only">
							<input type="checkbox" name="online_only" id="online_only" value="1" {if $product->online_only}checked="checked"{/if} >
							{l s='Online only (not sold in your retail store)'}</label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="condition" type="default"}</span></div>
				<label class="control-label col-lg-2" for="condition">
					{l s='Condition'}
				</label>
				<div class="col-lg-3">
					<select name="condition" id="condition">
						<option value="new" {if $product->condition == 'new'}selected="selected"{/if} >{l s='New'}</option>
						<option value="used" {if $product->condition == 'used'}selected="selected"{/if} >{l s='Used'}</option>
						<option value="refurbished" {if $product->condition == 'refurbished'}selected="selected"{/if}>{l s='Refurbished'}</option>
					</select>
				</div>
			</div>
		</div>
	</div>
	{/if}
	<hr/>


	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="regole" type="tinymce" multilang="true"}</span></div>
		<label class="control-label col-lg-2" for="description_short_{$id_lang}">
			<span class="label-tooltip" data-toggle="tooltip" title="{l s='Descrizione visualizzata nella sezione Regole della casa'}">
				{l s='Regole della casa'}
			</span>
		</label>
		<div class="col-lg-9">
			{include
			file="controllers/products/textarea_lang.tpl"
			languages=$languages
			input_name='regole'
			class="autoload_rte"
			input_value=$product->regole
			max=$PS_PRODUCT_SHORT_DESC_LIMIT}
		</div>
	</div>

	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="description_zona" type="tinymce" multilang="true"}</span></div>
		<label class="control-label col-lg-2" for="description_short_{$id_lang}">
			<span class="label-tooltip" data-toggle="tooltip" title="{l s='Descrizione visualizzata prima della mappa google'}">
				{l s='Descrizione della zona'}
			</span>
		</label>
		<div class="col-lg-9">
			{include
			file="controllers/products/textarea_lang.tpl"
			languages=$languages
			input_name='description_zona'
			class="autoload_rte"
			input_value=$product->description_zona
			max=$PS_PRODUCT_SHORT_DESC_LIMIT}
		</div>
	</div>

	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="description_short" type="tinymce" multilang="true"}</span></div>
		<label class="control-label col-lg-2" for="description_short_{$id_lang}">
			<span class="label-tooltip" data-toggle="tooltip" title="{l s='Appears in the product list(s), and at the top of the product page.'}">
				{l s='Short description'}
			</span>
		</label>
		<div class="col-lg-9">
			{include
				file="controllers/products/textarea_lang.tpl"
				languages=$languages
				input_name='description_short'
				class="autoload_rte"
				input_value=$product->description_short
				max=$PS_PRODUCT_SHORT_DESC_LIMIT}
		</div>
	</div>
	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="description" type="tinymce" multilang="true"}</span></div>
		<label class="control-label col-lg-2" for="description_{$id_lang}">
			<span class="label-tooltip" data-toggle="tooltip"
				title="{l s='Appears in the body of the product page.'}">
				{l s='Description'}
			</span>
		</label>
		<div class="col-lg-9">
			{include
				file="controllers/products/textarea_lang.tpl"
				languages=$languages input_name='description'
				class="autoload_rte"
				input_value=$product->description}
		</div>
	</div>
	{if $images}
	<div class="form-group">
		<div class="col-lg-9 col-lg-offset-3">
			<div class="alert alert-info">
				{capture}<a class="addImageDescription" href="javascript:void(0);">{l s='Click here'}</a>{/capture}
				{l s='Would you like to add an image in your description? %s and paste the given tag in the description.' sprintf=$smarty.capture.default}
			</div>
		</div>
	</div>
	<div id="createImageDescription" class="panel" style="display:none">
		<div class="form-group">
			<label class="control-label col-lg-3" for="smallImage_0">{l s='Select your image'}</label>
			<div class="col-lg-9">
				<ul class="list-inline">
					{foreach from=$images item=image key=key}
					<li>
						<input type="radio" name="smallImage" id="smallImage_{$key}" value="{$image.id_image}" {if $key == 0}checked="checked"{/if} >
						<label for="smallImage_{$key}" >
							<img src="{$image.src}" alt="{$image.legend}" />
						</label>
					</li>
					{/foreach}
				</ul>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-3" for="leftRight_1">{l s='Position'}</label>
			<div class="col-lg-5">
				<p class="checkbox">
					<input type="radio" name="leftRight" id="leftRight_1" value="left" checked>
					<label for="leftRight_1" >{l s='left'}</label>
				</p>
				<p class="checkbox">
					<input type="radio" name="leftRight" id="leftRight_2" value="right">
					<label for="leftRight_2" >{l s='right'}</label>
				</p>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-3" for="imageTypes_0">{l s='Select the type of picture'}</label>
			<div class="col-lg-5">
				{foreach from=$imagesTypes key=key item=type}
				<p class="checkbox">
					<input type="radio" name="imageTypes" id="imageTypes_{$key}" value="{$type.name}" {if $key == 0}checked="checked"{/if}>
					<label for="imageTypes_{$key}" >
						{$type.name} <span>{l s='%dpx by %dpx' sprintf=[$type.width, $type.height]}</span>
					</label>
				</p>
				{/foreach}
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-3" for="resultImage">
				<span class="label-tooltip" data-toggle="tooltip"
				title="{l s='The tag to copy/paste into the description.'}">
					{l s='Image tag to insert'}
				</span>
			</label>
			<div class="col-lg-4">
				<input type="text" id="resultImage" name="resultImage" />
			</div>
			<p class="help-block"></p>
		</div>
	</div>
	{/if}

	<div class="form-group">
		<label class="control-label col-lg-3" for="tags_{$id_lang}">
			<span class="label-tooltip" data-toggle="tooltip"
				title="{l s='Will be displayed in the tags block when enabled. Tags help customers easily find your products.'}">
				{l s='Tags:'}
			</span>
		</label>
		<div class="col-lg-9">
			{if $languages|count > 1}
			<div class="row">
			{/if}
				{foreach from=$languages item=language}
					{literal}
					<script type="text/javascript">
						$().ready(function () {
							var input_id = '{/literal}tags_{$language.id_lang}{literal}';
							$('#'+input_id).tagify({delimiters: [13,44], addTagPrompt: '{/literal}{l s='Add tag' js=1}{literal}'});
							$({/literal}'#{$table}{literal}_form').submit( function() {
								$(this).find('#'+input_id).val($('#'+input_id).tagify('serialize'));
							});
						});
					</script>
					{/literal}
				{if $languages|count > 1}
				<div class="translatable-field lang-{$language.id_lang}">
					<div class="col-lg-9">
				{/if}
						<input type="text" id="tags_{$language.id_lang}" class="tagify updateCurrentText" name="tags_{$language.id_lang}" value="{$product->getTags($language.id_lang, true)|htmlentitiesUTF8}" />
				{if $languages|count > 1}
					</div>
					<div class="col-lg-2">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
							{$language.iso_code}
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							{foreach from=$languages item=language}
							<li>
								<a href="javascript:tabs_manager.allow_hide_other_languages = false;hideOtherLanguage({$language.id_lang});">{$language.name}</a>
							</li>
							{/foreach}
						</ul>
					</div>
				</div>
				{/if}
				{/foreach}
			{if $languages|count > 1}
			</div>
			{/if}
		</div>
		<div class="col-lg-9 col-lg-offset-3">
			<div class="help-block">{l s='Each tag has to be followed by a comma. The following characters are forbidden: %s' sprintf='!&lt;;&gt;;?=+#&quot;&deg;{}_$%.'}
			</div>
		</div>
	</div>
	<div class="panel-footer">
		<a href="{$link->getAdminLink('AdminProducts')|escape:'html':'UTF-8'}{if isset($smarty.request.page) && $smarty.request.page > 1}&amp;submitFilterproduct={$smarty.request.page|intval}{/if}" class="btn btn-default"><i class="process-icon-cancel"></i> {l s='Cancel'}</a>
		<button type="submit" name="submitAddproduct" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> {l s='Save'}</button>
		<button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> {l s='Save and stay'}</button>
	</div>
</div>
<script type="text/javascript">
	hideOtherLanguage({$default_form_language});
	var missing_product_name = '{l s='Please fill product name input field' js=1}';
</script>
