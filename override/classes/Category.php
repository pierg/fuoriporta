<?php

class Category extends CategoryCore
{
        public function getProducts($id_lang, $p, $n, $order_by = null, $order_way = null, $get_total = false, $active = true, $random = false, $random_number_products = 1, $check_access = true, Context $context = null)
    {
    	$resultado = parent::getProducts($id_lang, $p, $n, $order_by, $order_way, $get_total, $active, $random, $random_number_products, $check_access, $context);
    	if($resultado && is_array($resultado)){
        	$resultado = Product::getProductsProperties((int)$id_lang, $resultado,  true);  
        }
        return $resultado;
    }
	

}
