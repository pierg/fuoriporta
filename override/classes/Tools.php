<?php
/*
* 2010-2012 LaBulle All right reserved
*/
if (_PS_VERSION_ >= "1.5.0.0") {
	class Tools extends ToolsCore
	{
		public static function displayPrice($price, $currency = NULL, $no_utf8 = false, Context $context = null)
		{
			if (Configuration::get('MYOWNRES_PRICE_TYPE')>0 && class_exists('reservation_price_display') && Configuration::get('MYOWNRES_PRICE_TYPE')==reservation_price_display::HIDE && intval($price)==0) return "";
			else return parent::displayPrice($price, $currency, $no_utf8, $context);
		}
	
	}
} else {
	class Tools extends ToolsCore
	{
		public static function displayPrice($price, $currency = NULL, $no_utf8 = false)
		{
			if (class_exists('reservation_price_display') && Configuration::get('MYOWNRES_PRICE_TYPE')==reservation_price_display::HIDE && intval($price)==0) return "";
			else return parent::displayPrice($price, $currency, $no_utf8);
		}
	
	}
}