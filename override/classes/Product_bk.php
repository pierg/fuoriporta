<?php
/*
* 2010-2012 LaBulle All right reserved
*/
class ProductTemp extends ProductCore
{
	public static function getOrderIdFromCart($id_cart) {
		$orders=Db::getInstance()->ExecuteS("SELECT `id_order` FROM `" . _DB_PREFIX_ ."orders` WHERE `id_cart` ='". $id_cart ."';");
		if (count($orders)>0) 
 			return $orders[0]['id_order'];
 		else
      		return 0;
	}
	
	public function loadStockData() {
		global $cart;
		parent::loadStockData();
		if ($cart!=null && $cart->myownreservations!=null) {
			$mainProduct = $cart->myownreservations->_products->getResProductFromProduct($this->id);
			if ($mainProduct!=null) $this->out_of_stock = true;
		}
	}
	
	public function checkQty($qty) {
		global $cart;
		if ($cart!=null && $cart->myownreservations!=null) {
			$mainProduct = $cart->myownreservations->_products->getResProductFromProduct($this->id);
			if ($mainProduct!=null) return true;
		}
		return parent::checkQty($qty);
	}
	
	public static function getAllCustomizedDatas($id_cart, $id_lang = null, $only_in_cart = true, $order = null, $id_customization = NULL)
	{
		global $cart;
		global $cookie;
		if (!$id_cart)
			return false;
		
		$customizedDatas = parent::getAllCustomizedDatas($id_cart, $id_lang, $only_in_cart);
		if (!$customizedDatas) $customizedDatas=array();
		if ($cart==null or $cart->id!=$id_cart) {
				$id_order = self::getOrderIdFromCart($id_cart); //$cart= new Cart($id_cart);
				if ($id_order) $order=new Order($id_order);
				$cart=new Cart($id_cart);
		}
		if ($cart!=null && $cart->myownreservations==null) return $customizedDatas;
		if ($order != null) {
			$cart= new Cart($order->id_cart);
			$resaOrders = new myOwnResas($order->id, $cart->myownreservations->_products, $cart->myownreservations->_pricerules, $cart->myownreservations->_timeSlots);
			$cartproducts = $cart->getProducts();
			$display_taxes = !($order->getTaxCalculationMethod() == PS_TAX_EXC);
			$orderStatus = $order->getCurrentState();
		} else {
			$orderStatus=0;
			$cartproducts = $cart->getProducts();
			$display_taxes = $cart->display_taxes;
		}
		$trace=debug_backtrace();
		if (count($trace>1)) {
			$func = strtolower($trace[1]['function']);
			$class = strtolower($trace[1]['class']);
		}
		if ($func!='prestashop\prestashop\adapter\cart\{closure}' && $func != "getcontent" && $func != "_assignsummaryinformations" && $func != "run" && $func != "displayajax" && $func != "viewdetails" && $func != "getproducts" && $class != "admincartscontrollercore" && ($order == null && $cart->id!=$id_cart)) return $customizedDatas;
		$obj=$cart->myownreservations;
		$resv_title=Configuration::get('MYOWNRES_MSG_RESV', $cookie->id_lang);
		if ($resv_title=='') $resv_title = ucfirst(myOwnLang::$object[MYOWN_OBJ::RESV]);
		foreach ($cartproducts as $product) {
			if ($order != null) {
				if (array_key_exists('id_product_attribute', $product)) {
					$id_product = $product['id_product'];
					$id_product_attribute = $product['id_product_attribute'];
				} else {
					$id_product = $product['product_id'];
					$id_product_attribute = $product['product_attribute_id'];
				}
				$resas = $resaOrders->getResas($id_product, $id_product_attribute);
			}
			else {
				$id_product = $product['id_product'];
				$id_product_attribute = $product['id_product_attribute'];
				$resas=$cart->getMyOwnReservationCart()->getResas($id_product, $id_product_attribute, true);
			}
			$virtualCustomization=array();
			
			foreach ($resas as $resa)
				if ((!array_key_exists('id_resa', $product) || $product['id_resa']==$resa->sqlId || $product['id_resa']==$resa->id_cartproduct)
					&& ($id_customization==null || $id_customization==-$resa->sqlId))
			{
				$resaLabel=$resv_title;
				if ($resa->id_customization) {
					if (_PS_VERSION_ < "1.5.0.0") 
						$customizedDatas[$resa->id_product][$resa->id_product_attribute][$resa->id_customization]['datas'][1][] = $resa->getCustomArray($obj, $cookie, $resaLabel);
					else {
						$customizedDatas[$resa->id_product][$resa->id_product_attribute][$cart->id_address_delivery][$resa->id_customization]['quantity'] = $resa->quantity;
						$customizedDatas[$resa->id_product][$resa->id_product_attribute][$cart->id_address_delivery][$resa->id_customization]['datas'][1][] = $resa->getCustomArray($obj, $cookie, $resv_title, $display_taxes, $orderStatus);
					}
				} else {
					$custQty = $resa->quantity;
					$virtualCustomization[-$resa->sqlId] = array(
						'id_customization' => (_PS_VERSION_ < "1.7.0.0" ? $resa->id_customization : -$resa->sqlId),
						'quantity' => $resa->quantity, 
						'resas_qty' => $resa->quantity, 
						'quantity_refunded' => 0,
						'quantity_returned' => 0,
						'datas' => array(
							1 => array(
								0 => $resa->getCustomArray($obj, $cookie, $resaLabel, $display_taxes, ($order != null && $class == "ordercore"))
								)
							)
						);
					if ($order != null) $virtualCustomization[-$resa->sqlId]['quantity_refunded'] = $resa->quantity_refunded;
				}
			}
			if (count($resas)) {
				if (_PS_VERSION_ < "1.5.0.0") 
					$customizedDatasToAdd = $virtualCustomization; //array($id_product_attribute => 
				else $customizedDatasToAdd = array($cart->id_address_delivery => $virtualCustomization); //array($id_product_attribute => 
				
				if (array_key_exists($id_product, $customizedDatas)) {
					if (array_key_exists($id_product_attribute, $customizedDatas[$id_product])) 
						if (_PS_VERSION_ < "1.5.0.0") 
							$customizedDatas[$id_product][$id_product_attribute] += $customizedDatasToAdd;
						else if (array_key_exists($cart->id_address_delivery, $customizedDatas[$id_product][$id_product_attribute])) 
							$customizedDatas[$id_product][$id_product_attribute][$cart->id_address_delivery] += $virtualCustomization;
						else $customizedDatas[$id_product][$id_product_attribute][$cart->id_address_delivery] = $virtualCustomization;
					else $customizedDatas[$id_product][$id_product_attribute] = $customizedDatasToAdd;
				}
				else $customizedDatas[$id_product] = array($id_product_attribute => $customizedDatasToAdd);
			}
		}
		if ($customizedDatas!=array()) return $customizedDatas;
	}
	
	public static function addProductCustomizationPrice(&$product, &$customized_datas)
    {
        if (!$customized_datas) {
            return;
        }
        $products = [$product];
        self::addCustomizationPrice($products, $customized_datas);
        $product = $products[0];
    }
	
	public static function addCustomizationPrice(&$products, &$customizedDatas)
	{	
		global $cart;
		Configuration::updateGlobalValue('PS_CUSTOMIZATION_FEATURE_ACTIVE', '1');
		parent::addCustomizationPrice($products, $customizedDatas);
		$trace=debug_backtrace();
		if (count($trace>1)) {
			$func=strtolower($trace[1]['function']);
			$class = strtolower($trace[1]['class']);
		}
		
		if ($func=='getproducts' && $class='ordercore' && Tools::getIsset('id_order') && !Tools::getIsset('vieworder')) {
			$id_order = Tools::getValue('id_order');
			$tmpOrder = new Order($id_order);
			if ($tmpOrder->id_cart) {
				$displayPrice =intval(Configuration::get('MYOWNRES_PRICE_TYPE')!=reservation_price_display::HIDE);
				$cart->list = myOwnCarts::getProducts($tmpOrder->id_cart, $cart->myownreservations->_products, $cart->myownreservations->_pricerules);
				$products = $cart->getMyOwnReservationCart()->changeProducts($cart->myownreservations, $products, false, false, $displayPrice, false);
			}
		}
		if (_PS_VERSION_ >= "1.5.0.0" && ($func == "addproductcustomizationprice" or $func == "_assignsummaryinformations" or $func == "getproducts" or $func == "run" or $func == "displayajax" or $class == "admincartscontrollercore")) {
			foreach ($products AS &$productUpdate)
			{
				if (isset($productUpdate['resas_total']) && $productUpdate['resas_total']>0) {
					$productUpdate['total_customization_wt'] = $productUpdate['resas_total_wt'];
					$productUpdate['total_customization'] = $productUpdate['resas_total'];
					$productUpdate['total_wt'] += $productUpdate['total_customization_wt'];
					$productUpdate['total'] += $productUpdate['total_customization'];
					$productUpdate['customizationQuantityTotal'] = $productUpdate['resas_qty'];
					$productUpdate['customization_quantity'] = $productUpdate['resas_qty'];
				}
			}
		}	
		if (_PS_VERSION_ < "1.5.0.0" && ($func == "viewdetails" or $func == "_assignsummaryinformations")) {
			foreach ($products AS &$productUpdate)
			{
				if (array_key_exists('resas_total_wt', $productUpdate)) {
					$productUpdate['total_customization_wt']=$productUpdate['resas_total_wt'];
					$productUpdate['total_wt']+=$productUpdate['resas_total_wt'];
					$productUpdate['total_customization']=$productUpdate['resas_total'];
					$productUpdate['total']+=$productUpdate['resas_total'];
				}
			}
		}
		
		/*
		if ($func == "viewdetails") {
				foreach ($products AS &$productUpdate)
				{
				}
		}
		*/
	}
}
if (_PS_VERSION_ < "1.7.0.0") {
	class Product extends ProductTemp
	{
		/*
    * module: myownreservations
    * date: 2018-12-18 16:12:36
    * version: 4.2.0
    */
    public static function getPriceStatic($id_product, $usetax = true, $id_product_attribute = null, $decimals = 6, $divisor = null,
			$only_reduc = false, $usereduc = true, $quantity = 1, $force_associated_tax = false, $id_customer = null, $id_cart = null,
			$id_address = null, &$specific_price_output = null, $with_ecotax = true, $use_group_reduction = true, Context $context = null,
			$use_customer_price = true)
		{
			if (!$context)
				$context = Context::getContext();
	
			$cur_cart = $context->cart;
			
			$trace=debug_backtrace();$func='';$class='';
			if (count($trace>1)) $func=strtolower($trace[1]['function']);
			if (count($trace>1)) $class=strtolower($trace[1]['class']);
	
			if ($func=='getordertotal' && $class=='cartcore_old' && $cur_cart->getMyOwnReservationCart() != null) {
				foreach ($cur_cart->resaCarts->list as $cartproduct)
					if ($cartproduct->id_product==$id_product && $cartproduct->id_product_attribute==$id_product_attribute)
						return $cartproduct->getUnitPriceWithReduc($usetax);
			} else 
				return parent::getPriceStatic($id_product, $usetax, $id_product_attribute, $decimals, $divisor,
			$only_reduc, $usereduc, $quantity, $force_associated_tax, $id_customer, $id_cart,
			$id_address, $specific_price_output, $with_ecotax, $use_group_reduction, $context,
			$use_customer_price); 
		}
		
		 public $posti;

    public function __construct($id_product = null, $full = false, $id_lang = null, $id_shop = null, Context $context = null)
    {
        self::$definition['fields']['posti'] = array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt');
        parent::__construct($id_product, $full, $id_lang, $id_shop);
    }



 public static function getProductProperties($id_lang, $row, Context $context = null, $check_unavailable=false)
    {
        if (!$row['id_product']) {
            return false;
        }

        if ($context == null) {
            $context = Context::getContext();
        }

        $id_product_attribute = $row['id_product_attribute'] = (!empty($row['id_product_attribute']) ? (int)$row['id_product_attribute'] : null);	
        
        // Product::getDefaultAttribute is only called if id_product_attribute is missing from the SQL query at the origin of it:
        // consider adding it in order to avoid unnecessary queries
        $row['allow_oosp'] = Product::isAvailableWhenOutOfStock($row['out_of_stock']);
        if (Combination::isFeatureActive() && $id_product_attribute === null
            && ((isset($row['cache_default_attribute']) && ($ipa_default = $row['cache_default_attribute']) !== null)
                || ($ipa_default = Product::getDefaultAttribute($row['id_product'], !$row['allow_oosp'])))) {
            $id_product_attribute = $row['id_product_attribute'] = $ipa_default;
        }
        if (!Combination::isFeatureActive() || !isset($row['id_product_attribute'])) {
            $id_product_attribute = $row['id_product_attribute'] = 0;
        }
        
        $quantity =0;
		//se ha quitado el check_unavailable para forzar que esta comprobacion se realice siempre
		//ya que en los modulos (blocknewproducts, blockbestsellers, blockspecials) siempre llega false
        if(!$row['allow_oosp'] && Configuration::get('PS_DISP_UNAVAILABLE_ATTR') == 0){
	        if ($id_product_attribute) {
	             $quantity = Product::getQuantity(
	                (int)$row['id_product'],
	                $id_product_attribute,
	                isset($row['cache_is_pack']) ? $row['cache_is_pack'] : null
	            );
	            $quantity_all_versions = Product::getQuantity(
		            (int)$row['id_product'],
		            0,
		            isset($row['cache_is_pack']) ? $row['cache_is_pack'] : null
		        );
		        if($quantity==0 && $quantity_all_versions>0){
		        	$sql = 'select id_product_attribute from '._DB_PREFIX_.'stock_available sa where quantity>0 and id_product_attribute<>0 and id_product = '.(int) $row['id_product'].' and id_shop = '.$context->shop->id;
		        	$row['id_product_attribute'] = $id_product_attribute = Db::getInstance()->getValue($sql);
		        }
	        }
        }

 		$id_image = $row['id_image'];
 		$row = parent::getProductProperties($id_lang, $row, $context);
		$arrexp = explode("-",$id_image);
		if(count($arrexp)<2){
			$id_image = $row['id_product'].'-'.$id_image;
		}
		$row['id_image'] = $id_image;
		return $row;
    }
    
	public static function getProductsProperties($id_lang, $query_result, $check_unavailable=false)
    {
        $results_array = array();

        if (is_array($query_result)) {
            foreach ($query_result as $row) {
                if ($row2 = Product::getProductProperties($id_lang, $row, null, $check_unavailable)) {
                    $results_array[] = $row2;
                }
            }
        }

        return $results_array;
    }
	}
} else {
	class Product extends ProductTemp
	{
		/*
    * module: myownreservations
    * date: 2018-12-18 16:12:36
    * version: 4.2.0
    */
    public static function getPriceStatic($id_product, $usetax = true, $id_product_attribute = NULL, $decimals = 6, $divisor = NULL, $only_reduc = false, $usereduc = true, $quantity = 1, $force_associated_tax = false, $id_customer = NULL, $id_cart = NULL, $id_address = NULL, &$specific_price_output = NULL, $with_ecotax = true, $use_group_reduction = true, Context $context = NULL, $use_customer_price = true, $id_customization = NULL)
		{
			if (!$context)
				$context = Context::getContext();
	
			$cur_cart = $context->cart;
			
			$trace=debug_backtrace();$func='';$class='';
			if (count($trace>1)) $func=strtolower($trace[1]['function']);
			if (count($trace>1)) $class=strtolower($trace[1]['class']);
			
			if ((!is_object($cur_cart) || (Validate::isUnsignedInt($id_cart) && $id_cart && $cur_cart->id != $id_cart))
				|| ($func=='getordertotal' && $class=='cartcore_old' && $cur_cart->getMyOwnReservationCart() != null)) {
				foreach ($cur_cart->resaCarts->list as $cartproduct)
					if ($cartproduct->id_product==$id_product && $cartproduct->id_product_attribute==$id_product_attribute)
						return $cartproduct->getUnitPriceWithReduc($usetax);
			} else 
				return parent::getPriceStatic($id_product, $usetax, $id_product_attribute, $decimals, $divisor,
			$only_reduc, $usereduc, $quantity, $force_associated_tax, $id_customer, $id_cart,
			$id_address, $specific_price_output, $with_ecotax, $use_group_reduction, $context,
			$use_customer_price, $id_customization); 
		}
	
	
	
	 public $posti;

    public function __construct($id_product = null, $full = false, $id_lang = null, $id_shop = null, Context $context = null)
    {
        self::$definition['fields']['posti'] = array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt');
        parent::__construct($id_product, $full, $id_lang, $id_shop);
    }



 public static function getProductProperties($id_lang, $row, Context $context = null, $check_unavailable=false)
    {
        if (!$row['id_product']) {
            return false;
        }

        if ($context == null) {
            $context = Context::getContext();
        }

        $id_product_attribute = $row['id_product_attribute'] = (!empty($row['id_product_attribute']) ? (int)$row['id_product_attribute'] : null);	
        
        // Product::getDefaultAttribute is only called if id_product_attribute is missing from the SQL query at the origin of it:
        // consider adding it in order to avoid unnecessary queries
        $row['allow_oosp'] = Product::isAvailableWhenOutOfStock($row['out_of_stock']);
        if (Combination::isFeatureActive() && $id_product_attribute === null
            && ((isset($row['cache_default_attribute']) && ($ipa_default = $row['cache_default_attribute']) !== null)
                || ($ipa_default = Product::getDefaultAttribute($row['id_product'], !$row['allow_oosp'])))) {
            $id_product_attribute = $row['id_product_attribute'] = $ipa_default;
        }
        if (!Combination::isFeatureActive() || !isset($row['id_product_attribute'])) {
            $id_product_attribute = $row['id_product_attribute'] = 0;
        }
        
        $quantity =0;
		//se ha quitado el check_unavailable para forzar que esta comprobacion se realice siempre
		//ya que en los modulos (blocknewproducts, blockbestsellers, blockspecials) siempre llega false
        if(!$row['allow_oosp'] && Configuration::get('PS_DISP_UNAVAILABLE_ATTR') == 0){
	        if ($id_product_attribute) {
	             $quantity = Product::getQuantity(
	                (int)$row['id_product'],
	                $id_product_attribute,
	                isset($row['cache_is_pack']) ? $row['cache_is_pack'] : null
	            );
	            $quantity_all_versions = Product::getQuantity(
		            (int)$row['id_product'],
		            0,
		            isset($row['cache_is_pack']) ? $row['cache_is_pack'] : null
		        );
		        if($quantity==0 && $quantity_all_versions>0){
		        	$sql = 'select id_product_attribute from '._DB_PREFIX_.'stock_available sa where quantity>0 and id_product_attribute<>0 and id_product = '.(int) $row['id_product'].' and id_shop = '.$context->shop->id;
		        	$row['id_product_attribute'] = $id_product_attribute = Db::getInstance()->getValue($sql);
		        }
	        }
        }

 		$id_image = $row['id_image'];
 		$row = parent::getProductProperties($id_lang, $row, $context);
		$arrexp = explode("-",$id_image);
		if(count($arrexp)<2){
			$id_image = $row['id_product'].'-'.$id_image;
		}
		$row['id_image'] = $id_image;
		return $row;
    }
    
	public static function getProductsProperties($id_lang, $query_result, $check_unavailable=false)
    {
        $results_array = array();

        if (is_array($query_result)) {
            foreach ($query_result as $row) {
                if ($row2 = Product::getProductProperties($id_lang, $row, null, $check_unavailable)) {
                    $results_array[] = $row2;
                }
            }
        }

        return $results_array;
    }
	}
}
