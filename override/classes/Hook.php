<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * @author    Presta.Site
 * @copyright 2016 Presta.Site
 * @license   LICENSE.txt
 */
class Hook extends HookCore
{
    /**
     * Get list of modules we can execute per hook
     *
     * @since 1.5.0
     * @param string $hook_name Get list of modules for this hook if given
     * @return array
     */
    /*
    * module: pmrestrictions
    * date: 2019-01-24 17:46:51
    * version: 1.0.5
    */
    public static function getHookModuleExecList($hook_name = null)
    {
        $hookModuleExecList = parent::getHookModuleExecList($hook_name);

        if (Module::isEnabled('pmrestrictions')) {
            $ps_version = _PS_VERSION_;
            $ps_version = Tools::substr($ps_version, 0, 3);
            $pmethods_hook_name = ($ps_version <= 1.6 ? 'displayPayment' : 'paymentOptions');
            if ($hook_name == $pmethods_hook_name) {

                $key = array_search('bankwire', array_column($hookModuleExecList, 'module'));


                $context = Context::getContext();
                $idcart = $context->cart->id;

                $sql='SELECT *, (datediff(CURDATE(), start_date))*-1 as gg FROM `fuoriporta`.`ps_myownreservations_cartproducts` WHERE  id_cart = '.$idcart.' AND (datediff(CURDATE(), start_date))*-1 < 8';

                //Disactive bankwite prenotation < 8 gg
                $res = Db::getInstance()->ExecuteS($sql);
                if($res) unset($hookModuleExecList[$key]);

                //die('--'.$idcart );

                $pmrestrictions = Module::getInstanceByName('pmrestrictions');
                if ($pmrestrictions && $pmrestrictions->active) {
                    return $pmrestrictions->filterPaymentModules($hookModuleExecList);
                } else {
                    return $hookModuleExecList;
                }
            }
        }
        return $hookModuleExecList;
    }
}
